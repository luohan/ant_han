!***********************************************************************
!   System:                     N2O
!   Functional form:            six-order polynomials
!***********************************************************************
      subroutine prepot
      return
      end
!***********************************************************************
      subroutine pot(r,e,de,nt,nsurf)
        implicit none
        double precision r, e, de
        integer i,nt,nsurf
        dimension r(nt,3),e(nt),de(3,nt)

        if (nsurf .eq. 1)then
          do i = 1,nt
            call n2o_1ap(r(i,:),e(i),de(:,i))
          enddo
        elseif (nsurf .eq. 2)then
          do i = 1,nt
            call n2o_spin_orbit(r(i,:),e(i),de(:,i))
          enddo
        elseif (nsurf .eq. 3)then
          do i = 1,nt
            call n2o_3app(r(i,:),e(i),de(:,i))
          enddo
        else
          write(6,*)' WARNING:  nsurf = ',nsurf, &
            ' in N2O potential routine!'
          stop
        endif
        return
      end subroutine pot
!************************************************************************
!   System:                     N2O (lowest 3A" state)
!   Functional form:            six-order polynomials
!   Common name:                N2O-3App (adiabatic triplet ground state)
!   Number of derivatives:      1
!   Number of bodies:           3
!   Number of electronic surfaces: 1
!   Interface: Section-2
!
!   References: P.Gamallo, Miguel Gonzalez, and R. Sayos
!               doi:10.1063/1.1586251
!
!   Note: HE-MM-1 format
!
!    1 : O - N
!    2 : N - N'
!    3 : N'- O
!   Aysymptotic energy: N+NO:  - De(1)
!                       O+N2:  - De(2)
!                       N+N+O:   0eV
!   Input: X(3),Y(3),Z(3)               in units of bohr
!   Output: E                           in units of hartree
!   Output: dEdX(3),dEdY(3),dEdZ(3)     hartree/bohr
!***********************************************************************
      subroutine n2o_3app(Rin,E,dVdR)

      implicit none

      integer,parameter :: dp=8
      ! input and ioutput
      ! first index is for different atom
      ! second index is for x,y,z
      ! oxygen should always be the first atom
      real(dp),intent(in) :: Rin(3)
      real(dp),intent(out) :: E,dVdR(3)

      ! constants
      real(dp),parameter :: kcal2ev=0.0433634D0,ev2hart=0.0367502D0
      real(dp),parameter :: bohr2a=0.52917721067D0
      real(dp),parameter :: irt2 =1.0D0/dsqrt(2.0D0)
      ! two body constants NO, N2, NO
      integer,parameter :: MolType(3) =  (/1,2,1/)
      real(dp),parameter :: De(2)=(/152.53D0,228.41D0/)*kcal2ev
      real(dp),parameter :: Re(2)=(/1.1508D0,1.0977D0/)
      real(dp),parameter :: a(3,2)=reshape((/4.3205D0,1.6392D0,1.4946D0,&
                               5.2376D0,6.3901D0,6.3676D0/),(/3,2/))

      ! three body constants
      real(dp),parameter :: Rtri(2)=(/1.3231D0,2.0980D0/)
      real(dp),parameter :: c(50) =  (/   7.4166D0,   6.2889D0,  14.4281D0,&
                             13.4837D0,  14.8181D0,   8.9826D0,   1.8306D0,&
                             -0.7505D0,  -8.2247D0, -38.7067D0, -48.1654D0,&
                            -15.3167D0,  -0.3524D0,  12.8055D0,  32.9097D0,&
                             71.4660D0,  45.2012D0,   3.0192D0,  12.9529D0,&
                             -7.0240D0, -31.3627D0,  -9.0038D0, -21.3448D0,&
                            -12.7283D0,   7.2399D0,   5.4116D0,   4.3132D0,&
                             -0.1399D0,  -8.4511D0, -30.9122D0, -34.5457D0,&
                            -23.2229D0, -12.2485D0,  12.7846D0,  74.1564D0,&
                             74.0436D0,  14.2243D0, -85.2948D0,-150.0063D0,&
                            -47.4708D0,  69.8672D0,  45.2050D0,  -5.4665D0,&
                             -3.0718D0,  13.6449D0,  11.6069D0,  41.4556D0,&
                              7.7826D0, -23.4852D0,  -3.8063D0/)
      integer,parameter :: k_stride(4) = (/1,28,15,6/)
      integer :: imax,jmax,kmax,ic2(2)
      real(dp),parameter :: gam(2) = (/2.5067D0, 3.0405D0/)

      ! variables used in the function
      integer :: i,j,k,ic
      real(dp) :: R(3)
      real(dp) :: rho(3),V2(3),rhotri(3),dV2dR(3)
      real(dp) :: spower(3,0:6),s(3),invs(3),dSdR(3),pfac(3),P,T,fac,dPdR(3)
      real(dp) :: tfac(2), dTdS(2),dTdR(3),V3,dV3dR(3)
      real(dp) :: temp,temp1

      R = Rin*bohr2a
      dV2dR = 0.0d0
      !-------------- Two-body interaction term
      do i = 1,3
        j = MolType(i)
        rho(i) = R(i) - Re(j)
        rhotri(i) = R(i) - Rtri(j)

        V2(i) = -De(j)*(1.0D0 + rho(i)*(a(1,j)+rho(i)*(a(2,j)+rho(i)*a(3,j))))
        V2(i) = V2(i)*dexp(-a(1,j)*rho(i))

        dV2dR(i) = -De(j)*(a(1,j)+rho(i)*(2.0D0*a(2,j)+3.0D0*rho(i)*a(3,j)))
        dV2dR(i) = dV2dR(i)*dexp(-a(1,j)*rho(i))
        dV2dR(i) = dV2dR(i) + V2(i)*(-a(1,j))
      enddo

      !--------------  Three-body interaction term
      ! precompute power
      s(1) = rhotri(2)
      s(2) = irt2*(rhotri(1)+rhotri(3))
      s(3) = irt2*(rhotri(1)-rhotri(3))
      invs(1) = 1.0D0/s(1)
      invs(2) = irt2/s(2)
      invs(3) = irt2/s(3)

      spower(:,0) = 1.0d0
      spower(:,1) = s
      do i = 2,6
        spower(:,i) = spower(:,i-1)*s
      enddo

      P = 0.0D0
      dPdR = 0.0D0
      dSdR = 0.0d0


      if (s(1)*s(2)*s(3) .ne. 0.0d0)then
         ic = 0
         do k = 0,6,2
            pfac(3) = invs(3)*dble(k)
            do j = 0,6-k,1
               pfac(2) = invs(2)*dble(j)
               dSdR(1) = pfac(2) + pfac(3)
               dSdR(3) = pfac(2) - pfac(3)
               temp = spower(2,j)*spower(3,k)
               do i = 0,(6-k-j)
                  dSdR(2) = invs(1)*dble(i)

                  ic = ic + 1
                  fac = c(ic)*spower(1,i)*temp
                  P = P + fac
                  dPdR = dPdR + fac*dSdR
               enddo
            enddo
         enddo
      else
         ! the following condition rarely happens
         if (s(3) .eq. 0.0d0) then
            kmax = 0
         else
            kmax = 6
         endif

         ic2(1) = 0
         do k = 0, kmax, 2
            if (s(2) .eq. 0.0d0) then
               jmax = 1
            else
               jmax = 6-k
            endif
            if ( k .eq. 6) jmax = 0
            ic2(1) = ic2(1)+k_stride(k/2+1) !index of 0,0,k
            if (k.eq. 0) then
               temp = 0.0d0
            else
               temp = irt2*dble(k)*spower(3,k-1)
            endif

            do j = 0,jmax
               if (s(1) .eq. 0.0d0) then
                  imax = 1
               else
                  imax = 6-k-j
               endif
               if ((k+j) .eq. 6) imax = 0
               ic2(2) = ic2(1) + int(dble(j)*(7.0d0-dble(k)-(dble(j)-1.0d0)/2.0d0)) !index of 0,j,k

               temp1 = spower(2,j)*spower(3,k)
               if ( j .eq. 0) then
                  pfac(2) = 0.0d0
               else
                  pfac(2) = irt2*dble(j)*spower(2,j-1)*spower(3,k)
               endif
               pfac(3) = temp*spower(2,j)

               do i = 0,imax
                  ic = ic2(2) + i
                  P = P + c(ic)*spower(1,i)*temp1
                  if ( i .eq. 0)then
                     dSdR(2) = 0.0d0
                  else
                     dSdR(2) = dble(i)*spower(1,i-1)*temp1
                  endif
                  !write(*,'("indx=",6I3,F8.3)') i,j,k,ic,ic2(1),ic2(2),c(ic)
                  dSdR(1) = (pfac(2)+pfac(3))*spower(1,i)
                  dSdR(3) = (pfac(2)-pfac(3))*spower(1,i)
                  dPdR = dPdR + dSdR*c(ic)
               enddo
            enddo
         enddo
      endif


      pfac(1:2) = gam*s(1:2)/2.0D0
      tfac = 1.0D0 - dtanh( pfac(1:2))
      T = tfac(1)*tfac(2)
      dTdS = 1.0D0/dcosh(pfac(1:2))
      dTdS = - dTdS * dTdS * gam/2.0D0  !dT/dS

      dTdR(2) = dTdS(1)*tfac(2)
      dTdR(1) = tfac(1)*dTdS(2)*irt2
      dTdR(3) = dTdR(1)

      V3 = P*T
      dV3dR = dPdR*T + P*dTdR

      ! ---------- summation
      E = V2(1)+V2(2)+V2(3)+V3
      E = E*ev2hart

      dVdR = dV2dR + dV3dR
      dVdR = dVdR*ev2hart*bohr2a
      end subroutine n2o_3app
!***********************************************************************
!   System:                     N2O (first excited 1A' state)
!   Functional form:            six-order polynomials
!   Common name:                N2O-1Ap
!   Number of derivatives:      1
!   Number of bodies:           3
!   Number of electronic surfaces: 1
!   Interface: Section-2
!
!   References: Miguel Gonzalez, R. Valero and R. Sayos
!               doi:10.1063/1.1327263
!
!   Note: HE-MM-1 format
!
!    1 : O - N
!    2 : N - N'
!    3 : N'- O
!   Aysymptotic energy: N(2D)+NO: V10(1) - De(2) = -3.468 eV
!                       O(1D)+N2: V10(3) - De(1) = -7.5128eV
!                       N+N+O: not tractable
!   Minimum energy:   R(N-N)=1.1326A R(N-O)=1.1999A N-N-O=180
!                     E=-10.8785eV
!   Input: X(3),Y(3),Z(3)               in units of bohr
!   Output: E                           in units of hartree
!   Output: dEdX(3),dEdY(3),dEdZ(3)     hartree/bohr
!***********************************************************************
      subroutine n2o_1ap(Rin,E,dVdR)

      implicit none

      integer,parameter :: dp=8
      ! input and ioutput
      ! first index is for different atom
      ! second index is for x,y,z
      ! Xin : O, N,  N'
      ! X   : N, N', O
      real(dp),intent(in) :: Rin(3)
      real(dp),intent(out) :: E,dVdR(3)


      ! constants
      real(dp),parameter :: kcal2ev=0.0433634D0, ev2hart=0.0367502D0
      real(dp),parameter :: bohr2a=0.52917721067D0
      real(dp),parameter :: irt2 =1.0D0/dsqrt(2.0D0)

      integer,parameter :: MolType(3) =  (/1,2,1/) !ON,NN',N'O

      ! one body constants
      real(dp),parameter :: V10(3) = (/2.6730d0, 2.6730d0, 2.1133d0/)
      real(dp),parameter :: alpha(3)=(/1.5d0, 1.5d0, 1.0d0/) !A^{-1}
      real(dp),parameter :: bp(3,3) = reshape((/-1.0d0, 3.0d0,-1.0d0,&
                                                -1.0d0,-1.0d0, 3.0d0,&
                                                 3.0d0,-1.0d0,-1.0d0/),&
                                               (/3,3/))  !bp(i,j)

      ! two body constants NO, N2, NO
      !real(dp),parameter :: De(2)=(/140.73D0,212.55D0/)*kcal2ev
      real(dp),parameter :: De(2)=(/6.14010d0,9.26610d0/)
      real(dp),parameter :: Re(2)=(/1.1598D0,1.1034D0/)
      real(dp),parameter :: a(5,2)=reshape((/4.14169D0, 0.858731D0,&
                                 -0.972979D0,-3.70379D0, 2.48770D0,&
                                  3.88769D0, 0.100392D0,-0.912322D0,&
                                 -2.73055D0, 1.69645D0/),(/5,2/))

      ! three body constants
      real(dp),parameter :: Rtri(2)=(/2.0d0,1.1343D0/)
      real(dp),parameter :: c(50) =  (/      1.0d00,  16.9687D0,  56.0342D0,&
                              183.288D0,  55.6607D0, -54.5182D0,  15.1949D0,&
                              4.85752D0, -18.5084D0,  58.6889D0,  221.503D0,&
                              176.678D0, -78.3086D0, -4.07967D0, -48.6335D0,&
                              -23.899D0,  150.763D0,   3.6847D0,  7.23895D0,&
                              49.8532D0,   14.073D0,  56.8991D0, -4.06696D0,&
                              51.1781D0,  93.8385D0,  1.87307D0,  14.6403D0,&
                             -1.96208D0, -38.1055D0, -134.143D0, -177.698D0,&
                             -312.039D0,  82.4051D0,  43.3783D0,  17.9349D0,&
                             -179.413D0, -204.166D0,  35.1623D0,  56.3209D0,&
                              5.92703D0,  17.0209D0, -106.102D0,  41.7606D0,&
                              103.779D0,  158.477D0,  112.289D0, -59.6943D0,&
                              113.151D0, -28.7549D0, -74.6061D0/)*(-0.244328D0)
      integer,parameter :: k_stride(4) = (/1,28,15,6/)
      integer :: imax,jmax,kmax,ic2(2)
      real(dp),parameter :: gam(2) = (/2.89960D0, 2.93103D0/)

      ! variables used in the function
      integer :: i,j,k,ic
      real(dp) :: R(3)
      real(dp) :: V1(3),dV1dR(3),Sp(3),rho(3)
      real(dp) :: V2(3),rhotri(3),dV2dR(3)
      real(dp) :: spower(3,0:6),s(3),invs(3),dSdR(3),pfac(3),P,T,fac,dPdR(3)
      real(dp) :: tfac(2), dTdS(2),dTdR(3),V3,dV3dR(3)
      real(dp) :: temp,temp1

      ! X order : O N N'

      R = Rin*bohr2a

      do i = 1,3
        j = MolType(i)
        rho(i) = R(i) - Re(j)
        rhotri(i) = R(i) - Rtri(j)
      enddo

      ! one body interaction
      dV1dR = 0.0d0
      do i = 1,3
        Sp(i) = bp(i,1)*rhotri(1) + bp(i,2)*rhotri(2) + bp(i,3)*rhotri(3)
        temp = alpha(i)*Sp(i)*0.5d0
        V1(i) = V10(i)*0.5d0*(1.0d0 - dtanh(temp))
        ! dV1 / d Sp
        temp1 = -V10(i)*0.5d0/dcosh(temp)/dcosh(temp)*alpha(i)*0.5d0
        dV1dR(1) = dV1dR(1) + temp1*bp(i,1)
        dV1dR(2) = dV1dR(2) + temp1*bp(i,2)
        dV1dR(3) = dV1dR(3) + temp1*bp(i,3)
      enddo

      ! two body interaction
      dV2dR = 0.0d0
      do i = 1,3
        j = MolType(i)

        V2(i) = a(4,j) + a(5,j)*rho(i)
        V2(i) = a(3,j) + V2(i)*rho(i)
        V2(i) = a(2,j) + V2(i)*rho(i)
        V2(i) = a(1,j) + V2(i)*rho(i)
        V2(i) = 1.0D0 + rho(i)*V2(i)
        V2(i) = -De(j)*V2(i)*dexp(-a(1,j)*rho(i))

        dV2dR(i) = 4.0d0*a(4,j) + 5.0d0*a(5,j)*rho(i)
        dV2dR(i) = 3.0d0*a(3,j) + dV2dR(i)*rho(i)
        dV2dR(i) = 2.0d0*a(2,j) + dV2dR(i)*rho(i)
        dV2dR(i) =       a(1,j) + dV2dR(i)*rho(i)
        dV2dR(i) = -De(j)*dV2dR(i)*dexp(-a(1,j)*rho(i))
        dV2dR(i) = dV2dR(i) + V2(i)*(-a(1,j))
      enddo

      !--------------  Three-body interaction term
      ! precompute power
      s(1) = rhotri(2)
      s(2) = irt2*(rhotri(1)+rhotri(3))
      s(3) = irt2*(rhotri(1)-rhotri(3))
      invs(1) = 1.0D0/s(1)
      invs(2) = irt2/s(2)
      invs(3) = irt2/s(3)

      spower(:,0) = 1.0d0
      spower(:,1) = s
      do i = 2,6
        spower(:,i) = spower(:,i-1)*s
      enddo

      P = 0.0D0
      dPdR = 0.0D0
      dSdR = 0.0d0

      if (s(1)*s(2)*s(3) .ne. 0.0d0) then
         ic = 0
         do k = 0,6,2
            pfac(3) = invs(3)*dble(k)
            do j = 0,(6-k)
               pfac(2) = invs(2)*dble(j)
               dSdR(1) = pfac(2) + pfac(3)
               dSdR(3) = pfac(2) - pfac(3)
               temp = spower(2,j)*spower(3,k)
               do i = 0,(6-k-j)
                  dSdR(2) = invs(1)*dble(i)

                  ic = ic + 1
                  fac = c(ic)*spower(1,i)*temp
                  !write(*,*) c(ic)/(-0.244328d0),i,j,k
                  P = P + fac
                  dPdR = dPdR + fac*dSdR
               enddo
            enddo
         enddo
      else
         ! the following condition rarely happens
         if (s(3) .eq. 0.0d0) then
            kmax = 0
         else
            kmax = 6
         endif

         ic2(1) = 0
         do k = 0, kmax, 2
            if (s(2) .eq. 0.0d0) then
               jmax = 1
            else
               jmax = 6-k
            endif
            if ( k .eq. 6) jmax = 0
            ic2(1) = ic2(1)+k_stride(k/2+1) !index of 0,0,k
            if (k.eq. 0) then
               temp = 0.0d0
            else
               temp = irt2*dble(k)*spower(3,k-1)
            endif

            do j = 0,jmax
               if (s(1) .eq. 0.0d0) then
                  imax = 1
               else
                  imax = 6-k-j
               endif
               if ((k+j) .eq. 6) imax = 0
               ic2(2) = ic2(1) + int(dble(j)*(7.0d0-dble(k)-(dble(j)-1.0d0)/2.0d0)) !index of 0,j,k

               temp1 = spower(2,j)*spower(3,k)
               if ( j .eq. 0) then
                  pfac(2) = 0.0d0
               else
                  pfac(2) = irt2*dble(j)*spower(2,j-1)*spower(3,k)
               endif
               pfac(3) = temp*spower(2,j)

               do i = 0,imax
                  ic = ic2(2) + i
                  P = P + c(ic)*spower(1,i)*temp1
                  if ( i .eq. 0)then
                     dSdR(2) = 0.0d0
                  else
                     dSdR(2) = dble(i)*spower(1,i-1)*temp1
                  endif
                  !write(*,'("indx=",6I3,F8.3)') i,j,k,ic,ic2(1),ic2(2),c(ic)
                  dSdR(1) = (pfac(2)+pfac(3))*spower(1,i)
                  dSdR(3) = (pfac(2)-pfac(3))*spower(1,i)
                  dPdR = dPdR + dSdR*c(ic)
               enddo
            enddo
         enddo
      endif


      pfac(1:2) = gam*s(1:2)/2.0D0
      tfac = 1.0D0 - dtanh( pfac(1:2))
      T = tfac(1)*tfac(2)
      dTdS = 1.0D0/dcosh(pfac(1:2))
      dTdS = - dTdS * dTdS * gam/2.0D0  !dT/dS

      dTdR(2) = dTdS(1)*tfac(2)
      dTdR(1) = tfac(1)*dTdS(2)*irt2
      dTdR(3) = dTdR(1)

      V3 = P*T
      dV3dR = dPdR*T + P*dTdR

      ! ---------- summation
      E = V1(1)+V1(2)+V1(3)+V2(1)+V2(2)+V2(3)+V3
      E = E-V10(3)+45.37d0*kcal2ev  ! match aymptotic energy O+N2 to experiment
      E = E*ev2hart

      ! dV/d  (R(O-N),R(N-N'), R(N'-O))
      dVdR = dV1dR + dV2dR + dV3dR
      dVdR = dVdR*ev2hart*bohr2a
      end subroutine n2o_1ap
!***********************************************************************
!   System:                     N2O (3A'' and 1A')
!   Common name:                N2O-1Ap
!   Number of derivatives:      1
!   Number of bodies:           3
!   Number of electronic surfaces: 1
!   Interface: Section-2
!
!   References: H. Nakamura, S. Kato
!               doi:10.1063/1.480741
!
!   Note: This is the spin orbit coupling between 3A'' and 1A'
!         MEX point is calculated from Gamallo and Gonzalez's paper
!
!    1 : O - N
!    2 : N - N'
!    3 : N'- O
!
!   Input: X(3),Y(3),Z(3)               in units of bohr
!   Output: E                           in units of hartree
!   Output: dEdX(3),dEdY(3),dEdZ(3)     hartree/bohr
!***********************************************************************
      subroutine n2o_spin_orbit(R,E,dEdR)
      implicit none
      integer,parameter :: dp=8
      real(dp),intent(in) :: R(3) ! in a.u.
      real(dp),intent(out) :: E,dEdR(3)

      ! constants
      real(dp),parameter :: bohr2a=0.52917721067D0
      real(dp),parameter :: autocmi=219474.6313705d0
      real(dp),parameter :: irt2=1.0D0/dsqrt(2.0D0),irt3=1.0/dsqrt(3.0D0)

      ! fiting parameters
      ! MEX: collinear N-N-O, N-N=1.11A, CM-O=2.228A
      real(dp),parameter :: Rmex0(2)=(/1.112545228d0,2.228886191d0/)/bohr2a
      real(dp),parameter :: c(7)=(/1796.0976d0, -687.19611d0, -481.76737d0,&
                              75.714010d0,66.696532d0,148.0255d0,-53.891519d0/)
      real(dp),parameter :: alpha(2)=(/2.19d0,0.57d0/)
      real(dp),parameter :: dQdR(3,3) = reshape((/irt3,  irt2, -irt3*irt2,&
                                           irt3, 0.0d0,  1.0d0/irt2*irt3, &
                                           irt3, -irt2, -irt3*irt2/),(/3,3/))


      ! variables used in the function
      integer :: i
      real(dp) :: Q(3),S(2),temp1,temp2,dtdQ(3)

      !all in a.u.
      Q(1) = irt3*(R(1)+R(2)+R(3))
      Q(2) = irt2*(R(1)-R(3))
      Q(3) = irt3*(1.0d0/irt2*R(2) - irt2*(R(1)+R(3)))

      S(1) = Q(2)*Q(2) + Q(3)*Q(3)
      S(2) = Q(2)*Q(2) - Q(3)*Q(3)

      temp1 = c(1)+c(2)*Q(1)+c(3)*Q(3)+c(4)*Q(1)*Q(1)+c(5)*S(1)+&
              c(6)*Q(1)*Q(3)+c(7)*S(2)
      dtdQ(1) = c(2)+2.0d0*c(4)*Q(1)+c(6)*Q(3)
      dtdQ(2) = 2.0d0*(c(5)+c(7))*Q(2)
      dtdQ(3) = c(3)+2.0d0*(c(5)-c(7))*Q(3)+c(6)*Q(1)

      temp2 = dexp(-alpha(1)*(R(2)-Rmex0(1))-alpha(2)*((R(1)+R(3))/2.0d0-Rmex0(2))**2)

      E = temp1*temp2

      dEdR(2) = E*(-alpha(1))
      dEdR(1) = E*(-alpha(2)*((R(2)+R(3))/2.0d0-Rmex0(2)))
      dEdR(3) = dEdR(2)
      do i = 1,3
        dEdR(i) = dEdR(i) + temp2*(dtdQ(1)*dQdR(1,i)+dtdQ(2)*dQdR(2,i)+dtdQ(3)*dQdR(3,i))
      enddo

      E = E/autocmi
      dEdR = dEdR/autocmi

      end subroutine n2o_spin_orbit
