      subroutine pot(x,y,z,v,dvdx,dvdy,dvdz,natoms,maxatom)

C   System:                        Al
C   Functional form:               Extended Rydberg with Screening and Coordination Number
C   Common name:                   NP-A
C   Number of derivatives:         1
C   Number of bodies:              variable
C   Number of electronic surfaces: 1
C   Interface:                     HO-MM-1
C
C   Notes:  Many-body aluminum potential energy function.  The functional
C           form is from Ref. 1.  The parameters were re-optimized in Ref. 2
C           against a data set of energies for aluminum clusters and
C           nanoparticles and bulk data.  This PEF has a cutoff at 6.5 angstroms.
C
C  References: (1) A. W. Jasper, P. Staszewski, G. Staszewska, N. E. Schultz,
C              and D. G. Truhlar, "Analytic Potential Energy Functions
C              for Aluminum Clusters," J. Phys. Chem. B 108, 8996 (2004).
C              (2) A. W. Jasper and D. G. Truhlar, "Analytic Potential Energy
C              Functions for Simulating Aluminum Nanoparticles," in preparation.
C The subroutine is optimized by Dr. Zhen Hua Li based on the original
C version of A. W. Jasper (NP-Ad.f). 
C
C  Units:
C       energies    - hartrees
C       coordinates - bohrs
C
C  v       --- energy of the system
C  x,y,z   --- one-dimensional arrays of the Cartesian coordinates
C                    of the system
C  dvdx,dvdy,dvdz   --- one-dimensional arrays of the gradients with respect
C                       to the Cartesian coordinates of the system (output)
C  natoms   --- number of atoms in the system
C  maxatom --- maximal number of atoms
C
C  Note:  Derivatives were generated using ADIFOR, whose disclaimer follows.
C
C                           DISCLAIMER
C
C   This file was generated on 09/13/04 by the version of
C   ADIFOR compiled on June, 1998.
C
C   ADIFOR was prepared as an account of work sponsored by an
C   agency of the United States Government, Rice University, and
C   the University of Chicago.  NEITHER THE AUTHOR(S), THE UNITED
C   STATES GOVERNMENT NOR ANY AGENCY THEREOF, NOR RICE UNIVERSITY,
C   NOR THE UNIVERSITY OF CHICAGO, INCLUDING ANY OF THEIR EMPLOYEES
C   OR OFFICERS, MAKES ANY WARRANTY, EXPRESS OR IMPLIED, OR ASSUMES
C   ANY LEGAL LIABILITY OR RESPONSIBILITY FOR THE ACCURACY, COMPLETE-
C   NESS, OR USEFULNESS OF ANY INFORMATION OR PROCESS DISCLOSED, OR
C   REPRESENTS THAT ITS USE WOULD NOT INFRINGE PRIVATELY OWNED RIGHTS.
C
        implicit double precision(a-h, o-z)
        parameter (autoev = 27.2113961d0)
        parameter (autoang = 0.529177249d0)
        dimension x(maxatom),y(maxatom),z(maxatom)
        double precision gxs(maxatom),gys(maxatom),gzs(maxatom),
     &  gcorr(maxatom),fgcn(maxatom,maxatom),
     &  ee(maxatom,maxatom),eeb(maxatom,maxatom)
        double precision gxf(maxatom,maxatom),gyf(maxatom,maxatom),
     &                   gzf(maxatom,maxatom)
        double precision dvdy(maxatom),dvdz(maxatom),dvdx(maxatom)
        double precision dx(maxatom,maxatom),dy(maxatom,maxatom),
     &  dz(maxatom,maxatom),rij(maxatom,maxatom)

c Nanoparticle parameters (Ref. 2)
c       two-body interaction
        de = 1.71013678553981441d0 / autoev
        re = 5.08182706399609163d0
        a1 = 1.24074255007327805d0
        a2 = 0.551880801172447422d0
        a3 = 0.129970688812896917d0
        au2 = 0.143243771372740580d0
        bu2 = 6.50000000000000000d0 / autoang
Cc       screening
        xk1= 4.24002677622442103d0
        xk2= 0.117656503960960862d0
        xk3= 4.78063179546451522d0
        au23= 1.63973192904916298d0
c       coordination number
        g1= 0.708483373073205747d0
        du2= 1.13286279334603357d0
        gu2= 0.663930057862113232d0
        gzero= 8.54498572971970027d0
        g2= 5.39584023677170066d0/autoang
c       effective two-body
        deb= 1.42526928794948882d0/autoev
        reb= 4.87735706664722812d0
        a1b= 1.20666644170640880d0
        a2b= 0.728296669115275908d0
        a3b= 0.215461507389864804d0
        au2b= 0.138211749991007299d0
C        bu2b= 6.50000000000000000/autoang

c Initialize arrays
        v = 0.d0
        rgzero = 1.d0 / gzero
        g1g2 = g1*g2
        d2g2g0 = du2*gu2*rgzero
        do 99998 i = 1, natoms
        dvdx(i) = 0.0d0
        dvdy(i) = 0.0d0
        dvdz(i) = 0.0d0
C store dxij,dyij,dzij,rij(i,j) for the later usage
          do j=i+1,natoms
            dx(i,j)=x(i)-x(j)
            dy(i,j)=y(i)-y(j)
            dz(i,j)=z(i)-z(j)
            rij(i,j)=dsqrt(dx(i,j)**2+dy(i,j)**2+dz(i,j)**2)
            rij(j,i) = rij(i,j)
            dx(j,i)=-dx(i,j)
            dy(j,i)=-dy(i,j)
            dz(j,i)=-dz(i,j)
            rrij = 1.d0/rij(i,j)
            if (rij(i,j).lt.g2) then
                    fgcn(i,j) = dexp( g1/( 1.d0-g2*rrij ) )
            else
                    fgcn(i,j) = 0.d0
            endif
            fgcn(j,i) = fgcn(i,j)
            if (rij(i,j).lt.bu2) then
C           pairwise exponential (including cutoff function)
              rho     = rij(i,j)-re
C           exponential part of the cutoff function
              tmp     = 1.d0 / (1.d0 - bu2*rrij)
              efcut   = au2 * tmp
              ee(i,j) = dexp(-a1*rho + efcut)
C           effective two-body exponential (including cutoff function)
              rho     = rij(i,j)-reb
              efcut   = au2b * tmp
              eeb(i,j)= dexp(-a1b*rho + efcut)
            else
              ee(i,j) = 0.d0
              eeb(i,j) = 0.d0
            endif
            ee(j,i) = ee(i,j)
            eeb(j,i)= eeb(i,j)
          enddo
99998   continue
C
        do 20 i = 1, natoms
          gcorr(i) = 0.d0
          do k = 1, natoms
            gxf(k,i) = 0.0d0
            gyf(k,i) = 0.0d0
            gzf(k,i) = 0.0d0
          enddo
          do 21 j = 1, natoms
            rr    = rij(i,j)
            if (i .eq. j .or. rr.ge.g2) goto 21
            gcorr(i)= gcorr(i) + fgcn(i,j)
            dfgcn   = -fgcn(i,j)*g1g2/( (rij(i,j) - g2)**2 )
            dum     =  dfgcn/rr
            gxf(i,i)=  gxf(i,i) + dum*dx(i,j)
            gyf(i,i)=  gyf(i,i) + dum*dy(i,j)
            gzf(i,i)=  gzf(i,i) + dum*dz(i,j)
            gxf(j,i)=  gxf(j,i) - dum*dx(i,j)
            gyf(j,i)=  gyf(j,i) - dum*dy(i,j)
            gzf(j,i)=  gzf(j,i) - dum*dz(i,j)
21        continue
20      continue

        do 510 i = 1, natoms
          do 515 j = i + 1, natoms
            deedr=0.d0
            deebdr=0.d0
            rr    = rij(i,j)
            if (rr .ge. bu2) goto 515
            do k = 1, natoms
              gxs(k) = 0.0d0
              gys(k) = 0.0d0
              gzs(k) = 0.0d0
            enddo
            rrij   = 1.d0 /rr
            rrijr0 = 1.d0 /( rr-bu2 )
            rrijr02 = rrijr0*rrijr0
c CN term
            gifgcn  = rgzero*( gcorr(i)-fgcn(i,j) )
            gjfgcn  = rgzero*( gcorr(j)-fgcn(i,j) )
C  when gu2 < 1, and natoms = 2, or gifgcn = 0.0 this algoryhm may
C   results in infinit gijg0gu2 = 1/0.0 (gu2-1 <0)
            if (gifgcn.gt.1.d-20) then
              gig0gu2  = gifgcn**(gu2-1.d0)
              rgig0gu2 = 1.d0/(1.d0 + gig0gu2*gifgcn)
            else
              gig0gu2  = 0.d0
              rgig0gu2 = 1.d0
            endif
            if (gjfgcn.gt.1.d-20) then
              gjg0gu2  = gjfgcn**(gu2-1.d0)
              rgjg0gu2 = 1.d0/(1.d0 + gjg0gu2*gjfgcn)
            else
              gjg0gu2  = 0.d0
              rgjg0gu2 = 1.d0
            endif
            gij      = rgig0gu2*rgjg0gu2

C
C effective two-body
            rhob  = rr-reb
            rhob2 = rhob*rhob
            rhob3 = rhob2*rhob
            polyb = 1.d0+a1b*rhob+a2b*rhob2+a3b*rhob3
C           exponential part of cutoff function
C           eeb*fcut
            ev2b     = -deb*eeb(i,j)*polyb

C effective two-body derivative
C derivative of the exponential part of the cutoff function
            defcut = -au2b*bu2*rrijr02
C           deeb*fcut + eeb*dfcut
            dpolyb = a1b+2.d0*a2b*rhob+3.d0*a3b*rhob2
C           derivative of the exponentials (ER2 + fcut)
            deebdr = -a1b + defcut
            deebdr = ev2b*deebdr - deb*dpolyb*eeb(i,j)

            s = 0.d0
            do 512 l = 1, natoms
              if (l .eq. j .or. l .eq. i) goto 512
              if (rij(j,l) .ge. bu2) goto 512
              if (rij(i,l) .ge. bu2) goto 512

C do S term, needs xk1,xk2,xk3
              rrr    = (rij(i,l) + rij(j,l)) * rrij
              rrilr0 = 1.d0 /( rij(i,l)-bu2 )
              rrjlr0 = 1.d0 /( rij(j,l)-bu2 )
              d3_1   =  xk2 * rrr ** (xk3 - 1.d0)
              scut   =  au23 * ( rij(i,l)*rrilr0 + rij(j,l)*rrjlr0 +
     &                           rr*rrijr0 )
              term   = xk1 * dexp( -d3_1 * rrr + scut)

C screen term derivatives
              tmp    = xk3 * d3_1 * rrij
C             dtermij/rij
              dtermij = term * ( tmp * rrr - au23*bu2*rrijr02)*rrij
C             dtermil/ril
              dtermil = term * (-tmp - au23*bu2*rrilr0*rrilr0)/rij(i,l)
C             dtermjl/rjl
              dtermjl = term * (-tmp - au23*bu2*rrjlr0*rrjlr0)/rij(j,l)

              gxs(i) = gxs(i) + dtermij*dx(i,j)
              gxs(i) = gxs(i) + dtermil*dx(i,l)
              gxs(j) = gxs(j) + dtermjl*dx(j,l)
              gxs(j) = gxs(j) - dtermij*dx(i,j)
              gxs(l) = gxs(l) - dtermil*dx(i,l)
              gxs(l) = gxs(l) - dtermjl*dx(j,l)

              gys(i) = gys(i) + dtermij*dy(i,j)
              gys(i) = gys(i) + dtermil*dy(i,l)
              gys(j) = gys(j) + dtermjl*dy(j,l)
              gys(j) = gys(j) - dtermij*dy(i,j)
              gys(l) = gys(l) - dtermil*dy(i,l)
              gys(l) = gys(l) - dtermjl*dy(j,l)

              gzs(i) = gzs(i) + dtermij*dz(i,j)
              gzs(i) = gzs(i) + dtermil*dz(i,l)
              gzs(j) = gzs(j) + dtermjl*dz(j,l)
              gzs(j) = gzs(j) - dtermij*dz(i,j)
              gzs(l) = gzs(l) - dtermil*dz(i,l)
              gzs(l) = gzs(l) - dtermjl*dz(j,l)

              s = s + term

512           continue

            d2_v = dtanh (s)
            d1_p = 1.0d0 - ( d2_v *  d2_v)
            fs = d2_v

C pairwise term
            rho  = rr-re
            rho2 = rho*rho
            rho3 = rho2*rho
            poly = 1.d0+a1*rho+a2*rho2+a3*rho3
            ev2   = -de*ee(i,j)*poly
C pairwise term derivative
C derivative of the exponential part of the cutoff function
            defcut = -au2*bu2*rrijr02
C           dee*fcut + ee*dfcut
            dpoly = a1+2.d0*a2*rho+3.d0*a3*rho2
C           derivative of the exponentials (ER2 + fcut)
            deedr = -a1 + defcut
            deedr =  ev2*deedr - de*dpoly*ee(i,j)

C            Effective two body derivatives du(rij)(fs+fcn)/rij and
C            pairwise term derivative
            deebdr  = deedr*rrij - deebdr*( fs + du2*(1.d0-gij))*rrij
            dvdx(i) = dvdx(i) + deebdr*dx(i,j)
            dvdy(i) = dvdy(i) + deebdr*dy(i,j)
            dvdz(i) = dvdz(i) + deebdr*dz(i,j)
            dvdx(j) = dvdx(j) - deebdr*dx(i,j)
            dvdy(j) = dvdy(j) - deebdr*dy(i,j)
            dvdz(j) = dvdz(j) - deebdr*dz(i,j)
C Sterm derivatives
            tmp1=d1_p*ev2b
            do k = 1, natoms
            dvdx(k) = dvdx(k) - tmp1*gxs(k)
            dvdy(k) = dvdy(k) - tmp1*gys(k)
            dvdz(k) = dvdz(k) - tmp1*gzs(k)
            enddo
C 
C CN term derivatives
            if (rr.lt.g2) then
              dfgcn   = -fgcn(i,j)*g1g2/( (rr - g2)**2 )
            else
                    dfgcn=0.d0
            endif
              pre     = ev2b*d2g2g0*gij
              prei    = pre*gig0gu2*rgig0gu2
              prej    = pre*gjg0gu2*rgjg0gu2
              dum     = (prei+prej)*dfgcn*rrij

              dvdx(i) = dvdx(i) + dum*dx(i,j)
              dvdy(i) = dvdy(i) + dum*dy(i,j)
              dvdz(i) = dvdz(i) + dum*dz(i,j)
              dvdx(j) = dvdx(j) - dum*dx(i,j)
              dvdy(j) = dvdy(j) - dum*dy(i,j)
              dvdz(j) = dvdz(j) - dum*dz(i,j)

            do k = 1, natoms
            dvdx(k) = dvdx(k) - (prei*gxf(k,i)+prej*gxf(k,j))
            dvdy(k) = dvdy(k) - (prei*gyf(k,i)+prej*gyf(k,j))
            dvdz(k) = dvdz(k) - (prei*gzf(k,i)+prej*gzf(k,j))
            enddo


            v = v + ev2 - ev2b*fs - ev2b*du2*(1.d0-gij)
515       continue
510     continue

      return
      end

      subroutine prepot

      return
      end
