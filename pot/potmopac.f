      SUBROUTINE pot(indatom,x,v,dx,dograd,natoms,n3tm)
      use param
      implicit none
!xxx      include 'param.f'
      include 'c_mopac.f90'
      integer i,j,natoms,n3tm
      integer indatom(natoms)
      real*8 v,x(3,natoms),dx(n3tm),dxyz(maxpar)
      real*8 calau,escf
      logical dograd
C
C  MOPAC COMMON BLOCK
      INTEGER IFLEPO, IITER
      COMMON /MESAGE/ IFLEPO,IITER
C
C  Part of this subroutine is obtained by modifying the mopeg subroutine 
C  in MORATE program
      
C
C   MOPAC data statements
C
      SAVE
      DATA CALAU   /627.5095D0/
C
C   Turn off the symmetry restrictions if they are on; this ensures
C   that the derivative of the energy with respect to all the
C   coordinates are returned.
C
!        CALL DOINIT
!        CALL MOLDAT(2)   
      IF (NDEP .NE. 0) NDEP = 0
C
C   Convert the Coordinates from Bohr to Angstrom
C
      do i = 1, natoms
         if(indatom(i) .le. 107) then
           labels(i) = indatom(i)
         else 
           write(6,*) 'Model atom cannot be used with MOPAC'
           write(6,*) 'indatom(',i,')',indatom(i)
           stop
         endif
      do j = 1, 3
         coord(j,i) = x(j,i) * autoang
      enddo        
      enddo  
      matoms = natoms
C
C   Set up constants and flags
C
         NA(1)=99
C
C   Set up the variables in the array LOC; LOC(1,I)=atom to be optimized,
C   LOC(2,I)=internal coordinate to be optimized.
C
         NVAR=0
         DO 20 I=1,NATOMS
         DO 20 J=1,3
               NVAR=NVAR+1
               LOC(1,NVAR)=I
               LOC(2,NVAR)=J
 20      CONTINUE
C
         IITER = 0
!        ESCF = 1D9
         CALL iter_INIT
         CALL COMPFG(COORD, .TRUE., ESCF, .TRUE.,DXYZ , DOGRAD)
         IF(IITER.EQ.2) then
! In case SCF does not converged, we set energy to a very large value
           ESCF = 1D9
           write(6,*) 'SCF failed in MOPAC'
           do i = 1,NATOMS
           write(6,'(2X,3f12.6)') (coord(j,i)*autoang,j =1, 3)
           enddo
!          stop
        endif

C
         V = ESCF/CALAU
         IF (DOGRAD) THEN 
            DO 30 I = 1,N3TM
               DX(I) = DXYZ(I)*AUTOANG/CALAU 
 30         CONTINUE
         ENDIF
C
      return
      end
       
      subroutine  prepot
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C
      INCLUDE '../mopac_src/SIZES.i'
C
C   MOPAC common blocks
C
      COMMON /KEYWRD/ KEYWRD
      COMMON /GEOVAR/ XPARAM(MAXPAR), NVAR, LOC(2,MAXPAR)               
      COMMON /GEOSYM/ NDEP,LOCPAR(MAXPAR),IDEPFN(MAXPAR),LOCDEP(MAXPAR)
      COMMON /GEOKST/ MAXATM,LABELS(NUMATM),
     1NA(NUMATM),NB(NUMATM),NC(NUMATM)
      COMMON /MOLKSI/ NUMAT,NAT(NUMATM),NFIRST(NUMATM),
     1                NMIDLE(NUMATM),NLAST(NUMATM), NORBS,
     2                NELECS,NALPHA,NBETA,NCLOSE,NOPEN
      COMMON /MOLKSR/ FRACT
      COMMON /GEOM  / GEO(3,NUMATM)
      COMMON /GRADNT/ GRAD(MAXPAR),GNORM
      COMMON /NUMCAL/ NUMCAL
      COMMON /TIMER / TIME0
      COMMON /PATHI / LATOM,LPARAM
      COMMON /PATHR / REACT(200)
      COMMON /COORD/ COORD(3,NUMATM)
C
      COMMON /SRPI/ IBTPTR(107), NATPTR(MXATSP), NATSP
     *       /SRPL/ ISSRP
     *       /SRPR/ BETSS(MAXBET), BETSP(MXATSP,MXATSP), BETPP(MAXBET)
      LOGICAL ISSRP
      COMMON /IOCM/ IREAD
C     COMMON /DOPRNT/ DOPRNT                                            LF0510
C     LOGICAL DOPRNT     

      DIMENSION GEODUM(3, NUMATM)
      EQUIVALENCE (GEO, GEODUM)
C
      CHARACTER*80 KEYWRD
      SAVE
C
C   Initialize the MOPAC variable NUMCAL
C
C     NUMCAL = 0
C     NUMCAL= NUMCAL + 1
      NUMCAL = 1
      TIME0=second()
      IREAD = 8 
      OPEN(unit=IREAD,file='mopac.in',status='old')
      CALL READMO      
      CLOSE(IREAD)
C     NUMAT = NT
      IF(INDEX(KEYWRD,'EXTERNAL') .NE. 0) THEN
         CALL MOLDAT(0)     
         CALL EXTPAR       
      ELSE
         CALL RESETC
         CALL MOLDAT(2)   
      ENDIF
C     DOPRNT = .true.
C
      IF (INDEX(KEYWRD,' XYZ') .NE. 0.AND.NVAR.NE.0) THEN
C
C   SET ALL OPTIMIZATION FLAGS TO 1.  IT IS POSSIBLE TO SPECIFY " XYZ"
C   AND HAVE 3N-6 FLAGS SET AND STILL NOT HAVE EVERY ATOM MARKED FOR
C   OPTIMIZATION.  THIS CAN HAPPEN IF DUMMY ATOMS ARE PRESENT
C   TO PREVENT THIS, WE EXPLICITELY SET ALL FLAGS.
C
         NVAR=0
         DO 30 I=1,MAXATM
C
C  I DON'T THINK THE FOLLOWING LOGICAL WILL EVER BE TRUE, BUT I'M NOT
C  TAKING IT OUT IN THIS VERSION, JUST IN CASE.
C
            IF(LABELS(I).EQ.99) GOTO 30
            DO 20 J=1,3
               NVAR=NVAR+1
               LOC(1,NVAR)=I
               LOC(2,NVAR)=J
   20       XPARAM(NVAR)=GEO(J,I)
   30    CONTINUE
      ENDIF
      IF (INDEX(KEYWRD,'RESTART').EQ.0)THEN
         IF (INDEX(KEYWRD,'1SCF') .NE. 0) THEN
            IF(LATOM.NE.0)THEN
C              WRITE (FU6, 6100)
               STOP 'MOPOPT 2'
            ENDIF
            NVAR=0
            IF(INDEX(KEYWRD,'GRAD').NE.0) THEN
               NVAR=0
               DO 50 I=2,MAXATM
                  IF(LABELS(I).EQ.99) GOTO 50
                  IF(I.EQ.2)ILIM=1
                  IF(I.EQ.3)ILIM=2
                  IF(I.GT.3)ILIM=3
                  DO 40 J=1,ILIM
                     NVAR=NVAR+1
                     LOC(1,NVAR)=I
                     LOC(2,NVAR)=J
   40             XPARAM(NVAR)=GEO(J,I)
   50          CONTINUE
            ENDIF
         ENDIF
      ENDIF

c
c    Optimizers
c
      IF(INDEX(KEYWRD,'NLLSQ') .NE. 0) THEN
         CALL NLLSQ(XPARAM, NVAR )
         CALL COMPFG(XPARAM,.TRUE.,ESCF,.TRUE.,GRAD,.TRUE.)
         GO TO 60
      ENDIF
C
      IF(INDEX(KEYWRD,'SIGMA') .NE. 0) THEN
         CALL POWSQ(XPARAM, NVAR, ESCF)
         GO TO 60
      ENDIF
C
C  EF OPTIMISATION (new method of choice for TS)
C
      IF(INDEX(KEYWRD,' EF').NE.0 .OR. INDEX(KEYWRD,' TS').NE.0) THEN
        CALL EF (XPARAM,NVAR,ESCF)
        GOTO 60
      ENDIF
C
C ORDINARY GEOMETRY OPTIMISATION
C
      CALL FLEPO(XPARAM, NVAR, ESCF)
   60 CALL WRITMO(TIME0, ESCF)
      CALL GMETRY(GEO,COORD)
C
C   Set up the geometry in Cartesians
C
      CALL GMETRY (GEODUM, COORD)
C     write(6,*) 'ESCF =', ESCF
C     stop 'setmopac'
      end


