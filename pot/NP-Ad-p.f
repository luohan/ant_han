      subroutine pot(x,y,z,v,dvdx,dvdy,dvdz,cell,natom,maxatom)

C   System:                        Al
C   Functional form:               Extended Rydberg with Screening and Coordination Number
C   Common name:                   NP-A
C   Number of derivatives:         1
C   Number of bodies:              variable
C   Number of electronic surfaces: 1
C   Interface:                     HO-MM-1
C
C   Notes:  Many-body aluminum potential energy function.  The functional
C           form is from Ref. 1.  The parameters were re-optimized in Ref. 2
C           against a data set of energies for aluminum clusters and
C           nanoparticles and bulk data.  This PEF has a cutoff at 6.5 angstroms.
C
C  References: (1) A. W. Jasper, P. Staszewski, G. Staszewska, N. E. Schultz,
C              and D. G. Truhlar, "Analytic Potential Energy Functions
C              for Aluminum Clusters," J. Phys. Chem. B 108, 8996 (2004).
C              (2) A. W. Jasper and D. G. Truhlar, "Analytic Potential Energy
C              Functions for Simulating Aluminum Nanoparticles," in preparation.
C
C  Units:
C       energies    - hartrees
C       coordinates - bohrs
C
C  v       --- energy of the system
C  x,y,z   --- one-dimensional arrays of the Cartesian coordinates
C                    of the system
C  dvdx,dvdy,dvdz   --- one-dimensional arrays of the gradients with respect
C                       to the Cartesian coordinates of the system (output)
C  natoms   --- number of atoms in the system
C  maxatom --- maximal number of atoms
C
C  Note:  Derivatives were generated using ADIFOR, whose disclaimer follows.
C
C                           DISCLAIMER
C
C   This file was generated on 09/13/04 by the version of
C   ADIFOR compiled on June, 1998.
C
C   ADIFOR was prepared as an account of work sponsored by an
C   agency of the United States Government, Rice University, and
C   the University of Chicago.  NEITHER THE AUTHOR(S), THE UNITED
C   STATES GOVERNMENT NOR ANY AGENCY THEREOF, NOR RICE UNIVERSITY,
C   NOR THE UNIVERSITY OF CHICAGO, INCLUDING ANY OF THEIR EMPLOYEES
C   OR OFFICERS, MAKES ANY WARRANTY, EXPRESS OR IMPLIED, OR ASSUMES
C   ANY LEGAL LIABILITY OR RESPONSIBILITY FOR THE ACCURACY, COMPLETE-
C   NESS, OR USEFULNESS OF ANY INFORMATION OR PROCESS DISCLOSED, OR
C   REPRESENTS THAT ITS USE WOULD NOT INFRINGE PRIVATELY OWNED RIGHTS.
C
        implicit double precision(a-h, o-z)
        parameter(autoang=0.5291772108d0)
        parameter(autoev=27.2113845d0)
        dimension x(maxatom),y(maxatom),z(maxatom)
        double precision gxs(maxatom),gys(maxatom),gzs(maxatom),
     &  gxgcorr(maxatom,maxatom),gygcorr(maxatom,maxatom),
     &  gzgcorr(maxatom,maxatom),gcorr(maxatom)
        double precision dvdy(maxatom),dvdz(maxatom),dvdx(maxatom)
        double precision dxij(maxatom,maxatom),dyij(maxatom,maxatom),
     &  dzij(maxatom,maxatom),rij(maxatom,maxatom)

c Nanoparticle parameters (Ref. 2)
c       two-body interaction
        de = 1.71013678553981441 / autoev
        re = 5.08182706399609163
        a1 = 1.24074255007327805
        a2 = 0.551880801172447422
        a3 = 0.129970688812896917
        au2 = 0.143243771372740580
        bu2 = 6.50000000000000000 / autoang
c       screening
        xk1= 4.24002677622442103
        xk2= 0.117656503960960862
        xk3= 4.78063179546451522
        au23= 1.63973192904916298
c       coordination number
        g1= 0.708483373073205747
        du2= 1.13286279334603357
        gu2= 0.663930057862113232
        gzero= 8.54498572971970027
        g2= 5.39584023677170066/autoang
c       effective two-body
        deb= 1.42526928794948882/autoev
        reb= 4.87735706664722812
        a1b= 1.20666644170640880
        a2b= 0.728296669115275908
        a3b= 0.215461507389864804
        au2b= 0.138211749991007299
C        bu2b= 6.50000000000000000/autoang

c Initialize arrays
        v = 0.d0
        do 99998 i = 1, natoms
        dvdx(i) = 0.0d0
        dvdy(i) = 0.0d0
        dvdz(i) = 0.0d0
C store dxij,dyij,dzij,rij(i,j) for the later usage
          do j=i+1,natoms
            dxij(i,j)=x(i)-x(j)
            dxij(i,j)=dxij(i,j)-cell(1)*nint(dxij(i,j)/cell(1))
            dyij(i,j)=y(i)-y(j)
            dyij(i,j)=dyij(i,j)-cell(2)*nint(dyij(i,j)/cell(2))
            dzij(i,j)=z(i)-z(j)
            dzij(i,j)=dzij(i,j)-cell(3)*nint(dzij(i,j)/cell(3))
            rij(i,j)=dsqrt(dxij(i,j)**2+dyij(i,j)**2+dzij(i,j)**2)
            rij(j,i) = rij(i,j)
            dxij(j,i)=-dxij(i,j)
            dyij(j,i)=-dyij(i,j)
            dzij(j,i)=-dzij(i,j)
          enddo
99998   continue

C do S term, needs xk1,xk2,xk3
        do 510 i = 1, natoms
          do 515 j = i + 1, natoms
            if (rij(i,j) .ge. bu2) goto 515
            do k = 1, natoms
            gxs(k) = 0.0d0
            gys(k) = 0.0d0
            gzs(k) = 0.0d0
            enddo
            s = 0.d0
            do 512 l = 1, natoms
              if (l .eq. j .or. l .eq. i) goto 512
              if (rij(j,l) .ge. bu2) goto 512
              if (rij(i,l) .ge. bu2) goto 512

              rrr = (rij(i,l) + rij(j,l)) / rij(i,j)
              d3_v = 1.d0 - rij(j,l) / bu2
              d6_b1 = -au23 / (d3_v**2 * bu2)
              ac = au23 * (1.d0 - 1.d0/d3_v)
              d4_v = 1.d0 - rij(i,j) / bu2
              d5_v = 1.d0 / d4_v
              d8_b1 = (-((-au23) * ((-d5_v) / d4_v))) * (1.0d0 / bu2)
              ac = ac + au23 * (1.d0 - d5_v)
              d4_v = 1.d0 - rij(i,l) / bu2
              d5_v = 1.d0 / d4_v
              d8_b2 = (-((-au23) * ((-d5_v) / d4_v))) * (1.0d0 / bu2)
              ac = ac + au23 * (1.d0 - d5_v)
              term = xk1 * dexp(-xk2 * rrr ** xk3 + ac)
              d5_b = -xk2 * xk3 * rrr ** ( xk3 - 1.0d0)
C              d6_b = d5_b * (1.0d0 / rij(i,j)) * term
C              d7_b = -d5_b * (rrr / rij(i,j)) * term
              d6_b = d5_b * term / rij(i,j)
              d7_b = -d6_b * rrr

              td8_b1=(term*d8_b1+d7_b)/rij(i,j)
              td8_b2=(term*d8_b2+d6_b)/rij(i,l)
              td6_b1=(term*d6_b1+d6_b)/rij(j,l)
              gxs(i) = gxs(i) + td8_b1*dxij(i,j)
              gxs(i) = gxs(i) + td8_b2*dxij(i,l)
              gxs(j) = gxs(j) + td6_b1*dxij(j,l)
              gxs(j) = gxs(j) - td8_b1*dxij(i,j)
              gxs(l) = gxs(l) - td8_b2*dxij(i,l)
              gxs(l) = gxs(l) - td6_b1*dxij(j,l)

              gys(i) = gys(i) + td8_b1*dyij(i,j)
              gys(i) = gys(i) + td8_b2*dyij(i,l)
              gys(j) = gys(j) + td6_b1*dyij(j,l)
              gys(j) = gys(j) - td8_b1*dyij(i,j)
              gys(l) = gys(l) - td8_b2*dyij(i,l)
              gys(l) = gys(l) - td6_b1*dyij(j,l)

              gzs(i) = gzs(i) + td8_b1*dzij(i,j)
              gzs(i) = gzs(i) + td8_b2*dzij(i,l)
              gzs(j) = gzs(j) + td6_b1*dzij(j,l)
              gzs(j) = gzs(j) - td8_b1*dzij(i,j)
              gzs(l) = gzs(l) - td8_b2*dzij(i,l)
              gzs(l) = gzs(l) - td6_b1*dzij(j,l)

              s = s + term

512           continue

            d2_v = tanh (s)
            d1_p = 1.0d0 - ( d2_v *  d2_v)
            fs = d2_v

c effective two-body
            rr = rij(i,j)
            rhob = rr-reb
            polyb=1.d0+a1b*rhob+a2b*rhob**2+a3b*rhob**3
            eeb=dexp(-a1b*rhob)
            cob=0.d0
            if (rr.le.bu2) cob=dexp(au2b*(1.d0-1.d0/(1.d0-rr/bu2)))
            tmpb=deb*eeb*polyb*cob
            dcobdrr=0.d0
            if (rr.le.bu2) dcobdrr=(-cob/bu2)*au2b/(1.d0-rr/bu2)**2
            dpolybdrr=a1b+2.d0*a2b*rhob+3.d0*a3b*rhob**2
            deebdrr=-a1b*eeb
            dtmpbdrr=deb*(eeb*polyb*dcobdrr+eeb*dpolybdrr*cob
     &         +deebdrr*polyb*cob)

            tmp1=dtmpbdrr/rr*fs
            dvdx(i) = dvdx(i) + tmp1*dxij(i,j)
            dvdx(j) = dvdx(j) - tmp1*dxij(i,j)
            dvdy(i) = dvdy(i) + tmp1*dyij(i,j)
            dvdy(j) = dvdy(j) - tmp1*dyij(i,j)
            dvdz(i) = dvdz(i) + tmp1*dzij(i,j)
            dvdz(j) = dvdz(j) - tmp1*dzij(i,j)

            tmp1=d1_p*tmpb
            do k = 1, natoms
            dvdx(k) = dvdx(k) + tmp1*gxs(k)
            dvdy(k) = dvdy(k) + tmp1*gys(k)
            dvdz(k) = dvdz(k) + tmp1*gzs(k)
            enddo

            v = v + tmpb * d2_v
515       continue
510     continue

C CALCULATE CN FUNCTION
        g1e = dexp(g1)
        do 20 i = 1, natoms
          do j = 1, natoms
            gxgcorr(j,i) = 0.0d0
            gygcorr(j,i) = 0.0d0
            gzgcorr(j,i) = 0.0d0
          enddo
          gcorr(i) = 0.d0
          do 21 j = 1, natoms
            if (i .eq. j) goto 21
            d2_v = rij(i,j)
            if (rij(i,j) .ge. g2) goto 21
            d3_v = 1.d0 - rij(i,j) / g2
            d4_v = (-g1) / d3_v
            d4_b = (-((-d4_v) / d3_v)) * (1.0d0 / g2)
            d1_w = d4_v
            d2_v = dexp(d1_w)
            d1_p =  d2_v
            dum = g1e*d1_p*d4_b/rij(i,j)
            term = d2_v
            gxgcorr(i,i) =  dum*dxij(i,j) + gxgcorr(i,i)
            gxgcorr(j,i) = -dum*dxij(i,j) + gxgcorr(j,i)
            gygcorr(i,i) =  dum*dyij(i,j) + gygcorr(i,i)
            gygcorr(j,i) = -dum*dyij(i,j) + gygcorr(j,i)
            gzgcorr(i,i) =  dum*dzij(i,j) + gzgcorr(i,i)
            gzgcorr(j,i) = -dum*dzij(i,j) + gzgcorr(j,i)
            gcorr(i) = gcorr(i) + term
21          continue
          gcorr(i) = gcorr(i) * g1e
20      continue

C     then calculate CN term
        do 30 i = 1, natoms
          do 31 j = 1, natoms
            if (i.eq.j) goto 31
            d2_v = rij(i,j)
            if (rij(i,j).ge.bu2) goto 31

            term = 0.d0
            gdum = 0.d0
            if (rij(i,j).ge.g2) goto 32
            d3_v = 1.d0-rij(i,j)/g2
            d4_v = 1.d0/d3_v
            d6_b = -g1*d4_v/(d3_v*g2)
            d1_w = g1 * (1.d0 - d4_v)
            d2_v = dexp(d1_w)
            d1_p =  d2_v
            gdum = d1_p * d6_b /rij(i,j)
            term = d2_v
32          continue

            cz1 = (gcorr(i) - term) / gzero
            if (cz1.gt.1.d-20) then
               d2_v = cz1 ** ( gu2 - 2.0d0)
               d2_v =  d2_v * cz1
               d1_p =  gu2 *  d2_v
               d2_v =  d2_v * cz1
            else
               cz1 = 0.d0
               d2_v = 0.d0
               d1_p = 0.d0
            endif
            d3_v = 1.d0 + d2_v
            d4_ba = -d1_p / d3_v**2
            es1 = 1.d0 / d3_v

            cz2 = (gcorr(j) - term) / gzero
            if (cz2.gt.1.d-20) then
               d2_v = cz2 ** ( gu2 - 2.0d0)
               d2_v =  d2_v * cz2
               d1_p =  gu2 *  d2_v
               d2_v =  d2_v * cz2
            else
               cz2 = 0.d0
               d2_v = 0.d0
               d1_p = 0.d0
            endif
            d3_v = 1.d0 + d2_v
            d4_v = 1.d0 / d3_v
            d4_bb = (-d4_v) / d3_v * d1_p
            ec1 = d4_v

            tt1 = du2 * ec1 * d4_ba
            tt2 = du2 * es1 * d4_bb

c modified two-body
              rr = rij(i,j)
              rhob = rr-reb
              polyb=1.d0+a1b*rhob+a2b*rhob**2+a3b*rhob**3
              eeb=dexp(-a1b*rhob)
              cob=0.d0
              if (rr.le.bu2) cob=dexp(au2b*(1.d0-1.d0/(1.d0-rr/bu2)))
              tmpb=0.5d0*deb*eeb*polyb*cob
              dcobdrr=0.d0
              if (rr.le.bu2) dcobdrr=(-cob/bu2)*au2b/(1.d0-rr/bu2)**2
              dpolybdrr=a1b+2.d0*a2b*rhob+3.d0*a3b*rhob**2
              deebdrr=-a1b*eeb
              dtmpbdrr=0.5d0*deb*(eeb*polyb*dcobdrr+eeb*dpolybdrr*cob
     &                +deebdrr*polyb*cob)

            tmp1=tmpb/gzero
            do k = 1, natoms
              dvdx(k) = dvdx(k) -
     &            tmp1*(tt2*gxgcorr(k,j)+tt1*gxgcorr(k,i))
              dvdy(k) = dvdy(k) -
     &            tmp1*(tt2*gygcorr(k,j)+tt1*gygcorr(k,i))
              dvdz(k) = dvdz(k) -
     &            tmp1*(tt2*gzgcorr(k,j)+tt1*gzgcorr(k,i))
            enddo
            tmp1=tmpb*(tt2+tt1)*gdum/gzero
              dvdx(i)=dvdx(i)+tmp1*dxij(i,j)
              dvdx(j)=dvdx(j)-tmp1*dxij(i,j)
              dvdy(i)=dvdy(i)+tmp1*dyij(i,j)
              dvdy(j)=dvdy(j)-tmp1*dyij(i,j)
              dvdz(i)=dvdz(i)+tmp1*dzij(i,j)
              dvdz(j)=dvdz(j)-tmp1*dzij(i,j)

            fcn = du2 * (es1 * ec1 - 1.d0)
            tmp1 = dtmpbdrr/rr*fcn
            dvdx(i) = dvdx(i) - tmp1*dxij(i,j)
            dvdx(j) = dvdx(j) + tmp1*dxij(i,j)
            dvdy(i) = dvdy(i) - tmp1*dyij(i,j)
            dvdy(j) = dvdy(j) + tmp1*dyij(i,j)
            dvdz(i) = dvdz(i) - tmp1*dzij(i,j)
            dvdz(j) = dvdz(j) + tmp1*dzij(i,j)

            v = v - tmpb * fcn

31          continue
30        continue

C pairwise term
        do 99989 i = 1, natoms
          do 99990 j = i + 1, natoms
            d2_v = rij(i,j)

            d1_p = 1.0d0 / (2.0d0 *  d2_v)
            g_rr = d1_p * 2.d0
            rr = d2_v
            if (rr .gt. bu2) goto 99990
            g_rho = g_rr
            rho = rr - re
            d4_v = rho * rho
            d2_p = 2.0d0 * rho
C            d7_v = rho ** ( 3 - 2)
C            d7_v =  d7_v * rho
C            d1_p =  3.d0 *  d7_v
C            d7_v =  d7_v * rho
            d1_p =  3.d0 *  d4_v
            d7_v =  d4_v * rho
            d5_b = a3 * d1_p + a2 * d2_p + a1
            g_poly = d5_b * g_rho
            poly = 1.d0 + a1 * rho + a2 * d4_v + a3 * d7_v
            d1_w = (-a1) * rho
            d2_v = dexp(d1_w)
            d1_p =  d2_v
            g_ee = d1_p * (-a1) * g_rho
            ee = d2_v
            g_co = 0.0d0
            co = 0.d0
            if (rr .le. bu2) then
              d3_v = 1.d0 - rr / bu2
              d4_v = 1.d0 / d3_v
              d6_b = (-((-au2) * ((-d4_v) / d3_v))) * (1.0d0 / bu2)
              d1_w = au2 * (1.d0 - d4_v)
              d2_v = dexp(d1_w)
              d1_p =  d2_v
              g_co = d1_p * d6_b * g_rr
              co = d2_v
            endif
            d2_v = de * ee
            d4_v = d2_v * poly
            d5_b = co * d2_v
            d6_b = co * poly * de
            g_tmp = d4_v * g_co + d5_b * g_poly + d6_b * g_ee
            tmp = d4_v * co
            dum =  - g_tmp
            dvdx(i) = dvdx(i) + dum * dxij(i,j)
            dvdx(j) = dvdx(j) - dum * dxij(i,j)
            dvdy(i) = dvdy(i) + dum * dyij(i,j)
            dvdy(j) = dvdy(j) - dum * dyij(i,j)
            dvdz(i) = dvdz(i) + dum * dzij(i,j)
            dvdz(j) = dvdz(j) - dum * dzij(i,j)
            v = v - tmp
99990     continue
99989   continue
        return
      end
      subroutine prepot
      return
      end
