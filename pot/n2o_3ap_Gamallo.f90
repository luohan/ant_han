!***********************************************************************
!   System:                     N2O (lowest 3A' state)
!   Functional form:            six-order polynomials
!   Common name:                N2O-3Ap (adiabatic triplet ground state)
!   Number of derivatives:      1
!   Number of bodies:           3
!   Number of electronic surfaces: 1
!   Interface: Section-2
!
!   References: P.Gamallo, Miguel Gonzalez, and R. Sayos
!               doi:10.1063/1.1586251
!
!   Note: HE-MM-1 format
!
!    1 : O - N
!    2 : N - N'
!    3 : N'- O
!   Aysymptotic energy: N+NO:  - De(1)
!                       O+N2:  - De(2)
!                       N+N+O:   0eV
!   Input: X(3),Y(3),Z(3)               in units of bohr
!   Output: E                           in units of hartree
!   Output: dEdX(3),dEdY(3),dEdZ(3)     hartree/bohr
!***********************************************************************
      subroutine pot(symb,x,y,z,e,dEdX,dEdY,dEdZ,nat,mnat)
      implicit none
      integer,parameter :: dp=8
      integer :: nat,mnat
      character(2) :: symb(mnat)
      real(dp) :: x(nat), y(nat),z(nat),e,dEdX(nat),dEdY(nat),dEdZ(nat)
      real(dp) :: xpes(3,3),dEdXpes(3,3)
      real(dp) :: temp(3)
      integer :: i,io
      io=1

      xpes(:,1) = x
      xpes(:,2) = y
      xpes(:,3) = z


      do i=1,nat
        if (symb(i) .eq. 'o') then
          if (i .ne.1) then
            ! make sure first one is always oxygen
            io = i
            temp = xpes(1,:)
            xpes(1,:) = xpes(io,:)
            xpes(io,:) = temp
            exit
          endif
        endif
      enddo

      call n2opes(xpes,e,dEdXpes)

      if (io .ne. 1 ) then
        temp = dEdXpes(io,:)
        dEdXpes(io,:) = dEdXpes(1,:)
        dEdXpes(1,:) = temp
      endif
      dEdX = dEdXpes(:,1)
      dEdY = dEdXpes(:,2)
      dEdZ = dEdXpes(:,3)

      end subroutine pot

! =============================================================================
!                      N2O PES
! ============================================================================
      subroutine n2opes(X,E,dEdX)

      implicit none

      integer,parameter :: dp=8
      ! input and ioutput
      ! first index is for different atom
      ! second index is for x,y,z
      ! oxygen should always be the first atom
      real(dp) :: X(3,3)
      real(dp) :: E,dEdX(3,3)

      ! constants
      ! real(dp),parameter :: kcal2ev=0.0433641D0
      real(dp),parameter :: ev2hart=0.03674932248D0
      real(dp),parameter :: kcal2hart=1.593601438d-3
      real(dp),parameter :: bohr2a=0.52917721067D0
      real(dp),parameter :: irt2 =1.0D0/dsqrt(2.0D0)
      ! two body constants NO, N2, NO
      integer,parameter :: MolType(3) =  (/1,2,1/)
      real(dp),parameter :: De(2)=(/152.53D0,228.41D0/)*kcal2hart
      real(dp),parameter :: Re(2)=(/1.1508D0,1.0977D0/)
      real(dp),parameter :: a(5,2)=reshape((/6.1958D0,11.1408D0,9.2570D0,&
                               6.9823D0,19.6021D0,3.7790D0,-0.2694D0,&
                              -0.6111D0,-1.9853D0,0.8992D0/),(/5,2/))

      ! three body constants
      real(dp),parameter :: Rtri(2)=(/1.4789D0,1.9680D0/)
      real(dp),parameter :: c(50) =  (/ 4.6391D0,  -0.4166D0,   6.7267D0,&
                            4.7954D0,   4.9946D0,   4.7293D0,   1.0226D0,&
                           -2.2228D0,  -4.4265D0,  -9.4334D0, -12.3015D0,&
                          -16.2801D0,  -7.0602D0,   3.5126D0,   4.8414D0,&
                           13.4089D0,  19.8618D0,   8.6305D0,  -3.2647D0,&
                          -11.3859D0, -11.8867D0,  -4.3822D0,  -0.3493D0,&
                            4.6653D0,   2.1739D0,  -0.0326D0,  -1.8714D0,&
                           -0.1853D0,   3.1430D0,   6.0499D0, -18.0314D0,&
                          -15.1099D0,   0.3747D0,  -3.9775D0,  19.3841D0,&
                           44.9758D0,   6.8153D0, -13.3716D0, -43.1208D0,&
                          -26.2565D0,  19.0150D0,  19.3009D0,  -2.1531D0,&
                           -7.5467D0,  -9.8957D0,   8.0923D0,  20.2721D0,&
                            4.2956D0, -11.7849D0,  -0.1736D0/)*ev2hart
      integer,parameter :: k_stride(4) = (/1,28,15,6/)
      integer :: imax,jmax,kmax,ic2(2)
      real(dp),parameter :: gam(2) = (/2.3806D0, 2.1632D0/)

      ! variables used in the function
      integer :: i,j,k,ic
      real(dp) :: R(3),dX(3,3),dRdX(3,3),dRdY(3,3),dRdZ(3,3)
      real(dp) :: rho(3),V2(3),rhotri(3),dV2dR(3)
      real(dp) :: spower(3,0:6),s(3),invs(3),dSdR(3),pfac(3),P,T,fac,dPdR(3)
      real(dp) :: tfac(2), dTdS(2),dTdR(3),V3,dV3dR(3),dVdR(3)
      real(dp) :: temp,temp1

      dRdX = 0.0d0
      dRdY = 0.0d0
      dRdZ = 0.0d0
      dX = 0.0d0

      do i = 1,3 !loop through O-N, N-N', N'-O bond
        j = i+1
        if (j > 3) j=1

        dX(i,:) = X(i,:) - X(j,:)
        R(i) = dsqrt(dX(i,1)*dX(i,1) + dX(i,2)*dX(i,2) + dX(i,3)*dX(i,3))

        dRdX(i,i) =  dX(i,1)/R(i)
        dRdX(i,j) = -dRdX(i,i)

        dRdY(i,i) =  dX(i,2)/R(i)
        dRdY(i,j) = -dRdY(i,i)

        dRdZ(i,i) =  dX(i,3)/R(i)
        dRdZ(i,j) = -dRdZ(i,i)
      enddo
      dX = dX*bohr2a
      R = R*bohr2a


      dV2dR = 0.0d0
      !-------------- Two-body interaction term
      do i = 1,3
        j = MolType(i)
        rho(i) = R(i) - Re(j)
        rhotri(i) = R(i) - Rtri(j)

        V2(i) = -De(j)*(1.0D0 + rho(i)*(a(1,j)+rho(i)*(a(2,j)+rho(i)*(a(3,j)+rho(i)*(a(4,j)+rho(i)*a(5,j))))))
        V2(i) = V2(i)*dexp(-a(1,j)*rho(i))

        dV2dR(i) = -De(j)*(a(1,j)+rho(i)*(2.0D0*a(2,j)+rho(i)*(3.0D0*a(3,j)+rho(i)*(4.0D0*a(4,j)+rho(i)*5.0D0*a(5,j)))))
        dV2dR(i) = dV2dR(i)*dexp(-a(1,j)*rho(i))
        dV2dR(i) = dV2dR(i) + V2(i)*(-a(1,j))
      enddo

      !--------------  Three-body interaction term
      ! precompute power
      s(1) = rhotri(2)
      s(2) = irt2*(rhotri(1)+rhotri(3))
      s(3) = irt2*(rhotri(1)-rhotri(3))
      invs(1) = 1.0D0/s(1)
      invs(2) = irt2/s(2)
      invs(3) = irt2/s(3)

      spower(:,0) = 1.0d0
      spower(:,1) = s
      do i = 2,6
        spower(:,i) = spower(:,i-1)*s
      enddo

      P = 0.0D0
      dPdR = 0.0D0
      dSdR = 0.0d0


      if (s(1)*s(2)*s(3) .ne. 0.0d0)then
         ic = 0
         do k = 0,6,2
            pfac(3) = invs(3)*dble(k)
            do j = 0,6-k,1
               pfac(2) = invs(2)*dble(j)
               dSdR(1) = pfac(2) + pfac(3)
               dSdR(3) = pfac(2) - pfac(3)
               temp = spower(2,j)*spower(3,k)
               do i = 0,(6-k-j)
                  dSdR(2) = invs(1)*dble(i)

                  ic = ic + 1
                  fac = c(ic)*spower(1,i)*temp
                  P = P + fac
                  dPdR = dPdR + fac*dSdR
               enddo
            enddo
         enddo
      else
         ! the following condition rarely happens
         if (s(3) .eq. 0.0d0) then
            kmax = 0
         else
            kmax = 6
         endif

         ic2(1) = 0
         do k = 0, kmax, 2
            if (s(2) .eq. 0.0d0) then
               jmax = 1
            else
               jmax = 6-k
            endif
            if ( k .eq. 6) jmax = 0
            ic2(1) = ic2(1)+k_stride(k/2+1) !index of 0,0,k
            if (k.eq. 0) then
               temp = 0.0d0
            else
               temp = irt2*dble(k)*spower(3,k-1)
            endif

            do j = 0,jmax
               if (s(1) .eq. 0.0d0) then
                  imax = 1
               else
                  imax = 6-k-j
               endif
               if ((k+j) .eq. 6) imax = 0
               ic2(2) = ic2(1) + int(dble(j)*(7.0d0-dble(k)-(dble(j)-1.0d0)/2.0d0)) !index of 0,j,k

               temp1 = spower(2,j)*spower(3,k)
               if ( j .eq. 0) then
                  pfac(2) = 0.0d0
               else
                  pfac(2) = irt2*dble(j)*spower(2,j-1)*spower(3,k)
               endif
               pfac(3) = temp*spower(2,j)

               do i = 0,imax
                  ic = ic2(2) + i
                  P = P + c(ic)*spower(1,i)*temp1
                  if ( i .eq. 0)then
                     dSdR(2) = 0.0d0
                  else
                     dSdR(2) = dble(i)*spower(1,i-1)*temp1
                  endif
                  !write(*,'("indx=",6I3,F8.3)') i,j,k,ic,ic2(1),ic2(2),c(ic)
                  dSdR(1) = (pfac(2)+pfac(3))*spower(1,i)
                  dSdR(3) = (pfac(2)-pfac(3))*spower(1,i)
                  dPdR = dPdR + dSdR*c(ic)
               enddo
            enddo
         enddo
      endif


      pfac(1:2) = gam*s(1:2)/2.0D0
      tfac = 1.0D0 - dtanh( pfac(1:2))
      T = tfac(1)*tfac(2)
      dTdS = 1.0D0/dcosh(pfac(1:2))
      dTdS = - dTdS * dTdS * gam/2.0D0  !dT/dS

      dTdR(2) = dTdS(1)*tfac(2)
      dTdR(1) = tfac(1)*dTdS(2)*irt2
      dTdR(3) = dTdR(1)

      V3 = P*T
      dV3dR = dPdR*T + P*dTdR

      ! ---------- summation
      E = V2(1)+V2(2)+V2(3)+V3

      dVdR = dV2dR + dV3dR
      dEdX(:,1) = dVdR(1)*dRdX(1,:) + dVdR(2)*dRdX(2,:) + &
                  dVdR(3)*dRdX(3,:)
      dEdX(:,2) = dVdR(1)*dRdY(1,:) + dVdR(2)*dRdY(2,:) + &
                  dVdR(3)*dRdY(3,:)
      dEdX(:,3) = dVdR(1)*dRdZ(1,:) + dVdR(2)*dRdZ(2,:) + &
                  dVdR(3)*dRdZ(3,:)

      dEdX = dEdX*bohr2a
      end subroutine n2opes

!==================================================================
!                   N2O two body interaction
!==================================================================
      subroutine diapot_int(r,im,nsurf,v)
      ! two body interaction
      ! im = 1,3 NO bond
      ! im = 2   NN bond
      ! r in bohr, v in hartree
      implicit none
      integer,parameter :: dp = 8
      ! constans
      ! real(dp),parameter :: kcal2ev=0.0433641D0
      real(dp),parameter :: ev2hart=0.03674932248D0
      real(dp),parameter :: kcal2hart=1.593601438d-3
      real(dp),parameter :: bohr2a=0.52917721067D0
      real(dp),parameter :: De(2)=(/152.53D0,228.41D0/)*kcal2hart
      real(dp),parameter :: Re(2)=(/1.1508D0,1.0977D0/)
      real(dp),parameter :: a(5,2)=reshape((/6.1958D0,11.1408D0,9.2570D0,&
                               6.9823D0,19.6021D0,3.7790D0,-0.2694D0,&
                              -0.6111D0,-1.9853D0,0.8992D0/),(/5,2/))
      ! contants
      real(dp) :: r(3), rA ,v, rho
      integer :: im,nsurf,j

      if (im .le. 3)then
        if (im .eq. 2)then
          j = 2
        else
          j = 1
        endif

        rA = r(im)*bohr2a

        rho = rA - Re(j)
        v = -De(j)*(1.0D0 + rho*(a(1,j)+rho*(a(2,j)+rho*(a(3,j)+rho*(a(4,j)+rho*a(5,j))))))
        v = v*dexp(-a(1,j)*rho)
      endif
      end subroutine diapot_int



      subroutine prepot
      return
      end
