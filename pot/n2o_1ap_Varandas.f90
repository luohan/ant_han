!***********************************************************************
!   System:                     N2O (1A' state)
!   Functional form:            DMBE
!   Common name:                N2O-1Ap
!   Number of derivatives:      1
!   Number of bodies:           3
!   Number of electronic surfaces: 1
!   Interface: Section-2
!
!   References: Jing Li and António J. C. Varandas
!               doi:10.1021/jp302173h
!
!   Note: HE-MM-1 format
!
!    1 : O - N
!    2 : N - N'
!    3 : N'- O
!
!   Input: X(3),Y(3),Z(3)               in units of bohr
!   Output: E                           in units of hartree
!   Output: dEdX(3),dEdY(3),dEdZ(3)     hartree/bohr
!***********************************************************************
      subroutine pot(symb,x,y,z,e,dEdX,dEdY,dEdZ,nat,mnat)
      implicit none
      integer,parameter :: dp=8
      integer :: nat,mnat
      character(2) :: symb(mnat)
      real(dp) :: x(nat), y(nat),z(nat),e,dEdX(nat),dEdY(nat),dEdZ(nat)
      real(dp) :: xpes(3,3),dEdXpes(3,3)
      real(dp) :: temp(3)
      integer :: i,io
      io=1

      xpes(:,1) = x
      xpes(:,2) = y
      xpes(:,3) = z


      do i=1,nat
        if (symb(i) .eq. 'o') then
          if (i .ne.1) then
            ! make sure first one is always oxygen
            io = i
            temp = xpes(1,:)
            xpes(1,:) = xpes(io,:)
            xpes(io,:) = temp
            exit
          endif
        endif
      enddo

      call n2opes_onep(xpes,e,dEdXpes)

      if (io .ne. 1 ) then
        temp = dEdXpes(io,:)
        dEdXpes(io,:) = dEdXpes(1,:)
        dEdXpes(1,:) = temp
      endif
      dEdX = dEdXpes(:,1)
      dEdY = dEdXpes(:,2)
      dEdZ = dEdXpes(:,3)

      end subroutine pot

! =============================================================================
!              N2O PES for 1A' State O(1D)+N2(X) or N(2D)+NO(X)
!                   Distance in Bohr, Energy in Hartree
! ============================================================================
      subroutine n2opes_onep(X,E,dEdX)

      implicit none
      integer,parameter :: dp=8
      ! input and ioutput
      ! first index is for different atom
      ! second index is for x,y,z
      ! Xin : O, N,  N'
      ! X   : N, N', O
      real(dp),intent(in) :: X(3,3)
      real(dp),intent(out) :: dEdX(3,3),E


      ! constants
      real(dp),parameter :: kcal2ev=0.0433634D0, ev2hart=0.0367502D0
      real(dp),parameter :: bohr2a=0.52917721067D0
      real(dp),parameter :: irt2 =1.0D0/dsqrt(2.0D0)

      integer,parameter :: MolAtom(2,3) = reshape((/2,3,3,1,1,2/),(/2,3/)) !atom id of each molecule
      integer,parameter :: MolType(3) = (/1,2,2/) !NaNb,NbO, ONa
      integer,parameter :: Amass(3) = (/16.0d0,14.0d0,14.0d0/) !O, Na, Nb
      ![1 2] correspond to [O(1D),N(2D)] or [NN and NO]

      ! ----------- one body constants
      real(dp),parameter :: V10(2) = (/0.0723399d0,0.0898679d0/) !O(1D),N(2D)
      real(dp),parameter :: Oalpha(2,2) = reshape((/0.632144d0,0.685171d0,&
                                         0.627059d0, 0.355785d0/),(/2,2/))
      real(dp),parameter :: Obeta(2,2) = reshape((/1.59035d0, 0.268852d0, &
                                         0.521073d0, 0.278026d0/),(/2,2/))
      real(dp),parameter :: OR0(4,2) = reshape((/1.65274d0,3.98608d0,2.8661d0,4.74015d0,&
                                  2.63604d0,3.78884d0,0.70132d0,4.74172d0/),(/4,2/))
      real(dp),parameter :: Oalpha2(2)=(/0.75d0,0.75d0/), Osr0(2)=(/5.5d0,5.5d0/)

      real(dp) :: V1,dV1dR(3),g(3),h(3),dhdR(3),dgdRj(3)


      ! ---------- two body constants NO, NN
      real(dp),parameter :: De(2)=(/0.705383d0,0.482145d0/)
      real(dp),parameter :: Re(2)=(/2.090857d0,2.190801d0/)
      real(dp),parameter :: a(5,2) = reshape((/ 1.394960d0,-1.028060d0, &
                                    0.627289d0,-0.237656d0, 0.041869d0, &
                                    1.461190d0,-0.929602d0, 0.840211d0, &
                                   -0.516115d0, 0.120291d0/),(/5,2/))
      real(dp),parameter :: Dgam0(3,2) = reshape((/ 0.859590d0, 1.728100d0, 0.145460d0,&
                                          0.922289d0, 1.773940d0, 0.225366d0/),(/3,2/))

      !real(dp),parameter :: DR0(2)=(/6.5780d0,6.1160d0/)
      real(dp),parameter :: Drho(2) = (/13.7225d0,13.1450d0/) !Drho=5.5+1.25*DRO
      real(dp),parameter :: DC(3,2) = reshape((/   24.10d0,  438.4155d0,10447.8275d0,&
                                         18.78d0,  305.3882d0, 6505.4914d0/),(/3,2/))


      !real(dp),parameter :: Dchic(4) = (/16.36606,0.70172,17.19338,0.09574/)
      ! precompted An and Bn,An = Dchi(1)*n^(-Dchi(2)), Bn=Dchi(3)*exp(-Dchi(4)*n)
      ! Dchi([An,Bn],[6,8,10])
      real(dp),parameter :: Dchi(2,3) = reshape((/4.654787330755237d0,9.680218041776636d0,&
                                                  3.803888981594235d0,7.993305876923704d0,&
                                                  3.252551179710757d0,6.600361537965583d0/),&
                                                   (/2,3/))

      real(dp) :: V2,dV2dX(3,3),V2EHF,V2DC,dV2dR(3),Dgam

      ! ---------------------------- three body constants ------------------------------------
      real(dp),parameter :: TRm(6,2) = reshape((/3.5420d0,3.4783d0,3.5483d0,3.4879d0,3.4765d0,&
                     3.5522d0,3.4000d0,3.3437d0,3.2932d0,3.3017d0,3.3074d0,4.2247d0/),(/6,2/))
      ! original parameters in varands paper, but it's wrong!
      !real(dp),parameter :: TDm(6,2) = reshape((/44.3630d0,5.7508d0,1684.5394d0,805.7889d0,&
                         !63.0d0,83794.8807d0,53.0984d0,7.4737d0,1827.0389d0,1176.5849d0,&
                         !105.1093d0,192939.9868d0/),(/6,2/))
      real(dp),parameter :: TDm(6,2) = reshape((/ 6.803d0,5.7508d0,1684.5394d0,805.7889d0,&
                         !63.0d0,83794.8807d0,53.0984d0,7.4737d0,1827.0389d0,1176.5849d0,&
                         !105.1093d0,192939.9868d0/),(/6,2/))
      real(dp),parameter :: Ta(3,6,2) = reshape((/ 1.194189980d0, 0.204898558d0, 0.00074840d0,&
                                  -0.872172953d0, 0.238686556d0, 0.008644480d0, 0.531898763d0,&
                                  -0.000406100d0,-0.022335300d0, 1.453654740d0, 0.784568952d0,&
                                   0.147548894d0, 1.598689500d0, 0.702606311d0, 0.129022906d0,&
                                   1.237515240d0, 0.228397612d0, 0.001683300d0, 0.811851550d0,&
                                  -0.353046402d0,-0.205365383d0, 0.878344815d0, 0.233889325d0,&
                                   0.000987620d0, 0.764321651d0, 0.150853830d0,-0.013641400d0,&
                                   1.156669580d0, 0.526217890d0, 0.021529990d0, 0.970437546d0,&
                                   0.260198976d0,-0.001662800d0, 1.041099550d0, 0.283568770d0,&
                                   0.000104870d0/),(/3,6,2/))
      integer(dp),parameter :: Mab(3) = (/2,4,0/)
      real(dp),parameter :: Tb(2,6,2) = reshape((/ 0.435614640d0,5.8d-2,&
                          0.512820625d0,3.1d-9, 0.334567520d0,3.8d-9, 0.871286483d0,3.7d-9,&
                          0.512538430d0,2.6d-2, 0.491703448d0,7.3d-2, 0.359021660d0,5.5d-2,&
                          0.585315550d0,2.3d-9, 0.557005836d0,1.5d-8, 1.168845940d0,1.5d-8,&
                          0.085731230d0,8.5d-9, 0.161270910d0,9.5d-3/),(/2,6,2/))
      real(dp) :: V3DC,Tf(3),dTfdR(3,3),TCD(3,3),dTCDdR(3,3,3) !TCD(n,i)


      ! Variables used in the function
      integer :: i,j,k,l,n,m
      real(dp) :: R(3),dX(3,3),dX2(3),dRdX(3,3),dRdY(3,3),dRdZ(3,3),rho(5,3)
      real(dp) :: Rj(3),CTj(3),dRjdR(3,3),dCTjdR(3,3),dVdR(3)


      real(dp) :: temp(6),temp1,temp2,temp3,temp4

      ! X order : O Na Nb

      ! ----------Calculate some distance ---------------------
      ! configuration: O-Na=Nb, Na-Nb=O, Nb-O=Na
      ! R is the distance of bond Na=Nb, Nb=O, O=Na
      dRdX = 0.0d0
      dRdY = 0.0d0
      dRdZ = 0.0d0
      do i = 1,3
        j = MolAtom(1,i)
        k = MolAtom(2,i)  !configuration i-j=k

        !------molecular bond distance k->j
        dX(i,:) = X(j,:) - X(k,:)
        R(i) = dsqrt(dX(i,1)*dX(i,1) + dX(i,2)*dX(i,2) + dX(i,3)*dX(i,3))

        dRdX(i,j) =  dX(i,1)/R(i)
        dRdX(i,k) = -dRdX(i,j)

        dRdY(i,j) =  dX(i,2)/R(i)
        dRdY(i,k) = -dRdY(i,j)

        dRdZ(i,j) =  dX(i,3)/R(i)
        dRdZ(i,k) = -dRdZ(i,j)
      enddo

      do i = 1,3
         j = MolAtom(1,i)
         k = MolAtom(2,i)

         ! Approximate Jacobian distance
         Rj(i) = (Amass(j)*R(j) + Amass(k)*R(k))/(Amass(j)+Amass(k))
         dRjdR(i,i) =   0.0d0
         dRjdR(i,j) =   Amass(j)/(Amass(j)+Amass(k))
         dRjdR(i,k) =   Amass(k)/(Amass(j)+Amass(k))

         ! Jacobian angle, theta = angle(i, CM(jk), j)
         temp1 = - R(k)*R(k) + R(j)*R(j)
         temp2 = - (Amass(k)-Amass(j))/(Amass(k)+Amass(j))
         temp3 = 0.5d0*(temp1/R(i)+temp2*R(i))
         CTj(i) = temp3/Rj(i)
         dCTjdR(i,i) = 0.5d0*(-temp1/R(i)/R(i) + temp2)/Rj(i)
         dCTjdR(i,k) = - R(k)/R(i)/Rj(i) - temp3/Rj(i)/Rj(i)*dRjdR(i,k)
         dCTjdR(i,j) = + R(j)/R(i)/Rj(i) - temp3/Rj(i)/Rj(i)*dRjdR(i,j)
      enddo

      ! --------- one body interaction----------------
      ! Order: O-NN, N_b-ONa, Na-ONb
      dV1dR = 0.0d0
      V1 = 0.0d0
      do i = 1,3
         j = MolType(i)
         ! Calculate R_i-R_i^m first
         do k = 1,4
            rho(k,i) = R(i) - OR0(k,j)
         enddo

         !---------- calculate h
         temp(1) = Obeta(1,j)*rho(2,i)*rho(2,i)
         temp(2) = Obeta(2,j)*rho(4,i)*rho(4,i)

         temp(3) = Oalpha(1,j)*rho(1,i)+temp(1)*rho(2,i) !eq 11
         temp(4) = Oalpha(2,j)*rho(3,i)+temp(2)*rho(4,i)
         h(i) = 0.5d0-0.25d0*(dtanh(temp(3))+dtanh(temp(4)))

         temp(5) = 1.0d0/dcosh(temp(3))
         temp(6) = 1.0d0/dcosh(temp(4))
         dhdR(i) = -0.25d0*(temp(5)*temp(5)*(Oalpha(1,j)+3.0d0*temp(1)) +&
                            temp(6)*temp(6)*(Oalpha(2,j)+3.0d0*temp(2)))

         !--------- calculate g
         temp(1) = Oalpha2(j)*(Rj(i)-Osr0(j))
         g(i) = 0.5d0*(1.0d0+dtanh(temp(1)))
         dgdRj(i) = 0.5d0/dcosh(temp(1))/dcosh(temp(1))*Oalpha2(j)

         ! calculate V1 and dV1dX
         V1 = V1 + V10(j)*h(i)*g(i)
         dV1dR(i) = dV1dR(i) + V10(j)*(dhdR(i)*g(i) + h(i)*dgdRj(i)*dRjdR(i,i))
         k = MolAtom(1,i)
         dV1dR(k) = dV1dR(k) + V10(j)*(h(i)*dgdRj(i)*dRjdR(i,k))
         k = MolAtom(2,i)
         dV1dR(k) = dV1dR(k) + V10(j)*(h(i)*dgdRj(i)*dRjdR(i,k))
      enddo


      ! ------------------- two body interaction --------------------
      ! Order: NaNb, ONa, ONb
      dV2dR = 0.0d0
      V2EHF = 0.0d0
      V2DC = 0.0d0
      do i = 1,3
        j = MolType(i)
        rho(1,i) = R(i) - Re(j)

        ! --  EHF term
        temp(1) = a(4,j) + a(5,j)*rho(1,i)
        temp(1) = a(3,j) + temp(1)*rho(1,i)
        temp(1) = a(2,j) + temp(1)*rho(1,i)
        temp(1) = a(1,j) + temp(1)*rho(1,i)
        temp(1) = 1.0D0 + rho(1,i)*temp(1)

        temp(2) = 4.0d0*a(4,j) + 5.0d0*a(5,j)*rho(1,i)
        temp(2) = 3.0d0*a(3,j) + temp(2)*rho(1,i)
        temp(2) = 2.0d0*a(2,j) + temp(2)*rho(1,i)
        temp(2) =       a(1,j) + temp(2)*rho(1,i)

        Dgam = Dgam0(1,j)*(1.0d0 + Dgam0(2,j)*dtanh(Dgam0(3,j)*rho(1,i))) !gamma
        temp(3) = dexp(-Dgam*rho(1,i))
        temp1 = 1.0d0/dcosh(-Dgam*rho(1,i))
        temp(4) = temp(3)*(-Dgam-rho(1,i)*Dgam0(1,j)*Dgam0(2,j)*Dgam0(3,j)*temp1*temp1)

        temp1 = temp(1)*temp(3)
        V2EHF = V2EHF - De(j)/R(i)*temp1

        dV2dR(i) = De(j)/R(i)*(temp1/R(i) - temp(2)*temp(3) - temp(1)*temp(4))

        ! -- dispersion term
        temp1 = R(i)/Drho(j)
        do k = 1,3
           l = 4+2*k
           temp2 = dexp(temp1*(-Dchi(1,k)-temp1*Dchi(2,k)))
           temp(2) = (1.0d0-temp2)**(l-1)
           ! chi_l (l=6,8,10)
           temp(1) = temp(2)*(1.0d0-temp2)
           ! d chi_k / d R
           temp(2) = dble(l)*temp(2)*(-temp2)/Drho(j)*(-Dchi(1,k)-2.0d0*Dchi(2,k)*temp1)
           temp(3) = DC(k,j)*R(i)**(-l)
           V2DC = V2DC - temp(3)*temp(1)
           dV2dR = dV2dR + temp(3)*(dble(l)/R(i)*temp(1) - temp(2))
        enddo
      enddo
      V2 = V2EHF + V2DC

      !--------------------- three-body interaction ---------------------------------
      !------- precompute f_i(R) and C_n^i
      do i = 1,3
         j = MolAtom(1,i)
         k = MolAtom(2,i)
         temp1 = 6.0d0*R(i) - R(j) - R(k)
         Tf(i) = 0.5d0*(1.0d0 - dtanh(temp1))

         temp2 = 1.0d0/dcosh(temp1)
         temp2 = -0.5d0*temp2*temp2
         dTfdR(i,i) = temp2*6.0d0
         dTfdR(i,j) = -temp2
         dTfdR(i,k) = -temp2
      enddo

      TCD = 0.0d0
      dTCDdR = 0.0d0
      do i = 1,3
         j = MolType(i)
         m = 0
         do k = 1,3
            n = 4+2*k
            do l = 0,Mab(k),2
               m = m+1 !index for TRm, Tdm, Ta, Tb
               ! contant term in eq. 19 of  10.1007/s00214-006-0092-6
               !if (l.eq. 0) then
                  !if ( i .eq. 1)then
                     !temp1 = DC(k,2)+DC(k,2)
                  !else
                     !temp1 = DC(k,1)+DC(k,2)
                  !endif
               !else
                  !temp1 = 0.0d0
               !endif

               temp1 = 0.0d0
               rho(1,i) = R(i) - TRm(m,j)
               ! first bracket
               temp(1) = Ta(2,m,j) + Ta(3,m,j)*rho(1,i)
               temp(1) = Ta(1,m,j) +   temp(1)*rho(1,i)
               temp(1) = 1.0D0 + temp(1)*rho(1,i)
               temp(2) = 2.0d0*Ta(2,m,j) + 3.0d0*Ta(3,m,j)*rho(1,i)
               temp(2) = Ta(1,m,j) +temp(2)*rho(1,i)

               ! second bracket Ta(1) = Tb(1)
               temp(3) = Tb(1,m,j) + Tb(2,m,j)*rho(1,i)
               temp(3) = -rho(1,i)*(Ta(1,m,j)+temp(3)*rho(1,i))
               temp(4) = -Ta(1,m,j) - (2.0d0*Tb(1,m,j) + 3.0d0*Tb(2,m,j)*rho(1,i))*rho(1,i)

               ! C_n^L and derivative
               temp3 = dexp(temp(3))

               temp1 = temp1 + TDm(m,j)*temp(1)*temp3   !C_n^L
               temp2 = TDm(m,j)*temp3*(temp(2)+temp(4)) !dC_n^L/dR
               ! Legendre term
               if (l .eq. 0) then
                  temp3 = 1.0d0
                  temp4 = 0.0d0
               elseif (l.eq. 2) then
                  temp3 = 0.5d0*(3.0d0*CTj(i)**2-1.0d0)
                  temp4 = 3.0d0*CTj(i)
               else
                  temp3 = 0.125d0*(3.0d0-CTj(i)**2*(-30.0d0+35.0d0*CTj(i)**2))
                  temp4 = CTj(i)*(-7.5d0 + 17.5d0*CTj(i)**2)
               endif

               TCD(k,i) = TCD(k,i) + temp1*temp3

               if ( i .eq. 1)then
                  if ((n .eq. 6) .and. (l.eq.0)) dEdX(1,1) = temp1
                  if ((n .eq. 6) .and. (l.eq.2)) dEdX(2,1) = temp1
                  if ((n .eq. 8) .and. (l.eq.0)) dEdX(1,2) = temp1
                  if ((n .eq. 8) .and. (l.eq.2)) dEdX(2,2) = temp1
                  if ((n .eq. 8) .and. (l.eq.4)) dEdX(3,2) = temp1
                  if ((n .eq. 10) .and. (l.eq.0)) dEdX(1,3) = temp1
               endif

               dTCDdR(k,i,:) = dTCDdR(k,i,:) + temp2*temp3 + temp1*temp4*dCTjdR(i,:)
            enddo
         enddo
      enddo


! ---------- summation
      E = V2

      dVdR = dV2dR
      !dEdX(:,1) = dVdR(1)*dRdX(1,:) + dVdR(2)*dRdX(2,:) + &
                  !dVdR(3)*dRdX(3,:)
      !dEdX(:,2) = dVdR(1)*dRdY(1,:) + dVdR(2)*dRdY(2,:) + &
                  !dVdR(3)*dRdY(3,:)
      !dEdX(:,3) = dVdR(1)*dRdZ(1,:) + dVdR(2)*dRdZ(2,:) + &
                  !dVdR(3)*dRdZ(3,:)

      !dEdX(1,1) = TCD(
      end subroutine n2opes_onep

!==================================================================
!                   N2O two body interaction
!==================================================================
      subroutine diapot_int(r,im,nsurf,v)
      ! two body interaction
      ! im = 1,3 NO bond
      ! im = 2   NN bond
      ! r in bohr, v in hartree
      implicit none
      integer,parameter :: dp = 8
      ! constans
      real(dp),parameter :: kcal2ev=0.0433634D0,ev2hart=0.0367502D0
      real(dp),parameter :: bohr2a=0.52917721067D0
      real(dp),parameter :: De(2)=(/140.73D0,212.55D0/)*kcal2ev
      real(dp),parameter :: Re(2)=(/1.1598D0,1.1034D0/)
      real(dp),parameter :: a(5,2)=reshape((/4.14169D0, 0.85873D0,&
                                 -0.97298D0,-3.70379D0, 2.48770D0,&
                                  3.88769D0, 0.10039D0,-0.91232D0,&
                                 -2.73055D0, 1.69645D0/),(/5,2/))

     ! contants
      real(dp) :: r(3), rA ,v, rho
      integer :: im,nsurf,j

      if (im .le. 3)then
        if (im .eq. 2)then
          j = 2
        else
          j = 1
        endif

        rA = r(im)*bohr2a

        rho = rA - Re(j)

        v = a(4,j) + a(5,j)*rho
        v = a(3,j) + v*rho
        v = a(2,j) + v*rho
        v = a(1,j) + v*rho
        v = 1.0D0 + rho*v
        v = -De(j)*v*dexp(-a(1,j)*rho)
        v = v*ev2hart
      endif
      end subroutine diapot_int


      subroutine prepot
      return
      end
