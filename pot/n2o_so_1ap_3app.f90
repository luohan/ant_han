!***********************************************************************
!   System:                     N2O (3A'' and 1A')
!   Common name:                n2o_so_1ap_3app
!   Number of derivatives:      1
!   Number of bodies:           3
!   Number of electronic surfaces: 1
!   Interface: Section-2
!
!   References: H. Nakamura, S. Kato
!               doi:10.1063/1.480741
!
!   Note: This is the spin orbit coupling between 3A'' and 1A'
!         MEX point is calculated from Gamallo and Gonzalez's paper
!
!    1 : O - N
!    2 : N - N'
!    3 : N'- O
! 
!   Input: X(3),Y(3),Z(3)               in units of bohr
!   Output: E                           in units of hartree
!   Output: dEdX(3),dEdY(3),dEdZ(3)     hartree/bohr
!***********************************************************************
      subroutine n2o_spin_orbit(R,E,dRdR)
      implicit none
      real(dp),intent(in) :: R(3) ! in a.u.
      real(dp),intent(out) :: E,dEdR(3)

      ! constants
      real(dp),parameter :: bohr2a=0.52917721067D0
      real(dp),parameter :: autocmi=219474.6313705d0
      real(dp),parameter :: irt2=1.0D0/dsqrt(2.0D0),irt3=1.0/dsqrt(3.0D0)

      ! fiting parameters 
      ! MEX: collinear N-N-O, N-N=1.11A, CM-O=2.228A
      real(dp),parameter :: Rmex0(2)=(/1.112545228d0,2.228886191d0/)/bohr2a
      real(dp),parameter :: c(7)=(/1796.0976d0, -687.19611d0, -481.76737d0,&
                              75.714010d0,66.696532d0,148.0255d0,-53.891519d0/)
      real(dp),parameter :: alpha(2)=(/2.19d0,0.57d0/)
      real(dp),parameter :: dQdR(3,3) = reshape((/irt3,  irt2, -irt3*irt2,&
                                           irt3, 0.0d0,  1.0d0/irt2*irt3, &
                                           irt3, -irt2, -irt3*irt2/),(/3,3/)) 


      ! variables used in the function
      integer :: i
      real(dp) :: Q(3),S(2),temp1,temp2,dtdQ(3)

      !all in a.u.
      Q(1) = irt3*(R(1)+R(2)+R(3))
      Q(2) = irt2*(R(1)-R(3))
      Q(3) = irt3*(1.0d0/irt2*R(2) - irt2*(R(1)+R(3)))
      
      S(1) = Q(2)*Q(2) + Q(3)*Q(3)
      S(2) = Q(2)*Q(2) - Q(3)*Q(3)

      temp1 = c(1)+c(2)*Q(1)+c(3)*Q(3)+c(4)*Q(1)*Q(1)+c(5)*S(1)+&
              c(6)*Q(1)*Q(3)+c(7)*S(2)
      dtdQ(1) = c(2)+2.0d0*c(4)*Q(1)+c(6)*Q(3)
      dtdQ(2) = 2.0d0*(c(5)+c(7))*Q(2)
      dtdQ(3) = c(3)+2.0d0*(c(5)-c(7))*Q(3)+c(6)*Q(1)

      temp2 = dexp(-alpha(1)*(R(2)-Rmex0(1))-alpha(2)*((R(1)+R(3))/2.0d0-Rmex0(2))**2)

      E = temp1*temp2
      
      dEdR(2) = E*(-alpha(1))
      dEdR(1) = E*(-alpha(2)*((R(2)+R(3))/2.0d0-Rmex0(2)))
      dEdR(3) = dEdR(2)
      do i = 1,3
        dEdR(i) = dEdR(i) + temp2*(dtdQ(1)*dQdR(1,i)+dtdQ(2)*dQdR(2,i)+dtdQ(3)*dQdR(3,i))
      enddo
      
      E = E/autocmi
      dEdR = dEdR/autocmi

      end subroutine n2o_spin_orbit
