!***********************************************************************
!   System:                     N2O (first excited 1A' state)
!   Functional form:            six-order polynomials
!   Common name:                N2O-1Ap
!   Number of derivatives:      1
!   Number of bodies:           3
!   Number of electronic surfaces: 1
!   Interface: Section-2
!
!   References: Miguel Gonzalez, R. Valero and R. Sayos
!               doi:10.1063/1.1327263
!
!   Note: HE-MM-1 format
!
!    1 : O - N
!    2 : N - N'
!    3 : N'- O
!   Aysymptotic energy: N(2D)+NO: V10(1) - De(2) = -3.468 eV
!                       O(1D)+N2: V10(3) - De(1) = -7.5128eV
!                       N+N+O: not tractable
!   Minimum energy:   R(N-N)=1.1326A R(N-O)=1.1999A N-N-O=180
!                     E=-10.8785eV
!   Input: X(3),Y(3),Z(3)               in units of bohr
!   Output: E                           in units of hartree
!   Output: dEdX(3),dEdY(3),dEdZ(3)     hartree/bohr
!***********************************************************************
      subroutine pot(symb,x,y,z,e,dEdX,dEdY,dEdZ,nat,mnat)
      implicit none
      integer,parameter :: dp=8
      integer :: nat,mnat
      character(2) :: symb(mnat)
      real(dp) :: x(nat), y(nat),z(nat),e,dEdX(nat),dEdY(nat),dEdZ(nat)
      real(dp) :: xpes(3,3),dEdXpes(3,3)
      real(dp) :: temp(3)
      integer :: i,io
      io=1

      xpes(:,1) = x
      xpes(:,2) = y
      xpes(:,3) = z


      do i=1,nat
        if (symb(i) .eq. 'o') then
          if (i .ne.1) then
            ! make sure first one is always oxygen
            io = i
            temp = xpes(1,:)
            xpes(1,:) = xpes(io,:)
            xpes(io,:) = temp
            exit
          endif
        endif
      enddo

      call n2opes_onep(xpes,e,dEdXpes)

      if (io .ne. 1 ) then
        temp = dEdXpes(io,:)
        dEdXpes(io,:) = dEdXpes(1,:)
        dEdXpes(1,:) = temp
      endif
      dEdX = dEdXpes(:,1)
      dEdY = dEdXpes(:,2)
      dEdZ = dEdXpes(:,3)

      end subroutine pot

! =============================================================================
!                      N2O PES for 1A' State O(1D)+N2(X) or N(2D)+NO(X)
! ============================================================================
      subroutine n2opes_onep(X,E,dEdX)

      implicit none

      integer,parameter :: dp=8
      ! input and ioutput
      ! first index is for different atom
      ! second index is for x,y,z
      ! Xin : O, N,  N'
      ! X   : N, N', O
      real(dp),intent(in) :: X(3,3)
      real(dp),intent(out) :: dEdX(3,3)
      real(dp) :: E


      ! constants
      real(dp),parameter :: kcal2ev=0.0433634D0, ev2hart=0.0367502D0
      real(dp),parameter :: bohr2a=0.52917721067D0
      real(dp),parameter :: irt2 =1.0D0/dsqrt(2.0D0)

      integer,parameter :: MolType(3) =  (/1,2,1/) !ON,NN',N'O

      ! one body constants
      real(dp),parameter :: V10(3) = (/2.6730d0, 2.6730d0, 2.1133d0/)
      real(dp),parameter :: alpha(3)=(/1.5d0, 1.5d0, 1.0d0/) !A^{-1}
      real(dp),parameter :: bp(3,3) = reshape((/-1.0d0, 3.0d0,-1.0d0,&
                                                -1.0d0,-1.0d0, 3.0d0,&
                                                 3.0d0,-1.0d0,-1.0d0/),&
                                               (/3,3/))  !bp(i,j)

      ! two body constants NO, N2, NO
      !real(dp),parameter :: De(2)=(/140.73D0,212.55D0/)*kcal2ev
      real(dp),parameter :: De(2)=(/6.14010d0,9.26610d0/)
      real(dp),parameter :: Re(2)=(/1.1598D0,1.1034D0/)
      real(dp),parameter :: a(5,2)=reshape((/4.14169D0, 0.858731D0,&
                                 -0.972979D0,-3.70379D0, 2.48770D0,&
                                  3.88769D0, 0.100392D0,-0.912322D0,&
                                 -2.73055D0, 1.69645D0/),(/5,2/))

      ! three body constants
      real(dp),parameter :: Rtri(2)=(/2.0d0,1.1343D0/)
      real(dp),parameter :: c(50) =  (/      1.0d00,  16.9687D0,  56.0342D0,&
                              183.288D0,  55.6607D0, -54.5182D0,  15.1949D0,&
                              4.85752D0, -18.5084D0,  58.6889D0,  221.503D0,&
                              176.678D0, -78.3086D0, -4.07967D0, -48.6335D0,&
                              -23.899D0,  150.763D0,   3.6847D0,  7.23895D0,&
                              49.8532D0,   14.073D0,  56.8991D0, -4.06696D0,&
                              51.1781D0,  93.8385D0,  1.87307D0,  14.6403D0,&
                             -1.96208D0, -38.1055D0, -134.143D0, -177.698D0,&
                             -312.039D0,  82.4051D0,  43.3783D0,  17.9349D0,&
                             -179.413D0, -204.166D0,  35.1623D0,  56.3209D0,&
                              5.92703D0,  17.0209D0, -106.102D0,  41.7606D0,&
                              103.779D0,  158.477D0,  112.289D0, -59.6943D0,&
                              113.151D0, -28.7549D0, -74.6061D0/)*(-0.244328D0)
      integer,parameter :: k_stride(4) = (/1,28,15,6/)
      integer :: imax,jmax,kmax,ic2(2)
      real(dp),parameter :: gam(2) = (/2.89960D0, 2.93103D0/)

      ! variables used in the function
      integer :: i,j,k,ic
      real(dp) :: R(3),dX(3,3),dRdX(3,3),dRdY(3,3),dRdZ(3,3)
      real(dp) :: V1(3),dV1dR(3),Sp(3),rho(3)
      real(dp) :: V2(3),rhotri(3),dV2dR(3)
      real(dp) :: spower(3,0:6),s(3),invs(3),dSdR(3),pfac(3),P,T,fac,dPdR(3)
      real(dp) :: tfac(2), dTdS(2),dTdR(3),V3,dV3dR(3),dVdR(3)
      real(dp) :: temp,temp1

      ! X order : O N N'

      ! convert X to R and calcualte dR/dX
      dRdX = 0.0d0
      dRdY = 0.0d0
      dRdZ = 0.0d0
      dX = 0.0d0

      !loop through O-N, N-N', N'-O bond
      do i = 1,3
        j = i+1
        if (j > 3) j=1

        dX(i,:) = X(i,:) - X(j,:)
        R(i) = dsqrt(dX(i,1)*dX(i,1) + dX(i,2)*dX(i,2) + dX(i,3)*dX(i,3))

        dRdX(i,i) =  dX(i,1)/R(i)
        dRdX(i,j) = -dRdX(i,i)

        dRdY(i,i) =  dX(i,2)/R(i)
        dRdY(i,j) = -dRdY(i,i)

        dRdZ(i,i) =  dX(i,3)/R(i)
        dRdZ(i,j) = -dRdZ(i,i)
      enddo
      dX = dX*bohr2a
      R = R*bohr2a


      do i = 1,3
        j = MolType(i)
        rho(i) = R(i) - Re(j)
        rhotri(i) = R(i) - Rtri(j)
      enddo

      ! one body interaction
      dV1dR = 0.0d0
      do i = 1,3
        Sp(i) = bp(i,1)*rhotri(1) + bp(i,2)*rhotri(2) + bp(i,3)*rhotri(3)
        temp = alpha(i)*Sp(i)*0.5d0
        V1(i) = V10(i)*0.5d0*(1.0d0 - dtanh(temp))
        ! dV1 / d Sp
        temp1 = -V10(i)*0.5d0/dcosh(temp)/dcosh(temp)*alpha(i)*0.5d0
        dV1dR(1) = dV1dR(1) + temp1*bp(i,1)
        dV1dR(2) = dV1dR(2) + temp1*bp(i,2)
        dV1dR(3) = dV1dR(3) + temp1*bp(i,3)
      enddo

      ! two body interaction
      dV2dR = 0.0d0
      do i = 1,3
        j = MolType(i)

        V2(i) = a(4,j) + a(5,j)*rho(i)
        V2(i) = a(3,j) + V2(i)*rho(i)
        V2(i) = a(2,j) + V2(i)*rho(i)
        V2(i) = a(1,j) + V2(i)*rho(i)
        V2(i) = 1.0D0 + rho(i)*V2(i)
        V2(i) = -De(j)*V2(i)*dexp(-a(1,j)*rho(i))

        dV2dR(i) = 4.0d0*a(4,j) + 5.0d0*a(5,j)*rho(i)
        dV2dR(i) = 3.0d0*a(3,j) + dV2dR(i)*rho(i)
        dV2dR(i) = 2.0d0*a(2,j) + dV2dR(i)*rho(i)
        dV2dR(i) =       a(1,j) + dV2dR(i)*rho(i)
        dV2dR(i) = -De(j)*dV2dR(i)*dexp(-a(1,j)*rho(i))
        dV2dR(i) = dV2dR(i) + V2(i)*(-a(1,j))
      enddo

      !--------------  Three-body interaction term
      ! precompute power
      s(1) = rhotri(2)
      s(2) = irt2*(rhotri(1)+rhotri(3))
      s(3) = irt2*(rhotri(1)-rhotri(3))
      invs(1) = 1.0D0/s(1)
      invs(2) = irt2/s(2)
      invs(3) = irt2/s(3)

      spower(:,0) = 1.0d0
      spower(:,1) = s
      do i = 2,6
        spower(:,i) = spower(:,i-1)*s
      enddo

      P = 0.0D0
      dPdR = 0.0D0
      dSdR = 0.0d0

      if (s(1)*s(2)*s(3) .ne. 0.0d0) then
         ic = 0
         do k = 0,6,2
            pfac(3) = invs(3)*dble(k)
            do j = 0,(6-k)
               pfac(2) = invs(2)*dble(j)
               dSdR(1) = pfac(2) + pfac(3)
               dSdR(3) = pfac(2) - pfac(3)
               temp = spower(2,j)*spower(3,k)
               do i = 0,(6-k-j)
                  dSdR(2) = invs(1)*dble(i)

                  ic = ic + 1
                  fac = c(ic)*spower(1,i)*temp
                  !write(*,*) c(ic)/(-0.244328d0),i,j,k
                  P = P + fac
                  dPdR = dPdR + fac*dSdR
               enddo
            enddo
         enddo
      else
         ! the following condition rarely happens
         if (s(3) .eq. 0.0d0) then
            kmax = 0
         else
            kmax = 6
         endif

         ic2(1) = 0
         do k = 0, kmax, 2
            if (s(2) .eq. 0.0d0) then
               jmax = 1
            else
               jmax = 6-k
            endif
            if ( k .eq. 6) jmax = 0
            ic2(1) = ic2(1)+k_stride(k/2+1) !index of 0,0,k
            if (k.eq. 0) then
               temp = 0.0d0
            else
               temp = irt2*dble(k)*spower(3,k-1)
            endif

            do j = 0,jmax
               if (s(1) .eq. 0.0d0) then
                  imax = 1
               else
                  imax = 6-k-j
               endif
               if ((k+j) .eq. 6) imax = 0
               ic2(2) = ic2(1) + int(dble(j)*(7.0d0-dble(k)-(dble(j)-1.0d0)/2.0d0)) !index of 0,j,k

               temp1 = spower(2,j)*spower(3,k)
               if ( j .eq. 0) then
                  pfac(2) = 0.0d0
               else
                  pfac(2) = irt2*dble(j)*spower(2,j-1)*spower(3,k)
               endif
               pfac(3) = temp*spower(2,j)

               do i = 0,imax
                  ic = ic2(2) + i
                  P = P + c(ic)*spower(1,i)*temp1
                  if ( i .eq. 0)then
                     dSdR(2) = 0.0d0
                  else
                     dSdR(2) = dble(i)*spower(1,i-1)*temp1
                  endif
                  !write(*,'("indx=",6I3,F8.3)') i,j,k,ic,ic2(1),ic2(2),c(ic)
                  dSdR(1) = (pfac(2)+pfac(3))*spower(1,i)
                  dSdR(3) = (pfac(2)-pfac(3))*spower(1,i)
                  dPdR = dPdR + dSdR*c(ic)
               enddo
            enddo
         enddo
      endif


      pfac(1:2) = gam*s(1:2)/2.0D0
      tfac = 1.0D0 - dtanh( pfac(1:2))
      T = tfac(1)*tfac(2)
      dTdS = 1.0D0/dcosh(pfac(1:2))
      dTdS = - dTdS * dTdS * gam/2.0D0  !dT/dS

      dTdR(2) = dTdS(1)*tfac(2)
      dTdR(1) = tfac(1)*dTdS(2)*irt2
      dTdR(3) = dTdR(1)

      V3 = P*T
      dV3dR = dPdR*T + P*dTdR

      ! ---------- summation
      E = V1(1)+V1(2)+V1(3)+V2(1)+V2(2)+V2(3)+V3
      E = E - V10(3)+45.37d0*kcal2ev
      E = E*ev2hart

      ! dV/d  (R(O-N),R(N-N'), R(N'-O))
      dVdR = dV1dR + dV2dR + dV3dR
      ! dE/dX (R(O-N), R(N-N'), R(N'-O))
      dEdX(:,1) = dVdR(1)*dRdX(1,:) + dVdR(2)*dRdX(2,:) + &
                  dVdR(3)*dRdX(3,:)
      dEdX(:,2) = dVdR(1)*dRdY(1,:) + dVdR(2)*dRdY(2,:) + &
                  dVdR(3)*dRdY(3,:)
      dEdX(:,3) = dVdR(1)*dRdZ(1,:) + dVdR(2)*dRdZ(2,:) + &
                  dVdR(3)*dRdZ(3,:)

      dEdX = dEdX*ev2hart*bohr2a
      end subroutine n2opes_onep

!==================================================================
!                   N2O two body interaction
!==================================================================
      subroutine diapot_int(r,im,nsurf,v)
      ! two body interaction
      ! im = 1,3 NO bond
      ! im = 2   NN bond
      ! r in bohr, v in hartree
      implicit none
      integer,parameter :: dp = 8
      ! constans
      real(dp),parameter :: kcal2ev=0.0433634D0,ev2hart=0.0367502D0
      real(dp),parameter :: bohr2a=0.52917721067D0
      real(dp),parameter :: De(2)=(/6.14010d0,9.26610d0/)
      real(dp),parameter :: Re(2)=(/1.1598D0,1.1034D0/)
      real(dp),parameter :: a(5,2)=reshape((/4.14169D0, 0.858731D0,&
                                 -0.972979D0,-3.70379D0, 2.48770D0,&
                                  3.88769D0, 0.100392D0,-0.912322D0,&
                                 -2.73055D0, 1.69645D0/),(/5,2/))

     ! contants
      real(dp) :: r(3), rA ,v, rho
      integer :: im,nsurf,j

      if (im .le. 3)then
        if (im .eq. 2)then
          j = 2
        else
          j = 1
        endif

        rA = r(im)*bohr2a

        rho = rA - Re(j)

        v = a(4,j) + a(5,j)*rho
        v = a(3,j) + v*rho
        v = a(2,j) + v*rho
        v = a(1,j) + v*rho
        v = 1.0D0 + rho*v
        v = -De(j)*v*dexp(-a(1,j)*rho)
        v = v*ev2hart
      endif
      end subroutine diapot_int


      subroutine prepot
      return
      end
