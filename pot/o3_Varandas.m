function [V, DVDR] = PotentialEnergySurface(Rin)
% This is a Verandas potential energy function for O3 Version 5
% Written by Marat Kulakhmetov and Devon Parkos based on Verandas paper and
% Fortran file

%Inputs: R1, R2 R3 vector in Angstrom
%Output V and dV/dR1, dV/dR2, dV/dR3 in eV

% Last changed on 5/4/2012
global Rm R0 D a gamma Rref gam1 gam2 beta rho 

%% Define global variables and constants. Some of these work better if defined within sub functions
a0 = 0.529177; %A
Eh = 27.211652; %eV

Rm = 2.2818;
R0 = 5.661693;

% Used within VEHF2
D = -0.142912;
a = [3.6445902 3.9281208 2.0986665];
gamma = 3.3522487;

% Used within VEHF3
Rref = 2.967200;
gam1 = 1.27;
gam2 = 0.635;
beta = 0.224;
%c = [0.3058714 0.1157783 0.1682194 -0.1502505 -0.6563872e-2 0.1805864e-1 0.3434551e-1];
%d = [0.1561575e3 0.4951110e2 0.1157478e7 -0.35624264e6 0.27725091e5];
%Qmat = [ 0.5773502 0.5773502 0.5773502; 0.0 0.7071067 -0.7071067; 0.8164965 -0.4082482, -0.4082482];

% Used within VCORR3
% n = [6 8 10];
% C = [15.4 235.21994 4066.2393];
% alpha0 = 25.952776;
% alpha1 = 1.186793;
% % 
% beta0 = 15.738100;
% beta1 = 0.097291;

% A = [3.0951333 2.1999 1.6880714];
% B = [8.7787894 7.2265122 5.9487108];
rho = 8.21801625;
% A = alpha0*n.^-alpha1;
% B = beta0*exp(-beta1*n);
% rho = (Rm + 2.5*R0)/2;

% k0 = [0.2772806 0.6233148 1.237123];
% k1 = [0.9545174 0.8116684 0.7043832];

%Used within VELEC3
% C5 = 1.3144;
% Kp = 3.3522;

%% Parse Subroutine Outputs into Potential and Gradient
%  a0=1; 
R = Rin/a0 ;

OUT = VEHF2(R)+ VEHF3(R) + VCORR3(R)+VELEC3(R);
%OUT = VEHF2(R);
%  OUT = test(R);
V = OUT(1)*Eh; % Convert to eV
DVDR = OUT(2:4)*Eh/a0;%*Eh/a0; % Convert to eV/A

return

function [OUT] = test(R)
V = sum(R.^3);
dV = 3*R.^2;

% V = sum(R);
% dV = [1 1 1];
OUT = [ V dV];

return

function [OUT] = VEHF2(R) %(GOOD)
global Rm D a gamma

%% Subroutine Specific Constants
% D = -0.142912;
% a = [3.6445902 3.9281208 2.0986665];
% gamma = 3.3522487;


%% Calculate EHF2 Potential Energy
r = R-Rm;

V = sum(D*(1 + a(1)*r + a(2)*r.^2 + a(3)*r.^3).*exp(-gamma*r));
DVDR = D*(-gamma*(1 + a(1)*r + a(2)*r.^2 + a(3)*r.^3) +(a(1) + 2*a(2)*r + 3*a(3)*r.^2)).*exp(-gamma*r);

OUT = [V DVDR];
return


function [OUT] = VEHF3(R) % (GOOD)
global Rref gam1 gam2 beta 

%% Subroutine Specific Constants
% Rref = 2.967200;
% gam1 = 1.27;
% gam2 = 0.635;
% beta = 0.224;

%Qmat = [sqrt(1/3) sqrt(1/3) sqrt(1/3); 0 sqrt(1/2) -sqrt(1/2); sqrt(2/3) -sqrt(1/6) -sqrt(1/6)];
Qmat = [ 0.5773502 0.5773502 0.5773502; 0.0 0.7071067 -0.7071067; 0.8164965 -0.4082482, -0.4082482];
Q = Qmat*(R'-Rref);
Gam = [ Q(1); Q(2)^2+Q(3)^2; Q(3)^3 - 3*Q(2)^2*Q(3)];
 
c = [0.3058714 0.1157783 0.1682194 -0.1502505 -0.6563872e-2 0.1805864e-1 0.3434551e-1];
d = [0.1561575e3 0.4951110e2 0.1157478e7 -0.35624264e6 0.27725091e5];

%% Calculate EHF3 Potential

RS = R.^2;
Den = 2*R(1)*R(2)*R(3); 
S = sum((2*RS-sum(RS)).*R)/Den;

P = [0 0];
exp1 = exp(-beta*Gam(2)); 
S1p5 = (1.5+S)^5;
dline = d(3)+d(4)*Gam(2)+d(5)*Gam(2)^2;
S1p25 = (1.2527+S)^5; 

P(1) = c*[1 Gam(1) Gam(1)^2 Gam(2) Gam(1)*Gam(2) Gam(3) Gam(2)^2]';
% P(2) = (d(1)+d(2)*Gam(1)+(d(3)+d(4)*Gam(2)+d(5)*Gam(2)^2)*(1.5+S)^5)*(1.2527+S)^5*exp1;
P(2) = (d(1)+d(2)*Gam(1)+(dline)*S1p5)*S1p25*exp1;

V = P(1)*(1-tanh(gam1*Gam(1))) + P(2)*(1-tanh(gam2*Gam(1)));

%% Gradient
dSdR = (R.*(2*R-sum(R)) - (sum(R.^3)-R.^2*sum(R)-R(1)*R(2)*R(3)./R.*(sum(R)-R))/2./R)/R(1)/R(2)/R(3);

dQdR = Qmat;

dGamdR = [dQdR(1,:); 2*Q(2)*dQdR(2,:)+2*Q(3)*dQdR(3,:); 3*Q(3)^2*dQdR(3,:) - 6*Q(2)*Q(3)*dQdR(2,:) - 3*Q(2)^2*dQdR(3,:)];
%dGamdR = [ 1 0 0; 0 2*Q(2) 2*Q(3); 0 -6*Q(2)*Q(3) 3*Q(3)^2-3*Q(2)^2] * Qmat;


dP1dR = c*[[0 0 0]; dGamdR(1,:); 2*Gam(1)*dGamdR(1,:); dGamdR(2,:); dGamdR(1,:)*Gam(2)+Gam(1)*dGamdR(2,:); dGamdR(3,:); 2*Gam(2)*dGamdR(2,:)];

% dP2dR = (d(2)*dGamdR(1,:)+(d(4)*dGamdR(2,:)+d(5)*2*Gam(2)*dGamdR(2,:))*(1.5+S)^5+(d(3)+d(4)*Gam(2)+d(5)*Gam(2)^2)*5*(1.5+S)^4*dSdR)*(1.2527+S)^5*exp(-beta*Gam(2));
% dP2dR = dP2dR + (d(1)+d(2)*Gam(1)+(d(3)+d(4)*Gam(2)+d(5)*Gam(2)^2)*(1.5+S)^5)*(5*(1.2527+S)^4*dSdR*exp(-beta*Gam(2))+(1.2527+S)^5*exp(-beta*Gam(2))*-beta*dGamdR(2,:));

% dP2dR = (d(2)*dGamdR(1,:)+(d(4)*dGamdR(2,:)+d(5)*2*Gam(2)*dGamdR(2,:))*S1p5+(dline)*5*(1.5+S)^4*dSdR)*S1p25*exp1;
% dP2dR = dP2dR + (d(1)+d(2)*Gam(1)+(dline)*S1p5)*(5*(1.2527+S)^4*dSdR*exp1+S1p25*exp1*-beta*dGamdR(2,:));
dP2dR = (d(2)*dGamdR(1,:)+(d(4)*dGamdR(2,:)+d(5)*2*Gam(2)*dGamdR(2,:))*S1p5+(dline)*5*(1.5+S)^4*dSdR)*S1p25;
dP2dR = dP2dR + (d(1)+d(2)*Gam(1)+(dline)*S1p5)*(5*(1.2527+S)^4*dSdR+S1p25*-beta*dGamdR(2,:));
dP2dR = dP2dR * exp1;


DVDR = dP1dR*(1-tanh(gam1*Gam(1))) + dP2dR*(1-tanh(gam2*Gam(1))) + P(1)*-sech(gam1*Gam(1))^2*gam1*dGamdR(1,:) + P(2)*-sech(gam2*Gam(1))^2*gam2*dGamdR(1,:);

 OUT = [V DVDR];
%OUT = [P(2) dP2dR];
return


function [OUT] = VCORR3(R)
global Rm rho

%% Subroutine Specific Constants
 n = [6 8 10];
% 
C = [15.4 235.21994 4066.2393];
% alpha0 = 25.952776;
% alpha1 = 1.186793;
% 
% beta0 = 15.738100;
% beta1 = 0.097291;

A = [3.0951333 2.1999 1.6880714];
B = [8.7787894 7.2265122 5.9487108];
% rho = 8.21801625;
% A = alpha0*n.^-alpha1;
% B = beta0*exp(-beta1*n);
% rho = (Rm + 2.5*R0)/2;


k0 = [0.2772806 0.6233148 1.237123];
k1 = [0.9545174 0.8116684 0.7043832];


%% Sum accross n values 
V = 0;
DVDR = [0 0 0];
gh_product = [ 0 0 0];
dgh_product_dR = zeros(3,3);

RoR = R/rho;
RoR2 = RoR.^2;

for k = 1:3 % Sum over R values
    %% Potential
    g = 1 + k0(k).*exp(-k1(k)*(R-Rm));
    h = tanh(k1(k)*R);
    
    gh_product(1) = h(2)*g(3)+h(3)*g(2);
    gh_product(2) = h(3)*g(1)+h(1)*g(3);
    gh_product(3) = h(1)*g(2)+h(2)*g(1);
    
    x = (1 - exp(-A(k)*RoR - B(k)*RoR2)).^n(k);

    V = V + -.5*C(k)*sum(x.*(gh_product).*R.^-n(k));
    
    %% Gradient
    dgdR = k0(k).*exp(-k1(k)*(R-Rm)).*-k1(k);
    dhdR =sech(k1(k)*R).^2*k1(k);
     
    dgh_product_dR(1,:) = [0 dhdR(2)*g(3)+h(3)*dgdR(2) dhdR(3)*g(2)+h(2)*dgdR(3)];
    dgh_product_dR(2,:) = [dhdR(1)*g(3)+h(3)*dgdR(1) 0 dhdR(3)*g(1)+h(1)*dgdR(3)];
    dgh_product_dR(3,:) = [dhdR(1)*g(2)+h(2)*dgdR(1) dhdR(2)*g(1)+h(1)*dgdR(2) 0];
    
    dxdR = n(k)*(1 - exp(-A(k)*RoR - B(k)*RoR2)).^(n(k)-1).*exp(-A(k)*RoR - B(k)*RoR2).*(A(k)/rho + 2*B(k)*R/rho^2);

    DVDR = DVDR + C(k)*dxdR.*(-.5*gh_product).*R.^-n(k) + C(k)*x.*(-.5*gh_product)*-n(k).*R.^(-n(k)-1);
    for k2 = 1:3
        DVDR = DVDR + C(k)*x(k2).*-.5*dgh_product_dR(k2,:).*R(k2).^-n(k);
    end
end
% 
OUT = [V,DVDR];
return


function [OUT] = VELEC3(R)
global Rm rho  

%% Subroutine Specific Constants

alpha0 = 25.952776;
alpha1 = 1.186793;

beta0 = 15.738100;
beta1 = 0.097291;
% 
n = 5; 
A = alpha0*n.^-alpha1;
B = beta0*exp(-beta1*n);
% % rho = (Rm + 2.5*R0)/2;


C5 = 1.3144;
Kp = 3.3522;


%% Sum accross n values 
DVDR = [0 0 0];

    %% Potential
    RoRm = R/Rm;
    exp1 = exp(-Kp*(R-Rm));
    g = ((RoRm).^4).*exp1;
    h = tanh(Kp*R);
    
    gh_product = [ 0 0 0];
    gh_product(1) = h(2)*g(3)+h(3)*g(2);
    gh_product(2) = h(3)*g(1)+h(1)*g(3);
    gh_product(3) = h(1)*g(2)+h(2)*g(1);
    
    RoR = R/rho; 
    x = (1 - exp(-A*RoR - B*RoR.^2)).^n;

    V = -0.5*C5*sum((x.*gh_product).*R.^-n);
    
    %% Gradient
    dgdR = (4*(RoRm).^3)/Rm.*exp1+ g*-Kp;
    dhdR =sech(Kp*R).^2*Kp;
    
    dgh_product_dR = zeros(3,3);
    dgh_product_dR(1,:) = [0 dhdR(2)*g(3)+h(3)*dgdR(2) dhdR(3)*g(2)+h(2)*dgdR(3)];
    dgh_product_dR(2,:) = [dhdR(1)*g(3)+h(3)*dgdR(1) 0 dhdR(3)*g(1)+h(1)*dgdR(3)];
    dgh_product_dR(3,:) = [dhdR(1)*g(2)+h(2)*dgdR(1) dhdR(2)*g(1)+h(1)*dgdR(2) 0];

    dxdR = n*(1 - exp(-A*RoR - B*RoR.^2)).^(n-1).*exp(-A*RoR - B*RoR.^2).*(A/rho + 2*B*R/rho^2);

    DVDR = DVDR + C5*dxdR.*(-.5*gh_product).*R.^-n + C5*x.*(-.5*gh_product)*-n.*R.^(-n-1);
    for k2 = 1:3
        DVDR = DVDR + C5*x(k2).*-.5*dgh_product_dR(k2,:).*R(k2).^-n;
    end

% 
OUT = [V,DVDR];
return


