      subroutine pot(x,y,z,vv,dvdx,dvdy,dvdz,natoms,maxatom)
      implicit real*8(a-h,o-z)
      double precision bij(maxatom,maxatom),dbij(maxatom,maxatom),
     & sbij(maxatom,maxatom),v3(maxatom),dv1(maxatom,maxatom),
     & rij(maxatom,maxatom)
      double precision dvdy(maxatom),dvdz(maxatom),dvdx(maxatom),
     &   x(maxatom),y(maxatom),z(maxatom)

      ca   = 0.14359D+01
      cc   = 0.20995D-02
      cd   = 0.15124D+02
      gam1 = 0.16211D-02
      alp  = 0.52879D+01
      c    = 0.35225D-02
      d    = 0.10409D+01
      en   = 0.86784D-03
      del1 = 0.13000D+02
C      r1=5.160d0

C      den=1.d0-(r1/del1)**en
C      cn1=dexp(gam1/den)
      cn1   = 7.556411418d0

C      calculate rij matrix
      alp2=0.5d0*alp
      d2=0.5d0*d
      ca2=0.5d0*ca
      cd2=0.5d0*cd
      gam1en=gam1*en
      do i=1,natoms
        dvdx(i) = 0.d0
        dvdy(i) = 0.d0
        dvdz(i) = 0.d0
        do j=i+1,natoms
          dx=x(i)-x(j)
          dy=y(i)-y(j)
          dz=z(i)-z(j)
          rij(i,j)=dsqrt(dx*dx+dy*dy+dz*dz)
          rij(j,i) = rij(i,j)
C
        if (rij(i,j).ge.del1) goto 222
C          rdel1=rij(i,j)/del1
          tmp=(rij(i,j)/del1)**en
          den=1.d0/(1.0d0-tmp)
          bij(i,j)=cn1*dexp(-gam1*den)
          sbij(i,j)=bij(i,j)**(alp2-1.d0)
C         dbij=dbij/rij
          dbij(i,j)=-bij(i,j)*gam1en*tmp/(rij(i,j)*rij(i,j))
          dbij(i,j)=dbij(i,j)*den*den
          dv1(i,j)=alp*c*sbij(i,j)*(sbij(i,j)*bij(i,j)-d2)
          sbij(i,j)=sbij(i,j)*bij(i,j)
          bij(j,i)=bij(i,j)
          dbij(j,i)=dbij(i,j)
          sbij(j,i)=sbij(i,j)
          dv1(j,i)=dv1(i,j)
 222    enddo
      enddo
      en1=0.d0
      en2=0.d0
      do 1 i=1,natoms
        v3(i)=0.d0
      do 2 j=1,natoms
        if(i.eq.j)go to 2
        if (rij(i,j).ge.del1) goto 2
C
        if (j.lt.i) goto 111
C          sbij=bij(i,j)**(0.5d0*alp)
          fa=c*sbij(i,j)*(sbij(i,j)-d)
          en2=en2+fa

 111    v3(i)=v3(i)+bij(i,j)

   2  continue   
      if (v3(i).lt.1.d-40) then
              va3=0.d0
      else
               va3=v3(i)**(ca2-1.d0)
      endif
      een1=va3*v3(i)*(va3*v3(i)-cd)
      en1=en1+cc*een1

      dv3a=cc*ca*va3*(va3*v3(i)-cd2)
C
      do 5 j=1,natoms
        if (i.eq.j .or. rij(i,j).ge.del1) goto 5
          tmp=(dv3a+dv1(i,j))*dbij(i,j)
          dvdx(i) = dvdx(i) + tmp*(x(i)-x(j))
          dvdy(i) = dvdy(i) + tmp*(y(i)-y(j))
          dvdz(i) = dvdz(i) + tmp*(z(i)-z(j))
          dvdx(j) = dvdx(j) - tmp*(x(i)-x(j))
          dvdy(j) = dvdy(j) - tmp*(y(i)-y(j))
          dvdz(j) = dvdz(j) - tmp*(z(i)-z(j))
 5    continue

   1  continue  
      vv=en1+en2*2.d0
C
      return
      end      

      subroutine prepot
      return
      end
