!***********************************************************************
!   System:                     O3 ground state
!   Functional form:            DMBE
!   Common name:                O3 Varandas PES
!   Number of derivatives:      1
!   Number of bodies:           3
!   Number of electronic surfaces: 1
!   Interface: Section-2
!
!   References: A realistic double many-body expansion (DMBE) potential
!               energy surface for ground-state O 3 from a multiproperty fit
!               to ab initio calculations, and to experimental spectroscopic,
!               inelastic scattering, and kinetic isotope thermal rate data
!
!               Varandas, A.J.C., Pais, A.A.C.C.
!               doi: 10.1080/00268978800101451
!
!   Coder: Han Luo (Converted form Marat Kulakhmetov's Matlab code)
!   Note: HE-MM-1 format
!
!    1 : O - O
!    2 : O - O
!    3 : O - O
!   Input: X(3),Y(3),Z(3)               in units of bohr
!   Output: E                           in units of hartree
!   Output: dEdX(3),dEdY(3),dEdZ(3)     hartree/bohr
!***********************************************************************
      subroutine pot(symb,x,y,z,E,dEdX,dEdY,dEdZ,nat,mnat)
        implicit none
        integer,parameter :: dp=8
        integer :: nat,mnat
        character(2) :: symb(mnat)
        real(dp) :: x(nat), y(nat),z(nat),E,dEdX(nat),dEdY(nat),dEdZ(nat)

        real(8) :: R(3)
        integer :: i,j

        real(dp) :: dX(3,3),dRdX(3,3),dRdY(3,3),dRdZ(3,3)
        real(dp) :: dEdR(3)

        dRdX = 0.0d0
        dRdY = 0.0d0
        dRdZ = 0.0d0
        do i = 1,3
          j = i+1
          if (j > 3) j=1

          dX(i,1) = x(i)-x(j)
          dX(i,2) = y(i)-y(j)
          dX(i,3) = z(i)-z(j)

          R(i) = dsqrt(dX(i,1)**2+dX(i,2)**2+dX(i,3)**2)

          dRdX(i,i) =  dX(i,1)/R(i)
          dRdX(i,j) = -dRdX(i,i)

          dRdY(i,i) =  dX(i,2)/R(i)
          dRdY(i,j) = -dRdY(i,i)

          dRdZ(i,i) =  dX(i,3)/R(i)
          dRdZ(i,j) = -dRdZ(i,i)
        enddo



        call o3pes(R,e,dEdR)
        dEdX = dEdR(1)*dRdX(1,:) + dEdR(2)*dRdX(2,:) + &
          dEdR(3)*dRdX(3,:)
        dEdY = dEdR(1)*dRdY(1,:) + dEdR(2)*dRdY(2,:) + &
          dEdR(3)*dRdY(3,:)
        dEdZ = dEdR(1)*dRdZ(1,:) + dEdR(2)*dRdZ(2,:) + &
          dEdR(3)*dRdZ(3,:)


      end subroutine pot


      subroutine o3pes(Rin, V, DVDR)
  ! Rin should be bohr
  ! V will be in hartree
  ! DVDR in hartree/bohr
        implicit none
        interface
          function VEHF2(Rin)
            implicit none
            real(8) :: VEHF2(4)
            real(8) :: Rin(3)
          end function VEHF2

          function VEHF3(R)
            implicit none
            real(8) :: VEHF3(4)
            real(8) :: R(3)
          end function VEHF3

          function VCORR3(R)
            implicit none
! Arguments declarations
            real(8) :: VCORR3(4)
            real(8) :: R(3)
          end function VCORR3

          function VELEC3(R)
            implicit none
! Arguments declarations
            real(8) :: VELEC3(4)
            real(8) :: R(3)
          end function VELEC3
        end interface


        real(8),parameter :: a0 = 0.529177d0 ! 1A = a0 Bohr
        real(8),parameter :: Eh = 27.211652d0 !eV
! Arguments declarations
        real(8), intent(in) :: Rin(3)
        real(8), intent(out) :: V
        real(8), intent(out) :: DVDR(3)
  ! real(8) :: R(3)
! Variable declarations
        real(8) :: result(4)
!
! This is a Verandas potential energy function for O3 Version 5
! Written by Marat Kulakhmetov and Devon Parkos based on Verandas paper and
! Fortran file

!Inputs: R1, R2 R3 vector in Angstrom
!Output V and dV/dR1, dV/dR2, dV/dR3 in eV

! Last changed on 5/4/2012
! global Rm R0 D a fgamma Rref gam1 gam2 beta rho

!! Define global variables and constants. Some of these work better if defined within sub functions

        real(8),parameter :: Rm = 2.2818d0
        real(8),parameter :: R0 = 5.661693d0



! Used within VCORR3
! n = [6 8 10];
! C = [15.4 235.21994 4066.2393];
! alpha0 = 25.952776;
! alpha1 = 1.186793;
! !
! beta0 = 15.738100;
! beta1 = 0.097291;

! A = [3.0951333 2.1999 1.6880714];
! B = [8.7787894 7.2265122 5.9487108];
        real(8),parameter :: rho = 8.21801625d0
! A = alpha0*n.^-alpha1;
! B = beta0*exp(-beta1*n);
! rho = (Rm + 2.5*R0)/2;

! k0 = [0.2772806 0.6233148 1.237123];
! k1 = [0.9545174 0.8116684 0.7043832];

!Used within VELEC3
! C5 = 1.3144;
! Kp = 3.3522;

!! Parse Subroutine Outputs into Potential and Gradient
! a0=1;
  ! R = Rin/a0

        result = VEHF2(Rin)+ VEHF3(Rin) + VCORR3(Rin)+VELEC3(Rin)
!result = VEHF2(R);
! result = test(R);
  ! V = result(1)*Eh ! Convert to eV
  ! DVDR = result(2:4)*Eh/a0 !*Eh/a0; ! Convert to eV/A
        V = result(1) ! in hartree
        DVDR = result(2:4) ! hartree/bohr
      end subroutine o3pes

      function VEHF2(Rin)
        implicit none
! Used within VEHF2
        real(8),parameter :: Rm = 2.2818d0
        real(8),parameter :: D = -0.142912d0
        real(8),parameter :: a(3) = (/3.6445902d0,3.9281208d0,2.0986665d0/)
        real(8),parameter :: fgamma = 3.3522487d0

! Arguments declarations
        logical :: Twobody
        real(8) :: VEHF2(4)
        real(8) :: Rin(3)
! Variable declarations
        real(8) :: DVDR(3),r(3),V
        integer :: i
!
! global Rm D a fgamma

!! Subroutine Specific Constants
! D = -0.142912;
! a = [3.6445902 3.9281208 2.0986665];
! fgamma = 3.3522487;


!! Calculate EHF2 Potential Energy
        r = Rin-Rm
        V = 0.0d0
        do i = 1,3
          V = V + D*(1.0d0+r(i)*(a(1)+r(i)*(a(2)+r(i)*a(3))))*dexp(-fgamma*r(i))
          DVDR(i) = D*(-fgamma*(1 + a(1)*r(i) + a(2)*r(i)**2 + a(3)*r(i)**3) + &
            +(a(1) + 2*a(2)*r(i) + 3*a(3)*r(i)**2))*exp(-fgamma*r(i))
        end do

        VEHF2(1) = V
        VEHF2(2:4) = DVDR
      end function VEHF2


      function VEHF3(R)
        implicit none
! Used within VEHF3

        real(8),parameter :: Rref = 2.967200d0
        real(8),parameter :: gam1 = 1.27d0
        real(8),parameter :: gam2 = 0.635d0
        real(8),parameter :: beta = 0.224d0

!c = [0.3058714 0.1157783 0.1682194 -0.1502505 -0.6563872e-2 0.1805864e-1 0.3434551e-1];
!d = [0.1561575e3 0.4951110e2 0.1157478e7 -0.35624264e6 0.27725091e5];
!Qmat = [ 0.5773502 0.5773502 0.5773502; 0.0 0.7071067 -0.7071067; 0.8164965 -0.4082482, -0.4082482];
! Arguments declarations
        real(8) :: VEHF3(4)
        real(8) :: R(3)
! Variable declarations
        real(8) :: Den,S
        real(8) :: dGamdR(3,3), dP1dRT(3,7), dP1dR(3), dP2dR(3)
        real(8) :: DVDR(3),dline,dQdR(3,3)
        real(8) :: dSdR(3),exp1,S1p25,S1p5
        real(8) :: V,Q(3)
        real(8) :: RS(3),SumRS,Gam(3),P(2)
        real(8) :: sumR, sumR3
        integer :: i,j
!Qmat = [sqrt(1/3) sqrt(1/3) sqrt(1/3); 0 sqrt(1/2) -sqrt(1/2); sqrt(2/3) -sqrt(1/6) -sqrt(1/6)];
        real(8),parameter :: Qmat(3,3) = reshape((/0.5773502,0.5773502,0.5773502,&
          0.0,0.7071067,-0.7071067,&
          0.8164965,-0.4082482,-0.4082482/),(/3,3/))
        real(8) :: c(7) = (/0.3058714d0,0.1157783d0,0.1682194d0, &
          -0.1502505d0,-0.6563872d-2,0.1805864d-1,0.3434551d-1/)
        real(8) :: d(5) = (/0.1561575d3,0.4951110d2,0.1157478d7, &
          -0.35624264d6,0.27725091d5/)
!

!! Subroutine Specific Constants
! Rref = 2.967200;
! gam1 = 1.27;
! gam2 = 0.635;
! beta = 0.224;

!m2f: Q = Qmat*(R'-Rref)
        do i = 1,3
          Q(i) = Qmat(1,i)*(R(1)-Rref) + Qmat(2,i)*(R(2)-Rref) + Qmat(3,i)*(R(3)-Rref)
        end do

        Gam(1) = Q(1)
        Gam(2) = Q(2)**2+Q(3)**2
        Gam(3) = Q(3)**3-3.0d0*Q(2)**2*Q(3)


!! Calculate EHF3 Potential
        SumRS = 0.0d0
        do i = 1,3
          RS(i) = R(i)**2
          SumRS = SumRS + RS(i)
        end do
        Den = 2*R(1)*R(2)*R(3)

        S = 0.0d0
        do i = 1,3
          S = S + (2.0d0*RS(i) - SumRS)*R(i)
        end do
        S = S/Den
! mf
! S = sum((2*RS-sum(RS)).*R)/Den

        exp1 = dexp(-beta*Gam(2))
        S1p5 = (1.5d0+S)**5
        dline = d(3)+d(4)*Gam(2)+d(5)*Gam(2)**2
        S1p25 = (1.2527d0+S)**5

        P = 0.0d0
!m2f: P(1) = c*[ 1,Gam(1),Gam(1)**2,Gam(2),Gam(1)*Gam(2),Gam(3),Gam(2)**2 ] '
        P(1) = P(1) + c(1)*1 + c(2)*Gam(1) + c(3)*Gam(1)**2 + c(4)*Gam(2)
        P(1) = P(1) + c(5)*Gam(1)*Gam(2) + c(6)*Gam(3) + c(7)*Gam(2)**2

! P(2) = (d(1)+d(2)*Gam(1)+(d(3)+d(4)*Gam(2)+d(5)*Gam(2)^2)*(1.5+S)^5)*(1.2527+S)^5*exp1;
        P(2) = (d(1)+d(2)*Gam(1)+(dline)*S1p5)*S1p25*exp1

        V = P(1)*(1.0d0-tanh(gam1*Gam(1))) + P(2)*(1.0d0-tanh(gam2*Gam(1)))

        sumR = 0.0d0
        sumR3 = 0.0d0
        do i = 1,3
          sumR = sumR + R(i)
          sumR3 = sumR3 + R(i)**3
        end do
!! Gradient
        do i = 1,3
          dSdR(i) = (R(i)*(2*R(i)-sumR) - (sumR3-R(i)**2*sumR-R(1)*R(2)*R(3)/R(i)*(sumR-R(i)))/2/R(i))/R(1)/R(2)/R(3)
        end do

        dQdR = Qmat

        dGamdR(:,1) = dQdR(:,1)
        dGamdR(:,2) = 2.0d0*Q(2)*dQdR(:,2) + 2.0d0*Q(3)*dQdR(:,3)
        dGamdR(:,3) = 3.0d0*Q(3)**2*dQdR(:,3) -&
          6.0d0*Q(2)*Q(3)*dQdR(:,2) - &
          3.0d0*Q(2)**2*dQdR(:,3)
!dGamdR = [ 1 0 0; 0 2*Q(2) 2*Q(3); 0 -6*Q(2)*Q(3) 3*Q(3)^2-3*Q(2)^2] * Qmat;


!dP1dR = c*[[0 0 0]; dGamdR(1,:); 2*Gam(1)*dGamdR(1,:); dGamdR(2,:); dGamdR(1,:)*Gam(2)+Gam(1)*dGamdR(2,:); dGamdR(3,:); 2*Gam(2)*dGamdR(2,:)];
        dP1dRT(:,1) = 0.0d0
        dP1dRT(:,2) = dGamdR(:,1)
        dP1dRT(:,3) = 2.0d0*Gam(1)*dGamdR(:,1)
        dP1dRT(:,4) = dGamdR(:,2)
        dP1dRT(:,5) = dGamdR(:,1)*Gam(2)+Gam(1)*dGamdR(:,2)
        dP1dRT(:,6) = dGamdR(:,3)
        dP1dRT(:,7) = 2.0d0*Gam(2)*dGamdR(:,2)

        dP1dR = 0.0d0
        do j = 1,3
          do i = 1,7
            dP1dR(j) = dP1dR(j) + c(i)*dP1dRT(j,i)
          end do
        end do



! dP2dR = (d(2)*dGamdR(1,:)+(d(4)*dGamdR(2,:)+d(5)*2*Gam(2)*dGamdR(2,:))*(1.5+S)^5+(d(3)+d(4)*Gam(2)+d(5)*Gam(2)^2)*5*(1.5+S)^4*dSdR)*(1.2527+S)^5*exp(-beta*Gam(2));
! dP2dR = dP2dR + (d(1)+d(2)*Gam(1)+(d(3)+d(4)*Gam(2)+d(5)*Gam(2)^2)*(1.5+S)^5)*(5*(1.2527+S)^4*dSdR*exp(-beta*Gam(2))+(1.2527+S)^5*exp(-beta*Gam(2))*-beta*dGamdR(2,:));

! dP2dR = (d(2)*dGamdR(1,:)+(d(4)*dGamdR(2,:)+d(5)*2*Gam(2)*dGamdR(2,:))*S1p5+(dline)*5*(1.5+S)^4*dSdR)*S1p25*exp1;
! dP2dR = dP2dR + (d(1)+d(2)*Gam(1)+(dline)*S1p5)*(5*(1.2527+S)^4*dSdR*exp1+S1p25*exp1*-beta*dGamdR(2,:));
        dP2dR = (d(2)*dGamdR(:,2)+(d(4)*dGamdR(:,2)+&
          d(5)*2.0d0*Gam(2)*dGamdR(:,2))*S1p5+dline*5.0d0*(1.5d0+S)**4*dSdR)*S1p25
        dP2dR = dP2dR + (d(1)+d(2)*Gam(1)+(dline)*S1p5)*(5.0d0*(1.2527d0+S)**4*dSdR+S1p25*(-beta)*dGamdR(:,2))
        dP2dR = dP2dR * exp1

        DVDR = dP1dR*(1.0d0-tanh(gam1*Gam(1)))
        DVDR = DVDR + dP2dR*(1.0d0-tanh(gam2*Gam(1)))
        DVDR = DVDR - P(1)/((cosh(gam1*Gam(1)))**2)*gam1*dGamdR(:,1)
        DVDR = DVDR - P(2)/((cosh(gam2*Gam(1)))**2)*gam2*dGamdR(:,1)

        VEHF3(1) = V
        VEHF3(2:4) = DVDR
! result = [ V,DVDR ]
!result = [P(2) dP2dR];
      end function VEHF3


      function VCORR3(R)
        implicit none
! Arguments declarations
        real(8) :: VCORR3(4)
        real(8) :: R(3)
! Variable declarations
        real(8) :: dgdR(3)
        real(8) :: dgh_product_dR(3,3)
        real(8) :: dhdR(3)
        real(8) :: DVDR(3)
        real(8) :: dxdR(3)
        real(8) :: g(3),h(3)
        real(8) :: gh_product(3) !m2f: check dim(:)!m
        real(8) :: RoR(3),RoR2(3)
        real(8) :: V
        real(8) :: x(3)
        integer :: i,k,k2
!
        real(8),parameter :: rho = 8.21801625d0
        real(8),parameter :: Rm = 2.2818d0

!! Subroutine Specific Constants
        integer,parameter :: n(3) = (/ 6,8,10 /)
!
! alpha0 = 25.952776;
! alpha1 = 1.186793;
!
! beta0 = 15.738100;
! beta1 = 0.097291;

        real(8),parameter :: A(3) = (/ 3.0951333d0,2.1999d0,1.6880714d0/)
        real(8),parameter :: B(3) = (/ 8.7787894d0,7.2265122d0,5.9487108d0/)
        real(8),parameter :: C(3) = (/ 15.4d0,235.21994d0,4066.2393d0 /)
! rho = 8.21801625;
! A = alpha0*n.^-alpha1;
! B = beta0*exp(-beta1*n);
! rho = (Rm + 2.5*R0)/2;


        real(8),parameter :: k0(3) = (/0.2772806d0,0.6233148d0,1.237123d0/)
        real(8),parameter :: k1(3) = (/0.9545174d0,0.8116684d0,0.7043832d0/)


!! Sum accross n values
        V = 0.0d0
        DVDR = 0.0d0
        gh_product = 0.0d0
!m2f: dgh_product_dR = zeros(3,3)
        dgh_product_dR = 0.0d0


        RoR = R/rho
        do i = 1,3
          RoR2(i) = RoR(i)**2
        end do


        do k=1,3 ! Sum over R values
!! Potential
          do i = 1,3
            g(i) = 1.0d0 + k0(k)*dexp(-k1(k)*(R(i)-Rm))
            h(i) = tanh(k1(k)*R(i))
          end do
          gh_product(1) = h(2)*g(3)+h(3)*g(2)
          gh_product(2) = h(3)*g(1)+h(1)*g(3)
          gh_product(3) = h(1)*g(2)+h(2)*g(1)

          do i=1,3
            x(i) = (1.0d0 - dexp(-A(k)*RoR(i) - B(k)*RoR2(i)))**n(k)
            V = V-0.5d0*C(k)*(x(i)*gh_product(i)*R(i)**(-n(k)))

    !! Gradient
            dgdR(i) = k0(k)*dexp(-k1(k)*(R(i)-Rm))*(-k1(k))
            dhdR(i) = cosh(k1(k)*R(i))**(-2)*k1(k)
          end do
  ! V = V + -.5*C(k)*sum(x.*(gh_product).*R.**-n(k))
          dgh_product_dR(1,1) = 0.d0
          dgh_product_dR(2,1) = dhdR(2)*g(3)+h(3)*dgdR(2)
          dgh_product_dR(3,1) = dhdR(3)*g(2)+h(2)*dgdR(3)

          dgh_product_dR(1,2) = dhdR(1)*g(3)+h(3)*dgdR(1)
          dgh_product_dR(2,2) = 0.d0
          dgh_product_dR(3,2) = dhdR(3)*g(1)+h(1)*dgdR(3)

          dgh_product_dR(1,3) = dhdR(1)*g(2)+h(2)*dgdR(1)
          dgh_product_dR(2,3) = dhdR(2)*g(1)+h(1)*dgdR(2)
          dgh_product_dR(3,3) = 0.d0

          do i=1,3
            dxdR(i) = n(k)*(1.0d0-dexp(-A(k)*RoR(i)-B(k)*RoR2(i)))**(n(k)-1)
            dxdR(i) = dxdR(i)*dexp(-A(k)*RoR(i)-B(k)*RoR2(i))*(A(k)/rho + 2.0d0*B(k)*R(i)/rho**2)
          end do

          do i=1,3
            DVDR(i) = DVDR(i) + C(k)*dxdR(i)*(-0.5d0*gh_product(i))*R(i)**(-n(k))
            DVDR(i) = DVDR(i) + C(k)*x(i)*(-0.5d0*gh_product(i))*(-n(k))*R(i)**(-n(k)-1)
            do k2 = 1,3
              DVDR(i) = DVDR(i) + C(k)*x(k2)*(-0.5d0)*dgh_product_dR(i,k2)*R(k2)**(-n(k))
            end do
          end do
        end do
!
        VCORR3(1) = V
        VCORR3(2:4) = DVDR
      end function VCORR3


!>
      function VELEC3(R)
        implicit none
! Arguments declarations
        real(8) :: VELEC3(4)
        real(8) :: R(3)
! Variable declarations
        real(8) :: dgdR(3),dhdR(3)
        real(8), dimension(3,3) :: dgh_product_dR
        real(8) :: DVDR(3)
        real(8) :: dxdR(3)
        real(8) :: exp1(3)
        real(8), dimension(3) :: g,h,gh_product,RoR,RoRm,x  !< !m2f: check dim(:)!m
        real(8) :: V
!
        real(8),parameter :: Rm = 2.2818d0
        real(8),parameter :: rho = 8.21801625d0

!! Subroutine Specific Constants

        real(8),parameter :: alpha0 = 25.952776d0
        real(8),parameter :: alpha1 = 1.186793d0
        real(8),parameter :: beta0 = 15.738100d0
        real(8),parameter :: beta1 = 0.097291d0
!
        real(8),parameter :: n = 5.0d0
        integer :: i,k2

        real(8),parameter :: A = alpha0*5.0d0**(-alpha1)
        real(8),parameter :: B = beta0*dexp(-beta1*5.0d0)
! ! rho = (Rm + 2.5*R0)/2;


        real(8),parameter :: C5 = 1.3144d0
        real(8),parameter :: Kp = 3.3522d0


!! Sum accross n values
        DVDR = 0.0d0

!! Potential
        do i = 1,3
          RoRm(i) = R(i)/Rm
          exp1(i) = dexp(-Kp*(R(i)-Rm))
          g(i) = (RoRm(i)**4)*exp1(i)
          h(i) = tanh(Kp*R(i))
        end do



        gh_product(1) = h(2)*g(3)+h(3)*g(2)
        gh_product(2) = h(3)*g(1)+h(1)*g(3)
        gh_product(3) = h(1)*g(2)+h(2)*g(1)

        RoR = R/rho
        V = 0.0d0
        do i = 1,3
          x(i) = (1.0d0 - dexp(-A*RoR(i) - B*RoR(i)**2))**n
          V = V + x(i)*gh_product(i)*R(i)**(-n)
!! Gradient
          dgdR(i) = (4*RoRm(i)**3)/Rm*exp1(i)+ g(i)*(-Kp)
          dhdR(i) = cosh(Kp*R(i))**(-2)*Kp
        end do
        V = -0.5*C5*V


!m2f: dgh_product_dR = zeros(3,3)
        dgh_product_dR(1,1) = 0.d0
        dgh_product_dR(2,1) = dhdR(2)*g(3)+h(3)*dgdR(2)
        dgh_product_dR(3,1) = dhdR(3)*g(2)+h(2)*dgdR(3)

        dgh_product_dR(1,2) = dhdR(1)*g(3)+h(3)*dgdR(1)
        dgh_product_dR(2,2) = 0.d0
        dgh_product_dR(3,2) = dhdR(3)*g(1)+h(1)*dgdR(3)

        dgh_product_dR(1,3) = dhdR(1)*g(2)+h(2)*dgdR(1)
        dgh_product_dR(2,3) = dhdR(2)*g(1)+h(1)*dgdR(2)
        dgh_product_dR(3,3) = 0.d0

        do i=1,3
          dxdR(i) = n*(1.0d0-dexp(-A*RoR(i)-B*RoR(i)**2))**(n-1)
          dxdR(i) = dxdR(i)*dexp(-A*RoR(i)-B*RoR(i)**2)*(A/rho + 2.0d0*B*R(i)/rho**2)
        end do

        do i=1,3
          DVDR(i) = C5*dxdR(i)*(-0.5d0*gh_product(i))*R(i)**(-n)
          DVDR(i) = DVDR(i) + C5*x(i)*(-0.5d0*gh_product(i))*(-n)*R(i)**(-n-1)
          do k2 = 1,3
            DVDR(i) = DVDR(i) + C5*x(k2)*(-0.5d0)*dgh_product_dR(i,k2)*R(k2)**(-n)
          end do
        end do
        VELEC3(1) = V
        VELEC3(2:4) = DVDR
      end function VELEC3
!==================================================================
!                   O2 two body interaction
!==================================================================
      subroutine diapot_int(rin,im,nsurf,v)
      ! two body interaction
      ! im = 1,2,3 O2 bond
      ! r in bohr, v in hartree
        implicit none
      ! constans
      ! real(dp),parameter :: kcal2ev=0.0433641D0
        real(8),parameter :: Rm = 2.2818d0
        real(8),parameter :: D = -0.142912d0
        real(8),parameter :: rho = 8.21801625d0
        real(8),parameter :: a(3) = (/3.6445902d0,3.9281208d0,2.0986665d0/)
        real(8),parameter :: fgamma = 3.3522487d0

        real(8),parameter :: A2(3) = (/ 3.0951333d0,2.1999d0,1.6880714d0/)
        real(8),parameter :: B2(3) = (/ 8.7787894d0,7.2265122d0,5.9487108d0/)
        real(8),parameter :: C2(3) = (/ 15.4d0,235.21994d0,4066.2393d0 /)
        integer,parameter :: n(3) = (/ 6,8,10 /)


      ! contants
        real(8) :: rin(3), r ,v, r2,x,RoR,RoR2
        integer :: im,nsurf,k

        v = 0.0d0
        if (im .le. 3)then
          r = rin(im)
        ! V2EHF
          r2 = r-Rm
          v = v + D*(1.0d0+r2*(a(1)+r2*(a(2)+r2*a(3))))*dexp(-fgamma*r2)

        ! vcorr
          RoR = r/rho
          RoR2 = RoR**2
          do k = 1,3
            x = (1.0d0 - dexp(-A2(k)*RoR - B2(k)*RoR2))**n(k)
            v = v - C2(k)*x*r**(-n(k))
          end do
        else
          stop
        endif
      end subroutine diapot_int



      subroutine prepot
        return
      end

  ! program main
    ! implicit none
    ! real(8),parameter :: a0 = 0.529177d0 ! a0 A = 1 Bohr
    ! real(8),parameter :: Eh = 27.211652d0 !eV
    ! real(8) :: R(3) = (/2.0,5.0,4.0/),E,dVdR(3)
    ! call o3pes(R/a0,E,dVdR)
    ! write(*,*) "E = ",E*Eh
    ! write(*,*) "dVdR = ", dVdR*Eh/a0
!
    ! R(1)=15
    ! R(3) = 20
    ! call o3pes(R/a0,E,dVdR)
    ! write(*,*) "E = ",E*Eh
    ! write(*,*) "dVdR = ", dVdR*Eh/a0
!
    ! R(1)=50
    ! R(2)=3
    ! R(3)=53
    ! Write(*,*) "R=",R(2)
    ! call o3pes(R/a0,E,dVdR)
    ! write(*,*) "E2 = ",E*Eh
!
    ! call diapot_int(R/a0,2,1,E)
    ! write(*,*) "E2 = ",E*Eh
    !
    ! R(1)=50
    ! R(2)=1.2
    ! R(3)=R(1)+R(2)
    ! Write(*,*) "R=",R(2)
    ! call o3pes(R/a0,E,dVdR)
    ! write(*,*) "E2 = ",E*Eh
!
    ! call diapot_int(R/a0,2,1,E)
    ! write(*,*) "E2 = ",E*Eh
    ! end program
