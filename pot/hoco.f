         SUBROUTINE POT(X, E, DX)
C         IMPLICIT DOUBLE PRECISION (A-H,O-Z)
         include 'traj.inc'
         PARAMETER(ND1=100)
         COMMON/POTCM/R(6),DVDR(6)
C         COMMON/PRLIST/T,V,H,TIME,NTZ,NT,ISEED0(8),NC,NX
         common /numstep/ numstep
         DIMENSION X(12),DX(12)
C   Q(i) are the coordinates from VENUS in angstrom, transform to bohr
C   and store for Collins PES

         c(1,1,1) = X(1)
         c(1,1,2) = X(2)
         c(1,1,3) = X(3)
         c(1,2,1) = X(4)
         c(1,2,2) = X(5)
         c(1,2,3) = X(6)
         c(1,3,1) = X(7)
         c(1,3,2) = X(8)
         c(1,3,3) = X(9)
         c(1,4,1) = X(10)
         c(1,4,2) = X(11)
         c(1,4,3) = X(12)

         NC = numstep
         call interpnmj(NC)
          E = (en(1) + 189.054021)
          DVDR(1) = dvr(1,1)
          DVDR(2) = dvr(1,2)
          DVDR(3) = dvr(1,3)
          DVDR(4) = dvr(1,4)
          DVDR(5) = dvr(1,5)
          DVDR(6) = dvr(1,6)

C   The energy units must be transformed for VENUS

C         COLLINSPOT = (VTOT + 189.054021) * 26.254386
C
C   Transform the derivatives of the energy with respect to the internal
C   coordinates to derivatives with respect to the atomic cartesians.
C
C   Transform to R to be able to have the Cartesian ders later

         CALL COORD4(X,12)

         CALL CHAIN4(X,DX,12)

         return
         end

         SUBROUTINE PREPOT
         RETURN
         END  
C
C
         SUBROUTINE coord4(X, N3TM)
C
C   This subprogram transforms the coordinates from atomic cartesians
C   to interatomic distances for a four-body system, ABCD.
C   The integer N3TM, passed by the calling routine, must be greater than 
C   or equal to 12.
C   The convention assumed in this subprogram is as follows:
C                  X(1)  - X(3)  : X, Y, Z of atom A
C                  X(4)  - X(6)  : X, Y, Z of atom B
C                  X(7)  - X(9)  : X, Y, Z of atom C
C                  X(10) - X(12) : X, Y, Z of atom D
C                  R(1) = R(A-B)
C                  R(2) = R(A-C)
C                  R(3) = R(A-D)
C                  R(4) = R(B-C)
C                  R(5) = R(B-D)
C                  R(6) = R(C-D)
C
         IMPLICIT DOUBLE PRECISION (A-H,O-Z)
         COMMON /POTCM/ R(6), DVDR(6)
         DIMENSION X(N3TM)
C
         M = 0
         DO 20 I = 1, 3
               ITIM = 4-I
               K    = 3*(I-1)+1
               L    = K
            DO 10 J = 1, ITIM
                  M = M+1
                  L = L+3
                  R(M) = SQRT((X(K)-X(L))**2 + (X(K+1)-X(L+1))**2 +
     *                        (X(K+2)-X(L+2))**2)
10          CONTINUE
20       CONTINUE
C
         RETURN
         END
C*****

      SUBROUTINE CHAIN4(X,DX,N3TM)
C
C   This subprogram uses the chain rule to calculate the derivatives of
C   the energy with respect to the cartesian coordinates from the derivatives
C   with respect to the internal coordinates for a four-body system, ABCD.
C   The integer N3TM, passed by the calling routine, must be greater than or 
C   equal to 12.
C   The convention assumed in this subprogram is as follows:
C                  R(1) : R(A-B)
C                  R(2) : R(A-C)
C                  R(3) : R(A-D)
C                  R(4) : R(B-C)
C                  R(5) : R(B-D)
C                  R(6) : R(C-D)
C                  X(1)  - X(3)  : X, Y, Z for atom A
C                  X(4)  - X(6)  : X, Y, Z for atom B
C                  X(7)  - X(9)  : X, Y, Z for atom C
C                  X(10) - X(12) : X, Y, Z for atom D
C
         IMPLICIT DOUBLE PRECISION (A-H,O-Z)
         COMMON/POTCM/R(6),DVDR(6)
         DIMENSION X(N3TM),DX(N3TM),Y(6)

C
         DO 10 I = 1, 6
               Y(I) = DVDR(I)/R(I)
10       CONTINUE

C
         DX(1) = (X(1)-X(4))*Y(1)+(X(1)-X(7))*Y(2)+(X(1)-X(10))*Y(3)
         DX(2) = (X(2)-X(5))*Y(1)+(X(2)-X(8))*Y(2)+(X(2)-X(11))*Y(3)
         DX(3) = (X(3)-X(6))*Y(1)+(X(3)-X(9))*Y(2)+(X(3)-X(12))*Y(3)
         DX(4) = (X(4)-X(1))*Y(1)+(X(4)-X(7))*Y(4)+(X(4)-X(10))*Y(5)
         DX(5) = (X(5)-X(2))*Y(1)+(X(5)-X(8))*Y(4)+(X(5)-X(11))*Y(5)
         DX(6) = (X(6)-X(3))*Y(1)+(X(6)-X(9))*Y(4)+(X(6)-X(12))*Y(5)
         DX(7) = (X(7)-X(1))*Y(2)+(X(7)-X(4))*Y(4)+(X(7)-X(10))*Y(6)
         DX(8) = (X(8)-X(2))*Y(2)+(X(8)-X(5))*Y(4)+(X(8)-X(11))*Y(6)
         DX(9) = (X(9)-X(3))*Y(2)+(X(9)-X(6))*Y(4)+(X(9)-X(12))*Y(6)
         DX(10) = (X(10)-X(1))*Y(3)+(X(10)-X(4))*Y(5)+(X(10)-X(7))*Y(6)
         DX(11) = (X(11)-X(2))*Y(3)+(X(11)-X(5))*Y(5)+(X(11)-X(8))*Y(6)
         DX(12) = (X(12)-X(3))*Y(3)+(X(12)-X(6))*Y(5)+(X(12)-X(9))*Y(6)
C
         RETURN
         END

       subroutine cntl
c   this subroutine reads in run control parameters from file on unit 5

      include 'traj.inc'

      open (unit=7,file ='IN_CNT',status='old')

c   read in title of run

      read(7,80) title
   80 format(a80)

c  read in the number 1...7 defining the units

c     read(7,80) comlin
c     read(7,*) nunits

      nunits=5
c---------------------------------------------------------------------
c
c   calculate and scale model parameters
c   units for inputing  masses,energies and lengths:
c   if (nunits) then (mass  energy   length     scale factor) are:
c         1           g/mol kcal/mol angstroms  2.045482828d13
c         2           g/mol kj/mol   angstroms  1.0d13
c         3           g/mol cm-1     angstroms  1.093739383d12
c         4           g/mol eV       angstroms  9.822693571d13
c         5           g/mol hartrees bohr       9.682886569d14
c         6           g/mol aJ           angstroms  9.124087591d13
c         7           g/mol 1.d+05 j/mol angstroms  1.0d15
c
c on output masses, energies, lengths and times are scaled by
c amsc, esc, rsc and tsc (in seconds), respectively and
c must be multiplied by these scale factors to be converted
c to the stated units.

      amsc=1.0d0
      esc=1.0d0
      rsc=1.0d0
      scalef=1.d0
      if(nunits.eq.1)then
        scalef=2.045482828d13
        msunit=' g/mol'
        enunit=' kcal/mol'
        lnunit=' angstroms'
        eukjpm=4.184d0
      elseif(nunits.eq.2)then
        scalef=1.0d13
        msunit=' g/mol'
        enunit=' kJ/mol'
        lnunit=' angstroms'
        eukjpm=1.00d0
      elseif(nunits.eq.3)then
        scalef=1.093739383d12
        msunit=' g/mol'
        enunit=' cm-1'
        lnunit=' angstroms'
        eukjpm=1.196265839d-02
      elseif(nunits.eq.4)then
        scalef=9.822693571d13
        msunit=' g/mol'
        enunit=' eV'
        lnunit=' angstroms'
        eukjpm=96.48530899d0
      elseif(nunits.eq.5)then
        scalef=9.682886569d14
        msunit=' g/mol'
        enunit=' hartree'
        lnunit=' bohr'
        eukjpm=2625.499964d0
      elseif(nunits.eq.6)then
        scalef=9.124087591d13
        msunit=' g/mol'
        enunit=' 0.1382 aJ'
        lnunit=' angstroms'
        eukjpm=83.24897441d0
      elseif(nunits.eq.7)then
        scalef=1.0d14
        msunit=' g/mol'
        enunit=' 1.0d+05 J/mol'
        lnunit=' angstroms'
        eukjpm=1.00d+02
      endif
      tsc=dsqrt(amsc/esc)*rsc/scalef
      tups=tsc*1.0d12

c   Avagadro's number

      avanum=6.0221367d23

c   speed of light in cm s-1 and cm tu-1

      ccmps=2.99792458d10
      ccmptu=ccmps*tsc

c   Planck's constant in J s, kJ/mol s and eu tu

      hjs=6.6260755d-34
      hkjpms=hjs*avanum*1.0d-03
      heutu=(hkjpms/tsc)/eukjpm

c---------------------------------------------------------------------
c   read in time step,no. of traj. step, nprint,no. of molecule etc.

      read(7,80) comlin
      read(7,*) delta,nstep,nprint,nmol

      read(7,80) comlin
      read(7,*) efraga,efragb,etrans

      read(7,80) comlin
      read(7,*)econ

      read(7,80) comlin
      read(7,*) bipmax,distab

c  determine one or twp part weight function is used, ipart=1,2

       read(7,80) comlin
       read(7,*)ipart

      if(ipart.eq.1.or.ipart.eq.2)go to 92
      stop
92    continue

c  read in the parameters defining the weight function, the neighbour
c  list and the number of timesteps between calls to neighbour

       read(7,80) comlin
       read(7,*)lowp,ipow

       read(7,80) comlin
       read(7,*)wtol,outer

       read(7,80) comlin
       read(7,*)neighco,neighci

c  to allow one part neighbour to work

      neighc=neighci

c  read in the sampling fraction used in outp to determine number
c  of trajectory points output in file TOUT, and number of
c  data points chosen

       read(7,80) comlin
       read(7,*) sample

       read(7,80) comlin
       read(7,*) nsel

c  read in energy maximum and minimum with which to screen data

       read(7,80)comlin
       read(7,*) potmax,vmin

c  read in flag to determine whether low energy trajectories are stopped

       read(7,80)comlin
       read(7,*) nlowstop

c read in the multipicitive factor used in radius.f, confidrad.f

       read(7,80)comlin
       read(7,*) nneigh

c  Enter the energy error tolerance for confidrad.f

       read(7,80)comlin
       read(7,*) sigma

c calculate pi, conversion factors

      pi=2.d0*dacos(0.d0)
      todeg=45.d0/atan(1.d0)
      toang=0.52917706d0
      sq2=dsqrt(2.d0)

c  scale atomic masses

      do 150 i=1,natom
        amas(i)=amas(i)/amsc
  150 continue

      close(unit=7)

      return
      end

c  --------------------------------------------------------------

      subroutine readz

c  modified jan 7 1996 by kt 

      include 'traj.inc'
 
c  given that we need all the atom-atom bonds, we just assign the indirect
c  addresses of the bonds, rather than the traditional read from ZMA

        k=1
        do i=1,natom-1
        do j=i+1,natom

           mb(k)=i
           nb(k)=j
           k=k+1

        enddo
        enddo

c   read fragment atom numbers

      open (unit=7,file='IN_ZMA',status='old')

80    format(a80)
81    format(1x,a79)

      read(7,80)comlin

      read(7,*)nfraga,nfragb

      read(7,80)comlin

      if(nfraga.gt.0)then
      read(7,*)(ifraga(i),i=1,nfraga)
      endif

      read(7,80)comlin

      if(nfragb.gt.0)then
      read(7,*)(ifragb(i),i=1,nfragb)
      endif
      if (nfraga .le. 1) go to 3100

      nbfraga=0
      do 3000 i=1,nbond
	do 2900 j=1,nfraga
	  do 2800 k=1,nfraga
            if ((mb(i).eq.ifraga(j)).and.(nb(i).eq.ifraga(k))) then
              nbfraga=nbfraga+1
              ibfraga(nbfraga)=i
            endif
 2800     continue 
 2900   continue
 3000 continue
 3100 continue
      if (nfragb .le. 1) go to 4100

      nbfragb=0
      do 4000 i=1,nbond
	do 3900 j=1,nfragb
	  do 3800 k=1,nfragb
            if ((mb(i).eq.ifragb(j)).and.(nb(i).eq.ifragb(k))) then
              nbfragb=nbfragb+1
              ibfragb(nbfragb)=i
              endif
 3800     continue 
 3900   continue
 4000 continue

 4100 continue

      close(unit=7)

      return
      end

      subroutine confidrad

      include 'traj.inc'

       dimension z(nintm),dei(nintm),dtdrm(nbondm),
     .           delrv(nbondm),dtdr(nbondm),asb(nbondm)
     .           ,bigsort(ndatam)

      sigma = 0.0002
      sigma2=sigma*sigma

      open(unit=3,file='CON',status='unknown')

      do j=1,ndata


      rcut=rads(j)+1.d-12

       sum=0.d0
       do i=1,nbond
       sumb(j,i)=0.d0
       enddo

      do m=1,ndata
      if(m.eq.j)go to 10

      do n=1,ngroup

       vt=0.d0

       do i=1,nbond
        t=rda(m,ip(i,n))-rda(j,i)
       vt=t*t+vt
       enddo
c  rda is a reciprocal bondlength

c  only consider the first nneigh neighbours



      if(vt.gt.rcut)go to 222


c the Taylor expansion

      do i=1,nint
         z(i)=0.d0
         do l=1,nbond
            z(i) = ut(j,i,ip(l,1))*rda(m,ip(l,n)) + z(i)
         enddo
          z(i)=(z(i)-zda(j,i))
      enddo

      taydif=v0(m)-v0(j)
      do i=1,nint
       taydif=taydif-z(i)*(v1(j,i)+0.5d0*v2(j,i)*z(i))
	 dei(i) = z(i)*v2(j,i) + v1(j,i)
      enddo

      sum=sum+taydif**2/vt**3


c  transform derivatives of taylor poly wrt normal local coords into 
c  derivs wrt inverse bondlengths

      do i=1,nbond
            dtdr(i)=0.d0
            dtdrm(i)=0.d0
            do l=1,nint

               dtdr(i)=dei(l)*ut(j,l,ip(i,1)) + dtdr(i)
               dtdrm(i)=v1(m,l)*ut(m,l,ip(i,n)) + dtdrm(i)

            enddo
      enddo

      do i=1,nbond
      delrv(i)=(rda(m,ip(i,n))-rda(j,i))*(dtdrm(i)-dtdr(i))
      sumb(j,i)=sumb(j,i)+delrv(i)**2/vt**3
      enddo


c end the ngroup loop
222   continue
      enddo

10    continue

c end the m ndata loop
      enddo

c     do i=1,nbond
c     if(sumb(j,i).lt.1.d-15)then
c     write(6,*)j,i,sumb(j,i)
c     stop
c     endif
c     enddo

      rcon=(nneigh*sigma2/sum)**(1.d0/3.d0)

      do i=1,nbond
      sumb(j,i)=(nneigh*sigma2/sumb(j,i))**(1.d0/3.d0)
      enddo

c  test to see if some sumb have been wiildly extrapolated

120    format(9e8.2)


c  replace rads by rcon
      if(ipart.eq.2)then
      rads(j)=rcon
      else
      rads(j)=1.d0
      endif

c end the j ndata loop
      enddo

c      stop

c  try to eliminate abnormally large radii and sumb

c  first evaluate averages

      ard=0.d0
      do m=1,ndata
      ard=ard+rads(m)
      enddo
      ard=ard/ndata

      do n=1,nbond
      asb(n)=0.d0
      do m=1,ndata
      asb(n)=asb(n)+sumb(m,n)
      enddo
      asb(n)=asb(n)/ndata
      enddo

c     do m=1,ndata
c     if(rads(m).gt.1.5d0*ard)rads(m)=1.5d0*ard
c     do n=1,nbond
c     if(sumb(m,n).gt.1.5d0*asb(n))sumb(m,n)=1.5d0*asb(n)
c     enddo
c     enddo

c      do n=1,nbond
c      write(6,*) n,asb(n)
c      enddo

      ndt2=ndata/2

      do n=1,nbond

      do k=1,ndata
       bigsort(k)=sumb(k,n)
      enddo
      call sort(ndata,bigsort)
      do k=1,ndata
       if(sumb(k,n).gt.2.5d0*bigsort(ndt2))sumb(k,n)=2.5d0*bigsort(ndt2)
      enddo

      enddo

c  write rads and sumb to CON

      do j=1,ndata
      write(3,*)rads(j),(sumb(j,n),n=1,nbond)
      enddo

      close(unit=3)

234   format(15e9.2)
c      stop
7763  continue
      return
      end

      subroutine dvdi(j)

c  evaluate the energy and gradients

      include 'traj.inc'

      dimension dvt(maxf,nbondm),ei(maxf),dei(maxf,nintm),vt1(maxf),
     .        rinv(nbondm),sumdvt(nmax,nbondm),vt(maxf),
     .        z(maxf,nintm),dtdr(maxf,nbondm)

c  zero those who need it

      en(j)=0.d0
      totsum=0.d0

      do i=1,nbond
         dvr(j,i)=0.d0
         sumdvt(j,i)=0.d0
         dvt(maxf,i)=0.0d0
      enddo

      do k=1,nforc
         vt(k)=0.d0
         vt1(k)=0.d0
      enddo
      vt(maxf)=0.0d0

c  we will need to have some quantities for the gradients
c  of the weights
c  Note an approx here, due to neglect of small weights

      ip2=ipow*2

c  calc the raw wts and totsum

      do i=1,nbond
      do k=1,nforc
         temp= 1.d0/rr(j,i)-1.d0/rda(mda(k),ip(i,nda(k)))
c        temp= rr(j,i)-rda(mda(k),ip(i,nda(k)))
         vt1(k)= temp*temp + vt1(k)
      enddo
      enddo

      do k=1,nforc
         vt(k)=1.d0/vt1(k)**ipow
      enddo

      do k=1,nforc
         totsum=totsum+vt(k)
      enddo

c  calculate the relative weights
  
      do k=1,nforc
         wt(k)=vt(k)/totsum
      enddo

c  calc the derivatives of the raw weights v

      do k=1,nforc
         temp=ip2*wt(k)/vt1(k)
         do i=1,nbond
            dvt(k,i)=temp*(1.d0/rr(j,i)-1.d0/rda(mda(k),ip(i,nda(k))))/
     &   rr(j,i)**2
         enddo
      enddo

c      do k=1,nforc
c        temp=-ip2*wt(k)/vt1(k)
c        do i=1,nbond
c           dvt(k,i)=temp*(rr(j,i)-rda(mda(k),ip(i,nda(k))))
c        enddo
c      enddo

      do i=1,nbond
      do k=1,nforc
         sumdvt(j,i)=sumdvt(j,i)+dvt(k,i)
      enddo
      enddo

c  calc the derivatives of the relative weights w

      do i=1,nbond
      do k=1,nforc
         dvt(k,i)= -wt(k)*sumdvt(j,i) + dvt(k,i)
      enddo
      enddo

c  check that the sum over weights vanishes

c      write(6,*)
c      write(6,*) '  weights : '
c      test=0.d0
c      do k=1,nforc
c         test=wt(k)+test
c         write(6,*) k,wt(k)
c      enddo
c      write(6,*) ' sum of weights = ',test 

c      do i=1,nbond
c         sumwt=0.d0
c         do k=1,nforc
c            sumwt=sumwt+dvt(k,i)
c         enddo
c         write(6,*)i,sumwt
c      enddo

c  construct the normal local coords for each neighbour
c  recalling that intern calculates bond lengths NOT inverses

      do i=1,nbond
         rinv(i)=1.d0/rr(j,i)
      enddo

c  !ocl vi(k)
      do k=1,nforc
      do i=1,nint
         z(k,i)=0.d0
         do l=1,nbond
            z(k,i) = ut(mda(k),i,ip(l,nda(k)))*rinv(l) + z(k,i)
         enddo
c         z(k,i)=tanh(z(k,i)-zda(mda(k),i))
          z(k,i)=(z(k,i)-zda(mda(k),i))
      enddo
      enddo

c  degub 

c     do k=1,nforc
c        write(6,*)
c        write(6,*) ' bond lengths ',k,imol(k)
c        write(6,1971) (rr(imol(k),i),i=1,nbond)
c        write(6,*) ' normal local coords z(k,nint)',k
c        write(6,1971) (z(k,i),i=1,nint)
c        write(6,*) ' data local coords zda',k
c        write(6,1971) (zda(mda(k),i),i=1,nint)
c        write(6,*) ' data bondlengths rda',k
c        write(6,1971) (rda(mda(k),ip(i,nda(k))),i=1,nbond)
c     enddo
1971  format(2x,6g11.4)

c  calculate the potential energy: scalar term 

      do k=1,nforc
         ei(k)=v0(mda(k))
      enddo

c  linear term in energy plus diagonal part of quadratic term
c  first term in derivative of taylor poly wrt local coords

      do i=1,nint
      do k=1,nforc

	 ei(k) = z(k,i)*v1(mda(k),i) 
     .   + 0.5d0*z(k,i)**2*v2(mda(k),i) + ei(k)

	 dei(k,i) = z(k,i)*v2(mda(k),i) + v1(mda(k),i)

      enddo
      enddo

c  the O(n^2) loop doesn't apply because we diagonalised d2v!!!

c  transform derivatives of taylor poly wrt normal local coords into 
c  derivs wrt bondlengths NOT inverses

c  !ocl vi(k)
      do i=1,nbond
         t=-rinv(i)**2
         do k=1,nforc
            dtdr(k,i)=0.d0
            do l=1,nint

                sechsq=1.d0
c               sechsq=1.d0 - z(k,l)**2

               dtdr(k,i)=sechsq*dei(k,l)*ut(mda(k),l,ip(i,nda(k))) +
     &  dtdr(k,i)

            enddo
            dtdr(k,i)=dtdr(k,i)*t
         enddo
      enddo

c  scale by relative weights, and their derivatives

      do k=1,nforc
         en(j)=ei(k)*wt(k)+en(j)
      enddo

      do i=1,nbond
      do k=1,nforc
         dvr(j,i)=ei(k)*dvt(k,i)+wt(k)*dtdr(k,i)+dvr(j,i)
      enddo
      enddo

c  smoothing attempt: <\delta T^2>

      rms=0.d0
      grms=0.d0
      do k=1,nforc
         rms=wt(k)*(ei(k)-en(j))**2 + rms
         do i=1,nbond
         grms=grms+wt(k)*(dtdr(k,i)-dvr(j,i))**2
         enddo
      enddo
      rms=sqrt(rms)
      totrms=totrms+rms

c  sum over interactions for each traj

c     do k=1,nforc
c        en(j)=en(j)+ei(k)
c     enddo

c     do i=1,nbond
c     do k=1,nforc
c        dvr(j,i)=dvr(j,i)+dtdr(k,i)
c     enddo
c     enddo

c  look out for loony geometries

      if (nlowstop.eq.0) goto 1975

      ect=vmin-0.0005d0

      if (en(j).lt.ect) then
c         if (nloop.gt.1) then
c            do n=1,natom
c               write(8,*)(c(j,n,i),i=1,3)
c            enddo
c            write(8,*)en(j),rms,totsum
c         endif
         nfin(j)=3
         nstop=2
      endif

1975  continue
c      write(6,*) 'From DVDI:'
c      write(6,*) en(1)
c      write(6,*) (dvr(1,i),i=1,6)

      return
      end


      subroutine dvdir(j)

c  evaluate the energy and gradients

      include 'traj.inc'

      dimension dvt(maxf,nbondm),ei(maxf),dei(maxf,nintm),vt1(maxf),
     .         rinv(nbondm),sumdvt(nmax,nbondm),vt(maxf),
     .         z(maxf,nintm),dtdr(maxf,nbondm),tsb(nbondm)


      dimension zed2(maxf), zedp(maxf), zed3(maxf), vtone(maxf)

c this version of dvdi uses a new two part primitive weight with a
c confidence "radius" rads(k) or sumb for each data point.


c  zero those who need it

      en(j)=0.d0
      totsum=0.d0

      do i=1,nbond
         dvr(j,i)=0.d0
         sumdvt(j,i)=0.d0
         dvt(maxf,i)=0.0d0
         rinv(i)=1.d0/rr(j,i)
      enddo
     
c      write(6,*) 'nforc=',nforc

      do k=1,nforc
         vt(k)=0.d0
         vt1(k)=0.d0
      enddo

      ip2=ipow*2

c  calc the raw wts and totsum

      do i=1,nbond
      do k=1,nforc
         i1=mda(k)
         i2=ip(i,nda(k))
         temp= rinv(i)-rda(i1,i2)
         vt1(k)= temp*temp/sumb(i1,i2) + vt1(k)
         vtone(k)=temp*temp + vtone(k)
      enddo
      enddo

      do k=1,nforc
         zedp(k)=vt1(k)**ipow
         zed2(k)=vt1(k)**lowp
         zed3(k)=vtone(k)**ipow
         vt(k)=1.d0/(zed2(k)+zedp(k))
      enddo

      do k=1,nforc
         totsum=totsum+vt(k)
      enddo

c  calculate the relative weights
  
      do k=1,nforc
         wt(k)=vt(k)/totsum
      enddo

c  calc the derivatives of the raw weights v

      do k=1,nforc
         temp=2.d0*wt(k)*vt(k)*(lowp*zed2(k)+ipow*zedp(k))/vt1(k)
         i1=mda(k)
         do i=1,nbond
         i2=ip(i,nda(k))
            dvt(k,i)=temp*(rinv(i)-rda(i1,i2))*rinv(i)**2
     .               /sumb(i1,i2)
         enddo
      enddo

c dvt is actually the derivative of the primitive weight, divided by
c the sum of the primitive weights

      do i=1,nbond
      do k=1,nforc
         sumdvt(j,i)=sumdvt(j,i)+dvt(k,i)
      enddo
      enddo

c  calc the derivatives of the relative weights w

      do i=1,nbond
      do k=1,nforc
         dvt(k,i)= -wt(k)*sumdvt(j,i) + dvt(k,i)
      enddo
      enddo

c  check that the sum over weights vanishes

c      write(6,*)
c      write(6,*) '  weights : '
c      test=0.d0
c      do k=1,nforc
c         test=wt(k)+test
c         write(6,*) k,wt(k)
c      enddo
c      write(6,*) ' sum of weights = ',test 

c      do i=1,nbond
c         sumwt=0.d0
c         do k=1,nforc
c            sumwt=sumwt+dvt(k,i)
c         enddo
c         write(6,*)i,sumwt
c      enddo

c  construct the normal local coords for each neighbour
c  recalling that intern calculates bond lengths NOT inverses

      do k=1,nforc
      k1=mda(k)
      do i=1,nint
         z(k,i)=0.d0
         do l=1,nbond
         l1=ip(l,nda(k))
             z(k,i) = ut(k1,i,l1)*rinv(l) + z(k,i)
         enddo

         z(k,i)=z(k,i)-zda(k1,i)
      enddo
      enddo

c  degub 

c     do k=1,nforc
c        write(6,*)
c        write(6,*) ' bond lengths ',k,imol(k)
c        write(6,1971) (rr(imol(k),i),i=1,nbond)
c        write(6,*) ' normal local coords z(k,nint)',k
c        write(6,1971) (z(k,i),i=1,nint)
c        write(6,*) ' data local coords zda',k
c        write(6,1971) (zda(mda(k),i),i=1,nint)
c        write(6,*) ' data bondlengths rda',k
c        write(6,1971) (rda(mda(k),ip(i,nda(k))),i=1,nbond)
c     enddo
1971  format(2x,6g11.4)

c  calculate the potential energy: scalar term 

      do k=1,nforc
         ei(k)=v0(mda(k))
      enddo

c  linear term in energy plus diagonal part of quadratic term
c  first term in derivative of taylor poly wrt local coords

      do k=1,nforc
         k1=mda(k)
      do i=1,nint
	 ei(k) = z(k,i)*v1(k1,i) 
     .   + 0.5d0*v2(k1,i)*z(k,i)**2 + ei(k)
	 dei(k,i) = z(k,i)*v2(k1,i) + v1(k1,i)

      enddo
      enddo

c  the O(n^2) loop doesn't apply because we diagonalised d2v!!!

c  transform derivatives of taylor poly wrt normal local coords into 
c  derivs wrt bondlengths NOT inverses

!ocl vi(k)
      do i=1,nbond
         t=-rinv(i)**2
         do k=1,nforc
            dtdr(k,i)=0.d0
            k1=mda(k)
            i1=ip(i,nda(k))
            do l=1,nint

               dtdr(k,i)=dei(k,l)*ut(k1,l,i1) + dtdr(k,i)

            enddo
            dtdr(k,i)=dtdr(k,i)*t
         enddo
      enddo

c  sum over neighbours

      do k=1,nforc
         en(j)=ei(k)*wt(k)+en(j)
      enddo

      do i=1,nbond
      do k=1,nforc
         dvr(j,i)=ei(k)*dvt(k,i)+wt(k)*dtdr(k,i)+dvr(j,i)
      enddo
      enddo

c      write(6,*) 'From DVDIR:'
c      write(6,*) en(1)
c      write(6,*) (dvr(1,i),i=1,6)
c variance in the energy

      rms=0.d0

      do k=1,nforc
         rms=wt(k)*(ei(k)-en(j))**2 + rms
      enddo

      rms=dsqrt(rms)

c  temporarily (10/2/98) evaluate the rms using only the one-part
c  weight function

c     totsum1=0.d0
c     do k=1,nforc
c     totsum1=1.d0/zed3(k)+totsum1
c     enddo

c     do k=1,nforc
c      rms=rms+(1.d0/zed3(k)/totsum1)*(ei(k)-en(j))**2
c     enddo

c     rms=dsqrt(rms)

c variance in the gradients

c     tdv=0.d0
c     do i=1,nbond
c        tvg(i)=0.d0
c        tdv=dvr(j,i)**2 + tdv
c     enddo

c      grms=0.d0
c      do k=1,nforc
c      do i=1,nbond
c         t=dtdr(k,i)-dvr(j,i)
c         t=t*t
c        tvg(i)=wt(k)*t + tvg(i)
c         grms=wt(k)*t + grms
c      enddo
c      enddo

c      grms=dsqrt(grms)
     

c  look out for loony geometries

      if (nlowstop.eq.0) goto 1975

      ect=vmin-0.0005d0

      if (en(j).lt.ect) then
         if (nloop.gt.1) then
      sumr=0.0d0
      do n=1,nbond
      tsb(n)=0.d0
      enddo
      do k=1,nforc
      sumr=sumr+wt(k)*rads(mda(k))
      do n=1,nbond
      tsb(n)=tsb(n)+wt(k)*sumb(mda(k),ip(n,nda(k)))
      enddo
      enddo

         endif
         nfin(j)=3
         nstop=2
      endif

1975  continue

      return
      end


      subroutine intern(j)
 
c  calculates bondlengths and their derivatives - NOT inverses !!!!!!!!

      include 'traj.inc'

c  bond lengths

      do i=1,nbond
         rr(j,i)=sqrt((c(j,mb(i),1)-c(j,nb(i),1))**2
     .              +(c(j,mb(i),2)-c(j,nb(i),2))**2
     .              +(c(j,mb(i),3)-c(j,nb(i),3))**2)
      enddo

c  bond length derivatives

      do i=1,nbond
         dr(j,i,mb(i),1)=(c(j,mb(i),1)-c(j,nb(i),1))/rr(j,i)
         dr(j,i,mb(i),2)=(c(j,mb(i),2)-c(j,nb(i),2))/rr(j,i)
         dr(j,i,mb(i),3)=(c(j,mb(i),3)-c(j,nb(i),3))/rr(j,i)
         dr(j,i,nb(i),1)=-dr(j,i,mb(i),1)
         dr(j,i,nb(i),2)=-dr(j,i,mb(i),2)
         dr(j,i,nb(i),3)=-dr(j,i,mb(i),3)
      enddo

      return
      end
      subroutine makebp

      include 'traj.inc'

      character*80 icomm

c  read in the atomic permutations into the nperm array

       open(unit=8, file='IN_ATOMPERMS',status='old')


cc read in a header

       read(8,80)icomm

80    format(a80)

c  read in a comment line

       read(8,80)icomm

c  read in the order of the group

       read(8,*)ngroup

       nstore=ngroup

c  read in the atomic perms

       do i=1,ngroup

c  read in a comment line or separator
       read(8,80)icomm
       do k=1,natom
       read(8,*)idummy,nperm(i,k)
       enddo
c  idummy will be equal to k

       enddo

       close(unit=8)

c  now set up the ip array of bond perms by looking at nperm

      do n=1,ngroup

      ic=0
      do i=1,natom-1
      do j=i+1,natom
      ic=ic+1
      do k=1,nbond
      if(mb(k).eq.nperm(n,i).and.nb(k).eq.nperm(n,j))ip(k,n)=ic
      if(nb(k).eq.nperm(n,i).and.mb(k).eq.nperm(n,j))ip(k,n)=ic
      enddo

      enddo
      enddo

      enddo

      end
C

      subroutine radius

      include 'traj.inc'

c  this subroutine looks at the data set and finds the "distance"
c  of the closest data point to each data point. The radius of 
c  "confidence" for each data point is set to ntimes times this 
c  closest distance

      dimension dists(ndatam*ngroupm),
     .          smdis(ndatam*ngroupm),z(nintm)

      dimension vt(ndatam,ngroupm),vt1(ndatam,ngroupm),mdat(maxf),
     .       ndat(maxf)
     .       ,wdat(maxf),dwt(maxf,nbondm),sumdvt(nbondm),dei(nintm)

      dimension dtdr(nbondm),dvkr(nbondm),grad(nbondm)

c  nneigh is now the number of neighbours in the POT file
c  who are to lie within rads (this is how rads is determined).

c  check that nneigh is not too big

c     call lisym(6,'-')

      do k=1,ndata

         dmin=1.d+06
         totsum=0.d0
         ic=0

         do m=1,ndata
            if (m.eq.k) go to 1
            do n=1,ngroup

               sum=0.d0

               do i=1,nbond
                  t=rda(k,i)-rda(m,ip(i,n))
                  t2=t*t
                  sum=t2+sum
               enddo

               ic=ic+1
               dists(ic)=sum

c              totsum=totsum+1.d0/sum**lowp

               if (sum.lt.dmin) then
                  dmin=sum
               endif

            enddo
1           continue
         enddo

c guard against identical data points

         if (dmin.lt.1.d-10) dmin=1.d-4

         dmin2=2.d0*dmin
50       continue

c find all the distances less than 2*dmin

         ndist=0

         do i=1,ic
            if (dists(i).le.dmin2) then
               ndist=ndist+1
               smdis(ndist)=dists(i)
            endif
         enddo

         if (ndist.lt.(nneigh+1)) then
            dmin2=dmin2*2.d0
c           write(11,500) k,ndist
            go to 50
         endif

500   format(2x,'increase dmin2 for k = ',i5,2x,i10)

c  we now have a set of at least nneigh small distances
c  we sort them

         call sort(ndist,smdis)

         rads(k)=smdis(nneigh)

c  temp patch 12/8/97

c         rads(k)=rads(k)/10.d0

c end of patch 12/8/97

c       write(6,3) k, rads(k)
3        format(2x,i5,g14.5)

      enddo

      avrads=0.d0
      do k=1,ndata
         avrads=avrads+rads(k)
      enddo
      avrads=avrads/float(ndata)

c temp patch 3/9/97 to put rads very small

c      do k=1,ndata
c       rads(k)=avrads/1000.d0
c      enddo

c fixrad ?   
c     av=0.04d0

c     do k=1,ndata
c        if (rads(k).lt.0.025d0) rads(k)=0.025d0
c        rads(k)=av
c     enddo

c     write(6,*)
c     write(6,2) avrads
c     stop
      return

c ---------------- now we interpolate -----------------------------

c  build neighbour list for each data point
c  first calculate raw weights and totsum

      enmx=0.d0
      gdmx=0.d0
      averr=0.d0
      avgrad=0.d0
      avneigh=0.d0

      do k=104,ndata

         totsum=0.d0
         do m=1,ndata
            if (m.eq.k) goto 27
            do n=1,ngroup
         
               sum=0.d0

               do i=1,nbond
                  t=rda(k,i)-rda(m,ip(i,n))
                  sum=t*t + sum
               enddo
               sum=sum/rads(m)

               vt1(m,n)=sum

               vt(m,n)=1.d0/(sum**lowp + sum**ipow)

               totsum = totsum + vt(m,n)

            enddo
27          continue
         enddo

c    calculate neighbour list 

         nforc=0
         do m=1,ndata
            if (m.eq.k) goto 29
            do n=1,ngroup
 
               wt1=vt(m,n)/totsum

               if (wt1.gt.wtol) then

                  nforc=nforc+1
                  mdat(nforc)=m 
                  ndat(nforc)=n

               endif

            enddo
29          continue
         enddo

         avneigh=avneigh + nforc

c  recalculate totsum for neighbours

         totsum=0.d0
         do j=1,nforc
            totsum=totsum + vt(mdat(j),ndat(j))
         enddo

c  calculate relative weights 

         do j=1,nforc
            wdat(j) = vt(mdat(j),ndat(j))/totsum
         enddo

c  derivatives of relative weight wrt bond lengths
          
         do i=1,nbond
            sumdvt(i)=0.d0
         enddo
 
         do j=1,nforc
            tv=vt1(mdat(j),ndat(j))
            z1=tv**ipow
            z2=tv**lowp
            vtt=1.d0/(z1 + z2)
            t=2.d0*wdat(j)*vtt*(lowp*z2+ipow*z1)/tv/rads(mdat(j))
            do i=1,nbond
               dwt(j,i) = t*(rda(k,i)-rda(mdat(j),ip(i,ndat(j))))*
     &         rda(k,i)**2
               sumdvt(i) = sumdvt(i) + dwt(j,i)
            enddo
         enddo

         do j=1,nforc
         do i=1,nbond
            dwt(j,i)= -wdat(j)*sumdvt(i) + dwt(j,i)
         enddo
         enddo

c    now we have a neighbour list, calculate interpolated potential

         shep=0.d0

         do i=1,nbond
            dvkr(i)=0.d0
         enddo

         do j=1,nforc

c    local coords
               
            do i=1,nint
               z(i)=0.d0
               do l=1,nbond
c                 t = rda(k,l) - rda(mdat(j),ip(l,ndat(j)))
                  z(i) = ut(mdat(j),i,ip(l,ndat(j)))*rda(k,l) + z(i)
               enddo
               z(i) = z(i) - zda(mdat(j),i) 
            enddo

c    taylor polys and 1st derivatives thereof

            ei=v0(mdat(j))

            do i=1,nint
               ei = z(i)*v1(mdat(j),i) + 0.5d0*v2(mdat(j),i)*z(i)**2 +
     &         ei
               dei(i) = v1(mdat(j),i) + v2(mdat(j),i)*z(i)
            enddo

c    shephard interpolation of energy
   
            shep = wdat(j)*ei + shep

c    transformation of d(ei)/dz to d(ei)/dr

            do i=1,nbond
               dtdr(i)=0.d0
               do l=1,nint
                  dtdr(i) = dei(l)*ut(mdat(j),l,ip(i,ndat(j))) + dtdr(i)
               enddo 
               dtdr(i)=-dtdr(i)*rda(k,i)**2
            enddo

c    derivative of interpolated potential at rda(k)

            do i=1,nbond
               dvkr(i) = ei*dwt(j,i) + wdat(j)*dtdr(i) + dvkr(i)
            enddo

c  close loop over neighbours

         enddo

c  energy error at rda(k) - in kJ/mol

         err = 100.d0*dabs(v0(k)-shep) 
         averr = averr + err

         if (err.gt.enmx) enmx=err

c  transform dv/dz(k) -> dv/dr|_k

         do i=1,nbond
            grad(i)=0.d0
            do j=1,nint
               grad(i) = v1(k,j)*ut(k,j,i) + grad(i)
            enddo
            grad(i) = -grad(i)*rda(k,i)**2
         enddo

c  gradient error at rda(k)

         gg=0.d0
         t1=0.d0
         t2=0.d0
         do i=1,nbond
            t1=grad(i)**2 + t1
            t2=dvkr(i)**2 + t2
            gg=(grad(i)-dvkr(i))**2 + gg
         enddo
         gg=100.d0*2.d0*dsqrt(gg)/(dsqrt(t1) + dsqrt(t2))

         avgrad = avgrad + gg

         if (gg.gt.gdmx) gdmx=gg
            
c close outer loop over data points

      enddo

      denom=1.d0/float(ndata-1)
      averr=averr*denom
      avgrad=avgrad*denom
      avneigh=avneigh*denom

      return
      end
      real*8 function usran(ir)
c
c   this subroutine generates random values between 0.0 and 1.0 using
c   an integer seed
c   it is based on the imsl routine ggubs.
c
c   real*8 version
c
      implicit real*8 (a-h,o-z)
      parameter(da=16807.d0,db=2147483647.d0,dc=2147483648.d0)
      ir=abs(mod(da*ir,db)+0.5d0)
      usran=dfloat(ir)/dc
      return
      end

	subroutine readccm
	include 'traj.inc'

	character*80 icomm, ielements, ialpha

        open (unit=7,file='IN_SYSTEM',status='old')

c  read comment ; a header for the SYSTEM file

        read(7,80)icomm
80      format(a80)

c  read in the actual number of atoms

        read(7,80)icomm

        read(7,*) natom

        nbond=natom*(natom-1)/2

        n3=3*natom

        nint=n3-6
        if(natom.eq.2)nint=1

c  read in the element labels for the atoms

        read(7,80)icomm

        read(7,80) ielements

c extract the elemental labels from ielements

        ialpha=ielements

        do i=1,natom-1
        length=len(ialpha)
        ifin=index(ialpha,',')
         if(ifin.gt.3)then
c         write(6,*)'an atomic label in SYSTEM has too many characters'
         stop
         endif
        lab(i)=ialpha(1:ifin-1)
        ialpha=ialpha(ifin+1:length)
        enddo
        ifin=index(ialpha,' ')
        lab(natom)=ialpha(1:ifin-1)

c        length=len(ialpha)
c         if(length.gt.2)then
c         write(6,*)'last atomic label in SYSTEM has too many characters'
c         stop
c         endif
c       lab(natom)=ialpha(1:length)

c  read in the atomic masses

        read(7,80)icomm
        read(7,*)(amas(i),i=1,natom)

c  read in the cutoff lengths rmin(i,j)

        read(7,80)icomm
        read(7,80)icomm
        do i=1,natom-1
        do j=i+1,natom
        read(7,*) rmin(i,j)
        enddo
        enddo


        close (unit=7)

        return
        end

      subroutine readcon

      include 'traj.inc'

       dimension z(nintm),dei(nintm),dtdrm(nbondm),
     .           delrv(nbondm),dtdr(nbondm),asb(nbondm)
     .           , rdifmax(nbondm)

      open(unit=16,file='CON',status='old')

      do j=1,ndata
      read(16,*)rads(j),(sumb(j,n),n=1,nbond)
      enddo

      close(unit=16)

      return
      end

      subroutine readt

      include 'traj.inc'

      open (unit=7,file='POT',status='unknown')

c   read comment/header

      read(7,80)comlin
   80 format(a80)

c  read in the bond lengths, the matrix defining the normal local coords, 
c  the potential energy, the gradients and diagonal force constants (wrt to 
c  the normal local coords) at all of the Ndata points.

c  temporarily store vmin which was read in cntl

      vminstore=vmin

      j=1
10    continue
         read(7,80,end=20)comlin

         read(7,*) (rda(j,k),k=1,nbond)
c        write(6,1971) (rda(j,k),k=1,nbond)

         do i=1,nint
            read(7,*) (ut(j,i,k),k=1,nbond)
         enddo

         read(7,*) v0(j)

c  find the minimum potential in the original data set

         if (j.eq.1) vmin=v0(j)
         if (v0(j).lt.vmin) vmin=v0(j)

         read(7,*) (v1(j,k),k=1,nint)
    
         read(7,*) (v2(j,k),k=1,nint)

         if (v0(j).gt.potmax) goto 10
            
         j=j+1
         go to 10

20    continue
1971  format(15(1x,g10.5))
    
      ndata=j-1

c set vmin to be the minimum of the readin value in cntl and the
c minimum found in the POT file

      if(vminstore.lt.vmin)vmin=vminstore

      close (unit=7)

c  construct the local coords for each data point
c  remembering that rda are read in as bond lengths NOT inverses 

      do n=1,ndata
      do j=1,nint
         zda(n,j)=0.d0
         do k=1,nbond
            zda(n,j)=ut(n,j,k)/rda(n,k) + zda(n,j)
         enddo
      enddo
      enddo

      if(ipart.eq.2)then

c  invert data bonds once and for all

      do n=1,ndata
      do i=1,nbond
         rda(n,i)=1.d0/rda(n,i)
      enddo
      enddo

      endif

c debug

c     do n=1,ndata
c        write(6,*) ' '
c        write(6,1812) (zda(n,i),i=1,6)
c        write(6,1812) (zda(n,i),i=7,12)
c     enddo

1812  format(2x,6g12.5)

c     stop

      return
      end


      SUBROUTINE SORT(N,RA)
      implicit real*8(a-h,o-z)
      DIMENSION RA(N)
      L=N/2+1
      IR=N
10    CONTINUE
        IF(L.GT.1)THEN
          L=L-1
          RRA=RA(L)
        ELSE
          RRA=RA(IR)
          RA(IR)=RA(1)
          IR=IR-1
          IF(IR.EQ.1)THEN
            RA(1)=RRA
            RETURN
          ENDIF
        ENDIF
        I=L
        J=L+L
20      IF(J.LE.IR)THEN
          IF(J.LT.IR)THEN
            IF(RA(J).LT.RA(J+1))J=J+1
          ENDIF
          IF(RRA.LT.RA(J))THEN
            RA(I)=RA(J)
            I=J
            J=J+J
          ELSE
            J=IR+1
          ENDIF
        GO TO 20
        ENDIF
        RA(I)=RRA
      GO TO 10
      END

      subroutine initializecollins

c  this program runs a potential energy sufrace interpolation scheme
c  the procedure is as follows:

c  	*initial conditions are chosen for the trajectories via a Markov
c  	 walk through configuration space

c  	*a number of trajectories are run from these initial 
c        conditions on a surface interpolated from a finite data set
c  	 and points are selected throughout these trajectories

c       *trajectory output file is OUT
c        trajectory configurations are written to TOUT

c  Latest Update: 7th January 1996

      include 'traj.inc'
   
      common /icount/ icount

c  read in the parameters which define the system

      call readccm
c   set up the bond definitions

      call readz
c  include permutational symmetry

      call makebp

c   read in run control parameters

      call cntl
80    format(a80)

c   read in the initial table of Taylor series potential coefficients 

      call readt
       if (ipart.eq.2)then
        call radius
c        write(6,*) 'icount=',icount
        if(icount.eq.0) then
         call confidrad
         icount = icount + 1
        endif
        call readcon
       endif

       return
       end

      subroutine interpnmj(NC)

c  this program runs a potential energy sufrace interpolation scheme
c  the procedure is as follows:

c  	*initial conditions are chosen for the trajectories via a Markov
c  	 walk through configuration space

c  	*a number of trajectories are run from these initial 
c        conditions on a surface interpolated from a finite data set
c  	 and points are selected throughout these trajectories

c       *trajectory output file is OUT
c        trajectory configurations are written to TOUT

c  Latest Update: 7th January 1996

      include 'traj.inc'
   
      common /icount/ icount

      call velverlet(1,NC)

      return
      end


      subroutine neighbour(j)

      include 'traj.inc'

      dimension vt(ndatam,ngroupm)

c    assume a weighting function of 1/r**(2*ipow)

      totsum=0.d0

c  !ocl vi(k)
      do m=1,ndata
      do n=1,ngroup

         vt(m,n)=0.d0

         do i=1,nbond
            t=1.d0/rr(j,i)-1.d0/rda(m,ip(i,n))
c           t=rr(j,i)-rda(m,ip(i,n))
            vt(m,n)=t*t+vt(m,n)
         enddo
         
         vt(m,n)=1.d0/vt(m,n)**ipow

         totsum=totsum+vt(m,n)

      enddo
      enddo

c  we set up the massive neighbour list
c  wtol is passed in common/shepdata
c  nforc is the number of neighbours
c  mda identifies the data point in POT that neighbour k comes from
c  nda identifies which permutation neighbour k is

      nforc=0

      do m=1,ndata
      do n=1,ngroup

         wt1=vt(m,n)/totsum

         if (wt1.gt.wtol) then
             nforc=nforc+1
             mda(nforc)=m
             nda(nforc)=n
         endif

      enddo
      enddo

      if (nforc.gt.maxf) then
         write(6,*)' *** ABORT ***'
         write(6,*)' nforc.gt.maxf'
         stop
      endif

c  debug


c      write(6,*)
c      write(6,*) ' nforc =',nforc
c      write(6,*) ' mda(k) =',(mda(k),k=1,nforc)
c      write(6,*) ' nda(k) =',(nda(k),k=1,nforc)

      return
      end

      subroutine neighbour1(j)

c  the original neighbour subroutine which calculates totsum from scratch

      include 'traj.inc'

      dimension vt(ndatam,ngroupm),rinv(nbondm)

c    assume a weighting function of 1/r**(2*ipow)

      totsum=0.d0

      do i=1,nbond
         rinv(i)=1.d0/rr(j,i)
      enddo

      do m=1,ndata
      do n=1,ngroup

         vt(m,n)=0.d0

         do i=1,nbond
            i1=ip(i,n)
            t=rinv(i)-rda(m,i1)
            vt(m,n)=t*t/sumb(m,i1)+vt(m,n)
         enddo

c patch to match in with dvdir.f

         vt(m,n)=vt(m,n)
         vt(m,n)=1.d0/(vt(m,n)**lowp+vt(m,n)**ipow)

c  end of patch

         totsum=totsum+vt(m,n)

      enddo
      enddo

c  we set up the massive neighbour list
c  wtol is passed in common/shepdata
c  nforc is the number of neighbours
c  mda identifies the data point in POT that neighbour k comes from
c  nda identifies which permutation neighbour k is

      nforco=0
      test=wtol*outer
55    continue

      do m=1,ndata
      do n=1,ngroup

         wt1=vt(m,n)/totsum

         if (wt1.gt.test) then
             nforco=nforco+1

      if (nforco.gt.maxfo) then
      nforco=0
      test=2.d0*test
      go to 55
      
        write(6,*)' *** ABORT ***'
        write(6,*)' nforco.gt.maxfo'
        write(6,*) ' neigh1: nforco =',nforco
        stop
      endif

             mdao(nforco)=m
             ndao(nforco)=n
         endif

      enddo
      enddo


c  debug

c      write(6,*) 
c      write(6,*) ' neigh1: nforco =',nforco
c      write(6,*) ' mda(k) =',(mda(k),k=1,nforc)
c      write(6,*) ' nda(k) =',(nda(k),k=1,nforc)

      return
      end

      subroutine neighbour2(j)

c  modified 19-7-96 by kt to approximate totsum by totsum from previous dvdi
c  and to calculate an outer neighbour list

      include 'traj.inc'

      dimension rinv(nbondm)

      do i=1,nbond
         rinv(i)=1.d0/rr(j,i)
      enddo

c  we set up the outer neighbour list
c  outer is the factor by which the outer wtol is less than the inner wtol
c  wtol is the cutoff for allowing points into the neibour list
c  nforco is the number of outer neighbours
c  mdao identifies the outer data point in POT that neighbour k comes from
c  ndao identifies which permutation outer neighbour k is

      nforco=0
      test=wtol*outer*totsum
      if(test.gt.1.d0)then
      npow=lowp
      else
      npow=ipow
      endif
      test=1.d0/test**(1.d0/npow)
55    continue
      do m=1,ndata
      do n=1,ngroup

         wt1=0.d0

         do i=1,nbond
            i1=ip(i,n)
            t=rinv(i)-rda(m,i1)
            wt1=t*t/sumb(m,i1)+wt1
         enddo


         if (wt1.lt.test) then
             nforco=nforco+1

      if (nforco.gt.maxfo) then
c      nforco=0
c      test=0.5d0*test
c      go to 55
        write(6,*)' *** ABORT ***'
        write(6,*)' nforco.gt.maxfo'
        write(6,*) ' neigh2: ngroup,nforco =',ngroup,nforco
        stop
      endif

             mdao(nforco)=m
             ndao(nforco)=n
         endif

      enddo
      enddo

c  debug

c      write(6,*)
c      write(6,*) ' neigh2: nforco =',nforco
c      write(6,*) ' mda(k) =',(mda(k),k=1,nforc)
c      write(6,*) ' nda(k) =',(nda(k),k=1,nforc)

      return
      end

      subroutine neighbour3(j)

c  modified 19-7-96 by kt to approximate totsum by totsum from previous dvdi
c  and to calculate an inner neighbour list from the outer one

      include 'traj.inc'

      dimension rinv(nbondm)

      do i=1,nbond
         rinv(i)=1.d0/rr(j,i)
      enddo

c  we set up the inner neighbour list
c  wtol is the cutoff
c  nforc is the number of neighbours
c  mda identifies the data point in POT that neighbour k comes from
c  nda identifies which permutation neighbour k is

      nforc=0
      test=wtol*totsum
      if(test.gt.1.d0)then
      npow=lowp
      else
      npow=ipow
      endif
      test=1.d0/test**(1.d0/npow)

      do k=1,nforco

         wt1=0.d0

         do i=1,nbond
            i1=mdao(k)
            i2=ip(i,ndao(k))
            t=rinv(i)-rda(i1,i2)
            wt1=t*t/sumb(i1,i2)+wt1
         enddo


         if (wt1.lt.test) then
             nforc=nforc+1

      if (nforc.gt.maxf) then
         write(6,*)' *** ABORT ***'
         write(6,*)' nforc.gt.maxf'
         write(6,*) ' neigh3: nforc =',nforc
         stop
      endif

             mda(nforc)=mdao(k)
             nda(nforc)=ndao(k)
         endif

      enddo

c  debug

c      write(6,*)
c      write(6,*) ' neigh3: nforc =',nforc
c      write(6,*) ' mda(k) =',(mda(k),k=1,nforc)
c      write(6,*) ' nda(k) =',(nda(k),k=1,nforc)

      return
      end

      subroutine velverlet(j,NC)

      include 'traj.inc'
C      COMMON/PRLIST/T,V,H,TIME,NTZ,NT,ISEED0(8),NC,NX

c      dimension omasd(natomm),dvold(nmax,natomm,3)

c   omasd is used in the leap-frog algorithm to update
c   the coordinates and velocities at each time step

c     do n=1,natom
c        omasd(n)=0.5d0*delta/amas(n)
c     enddo

c   evaluate the internal coords

      call intern(j)

c   find the nearest data points
 
c      nloop=0

c      if(NC.eq.0.or.NC.eq.1) then

      if(ipart.eq.1)then
       call neighbour(j)
      elseif(ipart.eq.2)then
       call neighbour1(j)
       call neighbour2(j)
       call neighbour3(j) 
      endif
       
c      endif

c   evaluate the internal coordinate derivatives

      if(ipart.eq.1)then
       call dvdi(j)
       else
       call dvdir(j)
      endif

c   evaluate the cartesian derivatives from the internal 
c   derivatives

c      call dvdx(j)


c   output re t=0 state

c      call outp(j)


c   start the trajectory integration using the velocity-verlet algorithm :

c  ntest=1 if a trajectory is still going
c  ntest=0 if a trajectory has triggered an end test or has gone bad

c      do 1500 nloop=1,nstep

c   update the coordinates


c        do 1250 i=1,natom
           
c              c(j,i,1)=c(j,i,1)+ntest(j)*(delta*v(j,i,1)-
c     .          delta*omasd(i)*dv(j,i,1))
c              c(j,i,2)=c(j,i,2)+ntest(j)*(delta*v(j,i,2)-
c     .          delta*omasd(i)*dv(j,i,2))
c              c(j,i,3)=c(j,i,3)+ntest(j)*(delta*v(j,i,3)-
c     .          delta*omasd(i)*dv(j,i,3))

c              dvold(j,i,1)=dv(j,i,1)
c              dvold(j,i,2)=dv(j,i,2)
c              dvold(j,i,3)=dv(j,i,3)

c 1250   continue

c   evaluate the internal coords

c        call intern(j)

c   peridically update the neighbour list (outer and inner)

c  two part or else one part

      if(ipart.eq.2) then

        ne=dble(NC)/dble(neighco)
        ncall=ne*neighco

c        if (NC.eq.ncall)  call neighbour2(j)
      
        call neighbour2(j)
     
c        call neighbour2(j)

c        ne=dble(NC)/dble(neighci)
        ncall=ne*neighci

c        if (NC.eq.ncall) call neighbour3(j)

        call neighbour3(j) 

c        call neighbour3(j)
      
c   evaluate the internal coordinate derivatives and potential energy

c        call dvdir(j)

c   evaluate the cartesian derivatives from the internal derivatives

c        call dvdx(j)

808   format(a50)
       else

c        ne=dble(NC)/dble(neighc)
        ncall=ne*neighc

c        if (NC.eq.ncall) call neighbour(j)

        call neighbour(j)
 
c        call neighbour(j)

c   evaluate the internal coordinate derivatives and potential energy

c        call dvdi(j)

c   evaluate the cartesian derivatives from the internal derivatives

c        call dvdx(j)

       endif

c   update the velocities

c        do 1350 i=1,natom
c
c              v(j,i,1)=v(j,i,1)-
c     .          ntest(j)*(dvold(j,i,1)+dv(j,i,1))*omasd(i)
c              v(j,i,2)=v(j,i,2)-
c     .          ntest(j)*(dvold(j,i,2)+dv(j,i,2))*omasd(i)
c              v(j,i,3)=v(j,i,3)-
c     .          ntest(j)*(dvold(j,i,3)+dv(j,i,3))*omasd(i)
c
c 1350   continue

c   write out the results where you will

c        np=dble(NC)/dble(nprint)
        nq=np*nprint

        if (NC.eq.nq) then
C   call outp(j)
           if (nstop.ge.1) return
        endif

        if (nstop.ge.1) then
C           call outp(j)
           return
        endif

      return
      end
