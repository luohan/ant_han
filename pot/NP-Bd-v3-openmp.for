      subroutine pot(x,y,z,v,dvdx,dvdy,dvdz,natoms,maxatom)

c   System:                        Al
c   Functional form:               Embedded atom
c   Common name:                   NP-B
c   Number of derivatives:         1
c   Number of bodies:              variable
c   Number of electronic surfaces: 1
c   Interface:                     HO-MM-1
c
c   Notes:  Many-body aluminum potential energy function.  The functional 
c           form is from Ref. 1.  The parameters were re-optimized in Ref. 2
c           against a data set of energies for aluminum clusters and 
c           nanoparticles and bulk data.  Reference 3 provides futher
c           background but is not a required reference for this potential.
c
c  References: (1) J. Mei and J. W. Davenport, Phys. Rev. B, 46, 21, (1992).
c              (2) A. W. Jasper and D. G. Truhlar, "Analytic Potential Energy 
c              Functions for Simulating Aluminum Nanoparticles," in preparation.
c              (3) A. W. Jasper, P. Staszewski, G. Staszewska, N. E. Schultz,
c              and D. G. Truhlar, "Analytic Potential Energy Functions
c              for Aluminum Clusters," J. Phys. Chem. B 108, 8996(2004).
c
c  Units:
c       energies    - hartrees
c       coordinates - bohrs
c
c  v       --- energy of the system (output)
c  x,y,z   --- one-dimensional arrays of the Cartesian coordinates
c              of the system (input)
c  dvdx,dvdy,dvdz   --- one-dimensional arrays of the gradients with respect
c                       to the Cartesian coordinates of the system (output)
c  natoms   --- number of atoms in the system (input)
c  maxatom --- maximum number of atoms (input)

      implicit double precision(a-h,o-z)
      dimension cc(0:5),s(3),xm(3)
      dimension d_v2(3,maxatom),d_rho(3,maxatom)  ! Derivs
C      double precision dx(maxatom,maxatom),dy(maxatom,maxatom),
C     &                 dz(maxatom,maxatom),rij(maxatom,maxatom)
      double precision dvdy(maxatom),dvdz(maxatom),dvdx(maxatom),
     &   x(maxatom),y(maxatom),z(maxatom)
C Nanoparticle-parameterized Parameters (Ref. 2)
        parameter (ec    =   0.104135151d0)
        parameter (phi0  =   7.698048881D-03)
        parameter (r0    =   5.215334169D0)
        parameter (alpha =   4.953631991d0)
        parameter (beta  =   5.202672172d0)
        parameter (gamma =   5.824302949d0)
        parameter (delta =   8.968682037d0)
        parameter (rn =   9.126834796D0)
        parameter (rc =   1.016990163D1)
C        cc/12.0
        data cc /0.0361078497d0,-0.6087732713d0,
     &           2.4849130518d0,-4.5364993027d0,
     &           4.0343389415d0,-1.2937687592d0/
C        data s /6.927645227d0,3.861172975d0,15.498062621d0/
C        s(m)=s(m)*dexp(-(dsqrt(m)-1.d0)*gamma)
        data s /6.927645227d0,0.3459246660d0,0.2180631338d0/
C xm=dsqrt(m), m=1,2,3
        data xm /1.d0,1.414213562373d0,1.732050807569d0/
C 2,3, lizh, March 9, 2006
C        ec   =            0.11686488D+00
C        phi0 =            0.21089713D-01
C        r0   =            0.50918036D+01
C        alpha=            0.38165118D+01
C        beta =            0.38794390D+01
C        gamma=            0.40819159D+01
C        delta=            0.71144992D+01
C        cc(0)=            0.34356185D-01
C        cc(1)=           -0.56645673D+00
C        cc(2)=            0.22968058D+01
C        cc(3)=           -0.43694048D+01
C        cc(4)=            0.41200394D+01
C        cc(5)=           -0.13515545D+01
C        s(1) =            0.52357172D+01
C        s(2) =            0.70957051D-01
C        s(3) =            0.20683357D-01
C 2,3,4 lizh, March 9, 2006
C        ec   =            0.11136863D+00
C        phi0 =            0.15972494D-01
C        r0   =            0.53118246D+01
C        alpha=            0.35938565D+01
C        beta =            0.39873350D+01
C        gamma=            0.38033588D+01
C        delta=            0.57905241D+01
C        cc(0)=            0.36447563D-01
C        cc(1)=           -0.55514935D+00
C        cc(2)=            0.23974206D+01
C        cc(3)=           -0.45791053D+01
C        cc(4)=            0.40845923D+01
C        cc(5)=           -0.13011578D+01
C        s(1) =            0.48096045D+01
C        s(2) =            0.24978563D+00
C        s(3) =            0.65432102D+00
C 2,3,4,7 lizh, March 9, 2006
C        ec   =            0.92543665D-01
C        phi0 =            0.20056359D-01
C        r0   =            0.48939824D+01
C        alpha=            0.42040761D+01
C        beta =            0.53733579D+01
C        gamma=            0.52726447D+01
C        delta=            0.71583691D+01
C        cc(0)=            0.35607978D-01
C        cc(1)=           -0.54429668D+00
C        cc(2)=            0.23838984D+01
C        cc(3)=           -0.45287818D+01
C        cc(4)=            0.40078848D+01
C        cc(5)=           -0.12465520D+01
C        s(1) =            0.47844839D+01
C        s(2) =            0.17989604D+00
C        s(3) =            0.15508702D+00
C nano(2-177) lizh, March 9, 2006
C        ec   =            0.89114461D-01
C        phi0 =            0.20731912D-01
C        r0   =            0.49920542D+01
C        alpha=            0.62289304D+01
C        beta =            0.78639625D+01
C        gamma=            0.54271540D+01
C        delta=            0.61588339D+01
C        cc(0)=            0.68237330D-02
C        cc(1)=           -0.65344580D+00
C        cc(2)=            0.27342882D+01
C        cc(3)=           -0.44611340D+01
C        cc(4)=            0.38594252D+01
C        cc(5)=           -0.12542307D+01
C        s(1) =            0.43283660D+01
C        s(2) =            0.21693455D+00
C        s(3) =            0.13952258D+00
C Parameters used for the spline function
        caa= 0.1d-7
        md = 4


c Initialize
      v = 0.d0
      phi02=0.5d0*phi0
      ab=alpha/beta
      cb=gamma/beta
      db=delta/beta

      do i=1,natoms
        dvdx(i) = 0.d0
        dvdy(i) = 0.d0
        dvdz(i) = 0.d0
C store dx,dy,dz,rij(i,j) for the later usage
C          do j=i+1,natoms
C            dx(i,j)=x(i)-x(j)
C            dy(i,j)=y(i)-y(j)
C            dz(i,j)=z(i)-z(j)
C            rij(i,j)=dsqrt(dx(i,j)**2+dy(i,j)**2+dz(i,j)**2)
C            rij(j,i) = rij(i,j)
C            dx(j,i)=-dx(i,j)
C            dy(j,i)=-dy(i,j)
C            dz(j,i)=-dz(i,j)
C          enddo
      enddo

c Double loop (main loop)
C$OMP PARALLEL DO 
C$OMP& default(private)
C$OMP& shared(natoms,phi02,ab,cb,db)
C$OMP& shared(cc,s,xm,caa,md)
C$OMP& shared(x,y,z)
C$OMP& reduction(+: v,dvdx,dvdy,dvdz)
      do 2 i=1,natoms
      rho = 0.d0
      v2 = 0.d0
      do k=1,3
      do j=1,natoms
      d_rho(k,j) = 0.d0
      d_v2(k,j) = 0.d0
      enddo
      enddo
      do 3 j=1,natoms
      if (i.eq.j) go to 3
        dx=x(i)-x(j)
        dy=y(i)-y(j)
        dz=z(i)-z(j)
        rr=dsqrt(dx*dx+dy*dy+dz*dz)

c Cutoff function
      if (rr.le.rn) then
             q=1.d0
             dqdr=0.d0  ! Derivs
      elseif(rr.ge.rc) then
             go to 3
      else
             tmp=1.d0/(rc-rn)
             xx=(rr-rn)*tmp
             s1=1.d0+3.d0*xx+6.d0*xx**2
             s2=1.d0-xx
             s3=s2*s2     ! (1.d0-xx)**2
             s4=s2*s3      ! (1.d0-xx)**3
             q=s4*s1
             dxxdr=3.d0*tmp
             dqdr = ( s4*(1.d0+4.d0*xx)-s3*s1 )*dxxdr
      endif

C only need to calculate v2 terms once.
C Pair potential phi
      if (i.gt.j) goto 4
      dr = rr/r0-1.d0
      tmp = -phi0*dexp(-gamma*dr)
      phi = tmp*(1.d0+delta*dr)
      v2 = v2 + phi*q
      dphidr = (-phi*gamma + tmp*delta )/r0 ! Derivs
      dphiqdr = dqdr*phi+dphidr*q  ! Derivs
C v2 (pairwise repulsion) derivs
      tmp=dphiqdr/rr
      d_v2(1,i) = d_v2(1,i) + tmp*dx
      d_v2(1,j) = d_v2(1,j) - tmp*dx
      d_v2(2,i) = d_v2(2,i) + tmp*dy
      d_v2(2,j) = d_v2(2,j) - tmp*dy
      d_v2(3,i) = d_v2(3,i) + tmp*dz
      d_v2(3,j) = d_v2(3,j) - tmp*dz
 4    continue

c Embedding potential rho
      tmp=r0/rr
      s2=tmp*tmp
      s3=tmp*s2
      s4=tmp*s3
      s5=tmp*s4
      ef = cc(0)+cc(1)*tmp+cc(2)*s2+cc(3)*s3+cc(4)*s4+cc(5)*s5
      drhodr = cc(1)*s2 + 2.d0*cc(2)*s3+3.d0*cc(3)*s4+4.d0*cc(4)*s5+
     &                 5.d0*cc(5)*s5*tmp
      drhodr = -drhodr*q/r0 + ef*dqdr
      rho = rho+ef*q

c rho (density) derivs
      tmp=drhodr/rr
      d_rho(1,i) = d_rho(1,i) + tmp*dx
      d_rho(1,j) = d_rho(1,j) - tmp*dx
      d_rho(2,i) = d_rho(2,i) + tmp*dy
      d_rho(2,j) = d_rho(2,j) - tmp*dy
      d_rho(3,i) = d_rho(3,i) + tmp*dz
      d_rho(3,j) = d_rho(3,j) - tmp*dz
    3 continue

      sterm=0.d0
      dstermdrho=0.d0  ! Derivs
      bigef = 0.d0
      dbigefdrho = 0.d0
      if (rho.gt.0.d0) then
        dlogrho=dlog(rho)
        rhomd=rho**md
        crhomd=1.d0/(caa+rhomd)
        crhomd2=crhomd*crhomd
        rhocrhomd=rho*crhomd
        do m=1,3
C          xm=dsqrt(dble(m))
          tmp=xm(m)*cb
          tmp1=xm(m)*db
          rhomtmp=rho**(dble(md-1)+tmp)
          s2 = 1.d0+delta*(xm(m)-1.d0)-tmp1*dlogrho 
          sterm = sterm + s(m)*s2*rhomtmp*rhocrhomd
          s4    = rhomtmp*(dble(md)*caa+tmp*caa+tmp*rhomd)*crhomd2
          dstermdrho=dstermdrho+s(m)*(s2*s4-rhomtmp*crhomd*tmp1)
        enddo

        rhomtmp=rho**(dble(md-1)+ab)
        s2   =  1.d0-ab*dlogrho 
        bigef = -ec*s2*rhomtmp*rhocrhomd + phi02*sterm
        s4   = rhomtmp*(dble(md)*caa+ab*caa+ab*rhomd)*crhomd2
        dbigefdrho = -ec*(s2*s4 - rhomtmp*crhomd*ab) + phi02*dstermdrho
      endif

c Combine derivs
      do j=1,natoms
      dvdx(j) = dvdx(j) + d_v2(1,j) + dbigefdrho*d_rho(1,j)
      dvdy(j) = dvdy(j) + d_v2(2,j) + dbigefdrho*d_rho(2,j)
      dvdz(j) = dvdz(j) + d_v2(3,j) + dbigefdrho*d_rho(3,j)
      enddo

      v = v + v2 + bigef
    2 continue
C$OMP END PARALLEL DO

      return
      end

      subroutine prepot
      return
      end

