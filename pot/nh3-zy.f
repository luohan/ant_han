C*******************************************************************
C     SUBROUTINE pot(Xcart,U11,U22,U12,V1,V2,
C    1           gU11,gU22,gU12,
C    2           gV1,gV2)
      SUBROUTINE dummy()
C*******************************************************************
C   System:                        NH3
C   Functional form:               polynomial 2x2 diabatic fit
C   Common name:                   NH3
C   Number of derivatives:         5
C   Number of bodies:              4
C   Number of electronic surfaces: 1
C   Interface:
C
C  Notes: 
C  This subroutine calls the SurfGen library for the NH3 potential 
C  developed by Xiaolei Zhu and David Yarkony. 
C
C References: 
C    J. Chem. Phys.*, **132**, 104101 (2010) http://dx.doi.org/10.1063/1.3324982 for fitting method
C    J. Phys. Chem. Lett. 5, 1055, (2014) for NH3 potential surfaces. 
C
C  Units:
C       energies    - hartrees
C       coordinates - bohrs
C  All gradients are in hartree/bohr
C  Format for   Xcart 
C  Xcart(1:3)   is N
C  Xcart(4:6)   is H1
C  Xcart(7:9)   is H2
C  Xcart(10:12) is H3

!     implicit none
! I/O variables
!     integer NATOM, NST
!     parameter (NATOM = 4)
!     parameter (NST = 2)
!     double precision Xcart(12) 
!     double precision U11
!     double precision U22
!     double precision U12
!     double precision V1
!     double precision V2
!     double precision gU11(12)
!     double precision gU22(12)
!     double precision gU12(12)
!     double precision gV1(12)
!     double precision gV2(12)

! Local variables
! cart(12) local Xcart in Bohr, x1,y1,z1,...,x4,y4,z4
!     double precision cart(12),e(nst),h(nst,nst),cg(3*natom,nst,nst)
!     double precision dcg(3*natom,nst,nst)
!     integer i,j


! get energies and gradients 
!     call packpot(cart,U11,U22,U12,V1,V2,gU11,gU22,gU12,gV1,gV2)
!     call EvaluateSurfgen(cart,e,cg,h,dcg)
!     V1 = e(1)
!     V2 = e(2)
!     U11 = h(1,1)
!     U22 = h(2,2)
!     U12 = h(1,2)
!     gU11(:) = dcg(:,1,1)
!     gU22(:) = dcg(:,2,2)
!     gU12(:) = dcg(:,1,2)
!     gV1(:) = cg(:,1,1)
!     gV2(:) = cg(:,2,2)

      return
      end

C23456789
C     SUBROUTINE prepot
C       call initPotential()
C     return
C     end

