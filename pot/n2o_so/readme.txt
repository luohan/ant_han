============== README.TXT =============================


Article Information- EJCPSA6-110-017920

Authors:  Hisao Nakamura, Shigeki Kato

Title:  Theoretical study on the spin-forbidden predissociation reaction of
N2O: Ab initio energy surfaces and quantum dynamics calculations

Journal:  Journal of Chemical Physics, 22 May 1999, Vol. 110, Issue No. 21, Page 9937-9947

Deposit Information:

Description: Parameters of Potential Energy and Spin-Orbit Interaction

Total No. of Files: Totally four files including README.TXT file

Filenames:  Hso_Param.txt, Singlet_Param.txt and Triplet_Param.txt

Filetypes:  ASCII

Corresponding Author: Shigeki Kato, Department of Chemistry, Graduate School
of Science, Kyoto University, Kitashirakawa, Sakyo-ku, Kyoto 606-8502, Japan
E-mail adree: shigeki@kuchem.kyoto-u.ac.jp 
Fax: 81-75-753-4000
Phone: 81-75-753-4004





