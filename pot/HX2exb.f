!*******************************************************************
      subroutine prepot
      implicit none
      return
      end subroutine prepot
!*******************************************************************
!*******************************************************************
      SUBROUTINE pot(Xcart,UIJ,VI,gUIJ,gVI,dvec,ccout)
!*******************************************************************
!  This subroutine computes the energy and gradients for HX2 
!  considering two electronic energy surfaces in the adiabatic and
!  diabatic approximations within the model to test army ants 
!  tunneling method for a nonadiabatic dynamics. It is based on the 
!  N-H dissociation reaction for HN2.
!
! Ruben Meana-Paneda Dec. 2013
!
!  System:                        HX2 
!  Functional form:               2x2 diabatic fit
!  Number of derivatives:         5
!  Number of bodies:              3
!  Number of electronic surfaces: 2
!  Units for ANT:
!     energies    - hartrees
!     coordinates - bohrs
!  Units in POT:
!     energies    - kcal/mol 
!     coordinates - angstrom
!  All gradients have to be passed in hartree/bohr
!  Format for   Xcart
!     Xcart(1:3)   is H
!     Xcart(4:6)   is X
!     Xcart(7:9)   is X
!
      implicit none
      integer :: i,j,k,l,m
      integer, parameter :: nat=3,nsurft=2
      double precision :: Xcart(3*nat),X(3*nat)
! arrays containing potential surfaces and Cartesian derivatives
      double precision :: UIJ(nsurft,nsurft),VI(nsurft)
      double precision :: gUIJ(3,nat,nsurft,nsurft),gVI(3,nat,nsurft)
      double precision :: cc(nsurft,nsurft),dvec(3,nat,nsurft,nsurft)
      double precision, intent(out) :: ccout(nsurft,nsurft)
!     double precision :: ccout(nsurft,nsurft)
! temporal variables
      double precision :: UIJm(nsurft,nsurft),UIJp(nsurft,nsurft)
      double precision :: Vm(nsurft),Vp(nsurft)
      double precision :: ccm(nsurft,nsurft),ccp(nsurft,nsurft)
      double precision :: a
! Conversion factors
      double precision :: FAC= 0.529177208D0   ! bohr to angstrom
      double precision :: cau= 627.5095D0      ! hatree to kcal/mol 
      double precision :: gconv= 8.432975246D-4! gradients (kcal/mol) / angstrom to hartree/bohr 
! Step size for the numerical gradients
      double precision, parameter :: stp=1.D-4
! Call prepot
!     call prepot
! Convert from bohr to angstrom
      do i=1,3*nat
       X(i)=Xcart(i)*FAC
      enddo
! Initialize energy matrix and derivatives
      UIJ(:,:)=0.D0
      VI(:)=0.D0
      gUIJ(:,:,:,:)=0.D0
      gVI(:,:,:)=0.D0
      dvec(:,:,:,:)=0.D0
! Compute the energies in the diabatic and adibatic representations
      call poten(X,UIJ,VI,cc)
      ccout = cc
! Change from kcal/mol to hartree
      do i=1,nsurft
       vi(i)=vi(i)/cau
       do j=1,nsurft
        uij(i,j)=uij(i,j)/cau
       enddo
      enddo
! Compute the Cartesian gradients numerically
      i=0
      do k=1,nat
       do j=1,3
          i=i+1
          a=x(i)
          x(i)=a+stp
          call poten(X,uijp,vp,ccp)
          x(i)=a-stp
          call poten(X,uijm,vm,ccm)
          do l=1,nsurft
           do m=1,nsurft
            gUIJ(j,k,l,m)=((UIJp(l,m)-UIJm(l,m))/(2.0d0*stp))*gconv
           enddo
           gVI(j,k,l)=((Vp(l)-Vm(l))/(2.0d0*stp))*gconv
          enddo
          x(i)=a  
       enddo
      enddo
! Compute the nonadiabatic coupling vectors DVEC.
      call getdvec3(3,UIJ,gUIJ,VI,dvec,cc)
      return
      end subroutine pot

!----------------------------------------------------------------------------------------
      subroutine poten(Xcart,u,v,cc)
!
! This subroutine computes the U energy matrix, V energy matrix and the
! NACT vectors
!
      integer, parameter :: natoms=3,nsurft=2
      double precision :: xcart(3*natoms)
      double precision :: u(nsurft,nsurft),v(nsurft),cc(nsurft,nsurft)
!
! temporal variables
      integer :: i,l,lwork,info,k
      double precision :: x(natoms,3)
      double precision, allocatable :: work(:)
      double precision :: wrk(nsurft*(3+nsurft/nsurft)) !work(n*(3+n/2))
      double precision :: ud(nsurft,nsurft)
      double precision :: r(natoms-1),theta(natoms-2),dih(natoms-3)
      double precision :: kstr(natoms-1),kbend(natoms-2),ktor(natoms-3)
      double precision :: abcd
!
! From 1D to 2D array
      do i=1,natoms
       do j=1,3
        x(i,j)=xcart(3*i-(3-j))
       enddo 
      enddo 
! Compute the distances
      do i=1,natoms-1
       call distinat(x,natoms,i,i+1,r(i))
      enddo
! Compute the angles
      do i=1,natoms-2
       call anglenat(x,natoms,i,i+1,i+2,theta(i))
      enddo
! Reinizialize U and V energy matrices 
      u(:,:)=0d0
      v(:)=0d0
! Compute the U energy matrix and the V eigenvalues
      call cm(r,theta,dih,u)
! Diagonalize the coupling matrix (from LAPACK)
!---------------------------------------------------------!
!Calls the LAPACK diagonalization subroutine DSYEV        !
!input:  u(n,n) = real symmetric matrix to be diagonalized!
!            n  = size of u                               !
!output: u(n,n) = orthonormal eigenvectors of u           !
!        v(n) = eigenvalues of a in ascending order       !
!---------------------------------------------------------!
      do i=1,2
       do j=1,2
        ud(i,j)=u(i,j)
       enddo 
      enddo 
      lwork = -1
      allocate (work (1) )
      call dsyev('V','L',2,ud,2,v,work,lwork,info)
      lwork=int(work(1))
      deallocate (work)
      allocate( work(lwork) )
      call dsyev('V','L',2,ud,2,v,work,lwork,info)
      deallocate (work)
      if(info.ne.0) then
       write(6,*) 'Error in diagolization of UIJ'
       stop
      endif
      do i=1,2
       do j=1,2
       cc(i,j)=ud(i,j)
       enddo
      enddo
      return
      end subroutine  poten

!***************
! Compute the coupling matrix between two electronic surfaces (Initially only
! two electronic surfaces 1 and 2)
!***************
      subroutine CM(r,theta,dih,u)
      implicit none
      integer, parameter :: natoms=3,nsurf=2
! Distances, angles, and dihedral angles of the input geometries
      double precision,intent(in) :: r(natoms-1),theta(natoms-2)
      double precision,intent(in) :: dih(natoms-3)
! U energy matrix
      double precision,intent(out) :: U(nsurf,nsurf)
! Temporal variables
      integer :: i,ns
! ------Bonds
! Morse parameters 
      double precision :: mde(natoms-1,nsurf),beta(natoms-1,nsurf)
! Equilibrium distances
      double precision :: re(natoms-1,nsurf)
! ------Angles
! Barrier height for the angle value of 180 and equilibrium values
      double precision :: vmax(natoms-2,nsurf),bd(natoms-2,nsurf)
! Equilibrium angle distances
      double precision :: ae(natoms-2,nsurf)
! ------Energies
! Relative energies of the two minima in the two potential energy
! surfaces
      double precision :: ener(nsurf)
! Conversion factors
      double precision, parameter :: r2d=57.298d0,pi=dacos(-1.d0)
!
! Parameters 
! ----Strechings
! H-X
      do i=1,nsurf
        mDe(1,i) = 57.0d0             ! kcal/mol 
        beta(1,i)= 1.867d0             ! A-1
        re(1,i)  = 1.06735d0           ! A 
      enddo
! X-X
      do i=1,nsurf
        mde(2,i) = 228.3951d0          ! kcal/mol 
        beta(2,i)= 2.85d0              ! A-1
        re(2,i)  = 1.19223625d0        ! A 
      enddo
! ----Bendings
! H-X-X
      do i=1,nsurf
        ae(1,i)  = 115.0d0/ r2d        ! surface 1
! Vmax is the barrier in kcal/mol between the two well minima
        vmax(1,i)=  25.6871d0          ! kcal/mol 
        bd(1,i)  =  pi-ae(1,i)         ! rad 
      enddo
! ----Relative energies (in kcal/mol)
        ener(1) = 0d0
        ener(2) = 45d0
!***********************************************************************************
! Initialize the U energy matrix
        U(:,:)=0.d0
! Compute the U energy matrix
! Streching
        do ns=1,nsurf
          do i=1,natoms-1   
           if (ns.eq.2.and.i.eq.1) then
! Only for surface 2 and X-H bond
            U(ns,ns)=U(ns,ns)+
! We consider that both surface cross at the value of 1.5 A for the X-H bond
     &  10.d0*exp(-4.167d0*(r(i)-re(i,ns)))
           else
! Morse potential
            U(ns,ns)=U(ns,ns)+
     &      mDe(i,ns)*(1.d0-dexp(-beta(i,ns)*(r(i)-re(i,ns))))**2.d0
           endif
          enddo
! Bending 
          do i=1,natoms-2   
! Quartic potential
            U(ns,ns)=U(ns,ns)+vmax(i,ns)/bd(i,ns)**4.d0*
     &     ((theta(i)-pi)**2.d0-bd(i,ns)**2.d0)**2.d0
          enddo
! Electronic energy difference
          U(ns,ns)=U(ns,ns)+ener(ns)
        enddo
! Compute the off-diagonal terms U12 U21 
        U(1,2)=2.5d1*exp(-((r(1)-1.6812d0)/(0.6d0))**2.d0)
        U(2,1)=U(1,2)
!***********************************************************************************
        return
        end subroutine CM

!***************
! Compute the distance between the i-j atoms 
!***************
        subroutine distinat(coord,natoms,i,j,r)
        implicit none
        double precision, intent(in) :: coord(natoms,3) 
        integer, intent(in) :: natoms,i,j
        double precision, intent(out) :: r 
        double precision :: r1(3),normnat
        r1(:)=coord(j,:) - coord(i,:)
        r=normnat(r1) 
        return
        end subroutine distinat

!***************
! Compute the angle that is defined by i-j-k atoms 
!***************
        subroutine anglenat(coord,natoms,i,j,k,theta)
        implicit none
        double precision, intent(in) :: coord(natoms,3) 
        double precision, intent(out) :: theta
        integer, intent(in) :: natoms,i,j,k
        double precision :: r1(3),r2(3),r3(3),normnat
        r1(:)=coord(i,:) - coord(j,:)
        r2(:)=coord(k,:) - coord(j,:) 
        theta=acos( (dot_product(r1,r2)) / (normnat(r1)*normnat(r2)) )
! Another way to compute it
!        call cross_productnat(r1,r2,r3)
!        theta = atan2(normnat(r3),dot_product(r1,r2)) 
        return
        end subroutine anglenat

!***************
! Cross Product  
!***************
        SUBROUTINE cross_productnat(A, B, C)
        implicit none
        double precision, dimension(3), intent (in)    :: a
        double precision, dimension(3), intent (in)    :: b
        double precision, dimension(3), intent (out)   :: c
        c(1) = a(2)*b(3) - a(3)*b(2)
        c(2) = a(3)*b(1) - a(1)*b(3)
        c(3) = a(1)*b(2) - a(2)*b(1)
        return
        end subroutine cross_productnat
        
!********************
! Norm of one vector 
!********************
        function normnat(v)
        implicit none
        double precision:: normnat
        double precision, dimension(3) :: v
        normnat=sqrt(v(1)**2.d0+v(2)**2.d0+v(3)**2.d0)
        end function normnat
        
        
        subroutine getdvec3(nclu,pemd,gpemd,pema,dvec,cc)
        implicit none
        integer, parameter :: nsurft=2
        double precision :: pema(nsurft),pemd(nsurft,nsurft)
        double precision :: gpemd(3,nclu,nsurft,nsurft)
        double precision :: dvec(3,nclu,nsurft,nsurft),cc(nsurft,nsurft)
        integer :: i1,i2,i,j,k,l,nclu
 
!     Program to compute nonadiabatic coupling vectors from the adiabatic energies and
!     the gradients of diabatic potential matrix elements for a multisurface system
!     The formula is taken from ANT07 and is described in M.D. Hack et al., J. Phys. Chem. A 103, 6309 (1999)
!
! compute d
!      dvec(:,:,:,:) = 1d-9

       dvec(:,:,:,:) = 0d0
        do i1 = 1,3
         do i2 = 1,nclu
          do i = 2,nsurft
           do j = 1, i-1
!            dvec(i1,i2,i,j) = 0.d0
             do k = 1, nsurft
              do l = 1, nsurft
               dvec(i1,i2,i,j) = dvec(i1,i2,i,j)
     &                         + cc(k,i)*cc(l,j)*gpemd(i1,i2,k,l)
              enddo
             enddo
             if ((pema(j) - pema(i)) .ne. 0.0d0) then
               dvec(i1,i2,i,j) = dvec(i1,i2,i,j)/(pema(j)-pema(i))
             else
               dvec(i1,i2,i,j) = 0.0d0
             endif
!              write(6,*) "dvec ",i1,i2,i,j,dvec(i1,i1,i,j)
             dvec(i1,i2,j,i) = -dvec(i1,i2,i,j)
           enddo
          enddo
         enddo
        enddo
!       stop
        return
        end subroutine getdvec3
                              

