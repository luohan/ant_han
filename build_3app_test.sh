#!/bin/bash -e
install_mod () {
  if [[ "$(hostname)" == *"ecn"* ]]; then
     modfile=$HOME/.local/modulefiles/ant/$1.lua
  else
    modfile=$HOME/privatemodules/ant/$1.lua
   fi
   echo $modfile
  mkdir -p $(dirname $modfile)
cat << EOF > $modfile
whatis("Description: ANT for PES ${POT}")
load("intel")
prepend_path ('PATH', "${CWD}/exe")
prepend_path ('PYTHONPATH', "${CWD}/tools/Post/")
execute{cmd="ln -sf ${CWD}/exe/$2 ${CWD}/exe/ant", modeA={"load"}}
execute{cmd="rm -f  ${CWD}/exe/ant", modeA={"unload"}}
EOF
}

# CWD=/home/luohan/Documents/ANT/ant_han
# POT=n2o_3ap_Gamallo
# NAME=n2o_3ap

CWD=/home/luohan/Documents/ANT/ant_han
CWD=$(pwd)
FC=ifort
POT=n2o_3app_Gamallo
NAME=n2o_3app

# CWD=/home/luohan/Documents/ANT/ant_han
# POT=n2o_1ap_Gonzalez
# NAME=n2o_1ap

DIAPOT=diapot2
ant_exe=ANT-${POT}.x.opt

cd $CWD/src
# --------------- load necessary module in this region ------------
#module load intel
module load myhdf5
# -----------------------------------------------------------------
make -f Makefile debug POT=${POT} DIAPOT=$DIAPOT FC=$FC -j 4
make -f Makefile wkb POT=${POT} DIAPOT=$DIAPOT FC=$FC -j 4
# make -f Makefile all POT=${POT} DIAPOT=$DIAPOT FC=gfortran -Bnd |\
	 # make2graph | dot -Tpng -o ../dependency.png

# --------------- install a module file ------------------------
#install_mod $NAME $ant_exe
# -------------------------------------------------------------
