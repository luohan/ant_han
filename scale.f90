      subroutine scale(im)
!
! Scale momentum and coordinates to get desired value
!   Input:
!  xxm,ppm,mmm,natoms
! qnmrot0       : Rotation state for linear AG 
! erot(im)      : Total rotation energy of an AG
! eroti(3,im)   : Three components of rotation energy of an AG
! momiprin(3)   : Three principal moment of inertia (in c_struct, cal by
!                 calling rotprin
! rotsel(im)    : 1: User provide; 0: Randomly generate (in c_sys.f)
! iflinear:     : 0: an atom; 1: Linear; 2; non-linear
!    Output:
!  new erot, eroti (if not rot selelect )
!  new ppm
!    Other variables
! vthet1:       angular rotation speed around x axis
! vthet2:       angular rotation speed around y axis
! vthet3:       angular rotation speed around z axis
! bigj0(3):     Angular momentum to be set
! bigjt:        Total angular momentum
      use param, only: mnat, amutoau, autoev,autoang
      use c_output, only: idebug, minprinticon, maxprint
      use c_sys, only: repflag, potflag
      use c_struct, only: nvibmods, xxm,  mmm, cell, symbolm, indmm, &
                        natoms, momiprin, iflinear, imulti, icharge, &
                        xx0, iatom, mmofag, rotaxis
      use c_traj, only: ppm, nsurf, dvec, gpema, gpemd, pema, pemd,  &
                        evib, erot, ke,pe, bigj, kbt0, bigjtoti
      use c_initial, only: ewell,eroti
      implicit none
      integer :: im
! local
      integer :: i,j,l,ii
      integer,parameter :: lmax = 100
      real(8),parameter :: ep0 = 1.0d-5
      real(8) :: xxm0(3,mnat),ppm0(3,mnat),bigj0(3),Jset
      real(8) :: etot,etotd,ke0,pe0,xeq(3,mnat),bigjm(3),erott(3)
      real(8) :: comxxm(3), comppm(3), totcomp
      real(8) :: temp

      etotd = evib(im)+erot(im)  !desired energy
      do i = 1,natoms 
        ii = iatom(im)+i-1
        do j = 1,3
          xeq(j,i) = xx0(j,ii) !get equilibrium configuration
        enddo
      enddo

      xxm0 = xxm
      ppm0 = ppm
      bigj0 = bigj

      write(6,*) 'Scale coordinates and momentum:'
      if (.not. minprinticon) then
        write(6,'(1x,a18,3f12.5)') 'J0 = ',(bigj(i),i=1,3)
        write(6,'(1x,a18,3f10.5)') 'Etot,Evib,Erot = ',etotd*autoev,&
          evib(im)*autoev,erot(im)*autoev
        write(6,'(1x,a18,3f10.5)') 'Eroti = ',(eroti(i,im)*autoev,i=1,3)
        write(6,'(1x,a18,3f10.5)') 'pe,ke,ewell = ',pe*autoev,ke*autoev,ewell(im)*autoev
        write(6,*)'------------------------------------------------'
        write(6,'(a5,1x,3a12,6a10)') 'Step','Jx(au)','Jy(au)','Jz(au)','Erot(eV)',&
          'Evib(eV)','Eint(eV)','Ejx(eV)','Ejy(eV)','Ejz(eV)'
        write(6,*)'-------------------------------------------------'
      endif

      do l = 1,lmax
        etot = ke+pe-ewell(im)
        if ( dabs(etot -etotd)*autoev .gt. ep0)then
          temp = dsqrt(etotd/etot)
          do i =1,3
            do j = 1,natoms
              xxm(i,j) = xeq(i,j) + (xxm(i,j) - xeq(i,j))*temp
              ppm(i,j) = ppm(i,j)*temp
            enddo
          enddo
          ! remove center of mass motion
          call comgen(xxm,mmm,natoms,comxxm,mmofag(im))
          call nocompp(ppm,mmm,natoms,comppm,totcomp,1) 
          call rotprin(xxm,mmm,natoms,momiprin,rotaxis,iflinear) !update principle axis 
          call angmom(iflinear,bigj,ppm,xxm,natoms,bigjtoti(im)) !update bigj and bigjtoti
          call gettemp(im) 
          call getpem(xxm,indmm,symbolm,cell,natoms,pema,pemd,gpema, &
            gpemd,dvec,mmm,1,nsurf,icharge(im),imulti(im))
          if (repflag.eq.1) then
            pe = pemd(nsurf,nsurf)
          else
            pe = pema(1)
          endif
          erot(im) = 0.0d0
          do i = 1,3
            erott(i) = 0.5d0*bigj(i)**2/momiprin(i)
            erot(im) = erot(im)+erott(i)
          enddo
          evib(im) = pe+ke -ewell(im) - erot(im) 
            
          
          if (maxprint .and. .not. minprinticon) then
            write(6,'(1x,I4,1x,3f12.5,6f10.5)') -l,(bigj(i),i=1,3),&
                erot(im)*autoev,evib(im)*autoev,(erot(im)+evib(im))*autoev,&
                              (erott(i)*autoev,i=1,3)
          endif

          do i=1,3
            Jset = dsqrt(2.0d0*momiprin(i)*eroti(i,im))
            Jset = dsign(Jset,bigj0(i))
            bigjm(i) = bigj(i) - Jset  !maintain erot 
          enddo
          call noang(iflinear,bigjm,ppm,xxm,mmm,natoms) !change ppm to scale momentum
          call angmom(iflinear,bigj,ppm,xxm,natoms,bigjtoti(im)) !update bigj and bigjtoti
          call gettemp(im)  !update ke
          erot(im) = 0.0d0
          do i = 1,3
            eroti(i,im) = 0.5d0*bigj(i)**2/momiprin(i)
            erot(im) = erot(im)+eroti(i,im)
          enddo
          evib(im) = pe+ke -ewell(im) - erot(im)

          if (maxprint .and. .not. minprinticon) then
            write(6,'(1x,I4,1x,3f12.5,6f10.5)') l,(bigj(i),i=1,3),&
                 erot(im)*autoev,evib(im)*autoev,(erot(im)+evib(im))*autoev,&
                              (eroti(i,im)*autoev,i=1,3)
          endif
        else
          exit
        endif
      enddo
      if (maxprint .and. .not. minprinticon)then
        write(6,*) '--------------------------------------------'
      endif


      if (l .lt. lmax) then
        write(6,'(a25,2f10.5)') 'Energy scaling succeeded',etot*autoev,etotd*autoev
        if (.not. minprinticon) then
          write(6,*)
          write(6,*) "Structure before scaling"
          do i=1,natoms
            write(6,107) i,mmm(i)/amutoau,(xxm0(j,i)*autoang,j=1,3)
          enddo
          write(6,*)
          write(6,*) "Structure after scaling"
          do i=1,natoms
            write(6,107) i,mmm(i)/amutoau,(xxm(j,i)*autoang,j=1,3)
          enddo
          write(6,*)
          write(6,*) "Momenta before scaling"
          do i=1,natoms
            write(6,107) i,mmm(i)/amutoau,(ppm0(j,i),j=1,3)
          enddo
          write(6,*)
          write(6,*) "Momenta after scaling"
          do i=1,natoms
            write(6,107) i,mmm(i)/amutoau,(ppm(j,i),j=1,3)
          enddo
        endif
      else
        write(6,'(a25,2f10.5)') 'Energy scaling failed',etot*autoev,etotd*autoev
        write(6,*) 'Change back to initial structure'
        xxm = xxm0
        ppm = ppm0
        call rotprin(xxm,mmm,natoms,momiprin,rotaxis,iflinear) !update principle axis 
        call angmom(iflinear,bigj,ppm,xxm,natoms,bigjtoti(im)) !update bigj and bigjtoti
        call gettemp(im) 
        call getpem(xxm,indmm,symbolm,cell,natoms,pema,pemd,gpema, &
          gpemd,dvec,mmm,1,nsurf,icharge(im),imulti(im))
        if (repflag.eq.1) then
          pe = pemd(nsurf,nsurf)
        else
          pe = pema(1)
        endif
        erot(im) = 0.0d0
        do i = 1,3
          eroti(i,im) = 0.5d0*bigj(i)**2/momiprin(i)
          erot(im) = erot(im)+erott(i)
        enddo
        evib(im) = pe+ke -ewell(im) - erot(im)
      endif
      write(6,*)



 100  format(1x,a,3f12.5,1x,a)
 106  format(1x,a,f12.5,1x,a)
 107  format(1x,i5,f12.4,3f14.8)
      end subroutine scale
