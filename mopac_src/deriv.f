      SUBROUTINE DERIV(GEO,GRAD,*)                                      CSTP
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C
      INCLUDE 'SIZES.i'
C
C
      DIMENSION GRAD(*), GEO(3,*)
      COMMON / EULER/ TVEC(3,3), ID
      COMMON /GEOVAR/ XDUMMY(MAXPAR), NVAR, LOC(2,MAXPAR)               IR0394 
c Common MOLKST splitted in MOLKSI and MOLKSR    Ivan Rossi 0394   &8)
      COMMON /MOLKSI/ NUMAT,NAT(NUMATM),NFIRST(NUMATM),
     1                NMIDLE(NUMATM),NLAST(NUMATM), NORBS,
     2                NELECS,NALPHA,NBETA,NCLOSE,NOPEN
      COMMON /MOLKSR/ FRACT
      COMMON /GEOKST/ NATOMS,LABELS(NUMATM),
     1NA(NUMATM),NB(NUMATM),NC(NUMATM)
      COMMON /GRAVEC/ COSINE
      COMMON /GEOSYM/ NDEP, IDUMYS(MAXPAR,3)
C    changed common path for portability  (IR)
      COMMON /PATHI / LATOM,LPARAM
      COMMON /PATHR / REACT(200)
c
      COMMON /UCELL / L1L,L2L,L3L,L1U,L2U,L3U
      COMMON /XYZGRA/ DXYZ(3,NUMATM*27)
      COMMON /ENUCLR/ ENUCLR
      COMMON /NUMCAL/ NUMCAL
      COMMON /DENSTY/ P(MPACK), PA(MPACK), PB(MPACK)
      COMMON /WMATRX/ WJ(N2ELEC), WK(N2ELEC)
      COMMON /HMATRX/ H(MPACK)
      COMMON /ATHEAT/ ATHEAT
C***********************************************************************
C
C    DERIV CALCULATES THE DERIVATIVES OF THE ENERGY WITH RESPECT TO THE
C          INTERNAL COORDINATES. THIS IS DONE BY FINITE DIFFERENCES.
C
C    THE MAIN ARRAYS IN DERIV ARE:
C        LOC    INTEGER ARRAY, LOC(1,I) CONTAINS THE ADDRESS OF THE ATOM
C               INTERNAL COORDINATE LOC(2,I) IS TO BE USED IN THE
C               DERIVATIVE CALCULATION.
C        GEO    ARRAY \GEO\ HOLDS THE INTERNAL COORDINATES.
C        GRAD   ON EXIT, CONTAINS THE DERIVATIVES
C
C***********************************************************************
      COMMON /KEYWRD / KEYWRD
      COMMON /ERRFN  / ERRFN(MAXPAR)
      COMMON /DOPRNT/ DOPRNT                                            LF0510
      LOGICAL DOPRNT                                                    LF0510
      include 'result.f'                                                LF1210
      CHARACTER*160 KEYWRD
      DIMENSION CHANGE(3), COORD(3,NUMATM), COLD(3,NUMATM*27)
     1,         XDERIV(3), XPARAM(MAXPAR), XJUC(3), W(N2ELEC)
      LOGICAL DEBUG, TIMES, HALFE, FAST, SCF1, CI, PRECIS
      EQUIVALENCE (W,WJ)
         SAVE                                                           GL0892
      DATA ICALCN /0/

      IF(ICALCN.NE.NUMCAL) THEN
         I = INDEX(KEYWRD,'PRESS')
         PRESS=0.D0
         IF(I.NE.0) PRESS=READA(KEYWRD,I)*1476.8992D0
         IDLO=NATOMS+1
         IF(LABELS(NATOMS) .EQ. 107) THEN
            IDLO=NATOMS
            IF(LABELS(NATOMS-1) .EQ. 107)THEN
               IDLO=NATOMS-1
               IF(LABELS(NATOMS-2) .EQ. 107)THEN
                  IDLO=NATOMS-2
               ENDIF
            ENDIF
         ENDIF
         GRLIM=0.01D0
         DEBUG = (INDEX(KEYWRD,'DERIV') .NE. 0)
         PRECIS= (INDEX(KEYWRD,'PRECIS') .NE. 0)
         TIMES = (INDEX(KEYWRD,'TIME') .NE. 0)
         CI    = (INDEX(KEYWRD,'C.I.') .NE. 0)
         SCF1  = (INDEX(KEYWRD,'1SCF') .NE. 0)
         ICALCN=NUMCAL
         IF(INDEX(KEYWRD,'RESTART') .EQ. 0) THEN
            DO 10 I=1,NVAR
   10       ERRFN(I)=0.D0
         ENDIF
         GRLIM=0.01D0
         IF(PRECIS)GRLIM=0.0001D0
         IF(INDEX(KEYWRD,'FULSCF') .GT.0) GRLIM=1.D9
         HALFE = (NOPEN.GT.NCLOSE .OR. CI)
         IDELTA=-7
*
*   IDELTA IS A MACHINE-PRECISION DEPENDANT INTEGER
*
         IF(HALFE.AND.PRECIS) IDELTA=-3
         IF(HALFE.AND..NOT.PRECIS) IDELTA=-3
         FAST=.TRUE.
         CHANGE(1)= 10.D0**IDELTA
         CHANGE(2)= 10.D0**IDELTA
         CHANGE(3)= 10.D0**IDELTA
C
C    CHANGE(I) IS THE STEP SIZE USED IN CALCULATING THE DERIVATIVES.
C    FOR "CARTESIAN" DERIVATIVES, CALCULATED USING DCART,AN
C    INFINITESIMAL STEP, HERE 0.000001, IS ACCEPTABLE. IN THE
C    HALF-ELECTRON METHOD A QUITE LARGE STEP IS NEEDED AS FULL SCF
C    CALCULATIONS ARE NEEDED, AND THE DIFFERENCE BETWEEN THE TOTAL
C    ENERGIES IS USED. THE STEP CANNOT BE VERY LARGE, AS THE SECOND
C    DERIVATIVE IN FLEPO IS CALCULATED FROM THE DIFFERENCES OF TWO
C    FIRST DERIVATIVES. CHANGE(1) IS FOR CHANGE IN BOND LENGTH,
C    (2) FOR ANGLE, AND (3) FOR DIHEDRAL.
C
         XDERIV(1)= 0.5D0/CHANGE(1)
         XDERIV(2)= 0.5D0/CHANGE(2)
         XDERIV(3)= 0.5D0/CHANGE(3)
      ENDIF
      GNORM=0.D0
      IF(NVAR.EQ.0) RETURN
      IF(DEBUG)THEN
         IF (DOPRNT) WRITE(6,'('' GEO AT START OF DERIV'')')            CIO
         IF (DOPRNT) WRITE(6,'(F19.5,2F12.5)')((GEO(J,I),J=1,3),I=1,    CIO
     &                     NATOMS)                                      CIO
      ENDIF
      DO 20 I=1,NVAR
         XPARAM(I)=GEO(LOC(2,I),LOC(1,I))
   20 GNORM=GNORM+GRAD(I)**2
      GNORM=SQRT(GNORM)
      FAST=(GNORM .GT. GRLIM .AND. .NOT. SCF1 .OR. .NOT. HALFE)
      TIME1=SECOND()
      IF(NDEP.NE.0) CALL SYMTRY(*9999)                                   CSTP(call)
      CALL GMETRY(GEO,COORD,*9999)                                       CSTP(call)
      IF( .NOT. FAST ) THEN
         IF(DEBUG.AND.DOPRNT)                                           CIO
     &                   WRITE(6,'('' DOING FULL SCF''''S IN DERIV'')') CIO
         CALL HCORE(COORD,H,W, WJ, WK, ENUCLR,*9999)                     CSTP(call)
         IF(NORBS*NELECS.GT.0)THEN
            CALL ITER(H, W, WJ, WK, AA,.TRUE.,.FALSE.,*9999)             CSTP(call)
         ELSE
            AA=0.D0
         ENDIF
         LINEAR=(NORBS*(NORBS+1))/2
         DO 30 I=1,LINEAR
   30    P(I)=PA(I)*2.D0
         AA=(AA+ENUCLR)
      ENDIF
      CALL DCART(COORD,DXYZ,*9999)                                       CSTP(call)
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C Get gradient of total energy with respect to each Cartesian coordinate.
C 
C This includes some redundancy for 5 (for linear) or 6 (for nonlinear
C molecules) degrees of freedom which can be removed from the total of 3N
C Cartesian coordinates, of course.  The dimension of the Cartesian
C coordinate-type gradient here is thus 5 or 6 larger than required
C to completely describe the gradient of the system and as such is
C actually reducible.
C
C Factor of two in derivatives appears at end of DCART subroutine
C when converting from eV to kcal/mol (uses factor of 46.122 instead
C of 23.061).  Also occurs in line 223 below.
      grnorm=0.0d0
      do myct=1,numat+3
c#        write(6,77) myct, dxyz(1,myct)
c#        write(6,78) myct, dxyz(2,myct)
c#        write(6,79) myct, dxyz(3,myct)
 77       format('Derivative of energy wrt atom #',I2,'''s x is ',F16.8)
 78       format('Derivative of energy wrt atom #',I2,'''s y is ',F16.8)
 79       format('Derivative of energy wrt atom #',I2,'''s z is ',F16.8)
c#        write(24,1111) 1,myct,dxyz(1,myct)
c#        write(24,1111) 2,myct,dxyz(2,myct)
c#        write(24,1111) 3,myct,dxyz(3,myct)
 1111  format('Increasing GNORM by square of DXYZ(',I1,',',I3'):',F16.8)
        grnorm=grnorm+(dxyz(1,myct)**2+dxyz(2,myct)**2+dxyz(3,myct)**2)
      enddo
      grnorm=dsqrt(grnorm)/2.0d0   ! Remove factor of two also.
      FGRADT=grnorm
c#      write(24,*) "Gradient norm (kcal/mol/angstrom) : ",grnorm
c#      write(6 ,*) "Gradient norm (kcal/mol/angstrom) : ",grnorm
      FGRPER=FGRADT/DBLE(3*NUMAT)
c#      write(24,*)"Gradient norm per Cartesian df (kcal/mol/angstrom): ",
c#     &      FGRPER
c#      write(6 ,*)"Gradient norm per Cartesian df (kcal/mol/angstrom): ",
c#     &      FGRPER
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      IF(NDEP.NE.0) CALL SYMTRY(*9999)                                   CSTP(call)
      CALL GMETRY(GEO,COORD,*9999)                                       CSTP(call)
      IJ=0
      DO 70 II=1,NUMAT
         DO 60 IL=L1L,L1U
            DO 60 JL=L2L,L2U
               DO 60 KL=L3L,L3U
                  DO 40 LL=1,3
   40             XJUC(LL)=COORD(LL,II)+TVEC(LL,1)*IL+TVEC(LL,2)*JL+TVEC
     1(LL,3)*KL
                  IJ=IJ+1
                  DO 50 KK=1,3
                     COLD(KK,IJ)=XJUC(KK)
   50             CONTINUE
   60    CONTINUE
   70 CONTINUE
      SUM11=1.D-9
      SUM22=1.D-9
      SUM12=1.D-9
      DO 150 I=1,NVAR
         K=LOC(1,I)
         L=LOC(2,I)
         XSTORE=XPARAM(I)
         DO 80 J=1,NVAR
   80    GEO(LOC(2,J),LOC(1,J))=XPARAM(J)
         GEO(L,K)=XSTORE-CHANGE(L)
         IF(NDEP.NE.0) CALL SYMTRY(*9999)                                CSTP(call)
         CALL GMETRY(GEO,COORD,*9999)                                    CSTP(call)
C#         CALL GEOUT
C
C    USE LOOKUP TABLE OF CARTESIAN DERIVATIVES TO WORK OUT INTERNAL
C    COORDINATE DERIVATIVE.
C
         TOTL=0.D0
         IJ=0
         DO 130 II=1,NUMAT
            IF(ID.EQ.0) THEN
               DO 90 LL=1,3
   90          TOTL=TOTL+DXYZ(LL,II)*(COORD(LL,II)-COLD(LL,II))
            ELSE
               DO 120 IL=L1L,L1U
                  DO 120 JL=L2L,L2U
                     DO 120 KL=L3L,L3U
                        DO 100 LL=1,3
  100                   XJUC(LL)=COORD(LL,II)+TVEC(LL,1)*IL+TVEC(LL,2)*J
     1L+TVEC(LL,3)*KL
                        IJ=IJ+1
                        TOTL=TOTL+DXYZ(1,IJ)*(XJUC(1)-COLD(1,IJ))
     &                           +DXYZ(2,IJ)*(XJUC(2)-COLD(2,IJ))
     &                           +DXYZ(3,IJ)*(XJUC(3)-COLD(3,IJ))
  120          CONTINUE
            ENDIF
  130    CONTINUE
         TOTL=TOTL*XDERIV(L)
C
C   IF NEEDED, CALCULATE "EXACT" DERIVATIVES.
C
         IF( .NOT. FAST ) THEN
            CALL HCORE(COORD,H,W, WJ, WK,ENUCLR,*9999)                   CSTP(call)
            IF(NORBS*NELECS.GT.0)THEN
               CALL ITER(H,W, WJ, WK,EE,.TRUE.,.FALSE.,*9999)            CSTP(call)
            ELSE
               EE=0.D0
            ENDIF
            DO 140 II=1,LINEAR
  140       P(II)=PA(II)*2.D0
            EE=(EE+ENUCLR)
            TOTL1=(AA-EE)*23.061D0*XDERIV(L)*2.D0
C#            WRITE(6,*)AA-EE
            ERRFN(I)=TOTL1-TOTL
         ENDIF
         GEO(L,K)=XSTORE
         SUM11=SUM11+GRAD(I)**2
         SUM22=SUM22+TOTL**2
         SUM12=SUM12+TOTL*GRAD(I)
         GRAD(I)=TOTL+ERRFN(I)
  150 CONTINUE
      IF(DEBUG.AND.DOPRNT) THEN                                         CIO
         WRITE(6,'('' GRADIENTS'')')
         WRITE(6,'(10F8.3)')(GRAD(I),I=1,NVAR)
         WRITE(6,'('' ERROR FUNCTION'')')
         WRITE(6,'(10F8.3)')(ERRFN(I),I=1,NVAR)
      ENDIF
      COSINE=SUM12/SQRT(SUM11*SUM22)
      IF(DEBUG.AND.DOPRNT)                                              CIO
     1WRITE(6,'('' COSINE OF SEARCH DIRECTION ='',F30.6)')COSINE        CIO
      IF( .NOT. FAST ) COSINE=1.D0
      IF(TIMES.AND.DOPRNT)                                              CIO
     1WRITE(6,'('' TIME FOR DERIVATIVES'',F12.6)')SECOND()-TIME1        CIO
      RETURN
 9999 RETURN 1                                                          CSTP
      ENTRY DERIV_INIT                                                  CSAV
                AA = 0.0D0                                              CSAV
            CHANGE = 0.0D0                                              CSAV
                CI = .FALSE.                                            CSAV
             DEBUG = .FALSE.                                            CSAV
                EE = 0.0D0                                              CSAV
              FAST = .FALSE.                                            CSAV
             GNORM = 0.0D0                                              CSAV
             GRLIM = 0.0D0                                              CSAV
             HALFE = .FALSE.                                            CSAV
                 I = 0                                                  CSAV
            ICALCN = 0                                                  CSAV
                ID = 0                                                  CSAV
            IDELTA = 0                                                  CSAV
              IDLO = 0                                                  CSAV
                II = 0                                                  CSAV
                IJ = 0                                                  CSAV
                 K = 0                                                  CSAV
                KK = 0                                                  CSAV
                 L = 0                                                  CSAV
            LINEAR = 0                                                  CSAV
                LL = 0                                                  CSAV
                NA = 0                                                  CSAV
            NALPHA = 0                                                  CSAV
                NB = 0                                                  CSAV
             NBETA = 0                                                  CSAV
                NC = 0                                                  CSAV
            NCLOSE = 0                                                  CSAV
            NELECS = 0                                                  CSAV
             NLAST = 0                                                  CSAV
            NMIDLE = 0                                                  CSAV
             NOPEN = 0                                                  CSAV
             NORBS = 0                                                  CSAV
            PRECIS = .FALSE.                                            CSAV
             PRESS = 0.0D0                                              CSAV
              SCF1 = .FALSE.                                            CSAV
             SUM11 = 0.0D0                                              CSAV
             SUM12 = 0.0D0                                              CSAV
             SUM22 = 0.0D0                                              CSAV
             TIME1 = 0.0D0                                              CSAV
             TIMES = .FALSE.                                            CSAV
              TOTL = 0.0D0                                              CSAV
             TOTL1 = 0.0D0                                              CSAV
              XJUC = 0.0D0                                              CSAV
            XDERIV = 0.0D0                                              CSAV
             COORD = 0.0D0                                              CSAV
             COLD  = 0.0D0                                              CSAV
            XPARAM = 0.0D0                                              CSAV
            XSTORE = 0.0D0                                              CSAV
      RETURN                                                            CSAV
      END
