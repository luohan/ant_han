# Change log
## Friday July 14th 2017
### Modified
- Add a function *scale.f90* to scale ppm and xxm to match erot and evib
- Add flag **scaleenergy**, **rotfreeze** to scale energy or remove angular momentum in initmol
- Change of indentation, extra debug output in *popnorm.f90*

## Mon July 10th 2017
### Modified
- Add a flag to force sample rotational energy for canonical ensemble irottemp, comment some code which change nvibmode

## Fri June 30th 2017
### Modified
- PES for n2o 1ap and 3app coupling
- **Tbug** in *popnorm.f90* of themal vibrational energy, variable tmp, and some indentation

## Fri June 16th 2017
### Added
- Varandas' 1A' PES for N2O. NOT FINISH YET
- *plot_pes.f90* for testing pes
- Gonzalez's original pes code
### Changed
- PESs are renamed

## Mon June 12th 2017
### Added 
- Gonzalez's PES for 1A', could not reproduce the collinear energy diagram in the paper, further modifications are needed
### Changed
- Improve 3A'' PES for rho2==rho3 or rho2==-rho3

## Mon May 29th 2017
### Added
- Finally add the accurate way to sample phase by 1) Integrate(dr/v,rin,rout)=T to get the relationship between intermolecular distance and time in *period.f90* 2) Linear interpolate this relationship to get molecular distance corresponding to the phase angle and use energy and angular momentum conservation to solve momentum in *atomdiatom.f90*
### Changed
- Some white space might be removed for formatting, redundant variables are also removed 

## Fri May 26th 2017
### Changed
- I made a stupid mistake yesterday by deleting some initialization in *N2O_GM.f90*, ALWAYS REMEMBER TO INITIALIZE
- Verify that extending rho works. But may not be the best choice.

## Thu May 25th 2017
### Changed
- *finalstate* better logics for detecting dissociation reaction. The reason why we couldn't detect dissociation reaction is ANT uses constant time step integration. Thus it's unable to propagate trajectories of dissociated molecule until more than 20A like mkqct.
- Change the bracketing region for *diamax* to 30 Bohr
- Add initialization in *brent*. This is a HUGE BUG!
- NOTE: REMEMBER IN FORTRAN, LOCAL VARIABLE IN FUNCTION AND SUBROUTINE WILL ONLY BE INITIALIZED ONCE.THIS IS A SO STUPID SET.
- Delete some whitespace
- Correct a bug **Tbug**: calculation of rmass is in wrong position in *preatomdiatom*, which results in wrong value of ppreli


## Sun May 21th 2017
### Changed
- Correct some constants in *param.f90* based on 2014 CODATA from NIST
- Correct bug in *diamin.f90*: wrong return
- Add the detection in diamin to avoid bracketing to infinity (not a smart method)
- *evwkb, vwkb, turn, finalstate* are changed for the following add
- Add debug, debug_opt target in *Makefile*
### Added
- Subroutine *diamax.f90*  to detect possible Vmax for quasibond state

## Fri May 19th 2017
### Changed
- correct a bug in Newconcate.max
- add python version post processing: *ant_post.sub* and *PostProcessStateSpecific.py* to replace *post30.sh* and *fort30post.m*. They are not much more efficient but more robust

## Mon May 15th 2017
### Changed
- comment a stop in vwkb for possible dissociation reaction

## Sun May 14th 2017
### Changed
- Modify functions to d version for N2O_GM.f90
- **Tbug** Correct a bug in initomol, which removes total angular momentum for atom-diatom collision
- **Tbug** Correct a bug in diapot2.f90 and diapot.f90

### Added
- Debug target for Makefile
- Write out initial angles and final scattering angle for atom-diatom collision
- Write out range of impact parameter
- Accept initial input for min and max impact parameter

## Sat May 13th 2017
### Changed
- Change the screen output for collisional energy and scattering energy
- Change Makefile to compile the pes with different file type
