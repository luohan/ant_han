      module wkb_mod
        implicit none
        integer,parameter :: MAXV=70,MAXJ=300
      end module wkb_mod

      program wkb_solver
        use wkb_mod,only : MAXV,MAXJ
        use c_initial,only:nsurf0,arrad,easym,rasym,phmode
        use c_struct,only : mmm,symbol0
        use param,only : autoev,autoang,mnphase
        implicit none
        real(8) :: Erv(MAXV+1,MAXJ+1),Ev(MAXV+1),a,b, mypi
        real(8) :: Rin(MAXV+1,MAXJ+1),Rout(MAXV+1,MAXJ+1),Tau(MAXV+1,MAXJ+1),vrmax
        real(8),allocatable,dimension(:,:,:) :: phase_ad, phase_ad2
        integer :: Vmax,JVmax(MAXJ+1), VJmax(MAXV+1),Jmax,hnphase

        integer :: i,j,k,fid,imid
        logical :: ierror
        character(len=100) :: fname, spname

        mypi = dacos(-1.0d0)
        write(6,'("...Entering READIN...",/)')
        call readin()
        write(6,'("...Exiting  READIN...",/)')
        write(6,'("...Entering ASYM_ANA...",/)')
        call asym_analysis()
        write(6,'("...Exiting  ASYM_ANA...",/)')


        ! Calculate rovibrational energy and find maximum vibrational level
        ! =========================================================================
        JVmax = -1
        Jmax = -1
        do j = 0,MAXJ
          call ewkb2(j,JVmax(j+1),arrad,mmm,Erv(:,j+1),Rin(:,j+1),Rout(:,j+1),nsurf0,ierror)
          if (JVmax(j+1) .ne. -1) then
            Jmax = j
          else
            exit
          end if
        end do
        write(*,"('Jmax = ',I4)") Jmax
        write(*,*) "JVmax:"
        write(*,*) JVmax(1:Jmax+1)

        ! Find maximum rotational level for different vibrational level
        ! =========================================================================
        Vmax = JVmax(1)
        do i = 0,Vmax
          do j = 0,Jmax
            if (i .le. JVmax(j+1)) then
              VJmax(i+1) = j
            else
              exit
            end if
          end do
        end do
        write(*,"('Vmax = ',I4)") Vmax
        write(*,*) "VJmax:"
        write(*,*) VJmax(1:Vmax+1)

        ! Calculate phase angle distribution of all ground rotational level
        !=======================================================================
        if (phmode == 2) then
          hnphase = (mnphase-1)/2+1
          allocate(phase_ad(mnphase,4,Vmax+1), phase_ad2(mnphase,4,Vmax+1))
          ! phase_ad(:,1,:): 0 - > 1 time
          ! phase_ad(:,2,:): intermolecule distance
          ! phase_ad(:,3,:): velocity
          ! phase_ad(:,4,:): effective phase angle for velocity
          ! phase_ad2 start with r=rin
          ! phase_ad  start with v = vmax, r = req+
          j = 0
          write(*,*) 'Start calculation of phase'
          do i = 0,Vmax
            call period(Erv(i+1,j+1),j,arrad,mmm,Rin(i+1,j+1),Rout(i+1,j+1),Tau(i+1,j+1), &
              phase_ad2(:,1:2,i+1),nsurf0,phmode,phase_ad2(:,3,i+1))
            write(*,'("V=",I3," J=",I3," Period(au)=",G13.5)') i,j,Tau(i+1,j+1)
            ! convert velocity back to effective angle
            vrmax = maxval(dabs(phase_ad2(:,3,i+1)))
            phase_ad2(1,4,i+1) = 0.0d0
            imid = 1 ! index for r = req, v = vmax
            do k = 2,hnphase
              if (phase_ad2(k-1,3,i+1) < phase_ad2(k,3,i+1)) then
                imid = k
                phase_ad2(k,4,i+1) = asin(phase_ad2(k,3,i+1)/vrmax)
              else
                phase_ad2(k,4,i+1) = mypi - asin(phase_ad2(k,3,i+1)/vrmax)
              end if
            end do
            do k = hnphase+1, mnphase
              if (phase_ad2(k-1,3,i+1) > phase_ad2(k,3,i+1)) then
                phase_ad2(k,4,i+1) = mypi - asin(phase_ad2(k,3,i+1)/vrmax)
              else
                phase_ad2(k,4,i+1) = mypi*2.0d0 + asin(phase_ad2(k,3,i+1)/vrmax)
              end if
            end do

            do k = imid,mnphase
              phase_ad(k-imid+1,1,i+1) = phase_ad2(k,1,i+1) - phase_ad2(imid,1,i+1)
              phase_ad(k-imid+1,2,i+1) = phase_ad2(k,2,i+1)
              phase_ad(k-imid+1,3,i+1) = phase_ad2(k,3,i+1)
              phase_ad(k-imid+1,4,i+1) = phase_ad2(k,4,i+1) - phase_ad2(imid,4,i+1)
            end do

            do k = 2, imid
              phase_ad(mnphase-imid+k,1,i+1) = phase_ad2(k,1,i+1) + phase_ad(mnphase-imid+1,1,i+1)
              phase_ad(mnphase-imid+k,2,i+1) = phase_ad2(k,2,i+1)
              phase_ad(mnphase-imid+k,3,i+1) = phase_ad2(k,3,i+1)
              phase_ad(mnphase-imid+k,4,i+1) = phase_ad2(k,4,i+1) + phase_ad(mnphase-imid+1,4,i+1)
            end do

          end do
        end if


        ! Output
        ! =======================================================================
  221   format(1a,1a)
        select case (arrad)
        case (1)
          write(spname,221) trim(adjustl(symbol0(1))),trim(adjustl(symbol0(2)))
        case (2)
          write(spname,221) trim(adjustl(symbol0(2))),trim(adjustl(symbol0(3)))
        case (3)
          write(spname,221) trim(adjustl(symbol0(1))),trim(adjustl(symbol0(3)))
        end select

        ! write first name containing all the rovibrational ladder
        ! =========================================================================
  222   format(a,"_ladder.txt")
        write(fname,222) trim(spname)

        fid = 10
        open(fid,FILE=trim(fname))
        write(fid, '(a)') "Rovibrational ladder for:"
        write(fid, '(5x,a)') trim(spname)
        write(fid, '(a)') "Vmax:"
        write(fid,*) Vmax
        write(fid, '(a)') "Jmax:"
        write(fid,*) (VJmax(i),i=1,Vmax+1)
        write(fid, '(a)') "WellDepth: in eV"
        write(fid,*) easym(nsurf0, arrad)*autoev
        write(fid, '(a)') "Rmin: in A"
        write(fid,*) rasym(nsurf0, arrad)*autoang
        write(fid, '(a)') "Ev: in eV"
        do i = 0,Vmax
          write(fid,'(1000(G20.12,2X))') Erv(i+1,1)*autoev
        end do
        write(fid, '(a)') "Erv: in eV"
        do i = 0,Vmax
          write(fid,'(1000(G20.12,2X))') (Erv(i+1,j)*autoev,j=1,VJmax(i+1)+1)
        end do
        write(fid, '(a)') "Rin: in A"
        do i = 0,Vmax
          write(fid,'(1000(G20.12,2X))') (Rin(i+1,j)*autoang,j=1,VJmax(i+1)+1)
        end do
        write(fid, '(a)') "Rout: in A"
        do i = 0,Vmax
          write(fid,'(1000(G20.12,2X))') (Rout(i+1,j)*autoang,j=1,VJmax(i+1)+1)
        end do
        close(fid)


        ! write second file containing the phase angle distribution
       write(*,*) "Write phase to file ..."
  223  format(a,"_vphase.bin")
       write(fname,223) trim(spname)
       fid = 11
       open(fid, file=fname, form='unformatted')
       write(fid) Vmax
       write(fid) mnphase
       write(fid) ((phase_ad(i,1,k),i=1,mnphase),k=1,Vmax+1)
       write(fid) ((phase_ad(i,4,k),i=1,mnphase),k=1,Vmax+1)
       close(fid)

       write(fname,'(a,"_vphase.dat")') trim(spname)
       fid = 12
       open(fid,file=fname)
       write(fid,'(a)') 'VARIABLES = "t/tau", "dis (au)", "velocity (au)", "phi"'
       do i = 0,Vmax
         write(fid,'("ZONE I = ",I6," T = ""V=",I6,"""")') mnphase, i
         do k=1,mnphase
           write(fid,'(4(E16.9,1x))') (phase_ad(k,j,i+1),j=1,4)
         end do
         write(fid,*)
       end do
       close(fid)
       deallocate(phase_ad, phase_ad2)

       ! open(fid,file='nn_vphase.bin', form='unformatted', action='read')
       ! read(fid) Vmax
       ! read(fid) mnphase
       ! allocate(phase_ad(mnphase,2,Vmax+1))
       ! read(fid) phase_ad(:,1,:)
       ! read(fid) phase_ad(:,2,:)
       ! write(*,*) phase_ad(:,1,Vmax+1)
       ! close(fid)
       ! deallocate(phase_ad)


      end program wkb_solver

!################################################################################
      subroutine readin()
        use param,only  : mnat,autoev,amutoau,autoang
        use c_initial,only: arrad,initx,nsurf0,nsurfi,initp,phmode
        use c_struct,only : ldiat,nmol,natom,nat,icharge,imulti,iatom,symbol0,&
           mm0,icell,mmofag,mmm,symbolm,indatom,indmm,xxm,xx0
        use c_sys,only    : nsurft,potflag
        use atomdata, only: atmradco, atomsymbols

        implicit none
        character(len=200) :: records
        integer :: endreccon, ifkeyexist,im,nmodel,ii,i,j
        logical :: atomdiatom=.false.,find_real
        real(8) :: mtotal
                character(len=200),external :: lowercase

        ! initilize
        icell  = 0
        nsurf0 = 1
        nsurft = 1
        ldiat  = .false.
        initx  = 4
        xx0 = 0.d0

        open(12,status='scratch')
        open(13,status='scratch')
! write(6,*) 'Your inputfile:'
        do while(.true.)
          read(5,'(a)',end=100) records
          if (records(1:1) .ne. '#') then
            records = lowercase(records)
            write(12,'(a)') records
! write(6,'(a)') records
          end if
        enddo
 100    continue
        write(12,'(a)') 'thend0'
        rewind(12)
        read(12,'(a)',end=9999) records
        do while (.true.)
          if (index(records,'$control') .GT. 0) then
            do while(.true.)
!          Potential used
              ifkeyexist=index(records,'potflag')
              if (ifkeyexist .GT. 0) then
                write(13,*) records(ifkeyexist+len('potflag')+1:)
                rewind(13)
                read(13,*) potflag
                rewind(13)
              end if
              if (index(records,'$end') .GT. 0) then
                exit
              else
                read(12,'(a)',end=9999) records
              end if
            enddo
            read(12,'(a)',end=9999) records
          end if

          if (index(records,'$atomdiatom') .GT. 0) then
!         if input for $atomdiatom appears, it is a special triatom run
            atomdiatom = .true.
            do while(.true.)
! Initial molecular arrangement, 1: AB+C, 2: BC+A, 3: AC+B
              ifkeyexist=index(records,'arrad')
              if (ifkeyexist .GT. 0) then
                write(13,*) records(ifkeyexist+len('arrad')+1:)
                rewind(13)
                read(13,*) arrad
                rewind(13)
              end if
! Controls how the diatomic potential is computed
              ifkeyexist=index(records,'flagdiat')
              if (ifkeyexist .GT. 0) then
                write(13,*) records(ifkeyexist+len('flagdiat')+1:)
                rewind(13)
                ldiat=.true.
              end if
! Atom diatom Phase Modes:
              ifkeyexist=index(records,'phmode')
              if (ifkeyexist .GT. 0) then
                write(13,*) records(ifkeyexist+len('phmode')+1:)
                rewind(13)
                read(13,*) phmode
                if(phmode.lt.1.or.phmode.gt.2) then
                  write(6,*) "Error in Atom-diatom phase mode"
                  write(6,*) "Currently only mode=1,2 are available."
                  stop
                endif
                rewind(13)
              endif
! Meet the end of this section
              if (index(records,'$end') .GT. 0) then
                exit
              else
                read(12,'(a)',end=9999) records
              end if
            enddo
            read(12,'(a)',end=9999) records
          end if

          if (index(records,'$surface') .GT. 0) then
            do  while(.true.)
!          Initial electronic surface
              ifkeyexist=index(records,'nsurf0')
              if (ifkeyexist .GT. 0) then
                write(13,*) records(ifkeyexist+len('nsurf0')+1:)
                rewind(13)
                read(13,*) nsurf0
                rewind(13)
              end if
!          Total number of electronic surfaces
              ifkeyexist=index(records,'nsurft')
              if (ifkeyexist .GT. 0) then
                write(13,*) records(ifkeyexist+len('nsurft')+1:)
                rewind(13)
                read(13,*) nsurft
                rewind(13)
              end if
              if (index(records,'$end') .GT. 0) then
                exit
              else
                read(12,'(a)',end=9999) records
              end if
            enddo
            read(12,'(a)',end=9999) records
          end if

          if (index(records,'$data') .GT. 0) then
!          Number of molecules or atom groups (AGs)
            read(12,'(a)',end=9999) records
            ifkeyexist=index(records,'nmol')
            if (ifkeyexist .GT. 0) then
              write(13,*) records(ifkeyexist+len('nmol')+1:)
              rewind(13)
              read(13,*) nmol
              rewind(13)
              if (atomdiatom .and. nmol.ne.1) then
                write(6,108) 'ERROR: atom-diatom calculation cant', &
                  ' be used with multi AGs (nmol>1)'
                stop
              end if
              read(12,'(a)',end=9999) records
            end if
! Data for each AG, loop over nmol AGs, had to be formated at present
! version of ants
            nat = 0
            do im=1,nmol
              ifkeyexist=index(records,'natom')
              if (ifkeyexist .GT. 0) then
                write(13,*) records(ifkeyexist+len('natom')+1:)
                rewind(13)
                read(13,*) natom(im)
                rewind(13)
                if (natom(im).ne. 3) then
                  write(6,*)'ERROR: WKB solver only works with initx=4, natom=3'
                  stop
                end if
              end if
              ifkeyexist=index(records,'initx')
              if (ifkeyexist .GT. 0) then
                write(13,*) records(ifkeyexist+len('initx')+1:)
                rewind(13)
                read(13,*) initx(im)
                rewind(13)
                if (initx(im).ne.4)  then
                  write(6,*)   'ERROR:WKB solver must be used with initx=4'
                  stop
                end if
              end if
              if (atomdiatom) then
                initx(im) = 4
                initp(im) = -1
              end if
              iatom(im) = nat + 1
              nat = nat+natom(im)
              if (nat.gt.mnat) then
                write(6,222) 'ERROR: the number of all atoms cant be', &
                  ' larger than',mnat
                stop
              end if
              read(12,*) icharge(im),imulti(im)
              do i = iatom(im), iatom(im)+natom(im)-1
                read(12,*) symbol0(i), mm0(i)
              enddo
              read(12,'(a)',end=9999) records !$end
            enddo
!        Move to next line
            read(12,'(a)',end=9999) records
          end if
! Meet the end of file
          if (index(records,'thend0') .gt. 0 ) then
            exit
          else
            read(12,'(a)',end=9999) records
          end if
        end do
9999    continue
        close(12)
        close(13)

        select case (arrad)
        case (1)
          write(6,'("Solve Erv for",1x,a,a)') symbol0(1),symbol0(2)
        case (2)
          write(6,'("Solve Erv for",1x,a,a)') symbol0(2),symbol0(3)
        case (3)
          write(6,'("Solve Erv for",1x,a,a)') symbol0(1),symbol0(3)
        end select

        write(6,*) 'POTFLAG = ',potflag

        if (potflag.eq.0) then
          write(6,*) 'Using the POTLIB MM-HO-1 interface'
          if (nsurft.ne.1) then
            write(6,108) 'POTLIB MM-HO-1 is for SINGLE surface only, ', &
              'NSURFT should be 1'
            stop
          end if
        elseif (potflag.eq.1) then
          write(6,*) 'Using the POTLIB 3-2V interface'
          if (nat.ne.3) then
            write(6,*) 'POTLIB 3-2V works only for 3 atoms'
            stop
          end if
          if (nsurft.ne.2) then
            write(6,*)'NSURFT = ',nsurft,' is not allowed for POTFLAG = 1'
            stop
          end if
        elseif (potflag.eq.2) then
          write(6,*) 'Using the POTLIB MM-HE-1 interface'
          if (nsurft.ne.1) then
            write(6,108) 'POTLIB MM-HE-1 is for SINGLE surface only, ', &
              'NSURFT should be 1'
            stop
          end if
        end if
        write(6,*) 'Doing a ',nmol,' AG calculation'
        write(6,*) 'Simulation for an isolated AG in vacuum'
        write(6,*) 'Initial electronic suface = ',nsurf0
        nsurfi = nsurf0
        write(6,*) 'Number of electronic surfaces = ',nsurft
        nmodel = 0
        do i=1,nat
          find_real = .false.
          do j=1,111
            if ( symbol0(i).eq.atomsymbols(j)) then
              indatom(i)=j
              find_real=.true.
            endif
          enddo
          if(.not.find_real) then
            nmodel = nmodel + 1
            indatom(i)= nmodel
          endif
        enddo

        write(6,'(1x,a10,4a12)') '    # symb','mass (amu)'
        im = 1
        mmofag(im)=0.d0
        do i = 1, natom(1)
          write(6,920) i,symbol0(i),mm0(i)
!          Convert from amu to au
          mm0(i)     = mm0(i)*amutoau
          mmofag(im) = mmofag(im)+mm0(i)

          ii=iatom(im)+i-1
          mmm(i)     = mm0(ii)
          mtotal = mtotal + mmm(i)
          symbolm(i) = symbol0(ii)
          indmm(i)   = indatom(ii)
          do j=1,3
            xxm(j,i)=xx0(j,ii)
          enddo
        enddo

   920  format(1x,i5,a5,4f12.5)
   108  format(1x,10a)
   222  format(1x,2a,i10)

      end subroutine readin
!################################################################################
      function lowercase (string)
        character(len=200) :: string, lowercase
        character(len=1)   :: xlett
!
        do i = 1,200
          xlett = string(i:i)
          itry = ichar (xlett)

          if (xlett .ge. 'A' .and. xlett .le. 'Z') then
            itry = itry + 32
            string (i:i) = char (itry)
          endif
        end do
        lowercase  = string

      end function lowercase
!################################################################################
      subroutine asym_analysis()

        use param, only: autofs, autoang, autoev
        use c_struct, only: mmm,symbol0
        use c_sys,    only: nsurft
        use c_initial, only: arrad,easym,rasym,nsurf0
        implicit none

        real(8) :: etmp,eelec,rmin
        integer :: isurf,iarr,ic
        logical :: ierror
        write(6,*)'Asymptotic analysis'
        write(6,*)' surf  arr      Emin (eV)      Rmin (A)'
        do iarr=1,3
          ic = iarr+1
          if (ic > 3) ic=1
          do isurf=1,nsurft
            call diamin(rmin,etmp,-1.d0,iarr,mmm,isurf,ierror)
            easym(isurf,iarr) = etmp
            rasym(isurf,iarr) = rmin
            write(6,50) isurf,symbol0(iarr),symbol0(ic),etmp*autoev,rmin*autoang
          enddo
        enddo
   50   format(i5,1x,a,'-',a,2f15.5)
  111   format(1x,a,i5,a,i5,a,i5,a,i5)
   100  format(1x,a,20x,f13.5,a)

        write(6,111)'Preparing the diatom in arrangement ',arrad
        write(6,*)
        eelec = easym(nsurf0,arrad)
        write(6,*)'Energetics (relative to zero of E)'
        write(6,*)'At the classical minimum in this arrangement:'
        write(6,100)'Re    = ',rasym(nsurf0,arrad)*autoang,' A'
        write(6,100)'Eelec = ',eelec*autoev,' eV'
      end subroutine asym_analysis
!################################################################################
    ! # subroutine ewkb2(ji,vi,arri,mm,erotvib,rin,rout,nsurf,ierror)
      subroutine ewkb2(ji,jvmax,arri,mm,erotvib,rin,rout,nsurf,ierror)

! This subroutine is specialized for atom-diatom initial conditions.
! It computes the WKB energy for the rovibrational state (vi,ji).
! INPUT
! ji = initial rot state of diatom
! vi = initial vib state of the diatom
! arri = initial arrangement
! mm = masses of the atoms
! OUTPUT
! erotvib = rivibrational energy for (vi,ji) state
      use wkb_mod,only : MAXV, MAXJ
      use param, only: mnat, autoev, autoang
      implicit none
      integer,intent(in) :: ji,arri,nsurf
      real(8),intent(in) :: mm(mnat)
      real(8),intent(out) :: erotvib(MAXV+1),rin(MAXV+1),rout(MAXV+1)
      integer,intent(out) :: jvmax
      logical :: ierror

      real(8) :: rmin,rmax,emin,xj,emax2,emax,tinyv,xv0,xvmax,f,fmid
      real(8) :: rtbis_vib,dx,etry,xv,rint,routt
      integer :: JMAX,j,vi
      parameter(tinyv=1.0d-12)

      ierror = .false.
      xj = dble(ji)

      write(6,"('------------------- J=',I3,' ---------------------------')") ji
      ! find diamin and diamax for rotational
      call diamin(rmin,emin,xj,arri,mm,nsurf,ierror)
      if (ierror)  return

      call diapot(50.d0,arri,emax2,0.d0,mm,nsurf)
      call diamax(rmax,emax,xj,arri,mm,nsurf,rmin,30.0d0,ierror)
      if  (ierror) return
      if (dabs(emax2-emax) > tinyv) then
         write(6,*) 'This rotational state could have quasi-bond'
      else
         rmax = 50.d0
         emax = emax2   !right bond for turn
      endif

      ! compute vibrational action at E = emin
      call vwkb(arri,mm,emin,xj,xv0,rint,routt,nsurf,rmin,rmax,emin,emax,ierror)
      write(6,'("Emin=",F10.5," eV, Vx0=",F10.5)') emin*autoev,xv0
      if (ierror) return
      ! compute vibrational action at E = Emax
      call vwkb(arri,mm,emax,xj,xvmax,rint,routt,nsurf,rmin,rmax,emin,emax,ierror)
      write(6,'("Emax=",F10.5," eV, Vxmax=",F10.5)') emax*autoev,xvmax
      if (ierror) return


      jvmax = -1
      do vi=0,MAXV
! Now determine energy of (vi,ji) state
        f = xv0 - dble(vi)
        fmid = xvmax - dble(vi)

        if (f*fmid.ge.0.d0) then
          !write(6,1000)
          ierror = .true.
          exit
        end if
        if (f.lt.0.d0) then
          rtbis_vib=emin
          dx=emax-emin
        else
          rtbis_vib=emax
          dx=emin-emax
        end if

        JMAX=100
        do j = 1, JMAX
          dx=dx*0.5d0
          etry=rtbis_vib+dx
          call vwkb(arri,mm,etry,xj,xv,rint,routt,nsurf,rmin,rmax,emin,emax,ierror)
          if (ierror) then
            exit
          end if
!          write(6,*)'trying...',etry*27.211d0
!          write(6,*)'vibrational action at etry = ',xv
          fmid = xv - dble(vi)
          if (fmid.le.0.d0) then
            rtbis_vib=etry
          elseif (abs(dx).lt.1.d-13.or.fmid.eq.0.d0) then
            exit
          end if
        end do
        if (j == JMAX .or. ierror) then
          write(6,'("No root is found for J=",I3," V=",I3)') ji,vi
          exit
        else
          write(6,1100) vi,etry*autoev,rint*autoang,routt*autoang
          jvmax = vi
          erotvib(vi+1) = etry
          rin(vi+1) = rint
          rout(vi+1) = routt
        end if
      end do
 1000 format(2x,'root must be bracketed in rtbis_vib, check ewkb.f90')
 1100 format(2x,'V=',I3,2x,"Erv=",F9.5,"eV",2x,"(Ri,Ro)=",2F9.5)

      end subroutine ewkb2
