! function to calculate bond length
       double precision function bond(p1,p2)
       implicit none
       integer i
       double precision p1(3),p2(3),v(3)
       bond = 0d0
       call vect(v,p1,p2)
       do i = 1, 3
         bond = bond + v(i)**2
       enddo
       bond = sqrt(bond)
       end function bond
!
! function to calculate bond angle
       double precision function angle(p1,p2,p3)
       implicit none
       integer i,j
       double precision p1(3),p2(3),p3(3),v1(3),v2(3)
       double precision d,va1,va2
       call vect(v1,p1,p2)
       call vect(v2,p3,p2)
       d = dot_product(v1,v2)
       va1 =sqrt(v1(1)**2+v1(2)**2+v1(3)**2)
       va2 =sqrt(v2(1)**2+v2(2)**2+v2(3)**2)      
       angle = acos(d/va1/va2) 
       
       end function angle

! RMP
!
! subroutine which computes the dihedral angles
! As described in Appendix of J. Comp. Chem. 2013, 34, 1842-1849
!
      double precision function dihedral(p1,p2,p3,p4)
      use param, only: pi
      implicit none
      double precision p1(3),p2(3),p3(3),p4(3),norm
      double precision v1(3),v2(3),v3(3),v4(3),n1(3),n2(3), dot1, dot2
      call vect(v1,p2,p1)
      call vect(v2,p3,p2)
      call vect(v3,p4,p3)
      call acrossb(v2,v3,n1)
      call acrossb(v1,v2,n2)
      v4(:)=norm(v2)*v1(:)
!        dihedral=atan2(dot_product(v4,n1),dot_product(n2,n1))*57.2958d0
      dot1=dot_product(v4,n1)
      dot2=dot_product(n2,n1)
      if((dot1.eq.0d0).and.(dot2.eq.0d0))then  ! avoid the undefined case for atan2
        dihedral=pi  ! any value should work here 
      else
        dihedral=atan2(dot1, dot2)
      endif
!xxx      dihedral=atan2(dot_product(v4,n1),dot_product(n2,n1))

      end function dihedral
!
! RMP
! Out-of-plane bending angle (Wilson-type out of plane angle coordinate)
! This subroutine computes the angle defined between the bond vector I-J 
! and the plane defined by the atoms J-K-L. Note that is sensitive to
! the order of the atoms, the central atom must be the second one. 
!
      double precision function oopba(p1,p2,p3,p4)
      implicit none
      double precision norm,theta
      double precision p1(3),p2(3),p3(3),p4(3)
      double precision v1(3),v2(3),v3(3),v4(3),n1(3),n2(3)
      call vect(v1,p4,p2)
      v1(:)=v1(:)/norm(v1)
      call vect(v2,p3,p2)
      v2(:)=v2(:)/norm(v2)
      theta=acos(dot_product(v1,v2))
      call acrossb(v1,v2,n1)
      n1(:) = n1(:)/sin(theta)
      call vect(v3,p1,p2)
      v3(:)=v3(:)/norm(v3)
      oopba = asin(dot_product(n1,v3))

!     call vect(v1,p4,p2)
!     v1(:)=v1(:)/norm(v1)
!     call vect(v2,p4,p3)
!     v2(:)=v2(:)/norm(v2)
!     theta=acos(dot_product(v1,v2))
!     call acrossb(v1,v2,n1)
!     call vect(v3,p4,p1)
!     v3(:)=v3(:)/norm(v3)
!     oopba = acos(dot_product(n1,v3)/sin(theta))

      end function oopba

      subroutine vect(v,a,b)
      implicit none
      integer i
      double precision v(3),a(3),b(3)
      do i = 1, 3
        v(i) = a(i) - b(i)
      enddo
      end subroutine vect

      subroutine acrossb(a,b,axb)
      implicit none
      double precision a(3), b(3), axb(3)
      axb(1)= a(2)*b(3)-a(3)*b(2)
      axb(2)= a(3)*b(1)-a(1)*b(3)
      axb(3)= a(1)*b(2)-a(2)*b(1)
      end subroutine acrossb

! RMP
! Subroutine to transform the geometry from Cartesian into isoinertial
! or vice versa
! t=1  From Cartesian to isoinertial
! t=2  From isoinertial to Cartesian
       subroutine cartoiso(xx,nat,mass,t)
       use param, only: mnat, pi, mu
       implicit none
       integer :: i,j,t,nat
       double precision :: xx(3,mnat),mass(mnat)
       do i=1,nat
         do j=1,3
           if (t.eq.1) xx(j,i)=xx(j,i)*sqrt(mass(i)/ mu)
           if (t.eq.2) xx(j,i)=xx(j,i)*sqrt(mu/mass(i))
         enddo
       enddo
       end subroutine cartoiso

! RMP
! Subroutine which computes the length between two geometries
       subroutine length(nat,x1,x2,leng)
       use param, only: mnat
       implicit none
       integer :: nat,i,j
       double precision :: xd(3,mnat),x1(3,mnat),x2(3,mnat),leng
       do i=1,nat
         do j=1,3
           xd(j,i)=x2(j,i)-x1(j,i)
         enddo
       enddo
       leng=0.d0
       do i=1,nat
         do j=1,3
           leng=leng+xd(j,i)**2.d0
         enddo
       enddo
       leng=sqrt(leng)
       end subroutine length

! RMP
! Subroutine to normalize a vector (2D array)
      subroutine normd(v,nat)
      use param, only: mnat

      implicit none
      integer :: i,j,nat
      double precision :: norm
      double precision :: v(3,mnat) 
      norm=0.d0
      do i=1,nat
        do j=1,3
          norm = norm + v(j,i)**2.d0
        enddo
      enddo
      norm=sqrt(norm) 
      do i=1,nat
        do j=1,3
          v(j,i)=v(j,i)/norm
        enddo
      enddo
      end subroutine normd
! RMP
      !********************
      ! Norm of one vector 
      !********************
      double precision function norm(v)
      implicit none
      double precision, dimension(3) :: v
      norm=sqrt(v(1)**2 + v(2)**2 + v(3)**2)
      end function norm
