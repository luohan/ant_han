      subroutine rijmatr
! 
!    Input:
! natoms            :  number of atoms
! xxm(3,mnat)       :  cartesian coord.
! indmm(mnat)       :  Index of atom in periodic table
! rijco(111,111)    :  Covalent bond threshold (au)
!
!     Output: 
! rij(mnat,mnat)         : distance matrix
! rij2(mnat,mnat)        : square distance matrix
! bndind(mnbond,mnat)    : bond information
! bndmatr(mnat,mnat)     : bond information matrix (0,1)
! nbond(mnat)            : maximum number atoms of an atom bonded to
      use c_struct, only: natoms, rij, rij2, bndmatr, nbond, xxm, rijco, &
                          bdbrthr, bndind, cell, indmm, bdfmthr
      use c_sys, only: periodic
      use c_initial, only: equilibrium
      use c_term, only: termflag

      implicit none
      integer i,j,k,l
      double precision rijmax,dx,dy,dz
!  Initialize rij matrix
!      do i=1,natoms
!        do j=1,natoms
!          rij(i,j) = 0.0d0
!          rij2(i,j) = 0.0d0
!          bndmatr(i,j)=0
!        enddo
!      enddo
!  Calculating distance matrix
      do i=1,natoms
        rij(i,i) = 0.0d0
        rij2(i,i) = 0.0d0
        bndmatr(i,i)=0
        nbond(i)=0
        do j=i+1,natoms
          dx=xxm(1,i)-xxm(1,j)
          dy=xxm(2,i)-xxm(2,j)
          dz=xxm(3,i)-xxm(3,j)
          if (periodic) then
            dx=dx-cell(1)*anint(dx/cell(1))
            dy=dy-cell(2)*anint(dy/cell(2))
            dz=dz-cell(3)*anint(dz/cell(3))
          endif
          rij2(i,j) = dx*dx + dy*dy + dz*dz
          rij(i,j)  = dsqrt( rij2(i,j) )
          rij(j,i)  = rij(i,j)
          rij2(j,i) = rij2(i,j)
          if (termflag.eq.7) then
            rijmax=bdbrthr*rijco(indmm(i),indmm(j))
!   Make the bdbrthr a little smaller in equilibrium than in DRIVER
          elseif (equilibrium) then
            rijmax=bdbrthr*.95d0*rijco(indmm(i),indmm(j))
          else
            rijmax=bdfmthr*rijco(indmm(i),indmm(j))
          endif
          if (rij(i,j).le.rijmax) then
            bndmatr(i,j) =1
            nbond(i) = nbond(i)+1
            bndind( nbond(i),i )=j
           else
            bndmatr(i,j) =0
          endif
          bndmatr(j,i)=bndmatr(i,j)
        enddo
        do j=1,i-1
          if (bndmatr(i,j).eq.1) then
            nbond(i) = nbond(i)+1
            bndind( nbond(i),i )=j
          endif
        enddo
!        do j=1,natoms
!          if (bndmatr(i,j).eq.1) then
!            nbond(i) = nbond(i)+1
!            bndind( nbond(i),i )=j
!          endif
!        enddo
      enddo
!      if (idebug) then
!        write(6,*) 'Rij Matrix'
!        do i=1,natoms
!          write(6,100) (rij(i,j)*autoang,j=1,natoms)
!        enddo
!        write(6,*) 'Bond information Matrix'
!        do i=1,natoms
!          write(6,200) (bndmatr(i,j),j=1,natoms)
!        enddo
!        write(6,*) 'Bond information'
!        do i=1,natoms
!          write(6,200) i,(bndind(j,i),j=1,nbond(i))
!        enddo
!      endif
! 100  format(1x,1000f10.4)
! 200  format(1x,1000i5)
!
      end subroutine rijmatr
