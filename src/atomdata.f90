      module atomdata
! Atomic symbols are stored in an array atomsymbols(111)
! Atomic names are stored in an array atomnames(111)
! Atomic radii in Angstrom, taken from www.webelements.com
! atmradsl: Slater empirical: J.C. Slater, J.Chem.Phys.1964,39,3199
! atmradcl: Clementi calculated: J.Chem.Phys.1963,38,2686
! atmradco: Covalent
! atmradvn: Van der Waals
!           Atomic radiis which are 0.00d0 are not available 
! atomwts:  Standard atomic weights IUPAC 2001: 
! Important: For the covalent radius of atom number from 58-70, 
! 88-95, they equal to slater radius, while for 84 and 85, they are
! Clementi radius; For Fr, an value of 2.15 is set, while for the rest
! (96-111), arbitrary 1.75 is set
! atmnspin: Nuclear spin degeneracy
!        Quantities, Units, and Symbols in Physical Chemistry,
      character*2 :: atomsymbols(111)
      character*15 :: atomnames(111)
      double precision :: atmradsl(111),atmradcl(111),atmradco(111), &
                          atmradvn(111),atomwts(111),atmnspin(111)
!
!     data atomsymbols /'H','He','Li','Be','B','C','N','O','F','Ne',
!    & 'Na','Mg','Al','Si','P','S','Cl','Ar',
!    & 'K', 'Ca','Sc','Ti','V', 'Cr','Mn','Fe','Co','Ni','Cu','Zn',
!    & 'Ga','Ge','As','Se','Br','Kr',
!    & 'Rb','Sr','Y', 'Zr','Nb','Mo','Tc','Ru','Rh','Pd','Ag','Cd',
!    & 'In','Sn','Sb','Te','I', 'Xe',
!    & 'Cs','Ba','La','Ce','Pr','Nd','Pm','Sm','Eu','Gd','Tb','Dy',
!    & 'Ho','Er','Tm','Yb',
!    & 'Lu','Hf','Ta','W', 'Re','Os','Ir','Pt','Au','Hg','Tl','Pb',
!    & 'Bi','Po','At','Rn',
!    & 'Fr','Ra','Ac','Th','Pa','U', 'Np','Pu','Am','Cm','Bk','Cf',
!    & 'Es','Fm','Md','No',
!    & 'Lr','Rf','Db','Sg','Bh','Hs','Mt','Ds','Rg'/
! change all symbols to lower case to make input case insensitive
      data atomsymbols /'h','he','li','be','b','c','n','o','f','ne', &
       'na','mg','al','si','p','s','cl','ar', &
       'k', 'ca','sc','ti','v', 'cr','mn','fe','co','ni','cu','zn',&
       'ga','ge','as','se','br','kr', &
       'rb','sr','y', 'zr','nb','mo','tc','ru','rh','pd','ag','cd',&
       'in','sn','sb','te','i', 'xe', &
       'cs','ba','la','ce','pr','nd','pm','sm','eu','gd','tb','dy',&
       'ho','er','tm','yb', &
       'lu','hf','ta','w', 're','os','ir','pt','au','hg','tl','pb',&
       'bi','po','at','rn', &
       'fr','ra','ac','th','pa','u', 'np','pu','am','cm','bk','cf',&
       'es','fm','md','no', &
       'lr','rf','db','sg','bh','hs','mt','ds','rg'/
!
      data atomnames /'Hydrogen','Helium','Lithium','Beryllium','Boron', &
       'Carbon','Nitrogen','Oxygen','Fluorine','Neon','Sodium', &
       'Magnesium','Aluminium','Silicon','Phosphorus','Sulfur', &
       'Chlorine','Argon','Potassium','Calcium','Scandium','Titanium', &
       'Vanadium','Chromium','Manganese','Iron','Cobalt','Nickel', &
       'Copper','Zinc','Gallium','Germanium','Arsenic','Selenium', &
       'Bromine','Krypton','Rubidium','Strontium','Yttrium','Zirconium', &
       'Niobium','Molybdenum','Technetium','Ruthenium','Rhodium',&
       'Palladium','Silver','Cadmium','Indium','Tin','Antimony',&
       'Tellurium','Iodine','Xenon','Caesium','Barium','Lanthanum',&
       'Cerium','Praseodymium','Neodymium','Promethium','Samarium',&
       'Europium','Gadolinium','Terbium','Dysprosium','Holmium','Erbium',&
       'Thulium','Ytterbium','Lutetium','Hafnium','Tantalum','Tungsten',&
       'Rhenium','Osmium','Iridium','Platinum','Gold','Mercury',&
       'Thallium','Lead','Bismuth','Polonium','Astatine','Radon',&
       'Francium','Radium','Actinium','Thorium','Protactinium',&
       'Uranium','Neptunium','Plutonium','Americium','Curium',&
       'Berkelium','Californium','Einsteinium','Fermium','Mendelevium',&
       'Nobelium','Lawrencium','Rutherfordium','Dubnium','Seaborgium',&
       'Bohrium','Hassium','Meitnerium','Darmstadtium','Roentgenium'/

      data atmnspin /0.5d0, 0.5d0, 1.0d0, 1.5d0, 3.0d0, 0.0d0, 1.0d0, 0.0d0, 0.5d0, 0.0d0, &
        1.5d0, 0.0d0, 2.5d0, 0.0d0, 0.5d0, 0.0d0, 1.5d0, 0.0d0, &
        1.5d0, 0.0d0, 3.5d0, 0.0d0, 6.0d0, 0.0d0, 2.5d0, 0.0d0, 3.5d0, 0.0d0, 1.5d0, 0.0d0, &
        1.5d0, 0.0d0, 1.5d0, 0.0d0, 1.5d0, 0.0d0, &
        2.5d0, 0.0d0, 0.5d0, 0.0d0, 4.5d0, 0.0d0, 6.0d0, 0.0d0, 0.5d0, 0.0d0, 0.5d0, 0.0d0, &
        4.5d0, 0.0d0, 2.5d0, 0.0d0, 2.5d0, 0.0d0, &
        3.5d0, 0.0d0, 5.0d0, 0.0d0, 2.5d0, 0.0d0, 2.5d0, 0.0d0, 2.5d0, 0.0d0, 1.5d0, 0.0d0, &
        3.5d0, 0.0d0, 0.5d0, 0.0d0, &
        3.5d0, 0.0d0, 8.0d0, 0.0d0, 2.5d0, 0.0d0, 1.5d0, 0.0d0, 1.5d0, 0.0d0, 0.5d0, 0.0d0, &
        4.5d0, 0.5d0, -1.0d0, -1.0d0, &
       -1.d0,-1.d0,-1.d0,-1.d0,-1.d0,-1.d0, -1.d0,-1.d0,-1.d0,-1.d0,-1.d0,-1.d0,&
       -1.d0,-1.d0,-1.d0,-1.d0, &
       -1.d0,-1.d0,-1.d0,-1.d0,-1.d0,-1.d0,-1.d0,-1.d0,-1.d0/


      data atomwts /1.00794D0,4.002602D0,6.941D0,9.012182D0,1.0811D1,&
      0.120107D2,0.140067D2,0.159994D2,0.189984D2,0.201797D2,2.298977D1,&
      0.24305D2,0.2698154D2,0.280855D2,0.3097376D2,0.32065D2,0.35453D2,&
      0.39948D2,0.390983D2,0.40078D2,0.4495591D2,0.47867D2,0.509415D2,&
      0.519961D2,0.5493805D2,0.55845D2,0.589332D2,0.586934D2,0.63546D2,&
      0.6539D2,0.69723D2,0.7264D2,0.749216D2,0.7896D2,0.79904D2,0.838D2,&
      8.54678D1,8.762D1,8.890585D1,9.1224D1,9.290638D1,9.594D1,9.8D1,&
      1.0107D2,1.029055D2,1.0642D2,1.078682D2,1.12411D2,1.14818D2,&
      0.11871D3,0.12176D3,0.1276D3,0.1269045D3,0.131293D3,0.1329055D3,&
      0.137327D3,0.1389055D3,0.140116D3,0.1409076D3,0.14424D3,0.145D3,&
      0.15036D3,0.151964D3,0.15725D3,0.1589253D3,0.1625D3,0.1649303D3,&
      0.167259D3,0.1689342D3,0.17304D3,0.174967D3,0.17849D3,0.1809479D3,&
      0.18384D3,0.186207D3,0.19023D3,0.192217D3,0.195078D3,0.1969666D3, &
      0.20059D3,0.2043833D3,0.2072D3,0.2089804D3,0.209D3,0.21D3,0.222D3, &
      2.23D2,2.26D2,2.27D2,2.320381D2,2.310359D2,2.380289D2,2.37D2, &
      0.244D3,0.243D3,0.247D3,0.247D3,0.251D3,0.252D3,0.257D3,0.258D3, &
      0.259D3,0.262D3,0.261D3,0.262D3,0.266D3,0.264D3,0.277D3,0.268D3, &
      0.281D3,0.272D3/
!
      data atmradsl /0.25d0,0.00d0,1.45d0,1.05d0,0.85d0,0.70d0,0.65d0,&
       0.60d0,0.50d0,0.00d0,1.80d0,1.50d0,1.25d0,1.10d0,1.00d0,1.00d0,&
       1.00d0,0.00d0,2.20d0,1.80d0,1.60d0,1.40d0,1.35d0,1.40d0,1.40d0,&
       1.40d0,1.35d0,1.35d0,1.35d0,1.35d0,1.30d0,1.25d0,1.15d0,1.15d0,&
       1.15d0,0.00d0,2.35d0,2.00d0,1.80d0,1.55d0,1.45d0,1.45d0,1.35d0,&
       1.30d0,1.35d0,1.40d0,1.60d0,1.55d0,1.55d0,1.45d0,1.45d0,1.40d0,&
       1.40d0,0.00d0,2.60d0,2.15d0,1.95d0,1.85d0,1.85d0,1.85d0,1.85d0,&
       1.85d0,1.85d0,1.80d0,1.75d0,1.75d0,1.75d0,1.75d0,1.75d0,1.75d0,&
       1.75d0,1.55d0,1.45d0,1.35d0,1.35d0,1.30d0,1.35d0,1.35d0,1.35d0,&
       1.50d0,1.90d0,1.80d0,1.60d0,1.90d0,0.00d0,0.00d0,0.00d0,2.15d0,&
       1.95d0,1.80d0,1.80d0,1.75d0,1.75d0,1.75d0,1.75d0,0.00d0,0.00d0,&
       0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,&
       0.00d0,0.00d0,0.00d0,0.00d0,0.00d0/
      data atmradcl /0.53d0,0.31d0,1.67d0,1.12d0,0.87d0,0.67d0,0.56d0,&
       0.48d0,0.42d0,0.38d0,1.90d0,1.45d0,1.18d0,1.11d0,0.98d0,0.88d0,&
       0.79d0,0.71d0,2.43d0,1.94d0,1.84d0,1.76d0,1.71d0,1.66d0,1.61d0,&
       1.56d0,1.52d0,1.49d0,1.45d0,1.42d0,1.36d0,1.25d0,1.14d0,1.03d0,&
       0.94d0,0.88d0,2.65d0,2.19d0,2.12d0,2.06d0,1.98d0,1.90d0,1.83d0,&
       1.78d0,1.73d0,1.69d0,1.65d0,1.61d0,1.56d0,1.45d0,1.33d0,1.23d0,&
       1.15d0,1.08d0,2.98d0,2.53d0,0.00d0,0.00d0,2.47d0,2.06d0,2.05d0,&
       2.38d0,2.31d0,2.33d0,2.25d0,2.28d0,2.26d0,2.26d0,2.22d0,2.22d0,&
       2.17d0,2.08d0,2.00d0,1.93d0,1.88d0,1.85d0,1.80d0,1.77d0,1.74d0,&
       1.71d0,1.56d0,1.54d0,1.43d0,1.35d0,1.27d0,1.20d0,0.00d0,0.00d0,&
       0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,&
       0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,&
       0.00d0,0.00d0,0.00d0,0.00d0,0.00d0/
      data atmradco /0.37d0,0.32d0,1.34d0,0.90d0,0.82d0,0.77d0,0.75d0,&
       0.73d0,0.71d0,0.69d0,1.54d0,1.30d0,1.18d0,1.11d0,1.06d0,1.02d0,&
       0.99d0,0.97d0,1.96d0,1.74d0,1.44d0,1.36d0,1.25d0,1.27d0,1.39d0,&
       1.25d0,1.26d0,1.21d0,1.38d0,1.31d0,1.26d0,1.22d0,1.19d0,1.16d0,&
       1.14d0,1.10d0,2.11d0,1.92d0,1.62d0,1.48d0,1.37d0,1.45d0,1.56d0,&
       1.26d0,1.35d0,1.31d0,1.53d0,1.48d0,1.44d0,1.41d0,1.38d0,1.35d0,&
       1.33d0,1.30d0,2.25d0,1.98d0,1.69d0,1.85d0,1.85d0,1.85d0,1.85d0,&
       1.85d0,1.85d0,1.80d0,1.75d0,1.75d0,1.75d0,1.75d0,1.75d0,1.75d0,&
       1.60d0,1.50d0,1.38d0,1.46d0,1.59d0,1.28d0,1.37d0,1.28d0,1.44d0,&
       1.49d0,1.48d0,1.47d0,1.46d0,1.35d0,1.27d0,1.45d0,2.15d0,2.15d0,&
       1.95d0,1.80d0,1.80d0,1.75d0,1.75d0,1.75d0,1.75d0,1.75d0,1.75d0,&
       1.75d0,1.75d0,1.75d0,1.75d0,1.75d0,1.75d0,1.75d0,1.75d0,1.75d0,&
       1.75d0,1.75d0,1.75d0,1.75d0,1.75d0/
      data atmradvn /1.20d0,1.40d0,1.82d0,0.00d0,0.00d0,1.70d0,1.55d0,&
       1.52d0,1.47d0,1.54d0,2.27d0,1.73d0,0.00d0,2.10d0,1.80d0,1.80d0,&
       1.75d0,1.88d0,2.75d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,&
       0.00d0,0.00d0,1.63d0,1.40d0,1.39d0,1.87d0,0.00d0,1.85d0,1.90d0,&
       1.85d0,2.02d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,&
       0.00d0,0.00d0,1.63d0,1.72d0,1.58d0,1.93d0,2.17d0,0.00d0,2.06d0,&
       1.98d0,2.16d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,&
       0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,&
       0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,1.75d0,1.66d0,&
       1.55d0,1.96d0,2.02d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,&
       0.00d0,0.00d0,0.00d0,1.86d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,&
       0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,0.00d0,&
       0.00d0,0.00d0,0.00d0,0.00d0,0.00d0/
 
       end module atomdata
