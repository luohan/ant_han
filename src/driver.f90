      subroutine driver(nat0)
!
! Computes a full trajectory from start to finish.
! itp_min:   count for steps after which a bond can be deemed as
!            formed
! itp_c_min: rij<rmin: 1, else,0
! itp_max:   count for steps after which a bond can be deemed as
!            broken
! itp_c_max: rij>rmax: 1, else,0
! ifrag:      count on fragments
! isame:      count on fragments identical to original AGs
! istat:      count on steps to average properties
! iwrite:     Count on steps to write geometries
! ce:         Cohesive energy
! ace:        Average cohesive energy
! ace2:       Average of the square of cohesive energy
! sce:        variation of ce
! ape:        Average potential energy
! ape2:       Average of the square of potential energy
! spe:        variation of pe
! ake:        Average kinetic energy
! ake2:       Average of the square of kinetic energy
! ske:        variation of ke
! ate:        Average total (internal) energy
! ate2:       Average of the square of total energy
! ste:        variation of te
! cv:         Heat capacity.
! atemp:      Average temperature
! atempm:     Temp. Store of Average temperature
! atemp2:     Average of the square of temperature
! stemp:      Variation of temperature
! timestat:   Count time used to stat on properties
! fpart:      partition function
! fparti:     exp(-u/kT) at each step
! xxlow:      Store the lowest energy structure
! nfrpvev:    Store previous number of fragments
! ntrsd:      total number of atoms transfered
! ifind:      find an atom
! itrsd(mnat) indice of the atom in the current step found transfered
! ibrkpre:    Previously atom found with broken bond
! ifmdpre:    Previously atom found with broken bond
! presst(3,3):    Pressure tensor matrix
! arcomd:     Average of the distance of atom I to CoM
! arcomd2:    Average of arcomd**2

      use param, only: mnmol, mnat, mnsurf, mnfrag, mnyarray, nmax2, mnbin, &
                       mnbin, mnsurf, mnoutcome, pi, &
                       amutoau, autoev, kb, autofs, autoang, autoatm
      use c_output, only: lwrite, idebug, nprint, maxprint, time_tunnel
      use c_struct, only: bndind, bndindi, nbond, nbondi, nfrag, nh, &
                          infree, momiprin, rotaxis, mmofag, mmm, mm0, &
                          xxm, indatom, indmm, nmol, symbol0, symbolm, &
                          iflinear, natoms, arij, arij2, cell, xxf, &
                          natinfrag, rijco, bdbrthr, bndmatr, natom, &
                          iatom, indfrdep, xx0, fragind, rij, cvec, &
                          nvibmods, xxi, nat

      use c_sys, only: itherm, nsurft, reactcol, potflag, methflag, sysfreq,&
                       periodic, rampfact, nramp, nequil, repflag, hstep0, &
                       iadjtemp, n_ramp, dimen, p0, intflag, &
                       trajlist, tflag, vol0, hstep, itempzpe, temp0, nrampstep

      use c_initial, only: nvinit, xnmref, pickthr, ifnoang, &
                           r0collision, nbend, nstr, drr, itunn, initx
      use c_traj, only: pe, ke, time, gv, tei, bigji, kbt0, kbtarg, zpe,  &
                        bigjtoti, bigj, inh, vol, step, phase, ppm, ppi,  &
                        nstat, t0react, trxover, ireact, cre, bigjtot,    &
                        press, lzerotrans, pem, cim, rbin, temp, gpem, &
                        dvec, gcre, nflip, te, pemd, pema, nhop, phop, &
                        icollide, rxprob, sumw, nsurf, potential,      &
                        icfrag, heatcap, kinetic, cohesive, ppf, collw, &
                        itraj, lmodpop, istep, weight, lzerocom, &
                        indtor, gcim, ltunn, tunnd, tuntype, ntrapz,  &
                        stodecoflag, nreinit
      use c_term, only: nbondbreak, nbondform, termflag, t_nstep, &
                        agmerge, indfm, dihfin, attrans, agdefrag, &
                        t_gradmax, n_turn, indbr, outbreak, n_atoffr, &
                        dihlim, dihd, ntorsion, termsteps, outform, &
                        trmax, trmin, t_stime, n_frags, tiform, t_gradmag, &
                        tibreak
      use c_common, only: numstep, taui

      use hdf5_output, only: lwrite_h5, hdf5o_write_peratom, &
                        hdf5o_trj_init,hdf5o_trj_insert_22,hdf5o_trj_insert_23, &
                        hdf5o_trj_end, hdf5o_trj_move

      implicit none
! nat0 == nat (used to define some local arrays)
      integer,intent(in) :: nat0
!
!  Parameters
      integer, parameter :: mnmol1=mnmol+1
!  au to second
      real(8), parameter :: autos=autofs*1.d-15
      real(8), parameter :: autoang2=autoang*autoang
!     Used for various Verlet algorithm
! xp:     Coordinates at previous step (time - hstep)
! gvp:    Gradients at previous step (time - hstep)
! pep:    For the simple Verlet algorithm, updating of momentum is one
!         step behind coordinates. pep is used to store the PE of the
!         previous step
      real(8) :: xp(3,mnat),gvp(3,mnat),pep,pept, time1, time2
!     number of bins for binning the radial distribution function
      integer :: nbinrad
!     rbmax: maximum distance, rbmin: mimimum distance in rdf
      real(8) :: rbmax,rbmin
!
      integer :: im,i,j,k,l,ifrag,i1,i2,i3,i4,ithistraj,iprt, &
                 frusflag(mnsurf), newsurf,iramp,itp_c_min,itp_c_max,itp_min, &
                 itp_max,nfrprev,indsame(mnat,mnfrag,mnfrag),ntrsd,ifind,itrsd(mnat), &
                 natsame(mnfrag,mnfrag),isame(mnfrag),itrsprv(mnat), &
                 istat,iwrite,nfrdep,itp_merg,itp_c_merg, &
                 ibrkpre(mnoutcome),ifmdpre(mnoutcome)
! ifupgrad:   if position is adjusted in advol, ifupgrad = .true.
      logical :: issame,isdepart,ifupgrad
      integer :: nfrag0
      real(8) :: kbtp0v,cfrag(mnat),erott,teint
!     ramp nrampt time for each ramp cycle
      integer :: nrampt,itmp,nstatm
!      real(8) :: fpartup,e00,de0,timeup
!     used for calculating CoM distance between two AGs
      real(8) :: com1x,com1y,com1z,com2x,com2y,com2z,rcom
!     CoMxxm:   CoM coordinate
!     CoMppm:   CoM Momentum vector
!     totcomp:  Total CoM momentum
!     etran:    Translational energy of the whole AG
      real(8) :: comxxm(3),comppm(3),totcomp,etran,mtot
      real(8) :: xxmin(3,mnat)
!     used for calculate the binned Berry parameter (lindemann like)
!     mbin:  Maximum bins used for the whole system
!     rbin:  The width of the bins
!     rcomb: binned rcom(i)
!     rcombm: temperary binned rcom(i)
!     lindbm: temperary binned lindi(i)
      integer mbin
      real(8) :: lindb(mnbin),lindis(mnat),anbin(mnbin), &
                          lindisa(mnat),rcomb(mnbin),rcombm(mnbin),lindbm(mnbin)
      real(8) :: arcomd,arcomd2, aspher,aspher2,aspherli,aspherli2, &
                          lindi(mnat),rcomi(mnat),rcomi2(mnat),rtim
!     used for calculating volumn of the cluster
! voln:  Volumn provided by Nate's subroutine
! volg:  Volumn calculated through radius of gyration
! rho:   Density calculated by Nate Method
! rgm(3): radius of gyration, temperary storage
      real(8) :: voln,volg,voln2,volg2,volt,rhon,rhon2,rhot, &
                          avoln,avolg,arhon,arhog,rhog,rhog2,rgm(3)
! iflind:  if do Berry parameter analysis
! ifrcom:  if do average distance between atom and CoM
! ifbin:   if bin distances from CoM, need calculate rcom
      logical :: iflind,ifrcom,ifbin
!     used for calculating CoM momentum and KE of the two AGs
      real(8) :: comp1x,comp1y,comp1z,comp2x,comp2y,comp2z,ke1, &
                          ke2,comv(3),rcom2,compro,t0stick,ttstick,rcommin(mnfrag)
      integer :: iturn
      logical :: iscollide,isturn,isstick,isreturn,isttstick
!
      real(8) :: rhor(mnsurf,mnsurf), rhoi(mnsurf,mnsurf),timeprev,tmp,tmp1, &
                          dmag,dmag1,dmag2,gvmag,rmsgrad,maxgrad,detmtbt, &
                          raddist(0:mnbin),xxlow(3,nat0),presst(3,3)
      real(8) :: fpart,fparti,ce,ace,ace2,ape,ape2,ake,ake2, &
                          ate,ate2,cv,sce,spe,ske,ste,timestat,lind,atemp,atemp2,stemp, &
                          minte,minpe,cem,atempm,tempzpe,temptarg
      logical :: lhop
!  RVM
      real(8) :: rhoro(mnsurf,mnsurf),phopmax(mnsurf), &
                          phopmin(mnsurf),y(mnyarray),yo(mnyarray)
!  JZ
!     real(8) :: denr(mnsurf,mnsurf),deni(mnsurf,mnsurf)
      real(8) :: delphase(mnsurf,mnsurf)
!  RVM

! Used for FSTU
      real(8) :: tu_xx(3,nat0,mnsurf),tu_pp(3,nat0,mnsurf), &
                          tu_cre(mnsurf,mnsurf),tu_cim(mnsurf,mnsurf),tu_hstep(mnsurf), &
                          tu_time(mnsurf),tu_phase(mnsurf,mnsurf),tu_maxt(mnsurf), &
                          deltforw,deltback,tu_maxtime
      integer :: tu_istep(mnsurf)
      logical :: lfrust

!     real(8) :: ptmp(3,mnat)
      real(8) :: ptmp(3,nat0)

! used for geometry optimization
! p    : one dimensional array: x1,y1,z1,...,xNatoms,yNatoms,zNatoms
! iter : Iteration used to achieve convergency
! ndim : Dimension of p (3*natoms)
! iopt : Count geometry optimization
! efinal : final energy after geometry optimization
      integer :: ndim,iter,iopt
      real(8) :: p(3*nat0),efinal,ran
!  freedoms for translation and rotation to be removed when calculating
!  temperature
      integer :: ifree_trans,ifree_rot

!  RVM
!     integer :: imin,imax
      integer :: kev
      real(8) :: speck(8,nmax2)
      real(8) :: inter,nstepmin
      real(8) :: egapint(1000000),nhbondint(3,1000000), &
                          nangint(1000000),nnstep(1000000),erelint(3,1000000), &
                          nhangleint(3,1000000)
!  RVM
      real(8) :: dydx(mnyarray)

! Relative energy for NH2-H
      integer :: nmax
      real(8) :: erel(4),distnh(4)

!c Tests on nonadiabatic couplings (06/03/2008)
!      real(8) :: dvecmod,vdotd(mnsurf,mnsurf)

! AJ SD
! stochastic decoherence
      integer :: n1,n2
      real(8) :: stodecotime,stodecotau
!      logical :: stodecoflag

!  For constrained ZPE method
      real(8) :: nvpre(3*mnat),xeq(3,mnat)

! We print out some information at each hopping event.
      real(8) :: nhangle(mnat-1),nhbond(mnat-1),dum(3), nang
!  RVM
      real(8) :: nhbond1min,nhbond2min,nhbond3min,nangmin
      real(8) :: nhangle1min,nhangle2min,nhangle3min
      integer :: istepmin
      integer :: nhopsv
      logical :: linmol
      data nhopsv/0/
      save nhopsv

!
      real(8) :: egap,egapmin

! Population of each mode
      real(8) :: nmpop(3*mnat)

! average on Tau in CSDM
      real(8) :: atau,tautim,tautun
! JZ tunneling
      integer :: itint(2),itc
      integer :: info
      real(8) :: pdi(3*nat0),pdiprev(3*nat0)
      real(8) :: turnd(3,mnat),xturn(3,mnat),pturn(3,mnat)
      real(8) :: xnew(3,mnat),pnew(3,mnat),wtmp
      real(8) :: rhortmp(mnsurf,mnsurf)
      real(8) :: v0, ttunn
      logical :: ltp,lbranch,lchgw
! JZ torsion
      real(8) :: dang,dihedral
      real(8) :: bond


! RMP open the tunneling file
!     open(34)

      atau=0.d0
      tautim=0.d0
      kev=0

! AJ SD
! STOCHASTIC DECOHERENCE FLAG
!      stodecoflag = .true.   ! turn on SD (use only with TFS & FSTU)
!      stodecoflag = .false.  ! turn off SD
! AJ SD

! used for geometry optimization
      iopt=0
! initialize for this trajectory
      ifupgrad=.false.
!     used for calculating CoM distance between two AGs
      iscollide=.false.
      isturn=.false.
      isreturn=.false.
      isstick=.false.
      isttstick=.false.
      iturn=0
      t0stick=0.d0
      ttstick=0.d0
!  For the entire MD run, always assuming it is non-linear
!     if (nat.eq.2) then
      if (nat0.eq.2) then
         iflinear=1
!      elseif (nat.gt.2) then
       elseif (nat0.gt.2) then
         iflinear=2
       else
         write(6,*) 'ERROR: Cannot run MD for just one atom'
         stop
      endif
      if (itherm.ne.0 .and. iadjtemp.eq.3) then
        inh=.true.
        nh(2,1)=0.d0
        nh(2,2)=0.d0
        nh(3,1)=0.d0
        nh(3,2)=0.d0
      else
         inh=.false.
      endif
      natoms=nat0
      ndim=3*natoms

      iwrite=0
      vol=vol0
      step = hstep
      itp_min = 0
      itp_max = 0
      dmag1 = 0.d0
      dmag2 = 0.d0
      do i=1,nsurft
        phase(i) = 0.d0
        tu_time(i) = -1.d0
      enddo
      lfrust = .false.
! initialize for Berry parameter analysis and RCoM analaysis
      iflind=.false.
      ifrcom=.false.
      ifbin=.false.
      if (lwrite(40).or.lwrite(400).or.lwrite(46).or.lwrite(460).or.  &
          lwrite(47).or.lwrite(470).or.lwrite(48).or.lwrite(480)) then
        iflind=.true.
        ifrcom=.true.
      endif
      if (lwrite(45).or.lwrite(450)) then
        iflind=.true.
      endif
      if (lwrite(43).or.lwrite(430).or.lwrite(44).or.lwrite(440)) then
        ifrcom=.true.
      endif
      if (lwrite(46).or.lwrite(460).or.lwrite(49).or.lwrite(490).or. &
          lwrite(51).or.lwrite(510) ) then
        ifbin=.true.
        ifrcom=.true.
      endif
      lind=0.d0
      do i=1,natoms
        rcomi(i)=0.d0
        rcomi2(i)=0.d0
        lindi(i)=0.d0
        lindis(i)=0.d0
        lindisa(i)=0.d0
        do j=1,natoms
          arij(i,j) = 0.d0
          arij2(i,j) = 0.d0
        enddo
        do j=1,3
          xxm(j,i)=xxi(j,i)
          ppm(j,i)=ppi(j,i)
        enddo
        symbolm(i)=symbol0(i)
        mmm(i)=mm0(i)
        indmm(i)=indatom(i)
!       Atom/group exchange/transfer termination condition
        itrsd(i)=0
        itrsprv(i)=0
!       stat on atom numbers in each fragment
        cfrag(i)=0.d0
      enddo
      kbtp0v=0.d0
      minpe=1.d40
!      minte=1.d40
      atempm = 0.d0
      atemp2 = 0.d0
      stemp = 0.d0
      iramp = 0
      nrampt = 0
      nstatm = nstat
      rbmin =  0.d0/autoang
      rbmax = 30.d0/autoang
      nbinrad = int((rbmax-rbmin)/rbin)
      if (nbinrad.ge.mnbin) then
        write(6,*) 'RBIN too small --- too many bins in RDF analysis'
        stop
      endif
      rbmax = rbmin + dble(nbinrad)*rbin
      do i=0,nbinrad+1
        raddist(i) = 0.d0
      enddo
! initialize for termination condition
      ntrsd=0
      nfrprev=0
      do i=1,nbondbreak
        ibrkpre(i)=0
      enddo
      do i=1,nbondform
        ifmdpre(i)=0
      enddo
      t0react=0.d0
      trxover=0.d0
!
!  Initializing on statistical properties
      fpart = 0.d0
      ace   = 0.d0
      ace2  = 0.d0
      ape   = 0.d0
      ape2  = 0.d0
      ake   = 0.d0
      ake2  = 0.d0
      ate   = 0.d0
      ate2  = 0.d0
      istat = 0
      mbin=0
      do i=1,mnbin
        lindb(i)=0.d0
        anbin(i)=0.d0
        rcomb(i)=0.d0
      enddo
      arcomd = 0.d0
      arcomd2= 0.d0
      aspher = 0.d0
      aspher2= 0.d0
      voln   = 0.d0
      voln2  = 0.d0
      rhon   = 0.d0
      rhon2  = 0.d0
      volg   = 0.d0
      volg2  = 0.d0
      rhog   = 0.d0
      rhog2  = 0.d0
!      aspherli = 0.d0
!      aspherli2= 0.d0
      timestat = 0.d0
!
!  Initializing on reaction collision
      ireact = 0
!  Initializing tunneling (JZ)
!      do i = 1, nat0-5   !RMP
      do i = 1, 3*nat0    !RMP
        pdi(i) = 999d0
        pdiprev(i) = 999d0
      enddo
!  Initializing the vibrational quantum number for unimolecular reaction
      nvpre(:) = nvinit(:,1)
      if(nmol.eq.1) then
         xeq(:,:) = xnmref(:,:)
        call comgen(xeq,mmm,natoms,comxxm,mmofag(mnmol1))
      endif

!
! get trajectory index
      if (tflag(2).eq.1) then
         ithistraj = trajlist(itraj)
       else
         ithistraj = itraj
      endif
!
! Remove the overall rotational momentum for the whole system
! Only need to do once for single AG run, and do not need to do for
! reaction collision run
!       put center of mass at the origin
      call comgen(xxm,mmm,natoms,comxxm,mmofag(mnmol1))
      call rotprin(xxm,mmm,natoms,momiprin,rotaxis,iflinear)
      if (lzerotrans) then
        call nocompp(ppm,mmm,natoms,comppm,totcomp,1)
        ifree_trans=3
      else
        ifree_trans=0
      endif
!       do not rotate the whole AG for reactcol for the convenience of
!       later analysis on angular distribution of the products
!      not for periodic condition
      if (.not.lmodpop) then
      if (.not.reactcol .and. initx(1).ne.4 .and. (.not.periodic)) &
          then
        call rottran(rotaxis,xxm,natoms)
        call rottran(rotaxis,ppm,natoms)
      endif
      endif
      call angmom(iflinear,bigj,ppm,xxm,natoms,bigjtoti(mnmol1))
!
!     Remove angular momentum if neccessary
      if (ifnoang) then
         write(6,106) 'Remove overall angular momentum'
         write(6,106) 'Total angular momentum before remove ', &
                       bigjtoti(mnmol1),' au'
         if (iflinear.eq.1) then
            ifree_rot=2
         else
            ifree_rot=3
         endif
         call noang(iflinear,bigj,ppm,xxm,mmm,natoms)
         call angmom(iflinear,bigj,ppm,xxm,natoms,bigjtoti(mnmol1))
      else
         ifree_rot=0
      endif
      infree = 3*natoms - ifree_rot - ifree_trans
!      write(6,*) 'infree=',infree
      tempzpe=0.d0
      if (itempzpe) then
        tempzpe=zpe(mnmol1)/(kb*dble(infree))
      endif
      temptarg=temp0+tempzpe
      kbtarg=kb*temptarg
      kbt0=kb*temp0
      nh(1,1)=2.d0*dble(infree)*kbt0*sysfreq**2
      nh(1,2)=nh(1,1)
      if (itempzpe) write(6,106) 'Correction to temperature due to ZPE is ', &
                   tempzpe, ' K'
! Initial rij, rij2 distance matrix and bonding information matrix
      call rijmatr
      call frag
      nfrag0=nfrag
!
! Save initial bonding information
      do i=1,natoms
        nbondi(i)=nbond(i)
        do j=1,nbond(i)
          bndindi(j,i) = bndind(j,i)
        enddo
      enddo
!     save initial rotation momentum
      do i=1,3
        bigji(i,mnmol1) = bigj(i)
      enddo
      call getgrad(dmag,0)
!      call getgrad(dmag,vdotd)
      call gettemp(mnmol1)
      tei=ke+pe
      write(6,106)'Initial kinetic energy     = ',ke*autoev,' eV'
      write(6,106)'Initial potential energy   = ',pe*autoev,' eV'
      write(6,106)'Initial total energy       = ',tei*autoev,' eV'
      write(6,106)'Initial temp               = ',temp,' K'
      write(6,106)'Total angular momentum     = ',bigjtoti(mnmol1), ' au'
      write(6,*)
! initialize the position at previous step (-hstep0) for Simple Verlet
! algorithm
      if (intflag.eq.2) then
        do i=1,natoms
          do j=1,3
! Force is the negative of dV/dx
           xp(j,i) = xxm(j,i) - hstep0 * ppm(j,i)/mmm(i) &
                   - hstep0**2 * gv(j,i)/(2.d0*mmm(i))
          enddo
        enddo
        pept = pe
      endif
! write initial coordinates and momenta to output
      if (maxprint) then
        write(6,*)
        write(6,*) 'Total degrees of freedom: ',infree
        write(6,*) 'Vibrational degrees of freedom: ',nvibmods(mnmol1)
        write(6,*) 'Initial structure of trajectory: ',ithistraj
        do i=1,natoms
          write(6,1010) symbolm(i),(xxm(j,i)*autoang,j=1,3)
        enddo
        write(6,*)
        write(6,*) 'Initial momenta of trajectory: ',ithistraj
        do i=1,natoms
          write(6,1010) symbolm(i),mmm(i)/amutoau,(ppm(j,i),j=1,3)
        enddo
        write(6,*)
      endif
!
      if (lwrite(10)) then
        write(10,*)ithistraj,pe*autoev
        do i=1,natoms
          write(10,1010) symbolm(i),mmm(i)/amutoau, (xxm(j,i)*autoang,j=1,3)
        enddo
      endif

      if (lwrite_h5(10)) then
        call hdf5o_write_peratom(10,ithistraj,pe*autoev, xxm(1:3,1:natoms)*autoang)
      endif

      if (lwrite(11)) then
        write(11,*)ithistraj,ke*autoev
        do i=1,natoms
          write(11,1010) symbolm(i),mmm(i)/amutoau,(ppm(j,i),j=1,3)
        enddo
      endif

      if (lwrite_h5(11)) then
        call hdf5o_write_peratom(11,ithistraj,ke*autoev, ppm(1:3,1:natoms))
      endif
!
! trajectory output
      if (termflag.eq.2) then
            write(6,1)'   time(fs)  ', &
                      '   PE(eV)    ', &
                      '|gradV|(eV/A)'
      else
        write(6,*) 'CoM momentums in amu*A/fs'
        if (itherm.eq.2) then
          if (nsurft.eq.1) then
            write(6,1)'  time(fs)   ', &
                      '   T(K)      ', &
                      'Pressure(atm)', &
                      '   KE(eV)    ', &
                      '   PE(eV)    ', &
                      '   TE(eV)    ', &
                      '  P_CoM(x)   ', &
                      '  P_CoM(y)   ', &
                      '  P_CoM(z)   ', &
                      ' P_CoM(tot)  ', &
                      '   Berry     ', &
                      '    <T(K)>   ', &
                      ' st.dev(T(K))'
          else
            write(6,1)'  time(fs)   ',&
                      '   T(K)      ',&
                      'Pressure(atm)',&
                      '   KE(eV)    ',&
                      '   PE(eV)    ',&
                      '   TE(eV)    ',&
                      '  P_CoM(x)   ',&
                      '  P_CoM(y)   ',&
                      '  P_CoM(z)   ',&
                      ' P_CoM(tot)  ',&
                      '    Probs    ',&
                      '             ',&
                      '    Nsurf    '
          endif
        else
          if (nsurft.eq.1) then
            write(6,1)'  time(fs)   ', &
                      '   T(K)      ', &
                      '   KE(eV)    ', &
                      '   PE(eV)    ', &
                      '   TE(eV)    ', &
                      '  P_CoM(x)   ', &
                      '  P_CoM(y)   ', &
                      '  P_CoM(z)   ', &
                      ' P_CoM(tot)  ', &
                      '   Berry     ', &
                      '    <T(K)>   ', &
                      ' st.dev(T(K))'
          else
            write(6,1)'  time(fs)   ', &
                      '   T(K)      ', &
                      '   KE(eV)    ', &
                      '   PE(eV)    ', &
                      '   TE(eV)    ', &
                      '  P_CoM(x)   ', &
                      '  P_CoM(y)   ', &
                      '  P_CoM(z)   ', &
                      ' P_CoM(tot)  ', &
                      '    Probs    ', &
                      '             ', &
                      '    Nsurf    '
          endif
        endif
      endif
      if (lwrite(77)) then
        write(77,*) ithistraj,'th Trajectory'
        write(77,2) 'time (fs)','R(CoM)','CoM_KE(R1)','CoM_KE(R2)', &
                      'CoMPP2.RCoM2','KE_RCOM2'
      endif
      if (lwrite(40)) then
        if(lwrite(50)) then
          write(40,107)'#traj','#step','time (fs)','T (K)','Lind', &
          'Cv (eV/K)','Ce(eV/atom)','KE (eV)','TE (eV)', &
          'Sphericity','Var Spheric', &
          'RCoM (A)','RCoM**2(A2)', &
          'Vol_Nate','Var Voln','Rho_Nate','Var Rhog', &
          'Vol_Gyr','Var Volg','Rho_Gyr','Var Rhog'
        else
          write(40,107)'#traj','#step','time (fs)','T (K)','Lind', &
          'Cv (eV/K)','Ce(eV/atom)','KE (eV)','TE (eV)', &
          'Sphericity','Var Spheric', &
          'RCoM (A)','RCoM**2(A2)', &
          'Vol_Gyr','Var Volg','Rho_Gyr','Var Rhog'
        endif
      endif
      step=hstep0
      time=step
! ######################################################################
! INTEGRATE TRAJECTORY
! ######################################################################
      istep = 0
      time  = 0.d0
      stodecotime = -1.d0
      stodecotau = 4.d0/autofs
      weight = 1.d0
!  RVM Reinitialize the y and yo arrays to zero
      do i = 1,mnyarray
        y(i) = 0.d0
        yo(i) = 0.d0
      enddo
      if (lwrite_h5(22) .or. lwrite_h5(23)) then
        call hdf5o_trj_init()
      endif
!      egapmin = pem(2) - pem(1)
!  RVM
      do 10 while (.true.)
      istep = istep + 1
      numstep = istep
!
!     quit if we exceed max number of steps
!     stop 'driver 1'
      if (termflag.eq.0.and.istep.gt.t_nstep) then
        goto 999
      endif
!
!     quit if we've integrated long enough (TERMFLAG = 1)
!      if (termflag.eq.1.and.time.ge.t_stime) then
      if (time.gt.t_stime) then
        goto 999
      endif
!
! take step
! Save the potential energy of the previous step for simple Verlet
! algorithm
      if (intflag.eq.2) pep = pept
      timeprev = time
!  RVM

      call takestep(xp,gvp,y,yo,rhor,rhoro)
!  RVM
      step = time - timeprev

!     if(mod(istep,nprint).eq.0.or.istep.eq.1) then
!       call getevibanh(ewell(1),pe,xxm(:,1:natoms),xeq(:,1:natoms), &
!       ppm(:,1:natoms),mmm(1:natoms),natoms,nmvec(:,:,:,1), &
!       freq(:,1), nvibmods(1),time)
!     endif

!
!  JZ : ZPE constrain
!     if(nmol.eq.1) then
!     call constrzpe(natoms,xeq,xxm,ppm,mmm,nvibmods(1),nvpre,  &
!                   nmvec(:,:,:,1),freq(:,1))
!     endif

!  JZ tunneling
!  Current implementation is only for unimolecular case
!
      if(ltunn .and. nmol.eq.1.and.istep.gt.1) then
        ltp =.false.
        pdiprev(:) = pdi(:)
        call pjctmoment(pdi,natoms,mmm,ppm,xxm,tunnd,tuntype)
!       do i = 1, 3*nat0
!         write(6,*) 'pdi pdiprev',pdi(i), pdiprev(i)
!       enddo
!       write(6,*) 'pdi pdiprev',pdi(12), pdiprev(12)
        if(istep.gt.1) then
          call turnpt(pdi,pdiprev,natoms,ltp,turnd,tunnd, &
                      xxm,ppm,xturn,pturn,itc,tuntype)
        endif
        if(ltp) then ! calculate imaginary action integral
!xxx         call noang(iflinear,bigj,pturn,xturn,mmm,natoms)
          if(tuntype.eq.1) then
             itint(1) = itc
             itint(2) = 0
          elseif(tuntype.eq.2) then
             k = 0
             do i = 1, 3*nat0-6
                if(itunn(i).ne.0) then
                  k = k + 1

! RMP this part should move to the readin
                  if(k .gt. 2) stop 'Number of combined stretching coordinates larger than 2'
! RMP this part should move to the readin

                  itint(k) = i
                endif
             enddo
          endif
          call cpu_time(time1)
          if(nsurft .eq. 1) then
            call actionint(ithistraj,xturn,pturn,itint,drr,nstr,nbend, &
                           turnd,lbranch,1,wtmp,lchgw,xnew,pnew)

          else
! calculate tautun before doing action integral to use rhor(k,k) at turning point
            rhortmp(:,:) = rhor(:,:)
            call actionint_nt(ithistraj,xturn,pturn,itint,drr, &
                              nstr,nbend,turnd,lbranch,1,wtmp,lchgw,xnew,pnew, &
                              info,ttunn,repflag,nsurft)
          endif
          call cpu_time(time2)
          time_tunnel=time_tunnel+time2-time1
          if(lchgw)  then
            weight = weight*wtmp
            if (methflag.eq.3.or.methflag.eq.4)  then
              tautun = 0d0
              do k = 1, nsurft
                if(k.ne.nsurf) tautun = tautun + taui(k)*rhortmp(k,k)
              enddo
!             write(6,'(2a,E12.5)')'Decoherence time to target state ',    &
!             'at beginning of tunneling (fs): ', autofs/tautun
              if(rhortmp(nsurf,nsurf).lt.0.98d0) then
                write(6,'(a,ES13.5)') 'Decoherence time with rhor(K,K) < 0.98 (fs)',autofs/tautun
              else
                write(6,'(a,ES13.5)') 'Decoherence time with any rhor(K,K) (fs)',autofs/tautun
              endif
            endif

            if(lbranch) then
              write(6,'(1X,a,es14.5e3,a,f15.2,a)')'branching weight   ', &
                    wtmp,' at',time*autofs,' fs'
              write(6,*)
              xxm(:,:) = xnew(:,:)
              ppm(:,:) = pnew(:,:)
            else
              write(6,'(1X,a,es14.5e3,a,f15.2,a)')'nonbranching weight', &
                      wtmp, ' at',time*autofs,' fs'
              write(6,*)
            endif
          endif
        endif       ! end of ltp

      endif  ! end of tunneling part

! NEW LOCATION FOR TRAPZ
! New subroutine to apply TRAPZ-like methods
! Applying TRAPZ-like methods to mean-field approaches (CSDM for instance)
! is not attempted yet since the dynamics is performed on an average surface.
! TRAPZ-like methods are tested on surface-hopping methods (TFS and FSTU).
! These methods may lead to some problems since Cartesian coordinates are not
! the most appropriate to calculate the projected Hessian at all geometries.
! We consequently apply these methods only if we obtain the correct number
! of zeroes (6 for nonlinear molecules and 5 for linear molecules)

      if ( (ntrapz.eq.2.or.ntrapz.eq.3).and.(methflag.eq.1.or. &
            methflag.eq.5) ) call trapz(ithistraj,lwrite)

! AJ SD
! stochastic decoherence
! RVM stodecoflag is now an input variable
      if (stodecoflag.eq.1) then
      call stodeco(ithistraj,istep,cre,cim,pem,nsurf,time, &
                   stodecotime,stodecotau,step)
      endif
! AJ SD

! Gradient needs to be upgraded after xxm have been changed in takestep
! except that velocity Verlet have updated gradient
! Note: Liouville verlocity verlet does not update gradient at new
! geometry. In order to save computation time, here we choose not to
! update. Thus, the  PE and KE are not synchronized, and TE is not
! correct and can't be used. If heat capacity is calculated, then it
! will be updated
      if (intflag.eq.3.or.intflag.eq.4.or.intflag.eq.5) then
        ifupgrad=.false.
      else
        ifupgrad=.true.
      endif
!
! Thermostat
      if (itherm.ne.0) then
        call gettemp(mnmol1)
        call adjtemp(temp,temptarg,mnmol1)
      endif
      if (itherm.ne.0.and.iadjtemp.eq.2) then
! Since for anderson therm, the Momentum of CoM is not zero, coord.
! of CoM (comxx) will change as simulation goes on. Move it back.
        if (lzerotrans) call nocompp(ppm,mmm,natoms,comppm,totcomp,1)
        if (lzerocom) call comgen(xxm,mmm,natoms,comxxm,mtot)
        if (ifnoang) then
          call angmom(iflinear,bigj,ppm,xxm,natoms,bigjtoti(mnmol1))
          call noang(iflinear,bigj,ppm,xxm,mmm,natoms)
        endif
      else
        if (mod(istep,nreinit).eq.0) then
         if (lzerocom) call comgen(xxm,mmm,natoms,comxxm,mtot)
         if (lzerotrans) call nocompp(ppm,mmm,natoms,comppm,totcomp,1)
         if (ifnoang) then
           call angmom(iflinear,bigj,ppm,xxm,natoms,bigjtoti(mnmol1))
           call noang(iflinear,bigj,ppm,xxm,mmm,natoms)
         endif
        endif
      endif
      if (.not.lzerotrans) then
         call nocompp(ppm,mmm,natoms,comppm,totcomp,0)
!    total translational energy
         etran = totcomp*totcomp/(2.d0*mmofag(mnmol1))
      endif
      if (ifupgrad) then
!  if xxm has been changed, call getgrad
        call getgrad(dmag,0)
!        call getgrad(dmag,vdotd)

!       Reinitialize ifupgrad
        ifupgrad=.false.
      endif
      call rijmatr
      if (itherm.eq.2) then
        call frag
        call fragcom(nfrdep,rcommin)
!      getpress calculate pressure based on the "particles" in the
!      system, i.e. number of fragments and their translation momentum.
        call getpress(dimen,presst)
        call adjpress(presst,press,p0)
!  recalculate volume after cell changed
        vol = detmtbt(cvec)
!  Recalculate gradient after adjpress (position has been changed)
        call getgrad(dmag,0)
!        call getgrad(dmag,vdotd)
        call rijmatr
      endif
!
! update info at new geometry: adjpress will change the
! position of the atoms
      call angmom(iflinear,bigj,ppm,xxm,natoms,bigjtot)
      if (itherm.eq.2) then
        call frag
        call fragcom(nfrdep,rcommin)
        call getpress(dimen,presst)
      elseif (tflag(1).ne.1 .and.(termflag.eq.4.or.termflag.eq.7  &
              .or. termflag.eq.6 .or. reactcol) ) then
          call frag
          call fragcom(nfrdep,rcommin)
      endif
! Volume
!
! reinitialize CSDM
      if (methflag.eq.4) then
      if (istep.gt.2) then
        if( (dmag1-dmag2).lt.0.d0 .AND. (dmag-dmag1).gt.0.d0 ) then
          write(6,*)'Electronic reinitialization!'
          do i=1,nsurft
            i2 = i + nsurft
            cre(i2)=cre(i)
            cim(i2)=cim(i)
            gcre(i2)=gcre(i)
            gcim(i2)=gcim(i)
          enddo
!         update information (check this)
          call getgrad(dmag,0)
!          call getgrad(dmag,vdotd)
        endif
      endif
      dmag2 = dmag1
      dmag1 = dmag
      endif

! multiply hopping probability by stepsize for both surface hopping
! and DM methods
!  RVM  If we are using any integration method except Hack's integration
!       do not integrate probabilities
      if(intflag.ne.6) then
      phop(nsurf) = 1.d0
      do i=1,nsurft
        if (i.ne.nsurf) then
          phop(i)=phop(i)*step
          phop(i)=max(phop(i),0.d0)
!         Note:  phop can be > 1 if the probabilities are changing too
!         quickly.  But this shouldn't happen to often.  For better
!         results we'd need to integrate the hopping probability so
!         that the integrator takes smaller steps when necessary.
          phop(i)=min(phop(i),1.d0)
!         prob of staying in NSURF = 1 - sum of all hopping probs
          phop(nsurf) = phop(nsurf) - phop(i)
        endif
      enddo

!  RVM  Add integration of bjk here
!      phop is gkj, no need to multiply by step
      elseif(intflag.eq.6) then
        call integhop(nsurf,y,yo,rhor,rhoro,phop,phopmax,phopmin)
      endif

! FSTU, check for frustrated hop
      if (methflag.eq.5) then
        call checkfrus(frusflag,tu_maxt)
        do k=1,nsurft
          if ((.not.lfrust).and.frusflag(k).eq.0) then
!           at this time a hop is allowed, save information
!           for k = nsurf, we save the current info
!           when lfrust = true, we are looking for a hopping
!           location, so we do not save info
            do i=1,natoms
            do j=1,3
              tu_xx(j,i,k) = xxm(j,i)
              tu_pp(j,i,k) = ppm(j,i)
            enddo
            enddo
            do i=1,nsurft
              tu_cre(i,k) = cre(i)
              tu_cim(i,k) = cim(i)
              tu_phase(i,k) = phase(i)
            enddo
            tu_hstep(k) = hstep
            tu_istep(k) = istep
            tu_time(k) = time
          endif
        enddo
      endif

! TFS and FSTU check for a hop
      if (methflag.eq.1.or.(methflag.eq.5.and.(.not.lfrust))) then
        lhop = .false.
        call checkhop(phop,nsurf,lhop,newsurf,kev,speck,step,time, &
                      phopmax,phopmin)
      endif

! intercept a frustrated hop for FSTU
      if (methflag.eq.5.and.lhop) then
        if (frusflag(newsurf).ne.0) then
!       this hop would be frustrated
        lfrust = .true.
        lhop = .false.
        tu_maxtime = tu_maxt(newsurf)
        endif
      endif

! handle frustrated hop with FSTU
      if (methflag.eq.5.and.lfrust) then
        deltback = tu_time(nsurf) - tu_time(newsurf)
        deltforw = time - tu_time(nsurf)
        if (tu_time(newsurf).ge.0.d0.and.deltback.lt.deltforw.and. &
                                         deltback.le.tu_maxtime) then
!         do a TU hop backward
          write(6,*)'BACKWARD HOP!'
          do i=1,natoms
          do j=1,3
            xxm(j,i) = tu_xx(j,i,newsurf)
            ppm(j,i) = tu_pp(j,i,newsurf)
          enddo
          enddo
          do i=1,nsurft
            cre(i) = tu_cre(i,newsurf)
            cim(i) = tu_cim(i,newsurf)
            phase(i) = tu_phase(i,newsurf)
          enddo
          hstep = tu_hstep(newsurf)
          istep = tu_istep(newsurf)
          time = tu_time(newsurf)
          lhop = .true.
        else if (frusflag(newsurf).eq.0.and.deltforw.le.tu_maxtime) then
!         do a TU hop forward
          write(6,*)'FORWARD HOP!'
          lhop = .true.
        else if (deltforw.gt.tu_maxtime) then
!         do a frustrated hop at the original hopping location
          do i=1,natoms
          do j=1,3
            xxm(j,i) = tu_xx(j,i,nsurf)
            ppm(j,i) = tu_pp(j,i,nsurf)
          enddo
          enddo
          do i=1,nsurft
            cre(i) = tu_cre(i,nsurf)
            cim(i) = tu_cim(i,nsurf)
            phase(i) = tu_phase(i,nsurf)
          enddo
          hstep = tu_hstep(nsurf)
          istep = tu_istep(nsurf)
          time = tu_time(nsurf)
          lhop = .true.
        endif
        if (lhop) then
          lfrust = .false.
          call getgrad(dmag,0)
!          call getgrad(dmag,vdotd)
          do i=1,nsurft
!           reset all backward hops
            tu_time(i) = -1.d0
          enddo
        endif
      endif

! hop or do a frustrated hop
      if ((methflag.eq.1.or.methflag.eq.5).and.lhop) then
! AJ SD
!  RVM stodecoflag is now an input variable
        if (stodecoflag.eq.1) then
        n1 = nsurf  ! current state
        n2 = newsurf  ! target state
        stodecotime=time ! save the time of the most recent hopping event in STODECOTIME
        call elecdeco(ithistraj,istep,time,nat,n1,n2, &
         pem,gpem,dvec,xxm,ppm,mmm,stodecotau)   ! returns STODECOTAU, which is the characteristic decoherence time computed using the JCP paper formula
        write(6,*)"Setting stodecotau to ",stodecotau*autofs," fs"
        endif
! AJ SD
!       switch surfaces or reflect/ignore frustrated hop
        call hop(newsurf)
        call getgrad(dmag,0)
!        call getgrad(dmag,vdotd)
        if (itherm.eq.2) call getpress(dimen,presst)
        do i=1,nsurft
!         reset all backward hops
          tu_time(i) = -1.d0
        enddo
      endif

! for Semiclassical Ehrenfest, change NSURF according to RHOR
      if (methflag.eq.2) then
        call getrho(cre,cim,rhor,rhoi,nsurft)
        tmp = 0.d0
        do i=1,nsurft
          if (rhor(i,i).gt.tmp) then
            tmp = rhor(i,i)
            nsurf = i
          endif
        enddo
      endif

! SCDM and CSDM, check for a decoherence state switch
      if (methflag.eq.3.or.methflag.eq.4) then
        lhop = .false.
        call decocheck(phop,nsurf,lhop,kev,speck,step,time, &
                       phopmax,phopmin)
        if (lhop) then
!      write(6,'(a,6f12.4)') '0gcre ',gcre(1),gcre(2),gcre(3)
!      write(6,'(a,6f12.4)') '0gcim ',gcim(1),gcim(2),gcim(3)
!      write(6,'(a,6f12.4)') '0cre ',cre(1),cre(2),cre(3)
!      write(6,'(a,6f12.4)') '0cim ',cim(1),cim(2),cim(3)
!      write(6,'(a,6f12.4)') '0cre-c ',cre(4),cre(5),cre(6)
!      write(6,'(a,6f12.4)') '0cim-c ',cim(4),cim(5),cim(6)
!      write(6,'(a,6f12.4)') '0gcre-c ',gcre(4),gcre(5),gcre(6)
!      write(6,'(a,6f12.4)') '0gcim-c ',gcim(4),gcim(5),gcim(6)
!      write(6,'(a,6f12.4)') '0pemd ',pemd(1,1),pemd(1,2),pemd(1,3)
!      write(6,'(a,6f12.4)') '0pemd ',pemd(2,1),pemd(2,2),pemd(2,3)
!      write(6,'(a,6f12.4)') '0pemd ',pemd(3,1),pemd(3,2),pemd(3,3)
          call getgrad(dmag,0)
! phoh debug
!      write(6,'(a,6f12.4)') 'gcre ',gcre(1),gcre(2),gcre(3)
!      write(6,'(a,6f12.4)') 'gcim ',gcim(1),gcim(2),gcim(3)
!      write(6,'(a,6f12.4)') 'cre  ',cre(1),cre(2),cre(3)
!      write(6,'(a,6f12.4)') 'cim  ',cim(1),cim(2),cim(3)
!      write(6,'(a,6f12.4)') 'pemd ',pemd(1,1),pemd(1,2),pemd(1,3)
!      write(6,'(a,6f12.4)') 'pemd ',pemd(2,1),pemd(2,2),pemd(2,3)
!      write(6,'(a,6f12.4)') 'pemd ',pemd(3,1),pemd(3,2),pemd(3,3)
          if (itherm.eq.2) call getpress(dimen,presst)
        endif
      endif

! Printings at each hopping event
      if  (nhop(2).ne.nhopsv.or.istep.eq.1) then
        nhopsv = nhop(2)
!c For large printings
!      endif

!      dvecmod = 0.0d0
!      do i=1,3
!         do j=1,natoms
!            dvecmod = dvecmod + dvec(i,j,1,2)**2
!         enddo
!      enddo
!      dvecmod = dsqrt(dvecmod)

        if (lwrite(27)) then
          if (repflag.eq.0) then
!  Adiabatic
            write(27,777) ithistraj,istep,nsurf,time*autofs, &
              (rhor(i,i),i=1,nsurft),(pema(i),i=1,nsurft)
!                     ,(vdotd(i,i),i=1,nsurft),vdotd(1,2), dvecmod
          elseif (repflag.eq.1) then
!  Diabatic
            write(27,777) ithistraj,istep,nsurf,time*autofs, &
              (rhor(i,i),i=1,nsurft),(pemd(i,i),i=1,nsurft)
!                     ,(vdotd(i,i),i=1,nsurft),vdotd(1,2), dvecmod
          endif
        endif

        if (lwrite(29).and.potflag.eq.3) then

          call relenerg(erel,distnh,nmax)
          linmol = .false.
          call linearbond(linmol,natoms,xxm,dum,nhbond,nhangle)

! Calculation of the nonplanarity angle, nang
          nang = 0.0d0
          do i=1,nmax
            nang = nang + nhangle(i)
          enddo
          nang = 360 - nang

          if (.not.lmodpop) then
            write(29,444)ithistraj,istep,nsurf,nhopsv,time*autofs, &
                   (pema(i)*autoev,i=1,nsurft), &
                   (pemd(i,i)*autoev,i=1,nsurft), &
                   (erel(i)*autoev,i=1,nmax), &
                   (nhbond(i)*autoang,i=1,nmax), &
                   (nhangle(i),i=1,nmax),nang

          else
            if (nmol.eq.1) call popmod(potflag,nmol,natoms,3*natoms-6,xxm,xx0,mmm, &
              ppm,nmpop)

              write(29,444)ithistraj,istep,nsurf,nhopsv,time*autofs, &
                          (pema(i)*autoev,i=1,nsurft),&
                          (pemd(i,i)*autoev,i=1,nsurft),&
                          (erel(i)*autoev,i=1,nmax),&
                          (nhbond(i)*autoang,i=1,nmax),&
                          (nhangle(i),i=1,nmax),nang,&
                          (nmpop(j),j=1,3*natoms-6)
          endif
        endif
! If we only want to print data at each allowed hop
      endif

!  Collect data to print trajectory information to unit 29

       if (lwrite(29).and.potflag.eq.3) then

         call relenerg(erel,distnh,nmax)
         linmol = .false.
         call linearbond(linmol,natoms,xxm,dum,nhbond,nhangle)

         if((nhbond(1)*autoang.gt.1.9.and.nhbond(1)*autoang.lt.2.2) .or. &
           (nhbond(2)*autoang.gt.1.9.and.nhbond(2)*autoang.lt.2.2) .or. &
           (nhbond(3)*autoang.gt.1.9.and.nhbond(3)*autoang.lt.2.2)) then
           egapint(istep) = (pema(2) - pema(1))*autoev
           nnstep(istep) = istep
           nhbondint(1,istep) = nhbond(1)*autoang
           nhbondint(2,istep) = nhbond(2)*autoang
           nhbondint(3,istep) = nhbond(3)*autoang
           nhangleint(1,istep) = nhangle(1)
           nhangleint(2,istep) = nhangle(2)
           nhangleint(3,istep) = nhangle(3)
           nangint(istep) = nang
           erelint(1,istep) = erel(1)
           erelint(2,istep) = erel(2)
           erelint(3,istep) = erel(3)
         else
           egapint(istep) = 20.d0
         endif

       endif

! For steepest descent (TFLAG(1) = 1)
! set PP to zero at every step
      if (tflag(1).eq.1) then
        do i=1,natoms
          do j=1,3
            ppm(j,i) = 0.d0
          enddo
        enddo
      endif
      if (tflag(1).ne.1) call gettemp(mnmol1)
! Momentum for simple Verlet algorithm is one step behind
      if (intflag.eq.2) then
        pept = pe
        pe = pep
      endif
      te = ke + pe
!
! check for trajectory termination here
! when termination occurs we skip out of the infinite
! "do 10 while" loop with "go to 999"
!
!     converge gradient (TERMFLAG = 2)
      if (termflag.eq.2) then
        gvmag = 0.d0
        maxgrad = 0.d0
        do i=1,natoms
          do j=1,3
            gvmag = gvmag + gv(j,i)**2
            if (dabs(gv(j,i)).gt.dabs(maxgrad)) maxgrad=gv(j,i)
          enddo
        enddo
        rmsgrad=dsqrt(gvmag/dble(natoms))
        if (rmsgrad.le.t_gradmag) then
          write(6,555) 'RMS gradient converged to ', &
                     rmsgrad*autoev/autoang,'eV/A'
          write(6,555) 'Maximum gradient converged to ', &
                     maxgrad*autoev/autoang,'eV/A'
          go to 999
        endif
      endif
!
!     monitor specific bond dissociation or forming (TERMFLAG = 3)
!      specifically used for atomdiatom terminating condition
      if (termflag.eq.3.and.initx(1).eq.4) then
        if (.not.isturn) then
          isturn=.true.
!       if two reactants are close enough, then a reaction may occur
          do k=1,nbondbreak
            if ( rij(tibreak(k,1),tibreak(k,2)).gt.trmax(k) ) then
               isturn=.false.
            endif
          enddo
        endif
        if (isturn) then
!        once they are far away again, terminate the trajectory
          do k=1,nbondbreak
            if ( rij(tibreak(k,1),tibreak(k,2)).gt.trmax(k) ) then
              indbr(k,1) = tibreak(k,1)
              indbr(k,2) = tibreak(k,2)
              outbreak = k
              goto 999
            endif
          enddo
        endif
      endif
!
!     monitor specific bond dissociation or forming (TERMFLAG = 3)
      if (termflag.eq.3.and.initx(1).ne.4) then
!       Bond breaking: find a distance greater than trmax(k)
        do k=1,nbondbreak
          if ( rij(tibreak(k,1),tibreak(k,2)).gt.trmax(k) ) then
            indbr(k,1) = tibreak(k,1)
            indbr(k,2) = tibreak(k,2)
            outbreak = k
            if (ibrkpre(k).eq.indbr(k,1)) then
                itp_max=itp_max+1
            else
                itp_max=0
            endif
            ibrkpre(k)=indbr(k,1)
            goto 100
!           Update bonding information: future implement
          endif
        enddo
 100    continue
!       Bond forming: find a distance less than trmin(k)
        do k=1,nbondform
          if ( rij(tiform(k,1),tiform(k,2)).lt.trmin(k) ) then
            indfm(k,1) = tiform(k,1)
            indfm(k,2) = tiform(k,2)
            outform = k
            if (ifmdpre(k).eq.indfm(k,1)) then
                itp_min=itp_min+1
            else
                itp_min=0
            endif
            ifmdpre(k)=indfm(k,1)
            goto 200
!           Update bonding information: future implement
          endif
        enddo
 200    continue
!       Count steps bond is broken
        if (itp_max.eq.termsteps) then
          ireact=1
          goto 999
        endif
!       Count steps bond is formed
        if (itp_min.eq.termsteps) then
          ireact=1
          goto 999
        endif
      endif
!
!  termflag = 7: Monitor unimolecular fragmentation
      if (termflag.eq.7) then
        if (time.gt.t_stime) goto 999
!       Call fragcom to see if fragments are leaving CoM
!        if (idebug) write(6,*) 'Fragments found,nmol',nfrag,nmol
        isdepart=.false.
!       n_frags: =0 or >=2, but !=1
        if (nfrdep.ge.1) then
         if ((n_frags.eq.0 .and. nfrag.ge.2) .or. nfrag.eq.n_frags) then
          if (n_atoffr.ne.0) then
            do k=1,nfrdep
!            do k=1,nfrag
!              natinfrag(k)>=1
              i1=indfrdep(k)
!              i1=k
              if (natinfrag(i1).eq.n_atoffr)  then
!              Judge if the fragment is the same as the required
                call ifsame(i1,mnmol1,issame)
                if (issame) then
                  ireact=1
                  goto 999
                endif
              endif
            enddo
!             Does not contain the fragment user required
              ireact=0
              goto 999
          else
!           User do not provide fragment to monitor so it is any
!           fragmentation with nfrag >=2
              ireact=1
              goto 999
          endif
!        Other conditions (nfrag>=3 but n_frags!=nfrag)
         elseif (nfrag.ge.3) then
                  ireact=0
                  goto 999
!        nfrag=1
         endif
        endif
      endif
!
!  termflag = 4: Monitoring fragments
      if (termflag.eq.4) then
!     Find out atoms with the same index in both fragment and original AG
        itp_c_min = 0
        do im=1,nmol
          do ifrag=1,nfrag
            k=0
            do i1=1,natinfrag(ifrag)
!            natinfrag(ifrag) >= natsame(im,ifrag)
              do i2=iatom(im),iatom(im)+natom(im)-1
                if (i2.eq.fragind(i1,ifrag) ) then
                  k=k+1
                  indsame(k,im,ifrag)=i2
                endif
              enddo
            enddo
            natsame(im,ifrag)=k
            if (idebug) write(6,*) 'im,ifrag,natsame',im,ifrag,k
          enddo
        enddo
        itp_c_max = 0
        if (agdefrag) then
          if (nfrag.gt.nmol) then
!          Fragment emerge from original AGs. Must be same type of
!          fragmentation
            if (nfrag.eq.nfrprev) then
              itp_c_max=1
            else
              itp_c_max=0
              itp_max=0
            endif
          endif
        endif
        if (agmerge) then
!          Two/more fragments merge into larger fragments
          if (nfrag.lt.nmol) then
            if (nfrdep.eq.nfrprev) then
              itp_c_merg=1
            else
              itp_merg=0
              itp_c_merg=0
            endif
          else
            itp_merg=0
            itp_c_merg=0
          endif
        endif
        if (attrans) then
          if (nmol.eq.nfrag) then
!          Judge if it is an atom/group transfer/exchange reaction
!          if no reaction occurs, fragments must have identical atom
!          numbers and indices as original AGs.
            itmp=0
            k=0
            do im=1,nmol
              do ifrag=1,nfrag
!                   natinfrag(ifrag) must >= natsame(im,ifrag)
                if ( natsame(im,ifrag).eq.natom(im) ) then
                  if (natsame(im,ifrag).eq.natinfrag(ifrag) ) then
                    itmp=itmp+1
                    isame(im)=ifrag
                  else
!                   here: natinfrag(ifrag) > natom(im)
!                   find out the atom which is exchanged/transfered,
!                   then jump out
                    do i1=1,natinfrag(ifrag)
                      ifind=0
                      do i2=iatom(im),iatom(im)+natom(im)-1
                        if (i2.eq.indsame(i1,im,ifrag) ) ifind=1
                      enddo
                      if (ifind.eq.0) then
                        k=k+1
                        itrsd(k)=indsame(i1,im,ifrag)
                      endif
                    enddo
                    goto 350
                  endif
                elseif (natsame(im,ifrag).eq.0) then
                  continue
                elseif (natsame(im,ifrag).lt.natom(im) ) then
!                 find out the atom which is exchanged/transfered
                  do i1=iatom(im),iatom(im)+natom(im)-1
                    ifind=0
                    do i2=1,natinfrag(ifrag)
                      if (i1.eq.indsame(i2,im,ifrag) ) ifind=1
                    enddo
                    if (ifind.eq.0) then
                      k=k+1
                      itrsd(k)=i1
                    endif
                    goto 350
                  enddo
                else
                  write(6,*) 'Something must be wrong'
                  stop
                endif
              enddo
            enddo
 350        ntrsd=k
            if (idebug) then
              write(6,*) 'Indices of transfered atoms found'
              write(6,*) (itrsd(i),i=1,ntrsd)
            endif
            if (itmp.eq.nmol) then
              itp_c_min=0
              itp_min=0
            else
              ifind=0
              do i1=1,ntrsd
                do i2=1,ntrsd
                  if (itrsprv(i1).eq.itrsd(i2)) ifind=ifind+1
                enddo
              enddo
              do i=1,ntrsd
                itrsprv(i)=itrsd(i)
              enddo
              if (ifind.eq.ntrsd) then
                itp_c_min=1
              else
                itp_c_min=0
                itp_min=0
              endif
            endif
          endif
        endif
!       Count steps bond is broken
 360    itp_max=itp_max+itp_c_max
        itp_merg=itp_merg+itp_c_merg
        itp_min=itp_min+itp_c_min
        if (itp_max.eq.termsteps .or. itp_min.eq.termsteps .or. &
            itp_merg.eq.termsteps) then
          write(6,106) 'Reaction occurs at ',time*autofs,' fs'
          ireact=1
          goto 999
        endif
        nfrprev=nfrdep
      endif
 399  continue
!
!     monitor torsion angles (only for unimolecular process)
      if(termflag .eq. 8 ) then
          nflip = 0
        do i = 1, ntorsion
           i1 = dihd(1,i)
           i2 = dihd(2,i)
           i3 = dihd(3,i)
           i4 = dihd(4,i)
           dang = dihedral(xxm(:,i1),xxm(:,i2),xxm(:,i3),xxm(:,i4))
           dang = dang*57.2958d0
           if(abs(dang).gt.dihlim(1,i).and.abs(dang).lt.dihlim(2,i)) then
             nflip = nflip +1
             indtor(nflip) = i
             dihfin(nflip) = dang
           endif
        enddo
        if(nflip .gt. 0) goto 999
        if (time.gt.t_stime) goto 999
      endif
!
!     monitor any bond forming/breaking (TERMFLAG = 5)
      if (termflag.eq.5) then
        itp_c_max = 0
        itp_c_min = 0
        do i1=1,natoms
          if (nbond(i1).le.nbondi(i1) ) then
            do i=1,nbondi(i1)
              itmp=0
              do j=1,nbond(i1)
                if (bndindi(i,i1).eq.bndind(j,i1) ) itmp=1
              enddo
!        Bond broken
              if (itmp.eq.0) then
                indbr(1,1) = i1
                indbr(1,2) = bndindi(i,i1)
                if (ibrkpre(i).eq.indbr(1,1)) then
                   itp_max=itp_max+1
                 else
                   itp_max=0
                endif
                ibrkpre(i)=indbr(1,1)
                goto 400
              endif
            enddo
          endif
 400      continue
          if (nbond(i1).ge.nbondi(i1) ) then
            do i=1,nbond(i1)
              itmp=0
              do j=1,nbondi(i1)
                if (bndind(i,i1).eq.bndindi(j,i1) ) itmp=1
              enddo
!        Bond formed
              if (itmp.eq.0) then
                indfm(1,1) = i1
                indfm(1,2) = bndind(i,i1)
                if (ifmdpre(i).eq.indfm(1,1)) then
                   itp_min=itp_min+1
                 else
                   itp_min=0
                endif
                ifmdpre(i)=indfm(1,1)
                goto 450
              endif
            enddo
          endif
        enddo
!       Count steps bond is broken
 450    continue
        if (itp_max.eq.termsteps) then
          outbreak=1
          ireact=1
          goto 999
        endif
!       Count steps bond is formed
        if (itp_min.eq.termsteps) then
          outform=1
          ireact=1
          goto 999
        endif
      endif
!
!     Average on sticking time, 1. Reaction must occurs; 2. The
!     outcoming product must have the same number and same atom types as
!     reactant
      if (termflag.eq.6) then
!       Judging if a reaction occurs (forming at least one bond)
        if (ireact.eq.0) then
          do i=1,natom(2)
            do j=1,natom(1)
              if (bndmatr(iatom(2)+i-1,iatom(1)+j-1).ne.0) then
                ireact=1
                t0react=time
                goto 500
              endif
            enddo
          enddo
        endif
!       Judge if the reactant comes out again
! itp_max count Dissociate into more than 2 pieces
 500    if (ireact.eq.1) then
          if (nfrag.eq.2) then
!     Find out atoms with the same index in both fragment and original AG
!         1) natom(1).eq.natom(2)
            if (natom(1).eq.natom(2)) then
!               natom(1)=natinfrag(1)=natom(2)=natinfrag(2)
                if (natinfrag(1).eq.natinfrag(2)) then
                  call ifsame(1,1,issame)
                  if (.not.issame) call ifsame(1,2,issame)
                else
                  issame=.false.
                endif
!         2) natom(1).ne.natom(2)
            else
                if (natom(1).eq.natinfrag(1)) then
                  call ifsame(1,1,issame)
                elseif (natom(2).eq.natinfrag(1)) then
                  call ifsame (2,1,issame)
                else
                  issame=.false.
                endif
            endif
              call rminfrag(1,2,trmax(1),indbr(1,1),indbr(1,2))
              if (trmax(1).gt.bdbrthr*rijco(indbr(1,1),indbr(1,2))) then
                if (issame) then
                  trxover=time
                  goto 999
                else
                  trxover=t0react
                  ireact=0
                  goto 999
                endif
              endif
!         Still stick together
          elseif (nfrag.eq.1) then
            trxover=time
          else
!           Dissociate into more than 2 pieces, unwanted product type
            i1=0
            do k=1,3
             do l=k+1,3
               i1=i1+1
               call rminfrag(k,l,trmax(1),indbr(1,1),indbr(1,2))
               if (trmax(1).gt.bdbrthr*rijco(indbr(1,1),indbr(1,2))) then
                 isdepart=.true.
               else
                 isdepart=.false.
               endif
             enddo
            enddo
!           Reaction over but not what we want (set ireact=0)
            if (isdepart) then
              trxover=t0react
              ireact=0
              goto 999
            endif
          endif
        else
!         if no reaction observed set trxover=t0react
          trxover=t0react
        endif
      endif
!     For reaction collision, if the CoM distance between the two AGs
!     is less than r0collision, a collision occurs, if after some time,
!     the CoM distance greater than r0collision again, terminate the
!     simulation
!     Calculate the CoM distance of the two colliding AGs
      if (reactcol) then
!     first AG
        com1x = 0.d0
        com1y = 0.d0
        com1z = 0.d0
        com2x = 0.d0
        com2y = 0.d0
        com2z = 0.d0
        do i=1,natom(1)
          com1x = com1x + mmm(i)*xxm(1,i)
          com1y = com1y + mmm(i)*xxm(2,i)
          com1z = com1z + mmm(i)*xxm(3,i)
        enddo
        com1x = com1x/mmofag(1)
        com1y = com1y/mmofag(1)
        com1z = com1z/mmofag(1)
        do i=natom(1)+1,natoms
          com2x = com2x + mmm(i)*xxm(1,i)
          com2y = com2y + mmm(i)*xxm(2,i)
          com2z = com2z + mmm(i)*xxm(3,i)
        enddo
        com2x = com2x/mmofag(2)
        com2y = com2y/mmofag(2)
        com2z = com2z/mmofag(2)
        rcom = (com1x-com2x)**2 + (com1y-com2y)**2 + (com1z-com2z)**2
        rcom = dsqrt(rcom)
  !     write(6,*) 'rcom = ', rcom*autoang
        if (.not.iscollide) then
          if (rcom.le.r0collision) then
            iscollide=.true.
          elseif (nfrdep.ge.nmol) then
            write(6,*) 'Dissociated before collision: exit'
            goto 999
          endif
        else
          comp1x = 0.d0
          comp1y = 0.d0
          comp1z = 0.d0
          comp2x = 0.d0
          comp2y = 0.d0
          comp2z = 0.d0
          ke1=0.d0
          ke2=0.d0
          do i=1,natom(1)
            comp1x = comp1x + ppm(1,i)
            comp1y = comp1y + ppm(2,i)
            comp1z = comp1z + ppm(3,i)
          enddo
          ke1 = comp1x*comp1x + comp1y*comp1y + comp1z*comp1z
          ke1 = 0.5d0*ke1/mmofag(1)
          do i=natom(1)+1,natoms
            comp2x = comp2x + ppm(1,i)
            comp2y = comp2y + ppm(2,i)
            comp2z = comp2z + ppm(3,i)
          enddo
          ke2 = comp2x*comp2x + comp2y*comp2y + comp2z*comp2z
          ke2 = 0.5d0*ke2/mmofag(2)
! Projection of CoM momentum of reactant 2 on CoM vector
          rcom2  = com2x*com2x + com2y*com2y + com2z*com2z
          rcom2  = dsqrt(rcom2)
          comv(1)= com2x/rcom2
          comv(2)= com2y/rcom2
          comv(3)= com2z/rcom2
          compro = comp2x*comv(1)+comp2y*comv(2)+comp2z*comv(3)
          if (agmerge) then
!           if (.not.isttstick) ttstick=0.d0
           if (isstick) then
!              write(6,*) 'nfrag,nfrdep,natinfrag(indfrdep),isreturn', &
!              nfrag,nfrdep,(natinfrag(indfrdep(k)),k=1,nfrdep),isreturn
             if (nfrdep.eq.2) then
               if (natinfrag(indfrdep(1)).eq.natom(2) .or.  &
                        natinfrag(indfrdep(2)).eq.natom(2)) then
                 write(6,555) 'Two reactants leave close contact at ', &
                        time*autofs,' fs'
               else
                 write(6,222) 'The formed product dissociated into 2 ', &
                   'products other than the reactants at ',time*autofs,' fs'
               endif
             elseif (nfrdep.gt.2) then
               write(6,222) 'The formed product dissociated into more ', &
                 'than 2 products at ',time*autofs,' fs'
             endif
!          Incoming reactant turn back
             if (.not.isturn .and. compro.gt.0.d0) then
               isturn=.true.
               iturn=iturn+1
               write(6,555) 'Two reactants reaches the turning point at ', &
                 & time*autofs,' fs'
               if (iturn.eq.1) t0stick=time
               if (iturn.ge.1) ttstick=time-t0stick
               if (termflag.ne.4 .and. nfrdep.lt.2.and.iturn.ge.n_turn) then
                 ireact=1
                 goto 999
               endif
             endif
!          Incoming reactant come back again
             if (isturn .and. compro.lt.0.d0) then
               isreturn=.true.
               isturn=.false.
               iturn=iturn+1
               if (iturn.ge.1) ttstick=time-t0stick
             endif
           else
             if (nfrag.lt.nmol) then
               t0stick=time
               isstick=.true.
               write(6,555) 'Two reactants in close contact at ', &
                 time*autofs,' fs'
             endif
           endif
         endif
         if (lwrite(77)) then
           write(77,*) ithistraj, istep,time*autofs,rcom*autoang, &
                      ke1*autoev,ke2*autoev,compro, &
                      0.5d0*compro*compro*autoev/mmofag(2)
         endif
         do i=1,nfrag
           if (rcommin(i).gt.r0collision .and. nfrdep.ge.2) then
             write(6,*) 'Collision ends: exit '
             ! added by J. Zheng for termflag=3 and 5
             if (termflag.eq.3.or.termflag.eq.5) then
               if(itp_max.lt.termsteps.and.itp_max.gt.0) then
                 if(termflag.eq.5) outbreak=1
                 call ifsame2(1,1,issame)
                 if(.not.issame) ireact=1
               endif
               if(itp_min.lt.termsteps.and.itp_min.gt.0) then
                 if(termflag.eq.5) outform=1
                 call ifsame2(1,1,issame)
                 if(.not.issame) ireact=1
               endif
             endif
              ! end
             goto 999
           endif
         enddo
        endif
      endif
!
! on-the-fly analyses
      atempm = atempm + temp*step
      atemp2 = atemp2 + (temp**2)*step
      atemp = atempm/time
      stemp = dsqrt(dabs(atemp2/time - atemp**2))
      if (lwrite(41)) then
        if (mod(istep,nprint).eq.0.or.istep.eq.1) then
          call radialdist(ithistraj,step,time,nbinrad,rbmax,rbmin,rbin,raddist,1)
        else
          call radialdist(ithistraj,step,time,nbinrad,rbmax,rbmin,rbin,raddist,0)
        endif
      endif
!
!  Thermal statistic on some properties
!      minte=min(minte,te)
      if (tflag(1).eq.5) then
        call get_rng(ran)
        if (ran.le.pickthr) then
          iopt = iopt + 1
          write(6,333)'Geometry optimization at step ',istep, &
          ' at time ',time*autofs,' fs, Temperature ',atemp,' K'
          do i=1,natoms
            j = 3*i
            p(j-2) = xxm(1,i)
            p(j-1) = xxm(2,i)
            p(j)   = xxm(3,i)
          enddo
          call dfpmin(p,ndim,ndim,t_gradmag,t_gradmax,iter,efinal)
          write(28,121) iopt,efinal*autoev,istep,time*autofs,atemp
          do i=1,natoms
            k = 3*(i-1)
            write(28,1010) symbolm(i),mmm(i)/amutoau, &
               (p(k+j)*autoang,j=1,3)
          enddo
          minpe=min(minpe,efinal)
          if (efinal.eq.minpe) then
            do i=1,natoms
              k = 3*(i-1)
              do j=1,3
                xxlow(j,i)=p(k+j)
              enddo
            enddo
          endif
        endif
      else
        minpe=min(minpe,pe)
        if (pe.eq.minpe .and. maxprint) then
          do i=1,natoms
            do j=1,3
              xxlow(j,i)=xxm(j,i)
            enddo
          enddo
        endif
      endif
      if (istep.gt.nstatm .and. tflag(1).ne.1) then
        istat  = istat + 1
        timestat = timestat+step
        call lindemann(step,timestat,lind,lindi,rcomi,rcomi2,rcomb, &
                       arcomd,arcomd2,rbin,mbin,lindb,lindis,anbin,lindisa, &
                       iflind,ifrcom,ifbin)
!       Volume by Nate's method
        if (lwrite(50).or.lwrite(501)) then
          call volume(volt,rhot,natoms)
          voln = voln + volt*step
          voln2= voln2+ volt*volt*step
          rhon = rhon + rhot*step
          rhon2= rhon2+ rhot*rhot*step
        endif
!        fpart = fpart + step*dexp(-(te-e00)/kbt0)
!        if (te.ge.de0) then
!                timeup=timeup+step
!                fpartup = fpartup + step*dexp(-(te-e00)/kbt0)
!        endif
        ate  = ate + te*step
        ate2 = ate2 + (te**2)*step
        if (cohesive) then
          ce   = -pe/dble(natoms)
          ace  = ace + ce*step
          ace2 = ace2 + (ce**2)*step
        endif
        if (icfrag) then
!        average on concentration of the fragments
          do j=1,natoms
            cfrag(natinfrag(j))=cfrag(natinfrag(j))+ &
                  dble(natinfrag(j))*step/vol
          enddo
        endif
        if (kinetic) then
          ake  = ake + ke*step
          ake2 = ake2 + (ke**2)*step
        endif
        if (potential) then
          ape  = ape + pe*step
          ape2 = ape2 + (pe**2)*step
        endif
!        temm(istat)=te
!        tempmm(istat)=temp
!       Properties of the shape of the cluster
!        if ((tflag(1).ge.2.and.tflag(1).le.4)) then
        if (lwrite(40).or.lwrite(401)) then
          call rotprin(xxm,mmm,natoms,momiprin,rotaxis,iflinear)
            tmp=(momiprin(1)+momiprin(2)+momiprin(3))/3.d0
            tmp1=-1.d0
            do j=1,3
              tmp1=max(tmp1,dabs(momiprin(j)-tmp))
              rgm(j) = dsqrt(2.5d0*momiprin(j)/mmofag(mnmol1))*autoang
              if (tmp1.eq.dabs(momiprin(j)-tmp)) itmp=j
            enddo
            tmp    = momiprin(itmp)/tmp
            aspher = aspher + tmp*step
            aspher2= aspher2+ tmp*tmp*step
!       Volume and density of the cluster by radius of gyration method
!            Volume here in A^3
            volt  = (4.d0/3.d0)*pi*rgm(1)*rgm(2)*rgm(3)
            rhot  = mmofag(mnmol1)/volt
!           convert rhot from au/A^3 to g/cm3 /(1.d-8 **3 *Na *amutoau)
            rhot  = rhot/(0.60221415d0*amutoau)
            volg  = volg + volt*step
            volg2 = volg2+ volt*volt*step
            rhog  = rhog + rhot*step
            rhog2 = rhog2+ rhot*rhot*step
!            tmp = 2.d0 + momiprin(1)/momiprin(3) - &
!                  2.d0 * momiprin(2)/momiprin(3)
!            aspherli = aspherli + tmp*step
!            aspherli2= aspherli2+ tmp*tmp*step
        endif
      endif
!
! write info every nprint steps
      call getrho(cre,cim,rhor,rhoi,nsurft)
      if (mod(istep,nprint).eq.0.or.istep.eq.1) then
        iwrite=iwrite+1
        if (timestat.gt.1.d-2) then
          rtim = 1.d0/timestat
          if (lwrite(400)) then
            avolg = volg*rtim
            arhog = rhog*rtim
            if (lwrite(50).or.lwrite(500)) then
              avoln = voln*rtim
              arhon = rhon*rtim
              write(400,108)ithistraj,istep,time*autofs, &
              atemp-tempzpe,lind,cv*autoev, &
              ace*autoev*rtim,ake*autoev*rtim,ate*autoev*rtim, &
              aspher*rtim,dsqrt(aspher2*rtim-(aspher*rtim)**2), &
              arcomd*autoang*rtim,arcomd2*autoang2*rtim, &
              avoln,dsqrt(voln2*rtim-avoln**2), &
              arhon,dsqrt(rhon2*rtim-arhon**2), &
              avolg,dsqrt(volg2*rtim-avolg**2), &
              arhog,dsqrt(rhog2*rtim-arhog**2)
            else
              write(400,108)ithistraj,istep,time*autofs, &
              atemp-tempzpe,lind,cv*autoev, &
              ace*autoev*rtim,ake*autoev*rtim,ate*autoev*rtim, &
              aspher*rtim,dsqrt(aspher2*rtim-(aspher*rtim)**2), &
              arcomd*autoang*rtim,arcomd2*autoang2*rtim, &
              avolg,dsqrt(volg2*rtim-avolg**2), &
              arhog,dsqrt(rhog2*rtim-arhog**2)
            endif
          endif
          if (lwrite(500)) then
            avoln = voln*rtim
            arhon = rhon*rtim
            write(500,111)ithistraj,istep,time*autofs, &
            atemp-tempzpe,avoln,dsqrt(voln2*rtim-avoln*avoln)/avoln, &
            arhon,dsqrt(rhon2*rtim-arhon*arhon)/arhon
          endif
          if (lwrite(430))  &
            write(430,111)ithistraj,istep,time*autofs, &
            atemp-tempzpe,arcomd*autoang*rtim, &
            (rcomi(i)*autoang*rtim,i=1,natoms)
          if (lwrite(440)) write(440,111)ithistraj,istep,time*autofs, &
            atemp-tempzpe,arcomd2*autoang2*rtim, &
            (rcomi2(i)*autoang2*rtim,i=1,natoms)
          if (lwrite(450)) write(450,111)ithistraj,istep,time*autofs, &
            atemp-tempzpe,lind,(lindi(i),i=1,natoms)
          if (lwrite(460).or.lwrite(510)) then
            do i=1,mbin+1
              if (anbin(i).gt.1.d-3) then
!                tmp=anbin(i)*rtim
                if (lwrite(460)) lindbm(i)=lindb(i)/anbin(i)
                if (lwrite(510)) rcombm(i)=rcomb(i)/anbin(i)
              else
                if (lwrite(460)) lindbm(i)=0.d0
                if (lwrite(510)) rcombm(i)=0.d0
              endif
            enddo
            if (lwrite(460)) write(460,111)ithistraj,istep,time*autofs, &
               atemp-tempzpe,lind,(lindbm(i),i=1,mbin+1)
            if (lwrite(510)) write(510,111)ithistraj,istep,time*autofs, &
                atemp-tempzpe,arcomd*rtim*autoang, &
                (rcombm(i)*autoang,i=1,mbin+1)
          endif
!          if (lwrite(470)) write(470,111)ithistraj,istep,time*autofs, &
!            atemp-tempzpe,lind,(lindis(i),i=1,natoms)
!          if (lwrite(480)) write(480,111)ithistraj,istep,time*autofs, &
!            atemp-tempzpe,lind,(lindisa(i)*rtim,i=1,natoms)
          if (lwrite(490)) write(490,111)ithistraj,istep,time*autofs, &
            atemp-tempzpe,lind,(anbin(i)*rtim,i=1,mbin+1)
        endif
        if (lwrite(42)) call honey(ithistraj,time)
        if (termflag.eq.2) then
          write(6,101)time*autofs,pe*autoev,rmsgrad*autoev/autoang
        else
          if (itherm.eq.2) then
            if (nsurft.eq.1) then
              write(6,101)time*autofs,temp,press*autoatm,ke*autoev, &
                pe*autoev,te*autoev, (comppm(k)*autoang/(amutoau*autofs),k=1,3), &
                totcomp*autoang/(amutoau*autofs), lind,atemp,stemp
            else
              write(6,101)time*autofs,temp,press*autoatm,ke*autoev, &
                pe*autoev,te*autoev, (comppm(k)*autoang/(amutoau*autofs),k=1,3), &
                totcomp*autoang/(amutoau*autofs), (rhor(k,k),k=1,nsurft),dble(nsurf)
            endif
          else
            if (nsurft.eq.1) then
              write(6,101)time*autofs,temp,ke*autoev, pe*autoev,te*autoev, &
                (comppm(k)*autoang/(amutoau*autofs),k=1,3), &
                totcomp*autoang/(amutoau*autofs),lind,atemp,stemp
            else
              write(6,101)time*autofs,temp,ke*autoev, pe*autoev,te*autoev, &
              (comppm(k)*autoang/(amutoau*autofs),k=1,3), &
                totcomp*autoang/(amutoau*autofs), (rhor(k,k),k=1,nsurft),dble(nsurf)
            endif
          endif
        endif
!        if (istep.ge.itosave) then
        if (lwrite(22)) then
          write(22,*) natoms
          write(22,*) ithistraj,istep,time*autofs,pe*autoev
!         write(99,*)ithistraj,time*autofs,rhor(5,5),rhor(7,7), rhor(11,11)
!         write(22,*)
          do i=1,natoms
!          write(22,1010) symbolm(i),mmm(i)/amutoau, (xxm(j,i)*autoang,j=1,3)
           write(22,1010) symbolm(i),(xxm(j,i)*autoang,j=1,3)
          enddo
        endif
        if (lwrite_h5(22)) then
          call hdf5o_trj_insert_22(natoms,time*autofs,pe*autoev,xxm*autoang)
        end if

        if (lwrite(23)) then
          write(22,*) natoms
          write(23,*) ithistraj,istep,time*autofs,ke*autoev
          do i=1,natoms
            write(23,1010) symbolm(i),mmm(i)/amutoau, (ppm(j,i),j=1,3)
          enddo
        endif
        if (lwrite_h5(23)) then
          call hdf5o_trj_insert_23(natoms,time*autofs,ke*autoev,ppm)
        end if
        if (lwrite_h5(22) .or. lwrite_h5(23)) then
          call hdf5o_trj_move()
        end if
! JZ : write PES information every nprint step
        if (lwrite(221))  then
           write(221,'(i7,f10.2,es14.5e3,20f10.3)')ithistraj,time*autofs &
           ,weight,pe*autoev,pema(1:nsurft)*autoev,pemd(1,1)*autoev,pemd(2,2)*autoev
        endif
        if (lwrite(222))  then
!          write(222,'(i7,f10.2,es14.5e3,20f8.3)')ithistraj,time*autofs, &
!      weight,((dsqrt(rhor(i,j)**2+rhoi(i,j)**2),j=i,nsurft),i=1,nsurft)
        do i = 1, nsurft
          do j = 1, nsurft
           delphase = phase(j)-phase(i)
          enddo
        enddo
           write(222,'(i7,f10.2,es14.5e3,20f8.3)')ithistraj,time*autofs, &
        weight,(((rhor(i,j)*dcos(delphase(i,j))+rhoi(i,j)* &
        dsin(delphase(i,j))),j=i,nsurft),i=1,nsurft)
        endif
        if (lwrite(223))  then
          write(223,'(i7,f10.2,es14.5e3,20f10.3)')ithistraj,time*autofs, &
          weight, ((pemd(i,j)*autoev,j = i, nsurft),i = 1, nsurft)
        endif
! still print something to cheat final state code
!       if(lwrite(224)) then
!          write(224,'(i7,f10.2,es14.5e3,20f8.3)')ithistraj,time*autofs,  &
!       weight,(((rhor(i,j)*dcos(delphase(i,j))+rhoi(i,j)* &
!       dsin(delphase(i,j))),j=i,nsurft),i=1,nsurft)
!         call adtod(1,1,denr,deni)
!         write(224,'(i7,f10.2,es14.5e3,20f8.3)')ithistraj,time*autofs,  &
!      weight,((dsqrt(denr(i,j)**2+deni(i,j)**2),j=i,nsurft),i=1,nsurft)
!       endif

!       endif
      endif

! ramp temperature and momentum
      if ((tflag(1).ge.2.and.tflag(1).le.5).and.istep.ge.nrampstep) then
       nrampt = nrampt + 1
       if ( nrampt.eq.(n_ramp+nequil) .or. istep.eq.nrampstep ) then
        iramp  = iramp + 1
        nrampt = 0
        rtim = 1.d0/timestat
        cv = (ate2*rtim - (ate*rtim)**2)/(kb*(atemp-tempzpe)**2)
        if (lwrite(40)) then
          avolg = volg*rtim
          arhog = rhog*rtim
          if (lwrite(50)) then
            avoln = voln*rtim
            arhon = rhon*rtim
            write(40,108)ithistraj,istep,time*autofs, &
            atemp-tempzpe,lind,cv*autoev,&
            ace*autoev*rtim,ake*autoev*rtim,ate*autoev*rtim,&
            aspher*rtim,dsqrt(aspher2*rtim-(aspher*rtim)**2), &
            arcomd*autoang*rtim,arcomd2*autoang2*rtim, &
            avoln,dsqrt(voln2*rtim-avoln**2), &
            arhon,dsqrt(rhon2*rtim-arhon**2), &
            avolg,dsqrt(volg2*rtim-avolg**2), &
            arhog,dsqrt(rhog2*rtim-arhog**2)
          else
            write(40,108)ithistraj,istep,time*autofs, &
            atemp-tempzpe,lind,cv*autoev, &
            ace*autoev*rtim,ake*autoev*rtim,ate*autoev*rtim, &
            aspher*rtim,dsqrt(aspher2*rtim-(aspher*rtim)**2), &
            arcomd*autoang*rtim,arcomd2*autoang2*rtim, &
            avolg,dsqrt(volg2*rtim-avolg**2), &
            arhog,dsqrt(rhog2*rtim-arhog**2)
          endif
        endif
        if (lwrite(43))  &
          write(43,111)ithistraj,istep,time*autofs, &
            atemp-tempzpe,arcomd*autoang*rtim, &
            (rcomi(i)*autoang*rtim,i=1,natoms)
        if (lwrite(44)) write(44,111)ithistraj,istep,time*autofs, &
            atemp-tempzpe,arcomd2*autoang2*rtim, &
            (rcomi2(i)*autoang2*rtim,i=1,natoms)
        if (lwrite(45)) write(45,111)ithistraj,istep,time*autofs, &
            atemp-tempzpe,lind,(lindi(i),i=1,natoms)
          if (lwrite(46).or.lwrite(51)) then
            do i=1,mbin+1
              if (anbin(i).gt.1.d-3) then
!                tmp=anbin(i)*rtim
                if (lwrite(46)) lindbm(i)=lindb(i)/anbin(i)
                if (lwrite(51)) rcombm(i)=rcomb(i)/anbin(i)
              else
                if (lwrite(46)) lindbm(i)=0.d0
                if (lwrite(51)) rcombm(i)=0.d0
              endif
            enddo
            if (lwrite(46)) write(46,111)ithistraj,istep,time*autofs, &
               atemp-tempzpe,lind,(lindbm(i),i=1,mbin+1)
            if (lwrite(51)) write(51,111)ithistraj,istep,time*autofs, &
                atemp-tempzpe,arcomd*rtim*autoang, &
                (rcombm(i)*autoang,i=1,mbin+1)
          endif
!          if (lwrite(47)) write(47,111)ithistraj,istep,time*autofs, &
!            atemp-tempzpe,lind,(lindis(i),i=1,natoms)
!          if (lwrite(48)) write(48,111)ithistraj,istep,time*autofs, &
!            atemp-tempzpe,lind,(lindisa(i)*rtim,i=1,natoms)
          if (lwrite(49)) write(49,111)ithistraj,istep,time*autofs, &
            atemp-tempzpe,(anbin(i)*rtim,i=1,mbin+1)
        if (lwrite(42)) call honey(ithistraj,step)
!       Ramp momentum
        if (tflag(1).eq.2) then
          do i=1,natoms
            do j=1,3
              ppm(j,i)=ppm(j,i)*rampfact
            enddo
          enddo
!       Change temperature
        else
          temptarg=temptarg+rampfact
          kbtarg=kb*temptarg
        endif
       endif
       if (iramp.gt.nramp) then
         if (tflag(1).eq.4) then
           do i=1,natoms
             j = 3*i
             p(j-2) = xxm(1,i)
             p(j-1) = xxm(2,i)
             p(j)   = xxm(3,i)
           enddo
           call dfpmin(p,ndim,ndim,t_gradmag,t_gradmax,iter,efinal)
           goto 999
         else
!          write geometry at this time
           if (lwrite(26)) then
           write(26,*)ithistraj,iramp,atemp-tempzpe,pe*autoev
           do i=1,natoms
             write(26,1010) symbolm(i),mmm(i)/amutoau, &
                           (xxm(j,i)*autoang,j=1,3)
           enddo
           endif
           goto 999
         endif
       endif
!      starting average on properties for a period of nequil steps
!      The lindemann parameter calc. when ramping temperature is an
!      average of this time only
       if ( nrampt.eq.n_ramp .or.  nrampt.eq.0 ) then
!       write geometry at this time
        if (lwrite(26)) then
        write(26,*)ithistraj,iramp,atemp-tempzpe,pe*autoev
        do i=1,natoms
          write(26,1010) symbolm(i),mmm(i)/amutoau, &
                          (xxm(j,i)*autoang,j=1,3)
        enddo
        endif
        atempm = 0.d0
        atemp2 = 0.d0
        stemp  = 0.d0
        nstatm = 1
        istat=0
        timestat=0.d0
        time = 0.d0
        ace  = 0.d0
        ace2 = 0.d0
        ate  = 0.d0
        ate2 = 0.d0
        ake  = 0.d0
        ake2 = 0.d0
        ape  = 0.d0
        ape2 = 0.d0
        arcomd = 0.d0
        arcomd2= 0.d0
        aspher = 0.d0
        aspher2= 0.d0
        voln   = 0.d0
        voln2  = 0.d0
        rhon   = 0.d0
        rhon2  = 0.d0
        volg   = 0.d0
        volg2  = 0.d0
        rhog   = 0.d0
        rhog2  = 0.d0
!        aspherli = 0.d0
!        aspherli2= 0.d0
        do i=1,mnbin
          lindb(i)=0.d0
          anbin(i)=0.d0
          rcomb(i)=0.d0
        enddo
        do i=1,natoms
          rcomi(i)=0.d0
          rcomi2(i)=0.d0
          lindi(i)=0.d0
          lindisa(i)=0.d0
          do j=1,natoms
            arij(i,j) = 0.d0
            arij2(i,j) = 0.d0
          enddo
        enddo
        do i=0,nbinrad+1
          raddist(i) = 0.d0
        enddo
       endif
      endif

!     if (methflag.eq.3.or.methflag.eq.4) then
! If system has already decoherent to target state, we don't count the
! decoherence time in the time average
      if (methflag.eq.3.or.methflag.eq.4 &
          .and.rhor(nsurf,nsurf).lt.0.98d0) then
      do k=1,nsurft
!     if (rhor(k,k).gt.0.02d0.and.rhor(k,k).lt.0.98d0  &
!         .and. taui(k).gt.1.d-7) then
! Taui is the inverse of tau, therefore, time average of 1/tau is the
! average of taui
! JZ 09-19-2014
!     if(rhor(k,k).lt.0.98d0.and.k.ne.nsurf) then
      if(k.ne.nsurf) then
!      atau=atau + step*taui(k)
       atau=atau + step*taui(k)*rhor(k,k)
      endif
      enddo
      tautim=tautim+step
      endif

      sumw = sumw + weight
 10   continue

! ######################################################################
! ######################################################################

! trajectory has ended
 999  continue
! Geometry optimization for the last step for tflag(1)=5
      if (tflag(1).eq.5) then
         write(6,333)'Geometry optimization at step ',istep,' at time ', &
            time*autofs,' fs, Temperature ',atemp,' K'
          do i=1,natoms
            j = 3*i
            p(j-2) = xxm(1,i)
            p(j-1) = xxm(2,i)
            p(j)   = xxm(3,i)
          enddo
          call dfpmin(p,ndim,ndim,t_gradmag,t_gradmax,iter,efinal)
          write(28,121) iopt,efinal*autoev,istep,time*autofs,atemp
          do i=1,natoms
            k = 3*(i-1)
            write(28,1010) symbolm(i),mmm(i)/amutoau, &
               (p(k+j)*autoang,j=1,3)
          enddo
          minpe=min(minpe,efinal)
          if (efinal.eq.minpe) then
            do i=1,natoms
              k = 3*(i-1)
              do j=1,3
                xxlow(j,i)=p(k+j)
              enddo
            enddo
          endif
      endif
!
! Reaction collision integeration
      if (reactcol.or.termflag.eq.7) then
!       Reaction propability
!       rxprob = rxprob+dble(ireact)
! JZ 09-16-2014
        rxprob = rxprob + weight
!       if (isstick) icollide = icollide+1
        if (isstick) then
            icollide = icollide+1
            collw = collw + weight
        endif
      endif
      if (termflag.eq.7 .and. ireact.eq.1) then
!        write(6,106) 'AG fragmentation time = ',time*autofs,' fs'
        write(6,*) 'nfrag natinfrag time = ',nfrag, &
               (natinfrag(i),i=1,nfrag),time*autofs,' fs'
         if (lwrite(24)) then
          do ifrag=1,nfrag
           write(24,*) '#traj, frag ',ithistraj,ifrag
           do i=1,natinfrag(ifrag)
             j=fragind(i,ifrag)
             write(24,1011) j,symbolm(j),mmm(j)/amutoau, &
                          (xxm(k,j)*autoang,k=1,3)
           enddo
          enddo
         endif
         if (lwrite(25)) then
          do ifrag=1,nfrag
           write(25,*) '#traj, frag ',ithistraj,ifrag
           do i=1,natinfrag(ifrag)
             j=fragind(i,ifrag)
             write(25,1011) j,symbolm(j),mmm(j)/amutoau, &
                           (ppm(k,j),k=1,3)
           enddo
          enddo
         endif
      endif
!
! write final coordinates and momenta to output
      write(6,*)
      write(6,*) 'Final information for trajectory',ithistraj
! write final coordinates and momenta to final storage for analysis in
! final state
      do i=1,natoms
        do j=1,3
          xxf(j,i)=xxm(j,i)
          ppf(j,i)=ppm(j,i)
        enddo
      enddo
      if (termflag.eq.2) then
        write(6,101)time*autofs,pe*autoev,rmsgrad*autoev/autoang
      else
        if (itherm.eq.2) then
          if (nsurft.eq.1) then
            write(6,101)time*autofs,temp,press*autoatm,ke*autoev, &
              pe*autoev,te*autoev, &
              (comppm(k)*autoang/(amutoau*autofs),k=1,3), &
              totcomp*autoang/(amutoau*autofs), &
              lind,atemp,stemp
          else
            write(6,101)time*autofs,temp,press*autoatm,ke*autoev, &
              pe*autoev,te*autoev, &
              (comppm(k)*autoang/(amutoau*autofs),k=1,3), &
              totcomp*autoang/(amutoau*autofs), &
              (rhor(k,k),k=1,nsurft),dble(nsurf)
          endif
        else
          if (nsurft.eq.1) then
            write(6,101)time*autofs,temp,ke*autoev, &
              pe*autoev,te*autoev, &
              (comppm(k)*autoang/(amutoau*autofs),k=1,3), &
              totcomp*autoang/(amutoau*autofs), &
              lind,atemp,stemp
          else
            write(6,101)time*autofs,temp,ke*autoev, &
              pe*autoev,te*autoev, &
              (comppm(k)*autoang/(amutoau*autofs),k=1,3), &
              totcomp*autoang/(amutoau*autofs), &
              (rhor(k,k),k=1,nsurft),dble(nsurf)
          endif
        endif
      endif
      if (tflag(1).eq.4) then
        write(6,*) 'Final optimized potential energy = ',efinal*autoev
        write(6,*) 'Final optimized geometry: '
        do i=1,natoms
          j = 3*(i-1)
          write(6,1010) symbolm(i),(p(j+k)*autoang,k=1,3)
        enddo
      endif
      if (maxprint) then
        write(6,*)
        write(6,*) 'Lowest energy structure (A):'
        write(6,1050) 'Potential energy (eV):',minpe*autoev
        do i=1,natoms
          write(6,1010) symbolm(i),(xxlow(j,i)*autoang,j=1,3)
        enddo
        write(6,*)
        write(6,*) 'Final structure (A):'
        do i=1,natoms
          write(6,1010) symbolm(i),(xxm(j,i)*autoang,j=1,3)
        enddo
        write(6,*) 'Final momenta (au):'
        do i=1,natoms
          write(6,1010) symbolm(i),mmm(i)/amutoau,(ppm(j,i),j=1,3)
        enddo
        write(6,106) 'Total angular momentum   = ',bigjtot,' au'
      endif

!  RVM
      if (kev .gt. 0) then
! this info is for those steps which 'should' have hopped, but did
! not (due to nonmonotonicity in the probabilities . . .)
      write(6,*) 'Errors in hopping:'
      write(6,*)
      do i = 1,kev
        write(6,2100)idint(speck(1,i)), &
                    (speck(j,i),j=2,5),idint(speck(6,i)),&
                     speck(7,i),speck(8,i)
      enddo
      endif
!  RVM
!
!  Thermal statistic on some properties
!      fpart = 0.d0
!      do i=1,istat
!        if ( reactcol .or. ( (.not.reactcol) .and. (.not.nve) ) ) then
!  For reaction collision, since the initial conditions are all prepared
!  at temp0, the temperature used to stat. should stick to temp0
!           tmp=temp0
!         else
!           tmp=tempmm(i)
!        endif
!        fparti = dexp(-(temm(i)-minte)/kbt0) )
!        fpart  = fpart + fparti
!        fpart  = fpart + dexp(-(temm(i)-minte)/kbt0 )
!      enddo
!  Avoiding numerical errors in adding small values
!      fpart  = fpart*dexp(-(minte-minpe)/kbt0 )
!      fpart = fpart/timestat
!      if (timeup.gt.1.d-5) fpartup = fpartup/timeup
      if (tflag(1).ne.1) then
        ate  = ate/timestat
        ate2 = ate2/timestat
        ste  = dsqrt(max((ate2 - ate**2),0.d0))
        write(6,1050)'Thermal properties at temperature ',atemp-tempzpe, &
                   ' statistic on ',istat,' steps'
        write(6,1050) 'Potential energy minimum (eV):',minpe*autoev
!        write(6,1051) 'Partition function = ',fpart
!        write(6,1051) 'Partition function with TE>=DE0 = ',fpartup
!        write(6,1051) 'N(TE>=E0)/N(all) = ',timeup/timestat
       if (istat.lt.1) then
         write(6,*) 'No enough steps to average on properties'
       else
        write(6,1060) 'energy (eV)','Aver','Var','Rel Var'
        if (cohesive) then
          ace  = ace/timestat
          ace2 = ace2/timestat
          sce  = dsqrt(ace2 - ace**2)
          write(6,1070) 'Cohesive energy (eV)', &
                        ace*autoev,sce*autoev,sce/ace
        endif
        if (kinetic) then
          ake  = ake/timestat
          ake2 = ake2/timestat
          ske  = dsqrt(ake2 - ake**2)
          write(6,1070) 'Kinetic energy (eV)', &
                         ake*autoev,ske*autoev,ske/ake
        endif
        if (potential) then
          ape  = ape/timestat
          ape2 = ape2/timestat
          spe  = dsqrt(ape2 - ape**2)
          write(6,1070) 'Potential energy (eV)', &
                        ape*autoev,spe*autoev,spe/ape
        endif
        write(6,1070) 'Total energy (eV)', &
                      ate*autoev,ste*autoev,ste/ate
        if (heatcap) then
         cv   = (ate2 - ate**2)/(kb*(atemp-tempzpe)**2)
         if (itherm.eq.1) write(6,1050) 'Heat capacity Cv = ', &
                                     cv*autoev,' eV/K'
         if (itherm.eq.2) write(6,1050) 'Heat capacity Cp = ', &
                                     cv*autoev,' eV/K'
        endif
        if (icfrag) then
          kbtp0v=kbtp0v/timestat
          write(6,*) 'Stat on concentration of each fragment'
          write(6,1050) 'Volum of standard state ',kbt0*autoatm,' au'
          write(6,*) 'atoms in frag  ','  concentration'
          do i=1,natoms
            cfrag(i)=cfrag(i)/timestat
            write(6,*) i,cfrag(i)
          enddo
        endif
       endif
      endif
      if (itherm.eq.2) then
        write(6,1050) 'Final volume is ',vol*autoang**3,' A^3'
        write(6,1050) 'Final parameters of the box = '
        write(6,1050) 'a = ',cell(1)*autoang,' A'
        write(6,1050) 'b = ',cell(2)*autoang,' A'
        write(6,1050) 'c = ',cell(3)*autoang,' A'
      endif
      if (reactcol) write(6,*) 'Collision ends: reactant return ',iturn, &
            ' times and stick at least ',ttstick*autofs,' fs', &
            ' and at most ',(time-t0stick)*autofs,' fs; Products ', &
            'ireact,nfrdep,nfrag ',ireact,nfrdep,nfrag, &
             (natinfrag(k),k=1,nfrag)
!
      if (lwrite(42)) call honey(ithistraj,time)
      if (lwrite(20)) then
        write(20,*)ithistraj,pe*autoev
        do i=1,natoms
         write(20,1010) symbolm(i),mmm(i)/amutoau, &
                        (xxm(j,i)*autoang,j=1,3)
        enddo
      endif
      if (lwrite_h5(20)) then
        call hdf5o_write_peratom(20,ithistraj,pe*autoev, xxm(1:3,1:natoms)*autoang)
      end if
      if (lwrite(41)) then
        call radialdist(ithistraj,step,time,nbinrad,rbmax,rbmin,rbin, &
                        raddist,1)
      endif

      if (lwrite(21)) then
        write(21,*)ithistraj,ke*autoev
        do i=1,natoms
          write(21,1010)symbolm(i),mmm(i)/amutoau,(ppm(j,i),j=1,3)
        enddo
      endif
      if (lwrite_h5(21)) then
        call hdf5o_write_peratom(21,ithistraj,ke*autoev, ppm(1:3,1:natoms))
      end if

      if (lwrite_h5(22) .or. lwrite_h5(23)) then
        call hdf5o_trj_end(natoms,ithistraj)
      end if

! fort.27 to only obtain data at the end of each trajectory
          if (lwrite(27)) then
             if (repflag.eq.0) then

!      dvecmod = 0.0d0
!      do i=1,3
!         do j=1,natoms
!            dvecmod = dvecmod + dvec(i,j,1,2)**2
!         enddo
!      enddo
!      dvecmod = dsqrt(dvecmod)

!  Adiabatic
             write(27,777) ithistraj,istep,nsurf,time*autofs, &
                     (rhor(i,i),i=1,nsurft),(pema(i),i=1,nsurft)
!                     ,(vdotd(i,i),i=1,nsurft),vdotd(1,2), dvecmod
             elseif (repflag.eq.1) then
!  Diabatic
             write(27,777) ithistraj,istep,nsurf,time*autofs, &
                     (rhor(i,i),i=1,nsurft),(pemd(i,i),i=1,nsurft)
!                     ,(vdotd(i,i),i=1,nsurft),vdotd(1,2), dvecmod
             endif
          endif

! fort.29 to only obtain data at the end of each trajectory
! Relative energies of the departing H atom in NH3
       if (lwrite(29).and.potflag.eq.3) then

             call relenerg(erel,distnh,nmax)
             linmol = .false.
             call linearbond(linmol,natoms,xxm,dum,nhbond,nhangle)

! Calculation of the nonplanarity angle, nang
             nang = 0.0d0
             do i=1,nmax
                nang = nang + nhangle(i)
             enddo
             nang = 360 - nang

              if (.not.lmodpop) then

              write(29,444)ithistraj,istep,nsurf,nhopsv,time*autofs, &
                   (pema(i)*autoev,i=1,nsurft), &
                   (pemd(i,i)*autoev,i=1,nsurft), &
                   (erel(i)*autoev,i=1,nmax), &
                   (nhbond(i)*autoang,i=1,nmax), &
                   (nhangle(i),i=1,nmax),nang

!              write(29,*) 'minimum gap',egapmin*autoev, &
!              istepmin,nhbond1min,nhbond2min,nhbond3min,nangmin

               else

             if (nmol.eq.1) call popmod(potflag,nmol,natoms,3*natoms-6,xxm,xx0,mmm, &
                                        ppm,nmpop)

              write(29,444)ithistraj,istep,nsurf,nhopsv,time*autofs, &
                   (pema(i)*autoev,i=1,nsurft), &
                   (pemd(i,i)*autoev,i=1,nsurft), &
                   (erel(i)*autoev,i=1,nmax), &
                   (nhbond(i)*autoang,i=1,nmax), &
                   (nhangle(i),i=1,nmax),nang, &
                   (nmpop(j),j=1,3*natoms-6)

              endif

!  Print data to fort29 at minimum of adiabatic gap for trajectories that dissociate adiabatically with no hops

      if(nhop(2).eq.0) then

!  Reorder the data and find egapmin

       do j=2,istep
      if(egapint(1).gt.egapint(j)) then
          inter = egapint(1)
          egapint(1) = egapint(j)
          egapint(j) = inter

          inter = nhbondint(1,1)
          nhbondint(1,1) = nhbondint(1,j)
          nhbondint(1,j) = inter

          inter = nhbondint(2,1)
          nhbondint(2,1) = nhbondint(2,j)
          nhbondint(2,j) = inter

          inter = nhbondint(3,1)
          nhbondint(3,1) = nhbondint(3,j)
          nhbondint(3,j) = inter

          inter = nhangleint(1,1)
          nhangleint(1,1) = nhangleint(1,j)
          nhangleint(1,j) = inter

          inter = nhangleint(2,1)
          nhangleint(2,1) = nhangleint(2,j)
          nhangleint(2,j) = inter

          inter = nhangleint(3,1)
          nhangleint(3,1) = nhangleint(3,j)
          nhangleint(3,j) = inter

          inter = nangint(1)
          nangint(1) = nangint(j)
          nangint(j) = inter

          inter = nnstep(1)
          nnstep(1) = nnstep(j)
          nnstep(j) = inter

          inter = erelint(1,1)
          erelint(1,1) = erelint(1,j)
          erelint(1,j) = inter

          inter = erelint(2,1)
          erelint(2,1) = erelint(2,j)
          erelint(2,j) = inter

          inter = erelint(3,1)
          erelint(3,1) = erelint(3,j)
          erelint(3,j) = inter

      endif
      enddo

       egapmin = egapint(1)
       nhbond1min = nhbondint(1,1)
       nhbond2min = nhbondint(2,1)
       nhbond3min = nhbondint(3,1)
       nangmin = nangint(1)
       nstepmin = nnstep(1)

      write(32,*) egapmin,nstepmin,nhbond1min,nhbond2min,nhbond3min, &
       nhangleint(1,1),nhangleint(2,1),nhangleint(3,1), &
       nangmin,erelint(1,1),erelint(2,1),erelint(3,1)

      endif

      endif

      if (methflag.eq.3.or.methflag.eq.4) then
!       write(6,'(a,f10.2,a)')'Inverse of the time average of 1/tau ',
        if (atau.eq.0d0.or.tautim.eq.0d0) then
           tmp = 0d0
        else
           tmp = (1.d0/(atau/tautim))*autofs
        endif
        write(6,'(2a,es13.5,a)')'Averaged decoherence time to ', &
         'target state',tmp,' fs'
      endif
!  All formats go here
 1    format(1x,2a12,20a16)
 2    format(1x,a12,10a12)
 101  format(1x,2f12.2,20g16.8)
 106  format(1x,a,f20.8,1x,a)
 1010 format(1x,a4,4f15.7)
 1011 format(1x,i6,a4,4f15.7)
 1050 format(1x,a35,f20.8,a,i10,a)
 1051 format(1x,a35,e20.8)
 1060 format(1x,a21,3a17)
 1070 format(1x,a21,"=",3f17.8)
 107  format(1x,a5,a10,a12,a10,16a12)
 108  format(1x,i5,i10,f12.2,f10.2,3E12.5,2f12.4,12E12.5)
 121  format(1x,i10,f20.10,i12,f14.2,f10.1)
 111  format(1x,i5,i10,f12.2,1000f12.5)
 222  format(1x,2a,e16.8,a)
 333  format(1x,a,i10,a,e16.8,a,e16.8,a)
 444  format(1x,i7,i10,2i5,100e16.8)
 555  format(1x,a,e16.8,a)
 666  format(1x,a,3d12.4)
 777  format(1x,i5,i10,i5,100d16.8)
!   RVM
 2100 format (i5,4e14.6,i4,f11.2,e17.9)

      end subroutine driver
