      subroutine getrho(cre,cim,rhor,rhoi,nsurft)

! Compute the density matrix from the electronic coefficients.
! Note:  Phases are handled separately, and RHOR and RHOI do
! not include phases.  These may need to be added to RHOR and RHOI,
! depending on how these quantities are used.

      use param, only: mnsurf
      implicit none

      integer :: nsurft,i,j
      double precision :: cim(2*mnsurf),cre(2*mnsurf), &
                          rhor(mnsurf,mnsurf),rhoi(mnsurf,mnsurf)

      do i=1,nsurft
        do j=1,nsurft
          rhor(i,j) = cre(i)*cre(j)+cim(i)*cim(j)
          rhoi(i,j) = cre(i)*cim(j)-cre(j)*cim(i)
        enddo
      enddo

      end subroutine getrho
