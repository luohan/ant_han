      subroutine derivs(x,yn,yout,nv,im)

! This subroutine (along with XPTOY) puts together a single array 
! and its derivatives for integration.  It is called by the
! integrators whenever they need derivative information.  YN
! is the array of variables to be integrated, and YOUT contains the derivatives.
! iflinear: 0: atom; 1: linear; 2: nonlinear

      use param, only: mnyarray

      implicit none
      integer nv,im, i
      double precision yn(mnyarray),yout(mnyarray),x,dmag
 
!     get xxm from yn
      call xptoy(yn,yout,nv,1)
!     get gradients
      call getgrad(dmag,im)
 
!     transform to 1-D
      call xptoy(yn,yout,nv,0)

      end subroutine derivs
