subroutine  getevibanh(v0,vtot,xx,xeq,pp,mass,natom,nmvec,freq,nd,time)
! calculate vibrational energy including anharmonicity
! see J. C. Corchado and J. Espinosa-Garcia, PCCP, 2009, 11, 10157-10164
use param, only: mnat, autocmi, autofs, mu
implicit none
integer :: nd,natom
integer :: i, j, k, l,im
real(8) :: evib(nd),nvib(nd),evibho(nd),nvibho(nd),xx(3,natom),pp(3,natom)
real(8) :: evkin(nd),evpotho(nd),evpota(nd)
real(8) :: xeq(3,natom),mass(natom),xsave(3,natom),psave(3,natom),xex(3,natom)
real(8) :: xrel(3,natom),dxnm(nd)
real(8) :: nmvec(3,mnat,3*mnat),freq(3*mnat)
real(8) :: dx,eex,erel,v0,sn,pe,time

real(8) :: pmi(3),imomeq(3,3),imom(3,3),vtemp(3),xptmp(3,natom),vel(3,natom)
real(8) :: w(natom),q(0:3),urot(3,3)

real(8) :: totmass,comv(3),angm(3),angv(3),etr,erot,evibt,evibt1,evibtanh,etot,vtot,kintot
real(8) :: evibtho,ecor(nd)
real(8) :: vtmp1(3),vtmp2(3)

! write normal mode vector
! do i = 1, natom
!    write(6,'(9f8.2)') nmvec(:,i,1),nmvec(:,i,2),nmvec(:,i,3)
! enddo
! do i = 1, 3*natom
!    write(6,'(a,3f8.2)') 'Freq',freq(i)*autocmi
! enddo
! stop

 xsave = xx
 psave = pp
 w(:) = 1.d0
! move origin to center of mass for product geometry
 call center(xx,mass,natom)
 call qtrfit(natom,xx,xeq,w,q,urot)

!rotate molecule
 call rotmol(natom,xx,xptmp,urot)
 xx = xptmp
 call rotmol(natom,pp,xptmp,urot)
 pp = xptmp
!write(35,*) 'Urot '
!do i = 1, 3
!   write(35,*) urot(:,i)
!enddo
!
!remove center of mass motion
 call calckin(natom,pp,mass,kintot)
 call removecomp(natom,pp,mass,comv,totmass)
! translational energy
 etr = 0d0
 do i = 1, 3
  etr = etr + totmass*comv(i)**2
 enddo
 etr = 0.5d0*etr
! calculate total angular momentum and angular velocity
 call angmom1(natom,xx,pp,mass,angm,angv)
! calculate rotational kinetic energy
 call calcerot(natom,xx,pp,mass,angv,erot)
! remove angular velocity 
 call removeang(natom,xx,pp,mass,angv)

! calculate vibrational kinetic energy
 call calckin(natom,pp,mass,evibt1)
! transfer pp to velocity
 do i = 1, natom
   vel(:,i) = pp(:,i)/mass(i)
 enddo

!etot     = 0d0
 etot     = vtot + kintot - v0
!write(35,*) 'kintot  vtot  vtot-v0', kintot*autocmi,vtot*autocmi,(vtot-v0)*autocmi
 evibt    = etot - erot - etr
 evibt1   = evibt1 + vtot - v0
 evibtanh = 0d0
 evibtho  = 0d0

 evib(:) = 0d0
 evibho(:) = 0d0
 nvib(:) = 0d0
 dxnm(:) = 0d0

do im = 1, nd  ! loop over vibrational mode

! calculate HO kinetic energy
   do i = 1, natom
   do j = 1, 3
!     evib(im)   = evib(im) + (nmvec(j,i,im)*vel(j,i))**2*mass(i)
!     evib(im)   = evib(im) + (nmvec(j,i,im)*vel(j,i)*dsqrt(mu/mass(i)))**2*mass(i)
      evib(im)   = evib(im) + nmvec(j,i,im)*vel(j,i)/dsqrt(mu/mass(i))
!     evib(im)   = evib(im) + nmvec(j,i,im)*vel(j,i)
!     evibho(im) = evibho(im) + (nmvec(j,i,im)*vel(j,i))**2*mass(i)
      dxnm(im)   = dxnm(im) + (xx(j,i)-xeq(j,i))*nmvec(j,i,im) /dsqrt(mu/mass(i))
!     dxnm(im)   = dxnm(im) + (xx(j,i)-xeq(j,i)) * nmvec(j,i,im) 
   enddo
   enddo
!  evib(im)   = 0.5d0 * evib(im)
   evib(im)   = 0.5d0 * evib(im)**2*mu
!  evibho(im) = 0.5d0 * evibho(im)
   evibho(im) = evib(im)
   evkin(im) = evibho(im)
!  write(35,*) 'Kinetic E ', evkin(im)*autocmi

   evpotho(im) = 0.5d0*(freq(im)*dxnm(im))**2 *mu
   evibho(im) = evibho(im) +  evpotho(im)
   evibtho    = evibtho + evibho(im)
enddo ! end loop vibrational mode


!write(35,'(10f10.2)') etot*autocmi,etr*autocmi,erot*autocmi,evibt*autocmi,evibt1*autocmi,evibtanh*autocmi,evibtho*autocmi
!write(35,'(10f10.2)') etot*autocmi,etr*autocmi,erot*autocmi,evibt*autocmi,evibtanh*autocmi,evibtho*autocmi
!write(35,'(5f12.2)') time*autofs,evib(1)*autocmi,evibho(1)*autocmi,evkin(1)*autocmi,evpot(1)*autocmi
!write(35,'(8f12.2)') time*autofs,evibho(1)*autocmi,evkin(1)*autocmi,evpotho(1)*autocmi, &
!                     evib(1)*autocmi,evkin(1)*autocmi,evpota(1)*autocmi
! write(35,'(34f12.2)') time*autofs,evibho(:)*autocmi
  evibho(:) = evibho(:)*autocmi
! write(35,'(34f12.2)') time*autofs,evibho(1),evkin(1)*autocmi,evpotho(1)*autocmi,evibho(2),evkin(2)*autocmi,evpotho(2)*autocmi,&
!  evibho(3),evkin(3)*autocmi,evpotho(3)*autocmi
! evib(:) = evib(:)*autocmi
  write(35,'(34f12.3)') time*autofs,evibho(1),evkin(1)*autocmi,evpotho(1)*autocmi,evibtho*autocmi,&
                        evibtho*autocmi-evibho(1), etot*autocmi,kintot*autocmi,(vtot-v0)*autocmi
!write(35,'(34f12.2)') time*autofs,evibho(1),evibho(9),evibho(11),evibho(15),evibho(16),evibho(20),evibho(31),evibtho*autocmi,&
!                       etot*autocmi
!stop
  xx = xsave
  pp = psave
return
end subroutine getevibanh

! Taken from MSTor written by Steven L. Mielke
subroutine make_imom(wcoor,natoms, masses, imom, pmi)
implicit none
integer :: natoms, i, info
!double precision :: wcoor(s,3), masses(natoms), imom(3,3), pmi(3), work(1000), vtemp(3)
double precision :: wcoor(3,natoms), masses(natoms), imom(3,3), pmi(3), work(1000), vtemp(3)
!
! Calculates the rotational moment of inertia matrix and diagonalizes
!
imom(:,:)=0d0

do i=1,natoms
! imom(1,1)= imom(1,1) + masses(i)*(wcoor(i,2)**2+wcoor(i,3)**2)
! imom(2,2)= imom(2,2) + masses(i)*(wcoor(i,1)**2+wcoor(i,3)**2)
! imom(3,3)= imom(3,3) + masses(i)*(wcoor(i,1)**2+wcoor(i,2)**2)
! imom(1,2)= imom(1,2) - masses(i)*wcoor(i,1)*wcoor(i,2)
! imom(1,3)= imom(1,3) - masses(i)*wcoor(i,1)*wcoor(i,3)
! imom(2,3)= imom(2,3) - masses(i)*wcoor(i,2)*wcoor(i,3)
  imom(1,1)= imom(1,1) + masses(i)*(wcoor(2,i)**2+wcoor(3,i)**2)
  imom(2,2)= imom(2,2) + masses(i)*(wcoor(1,i)**2+wcoor(3,i)**2)
  imom(3,3)= imom(3,3) + masses(i)*(wcoor(1,i)**2+wcoor(2,i)**2)
  imom(1,2)= imom(1,2) - masses(i)*wcoor(1,i)*wcoor(2,i)
  imom(1,3)= imom(1,3) - masses(i)*wcoor(1,i)*wcoor(3,i)
  imom(2,3)= imom(2,3) - masses(i)*wcoor(2,i)*wcoor(3,i)
enddo

imom(2,1)=imom(1,2)
imom(3,1)=imom(1,3)
imom(3,2)=imom(2,3)

! calculate the Principal axes
call dsyev('V', 'U',3,imom,3,pmi,work,1000,info)
if (info .ne.0)then
  write(6,*)'Error in imom, info=', info
  stop
endif

! Now make sure we have a properly handed frame
 call acrossb(imom(:,1), imom(:,2), vtemp)
 if(dot_product(vtemp, imom(:,3)) .lt. 0d0)then
   imom(:,3)=-imom(:,3)
 endif

end subroutine make_imom

subroutine rot2paxis(x,natom,imom)
! Orient the molecule to their principal axes
integer :: natom,i,j
real(8) :: x(3,natom),x1(3,natom),imom(3,3)

x1 = 0d0
do i = 1, natom
  do j = 1, 3  
  x1(j,i) = dot_product(x(:,i),imom(:,j)) 
  enddo
enddo

x = x1
return
end subroutine rot2paxis
subroutine calckin(natom,p,mass,kintot)
! calculate kinetic energy
implicit none
integer :: i, j, natom
real(8) :: p(3,natom),mass(natom),kintot

kintot = 0d0
do i = 1, natom
  do j = 1, 3
    kintot = kintot + p(j,i)**2/mass(i) 
  enddo
enddo
kintot = 0.5d0 * kintot

return
end subroutine calckin
subroutine removecomp(natom,p,mass,comv,totmass)
! remove center of mass momentum 
implicit none
integer :: natom,i, j
real(8) :: p(3,natom),v(3,natom),mass(natom),comp(3),comv(3)
real(8) :: totmass
 
comp(:) = 0d0
totmass = 0d0
do i = 1, natom
  totmass = totmass + mass(i)
  comp(:) = comp(:) + p(:,i)
  v(:,i) =  p(:,i)/mass(i)
enddo

! center of mass velocity
  comv(:) = comp(:)/totmass

do i = 1, natom
  v(:,i) = v(:,i) - comv(:)
  p(:,i) = v(:,i)*mass(i) 
enddo
return
end subroutine removecomp
subroutine calcerot(natom,x,p,mass,angv,erot)
! calculate rotational kinetic energy
implicit none
integer :: natom,i,j
real(8) :: x(3,natom),p(3,natom),mass(natom),angv(3),erot
real(8) :: imom(3,3),pmi(3)

! calculate principle moments of inertia -- pmi
call make_imom(x, natom, mass, imom, pmi)

erot = 0d0
do i = 1 ,3
 erot = erot + pmi(i) * angv(i)**2
enddo
erot = 0.5d0*erot
return
end subroutine calcerot

subroutine  removeang(natom,x,p,mass,omeg)
! remove angular velocity from the momenta
implicit none
integer :: i,natom
real(8) :: p(3,natom),x(3,natom),mass(natom),omeg(3)

 do i=1,natom
   p(1,i)=p(1,i)+mass(i)*(x(2,i)*omeg(3)-x(3,i)*omeg(2))
   p(2,i)=p(2,i)-mass(i)*(x(1,i)*omeg(3)-x(3,i)*omeg(1))
   p(3,i)=p(3,i)+mass(i)*(x(1,i)*omeg(2)-x(2,i)*omeg(1))
 enddo
return
end subroutine removeang

subroutine angmom1(natom,x,p,mass,angm,angv)
! calculate angular momentum and angular velocity
implicit none
integer :: i, j, k, natom
real(8) :: x(3,natom),p(3,natom),mass(natom),angm(3),angv(3)
real(8) :: imom(3,3),pmi(3)

integer :: info,lwork,indx(3)
real(8), allocatable :: work(:)


angm = 0d0
angv = 0d0

! angular momentunm -- x cross p
do i=1,natom
 angm(1)=angm(1)+(x(2,i)*p(3,i)-x(3,i)*p(2,i))
 angm(2)=angm(2)-(x(1,i)*p(3,i)-x(3,i)*p(1,i))
 angm(3)=angm(3)+(x(1,i)*p(2,i)-x(2,i)*p(1,i))
enddo

! calculate principle moments of inertia -- pmi
 call make_imom(x, natom, mass, imom, pmi)

! calculate moment of inertia
!do i=1,natom
!  imom(1,1)= imom(1,1) + mass(i)*(x(2,i)**2+x(3,i)**2)
!  imom(2,2)= imom(2,2) + mass(i)*(x(1,i)**2+x(3,i)**2)
!  imom(3,3)= imom(3,3) + mass(i)*(x(1,i)**2+x(2,i)**2)
!  imom(1,2)= imom(1,2) - mass(i)*x(1,i)*x(2,i)
!  imom(1,3)= imom(1,3) - mass(i)*x(1,i)*x(3,i)
!  imom(2,3)= imom(2,3) - mass(i)*x(2,i)*x(3,i)
!enddo

! calculate inverse of moment of inertia
 call dgetrf(3,3,imom,3,indx,info)
 if (info.ne.0 ) then
    write(6,*) "dgetrf exit abnormally"
    stop
 endif
 lwork = -1
 allocate(work(1))
 call dgetri(3,imom,3,indx,work,lwork,info)
 lwork=int(work(1))
 deallocate (work)
 allocate( work(lwork) )
 call dgetri(3,imom,3,indx,work,lwork,info)
 deallocate(work)
 if (info.ne.0 ) then
    write(6,*) "dgetri exit abnormally"
    stop
 endif

! angular velocity = inverse of MOI matrix * angular momentum
!do i = 1, 3
!  do j = 1, 3
!    angv(i) = angv(i) + imom(i,j)*angm(j)
!  enddo
!enddo
  angv(:) = angm(:)/pmi(:)

return
end subroutine angmom1
