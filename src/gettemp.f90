      subroutine gettemp(im)
!
! Compute the temperature and kinetic energy from the momentum.
!     1) isolated system, Nc=6 or 5 (2 atom dynamics)
!     2) periodic system, Nc=3
! im:  mnmol+1: for the whole run
!      others : for an AG only
! periodic: implement in the future. 1: periodic systems, 0:
!            nonperiodic systems
! imethod: For AGs can use normal mode analysis, the energy rising from
!          E0 (Ee+ZPE) due to temperature is: Etot - E0 =  Etot-(Ee+ZPE)
!          = vibfreedom * kT + other contributions (translation and
!          rotation if any (ignoring electronic contribution) )
      use param, only: kb
      use c_struct, only: natoms, mmm, infree
      use c_traj, only: temp, ppm, ke

      implicit none
      integer :: im
! local
      integer :: i
!
!      if (idebug) write(6,*)"Number of freedom = ",infree
      ke=0.d0
      do i=1,natoms
        ke = ke+( ppm(1,i)*ppm(1,i) + ppm(2,i)*ppm(2,i) + &
                  ppm(3,i)*ppm(3,i) ) /mmm(i)
      enddo
      temp = ke/(kb*dble(infree))
      ke = 0.5d0*ke
 
      end subroutine gettemp
