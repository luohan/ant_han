      function func(p,n,nmax)

!     to get the energy for the use of geometry optimization subroutine
! p:     one dimensional array of length n
! nmax : maximum length of p

      use c_struct, only: symbolm, indmm
      use c_sys, only: potflag
      implicit none
      integer n,nmax,nclu

!     input
      double precision xx(3,n/3),p(n)

      integer i,ii
      double precision func

      double precision v


! local (for HO-MM-1 and HE-MM-1 interfaces)
      double precision :: dx(n/3),dy(n/3),dz(n/3), &
                          x(n/3),y(n/3),z(n/3)
      integer inds(n/3)
      character*2 symbs(n/3)

      nclu=n/3
      do i=1,nclu
        ii = 3*i
        x(i) = p(ii-2)
        y(i) = p(ii-1)
        z(i) = p(ii)
        symbs(i) = symbolm(i)
        inds(i)  = indmm(i)
      enddo

! ######################################################################
!     POTLIB HO-MM-1 interface
      if (potflag.eq.0) then
! ######################################################################
!     Single surface

        call pot(x,y,z,v,dx,dy,dz,nclu,nclu)

! ######################################################################
!     POTLIB HE-MM-1 interface
      elseif (potflag.eq.2) then
! ######################################################################
!     Single surface

        call pot(symbs,x,y,z,v,dx,dy,dz,nclu,nclu)

! ######################################################################
!     POTLIB HE-MM-1' interface transfer atomic numbers instead
      elseif (potflag.eq.5) then
! ######################################################################
        call pot(inds,x,y,z,v,dx,dy,dz,nclu,nclu)

      else
        write(6,*) 'Not implemented yet for POTFLAG=',potflag
        stop

! ######################################################################
      endif
! ######################################################################

      func = v
      return

      end

      subroutine dfunc(p,v,g,n,nmax)

!     This subroutine handles all potential calls.
!     to get the energy for the use of geometry optimization subroutine
! p:     one dimensional array of length n (coordinate)
! g:     one dimensional array of length n (gradient)
! nmax : maximum length of p and g

      use c_struct, only: symbolm, indmm
      use c_sys, only: potflag

      implicit none
      integer n,nmax,nclu
      integer i,ii
      double precision xx(3,n/3),p(n),g(n)
      double precision func, v

! local (for HO-MM-1 and HE-MM-1 interfaces)
      double precision :: dx(n/3),dy(n/3),dz(n/3),  &
                          x(n/3),y(n/3),z(n/3)
      integer inds(n/3)
      character*2 symbs(n/3)

      nclu=n/3
      do i=1,nclu
        ii = 3*i
        x(i) = p(ii-2)
        y(i) = p(ii-1)
        z(i) = p(ii)
        symbs(i) = symbolm(i)
        inds(i)  = indmm(i)
      enddo

! ######################################################################
!     POTLIB HO-MM-1 interface
      if (potflag.eq.0) then
! ######################################################################
!     Single surface

      call pot(x,y,z,v,dx,dy,dz,nclu,nclu)

! ######################################################################
!     POTLIB HE-MM-1 interface
      elseif (potflag.eq.2) then
! ######################################################################
!     Single surface

      call pot(symbs,x,y,z,v,dx,dy,dz,nclu,nclu)

! ######################################################################
!     POTLIB HE-MM-1' interface transfer atomic numbers instead
      elseif (potflag.eq.5) then
! ######################################################################
      call pot(indmm,x,y,z,v,dx,dy,dz,nclu,nclu)

      else
        write(6,*) 'Not implemented yet for POTFLAG=',potflag
        stop
      
! ######################################################################
      endif
! ######################################################################

      do i=1,nclu
        ii = 3*i
        g(ii-2) = dx(i)
        g(ii-1) = dy(i)
        g(ii)   = dz(i)
      enddo

      end subroutine dfunc
