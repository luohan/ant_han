      subroutine popnorm(im)
!
! Populate the normal modes using information precalculated in
! NORMOD.  This subroutine selects initial conditions when
! INITx = 2.
! signs:    random signs
! ranm:     random numbers
! ivib:     ith vibmods
! natoms:   natom(im)
! evibi:    vibrational energy of ith mode
! rturn(3*natoms)   Turning point of a vibrational mode

      use param, only: mnat, autoev, mu, pi
      use c_output, only: minprinticon, idebug
      use c_struct, only: nvibmods, xxm, imulti, icharge, mmm, cell,  &
                          symbolm, indmm, natoms
      use c_sys, only: repflag, potflag
      use c_initial, only: ivibdist, vibdistn, freq, qnmvib0, nvinit, &
                           ewell, vibsel, vibene, initp, nmvec, ivibtype
      use c_traj, only: ppm, nsurf, dvec, gpema, gpemd, pema, pemd,  &
                        evib, ralpha, lmodpop, pe, ke

      implicit none
      integer :: im
      integer :: i,j,k,ivib,nlim,nv,ivd
      double precision :: signs(mnat*3),evibi,     &
                          xxnm(3,mnat),vvnm(3,mnat),kinho(3*mnat),potho,potho0,  &
                          disp(3*mnat),rturn(3*mnat),tmp,thermalvib,ranm,ran1,ran2
!
! sigma of the gaussian distribution of the coordinates
      double precision :: sigmax
! sigma of the gaussian distribution of the momentum
      double precision :: sigmap
      double precision :: r1,r2,x1,x2,w
      double precision :: rangauss
! temporary storage for coordinates
      double precision :: xxt(3,mnat),xxt0(3,mnat)
! array for generating nvibmod(im) random number and randomize the order of vibrational modes
      double precision, allocatable :: ranv(:)
      logical, allocatable :: mask(:)
      integer :: loc(1)

      disp = 0.0d0
      rturn = 0.0d0
      if(.not.allocated(ranv) )   &
            allocate(ranv(nvibmods(im)),mask(nvibmods(im)))
      mask(:) = .true.
!
! generate two sets of 3*N-6 / 3*N-5 random numbers
!
! compute HO turning points for each mode
      if(.not.minprinticon) then
        write(6,*) 'Turning point info'
        write(6,111) '     Mode     Q. Number  Energy (eV) ',  &
          ' Distance (mass-scaled bohr)'
      endif
      evib(im)=0.d0
      thermalvib = 0.0d0
      ivd = ivibdist(im)
      xxt(:,:) = xxm(:,:)
      !
      ! generate nvibmods(im) random numbers
      !
      do i=1,nvibmods(im)
        call get_rng(ranv(i))
      enddo
      !
      !     do i=1,nvibmods(im)
      do nv=1,nvibmods(im)
        ! put vibrational modes in a random order for each trajectory
        loc = maxloc(ranv,mask)
        i = loc(1)
        mask(i) = .false.

        if(ivd.eq.9) ivd = vibdistn(i,im)

        call get_rng(signs(i))
        if (signs(i).ge.0.5d0) then
          signs(i) = 1.d0
        else
          signs(i) = -1.d0
        endif


        if (potflag.eq.3.and..not.lmodpop) then

          if (i.eq.6) then
            ! We correct the value of the umbrella mode to get close to experiments
            evibi = 892.0d0/977.5069d0*freq(i,im)*(0.5d0 +  &
              dble(qnmvib0(i,im)))
            !           elseif (i.eq.1.or.i.eq.2) then
            ! Asymmetric modes are degenerated and we consider 1/2 quanta in each mode
            ! rather than 1 quantum in one of this mode  and 0 in the other
            !             evibi = 0.5d0*freq(i,im)*(1.0d0+dble(qnmvib0(i,im)))
          else
            evibi = freq(i,im)*(0.5d0+dble(qnmvib0(i,im)))
          endif

        elseif (potflag.ne.3.and..not.lmodpop) then

          evibi = freq(i,im)*(0.5d0+dble(qnmvib0(i,im)))

        elseif (potflag.eq.3.and.lmodpop) then

          if (i.eq.6) then
            ! We correct the value of the umbrella mode to get close to experiments
            evibi = 892.0d0/977.5069d0/ralpha*freq(i,im)*(0.5d0 +  &
              dble(qnmvib0(i,im)))
            !           elseif (i.eq.1.or.i.eq.2) then
            ! Asymmetric modes are degenerated and we consider 1/2 quanta in each mode
            ! rather than 1 quantum in one of this mode  and 0 in the other
            !              evibi = freq(i,im)/ralpha*(1.0d0+dble(qnmvib0(i,im)))
          else
            evibi = freq(i,im)/ralpha*(0.5d0+dble(qnmvib0(i,im)))
          endif
          !
          ! General case using HO approximation
          !
        elseif (potflag.ne.3.and.lmodpop) then

          evibi = freq(i,im)/ralpha*(0.5d0+dble(qnmvib0(i,im)))

        endif

        ! J. Zheng : set fixed energy for each mode
        if(vibsel(im).eq.4.or.vibsel(im).eq.5) then
          evibi = vibene(i,im)
        elseif(vibsel(im).eq.6.or.vibsel(im).eq.7) then
          evibi = dmin1(vibene(i,im),evibi)
        endif

        ! J. Zheng : calculate initial vibrational quantum number that can be a fraction number larger than -0.5
        nvinit(i,im) = evibi*ralpha/freq(i,im) - 0.5d0

        evib(im) = evib(im) + evibi
        thermalvib = thermalvib+freq(i,im)*dble(qnmvib0(i,im))
        rturn(i) = dsqrt(2.d0*evibi/mu)/freq(i,im)
        if(.not.minprinticon) then
          write(6,300) i,qnmvib0(i,im),evibi*autoev,rturn(i)
        endif
        kinho(i)=-1.d0
        !       do while(kinho(i).lt.0.d0)
        do while(kinho(i).lt.-1D-10)  ! always make sure kinhol(i) > 0
          call get_rng(ranm)
          ! VIBDIST = 0
          if (qnmvib0(i,im).ne.0.or.ivd.eq.0) then
            ! NAT: uniform distribution of phase between 0-2PI
            disp(i) = rturn(i)*dcos(2.d0*pi*ranm)
            ! VIBDIST = 1
          elseif (ivd.eq.1) then
            ran2 = 0.d0
            do while(ran2.eq.0.d0)
              call get_rng(ran2)
            enddo
            ! sigmax= dsqrt(1.d0/(2.d0*mu*freq(i,im)))
            !next line is equivalent to the above line and is more general
            sigmax= dsqrt(evibi/mu)/freq(i,im)
            rangauss = dsqrt(-2.d0*dlog(ran2))*dcos(2.d0*pi*ranm)
            disp(i) = sigmax*rangauss

            ! VIBDIST = 2
          elseif (ivd.eq.2) then
            ran2 = 0.d0
            do while(ran2.eq.0.d0)
              call get_rng(ran2)
            enddo
            ! sigmax= dsqrt(1.d0/(2.d0*mu*freq(i,im)))
            ! next line is equivalent to the above line and is more general
            sigmax= dsqrt(evibi/mu)/freq(i,im)
            sigmap= 1.d0/(2.d0*sigmax)
            rangauss = dsqrt(-2.d0*dlog(ran2))
            ! Gaussian distribution of x, with sigma=sqrt(1/(2*mu*v))
            disp(i)  = sigmax*rangauss*dcos(2.d0*pi*ranm)
            ! Gaussian distribution of p, with sigmp=1/(2*sigmax)
            kinho(i) = sigmap*rangauss*dsin(2.d0*pi*ranm)
            kinho(i) = 0.5d0*kinho(i)**2/mu
          endif
          if (initp(im).eq.1) then  !for initp ==1, always start with maximum stretch???
            disp(i)=rturn(i)
            kinho(i)=0.d0
          endif
          ! compute HO estimate of potential
          if (qnmvib0(i,im).ne.0.or.ivd.ne.2) then
            ! calculate actual potential for normal mode i
            do while(kinho(i) .lt. -1d-10)
              xxnm(:,:) = 0d0
              do j = 1, 3
                do k = 1, natoms
                  ! project displacement onto normal mode vector (mass-scaled)
                  xxnm(j,k) = xxnm(j,k) + nmvec(j,k,i,im)*disp(i)
                enddo
              enddo
              xxt0(:,:) = xxt(:,:)
              call getpem(xxt0,indmm,symbolm,cell,natoms,pema,pemd,gpema, &
                gpemd,dvec,mmm,1,nsurf,icharge(im),imulti(im))
              if (repflag.eq.1) then
                potho0 = pemd(nsurf,nsurf)
              else
                potho0 = pema(nsurf)
              endif
              do j = 1, natoms
                tmp = dsqrt(mu/mmm(j))
                xxt(:,j) = xxt(:,j) + xxnm(:,j)*tmp
              enddo
              call getpem(xxt,indmm,symbolm,cell,natoms,pema,pemd,gpema, &
                gpemd,dvec,mmm,1,nsurf,icharge(im),imulti(im))
              if (repflag.eq.1) then
                potho = pemd(nsurf,nsurf)
              else
                potho = pema(nsurf)
              endif
              !1/2 kr**2 = 1/2 v**2 mu r**2
              ! potho = 0.5d0*freq(i,im)**2*mu*disp(i)**2
              ! the available kinetic energy (in the HO approx) is therefore
              kinho(i) = evibi + potho0 -potho
              ! write(6,*) "kinho", kinho(i)
              if(kinho(i) .lt. 0d0) then
                !disp(i)=disp(i)*0.9
                xxt(:,:) = xxt0(:,:)
                kinho(i) = -100.0d0 !this loop fails, regenrate a phase
                exit
              endif
            enddo  ! end kinho(i) > 0
          endif
        enddo  ! end do while

      enddo  ! end all vibrational modes
      !
      if(.not.minprinticon) then
        write(6,400) 'HO approximation to the internal energy = ',  &
          evib(im)*autoev,' eV'
        write(6,400) 'Thermal vibrational energy              = ',  &
          thermalvib*autoev, ' eV'
        if(ivibtype(im).eq.0) then
          write(6,400) 'HO approximation to the total energy    = ',  &
            (ewell(im)+evib(im))*autoev,' eV'
        elseif(ivibtype(im).eq.1) then
          nlim = nvibmods(im)+1
          kinho(nlim) = dabs(qnmvib0(nlim,im))
          write(6,400) 'HO approximation to the total energy    = ', &
            (ewell(im)+evib(im)+kinho(nlim))*autoev,' eV'
        endif
      endif
      !
      ! initialize displacement and velocity
      do j=1,natoms
        xxnm(1,j) = 0.d0
        xxnm(2,j) = 0.d0
        xxnm(3,j) = 0.d0
        vvnm(1,j) = 0.d0
        vvnm(2,j) = 0.d0
        vvnm(3,j) = 0.d0
      enddo

      ! populate all modes as indicated
      ! NOTE: xxnm, rturn are mass-scaled
      ! freq is independent of mass-scaling
      do i=1,3
        do j=1,natoms
          do ivib=1,nvibmods(im)
            ! project displacement onto normal mode vector (mass-scaled)
            ! sum displacements for all modes
            xxnm(i,j) = xxnm(i,j) + nmvec(i,j,ivib,im)*disp(ivib)
            ! assign velocity based on this energy
            vvnm(i,j) = vvnm(i,j) &
              + signs(ivib)*nmvec(i,j,ivib,im)*dsqrt(2.d0*kinho(ivib)/mu)
          enddo
        enddo
      enddo

      if(idebug) then
        write(6,500) 'Disp = ',(disp(i),i=1,nvibmods(im))
        write(6,500) 'Rturn = ',(rturn(i),i=1,nvibmods(im))
        write(6,500) 'Momenta = ',(signs(i)*dsqrt(2.0d0*kinho(i)/mu),i=1,nvibmods(im))
      endif

      if (ivibtype(im).eq.1) then
        nlim = nvibmods(im)+1
        kinho(nlim) = dabs(qnmvib0(nlim,im))
        tmp = 1.d0
        if (qnmvib0(nlim,im).lt.0.d0) tmp=-1.d0
        do i=1,3
          do j=1,natoms
            vvnm(i,j) = vvnm(i,j) + tmp*nmvec(i,j,nlim,im)*dsqrt(2.d0*kinho(nlim)/mu)
          enddo
        enddo
      endif

      ! un-scale and transform velocity to momentum
      do j=1,natoms
        tmp = dsqrt(mu/mmm(j))
        xxm(1,j) = xxm(1,j) + xxnm(1,j)*tmp
        xxm(2,j) = xxm(2,j) + xxnm(2,j)*tmp
        xxm(3,j) = xxm(3,j) + xxnm(3,j)*tmp
        ppm(1,j) = ppm(1,j) + vvnm(1,j)*tmp*mmm(j)
        ppm(2,j) = ppm(2,j) + vvnm(2,j)*tmp*mmm(j)
        ppm(3,j) = ppm(3,j) + vvnm(3,j)*tmp*mmm(j)
      enddo

      if (idebug)then
        call getpem(xxm,indmm,symbolm,cell,natoms,pema,pemd,gpema, &
                gpemd,dvec,mmm,1,nsurf,icharge(im),imulti(im))
        if (repflag.eq.1) then
          pe = pemd(nsurf,nsurf)
        else
          pe = pema(nsurf)
        endif
        call gettemp(im)
        write(6,400) 'Accurate Hamitonian energy    = ', &
            (pe+ke)*autoev,' eV'
      endif


      !C For debug information. Calculate Ke generated. Ke should be >=0 and <= evib
      !     tmp=0.d0
      !     do i=1,3
      !       do j=1,natoms
      !         tmp = tmp + vvnm(i,j)**2*mu/2.d0
      !       enddo
      !     enddo
      !     print *,'Kinetic energy after popnorm = ',tmp*autoev,' eV'
      !     do i=1,natoms
      !        print *,'Position of atom ',i,' = ',(xxm(j,i),j=1,3)
      !        print *,'Momentum of atom ',i,' = ',(ppm(j,i),j=1,3)
      !     enddo
      if(.not.minprinticon) then
        write(6,*)
      endif
      ! Formats
    300  format(1x,i10,3f12.5)
    400  format(1x,a50,f20.8,a)
    111  format(1x,2a)
    500  format(1x,a10,4f20.8) 
!
      end subroutine popnorm
