      subroutine preatomdiatom(escat,ecoli,einti,temp0,&
                               ji,vi,arri,mm,rin,rout,  &
                               tau,phasead,bmax,bmin,ppreli,nsurf, &
                               nsurft,easym,rasym,bparam,        &
                               adbmode,ademod,phmode,evibi)
!xxx     &  tau,bmax,bmin,ppreli,ecoli,nsurf,nsurft,easym,rasym,bparam)

! Precompute some information for the atom-diatom initial conditions
! (INITx = 3).

! This subroutine assumes three atoms
! INPUT
! escat = total energy
! ji = initial rot state of diatom
! vi = initial vib state of the diatom
! arri = initial arrangement
! mm = masses of the atoms
! OUTPUT
! ecol = energy of collision
! bmax,bmin = impact parameter
! ppreli = atom diatomic relative momentum
! rin, rout = turning points of state (vi,ji)

      use param, only: mnat, mnsurf, autofs, autoang, autoev, mnphase
      implicit none
      ! input variables
      integer,intent(in) :: ji,vi,arri,nsurf,nsurft,adbmode,ademod,phmode
      real(8), intent(in) :: mm(mnat),bparam,temp0
      ! input and output variables
      real(8) :: escat,bmax,bmin,ecoli
      ! output variables
      real(8),intent(out) :: einti,rin,rout,tau,phasead(mnphase,2),&
            ppreli,easym(mnsurf,3),rasym(mnsurf,3),evibi
      ! local variables
      integer ia,ib
      real(8) :: eroti,eelec,rmin,rmass,term,mdum,etmp
      logical :: ierror
      interface
        subroutine period(edia,jj,arr,mm,rin,rout,peri,phasead,nsurf,phmode,velphase)
          use param, only: mnat, pi, mnphase
          implicit none
! input
          integer :: arr,nsurf,jj, phmode
          real(8) :: rin,rout,mm(mnat),edia,xj

! output
          real(8) :: peri,phasead(mnphase,2)
          real(8),optional :: velphase(mnphase)
        end subroutine period
      end interface

      ierror = .false.

      ! Calculate reduced mass of system ----------------------------
      mdum = mm(1)+mm(2)+mm(3)
      if (arri.eq.1) rmass = mm(3)*(mm(1)+mm(2))/mdum
      if (arri.eq.2) rmass = mm(1)*(mm(2)+mm(3))/mdum
      if (arri.eq.3) rmass = mm(2)*(mm(3)+mm(1))/mdum

      ! Asymptotic analysis------------------------------------------
      ! easym and rasym is set here
      write(6,*)'Asymptotic analysis'
      write(6,*)' surf  arr      Emin (eV)      Rmin (A)'
      do ib=1,3
         do ia=1,nsurft
            call diamin(rmin,etmp,-1.d0,ib,mm,ia,ierror)
            if (ierror) stop
            easym(ia,ib) = etmp
            rasym(ia,ib) = rmin
            write(6,50)ia,ib,etmp*autoev,rmin*autoang
            if (ia.eq.nsurf.and.ib.eq.arri) rasym(ia,ib) = rmin
         enddo
      enddo
 50   format(2i5,2f15.5)

      ! Calculate internal energy -----------------------------------
      ! einti, rinad, routad, is set here
      write(6,111)'Preparing the diatom in arrangement ',arri,  &
        'with v = ',vi,' j = ',ji,' surface = ',nsurf
      write(6,*)
      eelec = easym(nsurf,arri)
      write(6,*)'Energetics (relative to zero of E)'
      write(6,*)'At the classical minimum in this arrangement:'
      write(6,100)'Re    = ',rasym(nsurf,arri)*autoang,' A'
      write(6,100)'Eelec = ',eelec*autoev,' eV'
      call ewkb(0,vi,arri,mm,evibi,rin,rout,nsurf,ierror)
      if (ierror) stop
      write(6,102)'Evib  = E(v=',vi,',j=',0,') =',evibi*autoev,' eV'
      if (ademod .lt. 4) then
        call ewkb(ji,vi,arri,mm,einti,rin,rout,nsurf,ierror)
        if (ierror) stop
        eroti = einti-evibi
        write(6,102)'Eint  = E(v=',vi,',j=',ji,') =',einti*autoev,' eV'
        write(6,103)'Erot  = Eint  - Evib   = ',eroti*autoev,' eV'
        write(6,101)'Turning points = ',rin*autoang,' and ',   &
          rout*autoang,' A'
      ! calculate period of vibration ------------------------------
      ! tau and phasead are set here
        call period(einti,ji,arri,mm,rin,rout,tau,phasead,nsurf,phmode)
        write(6,103)'Period for the vibration = ',tau*autofs,' fs'
      end if



 100  format(1x,a,20x,f13.5,a)
 101  format(1x,a,f13.5,a,f13.5,a)
 102  format(1x,a,i3,a,i3,a,4x,f13.5,a)
 103  format(1x,a,3x,f13.5,a)

      ! calculate collisional energy -------------------------------
      ! ecolad, escatad, and ppreli  are set here for ademod < 3
      if (ademod .eq. 1 .or. ademod .eq.2) then
        if (ademod.eq.1) then
          ecoli = escat - einti
        else if (ademod.eq.2) then
          escat = ecoli + einti
        end if
        write(6,100)'Escat = ',escat*autoev,' eV'
        write(6,103)'Ecol  = Escat - Eint   = ',ecoli*autoev,' eV'
        if (escat .le. einti) then
          write(6,222)'Total energy ',escat*autoev,' <  energy of ', &
            'initial diatomic state ',einti*autoev
          stop
        endif
        ppreli = dsqrt(2.d0*ecoli*rmass)
        write(6,103)'Initial relative momentum = ',ppreli,' au'
      end if

! RMP14
! Mode 1: Zero angular momentum
!     for J=0, l=j, so the range of impact parameters is
      if  (adbmode .eq. 1) then
        term = 1.d0/dsqrt(2.d0*rmass*ecoli)
        bmin = dsqrt(dble(ji*(ji+1)))*term
        bmax = dsqrt(dble((ji+1)*(ji+2)))*term
        write(6,'(1x,a)') "Impact parameter: Zero total angular momentum"
        if (ademod .ne. 4) then
          write(6,101)'Range of impact parameters = ',bmin*autoang, &
            ' to ',bmax*autoang,' A'
        end if
      else if (adbmode.eq.2) then
! Mode 2: Impaact parameter by input
        write(6,'(1x,a)') "Impact parameter: Constant"
        write(6,101)'Impact parameter = ',bparam*autoang
        bmin = bparam
        bmax = bparam
      else if (adbmode .eq. 3) then
        write(6,'(1x,a)') "Impact parameter: Uniform sampling"
        write(6,101)'Range of impact parameters = ',bmin*autoang, &
          ' to ',bmax*autoang,' A'
      else if (adbmode .eq. 4) then
        write(6,'(1x,a)') "Impact parameter: Importance sampling"
        write(6,101)'Range of impact parameters = ',bmin*autoang, &
          ' to ',bmax*autoang,' A'
        write(6,'(1x,a)') 'Opacity function: f(b/bmax) = 3(1-b/bmax)'
      else if (adbmode .eq. 5) then
        write(6,'(1x,a)') "Impact parameter: Strict collinear collision"
        bmin = 0.0d0
        bmax = 0.0d0
      end if

      write(6,*)

 111  format(1x,a,i5,a,i5,a,i5,a,i5)
 222  format(1x,a,e16.8,2a,e16.8)

      end subroutine preatomdiatom
