      SUBROUTINE TES
!     --------------------------------------------------------------------
!     It computes the triangle vertex coordinates for a sphere of radius
!     one, projecting the pentakisdodecahedro onto it.
!     --------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)

      COMMON/POLI/CV(32,3),XC1(15360),YC1(15360),ZC1(15360)             ! IR1294
!      COMMON/POLI/CV(32,3)                                              GDH0593
      
      DIMENSION THEV(6),FIV(6)
      
      DATA THEV/0.6523581397843682D0,1.1071487177940905D0, &
                1.3820857960113345D0,1.7595068575784587D0, &
                2.0344439357957027D0,2.4892345138054251D0/
      DATA FIV/0.6283185307179586D0,0.0D0              ,  &
               0.6283185307179586D0,0.0D0              ,  &
               0.6283185307179586D0,0.0D0              /
      DATA FIR/1.2566370614359173D0/

      CV(1,1)=0.D0
      CV(1,2)=0.D0
      CV(1,3)=1.D0
      CV(32,1)=0.D0
      CV(32,2)=0.D0
      CV(32,3)=-1.D0
      II=1
      DO 520 I=1,6
      TH=THEV(I)
      FI=FIV(I)
      CTH=COS(TH)
      STH=SIN(TH)
      DO 521 J=1,5
      FI=FI+FIR
      IF(J.EQ.1) FI=FIV(I)
      II=II+1
      CV(II,1)=STH*COS(FI)
      CV(II,2)=STH*SIN(FI)
      CV(II,3)=CTH
  521 CONTINUE
  520 CONTINUE
      END
!
!
      SUBROUTINE CALVER(CVN)
!     ---------------------------------------------------------------------
!     It divides one triangle into four.
!     ---------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION CVN(6,3)
      DO 7 N=1,3
      N2=N+3
      N1=N-1
      IF(N.EQ.1)N1=3
      XXX=(CVN(N,1)+CVN(N1,1))/2.0D0
      YYY=(CVN(N,2)+CVN(N1,2))/2.0D0
      ZZZ=(CVN(N,3)+CVN(N1,3))/2.0D0
      RRR=SQRT(XXX*XXX+YYY*YYY+ZZZ*ZZZ)
      FC=1.0D0/RRR
      CVN(N2,1)=XXX*FC
      CVN(N2,2)=YYY*FC
      CVN(N2,3)=ZZZ*FC
    7 CONTINUE
      END
!
      SUBROUTINE CALCEN(XV1,YV1,ZV1,XV2,YV2,ZV2,XV3,YV3,ZV3,CC)
!     ---------------------------------------------------------------------
!     It computes the center of a spherical triangle.
!     ---------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION CC(3)
!     write(6,*) 'in CALCEN', XV1,YV1
!     STOP
      XXX=(XV1+XV2+XV3)/3.0D0
      YYY=(YV1+YV2+YV3)/3.0D0
      ZZZ=(ZV1+ZV2+ZV3)/3.0D0
      RRR=SQRT(XXX*XXX+YYY*YYY+ZZZ*ZZZ)
      FC=1.0D0/RRR
      CC(1)=XXX*FC
      CC(2)=YYY*FC
      CC(3)=ZZZ*FC
      END
