      program ant
!     ANT:  Adiabatic and Nonadiabatic Trajectories
!     Main program.  This routine calls HEADER and READIN once, then
!     calls PREMOL once per AG to precalculate some constants.
!     DRIVER is called NTRAJ times, once for each trajectory.
!     FINAL STATE is called after each trajectory to write some data.
!
      use param, only: mnmol, mnsurf, pi, kb, autoang, autofs, ntrj_print
      use c_struct, only: nmol, mmofag, nat
      use c_sys, only: potflag, nsurft, repflag, reactcol, hstep0, hstep, &
                       trajlist, ntraj, temp0, maxtraj, tflag, itempzpe
      use c_initial, only: e0, ewell, nsurf0, r0collision, ademod, rotphasead
      use c_traj, only: ltunn, cross, rxrate, sumw, itraj, nhop, nsurf, &
                        collw, zpe, rxprob, node, ngauss, icollide, gw, &
                        itrapz
      use c_term, only: t_stime, termflag, outform, outbreak
      use c_output, only: time_tunnel, time_brent, time_pgen,lwrite
      use hdf5_output, only: hdf5o_end, hdf5o_flush, outflag_h5

      implicit none
      integer :: im,initrng,minitrng,i,nat0
      integer :: repflag_tmp
      double precision :: t1,t2,t3,cputime,t3old,remuag
      double precision :: denr(mnsurf,mnsurf),deni(mnsurf,mnsurf)
! Gauss-Legendre quardrature
      double precision :: endpt(2)
      double precision, allocatable :: scratch(:)
      character(len=10) :: date, date2, time, time2

      call header
! Print out date and time a calculation was begun
      write(6,*)'  '
      call date_and_time(date,time)
      date2=date(5:6)//"/"//date(7:8)//"/"//date(1:4)
      time2=time(1:2)//":"//time(3:4)//":"//time(5:6)
      write(6,*)'Calculation run on ',date2,' at ', time2
      time_tunnel=0d0; time_brent=0d0; time_pgen=0d0


      write(6,'("...entering READIN...",/)')
      call readin
      write(6,'("...exiting READIN...",/)')
      call CPU_TIME(t1)
      write(6,'("...entering PREMOL...",/)')
! MOPAC
      if(potflag .eq.-2)  call prepot
! tunneling : calculate nodes and weights for Gauss-Legendre quadrature
! It is only for the single surface case
      if(ltunn.and.nsurft.eq.1) then
        allocate(scratch(ngauss))
        node(:) = 0d0
        gw(:)   = 0d0
!xxx        call gaussq(1,ngauss,0.0,0.0,0,endpt,scratch,node,gw)
        call gaussq(1,ngauss,0.0d0,0.0d0,0,endpt,scratch,node,gw)
      endif
!     prepare each molecule
!     compute quantities that are the same for all trajectories
!     initialize electronic coordinates
      call initelec
      zpe(mnmol+1)=0.d0
      ewell(mnmol+1)=0.d0
      e0(mnmol+1)=0.d0
      do im = 1 , nmol
        call premol(im)
        zpe(mnmol+1)=zpe(mnmol+1)+zpe(im)
        ewell(mnmol+1)=ewell(mnmol+1)+ewell(im)
        e0(im)=ewell(im)+zpe(im)
        e0(mnmol+1)=e0(mnmol+1)+e0(im)
      enddo
!     If E0 is not available, do not use the second method in
!     calculating temperature
      if (e0(mnmol+1).eq.0.d0) itempzpe=.false.
      write(6,'("...exiting PREMOL...",/)')
      call CPU_TIME(t2)
      write(6,'(" CPU time in PREMOL is ",f10.5," s",/)')(t2-t1)
!
!     open some files to write
!      call fileopen(0)
!
      nat0 = nat
      t3 = 0.d0
      cputime = 0.d0
      rxprob=0.d0
      icollide=0
      collw = 0d0

!     loop over trajectories
      if (lwrite(1000)) then
        open(1000,FILE='Log.txt')
      end if
      do itraj=1,ntraj

        write(6,*)'***TRAJECTORY ',itraj,' OUT OF ',ntraj,'***'
        write(6,*)

        if (tflag(2).eq.1) then
          write(6,*)'Restarting trajectory #',trajlist(itraj)
          write(6,*)
          initrng = trajlist(itraj)-1
         else
          initrng = itraj-1
        endif

!     initialize this trajectory
        nsurf = nsurf0
        hstep = hstep0
!   ! always use adiabatic representation initialize trajectory
        repflag_tmp = repflag
        repflag = 0

!     nhop(i): number of hops only used in TFS, FSTU, and FSTU/SD methods
        do i=1,4
          nhop(i) = 0
        enddo
!     initialize the integer that determines formed or broken bonds
        outbreak = 0
        outform = 0
!     initialize the integer that determines the number of times a TRAZ-like
!     method is used
        itrapz = 0
!     initialize electronic coordinates
        call initelec
!     initialize the random number generator
!        rng_stream = init_sprng(initrng,maxtraj,ranseed,SPRNG_DEFAULT) RMP14
        call rng_initrandom(initrng,maxtraj)
        write(6,'("...entering INITMOL...",/)')
        do im = 1 , nmol
!     generate specific initial coordinates for each molecule
!         if(repflag.eq.1) call adtod(im)
          call initmol(im)
          if(repflag_tmp.eq.1)  then
              repflag = 1
              call adtod(im,0,denr,deni)
              repflag = repflag_tmp
          endif
        enddo
        write(6,'("...exiting INITMOL...",/)')
!     propagate this trajectory
        write(6,'("...entering DRIVER...",/)')
        sumw = 0d0
        call driver(nat0)
        write(6,'("...exiting DRIVER...",/)')
        t3old = t3
        call CPU_TIME(t3)
        cputime = cputime + (t3-t3old)
        write(6,'(" CPU time for this trajectory is ",f20.1," s",/)')  (t3-t3old)

!     analyze trajectory
        write(6,'("...entering FINALSTATE...",/)')
        call finalstate
        write(6,'("...exiting FINALSTATE...",/)')

        if (mod(itraj,ntrj_print)==0) then
          if ( lwrite(1000)) then
            write(1000, '(I8,"/",I8)') itraj,ntraj
          end if
          if (outflag_h5 .ge. 0) then
            call hdf5o_flush()
          end if
        end if

!     end loop over trajectories
      enddo
      if (lwrite(1000)) then
        close(1000)
      end if
!     close hdf5 file
      if (outflag_h5 .ge. 0) then
        call hdf5o_end()
      end if
!     clean memory
      if (ademod == 4 ) then
        deallocate(rotphasead)
      end if
!
! Monitoring defragmentation probability
      if (termflag.eq.7) then
!       rxprob = rxprob / dble(ntraj)
! JZ 09-16-2014
        rxprob = rxprob / sumw
        rxrate = -dlog(1.d0-rxprob)/t_stime
        write(6,101) 'Unimolecular fragmentation propability',rxprob
        write(6,101) 'Unimolecular rate constant (s-1)',rxrate &
                      /(autofs*1.d-15)
      endif
!
! Reactive collision cross section
! Integeration to give reactive cross-section
      if (reactcol) then
!       rxprob = rxprob / dble(ntraj)
! JZ 09-16-2014
        rxprob = rxprob / sumw
!       Reaction cross-section
        cross = pi*(r0collision**2)*rxprob
!       Reaction rate
        remuag = mmofag(1)*mmofag(2)/(mmofag(1)+mmofag(2))
        rxrate = dsqrt(8.d0*kb*temp0/(pi*remuag))*cross
!       write(6,101) 'Collision propability',dble(icollide)/dble(ntraj)
        write(6,101) 'Collision propability',collw/sumw
        write(6,101) 'Reactive propability',rxprob
        write(6,101) 'Reactive cross-section (A2)',cross*autoang**2
        write(6,101) 'Rate constant (au)',rxrate
        write(6,101) 'Rate constant (cm3 s-1 mole-1)',rxrate* &
                      (autoang*1.d-8)**3/(autofs*1.d-15)
      endif


      write(6,'(" CPU time for all trajectories is ",f20.1," s or ", &
        &f20.1," s per trajectory.",/)') cputime,cputime/dble(ntraj)
      write(6,'(a,es15.4)')'Time in tunneling code= ',time_tunnel
      write(6,'(a,es15.4)')'Time in brent code=     ',time_brent
      write(6,'(a,es15.4)')'Time in cnsvangmom code=',time_pgen
      call date_and_time(date,time)
      date2=date(5:6)//"/"//date(7:8)//"/"//date(1:4)
      time2=time(1:2)//":"//time(3:4)//":"//time(5:6)
      write(6,*)'Calculation ended on ',date2,' at ', time2
      write(6,'("Well done!")')
!     close open files
!      call fileopen(1)
 101  format(1x,a40,e18.8,a)

      END program ant
