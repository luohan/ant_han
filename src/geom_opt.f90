!      SUBROUTINE dfpmin(p,n,gtol,iter,fret,func,dfunc)
      SUBROUTINE dfpmin(p,n,nmax,pgradmax,prmsgrad,iter,fret)
      implicit none
      INTEGER iter,n,NMAX,ITMAX
      double precision fret,gtol,p(n),func,EPS,STPMX,TOLX
      PARAMETER (ITMAX=3000,STPMX=100.,EPS=3.d-8, TOLX=4.d0*EPS)
      EXTERNAL dfunc,func
! USES dfunc,func,lnsrch
! Given a starting point p(1:n) that is a vector of length n, the
! Broyden-Fletcher-Goldfarb-
! Shanno variant of Davidon-Fletcher-Powell minimization is
! performed on a function func,
! using its gradient as calculated by a routine dfunc. The
! convergence requirement on zeroing
! the gradient is input as gtol. Returned quantities are p(1:n) (the
! location of the minimum),
! iter (the number of iterations that were performed), and fret (the
! minimum value
! of the function). The routine lnsrch is called to perform
! approximate line minimizations.
! Parameters: NMAX is the maximum anticipated value of n; ITMAX is
! the maximum allowed
! number of iterations; STPMX is the scaled maximum step length
! allowed in line searches;
! TOLX is the convergence criterion on x values.
      INTEGER :: i,its,j
      LOGICAL :: check,ierror
      double precision :: den,fac,fad,fae,fp,stpmax,sum0,sumdg,sumxi,temp, &
                          test,dg(NMAX),g(NMAX),hdg(NMAX),hessin(NMAX,NMAX), &
                          pnew(NMAX),xi(NMAX)
      double precision :: dum,ftol
      double precision :: gradmax,rmsgrad
      double precision :: pgradmax,prmsgrad
      logical :: allok,xisok
      allok=.false.
      xisok=.false.
! Calculate starting function value and gradient,
! and initialize the inverse Hessian to the unit matrix.
! Initial line direction.
!      fp=func(p,n,NMAX)
      call dfunc(p,fp,g,n,NMAX)
      sum0=0.d0
      do 12 i=1,n
        do 11 j=1,n
          hessin(i,j)=0.d0
 11     enddo
        hessin(i,i)=1.d0
        xi(i)=-g(i)
        sum0=sum0+p(i)**2
 12   enddo
      stpmax=STPMX*max(dsqrt(sum0),dble(n))

! Main loop over the iterations.
      do 27 its=1,ITMAX
        iter=its
        call lnsrch(n,nmax,p,fp,g,xi,pnew,fret,stpmax,check,func,ierror)
!        if (ierror) exit
!      The new function evaluation occurs in lnsrch; save the function
!      value in fp for the next
!      line search. It is usually safe to ignore the value of check.
        fp=fret
        do 13 i=1,n
! Update the line direction, and the current point.
          xi(i)=pnew(i)-p(i)
          p(i)=pnew(i)
 13     enddo
! Test for convergence on x.
        test=0.d0
        do 14 i=1,n
          temp=dabs(xi(i))/max(dabs(p(i)),1.d0)
          if (temp.gt.test) test=temp
 14     enddo
!        if(test.lt.TOLX) return
!        if(test.lt.TOLX) xisok=.true.
! ignor TOLX
        xisok=.true.
! Save the old gradient, and get the new gradient.
        do 15 i=1,n
          dg(i)=g(i)
 15     enddo
        ftol = dum
        call dfunc(p,dum,g,n,NMAX)
! Test for convergence on zero gradient.
!        test=0.d0
!        den=max(fret,1.d0)
!        do 16 i=1,n
!          temp=dabs(g(i))*max(dabs(p(i)),1.d0)/den
!          if(temp.gt.test) test=temp
! 16     enddo
!        if(test.lt.gtol) return
        gradmax = 0.d0
        rmsgrad = 0.d0
        do 16 i=1,n
          gradmax = max(gradmax,dabs(g(i)))
          rmsgrad = rmsgrad + g(i)**2
 16     enddo
        rmsgrad = dsqrt(rmsgrad/dble(n-1))
        if ( (rmsgrad.lt.prmsgrad .and. gradmax.lt.pgradmax) ) then
!     &      .and. xisok )     then
          allok=.true.
          exit
        endif
! Compute difference of gradients, and difference times current matrix.
        do 17 i=1,n
          dg(i)=g(i)-dg(i)
 17     enddo
        do 19 i=1,n
          hdg(i)=0.d0
          do 18 j=1,n
            hdg(i)=hdg(i)+hessin(i,j)*dg(j)
 18       enddo
 19     enddo
! Calculate dot products for the denominators.
        fac=0.d0
        fae=0.d0
        sumdg=0.d0
        sumxi=0.d0
        do 21 i=1,n
          fac=fac+dg(i)*xi(i)
          fae=fae+dg(i)*hdg(i)
          sumdg=sumdg+dg(i)**2
          sumxi=sumxi+xi(i)**2
 21     enddo
! Skip update if fac not sufficiently positive.
        if (fac.gt.dsqrt(EPS*sumdg*sumxi)) then
          fac=1.d0/fac
          fad=1.d0/fae
! The vector that makes BFGS different from DFP:
          do 22 i=1,n
            dg(i)=fac*xi(i)-fad*hdg(i)
 22       enddo
! The BFGS updating formula:
          do 24 i=1,n
          do 23 j=i,n
              hessin(i,j)=hessin(i,j)+fac*xi(i)*xi(j)  &
               -fad*hdg(i)*hdg(j)+fae*dg(i)*dg(j)
              hessin(j,i)=hessin(i,j)
 23       enddo
 24       enddo
        endif
! Now calculate the next direction to go,
! and go back for another iteration.
        do 26 i=1,n
              xi(i)=0.d0
        do 25 j=1,n
              xi(i)=xi(i)-hessin(i,j)*g(j)
 25     enddo
 26     enddo
 27   enddo

      if (ierror) then
        write(6,111) 'Optimization failed in lnsrch at iteration',iter
      endif
      if (allok) then
        write(6,111) 'Optimization successfully ended after ',iter,  &
                   ' iterations'
      else
        write(6,*) 'Optimization failed at',iter
!        write(6,111) 'Maximum iterations exceeded ',ITMAX
      endif
      write(6,211) 'Maximum gradient: ',gradmax
      write(6,211) 'RMS gradient: ',rmsgrad
      write(6,211) 'Function value converged to: ',dabs(dum-ftol)
!      pause 'Maximum iterations exceeded'

 111  format(1x,a40,i5,a)
 211  format(1x,a40,e15.8)

      END SUBROUTINE dfpmin

      SUBROUTINE lnsrch(n,nmax,xold,fold,g,p,x,f,stpmax,check, &
                       func,ierror)
      integer :: NMAX
      INTEGER :: n
      LOGICAL :: check,ierror
      double precision :: f,fold,stpmax,g(n),p(n),x(n),xold(n),func,ALF,TOLX
      PARAMETER (ALF=1.d-4,TOLX=1.d-7)
      EXTERNAL func
! C USES func
! Given an n-dimensional point xold(1:n), the value of the function
! and gradient there,
! fold and g(1:n), and a direction p(1:n), finds a new point x(1:n)
! along the direction
! p from xold where the function func has decreased sufficiently.
! The new function value
! is returned in f. stpmax is an input quantity that limits the
! length of the steps so that you
! do not try to evaluate the function in regions where it is
! undefined or subject to overflow.
! p is usually the Newton direction. The output quantity check is
! false on a normal exit.
! It is true when x is too close to xold. In a minimization algorithm,
! this usually signals
! convergence and can be ignored. However, in a zero-finding algorithm the
! calling program should check whether the convergence is spurious.
! Parameters: ALF ensures sufficient decrease in function value; TOLX is
! the convergence criterion on x.
      INTEGER :: i
      double precision :: a,alam,alam2,alamin,b,disc,f2,rhs1,rhs2,slope, &
                          sum0,temp,test,tmplam
      check=.false.
      ierror=.false.

      sum0=0.d0
      do 11 i=1,n
        sum0=sum0+p(i)*p(i)
 11   enddo

! Scale if attempted step is too big.
      sum0=dsqrt(sum0)
      if (sum0.gt.stpmax) then
        do 12 i=1,n
          p(i)=p(i)*stpmax/sum0
 12     enddo
      endif

      slope=0.d0
      do 13 i=1,n
        slope=slope+g(i)*p(i)
 13   enddo
      if (slope.ge.0.d0) then
!       pause 'roundoff problem in lnsrch'
        ierror=.true.
!        return
      endif

!  Compute min.
      test=0.d0
      do 14 i=1,n
        temp=dabs(p(i))/max(dabs(xold(i)),1.d0)
        if(temp.gt.test) test=temp
 14   enddo 
      alamin=TOLX/test
! Always try full Newton step first.
      alam=1.d0
      
! Start of iteration loop.
 1    continue
      do 15 i=1,n
        x(i)=xold(i)+alam*p(i)
 15   enddo
      f=func(x,n,NMAX)
! Convergence on x. For zero finding,
! the calling program should verify the convergence.
      if (alam.lt.alamin) then
         do 16 i=1,n
              x(i)=xold(i)
 16      enddo
         check=.true.
         return
! Sufficient function decrease.
      else if(f.le.fold+ALF*alam*slope) then
         return
! Backtrack.
      else
! First time.
        if(alam.eq.1.)then
          tmplam=-slope/(2.*(f-fold-slope))
! Subsequent backtracks.
        else
          rhs1=f-fold-alam*slope
          rhs2=f2-fold-alam2*slope
          a=(rhs1/alam**2-rhs2/alam2**2)/(alam-alam2)
          b=(-alam2*rhs1/alam**2+alam*rhs2/alam2**2)/(alam-alam2)
          if(a.eq.0.d0) then
            tmplam=-slope/(2.d0*b)
          else
            disc=b*b-3.d0*a*slope
            if(disc.lt.0.d0) then
              tmplam=.5d0*alam
            else if(b.le.0.d0) then
              tmplam=(-b+dsqrt(disc))/(3.d0*a)
            else
              tmplam=-slope/(b+dsqrt(disc))
            endif
          endif
!     lambda <= 0.5 lambda1
          if(tmplam.gt..5d0*alam) tmplam=.5d0*alam
        endif
      endif
      alam2=alam
      f2=f
!     lambda >= 0.1 lambda1
      alam=max(tmplam,.1d0*alam)
! Try again.
      goto 1

      END SUBROUTINE lnsrch
