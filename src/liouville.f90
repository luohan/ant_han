      subroutine liouv(im)
!     Velocity Verlet algorithm with a Liouville approach incorporated
!     with Nose-Hoover two chain thermostat
!   From Daan Frenkel & Berend Smit: Understanding Molecular Simulation
!   -- From Algorithms to Applications, Academic Press, New York, 2nd
!   Ed, 2002, P536-P542

      use c_sys, only: hstep
      use c_traj, only: inh

      implicit none
      integer :: im

!     propagating the chain
      if (inh) call chain
!     propagating the position and momentum
      call pos_vel(im)
!     propagating the chain
      if (inh) call chain

      end subroutine liouv

      subroutine chain
!   Propagating the chain (two chains)
! NH(1,1):   Mass of the first variable
! NH(1,2):   Mass of the second variable
! NH(2,1):   Position of the first variable
! NH(2,2):   Position of the second variable
! NH(3,1):   Velocity of the first variable
! NH(3,2):   Velocity of the second variable
      use c_struct, only: nh, infree, natoms
      use c_sys, only: hstep
      use c_traj, only: kbtarg, ke, ppm 

      implicit none
      integer :: i,j,k
      double precision :: G1,G2,dt2,dt4,dt8,s

      dt2 = 0.5d0*hstep
      dt4 = 0.5d0*dt2
      dt8 = 0.5d0*dt4

      G2 = ( nh(1,1)*nh(3,1)**2 - kbtarg ) / nh(1,2)
!     Update Second Velocity using E.2.6
      nh(3,2) = nh(3,2) + G2*dt4
!     Update first Velocity using E.2.7
      nh(3,1) = nh(3,1) * dexp( -nh(3,2)*dt8 )

      G1 = ( 2.d0*ke - dble(infree)*kbtarg ) / nh(1,1)
!     Update first Velocity using E.2.8
      nh(3,1) = nh(3,1) + G1 * dt4
!     Update first Velocity using E.2.7
      nh(3,1) = nh(3,1) * dexp( -nh(3,2)*dt8 )

!     Update first position using E.2.9
      nh(2,1) = nh(2,1) + nh(3,1)*dt2
!     Update first position using E.2.10
      nh(2,2) = nh(2,2) + nh(3,2)*dt2

!     Scale factor in E.2.11
      s = dexp( -nh(3,1)*dt2 )
!     update momentum using E.2.11
      do i=1,natoms
        ppm(1,i) = s*ppm(1,i)
        ppm(2,i) = s*ppm(2,i)
        ppm(3,i) = s*ppm(3,i)
      enddo
!     update kinetic energy
      ke = ke*s*s

!     update first velocity using E.2.7
      nh(3,1) = nh(3,1) * dexp( -nh(3,2)*dt8 )

      G1 = ( 2.d0*ke - dble(infree)*kbtarg ) / nh(1,1)
!     Update first Velocity using E.2.8
      nh(3,1) = nh(3,1) + G1 * dt4
!     Update first Velocity using E.2.7
      nh(3,1) = nh(3,1) * dexp( -nh(3,2)*dt8 )

      G2 = ( nh(1,1)*nh(3,1)**2 - kbtarg ) / nh(1,2)
!     Update Second Velocity using E.2.6
      nh(3,2) = nh(3,2) + G2*dt4

      end subroutine chain

      subroutine pos_vel(im)
! Propagating the Positions and Velocities
! Apply E.2.4 to the current position
      use c_struct, only: xxm, mmm, natoms
      use c_sys, only: hstep
      use c_traj, only: gv, ppm, ke

      implicit none
      integer :: i,j,k,im
      double precision :: mr,mgrad,dt,dt2

      dt  = hstep
      dt2 = 0.5d0*dt
      
! Update position using E.2.13
      do i=1,natoms
        mr = 1.d0/mmm(i)
        xxm(1,i) = xxm(1,i) + mr*ppm(1,i)*dt2
        xxm(2,i) = xxm(2,i) + mr*ppm(2,i)*dt2
        xxm(3,i) = xxm(3,i) + mr*ppm(3,i)*dt2
      enddo

! Calc force at half step
      call getgrad(mgrad,im)

      ke = 0.d0
      do i=1,natoms
        mr = 1.d0/mmm(i)
        do j=1,3
!       Update momentum using E.2.12
!       force is the negative of gv (dU/dx)
        ppm(j,i) = ppm(j,i) - gv(j,i)*dt
!       Update position using E.2.13
        xxm(j,i) = xxm(j,i) + mr*ppm(j,i)*dt2
!       Update kinetic energy
        ke = ke + 0.5d0*mr*ppm(j,i)*ppm(j,i)
        enddo
      enddo

      end subroutine pos_vel
