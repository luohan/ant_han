      subroutine invmtbt(m,invm)

!     This routine is to calculate the inverse of a 3by3 matrix m
      implicit none
      integer i,j
      DOUBLE PRECISION m(3,3),invm(3,3)
      integer lwork
      parameter (lwork=5)
      double precision work(lwork)
      integer IPIV(3),infor

      do i=1,3
        do j=1,3
          invm(i,j)=m(i,j)
        enddo
      enddo
      call dgetrf(3,3,invm,3,ipiv,infor)
      call dgetri(3,invm,3,ipiv,work,lwork,infor)
      END subroutine invmtbt
