! Subroutine that determines the relative kinetic energy of NH2-H 
! for each H atom and associated NH2 according to the time

! in common block c_struct.f
! natoms = number of atoms (input)
! mmm = mmm of atoms in au (input)
! xxm = positions in au (input)
! ppm = momentum in au (input)
! erel = relative energy in au (output)
! dist = distance between N and H in au (output)
! icpt = dimension of erel and dist (output) 

      subroutine relenerg(erel,dist,icpt)  

      use param, only: amutoau
      use c_struct, only: xxm, mmm, natoms
      use c_traj, only: ppm

      implicit none
      integer i,j,k,icpt,icpt2
      real*8 velh,velnh2,mnh2,redm,vrel,erel
      real*8 pos,dist

      dimension erel(4)
      dimension velnh2(3),velh(3)
      dimension dist(4) 

      icpt = 0     
      do i=1,natoms

! We look for H atoms
         if (mmm(i)/amutoau.lt.1.01d0) then
            icpt = icpt + 1
            do j=1,3
               velh(j) = ppm(j,i)/mmm(i)
            enddo
            mnh2 = 0.0d0
            do j=1,natoms
               if (j.ne.i) then
                  mnh2 = mnh2 + mmm(j)
                  do k=1,3
                     velnh2(k) = velnh2(k) + ppm(k,j)
                  enddo
               endif
            enddo  
            do k=1,3 
               velnh2(k) = velnh2(k)/mnh2
            enddo 
            redm = mmm(i)*mnh2/(mmm(i)+mnh2)
            vrel = dsqrt( (velh(1)-velnh2(1))**2 +   &
                   (velh(2)-velnh2(2))**2 + (velh(3)-velnh2(3))**2 ) 
            erel(icpt) = 0.5d0*redm*vrel**2
         endif

! We look for the N atom
         if (mmm(i)/amutoau.gt.14.0d0) then
            icpt2 = 0
            do j=1,natoms
               if (i.ne.j) then
                  icpt2 = icpt2 + 1
                  dist(icpt2) = dsqrt( (xxm(1,j)-xxm(1,i))**2 +  &
                                (xxm(2,j)-xxm(2,i))**2 +         &
                                (xxm(3,j)-xxm(3,i))**2 )
               endif
            enddo                
         endif 

      enddo 

! Test for NH3
      if (icpt.ne.3) then
         write(6,*) icpt,' relative energies calculated instead of 3 !'
         stop
      endif 

      end subroutine relenerg
