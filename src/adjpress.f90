      subroutine adjpress(presstnow,pressnow,press0)
! Simplest adjusting procedure: problem: hot solvent, cold solute
! presstnow:  Current pressure tensor
! pressnow:   Current pressure
! press0:     Target pressure
! lamda:      Adjusting matrix
! adjf:       Adjusting fact
! tmp(3):     Temperory storage of the three components of a vector of an atom

      use param, only: autoatm, autoang, autoatm
      use c_output, only: idebug
      use c_struct, only: icell, natoms, xxm, cvec, invcvec, cell
      use c_sys, only: hstep, p0, iadjpress, taup
      use c_traj, only: vol

      implicit none
      double precision :: presstnow(3,3),press0,lamda(3,3), &
                          pressnow,tmp(3),adjf,detmtbt
!     Local
      integer :: i,j,k
!
!     Berendsen barostat
      if (idebug) then
      write(6,111)'Adjusting the position of the atom, cell parameters', &
                  ' and volume for NPT ensemble'
        write(6,*) 'Desired pressure',p0*autoatm,' atm'
      endif
      if (iadjpress.eq.0) then
        if (idebug) then
          write(6,*) 'Berendsen barostat'
        endif
!       cubic
        if (icell.eq.1) then 
          adjf=(1.d0-(hstep/taup)*(press0-pressnow))**(1.d0/3.d0)
!          if (pressnow.le.0.d0.or.press0/pressnow .ge. 2.d0) goto 9999
!          if (pressnow/press0 .ge. 20.d0) then
!            write(6,50) 'ERROR: The current pressure is abnormal.', &
!                       ' P/P0 = ',pressnow/press0
!          write(6,*) 'Check your initial structure and cell parameters'
!            stop
!          endif
!           goto 9999
          if (adjf.le.0.d0) then
            write(6,*) 'ERROR: taup is too small'
            stop
          endif
          adjf = min(adjf,1.3d0)
          adjf = max(adjf,0.7d0)
          do i=1,natoms
            xxm(1,i)=adjf*xxm(1,i)
            xxm(2,i)=adjf*xxm(2,i)
            xxm(3,i)=adjf*xxm(3,i)
          enddo
!         update cell information
          cell(1)=adjf*cell(1)
          cell(2)=cell(1)
          cell(3)=cell(1)
          cvec(1,1)=cell(1)
          cvec(2,2)=cell(1)
          cvec(3,3)=cell(1)
          vol = cell(1)*cell(2)*cell(3)
          if (idebug) then
             write(6,50) 'The adjusting factor for the cubic or sphere', &
                        ' cell ',adjf
             write(6,100) 'The volume after adjusting is: ', &
                         vol*autoang**3,' A^3'
          endif
!         cuboid and other cells
         else
          do i=1,3
            do j=1,3
!            lamda=1-(hstep/taup)*(P0-Pt)
              if (i.ne.j)  then 
                 lamda(i,j)=0.d0-(hstep/taup)*(0.d0-presstnow(i,j))
               else
                 lamda(i,j)=1.d0-(hstep/taup)*(press0-presstnow(i,j))
              endif
            enddo
          enddo
          if (idebug) then
             write(6,50) 'The adjusting matrix for non cubic cell'
             do i=1,3
               write(6,150) (lamda(i,j),j=1,3)
             enddo
          endif
          do i=1,natoms
            tmp(1) = lamda(1,1)*xxm(1,i)+lamda(1,2)*xxm(2,i)+ &
                     lamda(1,3)*xxm(3,i)
            tmp(2) = lamda(2,1)*xxm(1,i)+lamda(2,2)*xxm(2,i)+ &
                     lamda(2,3)*xxm(3,i)
            tmp(3) = lamda(3,1)*xxm(1,i)+lamda(3,2)*xxm(2,i)+ &
                     lamda(3,3)*xxm(3,i)
            xxm(1,i)=tmp(1)
            xxm(2,i)=tmp(2)
            xxm(3,i)=tmp(3)
          enddo
!         update cell information 
          do i=1,3
            tmp(1) = lamda(1,1)*cvec(1,i)+lamda(1,2)*cvec(2,i)+ &
                     lamda(1,3)*cvec(3,i)
            tmp(2) = lamda(2,1)*cvec(1,i)+lamda(2,2)*cvec(2,i)+ &
                     lamda(2,3)*cvec(3,i)
            tmp(3) = lamda(3,1)*cvec(1,i)+lamda(3,2)*cvec(2,i)+ &
                     lamda(3,3)*cvec(3,i)
            cvec(1,i)=tmp(1)
            cvec(2,i)=tmp(2)
            cvec(3,i)=tmp(3)
          enddo
          vol=detmtbt(cvec)
!        update the inverse of the matrix of the cell vectors
        endif
        call invmtbt(cvec,invcvec)
!        to be implemented in the future
       else
         write(6,*) 'ERROR: Still working on other barostat'
         stop
      endif
 9999     continue
 50   format(1x,a,a,f12.5)
 100  format(1x,a,f20.1,1x,a)
 150  format(1x,3f20.5)
 111  format(1x,2a)
 
      end subroutine adjpress
