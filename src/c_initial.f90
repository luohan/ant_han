module c_initial
  use param, only: mnat, mnmol, mnsurf, mnjlevel
! **********************************************************************
! INITIAL CONDITIONS
! **********************************************************************
! nsurf0     Initial electronic surf for all trajectories
! nsurfi     Initial electronic surf, which may not be the same as
!            nsurf0, so that it is possible to prepare an initial
!            state coming from the distribution of nsurfi state
! repflgi    Initial representation, used in combination with nsurfi
! initx(i)   Initial conditions flag for AG i
! initp(i)   Initial conditions flag for AG i
! vibsel(i)  Normal mode analysis flag for AG i
! rotsel(i)  Rotational state flag for AG i
! rran(3,mnmol) INITx=1: Cluster radius used to generate randum clusters
!               INITx=3: a,b,c of a cuboid
!               (input in angstroms, converted immediately to bohr)
! temp0im(i)    Target temperature of AG i when INITp = 0 (Kelvin)
!
!
! --------- Normal Mode Analysis Related -----
! nmvec(3,j,m,mnmol) Normal mode vectors for the m^th normal mode
! freq(m,mnmol)    Normal mode frequencies for the m^th normal mode
! qnmrot0(j,im) initial rotational quantum number for AG im, j=1,3
! qnmvib0(n,im) initial vibrational quantum number for AG im,
!               n=1,3*natom(im)-5 (or 6)
! ivibtype(im)    Initial geometry is a well (0) or a saddle point (1).
! ewell(mnmol)     Energy of the initial structure when using normal
!                  mode method for initial conditions.  Should
!                  correspond to the energy of the bottom of some well.
! e0(mnmol)        Ee (ewell) + ZPE
! etrans(mnmol)    Translational energy of an AG
! eroti(mnmol)     Three components of rotational energies of an AG
! The following pertain to orienting the AGs with respect to each other
! icompp           Provide initial compp or not
! icomxx           Provide initial comxx or not
! comxx(j,mnmol)   Initial CoM coords for AG im (input in A, converted
!                  immediately to bohr)
! compp(j,mnmol)   Initial CoM velocity for AG im (input in au)
! ifnoang        .T. remove CoM angular momentum before entering DRIVE
! ifeqwithrot    If equilibrated with angularmomentum on. Default: .F.
! ifreinitrot    If reset Ang. Mom. after equilibrium on. Default: .T.
! lfixte         Every trajectory begin with same initial total energy
! te0fixed       Total energy to be fixed at the beginning of traj.
! vibene(3*mnat,im)     Energy for each vibrational mode

! irottemp          Force sample rotational energy based on temperature
! iscaleenergy      Scale ppm and xxm to match erot and evib
! irotfreeze        Freeze rotational energy i.e. no angular momentum

      logical :: irottemp,iscaleenergy,irotfreeze
      integer :: nsurf0,nsurfi,initx(mnmol),initp(mnmol),vibsel(mnmol), &
                 repflgi,ivibdist(mnmol),vibdistn(3*mnat,mnmol), &
                 ivibtype(mnmol)
      logical :: rotsel(mnmol),icompp,icomxx,ifnoang,ifeqwithrot,ifreinitrot,lfixte
      double precision :: rran(3,mnmol),temp0im(mnmol),eroti(3,mnmol), &
                          etrans(mnmol),te0fixed,deltate,vibene(3*mnat,mnmol),verte(mnmol), &
                          bandwd(mnmol)
      integer :: qnmrot0(3,mnmol)
      double precision :: qnmvib0(3*mnat,mnmol)
      double precision :: nmvec(3,mnat,3*mnat,mnmol),freq(3*mnat,mnmol), &
                          ewell(mnmol+1),e0(mnmol+1),nvinit(3*mnat,mnmol), &
                          comxx(3,mnmol),compp(3,mnmol), xnmref(3,mnat)
! The following are used for atom-diatom scattering conditions (INITx = 4)
! arrad      Initial molecular arrangement
! adbmode        = 1 Impact parameter is selected to make total angular momentum zero
!                = 2 constant value of b, bparam
!                = 3 bminad and bmaxad need to be provided, uniform sampling
!                = 4 Importance sampling with opacity function P(b/bmaxad)=3(1-b/bmaxad)
!                = 5 strict collinear collision, (i.e. bmaxad=0, theta=0, phi=0), you should
!                    manually set J=0
! ademod         = 1 collision energy ecolad is provided
!                = 2 escatad = ecolad + einti is provided
!                = 3 ecolad sampled from Boltzmann
!                = 4 ecolad and jjad sampled from Boltzmann
! nspinmode      = 0 nothing to do with nuclear spin degeneracy
!                = 1 (2I+1)(I+1) for even J, (2I+1)I for odd J
!                = 2 vice vesa
! phmode         = 1 start from maximum or minimum stretch (no accurate, ANT default)
!                = 2 solve phase v.s. interatomic distance accurately
! rrad0      Initial atom-diatom distance (input in A, converted
!            immediately to bohr)
! bparam     Constant impact parameter in au
! bmaxad     Maximum impact parameter in au
! bminad     Minimum impact parameter in au
! easym(k,m) Minimum energy for the diatom in arragement m (bottom of PEC)
! rasym(k,m) Minimum energy distance for the diatom in arragement m (equilibrium distance)
! --- all the following work with ademod = 4
! nrotmax     Maximum rotational level for given vvad
! rotelevel   energy of different rotational level in au
! rotrin      similar to rinad
! rotrout     similar to rinout
! rottau      similar to tauad
! rotpar      normalized partition function for each level
! rotphasead  similar to phasead
! rot_n0_level rotational levels with more than 1 traj
! rot_n0_ntrj  number of trajectories to calculate
! rot_nn0      number of rotational levels with ntrj>0
      integer :: arrad,adbmode,ademod,phmode,nspinmode
      double precision :: rrad0,bmaxad,bminad,bparam,easym(mnsurf,3),rasym(mnsurf,3),nspin
      integer :: nrotmax,rot_n0_ntrj(mnjlevel+1),rot_n0_level(mnjlevel+1),rot_nn0
      real(8) :: rotelevel(0:mnjlevel),rotpar(0:mnjlevel),&
                 rotrin(0:mnjlevel),rotrout(0:mnjlevel),rottau(0:mnjlevel)
      real(8),allocatable :: rotphasead(:,:,:)
!
! **********************************************************************
! Reaction collision
! **********************************************************************
! stateselect if reactcol, select if to do state select collision
!             or (default): run a MD to equilibrium each AG before
!             collision with time "teqcol0"
! r0collision   Initial distance between AGs
! transelect    1: Initial translation energy provided by the user
! b_fix         Fix initial impact parameter b
! itosave       Save initial conditions from this step
! nsave         Save initial conditions every nsave steps
! b0impact      Impact parameter
! equilibrium   Impact parameter
! ipicktrj      0: Descard saved points if equilibrium run failed
!               1: Keep saved points if equilibrium run failed
! pickthr       Probability of picking trajectories
! demin         energy >= demin will be picked. Default: e0(ewell+zpe)
! demax         energy < demax will be picked. Default: e0(ewell+zpe)
! r0effect      Collion cross-section between two reactants
! ifr0eff       .T. Provide r0effect

      integer :: itosave,nsave,ipicktrj
      logical :: b_fix,stateselect,transelect,equilibrium,ifr0eff
      real :: pickthr
      double precision :: r0collision,b0impact,demin,demax,r0effect
! **********************************************************************
! internal coordinates
!**********************************************************************
      integer :: nstr,nbend,ntor,nimptor,ntunn,istr(2,3*mnat),  &
                 ibend(3,3*mnat),itor(4,3*mnat),imptor(4,3*mnat), &
                 itunn(3*mnat),nlinben,ilinben(3,3*mnat)
      double precision :: drr(3*mnat)

end module c_initial
