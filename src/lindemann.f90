      subroutine lindemann(step,time,lind,lindi,rcomi,rcomi2,rcomb, &
                           arcomd,arcomd2,rbin,mbin,lindb,lindis,anbin,lindisa, &
                           iflind,ifrcom,ifbin)

! lindb: Bin the Berry parameter according to the distance from the CoM
      use param, only: mnat, mnbin, autoang
      use c_output, only: lwrite  
      use c_struct, only: natoms, rij, rij2, arij, arij2, xxm

      implicit none
      integer :: mbin
      double precision :: lind,lindi(mnat),step,time,lindb(mnbin), &
                          lindis(mnat),anbin(mnbin),lindisa(mnat),rcomb(mnbin)
      double precision :: rcomi(mnat),rcomi2(mnat),arcomd,arcomd2,rbin
      logical :: iflind,ifrcom,ifbin
      integer :: i,j,k,ibin,nbin,itmp
      double precision :: tmp1,tmp2,rtime,rr,rr2,ar,ar2,tmp,tmp3,rmax0, &
                          rmi(mnat),rcombm(mnbin)
      logical :: irorder
! rr     = distance
! rr2    = square distance
! rcomi  = distance from atom i to CoM
! rcomi2 = square distance from atom i to CoM
! rcombm = temperary storage of binned rcomi
! rmi    = distance from atom i to CoM
! mbin   = maximum bins in the AG

      ar=0.d0
      ar2=0.d0
      lind = 0.d0
      rtime= 1.d0/time
      tmp=step/dble(natoms)
! mbin: maximum bins
! rmax0: maximum radius of the system
      irorder=.false.
      if (lwrite(47).or.lwrite(48).or.lwrite(470).or.lwrite(480)) &
       irorder=.true.
      if (ifrcom) then
      rmax0=-1.d0
      do i=1,natoms
        rr2    = xxm(1,i)**2 + xxm(2,i)**2 + xxm(3,i)**2
        rr  = dsqrt(rr2)
        ar2 = ar2 + rr2
        ar  = ar  + rr
        rcomi(i) = rcomi(i) + rr*step
        rcomi2(i) = rcomi2(i) + rr2*step
        rmax0=max(rmax0,rr)
        rmi(i)=rr
      enddo
      arcomd2= arcomd2 + ar2*tmp
      arcomd = arcomd  + ar*tmp
      endif

! bin distance
      if (ifbin) then
      nbin=int(rmax0/rbin)
      mbin=max(nbin,mbin)
      if (mbin.ge.mnbin) then
        write(6,111) 'Too many bins. Please increase RBIN value to ', &
             'at least: ',(rmax0/dble(mnbin))*autoang,' A'
        stop
      endif
      endif

! Berry parameter
      do i=1,natoms
        lindi(i) = 0.d0
        if (iflind) then
          do 10 j=1,natoms
            if (j.ne.i) then
            arij(i,j) = arij(i,j) + rij(i,j)*step
            arij2(i,j) = arij2(i,j) + rij2(i,j)*step
            tmp1 = arij(i,j)  * rtime
            tmp2 = arij2(i,j) * rtime
!  Bond distance fluctuation (Berry parameter)
            if (tmp2.gt.tmp1**2 .and. tmp1.gt.1.d-2) then
              lindi(i) = lindi(i) + dsqrt(tmp2-tmp1**2)/tmp1
            endif
            endif
 10       continue
          lindi(i) = lindi(i)/dble(natoms-1)
          lind     = lind + lindi(i)
        endif
        if (ifbin) then
          ibin=int(rmi(i)/rbin)+1
!         Binned atom number distribution
          anbin(ibin) = anbin(ibin) + step
!         Binned lind
          if (lwrite(46).or.lwrite(460)) then
            lindb(ibin) = lindb(ibin) + lindi(i)*step
          endif
!         Binned rcom
          if (lwrite(51).or.lwrite(510))  &
             rcomb(ibin) = rcomb(ibin) + rmi(i)*step
        endif
        if (irorder) lindis(i) = lindi(i)
      enddo
!     sort lindi according to rmi
      if (irorder) then
        do i=1,natoms
        do j=i+1,natoms
          if (rmi(i).gt.rmi(j)) then
                tmp1     =rmi(i)
                rmi(i)   =rmi(j)
                rmi(j)   =tmp1
                tmp1     =lindis(i)
                lindis(i)=lindis(j)
                lindis(j)=tmp1
          endif
        enddo
        enddo
      endif
! Average on sorted lindi
      if (lwrite(48).or.lwrite(480)) then
        do i=1,natoms
          lindisa(i)=lindisa(i)+lindis(i)*step
        enddo
      endif
!
      lind = lind/dble(natoms)

 111  format(1x,2a,e16.8,a)

      end subroutine lindemann
