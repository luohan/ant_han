C
C This file contains the maximum number of atoms used in the program
C
      integer NPACK
      double precision pi,pd2,pd64
      PARAMETER (MXPT=3840)
      PARAMETER (MXPTSV=2620)
      PARAMETER (IATMSV=6)
      COMMON /PICOM/ PI,PD2,PD64
C
C other stuff taken from AMSOL:
C***********************************************************************
C*  THIS FILE CONTAINS ALL THE ARRAY SIZES FOR USE IN THE PACKAGE.     *
C*                                                                     *
C*    MAXPTS = DEFAULT VALUE FOR THE MAXIMUM NUMBER OF POINTS ON A     *
C*             SPHERE IN THE SURFACE AREA CALCULATION                  *
C*    MXPTSV = DEFAULT VALUE FOR THE MAXIMUM NUMBER OF POINTS ON A     *
C*             SPHERE IN THE SURFACE AREA CALCULATION WHERE ALL        *
C*             POSSIBLE INFORMATION ON THE DISTANCE BETWEEN POINTS ON  *
C*             EACH SPHERE AND THE SPHERES ITS CONNECTED TO WILL BE    *
C*             SAVED.                                                  *
C*    IATMSV = DEFAULT VALUE FOR THE MAXIMUM NUMBER OF ATOMS IN A      *
C*             SYSTEM WHERE ALL POSSIBLE INFORMATION ON THE DISTANCE   *
C*             BETWEEN POINTS ON EACH SPHERE AND THE SPHERES ITS       *
C*             CONNECTED TO WILL BE SAVED.                             *
C***********************************************************************
C*
