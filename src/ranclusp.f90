      subroutine ranclusp(radius)
!
! Generate a random cluster of atoms within radius RADIUS,
! and with minimum and maximum minimum-atom-atom distances
! of RSMALL and RBIG, respectively.
!     INPUT:
! natoms
! xxm:    coordinates for the molecule (AG)
! indmm:  atom index in periodic table
! radius: desired radius of the sphere 
!         Attention: since using this radius, the generated atoms are
!         closely packed, this is what one doesn't want. To be safer,
!         the radius is set to be larger. 2.0 the original, therefore
!         the diameter of the sphere.
! rsmal:  the smallest distance between two atoms allowed
! rbig:   the largest  distance between two atoms allowed
! ran:    random number
! iclu:   The times tried to generated a whole set of coordinates   
! ikp:    couting the atoms kept 
! inxt:   Index of the next atom to be generated
! iprev:  index of the atoms generated
! irest:  index of the rest atoms
! nrest:  The number of atoms rest
! ifar:   if one atom is far away to all other atoms, it is also
!         rejected. This is done by counting the atoms with distance
!         larger than rbig: if ifar equals to the number of previously
!         generated good atoms (kept atoms), then it is rejected
! Using spheric coordinates to generate atoms and then transfer back to
! cartesian coordinate
! ri:     the distance of an atom to origin
! theta:  theta angle (vector r to z)
! phi     phi angle (projection of vector r on xy plane to x)
! rixy:   The projection of ri on xy plane
! rijm:   distance between two atoms
      use param, only: mnat, pi
      use c_struct, only: xxm, bdfmthr, natoms, rijco, indmm
      implicit none
      double precision radius
!
      integer iclu,ikp,itry,i,j,k,irej,ifar,ipick,inxt,i1st,  &
         iprev(mnat),irest(mnat),itmp(mnat),nrest,itry1
      double precision rijm,rsmall,rbig,xxn(3)
      double precision ri,theta,phi,rixy,ran(3)
!
      iclu   = 0
 10   continue
      iclu = iclu + 1
      if (iclu.gt.20) then
        write(6,111) 'Tried too many times to generate a random sphere', &
                     ' structure ... quitting'
        stop
      endif
!     Randomly pick an atom and place it at the origin
      call get_rng(ran(1))
      i1st = nint( 0.5+ran(1)*dble(natoms) )
      iprev(1) = i1st
      do k=1,3
        xxm(k,i1st) = 0.d0
      enddo
!       update irest
      k=0
      do i=1,natoms
        if (i.ne.i1st) then
          k=k+1
          irest(k)=i
        endif
      enddo
!
      itry = 0
      ikp = 1
! generate the rest natom-1 points within the sphere
      do while(ikp.lt.natoms)
!       Randomly pick the next atom to be generated
        nrest=natoms-ikp
        call get_rng(ran(1)) 
        ipick = nint(0.5+ran(1)*dble(nrest) )
        inxt = irest(ipick)
!       update irest
        k=0
        do i=1,nrest
          if (i.ne.ipick) then
            k=k+1
            itmp(k)=irest(i)
          endif
        enddo
        nrest=natoms-ikp-1
        do i=1,nrest
          irest(i)=itmp(i)
        enddo
!
        itry = 0
 20     itry = itry + 1
        if (itry.eq.10000) then
          write(6,*) 'Bad packing.  Start again.'
          goto 10
        endif
!       generate an atom between rsmall and radius
        call get_rng(ran(1)) 
        rsmall = 0.8d0*rijco( indmm(i1st),indmm(inxt) )
        ri = rsmall+ ran(1)*(radius-rsmall)
        itry1 = 0
 30     itry1 = itry1+1
        if (itry1.eq.100) goto 20
        do i=2,3
          call get_rng(ran(i)) 
        enddo
        ran(2) = 1.0 - 2.0*ran(2)
        theta = dacos( ran(2) )
        phi = ran(3)*(2.0d0*pi)
!
        rixy   = ri*dsin( theta )
        xxn(1) = rixy*dcos( phi )
        xxn(2) = rixy*dsin( phi )
        xxn(3) = ri* ran(2)
!       rij: distance between the two atoms 
        ifar = 0
        do i=1,ikp
          rsmall = 0.8d0*rijco( indmm(iprev(i)),indmm(inxt) )
          rbig   = bdfmthr*rijco( indmm(iprev(i)),indmm(inxt) )
          rijm=0.d0
          do k=1,3
            rijm=rijm+(xxm(k,iprev(i))-xxn(k))**2
          enddo
          rijm=dsqrt(rijm)
!         Judge if two atoms are too close, if too close, just reject
!         if too close, try different angle first
          if (rijm.lt.rsmall) goto 30
!         counting atoms too far from this new atom
          if (rijm.gt.rbig) ifar = ifar + 1
        enddo
        if (ifar .eq. ikp) goto 20
!       Finally get one good atom
        ikp = ikp + 1
        iprev(ikp) = inxt
        do k=1,3
          xxm(k,inxt) = xxn(k)
        enddo
      enddo

 111  format(1x,2a)

      end subroutine ranclusp
