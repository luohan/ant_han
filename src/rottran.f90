      subroutine rottran(rotaxis,x,natoms)
!  this subroutine rotates the AG im to the desired coordinate system
!  given by the transformation matrix rotaxis(3,3)
!    Input: 
! x(3,mnat):     Coord. or momenta to be rotated
! natoms,rotaxis
!    output: 
! x
! xxtmp         Temporary storage of xxt
! 
      use param, only: mnat
      use c_output, only: 
      implicit none
      integer i,j,natoms
      double precision x(3,mnat),xxtmp(3),rotaxis(3,3)
      character*2 symb(mnat)
!
! Rotate the molecule to the new coordinate system by placing the principal 
! rotational axis with largest principal moment of inertial as z and followed
! by y and then x
! 
! Transform the old coodinate to the new coordinate:
! rotaxis * xxt 
      do i=1,natoms
        xxtmp(1) = rotaxis(1,1)*x(1,i) + rotaxis(1,2)*x(2,i) + &
                   rotaxis(1,3)*x(3,i)
        xxtmp(2) = rotaxis(2,1)*x(1,i) + rotaxis(2,2)*x(2,i) + &
                   rotaxis(2,3)*x(3,i)
        xxtmp(3) = rotaxis(3,1)*x(1,i) + rotaxis(3,2)*x(2,i) + &
                   rotaxis(3,3)*x(3,i)
! Get the new coordinate from the temporary storage xxt
        x(1,i) = xxtmp(1)
        x(2,i) = xxtmp(2)
        x(3,i) = xxtmp(3)
      enddo
 
      end subroutine rottran
