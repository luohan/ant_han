      subroutine verlet(xp,gvp,im)
!xxx      subroutine verlet(y,dydx,yp,nv)
!     Verlet algorithm

      use param, only: mnat, mnsurf
      use c_struct, only: mmm, xxm, natoms
      use c_sys, only: intflag, hstep
      use c_traj, only: ppm, istep, gv 

      implicit none
      integer nv,im
!      double precision y(mnyarray),dydx(mnyarray)
! xp:     coordinates at previous step (time - hstep)
! xt:     coordinates at next step (time + hstep)
!      double precision yp(mnyarray),yt
      double precision xp(3,mnat),xt,mr,gvp(3,mnat),mgrad,gvc(3,mnat)

      integer i,j,iat,jf
      double precision pemp(mnsurf),gcrep(mnsurf),gcimp(mnsurf)


      IF (intflag.eq.2) THEN
        do i=1,natoms
          do j=1,3
!   Integerate on coordinates
            xt = 2.d0*xxm(j,i) - xp(j,i) - hstep*hstep * gv(j,i)/mmm(i)
!   update momentum
            ppm(j,i) = mmm(i) * (xt - xp(j,i)) / (2.d0*hstep)
!   save current y as yp
            xp(j,i) = xxm(j,i)
!   save new x ( xt )
            xxm(j,i)  = xt
          enddo
        enddo

!      velocity Verlet algorithm
      ELSEIF (intflag.eq.3) THEN
        do i=1,natoms
          mr = 1.d0/mmm(i)
          do j=1,3
            xxm(j,i) = xxm(j,i) + ppm(j,i)*mr*hstep - 0.5d0 * hstep*hstep *  &
     &           gv(j,i)*mr
          enddo
        enddo

        do i=1,natoms
          gvc(1,i) = gv(1,i)
          gvc(2,i) = gv(2,i)
          gvc(3,i) = gv(3,i)
        enddo
        call getgrad(mgrad,im)

        do i=1,natoms
          do j=1,3
            ppm(j,i) = ppm(j,i) - 0.5d0 * (gv(j,i)+gvc(j,i)) * hstep
          enddo
        enddo

!      Beeman algorithm
      ELSEIF (intflag.eq.4) THEN
!      First step use velocity Verlet
      if (istep.eq.1) then
        do i=1,natoms
        mr = 1.d0/mmm(i)
        do j=1,3
        xxm(j,i) = xxm(j,i) + ppm(j,i)*mr*hstep - 0.5d0 * hstep*hstep * &
     &           gv(j,i)*mr
        enddo
        enddo
!     Current gradient, gradient of the previous step for istep>1
        do i=1,natoms
          gvp(1,i) = gv(1,i)
          gvp(2,i) = gv(2,i)
          gvp(3,i) = gv(3,i)
        enddo

!     Update gradient at new geometry
        call getgrad(mgrad,im)
!     Integrate on momentum
        do i=1,natoms
        do j=1,3
        ppm(j,i) = ppm(j,i) - 0.5d0 * (gv(j,i)+gvp(j,i)) * hstep
        enddo
        enddo
!      Other steps
      else
        do i=1,natoms
        mr = 1.d0/mmm(i)
        do j=1,3
        xxm(j,i) = xxm(j,i) + ppm(j,i)*mr*hstep - hstep*hstep *  &
     &           (4.d0*gv(j,i) - gvp(j,i) ) *mr/6.d0
        enddo
        enddo
!     Current gradient
        do i=1,natoms
          gvc(1,i) = gv(1,i)
          gvc(2,i) = gv(2,i)
          gvc(3,i) = gv(3,i)
        enddo

!     Update gradient at new geometry
        call getgrad(mgrad,im)
!     Integrate on momentum
        do i=1,natoms
        do j=1,3
        ppm(j,i) = ppm(j,i) - ( 2.d0*gv(j,i)+5.d0*gvc(j,i)-gvp(j,i) ) &
     &            * hstep/6.d0
        enddo
        enddo
!     Update gradient at previous step (i.e. gvc)
        do i=1,natoms
          gvp(1,i) = gvc(1,i)
          gvp(2,i) = gvc(2,i)
          gvp(3,i) = gvc(3,i)
        enddo

      endif

      ENDIF

!     Integerate on electronic variables (How to ?)

!     Nose-Hoover thermostat

      end subroutine verlet
