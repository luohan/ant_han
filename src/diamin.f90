      subroutine diamin(xmin,val,xj,im,mm,nsurf,ierror)

! Borrowed with modifications from NAT8.1.

! MNBRAK and BRENT are taken from Numerical Recipes.
! This routine is a combination of MNBRAK and BRENT.
! It minimizes the diatomic energy (obtained by calling
! DIAPOT) with respect to bond distance.  The value
! of the miminum-energy bond distance that is returned
! is XMIN, and the potential at the value is VAL.

! NOTE:  The code will automatically check for minima
! for all surfaces and for all three molecular
! arrangements.  If one of these combinations is
! unbound, the code may return an error message
! and non-physical values.  This is OK because
! the code does not use these values for molecular
! arrangements that are not energetically accessible.

      use param, only: mnat

      implicit none
      logical,intent(out) :: ierror
! mnbrak variables
      integer, intent(in) :: im,nsurf
      double precision, intent(in) :: xj,mm(mnat)
      double precision :: ax,bx,cx,fa,fb,fc,gold,glimit,tiny,guess
      parameter (gold=1.618034d0, glimit=100.0d0,tiny=1.d-20)
      double precision :: dum,fu,q,r,u,ulim

! brent variables
      integer :: itmax
      integer :: iter
      double precision :: tol,CGOLD,ZEPS
      parameter (itmax=100,CGOLD=.3819660,ZEPS=1.0e-10)
      double precision :: a,b,d,e,etemp,fv,fw,fx,p,   &
                          tol1,tol2,v,w,x,xm,val,xmin
      double precision :: rho2,dis2
      parameter (rho2 = 1.0d0, dis2 = 0.01d0)
      integer :: itmax2

      ierror = .false.
! hard-coded
      guess = 2.d0
      tol = 1.d-13

! ***********MNBRAK***********
! initial two points are guess, and 80% of guess
      ax = guess*0.8d0
      bx = guess

! func returns y(x), where y is the function to be minimized
      call diapot(ax,im,fa,xj,mm,nsurf)
      call diapot(bx,im,fb,xj,mm,nsurf)
!      write(6,*) xmin,val,xj,im,nsurf
!      write(6,*) ax*autoang,fa*autoev,im,nsurf

! make sure that fa > fb
      if (fb.gt.fa) then
        dum=ax
        ax=bx
        bx=dum
        dum=fb
        fb=fa
        fa=dum
      endif

! get third point by golden ration
      cx=bx+gold*(bx-ax)
      call diapot(cx,im,fc,xj,mm,nsurf)

! bracket minimum so that ax < bx < cx and fa > fb and fc > fb
      iter = 0
      do while( (fb.gt.fc)  .and. (iter .lt. itmax))
        iter = iter + 1
        r=(bx-ax)*(fb-fc)
        q=(bx-cx)*(fb-fa)
        u=bx-((bx-cx)*q-(bx-ax)*r)/    &
           (2.0d0*dsign(max(dabs(q-r),tiny),q-r))
        ulim=bx+glimit*(cx-bx)
        if ((bx-u)*(u-cx).gt.0.0d0) then
          call diapot(u,im,fu,xj,mm,nsurf)
          if (fu.lt.fc)then
            ax=bx
            fa=fb
            bx=u
            fb=fu
            exit
          elseif (fu.gt.fb) then
            cx=u
            fc=fu
            exit
          endif
          u=cx+gold*(cx-bx)
          call diapot(u,im,fu,xj,mm,nsurf)
        elseif ((cx-u)*(u-ulim).gt.0.0d0) then
          call diapot(u,im,fu,xj,mm,nsurf)
          if (fu.lt.fc) then
            bx=cx
            cx=u
            u=cx+gold*(cx-bx)
            fb=fc
            fc=fu
            call diapot(u,im,fu,xj,mm,nsurf)
          endif
        elseif ((u-ulim)*(ulim-cx).ge.0.0d0) then
          u=ulim
          call diapot(u,im,fu,xj,mm,nsurf)
        else
          u=cx+gold*(cx-bx)
          call diapot(u,im,fu,xj,mm,nsurf)
        endif
        ax=bx
        bx=cx
        cx=u
        fa=fb
        fb=fc
        fc=fu
        cycle
      enddo


! added by Han to avoid infinite bracketing, 10.0d0 is hard codded
      if ((iter .eq. itmax) .or. (ax .gt. 10.0d0)) then
         ax = 1.8d0
         bx = ax + dis2
         cx = bx + dis2*rho2
         call diapot(ax,im,fa,xj,mm,nsurf)
         call diapot(bx,im,fb,xj,mm,nsurf)
         call diapot(cx,im,fc,xj,mm,nsurf)
         iter = 0
         itmax2 = int(ceiling((10.0d0-ax)/dis2))
         do while( (fb .gt. fc ) .and. (iter .lt. itmax2))
           iter = iter + 1
           u = cx + rho2*(cx-bx)
           ax = bx
           fa = fb
           bx = cx
           fb = fc
           cx = u
           call diapot(cx,im,fc,xj,mm,nsurf)
         enddo
         if (iter  .eq. itmax) then
           write(6,*) 'Program fails at ''diamin.f90'' line 140'
           ! stop
           ierror = .true.
           return
         endif
       endif


! uses ax,bx,cx, and the tolerance tol
! ********** BRENT ***********

      a=min(ax,cx)
      b=max(ax,cx)
      if (cx.lt.ax) a=cx
      if (cx.lt.ax) b=ax

      v=bx
      w=v
      x=v
      e=0.d0
      call diapot(x,im,fx,xj,mm,nsurf)
      fv=fx
      fw=fx
      do iter=1,itmax
         xm=0.5d0*(a+b)
         tol1=tol*dabs(x)+ZEPS
         tol2=2.d0*tol1
         if(dabs(x-xm).le.(tol2-.5d0*(b-a))) go to 3
         if(dabs(e).gt.tol1) then
            r=(x-w)*(fx-fv)
            q=(x-v)*(fx-fw)
            p=(x-v)*q-(x-w)*r
            q=2.d0*(q-r)
            if(q.gt.0.d0) p=-p
            q=dabs(q)
            etemp=e
            e=d
            if(dabs(p).ge.dabs(.5d0*q*etemp).or.p.le.q*(a-x).or.   &
                 p.ge.q*(b-x)) go to 1
            d=p/q
            u=x+d
            if(u-a.lt.tol2 .or. b-u.lt.tol2) d=dsign(tol1,xm-x)
            go to 2
         endif
    1    if(x.ge.xm) then
            e=a-x
         else
            e=b-x
         endif
         d=CGOLD*e
    2    if(dabs(d).ge.tol1) then
            u=x+d
         else
            u=x+dsign(tol1,d)
         endif
         call diapot(u,im,fu,xj,mm,nsurf)
         if (fu.le.fx) then
            if (u.ge.x) then
               a=x
            else
               b=x
            endif
            v=w
            fv=fw
            w=x
            fw=fx
            x=u
            fx=fu
         else
            if(u.lt.x) then
               a=u
            else
               b=u
            endif
            if(fu.le.fw .or. w.eq.x) then
               v=w
               fv=fw
               w=u
               fw=fu
            else if(fu.le.fv .or. v.eq.x .or. v.eq.w) then
               v=u
               fv=fu
            endif
         endif
      enddo
      print *,"brent exceed maximum iterations"
      write(6,*) "Program fails at diamin line 225"
      ! stop
      ierror = .true.
      return
      x = 1.d10
      fx = 1.d10

    3 xmin=x
      val=fx

      END subroutine diamin
