! DESCRIPTION OF THE SUBROUTINE (References)
! This subroutine is based on the article 
! Drew A. McCormack and Kieran F. Lim, PCCP vol.1, 1-12 (1999)
! This article generalizes the TRAPZ (Trajectory projection onto ZPE orbit) to 
! some real molecular systems (Al3 and HCN).
! The whole procedure (7 steps):
! 1) Integration of Hamilton Equations (done outside TRAPZ).
! 2) Evaluation and diagonalize the projected force constant (must be done in 
!    normod2.f but should not be useful in normod.f since the initial point is
!    a minimum).
! 3) Checking the Sayvetz conditions (not done since J = 0).
! 4) Determination of P_k, D_k, and Emod(k). Which modes violate ZPE ? 
! 5) Mapping test : beta' and gamma.
! 6) Generating constrained normal mode momenta (vibration only).
! 7) Adding the rotational and translational motion if any.
! 8) Final scaling to cartesian coordinates. 
! 
! STRUCTURE OF THE SUBROUTINE (input/output)
! ithistraj : current trajectory index (input)
! lwrite(1000) : if lwrite(x) = .true. some data are printed in file fort.x 
!                (input)
! The main ouputs are the new momenta which are defined as common variables 
! in c_traj.f.
! 
! Last modification 07/10/2008
!
      subroutine trapz(ithistraj,lwrite2) 

      use param, only: mnat, mu, autocmi
      use c_output, only: minprinttrapz
      use c_struct, only: natoms, xxm, mmm
      use c_traj, only: prodzpe, nvers, freqlim, gv, ppm, itrapz

      implicit none
      integer i,j,jpos,natoms2,ndim,ithistraj
      integer iviol(3*mnat),nvibmods2
      double precision hh,tmp,ekin,ekinew,mtot,sumEmod,zpe2 
      double precision freq2(3*mnat),zpem2(3*mnat)
      double precision nmvec2(3*mnat,3*mnat),pms(3*mnat)
      double precision Emod(3*mnat),Pmod(3*mnat),fmod(3*mnat)
      double precision Dmod(3*mnat),Pmodp(3*mnat)
      logical lwrite2(1000),ltrapz

! AT THE BEGINNING WE JUST CARE ABOUT VIBRATIONAL MODES 
! (ie 3N-6 non-zero frequencies)
!
! natoms2 = natoms but natoms2 is only known in this subroutine 
! (flexibility for further use of this subroutine in a code without natoms as 
! a previously defined parameter)

      natoms2 = natoms
      ndim = 3*natoms2
 
! Step 2 : Calculation of the current projected hessian matrix: 
!         frequencies freq2 and normal modes nmvec2
! Frequencies are sorted as follows :
! 1) Positive frequencies (from the largest to the smallest),
! 2) Imaginary frequencies (with a sign "-" rather than i to distinguish them,
!    from the largest modulus to the smallest one), 
! 3) Zero frequencies (overall rotation and translation). 
! If a problem occurs during the calculation of the projected Hessian we 
! get out of the TRAPZ subroutine.

      ltrapz = .true. 
      call normod2(natoms2,nvibmods2,freq2,nmvec2,zpem2,zpe2,ltrapz)
      if (.not.ltrapz) then 
         return 
      else
         itrapz = itrapz + 1
      endif

! Step 4: Calculation of Q_k, P_k, D_k and Emod(k) in the normal mode basis by 
!         using: 
! (x - x_t) = L.Q ie t^L.(x - x_e) = Q but Q = 0 here
! p = L.P ie t^L.p = P.
! D_k = L_k.f(x) where f(x) is the matrix of gradients
! Emod(k) = 0.5*P_k^2 + 0.5*w_k^2*( Q_k + D_k/w_k )^2

      do i=1,3
         do j=1,natoms2
            jpos = 3*(j-1)+i
            pms(jpos) = dsqrt(mu/mmm(j))*ppm(i,j) 
         enddo
      enddo 

! dV(x~)/dx_i~ = dV(x)/dx_i -> mass-scaling needed for first derivatives.

      do i=1,3
         do j=1,natoms2
            jpos = 3*(j-1)+i
            fmod(jpos) = gv(i,j)*dsqrt(mu/mmm(j))
         enddo
      enddo

! nvibmods2 = 3N-6
      do i=1,nvibmods2
         Pmod(i) = 0.0d0
         Pmodp(i) = 0.0d0
         Dmod(i) = 0.0d0
         Emod(i) = 0.0d0
         iviol(i) = 0  
         do j=1,ndim
            Pmod(i) = Pmod(i) + nmvec2(j,i)*pms(j)
            Dmod(i) = Dmod(i) + nmvec2(j,i)*fmod(j)
         enddo

         if (freq2(i)*autocmi.gt.freqlim) then 
            Emod(i) = 0.5d0/mu*( Pmod(i)**2 + (Dmod(i)/freq2(i))**2 ) 
         endif 

!c Test
!         print *,'Eigenvector = ',(nmvec2(j,i),j=1,ndim)
!         print *,'Emod(',i,') = ',Emod(i)*autoev
!         print *,'Kinetic part = ',0.5d0/mu*Pmod(i)**2*autoev
!         print *,'Potential part =',0.5d0*(Dmod(i)/freq2(i))**2/mu &
!                                    *autoev

! Step 5: If Emod(k) >= zpem2(k) then P_k' = beta'*P_k 
!         If Emod(k) < zpem2(k) P_k' 
!         = sign(V_k)*dsqrt( w_k - w_k^2*( Q_k + D_k/w_k )^2 )
! Note that when the frequency is imaginary the ZPE does not exist and we 
! do not calculate the mode energy. The contribution of these modes is 
! introduced into the beta term that ensures total energy conservation.

          if(Emod(i).lt.zpem2(i).and.freq2(i)*autocmi.gt.freqlim)then
        
             iviol(i) = 1

             tmp = ( mu*freq2(i) - (Dmod(i)/freq2(i))**2 )
             if (tmp.gt.0) then 
                Pmodp(i) = sign(dsqrt(tmp),Pmod(i))
             else
                write(6,*) 'Error in the TRAPZ method'
                write(6,*) 'D_k is too large compared to w_k'
                stop
             endif

         endif 
       enddo 

! Selection of the TRAPZ version (Steps 6 to 8 in the subroutine called trapzcoeff) 
       if (nvers.eq.0) then 
! Meth 2: General TRAPZ method (NB: Meth 1 = calculation without TRAPZ)
         call trapzcoeff(natoms2,ndim,nvibmods2,iviol,Pmod,Pmodp,   &
                         nmvec2,pms,mmm,xxm,ppm)

       else

           sumEmod = 0.0d0
           do i=1,nvibmods2
              sumEmod = sumEmod + Emod(i)
           enddo
!           write(6,*) 'SumEmod (eV) = ',sumEmod*autoev
!           write(6,*)
! Meth 3: We compare the total instantaneous vibrational energy to the product ZPE
           if (nvers.eq.2.and.sumEmod.lt.prodzpe) then 
             call trapzcoeff(natoms2,ndim,nvibmods2,iviol,Pmod,Pmodp,  &
                             nmvec2,pms,mmm,xxm,ppm)

           elseif (nvers.eq.2.and.sumEmod.ge.prodzpe) then 
            if(.not.minprinttrapz) then
             write(6,*) 'mTRAPZ* is not applied: Energy > product ZPE'
             write(6,*) 
            endif    
! Meth 4: We compare the total instantaneous vibrational energy to the local ZPE of the system
           elseif (nvers.eq.1.and.sumEmod.lt.zpe2) then
             call trapzcoeff(natoms2,ndim,nvibmods2,iviol,Pmod,Pmodp,  &
                             nmvec2,pms,mmm,xxm,ppm)
           elseif (nvers.eq.1.and.sumEmod.ge.zpe2) then
            if(.not.minprinttrapz) then
             write(6,*) 'mTRAPZ is not applied: Energy > local ZPE'
             write(6,*)
            endif
           endif
             
       endif

!c Final printings (optional)
!
!       if (lwrite2(28)) then
!          write(28,111) ithistraj,istep,time*autofs,nvibmods2,  &
!     &                      ( freq2(i)*autocmi,i=1,ndim ),      &
!     &                      ( Emod(i)*autocmi,i=1,nvibmods2 )
!       endif


! Formats go here
 111   format(i5,i7,d16.8,2i5,100d16.8)

      end subroutine trapz

!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Subroutine that enforces the conservation of the COM linear momenta after TRAPZ. 

       subroutine rmvcom(nbrat,mass,xxm,ppm)

       use param, only: mnat
       use c_output, only: minprinttrapz
       implicit none

       integer i,j,nbrat
       double precision mtot,ekinold,ekinew
       double precision mass(mnat),pcm(3),rcm(3)
       double precision xxm(3,mnat),ppm(3,mnat)        


       mtot = 0.0d0  
       do i=1,nbrat
          mtot = mtot + mass(i) 
       enddo  

! Kinetic energy for old momenta
       ekinold = 0.0d0
       do i=1,3
          pcm(i) = 0.0d0
          do j=1,nbrat
             pcm(i) = pcm(i) + ppm(i,j)
             ekinold = ekinold + 0.5d0*ppm(i,j)**2/mass(j)
          enddo
       enddo

!       write(6,111) 'COM linear momenta after TRAPZ =',(pcm(i),i=1,3)

! Kinetic energy for new momenta
       ekinew = 0.0d0
       do i=1,3
          do j=1,nbrat
             ekinew = ekinew + 0.5d0*( ppm(i,j) -   &
                      mass(j)/mtot*pcm(i))**2/mass(j)
          enddo
       enddo        
       do i=1,3
          do j=1,nbrat
             ppm(i,j) = dsqrt(ekinold/ekinew)*      &
                        (ppm(i,j)-mass(j)/mtot*pcm(i))
          enddo 
       enddo

! Test on the center of mass 
       do i=1,3
          rcm(i) = 0.0d0
          pcm(i) = 0.0d0
          do j=1,nbrat
             rcm(i) = rcm(i) + mass(j)/mtot*xxm(i,j)
             pcm(i) = pcm(i) + ppm(i,j)
          enddo
       enddo
!       write(6,111) 'COM linear momenta after removal =',(pcm(i),i=1,3)
!       write(6,111) 'New COM location = ',(rcm(i),i=1,3)
       if(.not.minprinttrapz) then
       if (dabs(dsqrt(ekinold/ekinew)-1.0d0).gt.1.0d-6) write(6,111)   &
          'Scaling factor alpha = ',dsqrt(ekinold/ekinew)
       write(6,*)
       endif

! Formats go here
 111   format(a,3d16.8)

       return
       end

!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Subroutine that calculates coefficients for the TRAPZ method. 
!
! Last modified: 02/12/2008

       subroutine trapzcoeff(natoms2,ndim,nvibmods2,iviol,Pmod,Pmodp,  &
                             nmvec2,pms,mmm,xxm,ppm)

       use param, only: mnat, mu
       implicit none

       integer i,j,k,jpos,natoms2,ndim,nvibmods2,iviol(3*mnat)
       double precision sum1,sum2,sum3,sum4,beta,gamma
       double precision Pmod(3*mnat),Pmodp(3*mnat),pnew(3*mnat)
       double precision nmvec2(3*mnat,3*mnat),ppm(3,mnat)
       double precision mmm(mnat),xxm(3,mnat),pms(3*mnat)
 
       sum1 = 0.0d0
       sum2 = 0.0d0
       sum3 = 0.0d0  
       sum4 = 0.0d0
       do i=1,nvibmods2
          sum1 = sum1 + Pmod(i)**2
          if (iviol(i).eq.1) then
             sum2 = sum2 + Pmodp(i)**2
             sum3 = sum3 + Pmod(i)**2
          else
             sum4 = sum4 + Pmod(i)**2
          endif
       enddo

       beta = 0.0d0
       gamma = 0.0d0 
       if (sum2.le.sum1) then 
          beta = dsqrt( (sum1-sum2)/sum4 )

          do i=1,nvibmods2
             if (iviol(i).eq.0) Pmodp(i) = beta*Pmod(i)
          enddo
       else
          gamma =  dsqrt(sum1/sum3) 

          do i=1,nvibmods2
             if (iviol(i).eq.0) then
                Pmodp(i) = 0.0d0
             else  
                Pmodp(i) = gamma*Pmod(i)
             endif           
          enddo
       endif
    
! Step 6: vibrational contribution of the 3N new momenta, p' = L.P'

       do i=1,ndim
          pnew(i) = 0.0d0
          do j=1,nvibmods2
             pnew(i) = pnew(i) + nmvec2(i,j)*Pmodp(j)
          enddo
       enddo

! AT THE END WE INCLUDE ROTATION AND TRANSLATION
! Step 7: the translational and rotational motion are included
       
       do i=1,ndim
          do j=nvibmods2+1,ndim
             do k=1,ndim
                pnew(i) = pnew(i) + nmvec2(i,j)*nmvec2(k,j)*pms(k)
             enddo
          enddo
       enddo                  

! Step 8: We apply the final scaling to get cartesian coordinates
       do i=1,3
          do j=1,natoms2
             jpos = 3*(j-1) + i
             ppm(i,j) = dsqrt(mmm(j)/mu)*pnew(jpos) 
          enddo
       enddo

! We remove the total linear momentum but ensure total energy conservation. 

       call rmvcom(natoms2,mmm,xxm,ppm)

       end subroutine trapzcoeff
