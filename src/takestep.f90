      subroutine takestep(xp,gvp,y,yo,rhor,rhoro)

! Take a step with the integrator.
! nv: 
! nsurf:  current surface
!
      use param, only: mnat, mnsurf,  mnyarray
      use c_struct, only: natoms
      use c_sys, only: eps, hstep, intflag, methflag, nsurft
      use c_traj, only: inh, cim, cre, nsurf, time

      implicit none
      integer :: i,j,nv
      double precision :: y(mnyarray),yo(mnyarray) 
      double precision :: dydx(mnyarray),yscal(mnyarray)
      double precision :: hnext,hdid
! xp:     Coordinates at previous step (time - hstep)
! gvp:    Gradients at previous step (time - hstep)
      double precision :: xp(3,mnat),gvp(3,mnat)
!  RVM
      integer :: kels,imin,imax
      double precision :: rhor(mnsurf,mnsurf),rhoro(mnsurf,mnsurf)
      double precision :: rhoi(mnsurf,mnsurf)
      save hnext
! 
!  RVM define option for single-surface calculation
      if(nsurft.gt.1) then 
        nv = 6*natoms+3*nsurft
      elseif(nsurft.eq.1) then
        nv = 6*natoms
      endif
!  RVM define kels
      kels = nsurft*(nsurft-1)/2
!  RVM
      if (methflag.eq.4) nv = nv + 2*nsurft
! RVM   Add 3*kels variables for integration of bjk, bjk+ and bjk-
      if (intflag.eq.6) then
        if(methflag.ne.4) then
          nv = nv + 2*nsurft + 3*kels
        elseif(methflag.eq.4) then
          nv = nv + 3*kels
        endif
      endif
! RVM
      if (inh) nv=nv+2
! put xx and pp and their derivs into a 1-D array for integration
      if (intflag.eq.0.or.intflag.eq.1.or.intflag.eq.6) then
        call xptoy(y,dydx,nv,0)
      endif
! Debug information
!      if (idebug) then
!        write(6,*) 'Coord. before calling takestep'
!        do i=1,natoms
!          do j=1,3
!            xtmp(j,i) = xxm(j,i)
!          enddo
!          write(6,'(1x,3f15.6)') ( xxm(j,i)*autoang,j=1,3 )
!        enddo
!      endif

! Bulirsch-Stoer
      if (intflag.eq.0) then
        do i=1,mnyarray
          yscal(i)=1.d0
        enddo 

        call bsstep(y,dydx,nv,time,hstep,eps,yscal,hdid,hnext,nsurf)
        hstep=hnext
 
! RVM Bulirsch-Stoer with Mike Hack's integration method
      elseif (intflag.eq.6) then

        do i=1,mnyarray
          yscal(i)=1.d0
        enddo

!    Store y and rhor at previous step for calculation of hopping probs
        do i=1,mnyarray
          yo(i)=y(i)
        enddo

!  RVM
        if(methflag.ne.4) then
          call getrho(cre,cim,rhor,rhoi,nsurft)

          do i=1,nsurft
            rhoro(i,i) = rhor(i,i)
          enddo
        elseif(methflag.eq.4) then
          call getrhocsdm(cre,cim,rhor,rhoi,nsurft)

          do i=1,nsurft
            rhoro(i,i) = rhor(i,i)
          enddo
        endif        
!  RVM 
        
        call bsstep(y,dydx,nv,time,hstep,eps,yscal,hdid,hnext,nsurf)
        hstep=hnext

! Runge-Kutta 4th order
       elseif (intflag.eq.1) then
         call rk4(y,dydx,nv,0)
         time=time+hstep

! Verlet type: Simple-, Velocity-, and Beeman
       elseif (intflag.ge.2.and.intflag.le.4) then
         call verlet(xp,gvp,0)
         time=time+hstep

! Liouville approach to Velocity Verlet algorithm
       elseif (intflag.eq.5) then
         call liouv(0)
         time=time+hstep
!
! Other
       else
         write(6,*)'Dont know intflag = ',intflag
         stop
      endif
!
! transform 1-D array into xx and pp, no need to transfer for Verlet
! RVM
      if (intflag.eq.0.or.intflag.eq.1.or.intflag.eq.6) then 
        call xptoy(y,dydx,nv,1)
      endif 
! Obtain new rhor
      if(intflag.eq.6) then 
        call getrho(cre,cim,rhor,rhoi,nsurft)
      endif
! RVM
! Debug information
!      if (idebug) then
!C       write(6,*) 'Difference in xx after calling takestep'
!       write(6,*) 'New xx after calling takestep'
!       do i=1,natoms
!         write(6,'(1x,3f15.6)') ( (xxm(j,i)-xtmp(j,i))*autoang,j=1,3 )
!          write(6,'(1x,3f15.6)') ( xxm(j,i)*autoang,j=1,3 )
!       enddo
!       write(6,*) 'ppm after calling takestep'
!       do i=1,natoms
!         write(6,'(1x,3f15.6)') ( ppm(j,i),j=1,3 )
!       enddo
!      endif
!
      end subroutine takestep
