      subroutine rancluns
!
! Generate a random cluster of atoms with real randomly produced
! structures, with minimum and maximum minimum-atom-atom distances
! of RSMALL and RBIG, respectively.
!
! Basis idea: 
!  1) place an atom at the origin, as first atom
!  2) randomly generate an atom which is within the first atom between
!     rsmall and rbig
!  3) randomly pick one atom from the previously generated good
!     atoms (at least 2)
!  4) randomly generated the coordinate using the method of step 2)
!  5) judge the distance between this atom and the rest of atoms, if
!     find one distance is smaller than rsmall, reject. No need to judge
!     if its distance is larger than rbig to all other atoms because the
!     method used in step 2) automatically guarrented that this atom is
!     within the range rsmall and rbig to the atom randomly selected to
!     generate it.
!  6) loop through 3) to 4) untill all atoms have been done
!   INPUT: natoms
! xxm:    coordinates for the molecule (AG)
! indmm:  Atom indices in periodic table
! rsmal:  the smallest distance between two atoms allowed
! rbig:   the largest  distance between two atoms allowed
! ran:    random number
! iclu:   The times tried to generated a whole set of coordinates   
! ikp:  couting the atoms kept 
! irej:   0: keep, 1: reject
! imother:   the atom randomly selected to generate another atom
! Using spheric coordinates to generate atoms and then transfer back to
! cartesian coordinate
! ri: the distance of an atom to its mother
! theta: theta angle (vector r to z)
! phi: phi angle (projection of vector r on xy plane to x)
! rijm:      distance between two atoms
      use param, only: mnat, pi
      use c_struct, only: xxm, natoms, rijco, bdfmthr, indmm
      implicit none
      integer iclu,ikp,itry,i,j,k,imother,ipick,ison,i1st,i2nd,  &
        iprev(mnat),irest(mnat),itmp(mnat),nrest,itry1
      double precision rijm,rsmall,rbig,rixy,xxn(3),ran(5)
      double precision ri,theta,phi
!
      iclu   = 0
 10   continue
      do i=1,natoms
        irest(i)=i
      enddo
!
      iclu = iclu + 1
      if (iclu.gt.20) then
        write(6,111) 'Tried too many times to generate a real random', &
                     ' structure ... quitting'
        stop
      endif
!     Randomly pick an atom and place it at the origin
!      ran(1) = sprng(rng_stream) RMP14
      call get_rng(ran(1)) 
      i1st = 1+int( ran(1)*real(natoms-1) )
      iprev(1) = i1st
!     Randomly pick 2nd atom and place it on z axis
      i2nd = i1st
      do while(i2nd.eq.i1st)
        call get_rng(ran(1)) 
        i2nd = 1+int( ran(1)*real(natoms-1) )
      enddo
      iprev(2) = i2nd
!     Coord. of the 1st and 2nd atom
      do k=1,3
        xxm(k,i1st) = 0.d0
        xxm(k,i2nd) = 0.d0
      enddo
      rsmall = 0.8d0*rijco( indmm(i1st),indmm(i2nd) )
      rbig   = bdfmthr*rijco( indmm(i1st),indmm(i2nd) )
      call get_rng(ran(1)) 
      xxm(3,i2nd) = rsmall + ran(1)*(rbig-rsmall)
!       update irest
      k=0
      do i=1,natoms
        if (i.ne.i1st .and. i.ne.i2nd) then
          k=k+1
          irest(k)=i
        endif
      enddo
!
! generate natoms points 
      itry = 0
      ikp = 2
      do while (ikp .lt. natoms)
!
        nrest=natoms-ikp
        call get_rng(ran(1))
!       randomly pick one atom from the rest atoms
        ipick= 1+int( ran(1)*real(nrest-1) )
        ison = irest(ipick)
!       Transfer the old irest index to temp storage
        do i=1,nrest
          itmp(i)=irest(i)
        enddo
!
        itry=0
 20     itry = itry + 1
        if (itry.eq.100000) then
          write(6,*) 'Bad packing.  Start again.'
          goto 10
        endif
!
!       randomly pick one atom from the previously generated atoms
        call get_rng(ran(1))
        ipick= 1+int( ran(1)*real(ikp-1) )
        imother = iprev(ipick)
!      write(6,*) 'i1st,i2nd',i1st,i2nd
!      write(6,*) 'nrest,ikp,imother,ison',nrest,ikp,imother,ison
!       generate an atom within the rsmall and rbig reach of its mother
        call get_rng(ran(1)) 
        rsmall = 0.8d0*rijco( indmm(imother),indmm(ison) )
        rbig   = bdfmthr*rijco( indmm(imother),indmm(ison) )
        ri     = rsmall +  ran(1)*(rbig-rsmall)

        itry1=0
 30     itry1=itry1+1
        if (itry1.eq.100) goto 20
        do i=2,3
          call get_rng(ran(i)) 
        enddo
        theta  = dacos(  ran(2) )
        phi    = ran(3)*(2.0d0*pi)
        rixy   = ri*dsin( theta )
!       transfer the temporary spheric coordinates to cartesian
!       x
        xxn(1) = xxm(1,imother) + rixy*dcos(phi)
!       y
        xxn(2) = xxm(2,imother) + rixy*dsin(phi)
!       z
        xxn(3) = xxm(3,imother) + ri*ran(2)
!       rijm: distance between the two atoms 
        rijm  = 0.0d0
        do i=1,ikp
          rsmall = 0.8d0*rijco( indmm(iprev(i)),indmm(ison) )
          if (iprev(i).ne.imother) then
            do k=1,3
              rijm = rijm + (xxm(k,iprev(i))-xxn(k) )**2
            enddo
            rijm = dsqrt(rijm)
!         Judge if two atoms are too close, if too close, just reject
            if (rijm.lt.rsmall) goto 30
          endif
        enddo
!       Find one good atom
!       Update information
        ikp = ikp + 1
        iprev(ikp) = ison
        do k=1,3
          xxm(k,ison) = xxn(k)
        enddo
!       update irest
        k=0
        do i=1,nrest
          if (itmp(i).ne.ison) then
            k=k+1
            irest(k)=itmp(i)
          endif
        enddo
      enddo
!     Done

 111  format(1x,2a)

      end subroutine rancluns
