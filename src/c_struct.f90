module c_struct
 
! module data for structures
! XXI(3,mnat)     : Initial values of XX for a trajectory
! XX0(3,mnat)     : Initial values of XX read from input
! NH(3,2)         : variables used for Nose-Hoover thermostat
!                   NH(1,1),NH(1,2): Mass of the two variables
!                   NH(2,1),NH(2,2): Position of the two variables
!                   NH(3,1),NH(3,2): Momentum of the two variables
! XXM(3,mnat)     : Working storage of XX
! XXF(3,mnat)     : Final XX
! cell(6)    Cell of the simulation. 1-3: a,b,c; 4-6: alpha,beta,gamma
! cvec(3,3)  Cell vector for triclinic, cubic, cuboid cell
! icell      1: Cubic; 2: Cuboid; 3: Other than cubic and cuboid
! invcvec(3,3)  Inverse of cvec
! rij(mnat,mnat)         : Nuclear distance matrix
! rij2(mnat,mnat)        : Square of nuclear distance matrix
! arij(mnat,mnat)        : Nuclear distance matrix
! arij2(mnat,mnat)       : Square of nuclear distance matrix
! bndindi(mnbond,mnat)   : Initial bonding information for atoms
! nbondi(mnat)           : Initial coordinate number of atoms
! bndind(mnbond,mnat)    : Bonding information, atom indices of atoms
!                          bonded to an atom
! bndmatr(mnat,mnat)     : Bonding information matrix
! nbond(mnat)            : Coord. Number of an atom
! fragind(mnat,mnfrag)   : Atom indices of a fragment
! indfrdep(mnfrag)       : Store the leaving fragmentations
! comp(3,mnfrag)         : Store 2* the projection of CoM momenta onto CoM
!                          Coord of the leaving fragments
! mmoffr(mnfrag)         : Mass of the fragments
! natinfrag(mnfrag)      : Atom numbers in a fragment
! nfrag                  : Number of fragments
! mm0(mnat)      mass of atom j (input in amu, converted immediately to au)
! mmm(mnat)      Working storage of mm
! nmol           # of atom groups
! nat            # of total atoms (for all atom groups)
! natom(mnmol)   # of atoms in AG im
! iatom(mnmol)   index of first atom in AG im
! icharge(im)    Charge of the AG im, integer
! charge(mnat)   Charge of the atoms in the AG
! infree         local storage of nfree
! imulti(im)     Multiplicity of the AG im, integer
! symbol0(j)      atomic symbol of atom j
! symbolm(j)     Working storage of symbol
! indatom(j)     atom indices in the periodic table (nucleur charge)
! nattype(mnmol+1,121)   Number of the same type of atoms
!                        1 - 111 for real type of atoms, and 112 - 121
!                        are reserved for model atoms like in MCH system, M
!                        is not reconganized as any real element
! indmm(j)       Working storage of indatom
! nvibmods(mnmol) Store the number of vibrational modes
! nrotmods(mnmol) Store the number of rotational freedom
! linearmol(im)   0: An atom; 1: Linear; 2: Non-linear
! iflinear        Working storage of linearmol
! rijco:     Covalent bond distance of different types of bonds,
!            calculated by covalent atomic radii in atomdata
! bdfmthr:        Threshold of forming a bond is bdfmthr*rijco
! bdbrthr:        Threshold of breaking a bond is bdbrthr*rijco
! rotaxis(j,i)    Principal rotational axes of AG im, j=x,y,z, i=1,3,
!                  in an ascending order of principal moment of inertia
! mmofag(mnmol+1)  Total mass of AG im, the whole AG is at mnmol+1
! momiprin(3)      Principal moment of inertial
! xunit(3,50)      geometry of building block
! natunit          number of atoms in building block
! nunit            whole AG is composed of nunit building block
!
      use param, only: mnat, mnfrag, mnmol, mnbond
      integer :: bndind(mnbond,mnat),nbond(mnat),bndmatr(mnat,mnat),  &
                 fragind(mnat,mnfrag),natinfrag(mnfrag),nfrag,indfrdep(mnfrag), &
                 bndindi(mnbond,mnat),nbondi(mnat), &
                 nmol,nat,natom(mnmol),iatom(mnmol),indatom(mnat),indmm(mnat),  &
                 icharge(mnmol+1),imulti(mnmol+1),linearmol(mnmol+1),iflinear,  &
                 nvibmods(mnmol+1),nrotmods(mnmol+1),natoms,  &
                 nattype(mnmol+1,121),infree,icell,nunit,natunit
      double precision :: xxi(3,mnat),xxm(3,mnat),xx0(3,mnat),mm0(mnat), &
                          mmm(mnat),rij(mnat,mnat),rij2(mnat,mnat),rijco(111,111), &
                          arij(mnat,mnat),arij2(mnat,mnat),xxf(3,mnat),nh(3,2),    &
                          charge(mnat),cvec(3,3),invcvec(3,3),cell(6),comp(3,mnfrag),  &
                          mmoffr(mnfrag),compfr(3,mnfrag),xunit(3,50)
      double precision :: rotaxis(3,3),momiprin(3),mmofag(mnmol+1)
      double precision :: bdfmthr,bdbrthr
      character*2 :: symbol0(mnat),symbolm(mnat)
      logical ldiat ! Flag to compute the diatomic potential, RMP14

end module c_struct
