      subroutine angmom(iflinear,bigjm,ppt,xxm,natoms,bigjtm)
!
! computes angular momentum around the origin using the position XX and momentum PP
!   Input:
! ppt(3,mnat):  Momenta
! xxm, ppt, natoms, iflinear
! iflinear: 0: an atom; 1: Linear; 2: Non-linear
!   Output:
! bigjm,bigjtm

      use param, only: mnat, autoang
      use c_output, only: idebug

      implicit none
      integer :: natoms
      integer :: iflinear
      double precision :: xxm(3,mnat),ppt(3,mnat)
      double precision :: bigjtm,bigjm(3)
! local
      integer :: i,j
      double precision :: maxbigj

! zero
      bigjm(1)=0.d0
      bigjm(2)=0.d0
      bigjm(3)=0.d0
      if (idebug) then
        write(6,*) 'Momenta in angmom'
        do i=1,natoms
          write(6,107) i,(ppt(j,i),j=1,3)
        enddo
        write(6,*) 'Structure in angmom'
        do i=1,natoms
          write(6,107) i,(xxm(j,i)*autoang,j=1,3)
        enddo
      endif

! bigjm = xx cross pp (J = R x P)
      do i=1,natoms
       bigjm(1)=bigjm(1)+(xxm(2,i)*ppt(3,i)-xxm(3,i)*ppt(2,i))
       bigjm(2)=bigjm(2)-(xxm(1,i)*ppt(3,i)-xxm(3,i)*ppt(1,i))
       bigjm(3)=bigjm(3)+(xxm(1,i)*ppt(2,i)-xxm(2,i)*ppt(1,i))
      enddo
!
! bigjm(i) is the ith component of the total angular momentum
! bigjtm is the magnitide of the total angular momentum
! For an atom, bigjm(i) all = 0.0, bigjtm = 0.0
! For linear molecule, one bigjm = 0.0, bigjtm = the absolute of one 
! of none zero bigjm
      maxbigj=0.0d0
      bigjtm=0.d0
      do i=1,3
        bigjtm = bigjtm + bigjm(i)**2
        maxbigj=max( maxbigj,dabs( bigjm(i) ) )
      enddo
      if (iflinear.eq.1) then
          bigjtm = maxbigj
        else
          bigjtm = dsqrt(bigjtm)
      endif
      if (idebug) then
        write(6,*) 'Angular Momentum:'
        write(6,106) 'Total','1','2','3'
        write(6,108) bigjtm,(bigjm(i),i=1,3)
        write(6,*)
      endif
!
 106  format(1x,4a14)
 107  format(1x,i5,4f14.5)
 108  format(1x,4f14.5)
!
      end subroutine angmom
