      module tpotmod_nt
! this module stores parameters needed to obtain function results for the Brent
! zero finder used to obtain the final turning point position
      use param, only: mnat, mnsurf
      integer, save :: if1, itsearch(2)
      double precision, save :: cre_stash(2*mnsurf), cim_stash(2*mnsurf), phase_stash(mnsurf), &
                                gcre_stash(2*mnsurf), gcim_stash(2*mnsurf)
      double precision, save :: vturn, xlast(3,mnat), xnow(3,mnat), drrpass(3*mnat), &
                                din(2), scale, x_base(3,mnat), veloc_save
      end module tpotmod_nt

! This subroutine is to calculate the action integral for nonadiabatic tunneling
! J. Zheng (2014), corrected 2016

      subroutine actionint_nt(ithistraj,xturn,pturn,itint,drr,  &
                 nstr,nbend,turnd,lbranch,im,wght,lchgw,xnew,pnew,  &
                 info,ttunn,repflag,nsurft)

      use tpotmod_nt
      use param, only: mnat, mnsurf, mnyarray, pi, autoev, mu, autoang
      use c_struct, only: nat, mmm, symbol0
      use c_traj, only: cre, phase, cim, gcre, gcim, pe, pema, pemd, eta, pem
      use c_output, only: time_brent, time_pgen
      use c_common, only: td, veloc

      implicit none
      integer :: i,i2,j,k,ithistraj,nstr,nbend,im,nstep,jj
      integer :: itint(2),maxsteps,repflag,nsurft
! variable info is used to identify where this subroutine returns and 
! it is for debugging purposes
      integer :: info, iflag, n_waypts
      real*8 :: wght,dv,v0,lamda,dxi,dlen_iso,tstep,pepre,petmp,tmp
      real*8 :: probt,gamma,lamda2,pam,ram,dvpre,ttunn,dvsave
      real*8 :: direction,dmag,theta,len_iso,len_bh, turn2, time1, time2
      real*8 :: xturn(3,mnat),pturn(3,mnat),turnd(3,mnat),xprev(3,mnat)
      real*8 :: xx(3,mnat),xnew(3,mnat),pnew(3,mnat),drr(3*mnat)
      real*8 :: cre_save(2*mnsurf),cim_save(2*mnsurf),phase_save(mnsurf)
      real*8 :: gcre_save(2*mnsurf),gcim_save(2*mnsurf)
      real*8 :: pe_ini,pe_end,pe_bh,pema_ini(mnsurf),pema_bh(mnsurf)
      real*8 :: pema_end(mnsurf),pemd_ini(mnsurf,mnsurf)
      real*8 :: pemd_bh(mnsurf,mnsurf),pemd_end(mnsurf,mnsurf)
      real*8 :: rhor_ini(mnsurf,mnsurf),rhor_bh(mnsurf,mnsurf)
      real*8 :: rhoi_ini(mnsurf,mnsurf),rhoi_bh(mnsurf,mnsurf)
      real*8 :: rhor_end(mnsurf,mnsurf),x_bh(3,mnat)
      real*8 :: rhoi_end(mnsurf,mnsurf)                           
      real*8 :: rhor(mnsurf,mnsurf),rhoi(mnsurf,mnsurf)        
!     real*8 :: denr_ini(mnsurf,mnsurf),deni_ini(mnsurf,mnsurf) ! diabatic density
!     real*8 :: denr_bh(mnsurf,mnsurf),deni_bh(mnsurf,mnsurf)   ! diabatic density
!     real*8 :: denr_end(mnsurf,mnsurf),deni_end(mnsurf,mnsurf) ! diabatic density
      double precision :: xtemp(3,mnat), ptemp(3,mnat), ptemp2(3,mnat)
      real*8 :: delphase(mnsurf,mnsurf), len_correct, xtemp2(3,mnat), xtemp3(3,mnat), &
                dv_old, pe_tmp
      ! real*8 :: td(3,mnat) ! tunneling direction
      double precision, allocatable :: waypt(:,:,:)
      double precision, external :: brent, tpot_nt
      logical :: lbranch,lchgw,langle,lterm,quit,lhop
!
! local storage for integration 
      real*8 :: y(mnyarray),dydx(mnyarray)
! 1 to NSURFT   -> PHASE(k)
! NSURFT + 1 to 2*NSURFT -> Real part of the electronic variables CRE(k)  
! 2*NSURFT+1 to 3*NSURFT -> Imaginary part of the electronic variables CIM(k)
!
! save the electronic coefficients before start evolution along tunneling path
      if(.not.allocated(waypt)) then
        allocate(waypt(3,mnat,500) )
      endif

      cre_save(:) = cre(:)
      cim_save(:) = cim(:)
      phase_save(:) = phase(:)
      gcre_save(:)  = gcre(:)
      gcim_save(:)  = gcim(:)
! save the potential of the turning point to v0
      v0 = pe 
      ttunn = 0d0
      lbranch = .false.
      lchgw   = .false.    
! save initial PES information
      call getrho(cre,cim,rhor,rhoi,3)
      pe_ini = pe
!     rhor_ini(:,:) = rhor(:,:)
!     rhoi_ini(:,:) = rhoi(:,:)
! Include phase in calculating real part of density matrix
      do i = 1, nsurft
        do j = 1, nsurft
          tmp = phase(j)-phase(i)
          rhor_ini(i,j) = rhor(i,j)*dcos(tmp) + rhoi(i,j)*dsin(tmp)
        enddo
      enddo
      pema_ini(:)   = pema(:)
      pemd_ini(:,:) = pemd(:,:)
      pe_bh         = pe
      rhor_bh(:,:) = rhor_ini(:,:)
!     rhoi_bh(:,:) = rhoi(:,:)
      pema_bh(:)   = pema(:)
      pemd_bh(:,:) = pemd(:,:)
!     call adtod(1,1,denr_ini,deni_ini)
      
      if(itint(2).ne.0) then
        call comparint(itint,xturn,lterm)
        if(.not.lterm)  then
            info = 1
            return
        endif
      endif
!
! Search the ending point of tunneling path along an internal coordinate
!
      if((itint(1).le.nstr.and.itint(2).eq.0).or.itint(2).ne.0) then
        dxi    =  1d-3 ! in the unit of a.u. for length
        langle = .false.
        maxsteps = 20000
      else
        dxi    = 0.1d0*PI/180d0 ! in radians for angles 
        langle = .true.
        maxsteps = 1800
      endif
! determing the direction of tunneling path
      direction = 1d0
      din(1) = dxi*direction*drr(itint(1))
      if(itint(2).ne.0) din(2) = dxi*direction*drr(itint(2))
      call intcart(xturn,xx,nat,mmm,din,itint,1,lterm)
      if(lterm) stop 'intcart didn''t converge'
      xprev(:,:) = xturn(:,:)
      td(:,:) = xx(:,:) - xprev(:,:)
! change xx to isoinertial coordinate
      call cartoiso(xx,nat,mmm,1)
      call cartoiso(xprev,nat,mmm,1)
      call length(nat,xx,xprev,dlen_iso)
! change xx back to Cartesain
      call cartoiso(xx,nat,mmm,2) 

      call getgrad2(xx,petmp)
      dv = pe - v0
      dvsave = dv
      pepre = pe
!     write(6,*) 'dv =', dv
      if(dv.lt.0d0.and.itint(1).gt.nstr+nbend.and.itint(2).eq.0) then
         direction = -1d0
         din(1) = dxi*direction*drr(itint(1))
         if(itint(2).ne.0) din(2) = dxi*direction*drr(itint(2))
         call intcart(xturn,xx,nat,mmm,din,itint,1,lterm)
         if(lterm) stop 'failed to converge in call to intcart'
!xxx         if(lterm) then
!xxx           call recoverc(cre_save,cim_save,phase_save,gcre_save,gcim_save)
!xxx           info =3
!xxx           return
!xxx         endif
         xprev(:,:) = xturn(:,:)
         td(:,:) = xx(:,:) - xprev(:,:)
! change xx to isoinertial coordinate
         call cartoiso(xx,nat,mmm,1)
         call cartoiso(xprev,nat,mmm,1)
         call length(nat,xx,xprev,dlen_iso)
! change xx back to Cartesians
         call cartoiso(xx,nat,mmm,2)
         call getgrad2(xx,petmp)
         
         dv = pe - v0
         if(dv.lt.0d0) then
           call recoverc(cre_save,cim_save,phase_save,gcre_save,gcim_save)
           info =4
           return
         endif
      elseif(dv.lt.0d0.and.itint(2).ne.0) then
         call recoverc(cre_save,cim_save,phase_save,gcre_save,gcim_save)
         info =5
         return
      elseif(dv.lt.0d0.and.itint(1).le.nstr+nbend.and.itint(2).eq.0)then
         call recoverc(cre_save,cim_save,phase_save,gcre_save,gcim_save)
         info =6
         return
      endif
!  
! The time step corresponding to displacement of length
!
      veloc  = sqrt(2d0*dv)
      tstep  = dlen_iso/veloc
!     write(6,*) "time step (fs)",tstep*autofs
!
! calculate the electronic coefficients for the next step
      call takestep_nt(xturn,tstep)
      call getgrad2(xx,petmp)
      dv = pe -v0
      if (dv.lt.0d0) then
         call recoverc(cre_save,cim_save,phase_save,gcre_save,gcim_save)
         info = 14
         return
      endif
      nstep = 1
      len_iso = dlen_iso
      theta = sqrt(dv)
      ttunn = ttunn + tstep
!
      quit = .false.
!
!  Trapezoidal rule integral for theta
!--------------------------------------------
      n_waypts=1
      waypt(:,:,1)=xturn(:,:)

      jj=0
      do while (.not.quit) 
        jj=jj+1
        xprev(:,:) = xx(:,:)
        call intcart(xprev,xx,nat,mmm,din,itint,1,lterm)
        if(lterm) stop 'failure in intcart'
!xxx         call recoverc(cre_save,cim_save,phase_save,gcre_save,gcim_save)
!xxx          info =7
!xxx         return
!xxx        endif
        td(:,:) = xx(:,:) - xprev(:,:)
! change xx to isoinertial coordinate
        call cartoiso(xx,nat,mmm,1)
        call cartoiso(xprev,nat,mmm,1)
        call length(nat,xx,xprev,dlen_iso)
! change xx back to Cartesians
        call cartoiso(xx,nat,mmm,2)
        if(dlen_iso.eq.0d0) stop 'dlen_iso=0'
! 
! time step corresponding to displacement of length
        veloc  = sqrt(2d0*(pepre-v0))
        tstep  = dlen_iso/veloc
!       write(6,*) "time step (fs), dlen",tstep*autofs,dlen_iso
!
! calculate the electronic coefficients for the next step

        cre_stash(:)=cre(:)
        cim_stash(:)=cim(:)
        phase_stash(:)=phase(:)
        gcre_stash(:)=gcre(:)
        gcim_stash(:)=gcim(:)

        call takestep_nt(xx,tstep)
        call getgrad2(xx,petmp)
        dv_old=dv
        dv = pe -v0
        pepre = pe
        len_iso = len_iso + dlen_iso

        if(mod(jj,50).eq.0)then
          n_waypts=n_waypts+1
          if(n_waypts.gt.500)then
            stop 'too many waypts'
          endif
          waypt(:,:,n_waypts)=xx(:,:)
        endif
!
! judge if quit at this point      
        if(dv.lt.0d0) then
           quit = .true.

           call cartoiso(xprev,nat,mmm,2) !convert xprev back to cartesians
           xlast(:,:)=xprev(:,:)
           if1=0
           drrpass(:)=drr(:)
           itsearch(:)=itint(:)
           scale=dxi*direction
           vturn=v0
           veloc_save=veloc
           call cpu_time(time1)
           turn2=brent(0d0,1d0,tpot_nt,1d-10,dv_old,dv)

           call cpu_time(time2)
           time_brent=time_brent+time2-time1
           pe=tpot_nt(turn2)+vturn ! gets final set of coordinates and energy
           xx(:,:)=xnow(:,:) ! store the coordinates of the outer turning point
           xtemp2(:,:)=xnow(:,:)
           xtemp3(:,:)=xprev(:,:)
           call cartoiso(xtemp2,nat,mmm,1) ! get coordinates of outer turning point in isoinertial coordinates
           call cartoiso(xtemp3,nat,mmm,1) 
           call length(nat,xtemp2,xtemp3,len_correct) ! calculate dist.  between turning pt. and last point
           len_iso=len_iso-dlen_iso +len_correct


        elseif(dv.gt.0d0.and.nstep.gt.maxsteps) then
           quit = .true.
           call recoverc(cre_save,cim_save,phase_save,gcre_save,gcim_save)
           info =8
           return
        else 
           theta = theta + 2d0*sqrt(dv)
           ttunn = ttunn + tstep
           dvpre = dv
           nstep = nstep + 1
           call getrho(cre,cim,rhor,rhoi,3)
!
!======================================================================
! monitor energy changes every 600 steps; if energy is not changed we assume
! this path cannot go out to a classically allowed region, and we stop trying 
!
           if(mod(nstep,600) .eq. 0) then
             if((dv-dvsave)*autoev.lt.1d-1.and.(dv-dvsave).gt.0d0) then
               quit = .true.
               call recoverc(cre_save,cim_save,phase_save,gcre_save,gcim_save)
               return
             endif
             dvsave = dv
           endif

!========= end of debugging =======================================
          if(pe.gt.pe_bh) then
             pe_bh = pe
! Include phase in calculating real part of density matrix
             do i = 1, nsurft
               do j = 1, nsurft
                 tmp = phase(j)-phase(i)
                 rhor_bh(i,j) = rhor(i,j)*dcos(tmp) + rhoi(i,j)*dsin(tmp)
               enddo
             enddo
!            rhor_bh(:,:) = rhor(:,:)
!            rhoi_bh(:,:) = rhoi(:,:)
             pema_bh(:)   = pema(:)
             pemd_bh(:,:) = pemd(:,:)            
             x_bh(:,:)    = xx(:,:)
             len_bh       = len_iso
!            call adtod(1,1,denr_bh,deni_bh)
          endif

        endif
      enddo ! end do while
! save end of tunneling path PES information
      call getrho(cre,cim,rhor,rhoi,3)
      pe_end = pe
! Include phase in calculating real part of density matrix
      do i = 1, nsurft
        do j = 1, nsurft
          tmp = phase(j)-phase(i)
          rhor_end(i,j) = rhor(i,j)*dcos(tmp) + rhoi(i,j)*dsin(tmp)
        enddo
      enddo
!xxx     rhor_end(:,:) = rhor(:,:)
!xxx     rhoi_end(:,:) = rhoi(:,:)
      pema_end(:)   = pema(:)
      pemd_end(:,:) = pemd(:,:)
!     call adtod(1,1,denr_end,deni_end)
!
      theta = theta*len_iso*sqrt(2d0*mu)/2d0/dble(nstep)
      xnew(:,:) = xx(:,:)

! Army ant algorithm for branching
!
! generate a random number for accpeting tunneling probability
! and initilize variables
      call get_rng(lamda) 
      probt = exp(-2d0*theta)
      if (probt.gt.0.95d0) then 
        write(6,*)'Suprisingly large tunneling prob.=',probt
!xxx         call recoverc(cre_save,cim_save,phase_save,gcre_save,gcim_save)
!xxx           info =9
!xxx           return
      endif
      gamma = max(eta,probt)
        if(gamma .gt. lamda) then 
          call get_rng(lamda2) 
          lchgw = .true.
          if(lamda2 .gt. 0.5)  then
             lbranch = .true.
!xxx             wght = probt/gamma
             wght = 2d0*probt/gamma
!            call comgen(xnew,mmm,nat,com,mtotal)
!            do i = 1, nat
! amplitude of p and r vectors 
!               pam =sqrt(pturn(1,i)**2+pturn(2,i)**2+pturn(3,i)**2)
!               ram =sqrt(xnew(1,i)**2+xnew(2,i)**2+xnew(3,i)**2)
!            do j = 1, 3 
!               pnew(j,i) = pam*xx(j,i)/ram
!            enddo 
!            enddo

             ptemp(:,:)=pturn(:,:)
             xtemp(:,:)=xturn(:,:)

             call cpu_time(time1)
             do i=2,n_waypts
               call cnsvangmom(waypt(:,:,i-1),ptemp,waypt(:,:,i),ptemp2,nat,iflag)
               if(iflag.eq.0)then
                 ptemp(:,:)=ptemp2(:,:)
                 xtemp(:,:)=waypt(:,:,i)
               else
                 write(6,*)'nonfatal failure in cnsvangmom, i=', i
               endif
             enddo

!xxx             call cnsvangmom(xturn,pturn,xnew,pnew,nat,iflag)

             call cnsvangmom(xtemp,ptemp,xnew,pnew,nat,iflag)
             call cpu_time(time2)
             time_pgen=time_pgen+time2-time1
             if(iflag.eq.1)then
               stop 'fatal convergence failure in cnsvangmom'
             endif
           else
             lbranch = .false.
!xxx             wght = 1d0 - probt/gamma
             wght = 2d0*(1d0 - probt/gamma)
!            call recoverc(cre_save,cim_save,phase_save,gcre_save,gcim_save)
!            info =11
!            return
           endif
! print tunneling path information
      pe_ini = pe_ini*autoev
      pe_end = pe_end*autoev
      pe_bh  = pe_bh*autoev
      pema_ini(:)   = pema_ini(:)*autoev
      pema_bh(:)    = pema_bh(:)*autoev
      pema_end(:)   = pema_end(:)*autoev
      pemd_ini(:,:) = pemd_ini(:,:)*autoev
      pemd_bh(:,:)  = pemd_bh(:,:)*autoev
      pemd_end(:,:) = pemd_end(:,:)*autoev
      len_bh  = len_bh*autoang
      len_iso = len_iso*autoang
      if(lbranch) then
        write(340,'(2(2X,a,es14.5e3),2X,a,i7,a)')'Prob.',probt,'Weight', &
                               wght,'TRAJ',ithistraj,' branching path'
      else
       write(340,'(2(2X,a,es14.5e3),2X,a,i7,a)')'Prob.',probt,'Weight', &
                             wght,'TRAJ',ithistraj,' nonbranching path'
      endif
      write(340,'(20f10.3)') 0.00,pe_ini,pema_ini(1:nsurft)
      write(340,'(20f10.3)') 0.00,((pemd_ini(i,j),j=i,nsurft), i = 1, nsurft)
      write(340,'(20f10.3)')0.00,((rhor_ini(i,j),j=i,nsurft),i=1,nsurft)
!     write(340,'(20f10.3)')0.00,((rhor_ini(i,j),j=i,nsurft),i=1,nsurft)
!     write(340,'(20f10.3)')0.00,((denr_ini(i,j),j=i,nsurft),i=1,nsurft)

      write(340,'(20f10.3)') len_bh,pe_bh,pema_bh(1:nsurft)
      write(340,'(20f10.3)') len_bh,((pemd_bh(i,j),j=i,nsurft), i=1,nsurft)
      write(340,'(20f10.3)') len_bh,((rhor_bh(i,j),j=i,nsurft), i=1,nsurft)
!     write(340,'(20f10.3)') len_bh,((rhor_bh(i,j),j=i,nsurft), i=1,nsurft)
!     write(340,'(20f10.3)') len_bh,((denr_bh(i,j),j=i,nsurft), i=1,nsurft)
      write(340,'(20f10.3)') len_iso,pe_end,pema_end(1:nsurft)
      write(340,'(20f10.3)') len_iso,((pemd_end(i,j),j=i,nsurft), i=1,nsurft)
      write(340,'(20f10.3)') len_iso,((rhor_end(i,j),j=i,nsurft), i=1,nsurft)
!     write(340,'(20f10.3)') len_iso,((rhor_end(i,j),j=i,nsurft), i=1,nsurft)
!     write(340,'(20f10.3)') len_iso,((denr_end(i,j),j=i,nsurft), i=1,nsurft)
      write(340,*)
! end of print to file 340
!
! print tunneling path geometries
! print the beginning, maximum of mean potential, end of tunneling path
      write(34,'(1X,i5)') nat
      if(lbranch) then
       write(34,'(2(2X,a,es14.5e3),2X,a,i7,a)')'Prob.',probt,'Weight', &
                               wght,'TRAJ',ithistraj,' branching path'
      else
       write(34,'(2(2X,a,es14.5e3),2X,a,i7,a)')'Prob.',probt,'Weight', &
                             wght,'TRAJ',ithistraj,' nonbranching path'
      endif
      do k =1,nat
      write(34,'(1X,a,3f12.6)') symbol0(k), &
                                     (xturn(j,k)*autoang,j=1,3)
      enddo
      write(34,'(1X,i5)') nat
      write(34,*)
      do k =1,nat
      write(34,'(1X,a,3f12.6)') symbol0(k), (x_bh(j,k)*autoang,j=1,3)
      enddo
      write(34,'(1X,i5)') nat
      write(34,*)
      do k =1,nat
      write(34,'(1X,a,3f12.6)') symbol0(k), (xnew(j,k)*autoang,j=1,3)
      enddo
! return information
      if(lbranch) then
        info = 10
        return
      else
        info = 11
       call recoverc(cre_save,cim_save,phase_save,gcre_save,gcim_save)
        return
      endif


      endif

      call recoverc(cre_save,cim_save,phase_save,gcre_save,gcim_save)
      info =12
      return
      end subroutine actionint_nt

      subroutine recoverc(cre0,cim0,phase0,gcre0,gcim0)
      use param, only: mnsurf, mnat
      use c_traj, only: phase, cim, cre, gcre, gcim

      implicit none
      real*8 cre0(2*mnsurf),cim0(2*mnsurf),phase0(mnsurf)
      real*8 gcre0(2*mnsurf),gcim0(2*mnsurf)

      cre(:)    = cre0(:)
      cim(:)    = cim0(:)
      phase(:)  = phase0(:)
      gcre(:)   = gcre0(:)
      gcim(:)   = gcim0(:)

      end subroutine recoverc

      subroutine takestep_nt(xx,tstep)
! This subroutine is a simplified version of subroutine "takestep"
! 
      use param, only: mnsurf, mnat, mnyarray
      use c_sys, only: hstep, nsurft
      use c_traj, only: phase, cre, cim, pem, gcre, gcim

      implicit none
      integer :: i,nv
      real*8 :: tstep,hstep_save
      real*8 :: xx(3,mnat)
! local storage for integration 
      real*8 :: y(mnyarray),dydx(mnyarray)
! bsstep_nt
      real*8 :: hnext,hdid,yscal(mnyarray)

      nv = 3*nsurft
!     nv = 6*natoms+3*nsurft
! start integration of electronic coefficients
  
! put cre, cim, gcre, gcim, phase into a single array
      do i = 1, nsurft 
        y(i)             = phase(i)
        y(nsurft + i)    = cre(i)
        y(2*nsurft +i)   = cim(i)
        dydx(i)          = pem(i)
        dydx(nsurft+i)   = gcre(i)
        dydx(2*nsurft+i) = gcim(i)
      enddo     
      hstep_save = hstep
      hstep = tstep  ! temporarily change integration step hstep
      call rk4_nt(xx,y,dydx,nv,0)
      hstep = hstep_save

! Now we put the updated values back; this step was neglected before Ver. 2016
      do i = 1, nsurft
        phase(i) = y(i)
        cre(i)   = y(nsurft + i)
        cim(i)   = y(2*nsurft +i)
        pem(i)   = dydx(i)
        gcre(i)  = dydx(nsurft+i)
        gcim(i)  = dydx(2*nsurft+i)
      enddo

!xxx     call bsstep_nt(xx,y,dydx,nv,time,tstep,eps,yscal,hdid,hnext)

      end subroutine takestep_nt

      subroutine rk4_nt(xx,y,dydx,nv,im)
!     Fourth order Runge-Kutta integrator. From numerical recipes.

      use param, only: mnat, mnyarray
      use c_sys, only: hstep

      implicit none
      integer :: nv,im
      integer :: i,j
      double precision :: xx(3,mnat), y(mnyarray),dydx(mnyarray)
      double precision :: hh,h6
      double precision :: yt(mnyarray),dym(mnyarray),dyt(mnyarray)

      hh=hstep/2.d0
      h6=hstep/6.d0
!     first step
      do 11 i=1,nv
      yt(i)=y(i)+hh*dydx(i)
 11   continue

!     second step
      call derivs_nt(xx,yt,dyt,nv,im)
      do 12 i=1,nv
      yt(i)=y(i)+hh*dyt(i)
 12   continue

!     third step
      call derivs_nt(xx,yt,dym,nv,im)
      do 13 i=1,nv
      yt(i)=y(i)+hstep*dym(i)
      dym(i)=dyt(i)+dym(i)
 13   continue

!     fourth step
      call derivs_nt(xx,yt,dyt,nv,im)
!     accumulate increments with proper weights
      do 14 i=1,nv
      y(i)=y(i)+h6*(dydx(i)+dyt(i)+2.d0*dym(i))
 14   continue

      end subroutine rk4_nt

      subroutine derivs_nt(xx,yn,yout,nv,im)

! This subroutine (along with XPTOY) puts together an single array 
! and its derivatives for integration.  It is called by the
! integrators whenever they need derivative information.  YN
! is the array of variables to be integrated, and YOUT is the
! derivatives.
! iflinear: 0: atom; 1: linear; 2: nonlinear

      use param, only: mnat, mnyarray
      use c_common, only: td, veloc

      implicit none
      integer :: nv,im
      integer :: i
      double precision :: yn(mnyarray),yout(mnyarray),dmag
      double precision :: xx(3,mnat)
      double precision :: petmp
 
!     get xxm from yn
      call ctoy(yn,yout,nv,1)
!     get gradients
!     call getgrad2(xx,dmag,im)
      call getgrad2(xx,petmp)
!     nc = nc + 1
!     transform to 1-D
      call ctoy(yn,yout,nv,0)

      end subroutine derivs_nt

      subroutine ctoy(y,dydx,nv,ido)
      use param, only: mnyarray, mnat, mnsurf
      use c_sys, only: nsurft
      use c_traj, only: gcim, gcre, pem, cim, cre, phase 

      implicit none
      integer :: i, nv ,ido
! local storage for integration 
      real*8 :: y(mnyarray),dydx(mnyarray)

      if(ido.eq.1) then
 
        do i = 1, nsurft
          phase(i) = y(i)
          cre(i)   = y(nsurft + i) 
          cim(i)   = y(2*nsurft +i)
          pem(i)   = dydx(i) 
          gcre(i)  = dydx(nsurft+i)
          gcim(i)  = dydx(2*nsurft+i)
        enddo

      else
! put cre, cim, gcre, gcim, phase into a single array
      do i = 1, nsurft
        y(i)             = phase(i)
        y(nsurft + i)    = cre(i)
        y(2*nsurft +i)   = cim(i)
        dydx(i)          = pem(i)
        dydx(nsurft+i)   = gcre(i)
        dydx(2*nsurft+i) = gcim(i)
      enddo

      endif
      
      end subroutine ctoy

      double precision function tpot_nt(zed)
      use tpotmod_nt
      use c_struct, only: icharge, imulti, nat, mmm, symbol0
      use c_traj, only: pe, cre, phase, cim, gcre, gcim
      use c_common, only: td, veloc
      implicit none
      double precision :: zed, zold, xtemp(3,mnat), dlen_iso, &
                          tstep, x_base_iso(3,mnat), pe_tmp
      logical :: lterm

      save zold

      if(if1.ne.0)then !this is not the first potential call for this zero search
        xlast(:,:)=xnow(:,:)
      else  ! We need to treat the first call special
        if1=1
        x_base(:,:)=xlast(:,:)
        x_base_iso(:,:)=x_base(:,:)
        call cartoiso(x_base_iso,nat,mmm,1) ! put x_base_iso in isoinertial coordinates
        zold=0d0
      endif

      din(1)=(zed-zold)*scale*drrpass(itsearch(1))
      if(itsearch(2).ne.0)then
        din(2)=(zed-zold)*scale*drrpass(itsearch(2))
      endif

      cre(:)=cre_stash(:)
      cim(:)=cim_stash(:)
      phase(:)=phase_stash(:)
      gcre(:)=gcre_stash(:)
      gcim(:)=gcim_stash(:)
      call intcart(xlast, xnow, nat, mmm, din, itsearch, 1, lterm)
      if(lterm) then
        write(6,*)'failure in intcart'
        stop
      endif

! Using getgrad2 calls for now because we have no procedures in place to get
! the potential without also getting the gradient; this should be changed....
      xtemp(:,:)=xnow(:,:)
      call cartoiso(xtemp,nat,mmm,1) ! put xtemp in isoinertial coordinates
      call length(nat,xtemp,x_base_iso,dlen_iso)

      td(:,:)=xnow(:,:)-x_base(:,:)
      tstep=dlen_iso/veloc_save

      call takestep_nt(xnow,tstep)
      call getgrad2(xnow, pe_tmp)
      zold=zed
      tpot_nt=pe_tmp-vturn
  write(6,'(a,2es25.17)')'zed,tpot_nt=',zed,tpot_nt
      end function tpot_nt
