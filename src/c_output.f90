  module c_output
! **********************************************************************
! OUTPUT FLAGS
! **********************************************************************
! lwrite(x) = write to unit X if TRUE
! nprint:   print information every NPRINT steps
! idebug:   print debug information

      logical :: lwrite(1000),minprinticon,minprinttrapz,maxprint,idebug
      integer :: nprint
      double precision :: time_tunnel, time_brent, time_pgen

  end module c_output
! **********************************************************************
