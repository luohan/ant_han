      subroutine turnpt(pdi,pdipre,nat,ltp,turnd,tunnd,x,p,xturn,  &
                        pturn,it,tuntype)
      use param, only: mnat
      use c_initial, only: itunn

      implicit none
      integer nat,i,j,k,l,n,it,tuntype
      real*8 pdi(3*nat),pdipre(3*nat),turnd(3,nat)
      real*8 x(3,mnat),p(3,mnat),xturn(3,mnat),pturn(3,mnat)
      real*8 ppp,dt0
      real*8 tunnd(3,mnat,3*mnat-6)
      logical ltp

      ltp = .false.
      do l = 1, 3*nat-6
         if(itunn(l).eq.1.or.(tuntype.eq.2.and.l.eq.1)) then
! check if the current point is the turning point
         if(abs(pdi(l)) .lt. 1E-10) then
           ltp = .true.
           it = l
           do i = 1, nat
             do j = 1,  3
              xturn(j,i) = x(j,i)
              pturn(j,i) = p(j,i)
              turnd(j,i) = tunnd(j,i,l)
             enddo
           enddo
           return
         endif
! check if pdi change sign for two successive points
         ppp = pdi(l) * pdipre(l)
!        write(6,*) 'ppp ', ppp
         if(ppp.lt.0d0) then
           ltp = .true.
           it = l
!
! We temporarily use the current point as the turning point as an approximation
! later we may need to do interpolation or use smaller time step to reintegrate
! to find more precise turning point
!
           do i = 1, nat
             do j = 1,  3
              xturn(j,i) = x(j,i)
              pturn(j,i) = p(j,i)
              turnd(j,i) = tunnd(j,i,l)
             enddo
           enddo
           return
         endif

        endif 
        if(tuntype.eq.2 ) exit
      enddo

      end subroutine turnpt
