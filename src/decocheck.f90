      subroutine decocheck(phop,nsurf,lhop,kev,speck, &
                           step,time,phopmax,phopmin)

! Check for a switch of the decoherent state for the SCDM and CSDM 
! methods.  Draw and random number and compare it with PHOP.  If
! a switch occurs, set NSURF to the new surface and set LHOP = .TRUE.

      use param, only: mntraj, mnsurf, nmax2
      use c_sys, only: intflag, nsurft

      implicit none
! input
      double precision :: phop(mnsurf)

! input/output
      integer :: nsurf
      logical :: lhop

! local
      integer :: newsurf,i
      double precision :: dxi,tmp

!  RVM
!  input
      double precision :: step,time
      double precision :: phopmax(mnsurf),phopmin(mnsurf)
!  local
      double precision :: mxtmp,mntmp
!  output
      integer :: kev
      double precision :: speck(8,nmax2)
!  RVM

! get random number
      call get_rng(dxi)
!  JZ
      mxtmp = 0d0
      mntmp = 0d0
!  JZ
! check for switch
!  RVM if we're not using Mike Hack's method do as before
      if(intflag.ne.6) then
      newsurf = 0
      tmp = 0.d0
      do i=1,nsurft
      tmp = tmp + phop(i)
      if (dxi.le.tmp.and.newsurf.eq.0) newsurf = i
      enddo
      if (newsurf.eq.0) then
        write(6,*)'NEWSURF = 0 in DECOCHECK!'
        stop
      endif
      endif 

! RVM checking phopmax and phopmin
      if(intflag.eq.6) then
      newsurf = 0
      tmp = 0.d0
      do i=1,nsurft
      tmp = tmp + phop(i)
      mxtmp = mxtmp + phopmax(i)
      mntmp = mntmp + phopmin(i)
      if (dxi.le.tmp.and.newsurf.eq.0) then
      newsurf = i
! J Zheng debug
!     write(6,'(a,2f10.4,i4,a,3f10.4)')'dxi tmp newsurf',dxi,tmp,newsurf &
!          ,'phop =', phop(1),phop(2),phop(3)  
      endif
      enddo
        if(dxi.gt.tmp) then
         if(dxi.lt.mntmp) then
          kev = kev + 1
          if (kev .gt. nmax2) then
          write(*,*)'Error.  kev = ',kev,' but nmax2 = ',    nmax2
          stop
          endif
! save data in special K array
                 speck(1,kev)=dble(1)
                 speck(2,kev)=phop(i)
                 speck(3,kev)=phopmin(i)
                 speck(4,kev)=phopmax(i)
                 speck(5,kev)=dxi
                 speck(6,kev)=dble(nsurf)
                 speck(7,kev)=step
                 speck(8,kev)=time
         elseif(dxi.lt.mxtmp) then
          kev = kev + 1
          if (kev .gt. nmax2) then
          write(*,*)'Error.  kev = ',kev,' but nmax2 = ',   nmax2
          stop
          endif
! save data in special K array
                 speck(1,kev)=dble(1)
                 speck(2,kev)=phop(i)
                 speck(3,kev)=phopmin(i)
                 speck(4,kev)=phopmax(i)
                 speck(5,kev)=dxi
                 speck(6,kev)=dble(nsurf)
                 speck(7,kev)=step
                 speck(8,kev)=time
         endif
       endif
      endif
!  RVM

! switch!
      if (newsurf.ne.nsurf) then
!       get energies
        write(6,*)'DECO SWITCH from ',nsurf,'-->',newsurf
        nsurf = newsurf
        lhop = .true.
      endif

      end subroutine decocheck
