      subroutine periodimage(dx,dy,dz,cell)
! Deal with periodic conditions
! INPUT:  dx,dy,dz,Cell
! OUTPUT: new dx,dy,dz
! edge(3):    edge of the cell
! xxfrac(3):  Fractorial coordinate of an atom (with cell vector a, b
! and c as unit vector, thus all value are 0-1)
      implicit none
      double precision :: dx,dy,dz
      double precision :: cell(6)
      double precision,parameter :: small = 1.d-5
      double precision,parameter :: PI2 =3.14159265358979d0/2.d0

!      if ( dabs(cell(4)-PI2).lt.small .and.   &
!           dabs(cell(5)-PI2).lt.small .and.   &
!           dabs(cell(6)-PI2).lt.small ) then
        dx=dx-cell(1)*anint(dx/cell(1))
        dy=dy-cell(2)*anint(dy/cell(2))
        dz=dz-cell(3)*anint(dz/cell(3))
!      else
!        write(6,*) 'Now only work for cubic or cuboid cells'
!        stop
!      endif
!          Geometric center of a cell (a+b+c)/2
!          Judge if an atom moves out the cell
!          do i=1,natoms
!            xxfrac(1) = invcvec(1,1)*xx(1,i) + invcvec(1,2)*xx(2,i) + &
!                        invcvec(1,3)*xx(3,i)
!            xxfrac(2) = invcvec(2,1)*xx(1,i) + invcvec(2,2)*xx(2,i) + &
!                        invcvec(2,3)*xx(3,i)
!            xxfrac(3) = invcvec(3,1)*xx(1,i) + invcvec(3,2)*xx(2,i) + &
!                        invcvec(3,3)*xx(3,i)
!            jaf1 = idnint( xxfrac(1) )
!            jaf2 = idnint( xxfrac(2) )
!            jaf3 = idnint( xxfrac(3) )
!C            if ( jaf1 .ne. 0 ) ifsucc = .true.
!C            if ( jaf2 .ne. 0 ) ifsucc = .true.
!C            if ( jaf3 .ne. 0 ) ifsucc = .true.
!            xxfrac(1) = xxfrac(1) - dble(jaf1)
!            xxfrac(2) = xxfrac(2) - dble(jaf2)
!            xxfrac(3) = xxfrac(3) - dble(jaf3)
!            xx(1,i) = cvec(1,1)*xxfrac(1) + cvec(1,2)*xxfrac(2) +  &
!                       cvec(1,3)*xxfrac(3)
!            xx(2,i) = cvec(2,1)*xxfrac(1) + cvec(2,2)*xxfrac(2) +  &
!                       cvec(2,3)*xxfrac(3)
!            xx(3,i) = cvec(3,1)*xxfrac(1) + cvec(3,2)*xxfrac(2) +  &
!                       cvec(3,3)*xxfrac(3)
!          enddo
!      endif
!      if (ifsucc) write(6,*) 'Change positions'
!
      end subroutine periodimage
