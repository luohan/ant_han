!
      subroutine setupvolume
!
! Taken from AMSOL and used for the volume calculations
      double precision PI, PD2, PD64, INCONTR
      COMMON /PICOM/ PI,PD2,PD64
      COMMON /ONESCM/ ICONTR(100)
      DATA ZERO /0.0D0/
! taken from ominsol
!
      PI=ACOS(-1.D0)                                                   !GDH0897
      PD2=PI*0.5D0                                                     !GDH0897
      PD64=PI*0.015625D0                                               !GDH0897
!
! taken from AMSOL
      do i=1,100
      ICONTR(i) = 1
      enddo
      ICONTR(5)  = 2
      ICONTR(6)  = 2
      ICONTR(10) = 2
      ICONTR(14) = 2
      ICONTR(15) = 2
      ICONTR(18) = 2
      ICONTR(21) = 2
      ICONTR(22) = 2
      ICONTR(27) = 2
      ICONTR(29) = 2
      ICONTR(32) = 2
      ICONTR(34) = 2
      ICONTR(36) = 2
      ICONTR(37) = 2
      ICONTR(39) = 2
      ICONTR(40) = 2
      ICONTR(44) = 2
!      ICONTR=(/1,  1,  1,  1,  2,  2,  1,  1,  1,  2,  1,  1,
!    &         1,  2,  2,  1,  1,  2,  1,  1,  2,  2,  1,  1,
!    &         1,  1,  2,  1,  2,  1,  1,  2,  1,  2,  1,  2,
!    &         2,  1,  2,  2,  1,  1,  1,  2,  1,  1,  1,  1,
!    &         1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
!    &         1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
!    &         1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
!    &         1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
!    &         1,  1,  1,  1/)
!
!
      end subroutine setupvolume
