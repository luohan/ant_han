subroutine gen_prt(coord,masses,totmass,natoms,n3tm,prt)
!
! Project out translations and instantaneous rotations from Hessian
! coord is assumed to be centered at the center of mass
!
! This subroutine is taken from MSTor program. 
!
! J. Zheng July 2013
!
implicit none
integer :: natoms,n3tm,i,j,ip,jp,ia,ib,ja,jb,ic,jc,ix,jx,jend
double precision :: masses(natoms),coord(natoms,3),prt(n3tm,n3tm)
double precision :: rot(3,3),tensor(3,3,3),totmass,summ

DATA TENSOR / 5*0.0D0,-1.0D0,0.0D0,1.0D0,3*0.0D0,1.0D0,3*0.0D0,  &
                  -1.0D0,3*0.0D0,-1.0D0,0.0D0,1.0D0,5*0.0D0 /

! debugging 
! print geometries
  do i = 1, natoms
   write(6,'(2X,3f12.6)') (coord(i,j)*0.5291772108d0, j = 1, 3)
  enddo
 

! Calculate the moment of inertia 
!
rot(:,:) = 0.0d0
prt(:,:) = 0.0d0
do i=1,natoms
  rot(1,1)= rot(1,1) + masses(i)*(coord(i,2)**2+coord(i,3)**2)
  rot(2,2)= rot(2,2) + masses(i)*(coord(i,1)**2+coord(i,3)**2)
  rot(3,3)= rot(3,3) + masses(i)*(coord(i,1)**2+coord(i,2)**2)
  rot(1,2)= rot(1,2) - masses(i)*coord(i,1)*coord(i,2)
  rot(1,3)= rot(1,3) - masses(i)*coord(i,1)*coord(i,3)
  rot(2,3)= rot(2,3) - masses(i)*coord(i,2)*coord(i,3)
enddo
rot(2,1)=rot(1,2)
rot(3,1)=rot(1,3)
rot(3,2)=rot(2,3)

call inv3(rot) ! overwrite rot with its inverse

 do ip = 1, natoms
   ix = 3*(ip-1)
 do jp = 1, ip
   jx = 3*(jp-1)
   do ic = 1, 3
      JEND = 3
      IF (JP.EQ.IP) JEND = IC
   do jc = 1, JEND

        summ = 0.d0
        DO IA = 1, 3
        DO IB = 1, 3
           IF (TENSOR(IA,IB,IC).eq.0d0)cycle
           DO JA = 1, 3
           DO JB = 1, 3
              IF (TENSOR(JA,JB,JC).eq.0d0)cycle
              SUMM = SUMM+TENSOR(IA,IB,IC)*TENSOR(JA,JB,JC)*ROT(IA,JA)* &
                     COORD(IP,IB)*COORD(JP,JB)*sqrt(masses(ip)*masses(jp))
           enddo     
           enddo     
        enddo    
        enddo    

     i = ix + ic
     j = jx + jc
     prt(i,j)= summ  
     if (ic.eq.jc) prt(i,j) = prt(i,j) + sqrt(masses(ip)*masses(jp))/totmass
     if (i.ne.j)   prt(j,i) = prt(i,j)
   enddo ! end jc
   enddo ! end ic
 enddo ! end jp
 enddo ! end IP
end

subroutine inv3(m)
!
! subroutine to inverse a 3x3 matrix
!
implicit none
double precision :: m(3,3),w(3,3),z
w(1,1) = m(2,2)*m(3,3)-m(2,3)*m(3,2)
w(1,2) = m(1,3)*m(3,2)-m(1,2)*m(3,3)
w(1,3) = m(1,2)*m(2,3)-m(1,3)*m(2,2)
w(2,1) = m(2,3)*m(3,1)-m(2,1)*m(3,3)
w(2,2) = m(1,1)*m(3,3)-m(1,3)*m(3,1)
w(2,3) = m(1,3)*m(2,1)-m(1,1)*m(2,3)
w(3,1) = m(2,1)*m(3,2)-m(2,2)*m(3,1)
w(3,2) = m(1,2)*m(3,1)-m(1,1)*m(3,2)
w(3,3) = m(1,1)*m(2,2)-m(1,2)*m(2,1)
z = m(1,1)*w(1,1) + m(1,2)*w(2,1) + m(1,3)*w(3,1)
m(:,:) = w(:,:)/z
end subroutine inv3

