      subroutine arcom(rmi,rmi2,rbin,mbin)
!   distance of atoms to the CoM

      use param, only: mnat, autoang
      use c_struct, only: natoms, xxm

      implicit none
      integer ::  mbin
      integer :: i,nbin
      double precision :: rmi(mnat),rmi2(mnat),rbin,rmax0

      rmax0=-1.d0
      do i=1,natoms
        rmi2(i) = xxm(i,1)**2 + xxm(i,2)**2 + xxm(i,3)**2
        rmi(i)  = dsqrt(rmi2(i))
        rmax0=max(rmax0,rmi(i))
      enddo
      nbin=int(rmax0/rbin)
      mbin=max(nbin,mbin)
      if (mbin.ge.mnat) then
        write(6,'(a,a,d16.8,a)') 'Too many bins. Please increase RBIN value to ', &
             'at least: ',(rmax0/dble(mnat))*autoang,' A'
        stop
      endif

      end subroutine arcom
