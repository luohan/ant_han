      subroutine intcart(xx,xout,nat,mass,din,it,nd,lterm)
      use param, only: mnat

      implicit none
      integer, parameter :: ndec_max=15
      integer :: nat, nd, ndec, it(2)
      real*8 :: xx(3,mnat), xout(3,mnat), x0(3,mnat), din(2)
      real*8 :: qq(3*mnat), mass(mnat), amat(3*mnat,3*mnat)
      real*8 :: step, step_use, scale_done
      logical :: lterm ! if true, then there was convergence failure in iteramat

      x0(:,:) = xx(:,:)
      step=1d0/dble(nd)
      ndec=0
      scale_done=0d0

      do 
        step_use=min(1d0-scale_done,step)
        qq(:) = 0d0 
        qq(it(1)) = din(1) * step_use
        if(it(2).ne.0) qq(it(2)) = din(2)*step_use

        call iteramat(x0,xout,qq,nat,mass,lterm, amat)
                      
        if(lterm .eqv. .false.)then !there were no problems with convergence
          scale_done=scale_done+step_use
          x0 = xout
          if(scale_done.ge.1d0)exit !we are done
        else ! convergence failed within iteramat
          step=step/2d0 !retry with a smaller step size
          ndec=ndec+1
          if(ndec.gt.ndec_max)then
            stop 'using too small of a step in intcart'
          endif
        endif

      enddo 

      end subroutine intcart
