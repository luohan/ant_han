      subroutine diamax(xmax,val,xj,im,mm,nsurf,ax,cx,ierror)

! modified from diamin in order to handle quasi-bond state
! xmax: location of maxmimum
! val:  PES value
! xj:   rotational energy
! ax:    xmin found before
! cx:    infinite value
! find maximum value of diapot(x,xj) between ax and cx
        use param, only: mnat

        implicit none
! mnbrak variables
        integer,intent(in) :: im,nsurf
        real(8),intent(in) :: mm(mnat),xj,ax,cx
        real(8),intent(out) :: val,xmax
        logical,intent(out) :: ierror

        real(8) :: bx,a,b, xmax2, val2
        real(8),parameter :: CGOLD=.3819660d0, tinyv=1.0d-3
        logical :: ier(2)
        ierror = .false.
        ier = .false.

        a=min(ax,cx)
        b=max(ax,cx)
        ! get one from a,bx
        call brent_min(xmax,val,ier(1),a,b,im,xj,mm,nsurf)
        ! hard code here
        call brent_min(xmax2,val2,ier(2),a,6.0d0,im,xj,mm,nsurf)
        if (ier(1) .and. ier(2)) then
          write(*,*) "diamax fail"
          ierror = .true.
        elseif ((.not. ier(2)) .and. ier(1))then
          val = -val2
          xmax = xmax2
        elseif ((.not. ier(2)) .and. (.not. ier(1))) then
          if (val < val2) then
            val = -val
          else
            val = -val2
            xmax = xmax2
          end if
        else
          val = -val
        end if
      end subroutine diamax

      function negative_fdia(x,im,xj,mm,nsurf)
        use param, only: mnat
        implicit none
        real(8) :: x, el,xj,mm(mnat)
        real(8) :: negative_fdia
        integer :: im,nsurf

        call diapot(x,im,el,xj,mm,nsurf)
        negative_fdia = - el
        return
      end function negative_fdia

      subroutine brent_min(xmin,val,ierror,ax,cx,im,xj,mm,nsurf)
        ! brent minimum value of negative_fdia between
        ! ax and cx
        ! xmin is the location, val is negative_fdia(xmin)
        use param,only : mnat
        implicit none
        real(8),intent(in) :: xj,mm(mnat),ax,cx
        integer,intent(in) :: nsurf,im
        real(8),intent(out) :: xmin,val
        real(8),external :: negative_fdia
        logical :: ierror

        integer :: itmax
        integer :: iter
        real(8) :: tol,CGOLD,ZEPS
        parameter (itmax=100,CGOLD=.3819660,ZEPS=1.0e-10,tol=1.0d-13)
        real(8) :: a,b,d,e,etemp,fv,fw,fx,p,tol1,tol2,v,w,x,xm,bx
        real(8) :: r,q,u,fu
        bx = ax + (cx-ax)*CGOLD
        a = ax
        b = cx

        v=bx
        w=v
        x=v
        e=0.d0
        fx = negative_fdia(x,im,xj,mm,nsurf)
        fv=fx
        fw=fx
        do iter=1,itmax
          xm=0.5d0*(a+b)
          tol1=tol*dabs(x)+ZEPS
          tol2=2.d0*tol1
          if(dabs(x-xm).le.(tol2-.5d0*(b-a))) go to 3
          if(dabs(e).gt.tol1) then
            r=(x-w)*(fx-fv)
            q=(x-v)*(fx-fw)
            p=(x-v)*q-(x-w)*r
            q=2.d0*(q-r)
            if(q.gt.0.d0) p=-p
            q=dabs(q)
            etemp=e
            e=d
            if(dabs(p).ge.dabs(.5d0*q*etemp).or.p.le.q*(a-x).or.   &
              p.ge.q*(b-x)) go to 1
            d=p/q
            u=x+d
            if(u-a.lt.tol2 .or. b-u.lt.tol2) d=dsign(tol1,xm-x)
            go to 2
          endif
     1    if(x.ge.xm) then
            e=a-x
          else
            e=b-x
          endif
          d=CGOLD*e
     2    if(dabs(d).ge.tol1) then
            u=x+d
          else
            u=x+dsign(tol1,d)
          endif
          fu = negative_fdia(u,im,xj,mm,nsurf)
          if (fu.le.fx) then
            if (u.ge.x) then
              a=x
            else
              b=x
            endif
            v=w
            fv=fw
            w=x
            fw=fx
            x=u
            fx=fu
          else
            if(u.lt.x) then
              a=u
            else
              b=u
            endif
            if(fu.le.fw .or. w.eq.x) then
              v=w
              fv=fw
              w=u
              fw=fu
            else if(fu.le.fv .or. v.eq.x .or. v.eq.w) then
              v=u
              fv=fu
            endif
          endif
        enddo
        print *,"brent exceed maximum iterations"
        write(6,*) "Program fails at diamax line 108"
        ierror = .true.
        return
      ! stop
        x = 1.d10
        fx = 1.d10
      3 xmin=x
        val=fx
      end subroutine brent_min
