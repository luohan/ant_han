      subroutine integhop(nsurf,y,yo,rhor,rhoro,phop,phopmax,phopmin)

! Used for both surface hopping and DM methods
! This subroutine determines the hopping probabilities phop according to
! phop(j) = (integral(bjk+) + integral(bjk-))/(cre(k)**2 + cim(k)**2)
! See M.D. Hack, A.W. Jasper, Y.L. Volobuev, D.W. Schwenke, D.G. Truhlar, JPCA 103, 6309 (1999).
!
!  INPUT
!  y,yo: arrays containing all the integrated variables at the current and previous steps
!  rhor,rhoro: arrays containing the electronic probabilities at the 
!  current (t+deltat) and previous (t) steps
!
      use param, only: mnsurf, mnyarray
      use c_struct, only: natoms
      use c_sys, only: nsurft

      implicit none
      integer :: ii,k,kp,km,ivar,kels,nsurf
      double precision :: tmp,mntmp,mxtmp,dreal,dm,dp,pr,prmax,prmin
      double precision :: ptmp(mnsurf),pmaxtmp(mnsurf),pmintmp(mnsurf)
      double precision :: rhor(mnsurf,mnsurf),rhoro(mnsurf,mnsurf)
      double precision :: phop(mnsurf),phopmax(mnsurf),phopmin(mnsurf)
      double precision :: y(mnyarray),yo(mnyarray)

         tmp = 0.0d0
         mntmp = 0.0d0
         mxtmp = 0.0d0
         kels = nsurft*(nsurft-1)/2
         do ii = 1, nsurft
            if (ii.ne.nsurf) then                                       
               if (ii.lt.nsurf) then                                    
                   k = (nsurf-1)*(nsurf-2)/2+ii+ 6*natoms+ 5*nsurft
                   kp = k+kels
                   km = k+2*kels
                   dreal = y(k)-yo(k)
                   dm = dmax1(y(kp)-yo(kp),0.0d0)
                   dp = dmax1(yo(km)-y(km),0.0d0)
!                   write(6,*) 'dreal,dp,dm',dreal,dp,dm
               else if (ii.gt.nsurf) then                               
                   k = (ii-1)*(ii-2)/2+nsurf+ 6*natoms + 5*nsurft
                   kp = k+kels
                   km = k+2*kels
                   dreal = yo(k)-y(k)
                   dm = dmax1(yo(km)-y(km),0.0d0)
                   dp = dmax1(y(kp)-yo(kp),0.0d0)
!                   write(6,*) 'dreal,dp,dm',dreal,dp,dm
               end if 
               pr = dmax1(dreal/rhoro(nsurf,nsurf),0.0d0)
               prmax = dmin1(dm/rhoro(nsurf,nsurf),1.0d0)

               if ((rhoro(nsurf,nsurf) + dp) .le. 1.0d0) then
                 prmin = dm/(rhoro(nsurf,nsurf)+dp)
               else
                 prmin = dmax1(1.0d0 - rhor(nsurf,nsurf),0.0d0)
               endif
   
!               if (pr .gt. 1.0d0) then
!                 write(*,*)'pr = ',pr,' in hopp A! ',
!     &             (rhoro(ivar,ivar),ivar=1,nsurft),'nsurf = ',nsurf
!                 stop
!               endif
              
               pr = dmin1(pr,1.0d0)

               phop(ii) = pr
               tmp = tmp + phop(ii)

               phopmax(ii)=prmax
               mxtmp = mxtmp + phopmax(ii)

               phopmin(ii)=prmin
               mntmp = mntmp + phopmin(ii)

            end if
! probability of remaining is defined as unity minus the probability of hopping
         enddo                                                         

         phop(nsurf) = 1.0d0 - tmp
         phopmax(nsurf) = 1.0d0 - mxtmp
         phopmin(nsurf) = 1.0d0 - mntmp

! check that all probs add up to one
         tmp = 0.0d0
         mxtmp = 0.0d0
         mntmp = 0.0d0
         do ii = 1, nsurft
           ptmp(ii) = phop(ii) + tmp
           tmp = ptmp(ii)

           pmaxtmp(ii) = phopmax(ii) + mxtmp
           mxtmp = pmaxtmp(ii)

           pmintmp(ii) = phopmin(ii) + mntmp
           mntmp = pmintmp(ii)

         enddo

         if (abs(1.d0 -ptmp(nsurft)) .ge. 1.d-5) then
           write(6,*) 'ptmp(nsurft)=',ptmp(nsurft)
           write(6,*) 'hopping probs should add up to 1'
           stop
         endif

         if (abs(1.d0 -pmintmp(nsurft)) .ge. 1.d-5) then
           write(6,*) 'pmintmp(nsurft)=',pmintmp(nsurft)
           write(6,*) 'hopping probs should add up to 1'
           stop
         endif

         if (abs(1.d0 -pmaxtmp(nsurft)) .ge. 1.d-5) then
           write(6,*) 'pmaxtmp(nsurft)=',pmaxtmp(nsurft)
           write(6,*) 'hopping probs should add up to 1'
           stop 
         endif

      end subroutine integhop
