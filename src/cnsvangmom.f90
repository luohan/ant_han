subroutine cnsvangmom(x0,p0,x1,p1,natoms,iflag)
! This subroutine is to calculate the momenta after tunneling to 
! conserve total angular momentum.
! x0 -- geometry before tunneling
! p0 -- momenta before tunneling
! x1 -- geometry after tunneling
! p1 -- momenta after tunneling (to be calculated to 
!       conserve total angular momentum)
! y  -- array for the 3N+3 unknown variables 
!    1 , 2, 3       : u_1x, u_1y, u_1z (unit vector for final momentum)
!    ........
!    3N-2, 3N-1, 3N : u_nx, u_ny, u_nz
!    3N +1 : lamda_1
!    3N +2 : lamda_2
!    3N +3 : lamda_3
! g -- a set of 3N+3 constraint functions obtained by setting first 
!      derivative as zero using Lagrange multiplier method
! 
use param, only:  mnat
implicit none
integer :: i,j,k,natoms,nf,n3,niter,iter, iflag
real*8  :: x0(3,mnat),p0(3,mnat),x1(3,mnat),p1(3,mnat)
real*8  :: norm,pm(natoms),angmom(3,natoms),totang(3),u(3*natoms)
real*8  :: y(4*natoms+3),g(4*natoms+3),jacobg(4*natoms+3,4*natoms+3)
real*8  :: yn(4*natoms+3),gabs(4*natoms+3)
real*8, parameter :: tol=1d-12

integer :: indx(4*natoms+3),info,lwork
real*8, allocatable :: work(:)

niter = 100  ! maximum number of Newton-Raphson iterations
nf = 4*natoms+3
n3 = 3*natoms

! magnitude of momenta and the unit vector of momenta
do i = 1, natoms
  pm(i) = dsqrt(p0(1,i)**2+p0(2,i)**2+p0(3,i)**2)
  u(3*i-2) = p0(1,i)/pm(i)
  u(3*i-1) = p0(2,i)/pm(i)
  u(3*i)   = p0(3,i)/pm(i)
enddo

! initial value of y array
!xxx y(:) = -2d1
 y(:) = 1d-3
do i = 1, n3
  y(i) = u(i) - dble(i)*1d-2 
enddo

! angular momentum
totang(:) = 0d0
do i = 1, natoms
   angmom(1,i) =  x0(2,i)*p0(3,i) - x0(3,i)*p0(2,i) 
   angmom(2,i) =  x0(3,i)*p0(1,i) - x0(1,i)*p0(3,i) 
   angmom(3,i) =  x0(1,i)*p0(2,i) - x0(2,i)*p0(1,i) 
   totang(1)   =  totang(1) + angmom(1,i)
   totang(2)   =  totang(2) + angmom(2,i)
   totang(3)   =  totang(3) + angmom(3,i)
enddo

do iter = 1, niter 
!  write(6,*) 'iteration = ', iter
! evalulate constrain function g
  call gfunc(g,y,nf,x0,x1,u,pm,natoms,totang)
  gabs(:) = dabs(g(:))
! write(6,*) 'maxval(gabs)',maxval(gabs) 
  if(maxval(gabs).lt.tol) exit ! converged
! evalulate Jacobians of g function
  call cal_jacobg(jacobg,y,nf,x0,x1,pm,natoms)

! inverse jacobg
  call dgetrf(nf,nf,jacobg,nf,indx,info)
 if (info.ne.0 ) then
    write(6,*) "dgetrf exit abnormally in inversing Jacobian"
    write(6,*) "info in dgetrf = ",info
    iflag=1
    return
!xxx    stop
 endif
 lwork = -1
 allocate(work(1))
 call dgetri(nf,jacobg,nf,indx,work,lwork,info)
 lwork=int(work(1))
 deallocate (work)
 allocate( work(lwork) )
 call dgetri(nf,jacobg,nf,indx,work,lwork,info)
 deallocate(work)
 if (info.ne.0 ) then
    write(6,*) "dgetri exit abnormally in inversing Jacobian"
    write(6,*) "info in dgetri = ",info
    iflag=1
    return
!xxx    stop
 endif
 
 yn = 0d0
 do j = 1, nf
   yn(j) = y(j)
   do k = 1, nf
     yn(j) = yn(j) - jacobg(j,k)*g(k)
   enddo
 enddo
 y = yn
enddo ! end of iteration

if(iter.lt.niter)  then
  do i = 1, natoms
    p1(1,i) = pm(i)*y(3*i-2)
    p1(2,i) = pm(i)*y(3*i-1)
    p1(3,i) = pm(i)*y(3*i)
  enddo  
  iflag=0
  return
else
  iflag=1
!xxx  write(6,*)'Newton-Raphson iterations not converged'
!  stop
endif

end subroutine cnsvangmom

subroutine gfunc(g,y,nf,x0,x1,u,pm,natoms,totang)
! evaluate the function g
! y  -- array for the 3N+3 unknown variables 
!    1 , 2, 3       : u_1x, u_1y, u_1z (unit vector for final momentum)
!    ........
!    3N-2, 3N-1, 3N : u_nx, u_ny, u_nz
!    3N +1 : lamda_1
!    3N +2 : lamda_2
!    3N +3 : lamda_3
! g -- a set of 3N+3 constraint functions obtained by setting first 
!      derivative as zero using Lagrange multiplier method
! 
implicit none
integer :: nf,natoms
integer :: i,j,k,n3
real*8  :: g(nf),x0(3,natoms),x1(3,natoms),u(3*natoms),pm(natoms),y(nf)
real*8  :: totang(3),tmp

n3 = 3*natoms
do i = 1, natoms
  tmp = 2d0*(y(n3+3+i)+1d0)
  g(3*i-2) = tmp*y(3*i-2)-y(n3+2)*pm(i)*x1(3,i)+y(n3+3)*pm(i)*x1(2,i)-2d0*u(3*i-2)
  g(3*i-1) = tmp*y(3*i-1)-y(n3+3)*pm(i)*x1(1,i)+y(n3+1)*pm(i)*x1(3,i)-2d0*u(3*i-1) 
  g(3*i)   = tmp*y(3*i)  -y(n3+1)*pm(i)*x1(2,i)+y(n3+2)*pm(i)*x1(1,i)-2d0*u(3*i)
enddo

! 3N+1, 3N+2, 3N+3 terms
do i = 1, 3
  g(n3+i) = totang(i) 
enddo
do i = 1, natoms
  g(n3+1) = g(n3+1) - pm(i)*(x1(2,i)*y(3*i)   - x1(3,i)*y(3*i-1))
  g(n3+2) = g(n3+2) - pm(i)*(x1(3,i)*y(3*i-2) - x1(1,i)*y(3*i))
  g(n3+3) = g(n3+3) - pm(i)*(x1(1,i)*y(3*i-1) - x1(2,i)*y(3*i-2))
enddo

! 3N + 4 to 4N + 3 terms
do i = 1, natoms
  g(n3+3+i) = y(3*i-2)**2 + y(3*i-1)**2 + y(3*i)**2 - 1d0
enddo

end subroutine gfunc

subroutine cal_jacobg(jacobg,y,nf,x0,x1,pm,natoms)
! evaluate the Jacobian of function g
! y  -- array for the 3N+3 unknown variables 
!    1, 2, 3        : u_1x, u_1y, u_1z (unit vector for final momentum)
!    ........
!    3N-2, 3N-1, 3N : u_nx, u_ny, u_nz
!    3N +1 : lamda_1
!    3N +2 : lamda_2
!    3N +3 : lamda_3
! jacobg -- Jacobian of g function 
implicit none
integer :: nf,natoms
integer :: i,j,k,n3
real*8  :: jacobg(nf,nf),y(nf),x0(3,natoms),x1(3,natoms),pm(natoms)

jacobg(:,:) = 0d0
n3 = 3*natoms

! The first 3N rows of Jacobian
do i = 1, natoms
   jacobg(3*i-2,3*i-2) = 2d0*(1d0+y(n3+3+i))
   jacobg(3*i-1,3*i-1) = jacobg(3*i-2,3*i-2)
   jacobg(3*i,3*i)     = jacobg(3*i-2,3*i-2)
 
   jacobg(3*i-2,n3+2)  = -pm(i)*x1(3,i)
   jacobg(3*i-2,n3+3)  =  pm(i)*x1(2,i)
 
   jacobg(3*i-1,n3+1)  =  pm(i)*x1(3,i) 
   jacobg(3*i-1,n3+3)  = -pm(i)*x1(1,i)
  
   jacobg(3*i,  n3+1)  = -pm(i)*x1(2,i)
   jacobg(3*i,  n3+2)  =  pm(i)*x1(1,i)

   jacobg(3*i-2,n3+3+i)=2d0*y(3*i-2)
   jacobg(3*i-1,n3+3+i)=2d0*y(3*i-1)
   jacobg(3*i  ,n3+3+i)=2d0*y(3*i)
enddo

! The 3N+1, 3N+2, 3N+3  rows
do i = 1, natoms
   jacobg(n3+1,3*i-1) =  pm(i)*x1(3,i)
   jacobg(n3+1,3*i  ) = -pm(i)*x1(2,i)
 
   jacobg(n3+2,3*i-2) = -pm(i)*x1(3,i)
   jacobg(n3+2,3*i  ) =  pm(i)*x1(1,i)
 
   jacobg(n3+3,3*i-2) =  pm(i)*x1(2,i)
   jacobg(n3+3,3*i-1) = -pm(i)*x1(1,i)
enddo

! The 3N+4 to 4N+3 rows
do i = 1, natoms
   jacobg(n3+3+i,3*i-2) = 2d0*y(3*i-2)
   jacobg(n3+3+i,3*i-1) = 2d0*y(3*i-1)
   jacobg(n3+3+i,3*i  ) = 2d0*y(3*i)
enddo
 
end subroutine cal_jacobg
