      subroutine erotc(bigjm,erott)
!
! calculate rotational energy of an AG
!  Input:
!  xxm,ppm,mmm,natoms,iflinear
! This subroutine was inspired by a subroutine from GenDyne.
! bigjm:          angular momentum
! iflinear:      0: an atom; 1: Linear; 2; non-linear
! momi(3,3):     Moment of Inertia matrix
! ivmomi(3,3):   Invert of momi
!  Output:
!  erott:        linear: internal KE; non-linear: erot only
      use param, only: autoang, autoev
      use c_output, only: 
      use c_struct, only: iflinear, natoms, mmm, xxm
      use c_traj, only: ppm

      implicit none
      double precision bigjm(3),erott
! local
      integer :: i,j
      double precision :: momi(3,3),d1,d2,d3,d4,d5,d6,det,omeg(3), &
                          c11,c12,c13,c21,c22,c23,c31,c32,c33, &
                          ivmomi(3,3),detmtbt
      double precision :: r,vect(3),pptm,dx,dy,dz,erx,ery,erz,px,py,pz,&
                          compx,compy,compz,mtot
!
!     Nothing to do for just an atom
      erott=0.d0
      if (natoms.ne.1) then
!   Cannot calculate invert of the I matrix for linear molecules
        if (iflinear.eq.2) then
!         compute moment of intertia matrix momi
          call momigen(xxm,mmm,natoms,momi)
!         invert the I matrix
          det = detmtbt(momi)
          c11 = momi(2,2)*momi(3,3)-momi(2,3)*momi(3,2)
          c12 = momi(2,1)*momi(3,3)-momi(2,3)*momi(3,1)
          c13 = momi(2,1)*momi(3,2)-momi(2,2)*momi(3,1)
          c21 = momi(1,2)*momi(3,3)-momi(1,3)*momi(3,2)
          c22 = momi(1,1)*momi(3,3)-momi(1,3)*momi(3,1)
          c23 = momi(1,1)*momi(3,2)-momi(1,2)*momi(3,1)
          c31 = momi(1,2)*momi(2,3)-momi(1,3)*momi(2,2)
          c32 = momi(1,1)*momi(2,3)-momi(1,3)*momi(2,1)
          c33 = momi(1,1)*momi(2,2)-momi(1,2)*momi(2,1)
          ivmomi(1,1) = c11/det
          ivmomi(1,2) =-c12/det
          ivmomi(1,3) = c13/det
          ivmomi(2,1) =-c21/det
          ivmomi(2,2) = c22/det
          ivmomi(2,3) =-c23/det
          ivmomi(3,1) = c31/det
          ivmomi(3,2) =-c32/det
          ivmomi(3,3) = c33/det
!
! omega = inverse of moment of inertia matrix dotted into total angular
! momentum vector
! omega = I-1 . J (angular velocity)
          do i=1,3
            omeg(i) = ivmomi(i,1)*bigjm(1) + ivmomi(i,2)*bigjm(2) + &
                      ivmomi(i,3)*bigjm(3)
          enddo
! Rotational velocity v_rot = omega x r => the rotational energy
          do i=1,natoms
            erx=mmm(i)*(omeg(2)*xxm(3,i)-omeg(3)*xxm(2,i))
            ery=mmm(i)*(omeg(3)*xxm(1,i)-omeg(1)*xxm(3,i))
            erz=mmm(i)*(omeg(1)*xxm(2,i)-omeg(2)*xxm(1,i))
            erott=erott+0.5d0*(erx*erx+ery*ery+erz*erz)/mmm(i)
          enddo
! for debug information: if erott is abnormal >20 times 1.5 kbT
          if (erott.gt.10.d0/autoev) then
             write(6,*) 'Sth must be wrong:'
             write(6,*) 'Rotational energy (eV): ',erott*autoev
             write(6,*) 'Moment of inertia matrix (momi, in au)'
             do i=1,3
               write(6,108) (momi(i,j),j=1,3)
             enddo
             write(6,*) 'Determinant of moni (au): ',det
             write(6,*) 'Omega (in au)'
             write(6,108) (omeg(j),j=1,3)
             write(6,*) 'Geometry (in A)'
             do i=1,natoms
               write(6,108) (xxm(j,i)*autoang,j=1,3)
             enddo
             write(6,*) 'Momentum (in au)'
             do i=1,natoms
               write(6,108) (ppm(j,i),j=1,3)
             enddo
             write(6,*) 'Angular momentum (in au)'
             write(6,108) (bigjm(j),j=1,3)
             stop
          endif
         else
          dx=xxm(1,natoms)-xxm(1,1)
          dy=xxm(2,natoms)-xxm(2,1)
          dz=xxm(3,natoms)-xxm(3,1)
          r=dx*dx+dy*dy+dz*dz
          r=1.d0/dsqrt(r)
!         Linear molecular direction vector (any two atoms)
          vect(1)=dx*r
          vect(2)=dy*r
          vect(3)=dz*r
          compx=0.d0
          compy=0.d0
          compz=0.d0
          mtot=0.d0
          do i=1,natoms
!         Projection of ppm on the molecular direction
            pptm=ppm(1,i)*vect(1)+ppm(2,i)*vect(2)+ppm(3,i)*vect(3)
!         px,py,pz: ppm on the molecular direction
!         compx,compy,compz: CoM momentum on the molecular direction
            px=pptm*vect(1)
            py=pptm*vect(2)
            pz=pptm*vect(3)
            compx=compx+px
            compy=compy+py
            compz=compz+pz
            erott=erott+0.5d0*(px*px+py*py+pz*pz)/mmm(i)
            mtot=mtot+mmm(i)
          enddo
!         remove overal translation energy along molecular direction
          erott=erott-0.5d0*(compx*compx+compy*compy+compz*compz)/mtot
        endif
      endif
!
 107  format(1x,i5,f12.4,3f14.8)
 108  format(1x,3e16.8)
!
      end subroutine erotc
