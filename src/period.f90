      subroutine period(edia,jj,arr,mm,rin,rout,peri,phasead,nsurf,phmode,velphase)

! used by the atom-diatom initial conditions method
! computes the period in au of the diatomic vibration.
! edia: rovibrational energy of the molecule
! jj: rotational level of the molecule
! arr: arrad of the molecule
! mm: mass of each atom
! rin, rout: vibrational rin and rout of the molecule
! peri(output): period of vibration
! phasead(out): time vs dis
! nsurf: id of PES
! phmode: phase mode
! velphase(out,optional): velocity in a vibration

      use param, only: mnat, pi, mnphase
      implicit none

! input
      integer :: arr,nsurf,jj, phmode
      real(8) :: rin,rout,mm(mnat),edia,xj

! output
      real(8) :: peri,phasead(mnphase,2)
      real(8),optional :: velphase(mnphase)

! local
      integer :: i
      integer,parameter :: ncheb=20
      real(8) :: arg,x(ncheb),rmid,rdif,r,term,v,sum,rmass
      real(8),parameter :: p=4.0d0, q=4.0d0
      integer,parameter :: hnphase = (mnphase-1)/2+1
      real(8)  :: t(hnphase),dis(hnphase),y,dy,peri2,&
         damp,feval(2),z,const,const2

      xj = dble(jj)

      do i = 1,ncheb
        arg = dble(2*i-1)/dble(2*ncheb)*pi
        x(i) = dcos(arg)
      enddo

      rdif = (rout-rin)/2.d0
      rmid = (rout+rin)/2.d0
      sum = 0.d0
      do i = 1,ncheb
        r = rmid+rdif*x(i)
        call diapot(r,arr,v,xj,mm,nsurf)
        term = sqrt((r-rin)*(rout-r)/(edia-v))
        sum = sum+term
      enddo
      if (arr.eq.1) rmass = mm(1)*mm(2)/(mm(1)+mm(2))
      if (arr.eq.2) rmass = mm(2)*mm(3)/(mm(2)+mm(3))
      if (arr.eq.3) rmass = mm(3)*mm(1)/(mm(3)+mm(1))
      peri = sqrt(2.d0*rmass)*sum*pi/dble(ncheb)

!     Calculate molecular distance in a period of vibration
!     r = rin + (rout - rin)*y
      if (phmode .eq. 2) then
         ! change of variables
         !  y = [0 -> 1]
         !  z = y**p/(y**p + (1-y)**q)
         !  dis = rin + (rout-rin)*z
         !  t = int[ d(dis)/v ]
         y = 0.0d0
         dy = 1.0d0/dble(hnphase-1)
         dis(1) = rin
         t(1) = 0.0d0
         damp = 0.0d0
         feval(2) = 0.0d0
         const2 = dsqrt(2.0d0/rmass)
         const = (rout - rin)*dsqrt(rmass/2.0d0)

         if (present(velphase)) then
           velphase(1) = 0.0d0
           do i=2,hnphase
             feval(1) = feval(2)
             y = dy*dble(i-1)
             z = y**p/(y**p + (1.0d0-y)**q)
             dis(i) = rin + (rout-rin)*z
             call diapot(dis(i),arr,feval(2),xj,mm,nsurf)
             damp = (1.0d0 - y)**(q-1.0d0)*y**(p-1.0d0)*(p-p*y+q*y)
             damp = damp/(((1.0d0-y)**q+y**p)**2.0d0)
             if (edia > feval(2)) then
               velphase(i) = dsqrt(edia-feval(2))*const2
               feval(2) = const/dsqrt(edia - feval(2))*damp
             else
               velphase(i) = 0.0d0
               feval(2) = 0.0d0
             end if
             t(i) = t(i-1) + dy*(feval(1)+feval(2))/2.0d0
           enddo
           peri2 = 2.0d0*t(hnphase)
           ! peri2 should be very close to peri
           do i=1,hnphase
             phasead(i,1) = t(i)/peri2
             phasead(i,2) = dis(i)
             phasead(mnphase-i+1,1) = 1.0d0 - t(i)/peri2
             phasead(mnphase-i+1,2) = dis(i)
             velphase(mnphase-i+1) = -velphase(i)
           enddo
         else
           do i=2,hnphase
             feval(1) = feval(2)
             y = dy*dble(i-1)
             z = y**p/(y**p + (1.0d0-y)**q)
             dis(i) = rin + (rout-rin)*z
             call diapot(dis(i),arr,feval(2),xj,mm,nsurf)
             damp = (1.0d0 - y)**(q-1.0d0)*y**(p-1.0d0)*(p-p*y+q*y)
             damp = damp/(((1.0d0-y)**q+y**p)**2.0d0)
             if (edia > feval(2)) then
               feval(2) = const/dsqrt(edia - feval(2))*damp
             else
               feval(2) = 0.0d0
             end if
             t(i) = t(i-1) + dy*(feval(1)+feval(2))/2.0d0
           enddo
           peri2 = 2.0d0*t(hnphase)
           ! peri2 should be very close to peri
           do i=1,hnphase
             phasead(i,1) = t(i)/peri2
             phasead(i,2) = dis(i)
             phasead(mnphase-i+1,1) = 1.0d0 - t(i)/peri2
             phasead(mnphase-i+1,2) = dis(i)
           enddo
         end if
         peri = peri2
         ! open(111,FILE='TEST.DAT',ACCESS="APPEND")
         ! write(111,*)
         ! do i=1,mnphase
           ! if (present(velphase)) then
             ! write(111,*) phasead(i,1),phasead(i,2),velphase(i)
           ! else
             ! write(111,*) phasead(i,1),phasead(i,2)
           ! end if
         ! enddo
         ! close(111)
!
         ! open(111,FILE='TEST2.DAT',ACCESS="APPEND")
         ! write(111,*)
         ! write(111,*) peri,peri2
         ! close(111)
      endif

      end subroutine period
