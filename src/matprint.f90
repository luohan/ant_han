subroutine matprint(iunit,amat,ni,nj,ndim)
!
! This subroutine prints the elements of the ni rows and nj columns of
! array amat.
!
implicit none
integer, intent(in) :: ni, nj, ndim, iunit
integer :: numcol, jcol, jmin, i, jmax, im, j
double precision, intent(in) :: amat(ndim,ndim)

jcol=5
numcol=5
jmin=1
do
  jmax=min(jcol,nj)
  write(iunit,'(3x,"m-n",9(5x,i4,5x))')(im,im=jmin,jmax)                                  
  do i=1,ni                                                      
    write(iunit,'(1x,i4,1x,9es14.6)')i,(amat(i,j),j=jmin,jmax)                             
  enddo
  jmin=jmax+1
  jcol=jcol+numcol                                                  
  if(jmax.eq.nj) exit
enddo    
!write(iunit,'(2x)') ! add a blank line at the end

end subroutine matprint

