! Small code to calculate the population in each mode
!
! imol = number of the molecule under study (input)
! nbrat = number of atoms (input)
! nvib = number of modes to consider (input)
! rx = current atomic positions in au (input)
! rx0 = initial atomic positions in au (input)
! mass = mass of atoms in au (input)
! px = current atomic linear momenta in au (input)
! nmpop = mode populations (output)

      subroutine popmod(potflag,imol,nbrat,nvib,rx,rx0,mass,  &
                        px,nmpop)

      use param, only: mnat, mu
      use c_initial, only: nmvec, freq
      use c_traj, only: ralpha

      implicit none
      integer i,j,k,imol,nvib,nbrat,potflag
      real*8 rx(3,mnat),rxm(6),px(3,mnat),vx(3,mnat),vxm(6)
      real*8 mass(mnat),nmpop(3*mnat),emod(3*mnat),ekm(3*mnat)
      real*8 epm(3*mnat),rx0(3,mnat),etot

! Calculation of the total energy in modes:
! Emod = SUM_m 0.5d0*p_m^2/mu + 0.5d0*mu*w_m^2*R_m^2 = Ek_m + Ep_m
!
! Determination of Ep_m
      do k=1,nvib
         rxm(k) = 0.0d0     
         do i=1,nbrat
            do j=1,3
               rxm(k) = rxm(k) + dsqrt(mass(i)/mu)*(rx(j,i)-rx0(j,i)) &
                        *nmvec(j,i,k,imol)
                print *,k,i,j,' rx and rx0 = ',rx(i,j),rx0(i,j)
                print *,'Square root = ',dsqrt(mass(i)/mu)
                print *,'Mode = ',nmvec(j,i,k,imol)   
            enddo
         enddo
         epm(k) = 0.5d0*mu*freq(k,imol)**2*rxm(k)**2
       enddo

! Determination of Ek_m

      do k=1,nvib
         vxm(k) = 0.0d0     
         do i=1,nbrat
            do j=1,3
               vx(j,i) = px(j,i)/mass(i)
               vxm(k) = vxm(k) + dsqrt(mass(i)/mu)*vx(j,i)*  &
                        nmvec(j,i,k,imol)
            enddo
         enddo
         ekm(k) = 0.5d0*mu*vxm(k)**2
         emod(k) = ekm(k) + epm(k)
      enddo

! Determination of mode populations 
! WARNING: We do not take into account the fact that the umbrella mode is 
!          badly estimated
! ralpha must be provided in the input file, the default is 1

      if (potflag.eq.3) then

         do i=1,nvib
            if (i.eq.6) then
               nmpop(i) = 977.5069d0/892.0d0*ralpha*emod(i) &
                          /freq(i,imol)-0.5d0
            else
               nmpop(i) = ralpha*emod(i)/freq(i,imol)-0.5d0
            endif
         enddo

      else

         do i=1,nvib
            nmpop(i) = ralpha*emod(i)/freq(i,imol)-0.5d0
         enddo 

      endif

!c Tests
!      etot = 0.0d0
!      do i=1,nvib
!         print *,'ekm(',i,') = ',ekm(i)*autoev,' eV'
!         print *,'epm(',i,') = ',epm(i)*autoev,' eV'
!         print *,'freq(',i,') = ',freq(i,imol)*autocmi,' cm-1'
!         print *,'rxm(',i,') = ',rxm(i)*autoang,' A'
!         print *,'vxm(',i,') = ',vxm(i),' au'
!         print *,'emod(',i,') = ',emod(i)*autoev,' eV'
!         etot = etot + emod(i)
!      enddo
!      print *,'etot = ',etot*autoev,' eV'
!      print *,'POPULATIONS'
!      print *,(nmpop(i),i=1,nvib)
!      stop

      end  subroutine popmod
