      subroutine ranclucub(a,b,c)
!
! Generate natoms random atoms within a cubic of rran(1)*rran(2)*rran(3)
!
! xxm:    coordinates for the molecule (AG)
! rijco:  the smallest distance between two atoms allowed 0.8*R0covalent
! ran:    random number
! iclu:   The times tried to generated a whole set of coordinates   
! ikp:  couting the atoms kept 
! irej:   0: keep, 1: reject
! rijm:      distance between two atoms
      use param, only: mnat, mnfrag, mnmol, mnbond
      use c_struct, only: xxm, rijco, natoms, indmm
      implicit none
      double precision a,b,c

      integer :: iclu,ikp,itry,i,j,k,ipick,inxt,  &
                 iprev(mnat),irest(mnat),itmp(mnat),nrest
      double precision :: rijm,xxn(3),rsmall,ran(3)
!
      iclu   = 0
 10   continue
      do i=1,natoms
       irest(i)=i
      enddo
!
!
      iclu = iclu + 1
      if (iclu.gt.20) then
        write(6,111) 'Tried too many times to generate a random sphere', &
                     ' structure ... quitting'
        stop
      endif
! generate natoms points within the cubic
      itry = 0
      ikp = 0
      do while (ikp .lt. natoms)
!       Randomly pick the next atom to be generated
        nrest=natoms-ikp
        call get_rng(ran(1))
        ipick = 1+int( ran(1)*real(nrest-1) )
        inxt = irest(ipick)
!       put the old irest to temp. storage
        do i=1,nrest
          itmp(i)=irest(i)
        enddo
!
        itry = 0
 20     itry = itry + 1
        if (itry.eq.100000) then
          write(6,*) 'Bad packing.  Start again.'
          goto 10
        endif
        do i=1,3
          call get_rng(ran(i))
        enddo
        xxn(1) = a* ran(1)
        xxn(2) = b* ran(2) 
        xxn(3) = c* ran(3)
!       rijm: distance between the two atoms 
        do i=1,ikp
          rsmall = 0.8d0*rijco( indmm(iprev(i)),indmm(inxt) )
          rijm = 0.0d0
          do k=1,3
            rijm = rijm + ( xxm(k,iprev(i)) - xxn(k) )**2
          enddo
          rijm = dsqrt(rijm)
          if (rijm.lt. 0.8d0*rsmall) goto 20
        enddo
        ikp = ikp + 1
        iprev(ikp) = inxt
        do k=1,3
          xxm(k,inxt) = xxn(k)
        enddo
!       update irest
        k=0
        do i=1,nrest
          if (irest(i).ne.inxt) then
            k=k+1
            irest(k)=itmp(i)
          endif
        enddo
      enddo

 111  format (1x,2a)

      end subroutine ranclucub
