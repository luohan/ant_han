      subroutine driverim(im,iexit)
!
! Computes a full trajectory from start to finish.
! itp_min:   count for steps after which a bond can be deemed as
!            formed
! itp_c_min: rij<rmin: 1, else,0
! itp_max:   count for steps after which a bond can be deemed as
!            broken
! itp_c_max: rij>rmax: 1, else,0
! ifrag:      count on fragments
! isame:      count on fragments identical to original AGs
! istat:      count on steps to average properties
! ce:         Cohesive energy
! ace:        Average cohesive energy
! ace2:       Average of the square of cohesive energy
! sce:        variation of ce
! ape:        Average potential energy
! ape2:       Average of the square of potential energy
! spe:        variation of pe
! ake:        Average kinetic energy
! ake2:       Average of the square of kinetic energy
! ske:        variation of ke
! ate:        Average total (internal) energy
! ate2:       Average of the square of total energy
! ste:        variation of te
! cv:         Heat capacity.
! presst(3,3):    Pressure tensor matrix
! atemp:      Average temperature
! atempm:     Local Store of atemp
! atemp2:     Average of the square of temperature
! stemp:      Variation of temperature
! timestat:   Count time used to stat on properties
! fpart:      partition function
! fparti:     exp(-u/kT) at each step
! nfrpvev:    Store previous number of fragments
! ntrsd:      total nuber of atoms transfered
! ifind:      find an atom
! itrsd(mnat) indice of the atom in the current step found transfered
! ibrkpre:    Previously atom found with broken bond
! ifmdpre:    Previously atom found with broken bond
!
      use param, only: mnmol, mnat, mnsurf, mngauss, mntmod, mntor, &
                       mnfrag, mnmol, mntraj, mnyarray, nmax2, mnbin,&
                       mnbin, mnsurf, mnoutcome, mnbond, pi,         &
                       amutoau, autoev, kb, autofs, autoang, autoatm
      use c_output, only: maxprint, nprint
      use c_struct, only: natoms, natom, xxm, comp, mmoffr, fragind, cvec, &
                          indatom, infree, nh, cell, natinfrag, indfrdep, &
                          mmm, mm0, indmm, mmofag, nfrag, symbol0, arij2,&
                          arij, iatom, iflinear, symbolm
      use c_sys, only: itempzpe, ieqtherm, nsurft, hstep, hstep0, &
                       nsurft, nramp, sysfreq, rampfact, nequil, &
                       n_ramp, nrampstep, methflag, tflag, p0, dimen, &
                       iadjtemp, ntraj, itherm, intflag
      use c_initial, only: pickthr, demin, demax, itosave, ipicktrj,temp0im
      use c_traj, only: ppm, vol, press, bigjtot, lzerotrans, kbtarg, &
                        zpe, isave, nreinit, lzerocom, lzerotrans,  &
                        phase, cim, cre, phop, nsurf, gcim, gcre,  &
                        istep, heatcap, potential, kinetic, tei, te, ke, &
                        pe, bigjtoti, bigj, nstat, step, time, temp
      use c_term, only: termflag

      implicit none
      integer :: im,iexit
!     Used for various Verlet algorithm
! xp:     Coordinates at previous step (time - hstep)
! gvp:    Gradients at previous step (time - hstep)
! pep:    For the simple Verlet algorithm, updating of momentum is one
!         step behind coordinates. pep is used to store the PE of the
!         previous step
      double precision :: xp(3,mnat),gvp(3,mnat),pep,pept
!
      integer :: i,j,k,l,ii,i1,i2,frusflag(mnsurf),newsurf, &
                 iramp,istat,nfrag0,nfrdep,nstatm,nrampt
      logical :: istrue,isdepart,ifupgrad
      double precision :: rhor(mnsurf,mnsurf),ran1,   &
                          rhoi(mnsurf,mnsurf),timeprev,tmp,  &
                          dmag,dmag1,dmag2,gvmag,erott,teint,  &
                          raddist(0:mnbin),detmtbt,presst(3,3)
!
      double precision :: comxxm(3),comppm(3),totcomp,etran
      double precision :: fpart,fparti,ce,ace,ace2,ape,ape2,ake,ake2, &
                          ate,ate2,cv,sce,spe,ske,ste,timestat,lind,  &
                          atemp,atemp2,stemp,minte,minpe,cem,atempm,  &
                          tempzpe,temptarg,rcommin(mnfrag)
      logical lhop
! used for average on energies obove demax and below demax
      double precision eplus,eminus,timeplus,timeminus

! used for FSTU
      double precision :: tu_xx(3,mnat,mnsurf),tu_pp(3,mnat,mnsurf), &
                          tu_cre(mnsurf,mnsurf),tu_cim(mnsurf,mnsurf),&
                          tu_hstep(mnsurf),tu_time(mnsurf), &
                          tu_phase(mnsurf,mnsurf),tu_maxt(mnsurf), &
                          deltforw,deltback,tu_maxtime
      integer :: tu_istep(mnsurf)
      logical :: lfrust

!xxx      double precision step,time
! storage for stuff in forgotten arguments to checkhop; presumably never used
      double precision :: phopmax(mnsurf),phopmin(mnsurf)
      integer :: kev
      double precision :: speck(8,nmax2)


! initialize for this trajectory
      ifupgrad=.false.
!      isdepart=.false.
      natoms=natom(im)
      tempzpe=0.d0
      etran  =0.d0
      if (itempzpe) then
        tempzpe=zpe(im)/(kb*dble(infree))
      endif
      temptarg=temp0im(im)+tempzpe
      kbtarg=kb*temptarg
      if (ieqtherm.ne.0 .and. iadjtemp.eq.3) then 
        nh(2,1) = 0.0d0
        nh(2,2) = 0.0d0
        nh(3,1) = 0.0d0
        nh(3,2) = 0.0d0
        nh(1,1)=2.d0*dble(infree)*kb*temp0im(im)*sysfreq**2
        nh(1,2)=nh(1,1)
      endif
      if (itempzpe)  &
        write(6,106) 'Correction to temperature due to ZPE is ', &
                   tempzpe, ' K'
      if (natoms.eq.2) then
         iflinear=1
       else
         iflinear=2
      endif
      time = 0.d0
      step = hstep
      dmag1 = 0.d0
      dmag2 = 0.d0
      do i=1,nsurft
        phase(i) = 0.d0
        tu_time(i) = -1.d0
      enddo
      lfrust = .false.
      do i=1,natoms
        ii=iatom(im)+i-1
        do j=1,natoms
          arij(i,j) = 0.d0
          arij2(i,j) = 0.d0
        enddo
        symbolm(i)=symbol0(ii)
        mmm(i)=mm0(ii)
        indmm(i)=indatom(ii)
!       Atom/group exchange/transfer termination condition
      enddo
      minpe=1.d50
!      minte=1.d9
      atempm = 0.d0
      atemp2 = 0.d0
      stemp = 0.d0
      iramp = 0
      nrampt = 0
      nstatm = nstat
!
!  Initializing on statistical properties
      fpart = 0.d0
      ape   = 0.d0
      ape2  = 0.d0
      ake   = 0.d0
      ake2  = 0.d0
      ate   = 0.d0
      ate2  = 0.d0
      istat = 0
      timestat = 0.d0
      eplus = 0.d0
      eminus= 0.d0
      timeplus = 0.d0
      timeminus = 0.d0
!      
      call angmom(iflinear,bigj,ppm,xxm,natoms,bigjtoti(im))
      call getgrad(dmag,im)
      call gettemp(im)
      atemp=temp
      tei=ke+pe
      write(6,106)'Total angular momentum   = ',bigjtoti(im),' au'
      write(6,106)'Initial kinetic energy   = ',ke*autoev,' eV'
      write(6,106)'Initial potential energy = ',pe*autoev,' eV'
      write(6,106)'Initial total energy     = ',tei*autoev,' eV'
      write(6,106)'Initial temp             = ',temp,' K'
      write(6,*)
! Initial rij, rij2 distnace matrix and bonding information matrix
      call rijmatr
      call frag
      nfrag0=nfrag

! initialize the position at previous step (-hstep0) for Simple Verlet algorithm
      if (intflag.eq.2) then
        do i=1,natoms
          do j=1,3
           k = (j-1)*3 + i
           xp(j,i) = xxm(j,i) - hstep0 * ppm(j,i)/mmm(i)
          enddo
        enddo
        pept = pe
      endif
!
      if (maxprint) then
        write(6,*)
        write(6,*) 'Initial structure of equilibrium run for AG ',im
        do i=1,natoms
          write(6,1010) symbolm(i),(xxm(j,i)*autoang,j=1,3)
        enddo
        write(6,*)
        write(6,*) 'Initial momenta of equilibrium run for AG ',im
        do i=1,natoms
          write(6,1010) symbolm(i),mmm(i)/amutoau,(ppm(j,i),j=1,3)
        enddo
        write(6,*)
      endif
!
! trajectory output
      if (termflag.eq.2) then
            write(6,1)'   time(fs)     PE(eV)  ', &
                      '|gradV|(eV/A)           '
      else
        if (itherm.eq.2) then
          if (nsurft.eq.1) then
            write(6,1)'   time(fs)      T(K)   ',  &
                      'Pressure(atm)   KE(eV)  ',  &
                      '    PE(eV)      TE(eV)  ',  &
                      '   <T(K)>   st.dev(T(K) '    
!    &                '   Lindemann    <T(K)>  ',
!    &                ' st.dev(T(K))           '
          else
            write(6,1)'   time(fs)      T(K)   ',  &
                      'Pressure(atm)   KE(eV)  ',  &
                      '    PE(eV)      TE(eV)  ',  &
                      '    Probs               ',  &
                      '    Nsurf               '
          endif
        else
          if (nsurft.eq.1) then
            write(6,1)'   time(fs)     T(K)    ', &
                      '   KE(eV)       PE(eV)  ', &
                      '   TE(eV)      <T(K)>   ', &
                      '  st.dev(T(K))          '
!    &                '   TE(eV)      Lindemann',
!    &                '   <T(K)>   st.dev(T(K))'
          else
            write(6,1)'   time(fs)     T(K)    ', &
                      '    KE(eV)     PE(eV)   ', &
                      '    TE(eV)      Probs   ', &
                      '                Nsurf   '
          endif
        endif
      endif

! ######################################################################
! INTEGRATE TRAJECTORY
! ######################################################################
!
      istep = 0
      do 10 while (isave.lt.ntraj)
      istep = istep + 1
!          
! take step
! Save the potential energy of the previous step for simple Verlet algorithm
      if (intflag.eq.2) pep = pept
      timeprev = time
      call takestep2(xp,gvp,im)
      step = time - timeprev
! Gradient needs to be upgraded after xxm have been changed in takestep
! except that velocity Verlet have updated gradient
      if (intflag.eq.3.and.intflag.eq.4) then
        ifupgrad=.false.
      else
        ifupgrad=.true.
      endif
!
!
! Themostat
      if (ieqtherm.ne.0) then
        call gettemp(im)
        call adjtemp(temp,temptarg,im)
      endif
      if (iadjtemp.eq.2 .and. ieqtherm.ne.0) then
! Since for anderson therm, the Momentum of CoM is not zero, coord.
! of CoM (comxx) will change as simulation goes on. Move it back.
          if (lzerocom) call comgen(xxm,mmm,natoms,comxxm,mmofag(im))
          if (lzerotrans) call nocompp(ppm,mmm,natoms,comppm,totcomp,1)
      else
        if (mod(istep,nreinit).eq.0) then
          if (lzerocom) call comgen(xxm,mmm,natoms,comxxm,mmofag(im))
          if (lzerotrans) call nocompp(ppm,mmm,natoms,comppm,totcomp,1)
        endif
      endif

      if (.not.lzerotrans) then
         call nocompp(ppm,mmm,natoms,comppm,totcomp,0)
!    total translational energy
         etran = totcomp*totcomp/(2.d0*mmofag(im))
      endif
      if (ifupgrad) then
!  if xxm has been changed, call getgrad 
        call getgrad(dmag,im)
!       Reinitialize ifupgrad
        ifupgrad=.false.
      endif

! update info at new geometry
      call rijmatr
      call angmom(iflinear,bigj,ppm,xxm,natoms,bigjtot)
      if (itherm.eq.2) then
        call frag
        call fragcom(nfrdep,rcommin)
!      getpress calculate pressure based on the "particles" in the
!      system, i.e. number of fragments and their translation momentum.
        call getpress(dimen,presst)
        call adjpress(presst,press,p0)
!  recalculate volume after cell changed
        vol = detmtbt(cvec)
!  Recalculate gradient after adjpress (position has bee changed)
        call getgrad(dmag,im)
        call getpress(dimen,presst)
        call rijmatr
      endif
! Once minimum distance between two atom in the two fragments exceeds
! threshold, the eqiulibrium run restarted
      call frag
      call fragcom(nfrdep,rcommin)
      if (nfrag.gt.nfrag0) then
        if (ipicktrj.eq.2) then
! Effectively put a hard wall there to pull the leaving atom/group back
! by reversing their moment along its CoM vector
          do k=1,nfrdep
              i2=indfrdep(k)
              do l=1,natinfrag(i2)
                i1=fragind(l,i2)
                tmp=mmm(i1)/mmoffr(i2)
                ppm(1,i1)=ppm(1,i1)-comp(1,k)*tmp
                ppm(2,i1)=ppm(2,i1)-comp(2,k)*tmp
                ppm(3,i1)=ppm(3,i1)-comp(3,k)*tmp
              enddo
          enddo
        else
          if (nfrdep.ge.1) then
            iexit=iexit+1
            goto 9999
          endif
        endif
      endif

!
! reinitialize CSDM
      if (methflag.eq.4) then
      if (istep.gt.2) then
        if( (dmag1-dmag2).lt.0.d0 .AND. (dmag-dmag1).gt.0.d0 ) then
          write(6,*)'Electronic reinitialization!'
          do i=1,nsurft
            i2 = i + nsurft
            cre(i2)=cre(i)
            cim(i2)=cim(i)
            gcre(i2)=gcre(i)
            gcim(i2)=gcim(i)
          enddo
!         update information (check this)
          call getgrad(dmag,im)
        endif
      endif
      dmag2 = dmag1
      dmag1 = dmag
      endif

! multiply hopping probability by stepsize for both surface hopping
! and DM methods
      phop(nsurf) = 1.d0
      do i=1,nsurft
        if (i.ne.nsurf) then
          phop(i)=phop(i)*step
          phop(i)=max(phop(i),0.d0)
!         Note:  phop can be > 1 if the probabilities are changing too
!         quickly.  But this shouldn't happen too often.  For better
!         results we'd need to integrate the hopping probability so
!         that the integrator takes smaller steps when necessary.
          phop(i)=min(phop(i),1.d0)
!         prob of staying in NSURF = 1 - sum of all hopping probs
          phop(nsurf) = phop(nsurf) - phop(i)
        endif
      enddo

! FSTU, check for frustrated hop
      if (methflag.eq.5) then
        call checkfrus(frusflag,tu_maxt)
        do k=1,nsurft
          if ((.not.lfrust).and.frusflag(k).eq.0) then
!           at this time a hop is allowed, save information
!           for k = nsurf, we save the current info
!           when lfrust = true, we are looking for a hopping
!           location, so we do not save info
            do i=1,natoms
            do j=1,3
              tu_xx(j,i,k) = xxm(j,i)
              tu_pp(j,i,k) = ppm(j,i)
            enddo
            enddo
            do i=1,nsurft
              tu_cre(i,k) = cre(i)
              tu_cim(i,k) = cim(i)
              tu_phase(i,k) = phase(i)
            enddo
            tu_hstep(k) = hstep
            tu_istep(k) = istep
            tu_time(k) = time
          endif
        enddo
      endif

! TFS and FSTU check for a hop
      if (methflag.eq.1.or.(methflag.eq.5.and.(.not.lfrust))) then
        lhop = .false.
!xxx        call checkhop(phop,nsurf,lhop,newsurf)
        call checkhop(phop,nsurf,lhop,newsurf,kev,speck,step,time, &
                      phopmax,phopmin)  !added required arguments to checkhop call
      endif

! intercept a frustrated hop for FSTU
      if (methflag.eq.5.and.lhop) then
        if (frusflag(newsurf).ne.0) then
!       this hop would be frustrated
        lfrust = .true.
        lhop = .false.
        tu_maxtime = tu_maxt(newsurf)
        endif
      endif

! handle frustrated hop with FSTU
      if (methflag.eq.5.and.lfrust) then
        deltback = tu_time(nsurf) - tu_time(newsurf)
        deltforw = time - tu_time(nsurf)
        if (tu_time(newsurf).ge.0.d0.and.deltback.lt.deltforw.and.  &
                                         deltback.le.tu_maxtime) then
!         do a TU hop backward
          write(6,*)'BACKWARD HOP!'
          do i=1,natoms
          do j=1,3
            xxm(j,i) = tu_xx(j,i,newsurf)
            ppm(j,i) = tu_pp(j,i,newsurf)
          enddo
          enddo
          do i=1,nsurft
            cre(i) = tu_cre(i,newsurf)
            cim(i) = tu_cim(i,newsurf)
            phase(i) = tu_phase(i,newsurf)
          enddo
          hstep = tu_hstep(newsurf)
          istep = tu_istep(newsurf)
          time = tu_time(newsurf)
          lhop = .true.
        else if (frusflag(newsurf).eq.0.and.deltforw.le.tu_maxtime) then
!         do a TU hop forward
          write(6,*)'FORWARD HOP!'
          lhop = .true.
        else if (deltforw.gt.tu_maxtime) then
!         do a frustrated hop at the original hopping location
          do i=1,natoms
          do j=1,3
            xxm(j,i) = tu_xx(j,i,nsurf)
            ppm(j,i) = tu_pp(j,i,nsurf)
          enddo
          enddo
          do i=1,nsurft
            cre(i) = tu_cre(i,nsurf)
            cim(i) = tu_cim(i,nsurf)
            phase(i) = tu_phase(i,nsurf)
          enddo
          hstep = tu_hstep(nsurf)
          istep = tu_istep(nsurf)
          time = tu_time(nsurf)
          lhop = .true.
        endif
        if (lhop) then
          lfrust = .false.
          call getgrad(dmag,im)
          do i=1,nsurft
!           reset all backward hops
            tu_time(i) = -1.d0
          enddo
        endif
      endif

! hop or do a frustrated hop
      if ((methflag.eq.1.or.methflag.eq.5).and.lhop) then
!       switch surfaces or reflect/ignore frustrated hop
        call hop(newsurf)
        call getgrad(dmag,im)
        do i=1,nsurft
!         reset all backward hops
          tu_time(i) = -1.d0
        enddo
      endif

! for Semiclassical Ehrenfest, change NSURF according to RHOR
      if (methflag.eq.2) then
        call getrho(cre,cim,rhor,rhoi,nsurft)
        tmp = 0.d0
        do i=1,nsurft
          if (rhor(i,i).gt.tmp) then
            tmp = rhor(i,i)
            nsurf = i
          endif
        enddo
      endif

! SCDM and CSDM, check for a decoherence state switch
      if (methflag.eq.3.or.methflag.eq.4) then
        lhop = .false.
! added neglected arguments to decocheck call on 12/3/15 
!xxx        call decocheck(phop,nsurf,lhop)
        call decocheck(phop,nsurf,lhop,kev,speck,step,time,  &
                       phopmax,phopmin)  

        if (lhop) then
          call getgrad(dmag,im)
        endif
      endif

! for steepest descent (TFLAG(1) = 1)
! set PP to zero at every step
      if (tflag(1).eq.1) then
        do i=1,natoms
          do j=1,3
            ppm(j,i) = 0.d0
          enddo
        enddo
      endif
!
! on-the-fly analyses
      if (tflag(1).ne.1) call gettemp(im)
! Momentum for simple Verlet algorithm is one step behind
      if (intflag.eq.2) then
              pept = pe
              pe = pep
      endif
      te = ke + pe
      atempm = atempm + temp*step
      atemp = atempm/time
      atemp2 = atemp2 + (temp**2)*step
      stemp = dsqrt(atemp2/time - (atemp)**2)
!
!  Thermal statistic on some properties
!      minte=min(minte,te)
      minpe=min(minpe,pe)
      if (istep.ge.itosave .and. tflag(1).ne.1) then
        istat  = istat + 1
        timestat = timestat+step
        ate  = ate + te*step
        ate2 = ate2 + (te**2)*step
        if (kinetic) then
          ake  = ake + ke*step
          ake2 = ake2 + (ke**2)*step
        endif
        if (potential) then
          ape  = ape + pe*step
          ape2 = ape2 + (pe**2)*step
        endif
        if (te.ge.demax) then 
           timeplus = timeplus + step
           eplus = eplus + te
        endif
!        temm(istat)=te
!        tempmm(istat)=temp
      endif
!
! write info every nprint steps
      call getrho(cre,cim,rhor,rhoi,nsurft)

      if (mod(istep,nprint).eq.0.or.istep.eq.1) then
          if (ieqtherm.eq.2) then
            if (nsurft.eq.1) then
              write(6,101)time*autofs,temp,press*autoatm,ke*autoev, &
                pe*autoev,te*autoev,atemp,stemp
!    &          pe*autoev,te*autoev,lind,atemp,stemp
            else
              write(6,101)time*autofs,temp,press*autoatm,ke*autoev,  &
                pe*autoev,te*autoev,(rhor(k,k),k=1,nsurft),dble(nsurf)
            endif
          else
            if (nsurft.eq.1) then
              write(6,101)time*autofs,temp,ke*autoev,  &
                pe*autoev,te*autoev,atemp,stemp
!    &          pe*autoev,te*autoev,lind,atemp,stemp
            else
              write(6,101)time*autofs,temp,ke*autoev,   &
                pe*autoev,te*autoev,(rhor(k,k),k=1,nsurft),dble(nsurf)
            endif
          endif
      endif

!     Save the last ntraj for the later reactive collision run.
      if (istep .ge. itosave) then
        teint = te - etran
        if (teint.lt.demax .and. teint.ge.demin) then
          eminus  = eminus + te*step
          timeminus   = timeminus + step
         call get_rng(ran1)
         if (ran1.le.pickthr .and. nfrdep.lt.2) then
          isave=isave+1
          do i=1,natoms
            if (im.eq.1) then
               write(60,'(3f23.15)') (xxm(j,i),j=1,3)
               write(61,'(3f23.15)') (ppm(j,i),j=1,3)
             else
               write(62,'(3f23.15)') (xxm(j,i),j=1,3)
               write(63,'(3f23.15)') (ppm(j,i),j=1,3)
            endif
          enddo
         endif
        endif
      endif

!
! ramp temperature and momentum
      if ((tflag(1).eq.2.or.tflag(1).eq.3).and.istep.ge.nrampstep) then
       nrampt = nrampt + 1
       if ( nrampt.eq.(n_ramp+nequil) .or. istep.eq.nrampstep ) then
        iramp  = iramp + 1
        nrampt = 0
!       Change temperature 
        if (tflag(1).eq.3) then
          temptarg=temptarg+rampfact
          kbtarg=kb*temptarg
!       Ramp momentum 
        else
          do i=1,natoms
            do j=1,3
              ppm(j,i)=ppm(j,i)*rampfact
            enddo
          enddo
        endif
       endif

       if (iramp.gt.nramp) goto 9999
!      starting average on properties for a period of nequil steps
!      The lindemann parameter calc. when ramping temperature is an 
!      average of this time only
       if ( nrampt.eq.n_ramp .or.  nrampt.eq.0 ) then
        atempm = 0.d0
        atemp2 = 0.d0
        stemp  = 0.d0
        nstatm = 1
        istat=0
        timestat=0.d0
        time = 0.d0
        ace  = 0.d0
        ace2 = 0.d0
        ate  = 0.d0
        ate2 = 0.d0
        ake  = 0.d0
        ake2 = 0.d0
        ape  = 0.d0
        ape2 = 0.d0
       endif
      endif

 10   continue
      if (isave.ge.ntraj) iexit=0
!      close(1)
!      close(2)

! ######################################################################
! ######################################################################

! trajectory has ended
 9999  continue
!
! write final coordinates and momenta to output
      write(6,*)
      write(6,*) 'Final information for equilibrium run of AG ',im
        if (ieqtherm.eq.2) then
          if (nsurft.eq.1) then
            write(6,101)time*autofs,temp,press*autoatm,ke*autoev,  &
              pe*autoev,te*autoev,atemp,stemp
!             pe*autoev,te*autoev,lind,atemp,stemp
          else
            write(6,101)time*autofs,temp,press*autoatm,ke*autoev,  &
              pe*autoev,te*autoev,(rhor(k,k),k=1,nsurft)
          endif
        else
          if (nsurft.eq.1) then
            write(6,101)time*autofs,temp,ke*autoev,   &
              pe*autoev,te*autoev,atemp,stemp
!             pe*autoev,te*autoev,lind,atemp,stemp
          else
            write(6,101)time*autofs,temp,ke*autoev,  &
              pe*autoev,te*autoev,(rhor(k,k),k=1,nsurft)
          endif
        endif
      if (maxprint) then
        write(6,*) 'Final structure (A):'
        do i=1,natoms
          write(6,1010) symbolm(i),(xxm(j,i)*autoang,j=1,3)
        enddo
        write(6,*) 'Final momenta (au):'
        do i=1,natoms
          write(6,1010) symbolm(i),mmm(i)/amutoau,(ppm(j,i),j=1,3)
        enddo
      endif
      write(6,106) 'Final angular momentum   = ',bigjtot,' au'
!
!  Thermal statistic on some properties
      fpart = 0.d0
!      do i=1,istat
!        fparti = dexp(-(temm(i)-minte)/(kb*tempmm(i)) )
!        fpart  = fpart + fparti
!        fpart = fpart + dexp(-(temm(i)-minte)/kbt0 )
!      enddo
!      fpart  = fpart*dexp(-(minte-minpe)/kbt0 )
      if (tflag(1).ne.1) then
        ate  = ate/timestat
        ate2 = ate2/timestat
        ste  = dsqrt(ate2 - ate**2)
        write(6,1050) 'Properties at temperature ',atemp- tempzpe,  &
                   ' statistic on ',istat,' steps'
        write(6,1050) 'Potential energy minimum (eV):',minpe*autoev
       if (istat.lt.1) then
         write(6,*) 'No enough steps to average on properties'
       else
        write(6,1050) 'Partition function = ',fpart
        write(6,1060) 'energy (eV)','Aver','Var','Rel Var'
        if (kinetic) then
          ake  = ake/timestat
          ake2 = ake2/timestat
          ske  = dsqrt(ake2 - ake**2)
          write(6,1070) 'Kinetic energy (eV)', ake*autoev,ske*autoev,ske/ake
        endif
        if (potential) then
          ape  = ape/timestat
          ape2 = ape2/timestat
          spe  = dsqrt(ape2 - ape**2)
          write(6,1070) 'Potential energy (eV)', &
                        ape*autoev,spe*autoev,spe/ape
        endif
        write(6,1070) 'Total energy (eV)',  &
                      ate*autoev,ste*autoev,ste/ate
        if (heatcap) then
         cv   = (ate2 - ate**2)/(kb*temp0im(im)**2)
         if (ieqtherm.eq.1) write(6,1050) 'Heat capacity Cv = ', &
                                     cv*autoev,' eV/K'
         if (ieqtherm.eq.2) write(6,1050) 'Heat capacity Cp = ', &
                                     cv*autoev,' eV/K'
        endif
        if (timeplus.lt.1.d-5) then 
          write(6,*) 'No energy higher than DEMAX: ',demax
        else
          eplus = eplus/timeplus
        write(6,1050)'Average energy >= demax ',eplus*autoev,' eV'
        endif
        eminus= eminus/timeminus
        write(6,1050)'Average energy <demax>demin ',eminus*autoev,' eV'
        write(6,1050)'Delta(Eabove-Ebelow) ',  &
                (eplus-eminus)*autoev*23.060549d0,' kcal/mol'
       endif
      endif
      if (ieqtherm.eq.2) then
        write(6,1050) 'Final volume is ',vol*autoang**3,' A^3'
        write(6,1050) 'Final parameters of the box = '
        write(6,1050) 'a = ',cell(1)*autoang,' A'
        write(6,1050) 'b = ',cell(2)*autoang,' A'
        write(6,1050) 'c = ',cell(3)*autoang,' A'
      endif
!  After equilbrium run, if AG is not defragmented, iexit=0 (success)
!  Else, count on failure times
      if (iexit.gt.0 .and. ipicktrj.eq.0) then
        isave=0
        rewind(60)
        rewind(61)
        rewind(62)
        rewind(63)
       elseif (iexit.eq.0) then
        rewind(60)
        rewind(61)
        rewind(62)
        rewind(63)
      endif
!      write(6,*) nfrag,trmax(1)*autoang,indbr(1,1),indbr(1,2)
! reinitialize record 60-63 and isave if failed
!
!  All formats go here
 1    format(1x,6a24)
 101  format(1x,f12.2,10f12.5)
 106  format(1x,a,f20.8,1x,a)
 1010 format(5x,a4,4f15.7)
 1050 format(1x,a35,f20.8,a,i10,a)
 1060 format(1x,a21,3a18)
 1070 format(1x,a21,3f18.8)
 
      end subroutine driverim
