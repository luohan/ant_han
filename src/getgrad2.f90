       subroutine getgrad2(xx,petmp)
!
!  This subroutine getgrad2 is modified based on the subroutine getgrad.
!  It calls getpem to calculate energy for xx instead of xxm in getgrad.
!  It is called in the tunneling calculations because we need to calculate
!  potential for geometries along tunneling path instead of geometries along
!  the classical trajectory that are stored in xxm.
!
!  For nonadiabatic case, only SE method is implemented for tunneling
!
!  J. Zheng, Sep. 2013

! Calls GETPEM once, and then computes several important variables:
! iflinear:      0: an atom; 1: Linear; 2; non-linear
!  INPUT:
! xx,ppm,mmm,symbolm,nsurf,nsurft,iflinear,bigj
!  OUTPUT:
! pe,pem,pema,pemd,gpema,gpemd,gpem,gv,cre,cim,gcre,gcim,phop,phase
! gv(3,mnat)     : Nuclear gradient.
! cre(2*mnsurf)  : Real part of the electronic coefficients
! cim(2*mnsurf)  : Imaginary part of the electronic coefficients
! gcre(2*mnsurf) : Time derivative of real part of electronic coefficients
! gcim(2*mnsurf) : Time derivative of imaginary part of electronic coefficients
! PHOP = Hopping probability (for surface hoppping) or switching
!   probability (for SCDM and CSDM) divided by the stepsize.

!  RVM
! For Mike Hack's method of integration we define the bjk here
! bjk = -2*Re(ajk* R.djk) (adiabatic)
! bjk =  2 * hbar-1 *Im(ajk* pemd(j,k)) (diabatic)
!  RVM
! pe             : Potential energy
! dmag = Magnitude of the nonadiabatic coupling vector (for CSDM)
! GCRE and GCIM = Time derivatives of the real and imaginary parts
!   of the electronic variables, including the DM terms for the
!   SCDM and CSDM methods.
! pem            : Array of adiabatic or diabatic potential energies.
! gpem = Gradients of the adiabatic or diabatic potential energies.

      use param, only: mnat, mnsurf
      use c_struct, only: indmm, symbolm, cell, natoms, mmm
      use c_sys, only: repflag, intflag, nsurft
      use c_traj, only: gcre, gcim, bfunc, phop, phase, gv, pe, &
                        cre, cim, gpema, gpemd, dvec, pema, pemd,  &
                        nsurf, pem, gpem 
      use c_common,only: td,veloc

      implicit none
      integer :: i,j,k,l,m,ii,i2,j2,kk
      integer :: hh,hi
      integer :: im,jcharge,jmulti
      double precision :: xx(3,mnat) 
      double precision :: rhor(mnsurf,mnsurf),rhoi(mnsurf,mnsurf),vdotd(mnsurf,mnsurf)

      double precision :: ppvib(3,mnat),svec(3,mnat,mnsurf), &
                          es,taui(mnsurf),ps(mnsurf),tmp,dum,dvecmag,pdotd, &
                          u2b2(2,2),gu2b2(3,mnat,2,2),dvec2b2(3,mnat),gcred(mnsurf), &
                          gcimd(mnsurf),vd(mnsurf),pdeco(3,mnat),smagms, &
                          rhorc(mnsurf,mnsurf),rhoic(mnsurf,mnsurf),tmpr, &
                          tmpi,dsum2,dmag,sntmp,cstmp,rhotmp, &
                          rhorc2(mnsurf,mnsurf),rhoic2(mnsurf,mnsurf)

      double precision :: u2b3(3,3),gu2b3(3,mnat,3,3),dvec2b3(3,mnat,3,3), &
                          adiab3(3),mat(3,3)

      double precision :: petmp

      double precision :: comxxm(3),comppm(3),mtot,totcomp
      double precision :: bigjm(3),bigjtm

! change methflag to 2 temperorily
!     methflag = 2
      

! GET ENERGIES,pema,pemd,gpema,gpemd,dvec
!     if(im.gt.0) then
!       jcharge=icharge(im)
!       jmulti =imulti(im)
!     else
        jcharge=999
        jmulti =999
!     endif
      call getpem(xx,indmm,symbolm,cell,natoms,pema,pemd,gpema, &
                  gpemd,dvec,mmm,0,nsurf,jcharge,jmulti)
!     write(6,*) 'U(1,2) ',pemd(1,2)*autoev

! for multiple surface trajectories (nsurft > 1)
      if (nsurft.gt.1) then

! ZERO
      do i=1,nsurft
        phop(i) = 0.d0
      enddo
!     Semiclassical Ehrenfest and DM methods

!     convert electronic variables to density matrix
!       if(METHFLAG.EQ.4) THEN 
!         call getrhocsdm(cre,cim,rhor,rhoi,nsurft)
!       else
          call getrho(cre,cim,rhor,rhoi,nsurft)
!       endif
      if (repflag.eq.1) then

!       diabatic representation
        pe = 0.d0
        do k=1,nsurft
        do l=1,nsurft
! integrate phase angle separately
        tmp = phase(l)-phase(k)
        sntmp = sin(tmp)
        cstmp = cos(tmp)
        rhotmp = (rhor(k,l)*cstmp+rhoi(k,l)*sntmp)
!       imaginary terms cancel exactly
        pe=pe+rhotmp*pemd(k,l)
! end
! integrate whole coefficient
!        pe = pe + rhor(k,l)*pemd(k,l)
! end
        enddo
        enddo

        do 15 i=1,3
        do 15 j=1,natoms
        gv(i,j) = 0.d0
        do 15 k=1,nsurft
        do 15 l=1,nsurft
! integrate phase angle separately
        tmp = phase(l)-phase(k)
        sntmp = sin(tmp)
        cstmp = cos(tmp)
        rhotmp = (rhor(k,l)*cstmp+rhoi(k,l)*sntmp)
        gv(i,j)=gv(i,j)+rhotmp*gpemd(i,j,k,l)
! end
! integrate whole coefficient
!        gv(i,j) = gv(i,j) + rhor(k,l)*gpemd(i,j,k,l)
! end
  15    continue

      else if (repflag.eq.0) then

!       adiabatic representation
        pe = 0.d0
        do k=1,nsurft
        pe = pe + rhor(k,k)*pema(k)
!       write(6,*) 'rhork,k)', k, rhor(k,k)
        enddo
!       write(6,*) 'PE in getgrad2', pe*autoev
        do 20 i=1,3
        do 20 j=1,natoms
        gv(i,j) = 0.d0
        do 20 k=1,nsurft
        gv(i,j) = gv(i,j) + rhor(k,k)*gpema(i,j,k)
        do 20 l=1,nsurft
! integrate whole coefficient
!        gv(i,j) = gv(i,j) - 2.d0*rhor(k,l)*dvec(i,j,k,l)*pema(k)
! end
! integrate phase angle separately
        tmp = phase(k)-phase(l)
        sntmp = sin(tmp)
        cstmp = cos(tmp)
        rhotmp = (rhor(k,l)*cstmp-rhoi(k,l)*sntmp)
        gv(i,j) = gv(i,j) - 2.d0*rhotmp*dvec(i,j,k,l)*pema(k)
! end
  20    continue

      else
        write(6,*)'REPFLAG = ',repflag,' in GETGRAD'
        stop
      endif

! TIME-DERIVATIVES OF THE ELECTRONIC COORDINATES
!     All methods, DM methods add terms later
        if (repflag.eq.1) then
! RVM If we're not using Mike Hack's method define phop as before
         if(intflag.ne.6) then
!         diabatic rep
          do i=1,nsurft
            gcim(i) = 0.d0
            gcre(i) = 0.d0
            do j=1,nsurft
! integrate whole coefficient
!              gcre(i) = gcre(i) - cim(j)*pemd(i,j)
!              gcim(i) = gcim(i) + cre(j)*pemd(i,j)
! end
! integrate phase angle separately
              if (i.ne.j) then
                tmp = phase(j)-phase(i)
                sntmp = sin(tmp)
                cstmp = cos(tmp)
                tmpr = -(sntmp*cre(j)-cstmp*cim(j))*pemd(i,j)
                tmpi = -(sntmp*cim(j)+cstmp*cre(j))*pemd(i,j)
                gcre(i) = gcre(i) + tmpr
                gcim(i) = gcim(i) + tmpi
                if (i.eq.nsurf) then
                  phop(j) = -2.d0*(tmpr*cre(i)+tmpi*cim(i)) &
                     /(cre(i)**2+cim(i)**2)
                endif 
              endif 
! end
            enddo
          enddo
!  RVM Mike Hack's method in diabatic rep
!  Define bjk for hopping probabilities: gkj = (integral(bjk+) + integral(bjk-))/akk
         elseif(intflag.eq.6) then
          do i=1,nsurft
            gcim(i) = 0.d0
            gcre(i) = 0.d0
            do j=1,nsurft
! integrate whole coefficient
!              gcre(i) = gcre(i) - cim(j)*pemd(i,j)
!              gcim(i) = gcim(i) + cre(j)*pemd(i,j)
! end
! integrate phase angle separately
              if (i.ne.j) then
                tmp = phase(j)-phase(i)
                sntmp = sin(tmp)
                cstmp = cos(tmp)
                tmpr = -(sntmp*cre(j)-cstmp*cim(j))*pemd(i,j)
                tmpi = -(sntmp*cim(j)+cstmp*cre(j))*pemd(i,j)
                gcre(i) = gcre(i) + tmpr
                gcim(i) = gcim(i) + tmpi
              endif
             enddo
            enddo
            do i = 2, nsurft
             do j = 1, i-1
                tmp = phase(j)-phase(i)
                sntmp = sin(tmp)
                cstmp = cos(tmp)
                tmpr = -(sntmp*cre(j)-cstmp*cim(j))*pemd(i,j)
                tmpi = -(sntmp*cim(j)+cstmp*cre(j))*pemd(i,j)
                  bfunc(j,i) = -2.d0*(tmpr*cre(i)+tmpi*cim(i))
! end
             enddo
            enddo
        endif 
        elseif (repflag.eq.0) then
!         adiabatic rep
!         compute velocity-dot-dvec
!         write(6,*) "veloc =",veloc
!         veloc = 0d0
! normalized tunneling direction vector td
          call normd(td,natoms)
          do 10 k=1,nsurft
          do 10 l=1,nsurft
          vdotd(k,l) = 0.d0
          do 10 i=1,3
          do 10 j=1,natoms
!           vdotd(k,l) = vdotd(k,l) + dvec(i,j,k,l)*ppm(i,j)/mmm(j)
            vdotd(k,l) = vdotd(k,l) + dvec(i,j,k,l)*td(i,j)*veloc
!         write(6,*) "dvec ",dvec(i,j,k,l)
  10      continue
!         write(6,*) "vdotd ", vdotd(1,2)
!  
! RVM If we're not using Mike Hack's method define phop as before
         if(intflag.ne.6) then
          do i=1,nsurft
! integrate whole coefficient
!            gcre(i) =  cim(i)*pema(i)
!            gcim(i) = -cre(i)*pema(i)
! end
! integrate phase angle separately
            gcre(i) = 0.d0
            gcim(i) = 0.d0
            do j=1,nsurft
! end
! integrate whole coefficient
!              gcre(i) =  gcre(i) - cre(j)*vdotd(i,j)
!              gcim(i) =  gcim(i) - cim(j)*vdotd(i,j)
! end
! integrate phase angle separately
              if (i.ne.j) then
                tmp = phase(j)-phase(i)
                sntmp = sin(tmp)
                cstmp = cos(tmp)
                tmpr = -(cstmp*cre(j)+sntmp*cim(j))*vdotd(i,j)
                tmpi = -(cstmp*cim(j)-sntmp*cre(j))*vdotd(i,j)
                gcre(i) = gcre(i) + tmpr
                gcim(i) = gcim(i) + tmpi
                if (i.eq.nsurf) then
                  phop(j) = -2.d0*(tmpr*cre(i)+tmpi*cim(i)) &
                     /(cre(i)**2+cim(i)**2)
                endif
               endif
! end
            enddo
          enddo
!  RVM Mike Hack's method in adiabatic rep
!  Define bjk for hopping probabilities: gkj = (integral(bjk+) + integral(bjk-))/akk
        elseif(intflag.eq.6) then
          do i=1,nsurft
! integrate whole coefficient
!            gcre(i) =  cim(i)*pema(i)
!            gcim(i) = -cre(i)*pema(i)
! end
! integrate phase angle separately
            gcre(i) = 0.d0
            gcim(i) = 0.d0
            do j=1,nsurft
! end
! integrate whole coefficient
!              gcre(i) =  gcre(i) - cre(j)*vdotd(i,j)
!              gcim(i) =  gcim(i) - cim(j)*vdotd(i,j)
! end
! integrate phase angle separately
              if (i.ne.j) then
                tmp = phase(j)-phase(i)
                sntmp = sin(tmp)
                cstmp = cos(tmp)
                tmpr = -(cstmp*cre(j)+sntmp*cim(j))*vdotd(i,j)
                tmpi = -(cstmp*cim(j)-sntmp*cre(j))*vdotd(i,j)
                gcre(i) = gcre(i) + tmpr
                gcim(i) = gcim(i) + tmpi
               endif 
             enddo
            enddo
            do i = 2, nsurft
             do j= 1, i-1 
                tmp = phase(j)-phase(i)
                sntmp = sin(tmp)
                cstmp = cos(tmp)
                tmpr = -(cstmp*cre(j)+sntmp*cim(j))*vdotd(i,j)
                tmpi = -(cstmp*cim(j)-sntmp*cre(j))*vdotd(i,j)
                bfunc(j,i) = -2.d0*(tmpr*cre(i)+tmpi*cim(i))
              enddo
             enddo
         endif
! end
        else
          write(6,*)'REPFLAG = ',repflag,' in GETGRAD'
          stop
        endif
        do i = 1, nsurft
          i2 = i + nsurft
          do j = 1, nsurft
            j2 = j + nsurft
            gcre(i2) = gcre(i)
            gcre(j2) = gcre(j)
          enddo
        enddo
!
! save PEM as the diagonal elements of whichever representation we are using
!
      do i=1,nsurft
        if (repflag.eq.0) then
          pem(i) = pema(i)
          do j=1,3
            do k=1,natoms
              gpem(j,k,i) = gpema(j,k,i)
            enddo
          enddo
        else
          pem(i) = pemd(i,i)
          do j=1,3
            do k=1,natoms
              gpem(j,k,i) = gpemd(j,k,i,i)
            enddo
          enddo
        endif
      enddo

! for single surface trajectories
      else
        pe = pema(nsurf)
        pem(1) = pema(1)
        do i=1,3
          do j=1,natoms
            gv(i,j)=gpema(i,j,nsurf)
          enddo
        enddo
        gcre(1) = 0.d0
        gcim(1) = 0.d0
      endif
!
 111  format(1x,100g10.3)
!     methflag = 4
      petmp = pe

      end subroutine getgrad2
