      subroutine vwkb(arr,mm,edia,xj,xv,rin,rout,nsurf, &
                                  rmin,rmax,emin,emax,ierror)

! Computes the WKB vibrational action at the energy EDIA and
! for the rotational state XJ.  For the atom-diatom initial
! conditions method (INITx = 3) only.  From NAT8.1.

      use param, only: mnat, pi
      implicit none

      integer arr,i,nsurf
      ! set number of Chebyshev quadrature
      integer,parameter :: ncheb = 20
      double precision mm(mnat),edia,sum0,arg,x(ncheb),wgt(ncheb), &
        v,rin,rout,rmass,term,r,rmid,rdif
      double precision, intent(in) ::  rmin,rmax,emin,emax,xj
      double precision, intent(out) :: xv
      logical :: ierror
      ierror = .false.

      do i = 1,ncheb
        arg = dble(i)/dble(ncheb+1)*pi
        x(i) = dcos(arg)
        wgt(i) = pi/dble(ncheb+1)*(dsin(arg))**2
      end do

      ! find turning point and calculate Int_p(r)dr
      call turn(arr,mm,edia,xj,rin,rout,nsurf,rmin,rmax,emin,emax)
      ! write(6,*) "rin:",rin,"rout",rout
      if (rin .gt. rout ) then
        print*,'Outer turning point smaller than inner turning', &
     &      ' point in vwkb, trajectory'
!       stop
        xv = -1.0d0
        return
      else if ((rout - rin) .lt. 1.d-5) then
        xv = -0.5d0
      else
        rdif = 0.5d0*(rout-rin)
        rmid = 0.5d0*(rout+rin)
        sum0 = 0.d0
        do i = 1,ncheb
          r = rmid+rdif*x(i)
          call diapot(r,arr,v,xj,mm,nsurf)
          if( (edia-v)/((r-rin)*(rout-r)) .lt. 0.0d0)then
            print*,'WARNING vwkb,  attempting ', &
     &       'to take a sqrt of a negative number ',edia-v
            print *,'Change guess in diamin?'
            ierror = .true.
            return
            ! stop
          else
            term = sqrt((edia-v)/((r-rin)*(rout-r)))
          endif
          sum0 = sum0 + wgt(i)*term
        end do
      if (arr.eq.1) rmass = mm(1)*mm(2)/(mm(1)+mm(2))
      if (arr.eq.2) rmass = mm(2)*mm(3)/(mm(2)+mm(3))
      if (arr.eq.3) rmass = mm(3)*mm(1)/(mm(3)+mm(1))
        xv = sqrt(2.d0*rmass)*rdif**2*sum0/pi-0.5d0
      endif

      end subroutine vwkb
