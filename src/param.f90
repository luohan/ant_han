 module param
! This file stores constants used for dimensioning arrays and for unit conversions.

      integer, parameter :: mnmol=2        ! max number of atom groups
      integer, parameter :: mnbond=100     ! max number of bonds for one atom (maximum coord. number)
      integer, parameter :: mnat=100       ! max number of total atoms (total for all molecules)
      integer, parameter :: mnfrag=mnat    ! max fragments to be analysed
      integer, parameter :: mnsurf=26      ! max number of electronic states
      integer, parameter :: mntraj=1000000 ! max number of trajectories
      integer, parameter :: mnoutcome=5    ! max number of termination condition outcomes
      integer, parameter :: mnbin=300      ! max number of bins in radiadist and binned rcom values
      integer, parameter :: mnphase=3601  ! length of phase array, 0.1 degree
      integer, parameter :: mnjlevel=501   ! maximum rotational level
      integer, parameter :: ntrj_print=1000 ! number of trjs when flush hdf5
!
!     max interaction energy (in hartree) between two AGs that can be viewed as no interaction.
      double precision, parameter :: eintmin=1.0d-6

!     maximum dimension of the Y-array that is integrated
      integer, parameter :: mnyarray=6*mnat+5*mnsurf+3*mnsurf*(mnsurf-1)/2

!     RVM
!     maximum number of trajectories that should have hopped but did not
      integer, parameter :: nmax2=100
!
      integer, parameter :: mntor=4         ! JZ torsion
      integer, parameter :: mngauss=512     ! JZ Gauss-legendre quadrature
      integer, parameter :: mntmod=3*mnat-5 ! JZ maximum tunneling modes

!     conversions from CODATA 2000
      double precision :: amutoau,kb,autoang,autoev,autofs,mu,autocmi,pi, autoatm
      !parameter(pi=3.1415926536d0)
      parameter(pi=dacos(-1.0d0))
!      parameter(amutoau=1822.844987d0) ! best
!      parameter(amutoau=1822.888506d0) ! used by NAT
      parameter(mu=5.48579909070d-4)
      parameter(amutoau=1.0d0/mu)
! Han Luo obtains this value from nist 1 me = 5.48579909070e-4 u
!      parameter(mu=1.d0*amutoau)  ! mass-scaling mass
!  used by lizh
!     parameter(kb=3.1668154d-6)  ! Boltzmann in hartee/K
      parameter(kb=3.16681577d-6)  ! Boltzmann in hartee/K
!     parameter(autoang=0.5291772108d0)
!     parameter(autoang=0.529177100d0)
      parameter(autoang=0.52917721067d0)
! Han Luo obtains this from nist
      parameter(autoev=27.21138602d0)
!      parameter(autofs=2.418884326505d-2)
      parameter(autofs=2.418884326509d-2)
      parameter(autocmi=219474.6313705d0)
!     au to atm for pressure
      parameter (autoatm=8.2387225d-8/(1.d-20*autoang**2)/101325.d0)
!      parameter (autoatm=2.903629796d8)
!  used by Ahren    !RMP remove this part
!      parameter(kb=3.166829d-6)  ! Boltzmann in hartee/K
!      parameter(autoang=0.52917706d0)
!      parameter(autoev=27.2113961d0)
!      parameter(autofs=0.024189d0)
!      parameter(autocmi=219474.63067d0)
 end module param
