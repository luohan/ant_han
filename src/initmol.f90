      subroutine initmol(im)
!
        use param, only: pi, autofs, autoev, kb, autoang
        use c_output, only: maxprint, idebug, minprinticon
        use c_struct, only: natoms, natom, mmm, xxm, xx0, nunit, nrotmods, &
          nvibmods, linearmol, rotaxis, momiprin, mm0, iatom,&
          xxi, imulti, nfrag, cell, nmol, mmofag, &
          symbolm, symbol0, infree, icharge, xunit, natunit, &
          indmm, iflinear, indatom
        use c_sys, only:  itherm, hstep0, p0, temp0, reactcol, repflag, &
          ieqtherm, ieqtherm, periodic, iadjtemp
        use c_initial, only: ewell, temp0im, lfixte, ivibtype, qnmvib0, initp, &
          vibsel, ipicktrj, nsurfi, &
          nsurf0, bandwd, nsurfi, verte, te0fixed, &
          ifreinitrot, deltate, compp, ifeqwithrot, &
          qnmvib0, initp, r0collision, stateselect, &
          b0impact, b_fix, comxx, etrans, transelect, &
          icompp, r0effect, ifr0eff, demin, equilibrium, &
          freq, demax, rran, initx, irottemp, iscaleenergy,irotfreeze,&
          rrad0, bminad, bmaxad, arrad, adbmode, phmode, bparam,ademod, &
          rotrin,rotrout,rottau,rotphasead,rotelevel,rot_n0_level,rot_n0_ntrj,rot_nn0
        use c_traj, only: nsurf, dvec, gpemd, gpema, pema, pemd, ppm,  &
          inh, lzerotrans, bigj, lmodpop, bigjtoti, &
          temp, te, ke, itraj, pe, isave, kbt0, ppi, bigji, vvad, &
          jjad, ecolad,escatad,einti, rinad, routad, tauad, pprelad, phasead
        use c_term, only: t_nstep, termflag

!
! Initialize AG #IM.  Use information computed and stored from PREMOL
! to set up each trajectory.
!
! natoms        natom(im)
! comxxm:       CoM, temperary storage
! linearmol:    0: an atom; 1: linear; 2: non-linear
! ranm:         Temperary storage of random numbers
! etrans:       transition energy
! etot:         total energy
! b0impact:     impact parameter: distance between AG2 and AG1 on the y axis
! equilibrium:   True: equilibrium. After equilibrium, setting it as
!               .false.
! iexit     Exit status: 1: success; 0: failure: The AG to run
!           equilibrium is no longer an AG
! dmfp      Mean free path between two AGs
! einti     Internal energy of diatom for initx = 4

        implicit none
        integer :: im
        integer :: i,j,k,ii,ij,iexit
        double precision :: tmp,tmp1,mue,vrel
        double precision :: comxxm(3),rranm(3),dmfp,ranm(4)
        double precision :: comppm(3),totcomp
        logical :: readstruct,lremovang,ierror
        double precision :: small,dmag,pe1,eup,elow,term
        double precision :: rmass, mdum

        small = 1.d-10

        write(6,*) 'Generating initial conditions for AG ',im
        write(6,*) '-------------------------------------------'
!  Initialize
        iexit=0
        isave=0
        pe=ewell(im)
        kbt0=kb*temp0im(im)
5       natoms=natom(im)
        if ((reactcol.or.termflag.eq.7) .and. (.not.stateselect) .and.  &
              itraj.eq.1 .and. natoms.ge.2 ) then
          equilibrium=.true.
        else
          equilibrium=.false.
        endif
        if ((reactcol.or.termflag.eq.7) .and. (.not.stateselect) .and. &
          itraj.gt.1 .and. natoms.ge.2 ) then
          readstruct=.true.
        else
          readstruct=.false.
        endif
        do i=1,natoms
          ii = iatom(im)+i-1
          mmm(i) = mm0(ii)
          symbolm(i) = symbol0(ii)
          indmm(i) = indatom(ii)
          iflinear = linearmol(im)
          do j=1,3
            xxm(j,i) = 0.d0
            ppm(j,i) = 0.d0
          enddo
        enddo
! Single atom. Skip initial setting.
        if (natoms.eq.1) then
          write(6,*) 'Single atom AG'
          linearmol(im)= 0
          nvibmods(im) = 0
          nrotmods(im) = 0
          mmofag(im)   = mmm(1)
          pe=0.d0
          ke=0.d0
          te=0.d0
          bigjtoti(im)=0.d0
          temp=0.d0
          goto 30
        endif

! GENERATE INITIAL COORDINATES
        if (readstruct) then
!        Read initial conditions from the saved
!        information from the equilibrium run.
          if (reactcol) then
            write(6,108) 'Reactive collision: Initial coordinates are ', &
              ' read from previous equilibrium run'
          else
            write(6,108) 'Termflag=7: Initial coordinates are ',  &
              ' read from previous equilibrium run'
          endif
          do i=1,natoms
            if (im.eq.1) then
              read(60,'(3f23.15)') (xxm(j,i),j=1,3)
              read(61,'(3f23.15)') (ppm(j,i),j=1,3)
            else
              read(62,'(3f23.15)') (xxm(j,i),j=1,3)
              read(63,'(3f23.15)') (ppm(j,i),j=1,3)
            endif
          enddo
          if (idebug) then
            write(6,*) 'Initial coordinates read from equilibrium run'
            do i=1,natoms
              write(6,'(a5,3f23.15)') symbolm(i),(xxm(j,i),j=1,3)
            enddo
            write(6,*) 'Initial momentum read from equilibrium run'
            do i=1,natoms
              write(6,'(a5,3f23.15)') symbolm(i),(ppm(j,i),j=1,3)
            enddo
          endif
          !  once the coordinates are read, set initp(im) = -1
          initp(im) = -1
        else
          if (initx(im).eq.0) then
            !  they were read in readin and stored in xx0
            if(.not.minprinticon) then
              write(6,108)'INITx = 0:  Initial coordinates are ',  &
                'specified by the user'
            endif
            do i=1,natoms
              ii=iatom(im)+i-1
              do j=1,3
                xxm(j,i) = xx0(j,ii)
              enddo
            enddo
          elseif (initx(im).eq.1) then
            write(6,108) 'INITx = 1:  Initial coordinates are ', &
              'randomly determined in a sphere'
            call ranclusp(rran(1,im))
          elseif (initx(im).eq.2) then
            write(6,108) 'INITx =2:  Initial coordinates are ', &
              'randomly determined in a cubic'
            do i=1,3
              rranm(i)=rran(i,im)
            enddo
            call ranclucub(rranm(1),rranm(2),rranm(3))
          elseif (initx(im).eq.3) then
            write(6,108) 'INITx =3:  Initial coordinates are ', &
              'randomly determined in arbitray shape'
            call rancluns
          elseif (initx(im).eq.4) then
            write(6,*)'INITx = 4:  Atom-diatom initial conditions '
            ! Calculate reduced mass of the system
            mdum = mmm(1)+mmm(2)+mmm(3)
            if (arrad .eq. 1) then
              rmass = mmm(3)*(mmm(1)+mmm(2))/mdum
            else if (arrad .eq. 2) then
              rmass = mmm(1)*(mmm(2)+mmm(3))/mdum
            else if (arrad .eq. 3) then
              rmass = mmm(2)*(mmm(1)+mmm(3))/mdum
            else
              write(6,*) "ERROR: Wrong arrad in initmol"
              stop
            end if

            ! stratified sampling to get the rotational level
            if (ademod .eq. 4) then
              if (itraj .le. rot_n0_ntrj(1)) then
                jjad = rot_n0_level(1)
              else
                call binary_search_int(ii,ij,itraj,rot_n0_ntrj,1,rot_nn0,ierror)
                if (ii .eq. ij) then
                  jjad = rot_n0_level(ii)
                else
                  jjad = rot_n0_level(ij)
                end if
              end if
              rinad  = rotrin(jjad)
              routad = rotrout(jjad)
              tauad  = rottau(jjad)
              phasead = rotphasead(:,:,jjad)
              einti  = rotelevel(jjad) !rotational+vibrational
              write(6,202) 'Eint = E(v=',vvad,',j=',jjad,')=',einti*autoev,' eV'
              write(6,200) 'Turning points = ',rinad*autoang, ' and ', &
                routad*autoang,' A'
              write(6,203) 'Period for the vibration = ',tauad*autofs, ' fs'
              if (adbmode .eq. 1) then
                term = 1.0d0 / dsqrt(2.d0*ecolad*rmass)
                bminad = dsqrt(dble(jjad*(jjad+1)))*term
                bmaxad = dsqrt(dble((jjad+1)*(jjad+2)))*term
                write(6,200)'Range of impact parameters = ',bminad*autoang, &
                  ' to ',bmaxad*autoang,' A'
              end if
            end if
200         format(1x,a,f13.5,a,f13.5,a)
201         format(1x,a,3x,f13.5,a)
202         format(1x,a,i3,a,i3,a,4x,f13.5,a)
203         format(1x,a,3x,f13.5,a)

            ierror = .true.
            do while (ierror)
              call atomdiatom(arrad,rrad0,ecolad,escatad,einti,kbt0,&
                pprelad,ademod, &
                adbmode,bminad,bmaxad,bparam,jjad, &
                rinad,routad,tauad,phmode,phasead,&
                mmm,rmass,xxm,ppm,nsurf,ierror)
            end do
          elseif (initx(im).eq.5) then
            write(6,*) 'INITx =5:  Initial coordinates are ',  &
              'randomly determined in a cuboid with ',nunit
            write(6,*) 'building mols each has ',natunit,' atoms: '
            do i=1,natunit
              write(6,107) symbolm(i),(xunit(j,i)*autoang,j=1,3)
            enddo
            call ranmolcub(rran(1,im),rran(2,im),rran(3,im))
          else
            continue
!       endif on initx
          endif
!      Print out coordinates randomly generated
          if (maxprint) then
            if (initx(im).ge.1 .and. initx(im).le.3) then
              write(6,*) 'Structure randomly generated for AG ',im
              do i=1,natoms
                write(6,107) symbolm(i),(xxm(j,i)*autoang,j=1,3)
              enddo
            endif
          endif
!     endif of initial structure
        endif
!
!  Put center of mass at the origin
        if (.not.minprinticon) then
          write(6,*) 'Placing CoM at origin'
          write(6,*)
        endif
!       put center of mass at the origin, for initx(im).eq.0, this has
!       been done in readin
        if (initx(im).ne.0) call comgen(xxm,mmm,natoms,comxxm,mmofag(im))
!
!  INITIAL MOMENTA and initial coord. for normal mode analysis only
10      continue
        if (readstruct) then
          continue
        elseif ( (vibsel(im).eq.1 .or. vibsel(im).eq.2.or.vibsel(im).ge.4)  &
          .and. natoms.gt.1 ) then
!       Judging if AG is linear or not, done in readin for
!       vibsel(im).ne.0 by calling rotprin(iflinear)
          if(.not.minprinticon) then
            write(6,*) 'Normal mode analysis'
          endif
!       Reaction collision for vibselect = 2 program generate random
!       quantum vibrational state qnmvib0
          if (vibsel(im).eq.2) then
!         Generate nvibmods qnmvib0 numbers
            do i=1,nvibmods(im)
              call get_rng(ranm(1))
              tmp1 = 1.d0 - ranm(1)
              qnmvib0(i,im) = int(-dlog(tmp1)*kbt0/freq(i,im)) - 1
              if (qnmvib0(i,im).lt.0) qnmvib0(i,im)=0
            enddo
            write(6,108) 'VibSELECT = 2:', &
              ' randomly generate vibrational states:'
            write(6,*) (qnmvib0(i,im),i=1,nvibmods(im))
          endif
          call popnorm(im)
!       put center of mass at the origin after changing xxm
          call comgen(xxm,mmm,natoms,comxxm,mmofag(im))
          initp(im)=-1
        endif
!
!  INITIAL MOMENTA for initp=0 and initp=1.
        if (initp(im).eq.0) then
          write(6,*) 'INITp = 0:  Setting initial momenta to zero'
          do i=1,natoms
            do j=1,3
              ppm(j,i)=0.d0
            enddo
          enddo
        elseif (initp(im).eq.1) then
          ! random thermal sample
          write(6,108) 'INITp = 1:  Generating initial velocities from', &
            ' a random thermal distribution'
  ! for rantherm method, if frequency is available, add the
  ! generated pp into ppm of zero-point vibration
  ! if the exit equilibrium run unsuccessfully, do not call popnorm
          if (vibsel(im).eq.3 .and. itraj.eq.1 .and. iexit.eq.0) then
            do i=1,nvibmods(im)
              qnmvib0(i,im)=0
            enddo
            call popnorm(im)
!       put center of mass at the origin after changing xxm
            call comgen(xxm,mmm,natoms,comxxm,mmofag(im))
!       save the geometry back to xx0 reset ppm to zero before calling rantherm
            do i=1,natoms
              ii=iatom(im)+i-1
              do j=1,3
                ppm(j,i)=0.d0
                xx0(j,ii)=xxm(j,i)
              enddo
            enddo
          endif
          call rantherm(temp0im(im),im)
          write(6,106) 'Desired temp    = ',temp0im(im),' K'
        elseif (initp(im).eq.-1) then
          if(.not.minprinticon) then
            write(6,108) 'INITp = -1:  Initial momenta selected along ', &
              'with other choices'
            write(6,*)
          endif
        else
          write(6,*) 'ERROR:  INITp = ',initp(im),' is not an option'
          stop
        endif
        if(.not.minprinticon) then
          write(6,*)
        endif
20      continue
!
! Remove CoM momenta: nothing to do for initp=0
        if (initp(im).ne.0 .and. natoms.gt.1) then
           call nocompp(ppm,mmm,natoms,comppm,totcomp,1)
           infree=3*natoms-3
           call gettemp(im)
           if(.not.minprinticon) then
              write(6,*) 'Removed CoM motion'
              write(6,106) 'Calculated temp = ',temp,' K'
              write(6,*)
           endif
        endif
!
!  Rotate the molecule to its own principal orientation
!  nothing to do for atom-diatom
        if (initx(im).eq.4) then
          continue
!       Calculate principal rotational axes
        else
!       Normal mode analysis has changed the geometry
          call rotprin(xxm,mmm,natoms,momiprin,rotaxis,iflinear)
     ! the following will change the nvibmods and etc., Han comment it
     ! only update iflinear here
          !linearmol(im)=iflinear
          !if (iflinear.eq.0) then
          !nvibmods(im) = 0
          !nrotmods(im) = 0
          !elseif (iflinear.eq.1) then
          !if(ivibtype(im).eq.0) then
          !nvibmods(im) = 3*natom(im) - 5
          !elseif(ivibtype(im).eq.1) then
          !nvibmods(im) = 3*natom(im) - 6
          !endif
          !nrotmods(im) = 1
          !else
          !if(ivibtype(im).eq.0) then
          !nvibmods(im) = 3*natom(im) - 6
          !elseif(ivibtype(im).eq.1) then
          !nvibmods(im) = 3*natom(im) - 7
          !endif
          !nrotmods(im) = 3
          !endif

!       Put its coord. to its own principal rotation axes coord. sys.

          if (.not.lmodpop) then
            if (natoms.gt.1 .and. (.not.periodic)) then
              if(.not.minprinticon) then
                write(6,108) 'Transform the AG to its principal axes of ', &
                  'rotation coord. sys.'
              endif
              call rottran(rotaxis,xxm,natoms)
!       transform the momentum into this new orientation, nothing to do
!       for initp=0
              if(.not.minprinticon) then
                write(6,*) 'Transform moment after Rotating the AG'
              endif
              if (initp(im).ne.0) call rottran(rotaxis,ppm,natoms)
            endif
          !  update rotaxis
            call rotprin(xxm,mmm,natoms,momiprin,rotaxis,iflinear)
          endif
          ! update angular momentum
          call angmom(iflinear,bigj,ppm,xxm,natoms,bigjtoti(im)) !update bigj and bigjtoti
        endif
!
! Calculate and remove angular motion
! Nothing to do for atom-diatom, initp=0, single atom
        if (lfixte) lremovang=.true.
        if (initx(im).eq.4 .or. initp(im).eq.0 .or. nmol .eq. 1) then
          lremovang=.false.
! ifreinitrot: whether or not remove angular mom after equil. run
        elseif ( (.not.ifreinitrot) .and. (.not.stateselect) &
          .and. (.not.equilibrium) ) then
          lremovang=.false.
        else
          lremovang=.true.
        endif
        if (irotfreeze) lremovang = .true.
! Han: this might be wrong            if (nmol.eq.1) lremovang=.true.
        if (lremovang) then
          call angmom(iflinear,bigj,ppm,xxm,natoms,bigjtoti(im))
          if(.not.minprinticon) then
            write(6,106) 'Total angular momentum before remove ', &
              bigjtoti(im),' au'
          endif
          call noang(iflinear,bigj,ppm,xxm,mmm,natoms)
          call angmom(iflinear,bigj,ppm,xxm,natoms,bigjtoti(im))
!        Motion for CoM and Rot. are both removed
          if (iflinear.eq.1) then
            infree=3*natoms-5
          else
            infree=3*natoms-6
          endif
          call gettemp(im)
          if(.not.minprinticon) then
            write(6,*) 'Overall angular motion removed'
            write(6,106) 'Calculated temp = ',temp,' K'
            write(6,106) 'Total angular momentum after remove ', &
              bigjtoti(im),' au'
            write(6,*)
          endif
        else
          write(6,106) 'Total angular momentum will not be removed in initmol'
        endif

!     Equilibrium run for the first trajectory
        if (equilibrium) then
!        set overall rotation before entering equilibrium
          if (ifeqwithrot) then
            call initrot(im)
            infree=3*natoms - 3
          endif
!        Set initial CoM motion for Andersen thermostat only
          if (.not.lzerotrans) then
            infree=3*natoms
            call inittrans(temp0im(im),im)
            do i=1,natoms
              tmp=mmm(i)/mmofag(im)
              do k=1,3
                ppm(k,i)=ppm(k,i)+tmp*compp(k,im)
              enddo
            enddo
!        after setting initial CoM translation motion reset compp
            do k=1,3
              compp(k,im)=0.0d0
            enddo
          endif
!    if fixed total energy at the beginning of trajectory then
          if (lfixte) then
!          call getpem(xxm,indmm,symbolm,cell,natoms,pema,pemd,gpema, &
!                      gpemd,dvec,mmm,1,nsurf,icharge(im),imulti(im))
!          if (repflag.eq.1) then
!            pe = pemd(nsurf,nsurf)
!          else
!            pe = pema(nsurf)
!          endif

            call getgrad(dmag,im)

            call gettemp(im)
            te = pe+ke
            if (deltate.gt.small) then
              if (te.gt.(te0fixed+deltate) .or.  &
                te.lt.(te0fixed-deltate) ) then
                if(.not.minprinticon) write(6,*) 'Re-generate initial geometry and momentum'
                goto 5
              endif
            else
              if (pe.ge.te0fixed) then
                if(.not.minprinticon)  write(6,*) 'Re-generate initial geometry and momentum'
                goto 5
              else
                tmp = dsqrt((te0fixed-pe)/ke)
                do i=1,natoms
                  do j=1,3
                    ppm(j,i)=ppm(j,i)*tmp
                  enddo
                enddo
                te = te0fixed
              endif
            endif
          endif
!        Equilibrium run first, save initial geometry for later usage
          write(6,108) 'Equilibrium run first for each AG to generate', &
            ' initial coordinates,'
          write(6,108) 'and momentum'
!        equilibrium run
          write(6,*)
          write(6,*) 'Entering ',iexit+1,'th time equilibrium run'
!        If an AG comes out defragmented, regenerate initial momentum
!        transfer by iexit
!         write(6,*) 'infree=',infree
          if (ieqtherm.ne.0 .and. iadjtemp.eq.3) then
            inh=.true.
          else
            inh=.false.
          endif
          call driverim(im,iexit)
          write(6,*) 'Exit ',iexit,'th time equilibrium run'
          if (iexit.gt.0) then
            write(6,*) 'AG dissociates into ',nfrag, &
              ' pieces after equilibrium run'
            write(6,*) 'Regenerate initial momentum and re-equilibrium'
            if (iexit.gt.5000) then
              write(6,*) 'ERROR: Too many times of equilibrium run'
              write(6,*) 'Equilibrium temperature maybe too high'
              write(6,*) 'Try initp=1 or vibselect=2 and no equilibrium'
              stop
            endif
            goto 5
          endif
          write(6,*)
!        Read initial xxm, ppm for itraj = 1 only (reactcol with
!        stateselect
          do i=1,natoms
            if (im.eq.1) then
              read(60,'(3f23.15)') (xxm(j,i),j=1,3)
              read(61,'(3f23.15)') (ppm(j,i),j=1,3)
            else
              read(62,'(3f23.15)') (xxm(j,i),j=1,3)
              read(63,'(3f23.15)') (ppm(j,i),j=1,3)
            endif
          enddo
          write(6,*)
!        After equilibrium, re-do the previous steps in case the
!        AG is rotated or its CoM is moved elsewhere after a long
!        equilibrium run. Transfer this option by equilibrium
          equilibrium=.false.
          goto 20
        endif
!
! Initial rotational momentum
! only need to do for nmol>1 and nvt or npt but with anderson thermo
        if (initx(im).eq.4) then
          continue
        elseif (lfixte) then
          continue
        elseif (.not.stateselect .and. .not.ifreinitrot .and. .not. irottemp) then
          continue
        else
          write(6,*) 'Set initial rotation states'
          ! spurious angular momentum will also be removed
          call initrot(im)
          infree=3*natoms-3
        endif
        call angmom(iflinear,bigj,ppm,xxm,natoms,bigjtoti(im)) !update bigj

!
! Scale coordinates and momenta to meet etot = pe+ke = ewell+erot+evib
        if (initx(im) .ne. 4 .and. nmol .eq. 1 .and. iscaleenergy) then
          call scale(im)
        endif


!
! Randomly generate initial rotation orientation of each AG and transform
!       the current xxm into this orientation. Operation on angles for
!       reaction collision (easy to integrate later).
!    !!! This can only be done after AG is rotated to its principle axes
!       of rotation
! Nothing to do for single AG, single atom or atom-diatom run
        if (nmol.eq.1) then
          continue
        elseif (.not.periodic) then
          write(6,*) 'Set initial rotation orientation'
          call ranrot(rotaxis,reactcol)
          call rottran(rotaxis,xxm,natoms)
!       transform the momentum into this new orientation, nothing to do
!       for initp=0
          write(6,*) 'Rotate momentum to new orientation'
          if (initp(im).ne.0) call rottran(rotaxis,ppm,natoms)
        endif
!
! print coordinates
        if (maxprint.and..not.minprinticon) then
          write(6,*)'Initial coordinates'
          write(6,105)'    # symb','x (A)','y (A)','z (A)'
          do i=1,natoms
            ii=i+iatom(im)-1
            write(6,101) ii,symbolm(i),(xxm(j,i)*autoang,j=1,3)
          enddo
          write(6,*)
        endif
!
! WRITE INITIAL ENERGY AND ANGULAR MOMENTUM
! Initial setting of rotational momentum changed temperature also
! No overall translation nc=3
!        For a single atom, although nc should be 3, but in order to
!        avoid to divide by 0, set nc to 0, after romoving CoM
!        momenta, T of a single atom should be zero anyway

        call getpem(xxm,indmm,symbolm,cell,natoms,pema,pemd,gpema, &
              gpemd,dvec,mmm,1,nsurf,icharge(im),imulti(im))
        if (repflag.eq.1) then
          pe = pemd(nsurf,nsurf)
          pe1 = pemd(1,1)
        else
          pe = pema(nsurf)
          pe1 = pema(1)
        endif
!     write(6,*)"adiabatic PE ", pema(nsurf)*autoev
!     write(6,*)"adiabatic PEs ", pema(1)*autoev
!     write(6,*)" diabatic PE ", Pemd(nsurf,nsurf)*autoev
!     call getgrad(dmag,im)

        call gettemp(im)
        te = pe+ke
30      continue
!    if fixed total energy at the beginning of trajectory then
        if (lfixte) then
          if (deltate.gt.small) then
            if (te.gt.(te0fixed+deltate) .or.  &
              te.lt.(te0fixed-deltate) ) then
!              write(6,*) 'TE, TE0FIXED, DELTATE',te*autoev, &
!                te0fixed*autoev,deltate*autoev
!              stop
              goto 5
            endif
          else
!              write(6,*) 'TE, TE0FIXED, DELTATE',te*autoev, &
!                te0fixed*autoev,deltate*autoev
            if (pe.ge.te0fixed) then
              if(.not.minprinticon) then
                write(6,*) 'Re-generate initial geometry and momentum'
                write(6,*) 'PE(ev) = ', pe*autoev
              endif
              goto 5
            else
              tmp = dsqrt((te0fixed-pe)/ke)
              do i=1,natoms
                do j=1,3
                  ppm(j,i)=ppm(j,i)*tmp
                enddo
              enddo
              te = te0fixed
            endif
          endif
        endif
!  for vertical excitation using photon energy x +/- y
!              if((pe-pe1)*autoev.lt. 4.9.and.(pe-pe1)*autoev .gt. 4.7)   &
!                   then
        if(verte(im).gt.1d-10) then
          eup = verte(im)+bandwd(im)
          elow = verte(im)-bandwd(im)
          if((pe-pe1).lt.eup.and.(pe-pe1).gt.elow) then
            write(6,*) 'Vertical Excitation (eV)', (pe-pe1)*autoev
          else
!         write(6,*) 'Vertical Excitation (eV)', (pe-pe1)*autoev
            goto 5
          endif
        elseif(nsurfi.lt.nsurf0) then
          write(6,*) 'Vertical Excitation (eV)', (pe-pe1)*autoev
        endif
! end of vertical excitation
        write(6,*)   'Info for this AG only'
        write(6,106) 'Initial kinetic energy  = ', &
          ke*autoev,' eV'
        write(6,106) 'Initial potential energy= ', &
          pe*autoev,' eV'
        write(6,106) 'Initial total energy    = ', &
          te*autoev,' eV'
        write(6,106) 'Initial temp            = ', &
          temp,' K'
        write(6,106) 'Initial angular momentum= ', &
          bigjtoti(im),' au'
        write(6,*)
        if (termflag.eq.7 .and. stateselect .and. ipicktrj.eq.1) then
          if (te.lt.demin .and. te.ge.demax) goto 5
        endif
!
! maxprint: default .false. (minimal printing)
        if (maxprint) then
          write(6,*)'Initial momenta for this AG'
          write(6,105)'    # symb','x (au)','y (au)','z (au)'
          do i=1,natoms
            ii=i+iatom(im)-1
            write(6,101)ii,symbolm(i),(ppm(j,i),j=1,3)
          enddo
          write(6,*)
        endif
!
! Overall initial comxx and compp (Momentum of CoM)
        if (reactcol) then
          mue = mmofag(1)*mmofag(2) / (mmofag(1)+mmofag(2))
          if (im.eq.1) then
!      GENERATE INITIAL CoM momentum for reactcol only
!      The first AG was fixed, the direction of the compp of the 2nd AG
!      is arbitrarily set along +z.
!      The generated Erel = etrans*kB*T with an average of 2kT
            if (.not.transelect) then
              call get_rng(ranm(1))
              tmp = 1.d0
              do while(.true.)
                etrans(im)=tmp+(tmp+1.d0-(1.d0-ranm(1))*dexp(tmp))/tmp
                if (dabs(etrans(im)-tmp).lt.1.d-8) goto 1111
                tmp = etrans(im)
              enddo
!          Real energy of relative translation
 1111            etrans(im) = etrans(im)*kbt0
              etrans(2)  = etrans(1)
            endif
            do j=1,3
              comxx(j,im) = 0.d0
              compp(j,im) = 0.d0
            enddo
            compp(3,im) = -dsqrt( 2.d0 * etrans(im) * mue )
          else
            compp(1,im) = 0.0d0
            compp(2,im) = 0.0d0
!          compp(3,im) = dsqrt( 2.d0 * etrans(im) * mue)
!         mue: reduced mass between the two reactants
!         average of relative speed is sqrt(8kT/(pi mue)): mue reduced
!         mass. average of etrans is 2kT, then speed should be sqrt(4
!         etran/(pi mue) )
!          compp(3,im) = mmofag(2)*dsqrt( 4.d0 * etrans(im) /  &
!                     ( pi*mmofag(1)*mmofag(2)/(mmofag(1)+mmofag(2)) ) )
            write(6,106) 'Relative translation energy generated: ', &
              etrans(im)*autoev,' eV'
            compp(3,im) = dsqrt( 2.d0 * etrans(im) * mue )
            vrel        = compp(3,2)/mmofag(2)-compp(3,1)/mmofag(1)
            write(6,106) 'Relative speed generated:              ', &
              vrel*autoang/autofs,' A/fs'
!
!       GENERATE INITIAL CoM for 2nd AG
            if (.not.b_fix) then
              call get_rng(ranm(1))
              b0impact = dsqrt( ranm(1) )*r0collision
            endif
            write(6,106) 'Initial impact parameter b =           ', &
              b0impact*autoang,' A'
            comxx(1,im)=0.d0
            comxx(2,im)=b0impact
!       In a realistic reaction, the reactants are seperated by mean
!       free path: about 1/(sqrt(2) pi r0**2 n), n: mole density
!       mean free path
!       Conventional comxx
            if (ifr0eff) then
!           ranm(1) = sprng(rng_stream)
!           comxx(3,im)=-ranm(1) * dmfp+comxx(3,im)
!           comxx(3,im) = -dmfp
              dmfp = dsqrt(mmofag(1)/(mmofag(1)+mmofag(2)))*kb*temp0/ &
                (pi*r0effect**2*p0)
              write(6,106) 'Mean free path = ',dmfp*autoang,' A'
              comxx(3,im)=-dsqrt(dmfp**2 - b0impact**2)
            else
              comxx(3,im)=-dsqrt(r0collision**2 - b0impact**2)
            endif
            write(6,106) 'The two AG will collide in             ', &
              -comxx(3,im)/vrel*autofs,' fs'
!         if (t_nstep-int(tmp/hstep0).lt.2000) then
            if (t_nstep-int(etrans(im)/kbt0/hstep0).lt.2000) then
              write(6,*) 'WARNING: Equilbrium steps seems not large ', &
                'enough for the two AGs to collide'
            endif
          endif
!       end of comxx and compp for react collision
!       Nothing to do for atom-diatom and initp(im)=0
        elseif (initp(im).eq.0 .or. initx(im).eq.4) then
          continue
!       for anderson thermostat, setting initial CoM motion
        elseif ( (itherm.ne.0 .and. iadjtemp.eq.2) &
          .or. (nmol.ge.2 .and. (.not.icompp)) ) then
          call inittrans(temp0im(im),im)
        endif
!
!     ORIENT AGs WITH RESPECT TO EACH OTHER
! set rel. translation momenta to compp (which was read in by readin)
        write(6,108)  'Orienting AGs with respect to each other'
        write(6,100) 'Setting initial CoM momenta to     ', &
              (compp(i,im),i=1,3),' au'
        write(6,100) 'Setting initial CoM coordinates to ', &
              (comxx(i,im)*autoang,i=1,3),' A'
        write(6,*)
        do i=1,natoms
          tmp=mmm(i)/mmofag(im)
          do k=1,3
            ppm(k,i)=ppm(k,i)+tmp*compp(k,im)
            xxm(k,i)=xxm(k,i)+comxx(k,im)
          enddo
        enddo
 40     continue
!
! write manipulated data back to xx and pp arrays
        do i=1,natoms
          ii=i+iatom(im)-1
          do j=1,3
            xxi(j,ii) = xxm(j,i)
            ppi(j,ii) = ppm(j,i)
          enddo
        enddo
        do i=1,3
          bigji(i,im) = bigj(i)
        enddo
!  All formats go here
 100    format(1x,a,3f12.5,1x,a)
 101    format(1x,i5,a5,3f18.5)
 105    format(1x,a10,3a12)
 106    format(1x,a,f12.5,1x,a)
 107    format(1x,a5,3f12.5)
 108    format(1x,10a)
!
!
        return
     end subroutine initmol
