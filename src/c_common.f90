module c_common
! a module used to replace /common/
! most of them are actually not used in the code

! HOCO
  use param, only : mnsurf, mnat
  integer :: numstep
! average on Tau in CSDM
  real(8) :: taui(mnsurf)

! c_vel
  real(8) :: td(3,mnat), veloc ! tunneling direction
end module c_common

