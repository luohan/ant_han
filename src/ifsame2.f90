      subroutine ifsame2(ifrag,im,issame)
! This subroutine judges if fragment ifrag is the same as the initial
! reactant im
!    Input:
! indag (in module  c_struct)
! 
! im:         count on reactants
! ifrag:      Count on fragments
!    Output:
! issame:     if the fragment is the same as AG im
      use c_struct, only: fragind, natom, natinfrag, iatom
      implicit none
 
      integer ifrag,im
      integer i,ii,sum0,sumfrag
      logical issame
!
! Initialize
      issame=.true.
      sum0 = 0
      sumfrag =  0
      if(natinfrag(ifrag).ne.natom(im) ) then
        issame = .false.
        goto 999
      endif
      do i = 1, natom(im)
        ii = iatom(im)+i-1
        sum0 = sum0 + ii
      enddo
      do i = 1, natinfrag(ifrag)
        sumfrag=sumfrag+fragind(i,ifrag)
      enddo 
      if(sum0.ne.sumfrag) then
          issame = .false.
          goto 999
      endif

 999  continue
      end subroutine ifsame2
