module c_term
! **********************************************************************
! TERMINATION CONDITIONS
! **********************************************************************
! t_stime    = For TERMFLAG =1, Terminate after T_STIME fs.
! t_gradmax  = For TERMFLAG = 2, tflag(1) = 4, 5
!              Maximum gradient is less than T_GRADMAX in eV/A.
! t_gradmag  = For TERMFLAG =2, tflag(1) = 4, 5 Terminate when the
!              RMS of the gradient is less than T_GRADMAG in eV/A.
! trmin(o)   = For TERMFLAG = 3, Termination distance, input in A, 
!              immediately converted to bohr. bond forming
! trmax(o)   = For TERMFLAG = 3, Termination distance, input in A, 
!              immediately converted to bohr. bond breaking
! t_nstep    = Maximum number of steps for each trajectory.
! termsteps  = Terminate after termsteps after bond forming, breaking,
!              fragmentation, atom/group transfer/exchange is monitored
! nbondform  = Maximum number of forming bonds monitored
! nbondbreak = Maximum number of breaking bonds monitored
! tiform     = atom pairs to be monitored for bond forming
! tibreak    = atom pairs to be monitored for bond breaking
! indfm      = Used for TERMFLAG =3, atom indices of forming bond
! indbr      = Used for TERMFLAG =3, atom indices of broken bond
! outform    = Index to store which pair of bond is formed
! outbreak   = Index to store which pair of bond is broken
! determ     = used for termflag=4. if TE < determ, merge true
! agmerge    = Monitor AG merge (termflag=4). Default .true.
! agdefrag   = Monitor AG defragmentation (termflag=4). Default .true.
! attrans    = Monitor atom/group transfer/exchange (termflag=4).
!              Default .true.
! n_frags    = Fragment number the user input (termflag=7 only).
!              Regardless of bonding patterns. Default: 0 (any >1) 
! n_atoffr   = Number of atoms in the fragment to be monitored
!              (termflag=7 only)
! n_turn     = in react collision: P.dot.R change n_turn times is deemed
!              as a reactive collision (default: 2)
! Used for TERMFLAG = 8 (JZ)
! mntor      = Maximum number of torsions to be monitored  !RMP
! ntorsion   = actual number of torsions input  !RMP
! dihd       = list of four atom index for a dihedral angle/torsion
! dihlim     = the range for a torsion that is used to judge the target 
!              conformation observed 

      use param, only: mntor, mnoutcome
      double precision :: t_stime,t_gradmag,t_gradmax,determ, &
                          trmin(mnoutcome),trmax(mnoutcome),dihlim(2,mntor),dihfin(mntor)
      integer :: termflag,t_nstep,termsteps,nbondform,nbondbreak, &
                 tiform(mnoutcome,2),tibreak(mnoutcome,2),outform,outbreak, &
                 indfm(mnoutcome,2),indbr(mnoutcome,2),n_atoffr,n_frags, &
                 n_turn,dihd(4,mntor),ntorsion
      logical :: agdefrag,agmerge,attrans
end module c_term
