      subroutine atomdiatom(arr,rho,ecol,escat,einti,kbt0,&
                            ppreli,ademod, &
                            adbmode,bmin,bmax,bparam,jj,&
                            rin,rout,tau,phmode,phasead,&
                            mm,rmass,xx,pp,nsurf,ierror)

!     monte carlo selection of initial conditions for
!     the atom-diatom initial conditions (INITx = 4)
!     Some of these routines were borrowed (with modifications)
!     from NAT8.1.
!   INPUT
! jj              : rotational quantum number
! rho             : Initial seperation
! rin             : Inner turning point of initial diatom
! rout            : Outer turning point of initial diatom
! tau             : Period in au of initial diatom
! bmax            : Maximum impact parameter in au
! bmin            : Minimum impact parameter in au
! bparam          : Impact parameter in au
! ppreli          : Initial relative momenta in au
! ecol            : Initial energy in au
! phmode          : Method to initialize phase angle
! phasead         : Molecular ditance vs phase
!   OUTPUT
! xx,pp

      use param, only: mnat, pi, autoang, mnphase, autoev
      use c_traj,only: theta=>thetaad, phi=>phiad, eta=>etaad, &
        psi=>psiad, b0=>b0ad

      implicit none
      ! input
      integer,intent(in) :: arr,jj,adbmode,phmode,ademod,nsurf
      double precision,intent(in) :: rho,einti,bmin,bmax,rin,rout,&
            tau,mm(mnat),rmass,bparam,phasead(mnphase,2),kbt0
      ! output
      double precision,intent(out) ::xx(3,mnat),pp(3,mnat)
      ! in and out
      double precision :: ppreli,ecol,escat
      ! local
      integer :: i,irel,ii,ij
      double precision :: mi,mj,tin,tout,dist,rr
      double precision :: t,xvec(5),ran(5),tmp(12),r(3)
      double precision :: xjv(3),minor(2),b(3),tmp2(3),pbond
      double precision :: snt,cst,snp,csp,sne,cse,bsq
      double precision :: mdum,rmass2,xji,pp0,xxo
      double precision :: gam_dist,ram(2)
      logical,intent(out) :: ierror
      ierror = .false.

      do i=1,5
        call get_rng(ran(i))
        xvec(i) = ran(i)
      enddo

      ! rotational angular momentum
      xji = dsqrt(dble(jj)*(dble(jj)+1.0d0))

      xxo = 2.0d0*xvec(1)-1.0d0
      phi = 2.0d0*pi*xvec(2)
      eta = 2.0d0*pi*xvec(3)
      psi = 2.0d0*pi*xvec(5)
! RMP14
      if ((adbmode.eq.1) .or. (adbmode .eq. 3) ) then
        bsq = bmin**2 + xvec(4)*(bmax**2-bmin**2)
      else if (adbmode.eq.2) then
        bsq = xvec(4)*(bparam)**2
      else if (adbmode .eq. 4) then
        bsq = dcos(1.0d0/3.0d0*dacos(1.0d0-2.0d0*xvec(4))+4.0d0/3.d0*pi)+0.5d0
        bsq = (bmax*bsq)**2
      else if (adbmode .eq. 5) then
        ! strict collinear collision
        bsq = 0.0d0
        xxo = 1.0d0
        phi = 0.0d0
        eta = 0.0d0
      end if
      write(6,*) "Impact parameter (in Angstrom):",dsqrt(bsq)*autoang
      b0 = dsqrt(bsq)

      mdum=(mm(1)+mm(2)+mm(3))

      if (arr.eq.1) rmass2 = mm(1)*mm(2)/(mm(1)+mm(2))
      if (arr.eq.2) rmass2 = mm(2)*mm(3)/(mm(2)+mm(3))
      if (arr.eq.3) rmass2 = mm(3)*mm(1)/(mm(3)+mm(1))

      ! For ademod=1 or 2, ppreli,ecol,escat should already been calculated in
      ! preatomdiatom
      if (ademod .ne. 1 .and. ademod .ne. 2) then
        call get_rng(ram(1))
        call get_rng(ram(2))
        gam_dist = -dlog(ram(1)*ram(2))  !Generate GAM[2,1] dist
        ecol = kbt0*gam_dist
        escat = ecol + einti
        ppreli = dsqrt(2.0d0*ecol*rmass)
        write(6,103)'Initial relative momentum = ',ppreli,' au'
        write(6,103)'Ecol  = Escat - Eint   = ',ecol*autoev,' eV'
      end if

 103  format(1x,a,3x,f13.5,a)

      theta=acos(xxo)
      snt=sin(theta)
      cst=cos(theta)
      snp=sin(phi)
      csp=cos(phi)
      sne=sin(eta)
      cse=cos(eta)

      write(6,"(a8,f20.14)") "Theta:",theta
      write(6,"(A8,F20.14)") "Phi:",phi
      write(6,"(A8,F20.14)") "Eta:",eta
      write(6,"(A8,F20.14)") "Phase:",psi

      if (phmode .eq. 1)then
! molecule starts with maximum or minmum stretch
         t = tau*xvec(5)
         tin = t
         tout = t - tau/2.0d0
         if (tout .ge. 0.0d0) then
            rr = rout
            dist = ppreli*tout/rmass
         else
            rr = rin
            dist = ppreli*tin/rmass
         endif
         ! initialize coordinates
         tmp(1)=rr*snt*csp
         tmp(2)=rr*snt*snp
         tmp(3)=rr*cst
         tmp(4)=0.0d0
         tmp(5) = sqrt(bsq)
         tmp(6) = -sqrt(rho*rho-bsq)-dist
         ! initialize momenta
         pp0=xji/rr
         ! RMP14
         !tmp(7)=pp0*(snp*cse-csp*cst*sne)
         !tmp(8)=-pp0*(csp*cse+snp*cst*sne)
         !tmp(9)=pp0*snt*sne

         tmp(7)=-pp0*(snp*cse+csp*cst*sne)
         tmp(8)=pp0*(csp*cse-snp*cst*sne)
         tmp(9)=pp0*snt*sne

      elseif (phmode .eq. 2) then
! molecule starts with correct phase
         call binary_search(ii,ij,xvec(5),phasead(:,1),1,mnphase,ierror)
         if (ierror) then
           ! failed to get the phase, retry
           return
         end if
         if ( ii .eq. ij) then
           rr = phasead(ii,2)
         else
           rr =( xvec(5) - phasead(ii,1))
           rr = rr*(phasead(ii,2)-phasead(ij,2))/(phasead(ii,1)-phasead(ij,1))
           rr = rr+phasead(ii,2)
         endif
         ! initialize coordinates
         ! diatom is placed at origin now, but it''s location
         ! will be changed later to make COM at the origin
         tmp(1)=rr*snt*csp
         tmp(2)=rr*snt*snp
         tmp(3)=rr*cst
         tmp(4)=0.0d0
         tmp(5) = sqrt(bsq)
         tmp(6) = -sqrt(rho*rho-bsq)
         ! refernce vector K = cross(tmp(1:3),[0,0,1])
         !!tmp(7) = -tmp(2)
         !!tmp(8) = tmp(1)
         !!tmp(9) = 0.0d0
         !!tmp(7:9) = tmp(7:9) / dsqrt(tmp(7)*tmp(7)+tmp(8)*tmp(8)+tmp(9)*tmp(9))
         !
         ! angular momentum vector
         ! xjv vector is eta degree from K
         ! dot(K,tmp(7:9)) = cos(eta)*|tmp(7:9)|
         xjv(1)=xji*( snp*sne+cst*csp*cse)
         xjv(2)=xji*(-csp*sne+cst*snp*cse)
         xjv(3)=xji*(-snt*cse)
         write(6,"(a8,3f20.14)") 'Jxyz:',xjv(1),xjv(2),xjv(3)
         ! compnent of momentum in the direction of bond
         call diapot(rr,arr,t,dble(jj),mm,nsurf)
         pbond = dsqrt(2.0d0*rmass2*(einti-t))
         ! einti -t is the rotational energy at maximum stretch
         ! pbond is the momentum of vibration
         ! if xvec(5)>0.5, the bond is squeezing
         if ( xvec(5) .gt. 0.5) pbond = -pbond
         ! solve equations:
         !    |   0     -r3    r2 |      Jx
         !    |  r3       0   -r1 | P =  Jy
         !    |  r1      r2    r3 |      Pbond*rr
         ! or
         !    |   0       -cst   snt*snp |      Jx/rr
         !    |  cst         0  -snt*csp | P =  Jy/rr
         !    |snt*csp snt*snp       cst |      Pbond

         ! Just use Gauss elimination to* solve
         if (cst .ne. 0.0d0) then
           minor(1) = snt*snp/cst
           minor(2) = - snt*csp/cst
           tmp(9) = (pbond +  xjv(1)/rr*minor(1) + xjv(2)/rr*minor(2))/ &
             (cst + minor(1)*snt*snp - minor(2)*snt*csp)
           tmp(8) = -xjv(1)/rr/cst + tmp(9)*minor(1)
           tmp(7) =  xjv(2)/rr/cst - tmp(9)*minor(2)
           ! Let's try improve the soluton once
           b(1) = -cst*tmp(8) + snt*snp*tmp(9) - xjv(1)/rr
           b(2) =  cst*tmp(7) - snt*csp*tmp(9) - xjv(2)/rr
           b(3) = snt*csp*tmp(7) + snt*snp*tmp(8) +cst*tmp(9) - pbond

           t = dsqrt(b(1)*b(1)+b(2)*b(2)+b(3)*b(3))
           if (t .gt. 1.0d-7) then
             tmp2(3) = (b(3) +  b(1)*minor(1) + b(2)*minor(2))/ &
               (cst + minor(1)*snt*snp - minor(2)*snt*csp)
             tmp2(2) = -(b(1)-tmp2(3)*snt*snp)/cst
             tmp2(1) =  (b(2)+tmp2(3)*snt*csp)/cst
             tmp(7) = tmp(7) - tmp2(1)
             tmp(8) = tmp(8) - tmp2(2)
             tmp(9) = tmp(9) - tmp2(3)
           endif
         else
           ! theta = pi/2, BC in x-y plane
           tmp(9)=xji/rr*sne/snt
           tmp(8)=-xji/rr*cse*csp+Pbond/snt*snp
           tmp(7)= xji/rr*cse*snp+Pbond/snt*csp
         endif
         b(arr) = rr
         call diapot_int(b,arr,nsurf,t)
         t= t+(tmp(7)*tmp(7)+tmp(8)*tmp(8)+tmp(9)*tmp(9))/2.0/rmass2
         write(6,"(a15,f20.14,'eV')") "Energy Loss:",(t-einti)*autoev
      endif

      !atom will went upward
      tmp(10)=0.0d0
      tmp(11)=0.0d0
      tmp(12)=ppreli

! transform Jacobis to x,y,z
! tmp(4-6) and tmp(10-12) correspond to the atom-diatom relative motion
! in the new coordinates we place the center of mass of the diatom
! at the origin, so tmp(4-6) and tmp(10-12) do not need to be changed
        if (arr.eq.1) then
           irel = 3
           ii = 1
           ij = 2
        endif
        if (arr.eq.2) then
           irel = 1
           ii = 2
           ij = 3
        endif
        if (arr.eq.3) then
           irel = 2
           ii = 3
           ij = 1
        endif
        xx(1,irel) = tmp(4)
        xx(2,irel) = tmp(5)
        xx(3,irel) = tmp(6)
        pp(1,irel) = tmp(10)
        pp(2,irel) = tmp(11)
        pp(3,irel) = tmp(12)

        mi = mm(ij)/(mm(ii)+mm(ij))
        mj = mm(ii)/(mm(ii)+mm(ij))
        xx(1,ii) = -tmp(1)*mi
        xx(2,ii) = -tmp(2)*mi
        xx(3,ii) = -tmp(3)*mi
        xx(1,ij) = tmp(1)*mj
        xx(2,ij) = tmp(2)*mj
        xx(3,ij) = tmp(3)*mj

        pp(1,ii) = -tmp(7)-mj*tmp(10)
        pp(2,ii) = -tmp(8)-mj*tmp(11)
        pp(3,ii) = -tmp(9)-mj*tmp(12)
        pp(1,ij) = tmp(7)-mi*tmp(10)
        pp(2,ij) = tmp(8)-mi*tmp(11)
        pp(3,ij) = tmp(9)-mi*tmp(12)

        call rtox(xx,r,1)

        end subroutine atomdiatom


