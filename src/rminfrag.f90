      subroutine rminfrag(ifr1,ifr2,rminfr,i1,i2)
! This subroutine finds the minimum distance between two fragments
! and the indices of the two atoms with the minimum distance
!    Input:
! indatom,rij,ifr1,ifr2
!    Output:
! rminfr   Minimum distance
! i1,i2    Indices of the two atoms with rminfr
      use param, only: autoang
      use c_output, only: idebug
      use c_struct, only: indatom, fragind, rij, natinfrag  
      implicit none
 
      integer ifr1,ifr2,i1,i2
      integer i,j,ind1,ind2
      double precision rminfr,r
 
      rminfr=1.d10
      do i=1,natinfrag(ifr1)
        do j=1,natinfrag(ifr2)
!         Minimum distance between two fragments
          ind1=fragind(i,ifr1)
          ind2=fragind(j,ifr2)
          r=rij(ind1,ind2)
          rminfr=min(rminfr,r)
          if (r.eq.rminfr) then
              i1=indatom(ind1)
              i2=indatom(ind2)
          endif
        enddo
      enddo
      if (idebug)   & 
        write(6,111) 'Minimum distance between ',ifr1,' and ',ifr2, &
              ' is ',rminfr*autoang,' A'

 111  format(1x,a,i10,a,i10,a,e16.8,a)

      end subroutine rminfrag
