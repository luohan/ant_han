      DOUBLE PRECISION FUNCTION detmtbt(m)

!     This routine is to calculate the Det of 3by3 matrix m
      implicit none
      DOUBLE PRECISION m(3,3)

      detmtbt = m(1,1)*( m(2,2)*m(3,3) - m(3,2)*m(2,3) )  &
              - m(1,2)*( m(2,1)*m(3,3) - m(3,1)*m(2,3) )  &
              + m(1,3)*( m(2,1)*m(3,2) - m(3,1)*m(2,2) )
      END FUNCTION detmtbt

