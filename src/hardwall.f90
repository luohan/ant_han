      subroutine hardwall(xx)
! Deal with periodic conditions
! INPUT:
! xxm, ppm, cell, vol
! jaf:        Adjusting factor
! edge(3):    edge of the cell
! xxfrac(3):  Fractorial coordinate of an atom (with cell vector a, b
! and c as unit vector, thus all value are 0-1)
! OUTPUT:
! New ppm, xxm
      use param, only: mnat
      use c_output, only:
      use c_struct, only: natoms, cell, icell
      use c_sys, only: periodic
      use c_traj, only: ppm 

      implicit none
!     Local
      double precision xx(3,mnat)
      integer i,j,k
      double precision edge(3),r
      logical ifsucc

!      initialize
      ifsucc=.false.
!
      if (icell.eq.1 .or. icell.eq.2) then
        if (periodic) then
!         This part is dealt within the potential routine
                continue
        else
!         The center of the cell is at zero
          do j=1,3
            edge(j) = 0.5d0*cell(j)
          enddo
          do i=1,natoms
            do j=1,3
!               xx(j,i)>0
              if (xx(j,i).ge.edge(j)) then
!                 bounce back
                  ppm(j,i) = -ppm(j,i)
                  xx(j,i) =  cell(j) - xx(j,i)
                  ifsucc = .true.
!               xx(j,i)<0
              elseif (xx(j,i).le.-edge(j)) then
!                 bounce back
                  ppm(j,i) = -ppm(j,i)
                  xx(j,i) = -cell(j) - xx(j,i)
                  ifsucc = .true.
              endif
            enddo
          enddo
        endif
      else
        continue
      endif
!      if (ifsucc) write(6,*) "Change positions"
!
      end subroutine hardwall
