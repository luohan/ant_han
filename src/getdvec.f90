      subroutine getdvec(nclu,u2b2,gu2b2,dvec2b2)

! Computes an effective nonadiabatic coupling vector c
! from a 2x2 diabatic matrix.
      use param, only: mnsurf, mntraj, mnat
      use c_sys, only:
      implicit none

      integer :: i1,i2,i,j,k,l,nclu
      double precision :: u2b2(mnsurf,mnsurf)
      double precision :: gu2b2(3,mnat,mnsurf,mnsurf)
      double precision :: dvec2b2(3,mnat,mnsurf,mnsurf)
      double precision :: u11,u12,u22,u21,cc(2,2),v1,v2,sn1,cs1,tmp
      double precision :: u2(2,2),ccc(mnsurf,mnsurf),pema(mnsurf)
      double precision :: v2b2(2)

!     compute adiabatic info
!     diagonalize the 2x2
      do i=1,mnsurf
        do j=1,mnsurf
          ccc(i,j)=0.d0
        enddo
      enddo
      u2(1,1) = u2b2(1,1)
      u2(1,2) = u2b2(1,2)
      u2(2,1) = u2b2(2,1)
      u2(2,2) = u2b2(2,2)
      call diag2(u2,v2b2,cc)
      pema(1)=v2b2(1)
      pema(2)=v2b2(2)
      do i=1,2
        do j=1,2
         ccc(i,j)=cc(i,j)
        enddo
      enddo

      call getdvect(nclu,gu2b2,pema,dvec2b2,ccc)

      end subroutine getdvec
