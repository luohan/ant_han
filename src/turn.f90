      module diaturn_mod
      use param, only: mnat
      implicit none
      integer, save :: nsurf_pass, arr_pass
      double precision, save :: mmm(mnat), xj_pass, edia_pass
      end module diaturn_mod

      subroutine turn(arr,mm,edia,xj,rin,rout,nsurf,rmin,rmax,emin,emax)

! For the atom-diatom initial conditions method (INITx = 3).
! Computes the classical turning points for the energy edia and the
! rotational state xj.  From NAT8.1.

      use param, only: mnat
      use diaturn_mod, only: nsurf_pass, arr_pass, mmm, xj_pass, edia_pass
      implicit none
      integer :: arr, nsurf
      double precision :: mm(mnat),edia,pl,pr,rtbis_turn, xj
      double precision, external :: brent, f_diaturn
      double precision, intent(in) :: rmin,rmax,emin,emax
      double precision, intent(out) :: rin, rout
      double precision :: tinyv
      parameter(tinyv=1.0d-12)

      mmm(:)=mm(:)
      xj_pass=xj
      arr_pass=arr
      nsurf_pass=nsurf
      edia_pass=edia
! to find the minimum of the effective potential, we now call diamin
!      call diamin(rmin,emin,xj,arr,mm,nsurf)
!  direct pass arguments

!      write(6,*)'minimum energy for j=',xj,' is ',emin*27.211d0, &
!       ' at rmin=',rmin

      if (edia .lt. emin) then
        write(6,111)'diatomic energy',edia,' is less than the', &
             ' minimum vib', ' energy ',emin,' for this value of j (',xj,')'
        rin = rmin
        rout = rmin
      elseif (edia .eq. emin) then
        rin = rmin
        rout = rmin
      else
! the turning points will now be found by calling brent.  For a given
! energy, we need only pick values of r which are large (and small)
! enough.  Should these values fail, we print them out, and the user
! can fine tune them.
        pl =  0.5d0
        pr = rmax
        rin=brent(pl,rmin,f_diaturn,1d-15,0.d0, 0.d0)
        if (dabs(edia_pass) < tinyv) then
          rout = pr
        else
          rout=brent(rmin,pr,f_diaturn,1d-15,0.d0, 0.d0)
        endif
      endif
 111  format(a,es16.8,3a,es16.8,a,es16.8,a)
      end subroutine turn

      double precision function f_diaturn(x)
      use diaturn_mod, only: arr_pass, xj_pass, mmm, nsurf_pass, edia_pass
      implicit none
      double precision :: x, el
      call diapot(x,arr_pass,el,xj_pass,mmm,nsurf_pass)
      f_diaturn=el-edia_pass
      end function f_diaturn
