C     SUBROUTINE GEPOL(NUMAT,RA,COORD,volum)                       
!xxx      function volum(NUMAT,RA,COORD,volat)  ! deleted unused argument
      function volum(NUMAT,RA,COORD)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)                                GDH0892
C     INCLUDE 'maxatom.h' 
      INCLUDE 'amsol.h'
      LOGICAL GHOST
      INTEGER NUMAT 
C     added by ns
      CHARACTER*480 KEYWRD                                              GDH0992
      
      PARAMETER (MC=100000,MV=100000)
      
      COMMON/POLI/CV(32,3),XC1(15360),YC1(15360),ZC1(15360)
      COMMON/POLI2/XC2(15360),YC2(15360),ZC2(15360)
      COMMON/PENTA/JVT1(3,60),JVT2(3,4)
      COMMON/CSFE/XE(MC),YE(MC),ZE(MC),RE(MC),IUSE(MC)
      COMMON/PUN/ITO(MV),ISO(MV),ISA(MV),XP(MV),YP(MV),ZP(MV),AP(MV)
      COMMON /KEYWRD/ KEYWRD                                            GDH0992
      COMMON/GEPCOM/ISORTS,NDIV1,NDIV2,IPSUMC,IPSUMS                    GDH0992
C     COMMON /VOLMCM/ VOLUM,VOLM                                        GDH0793
    
      DIMENSION COORD(3,*),RA(*)                                        GDH0992
C added by ns
      LOGICAL VOLM

C     write(6,*) 'in gepol',NP
C     STOP
   
      FRADIO=0.55
      DVEC=1.0
      OMEGA=50.0
      IHOLD=NDIV2                                                       GDH0793
      NDIV2=5                                                           GDH0793
      CALL RDCOOR(NATOM,GHOST,NUMAT,COORD,RA)                           GDH0992
C     IF (INDEX(KEYWRD,'NDIVCD=').NE.0) THEN                            GDH0895
C        IFIX1=INDEX(KEYWRD,'NDIVCD=')                                  GDH0895
C        NDIV2=READIF(KEYWRD,IFIX1)                                     GDH0895
C     ENDIF                                                             GDH0895
      NDIV2=5 
      CALL TES                                                          GDH0793
      CALL DIV2                                                         GDH0793
      NCOR=NATOM                                                        GDH0992


C*****Begining  the calculations ******

      
C     CALL GEOCAV(NCOR,NP,GHOST)                                        GDH0892
      CALL GEOCAV(NCOR,NP,GHOST,VOLUM,VOLM)
C     write(6,*) 'in gepol',NP

      RETURN
      END
C


      SUBROUTINE GEPINF                                                 GDH0893
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)                                GDH0892

      COMMON/PENTA/JVT1(3,60),JVT2(3,4)
      COMMON /ONESCM/ ICONTR(100)                                       DJG0295
      DIMENSION JT1H(3,60),JT2H(3,4)                                    GDH0893
   

C     -----------------------------------------------------------------
C     Has the information of the vertices
C     -----------------------------------------------------------------
     
      DATA JT1H /  1,   6,   2,  1,   2,   3,  1,   3,   4,
     &             1,   4,   5,  1,   5,   6,  7,   2,   6,
     &             8,   3,   2,  9,   4,   3, 10,   5,   4,
     &            11,   6,   5,  8,   2,  12,  9,   3,  13,
     &            10,   4,  14, 11,   5,  15,  7,   6,  16,
     &             7,  12,   2,  8,  13,   3,  9,  14,   4,
     &            10,  15,   5, 11,  16,   6,  8,  12,  18,
     &             9,  13,  19, 10,  14,  20, 11,  15,  21,
     &             7,  16,  17,  7,  17,  12,  8,  18,  13,
     &             9,  19,  14, 10,  20,  15, 11,  21,  16,
     &            22,  12,  17, 23,  13,  18, 24,  14,  19,
     &            25,  15,  20, 26,  16,  21, 22,  18,  12,
     &            23,  19,  13, 24,  20,  14, 25,  21,  15,
     &            26,  17,  16, 22,  17,  27, 23,  18,  28,
     &            24,  19,  29, 25,  20,  30, 26,  21,  31,
     &            22,  28,  18, 23,  29,  19, 24,  30,  20,
     &            25,  31,  21, 26,  27,  17, 22,  27,  28,
     &            23,  28,  29, 24,  29,  30, 25,  30,  31,
     &            26,  31,  27, 32,  28,  27, 32,  29,  28,
     &            32,  30,  29, 32,  31,  30, 32,  27,  31 /

      DATA JT2H /  1,   5,   4,
     &             5,   2,   6,
     &             4,   6,   3,
     &             6,   4,   5 /
      SAVE

C     do i=1,100
C      write(6,*) 'in gepinf', ICONTR(i)
C     enddo
C
C     stop

      IF (ICONTR(43).EQ.1) THEN                                         DJG0295
        ICONTR(43)=2                                                    DJG0295
        DO 87 I=1,3                                                     GDH0893 
        DO 88 J=1,4                                                     GDH0893 
88         JVT2(I,J)=JT2H(I,J)                                          GDH0893
        DO 87 K=1,60                                                    GDH0893
87         JVT1(I,K)=JT1H(I,K)                                          GDH0893
      ENDIF                                                             GDH0893
      RETURN                                                            GDH0893
      END

      SUBROUTINE RDCOOR(NATOM,GHOST,NUMAT,COORD,RA)                     GDH0992
C     ---------------------------------------------------------------
C     Read coordinates and radii
C     ---------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)

      LOGICAL GHOST

      INTEGER NUMAT

      PARAMETER (MC=100000)
      
      COMMON/CSFE/XE(MC),YE(MC),ZE(MC),RE(MC),IUSE(MC)
      COMMON/GEPCOM/ISORTS,NDIV1,NDIV2,IPSUMC,IPSUMS                    GDH0992

      DIMENSION COORD(3,*),RA(NUMAT)                                    GDH0992

      GHOST=.FALSE.
      NATOM=NUMAT                                                       GDH0992
      DO 100 I = 1,NATOM
       XE(I)=COORD(1,I)                                                 GDH0992
       YE(I)=COORD(2,I)                                                 GDH0992
       ZE(I)=COORD(3,I)                                                 GDH0992
C      write(6,*) XE(I), YE(I), ZE(I)
       RE(I)=RA(I)                                                      GDH0992
       IF((RE(I).GT.-0.00001).AND.(RE(I).LT.0.00001)) THEN
          RE(I)=0.0000000E0
          IUSE(I)=0
       ELSE IF(RE(I).GT.0.00001) THEN
          IUSE(I)=4
       ELSE IF(RE(I).LT.-0.00001) THEN
          GHOST=.TRUE.
          IUSE(I)=2
          RE(I)=ABS(RE(I))

      END IF

100   CONTINUE


      RETURN
      END
C
C
C     SUBROUTINE GEOCAV(NCOR,NP,GHOST)
      SUBROUTINE GEOCAV(NCOR,NP,GHOST,VOLUM,VOLM)
C     ------------------------------------------------------------------
C     Compute the surface
C     ------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)

C     INCLUDE 'maxatom.h'                                               GDH0993
      INCLUDE 'amsol.h'
      LOGICAL GHOST
      CHARACTER*480 KEYWRD

      PARAMETER (MC=100000,MV=100000)
      
      COMMON/PENTA/JVT1(3,60),JVT2(3,4)
      COMMON/POLI/CV(32,3),XC1(15360),YC1(15360),ZC1(15360)
      COMMON/POLI2/XC2(15360),YC2(15360),ZC2(15360)
      COMMON/CSFE/XE(MC),YE(MC),ZE(MC),RE(MC),IUSE(MC)
      COMMON/PUN/ITO(MV),ISO(MV),ISA(MV),XP(MV),YP(MV),ZP(MV),AP(MV)
      COMMON/GEPCOM/ISORTS,NDIV1,NDIV2,IPSUMC,IPSUMS                    GDH0992
C     COMMON/VOLMCM/VOLUM,VOLM                                          GDH0793
C     COMMON /KEYWRD/ KEYWRD                                            GDH0992
      LOGICAL VOLM                                                      GDH1093
      DIMENSION IJE(MC)

      DATA ZERO/0.0D0/,FOUR/4.0D0/

      STOT=ZERO
      VOL=ZERO
      VOL2=ZERO
      IJ=0

C begin
C TWINS
      NTRIAN=4**(NDIV2-1)
      FNDIV=60*NTRIAN

C It selects one sphere
      DO 1 I=1,NCOR
      volt=0.d0
      IF(RE(I).EQ.0.D0) GO TO 1
      IF(IUSE(I).EQ.4) THEN
      REI=RE(I)
      SRE=FOUR*PI*REI*REI
      ATS=SRE/FNDIV
      XEI=XE(I)
      YEI=YE(I)
      ZEI=ZE(I)
      DDCO=XEI*XEI+YEI*YEI+ZEI*ZEI
      VCTEN=REI*REI-DDCO
      VCTED=6.0D0*REI
      IIJ=0
      NEJCI = 0  ! JZ
C
C  It fixes which spheres are linked to sphere I
      DO 100 JJ=1,NCOR

        J=JJ
      IF(IUSE(J).GE.2) THEN

      IF(I.NE.J)THEN

      DIJ2=(XEI-XE(J))*(XEI-XE(J))+
     &     (YEI-YE(J))*(YEI-YE(J))+
     &     (ZEI-ZE(J))*(ZEI-ZE(J))
      SRE2=(REI+RE(J))*(REI+RE(J))

          IF((DIJ2.LT.SRE2).AND.
     &       ((SQRT(DIJ2)+RE(J)).GT.REI)) THEN                          GDH0992
            IIJ=IIJ+1
            IJE(IIJ)=J
            NEJCI=IIJ
          END IF

      END IF
      END IF
100   CONTINUE
C 
C It selects one main triangle.
      NSUP=0
      DO 2 J=1,60
      XPL=0.0D0
      YPL=0.0D0
      ZPL=0.0D0
      NTS=0
      NINF=NSUP+1
      NSUP=NINF+NTRIAN-1
C
C  It selects one secondary triangle.
      DO 3 K=NINF,NSUP
      XSL=XC2(K)*REI
      YSL=YC2(K)*REI
      ZSL=ZC2(K)*REI
      XSM=XSL+XEI
      YSM=YSL+YEI
      ZSM=ZSL+ZEI
C
C It fixes if the secondary triangle is inside or outside.
      DO 8 N3=1,NEJCI
      N4=IJE(N3)
C     IF(N4.eq.0) then 
C       write(6,*) 'N3  NEJCI',N3,NEJCI
C     ENDIF
C      write(6,*) XSM, YSM, ZSM, XE(N4), YE(N4), ZE(N4)
C      STOP
      DD=(XSM-XE(N4))*(XSM-XE(N4))+
     &   (YSM-YE(N4))*(YSM-YE(N4))+
     &   (ZSM-ZE(N4))*(ZSM-ZE(N4))
      RREJ=RE(N4)*RE(N4)
      IF(DD.LT.RREJ) GO TO 3
    8 CONTINUE
C
C It prepares the coordinates for the main triangle
      XPL=XPL+XSL
      YPL=YPL+YSL
      ZPL=ZPL+ZSL
      NTS=NTS+1
C TWINS
C     write(6,*) 'NTS=', DD, NEJCI
    3 CONTINUE
C
C  It reduces the secondary triangles to the main triangle.
      IF(NTS.EQ.0)GO TO 2

      ATP=ATS*NTS
      XPL=XPL/NTS
      YPL=YPL/NTS
      ZPL=ZPL/NTS
      RRR=SQRT(XPL*XPL+YPL*YPL+ZPL*ZPL)
C     write(6,*) ATP, ATS, NTS
      FC=REI/RRR

      IJ=IJ+1
      XP(IJ)=XPL*FC+XEI
      YP(IJ)=YPL*FC+YEI
      ZP(IJ)=ZPL*FC+ZEI
      AP(IJ)=ATP
      ITO(IJ)=J 
      ISO(IJ)=I
      ISA(IJ)=I

      STOT=STOT+ATP

      DD=XP(IJ)*XP(IJ)+YP(IJ)*YP(IJ)+ZP(IJ)*ZP(IJ)
C     write(6,*) 'XP(IJ)', XP(IJ)
C     stop
      volt=volt+ATP*(DD+VCTEN)/(VCTED)
      VOL=VOL+ATP*(DD+VCTEN)/(VCTED)
C     write(6,*) 'vol=', VOL, RRR
      
      
    2 CONTINUE

      END IF

      STOT=ZERO                                                               C#

    1 CONTINUE
      VOLUM=VOL                                                         GDH0793
C     write(6,*) 'volume=', VOLUM
C     STOP
C#    NP=IJ

      RETURN
      END
C
      SUBROUTINE DIV2
C     ---------------------------------------------------------------
C     It divides the initial 60 spherical triangles to the level
C     indicated by NDIV
C     ---------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION(A-H,O-Z) 
C     INCLUDE 'maxatom.h'
      INCLUDE 'amsol.h'


      COMMON/POLI/CV(32,3),XC1(15360),YC1(15360),ZC1(15360)
      COMMON/POLI2/XC2(15360),YC2(15360),ZC2(15360) 
      COMMON/PENTA/JVT1(3,60),JVT2(3,4)
      COMMON/GEPCOM/ISORTS,NDIV1,NDIV2,IPSUMC,IPSUMS                    GDH0992

      DIMENSION CVN2(6,3),CVN3(6,3),CVN4(6,3),CVN5(6,3),CC(3)

      DATA ZERO/0.0D0/

      CALL GEPINF                                                       GDH0893

      IJ=0
C*****Level 1****************************
      DO 11 J=1,60
      NV1=JVT1(1,J)
      NV2=JVT1(2,J)
      NV3=JVT1(3,J)
      XV1=CV(NV1,1)
      YV1=CV(NV1,2)
      ZV1=CV(NV1,3)
      XV2=CV(NV2,1)
      YV2=CV(NV2,2)
      ZV2=CV(NV2,3)
      XV3=CV(NV3,1)
      YV3=CV(NV3,2)
      ZV3=CV(NV3,3)
      IF(NDIV2.GT.1) GO TO 23
      CALL CALCEN(XV1,YV1,ZV1,XV2,YV2,ZV2,XV3,YV3,ZV3,CC)
      IJ=IJ+1
      XC2(IJ)=CC(1)
      YC2(IJ)=CC(2)
      ZC2(IJ)=CC(3)
      GO TO 11
C*****Level 2**********************
   23 CONTINUE
      CVN2(1,1)=XV1
      CVN2(1,2)=YV1
      CVN2(1,3)=ZV1
      CVN2(2,1)=XV2
      CVN2(2,2)=YV2
      CVN2(2,3)=ZV2
      CVN2(3,1)=XV3
      CVN2(3,2)=YV3
      CVN2(3,3)=ZV3
      CALL CALVER(CVN2)
      DO 22 J2=1,4
      NV21=JVT2(1,J2)
      NV22=JVT2(2,J2)
      NV23=JVT2(3,J2)
      XV1=CVN2(NV21,1)
      YV1=CVN2(NV21,2)
      ZV1=CVN2(NV21,3)
      XV2=CVN2(NV22,1)
      YV2=CVN2(NV22,2)
      ZV2=CVN2(NV22,3)
      XV3=CVN2(NV23,1)
      YV3=CVN2(NV23,2)
      ZV3=CVN2(NV23,3)
      IF(NDIV2.GT.2) GO TO 33
      CALL CALCEN(XV1,YV1,ZV1,XV2,YV2,ZV2,XV3,YV3,ZV3,CC)
      IJ=IJ+1
      XC2(IJ)=CC(1)
      YC2(IJ)=CC(2)
      ZC2(IJ)=CC(3)
      GO TO 22
C*****Level 3**********************************
   33 CONTINUE
      CVN3(1,1)=XV1
      CVN3(1,2)=YV1
      CVN3(1,3)=ZV1
      CVN3(2,1)=XV2
      CVN3(2,2)=YV2
      CVN3(2,3)=ZV2
      CVN3(3,1)=XV3
      CVN3(3,2)=YV3
      CVN3(3,3)=ZV3
      CALL CALVER(CVN3)
      DO 32 J3=1,4
      NV31=JVT2(1,J3)
      NV32=JVT2(2,J3)
      NV33=JVT2(3,J3)
      XV1=CVN3(NV31,1)
      YV1=CVN3(NV31,2)
      ZV1=CVN3(NV31,3)
      XV2=CVN3(NV32,1)
      YV2=CVN3(NV32,2)
      ZV2=CVN3(NV32,3)
      XV3=CVN3(NV33,1)
      YV3=CVN3(NV33,2)
      ZV3=CVN3(NV33,3)
      IF(NDIV2.GT.3) GO TO 43
      CALL CALCEN(XV1,YV1,ZV1,XV2,YV2,ZV2,XV3,YV3,ZV3,CC)
      IJ=IJ+1
      XC2(IJ)=CC(1)
      YC2(IJ)=CC(2)
      ZC2(IJ)=CC(3)
      GO TO 32
C*****Level 4******************************
   43 CONTINUE
      CVN4(1,1)=XV1
      CVN4(1,2)=YV1
      CVN4(1,3)=ZV1
      CVN4(2,1)=XV2
      CVN4(2,2)=YV2
      CVN4(2,3)=ZV2
      CVN4(3,1)=XV3
      CVN4(3,2)=YV3
      CVN4(3,3)=ZV3
      CALL CALVER(CVN4)
      DO 42 J4=1,4
      NV41=JVT2(1,J4)
      NV42=JVT2(2,J4)
      NV43=JVT2(3,J4)
      XV1=CVN4(NV41,1)
      YV1=CVN4(NV41,2)
      ZV1=CVN4(NV41,3)
      XV2=CVN4(NV42,1)
      YV2=CVN4(NV42,2)
      ZV2=CVN4(NV42,3)
      XV3=CVN4(NV43,1)
      YV3=CVN4(NV43,2)
      ZV3=CVN4(NV43,3)
      IF(NDIV2.GT.4) GO TO 53
      CALL CALCEN(XV1,YV1,ZV1,XV2,YV2,ZV2,XV3,YV3,ZV3,CC)
      IJ=IJ+1
      XC2(IJ)=CC(1)
      YC2(IJ)=CC(2)
      ZC2(IJ)=CC(3)
      GO TO 42
C*****Level 5*************************************
   53 CONTINUE
      CVN5(1,1)=XV1
      CVN5(1,2)=YV1
      CVN5(1,3)=ZV1
      CVN5(2,1)=XV2
      CVN5(2,2)=YV2
      CVN5(2,3)=ZV2
      CVN5(3,1)=XV3
      CVN5(3,2)=YV3
      CVN5(3,3)=ZV3
      CALL CALVER(CVN5)
      DO 52 J5=1,4
      NV51=JVT2(1,J5)
      NV52=JVT2(2,J5)
      NV53=JVT2(3,J5)
      XV1=CVN5(NV51,1)
      YV1=CVN5(NV51,2)
      ZV1=CVN5(NV51,3)
      XV2=CVN5(NV52,1)
      YV2=CVN5(NV52,2)
      ZV2=CVN5(NV52,3)
      XV3=CVN5(NV53,1)
      YV3=CVN5(NV53,2)
      ZV3=CVN5(NV53,3)
      CALL CALCEN(XV1,YV1,ZV1,XV2,YV2,ZV2,XV3,YV3,ZV3,CC)
      IJ=IJ+1
      XC2(IJ)=CC(1)
      YC2(IJ)=CC(2)
      ZC2(IJ)=CC(3)
   52 CONTINUE
   42 CONTINUE
   32 CONTINUE
   22 CONTINUE
   11 CONTINUE
      RETURN
      END

