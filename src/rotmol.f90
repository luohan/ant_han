!******
! ROTMOL
! rotate a molecule
!
      SUBROUTINE rotmol (n, x, y, u)
      IMPLICIT none
 
      INTEGER n, i
      DOUBLE PRECISION x(3, n)
      DOUBLE PRECISION y(3, n)
      DOUBLE PRECISION u(3, 3)
      DOUBLE PRECISION yx, yy, yz
 
      DO 10000 i = 1, n
        yx = u(1, 1) * x(1, i) + u(1, 2) * x(2, i) + u(1, 3) * x(3, i)
        yy = u(2, 1) * x(1, i) + u(2, 2) * x(2, i) + u(2, 3) * x(3, i)
        yz = u(3, 1) * x(1, i) + u(3, 2) * x(2, i) + u(3, 3) * x(3, i)
 
        y (1, i) = yx
        y (2, i) = yy
        y (3, i) = yz
10000   CONTINUE
 
      END SUBROUTINE rotmol
