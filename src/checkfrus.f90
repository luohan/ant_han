      subroutine checkfrus(frusflag,tu_maxt)

! Used for the FSTU method.
! At every step, check to see of a hop would be energetically allowed
! to all other states.  If a hop is allowed to state K, set FRUSFLAG(K) = 0.
! Otherwise, FRUSFLAG(K) = 1.
!  INPUT
! natoms,nsurf,mmm,ppm,pem,dvec
      use param, only: mnsurf
      use c_struct, only: mmm, natoms
      use c_sys, only: nsurft
      use c_traj, only: ppm, pem, dvec, nsurf

      implicit none
      integer :: is,i,j
      integer :: frusflag(mnsurf)
      double precision :: tu_maxt(mnsurf)
      double precision :: sectrm,a,b,a2,ab,radi,scr,deltae,talongd

! check for forbidden hops
      do is = 1,nsurft
       if (is.ne.nsurf) then
        deltae = pem(is) - pem(nsurf)
        a=0.0d0
        b=0.0d0
        do i=1,3
          do j=1,natoms
             scr=dvec(i,j,nsurf,is)/mmm(j)
!            a = momentum.dot.dvec/mass
!            b = dvec.dot.dvec/mass
             a=a+scr*ppm(i,j)
             b=b+scr*dvec(i,j,nsurf,is)
          enddo
        enddo
        a2=a*a
        ab=a/b
!       sectrm = deltae/kinetic energy along d
        if (b.gt.0.d0) then
!         DVEC is nonzero
          sectrm = 2.0d0*deltae*b/a2
          talongd = a2/b/2.d0 ! kinetic energy along d
          tu_maxt(is) = 0.5d0/(deltae - talongd)
          radi = 1.0d0-sectrm
        else
!         DVEC is zero, mark as frustrated
          radi = -1.d0
          tu_maxt(is) = 0.d0
        endif
!       if radi > 0 then delta > kinetic energy along d, so we can hop
!       note that for a hop down, sectrm is negative, so we can always hop
        if(radi.ge.0.0d0) then
!         successful hop
          frusflag(is) = 0
        else
!         frustrated hop
          frusflag(is) = 1
        endif
       else
         frusflag(is) = 0
       endif
      enddo

      end subroutine checkfrus
