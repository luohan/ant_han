      subroutine checkhop(phop,nsurf,lhop,newsurf,kev,speck,  &
                          step,time,phopmax,phopmin)

! Used for surface hopping methods.
! Draw a random number and compare it to PHOP to check for a hop.
! If a hop occurs, LHOP = .TRUE. and NEWSURF = whichever surface
! the system is trying to hop to.  The hop does not actually 
! occur in this subroutine.  Hops (frustrated and successful) 
! occur in HOP.

      use param, only: mnsurf, mntraj, nmax2
      use c_sys, only: nsurft, intflag

      implicit none
! input
      double precision :: phop(mnsurf)
      integer :: nsurf

! output
      logical :: lhop
      integer :: newsurf

! local
      integer :: i,j
      double precision :: dxi,tmp

!  RVM
!  input
      double precision :: step,time
      double precision :: phopmax(mnsurf),phopmin(mnsurf)
!  local
      double precision :: mxtmp,mntmp
!  output
      integer :: kev
      double precision :: speck(8,nmax2)
!  RVM

      call get_rng(dxi)

!c Test: random number before hops       
!      write(6,*) 'Current surface = surface ',nsurf
!      write(6,*) 'Hopping probabilities = ',(phop(i),i=1,nsurft)  
!      write(6,*) 'Random number for the hop = ',dxi
!      write(6,*)

! check for hop
!  RVM if we're not using Mike Hack's method do as before
      if(intflag.ne.6) then
      newsurf = 0
      tmp = 0.d0
      do i=1,nsurft
      tmp = tmp + phop(i)
      if (dxi.le.tmp.and.newsurf.eq.0) then
      newsurf = i
      endif
      enddo
      if (newsurf.eq.0) then
        write(6,*)'NEWSURF = 0 in HOPCHECK!'
        stop
      endif
      endif
! RVM checking phopmax and phopmin
      if(intflag.eq.6) then
      newsurf = 0
      tmp = 0.0d0
      mntmp = 0.0d0
      mxtmp = 0.0d0
      do i=1,nsurft
      tmp = tmp + phop(i)
      mxtmp = mxtmp + phopmax(i)
      mntmp = mntmp + phopmin(i)
      if (dxi.le.tmp.and.newsurf.eq.0) then
      newsurf = i
      endif
      enddo
        if(dxi.gt.tmp) then
         if(dxi.lt.mntmp) then
          kev = kev + 1
          if (kev .gt. nmax2) then
          write(*,*)'Error.  kev = ',kev,' but nmax2 = ',  nmax2
          stop 
          endif
! save data in special K array
                 speck(1,kev)=dble(1)
                 speck(2,kev)=phop(i)
                 speck(3,kev)=phopmin(i)
                 speck(4,kev)=phopmax(i)
                 speck(5,kev)=dxi
                 speck(6,kev)=dble(nsurf)
                 speck(7,kev)=step
                 speck(8,kev)=time
         elseif(dxi.lt.mxtmp) then
          kev = kev + 1
          if (kev .gt. nmax2) then
          write(*,*)'Error.  kev = ',kev,' but nmax2 = ',   nmax2
          stop 
          endif
! save data in special K array
                 speck(1,kev)=dble(1)
                 speck(2,kev)=phop(i)
                 speck(3,kev)=phopmin(i)
                 speck(4,kev)=phopmax(i)
                 speck(5,kev)=dxi
                 speck(6,kev)=dble(nsurf)
                 speck(7,kev)=step
                 speck(8,kev)=time
         endif
       endif     
      endif
      if (newsurf.eq.0) then
        write(6,*)'NEWSURF = 0 in HOPCHECK!'
        stop
      endif
!   RVM

      if (newsurf.ne.nsurf) then
         lhop = .true.
      endif

      end subroutine checkhop
