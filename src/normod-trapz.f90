! DESCRIPTION OF THE SUBROUTINE (References)
! This subroutine is based on the book chapter
! "Generalized Transition State Theory," D. G. Truhlar, A. D. Isaacson, 
! and B. C. Garrett, in Theory of Chemical Reaction Dynamics, edited by 
! M. Baer (CRC Press, Boca Raton, FL, 1985), Vol. 4, pp. 65-137. 
! We have extracted the formula for the Hessian projection from this book 
! chapter and we have subsequently performed a normal mode analysis of the 
! semianalytical projected Hessian.
!
! STRUCTURE OF THE SUBROUTINE (input/output)
! nvibmods2: number of vibrational modes = 3N-6 here (input)
! freq2(i): NH3 frequencies (output)
! nmvec2(i,j,k): NH3 eigenvectors (output)
! zpem2: NH3 zero-point energy (output)
!
! Last modification: 06/26/2008.

      subroutine normod2(nbrat,nvibmods2,freq2,nmvec2,zpem2,zpe2,ltrapz)

      use param, only: mnat, autocmi, autoev,mu
      use c_output, only: minprinttrapz
      use c_struct, only: mmm, xxm, cell, indmm, symbolm
      use c_sys, only: repflag
      use c_initial, only:
      use c_traj, only: pema, pemd, gpema, gpemd, dvec, nsurf, freqlim

      implicit none
      integer :: i,j,k,l,ndim,icount,iend,ilinear,nvibmods2
      integer :: ierr,nbrat,lpos,jpos,icptb,icptub,i1,i2,jmax
      integer :: idum1, idum2 ! needed to fill out an argument list
      double precision :: nhbond(mnat-1),nhangle(mnat-1) ! only needed to fill out an argument list
      double precision :: hh,gv1(3,mnat),gv2(3,mnat)
      double precision :: hess(3*mnat,3*mnat),freqm(3*mnat)
      double precision :: xxm0(3,mnat),ewell2,zpe2
      double precision :: freq2(3*mnat),nmvec2(3*mnat,3*mnat)
      double precision :: proj(3*mnat,3*mnat),right(3*mnat,3*mnat)
      double precision :: xproj(3,mnat),projhess(3*mnat,3*mnat)
      double precision :: freqtmp(3*mnat),dirmol(3)
      double precision :: freqabs(3*mnat),zpem2(3*mnat)
      double precision :: nmvec2tmp(3*mnat,3*mnat),mm(mnat)
      double precision :: rdum1(3),rdum2(3,3)
      character*2 :: symbolm0(mnat)
      logical :: linmol,lprint,ltrapz

! 3 different diagonalisation subroutines are available: 
! 1) F02ABF is a set of EISPACK subroutines.
! 2) DSYEV comes from LAPACK 1.0
! 3) DSYEVD comes from LAPACK 2.0 (faster than DSYEV)
! Bonus: DSYEVX and DSYEVR could be used but are not tested here since we have
! no access to LAPACK 3.0 or 3.1 in MSI (DSYEVR is assumed to be faster than 
! DSYEV and DSYEVD)
!
!c Variables and parameters for f02abf
!      integer :: ifail
!      double precision :: nmvectmp(3*mnat,3*mnat),work(3*mnat)
!c Variables and parameters for dsyev
!      integer :: info,lwork,nmax
!      parameter (lwork = 9*mnat-1)
!      parameter (nmax = 3*mnat)
!      double precision :: work(lwork)
! Variables and parameters for dsyevd
      integer :: info,lwork,nmax,liwork
      parameter (liwork = 3+15*mnat)
      parameter (lwork = 1+18*mnat+18*mnat**2)
      parameter (nmax = 3*mnat)
      integer :: iwork(liwork) 
      double precision :: work(lwork)

      ndim = 3*nbrat  

      if (nbrat.lt.2) then
        write(6,*) 'cannot do normal modes for ',nbrat,' atoms'
        stop
      endif

! ilinear is locally used only for the TRAPZ
      call rotprin(xxm,mmm,nbrat,rdum1,rdum2,ilinear)

! Initialization of arrays
      do i=1,ndim
        do j=1,ndim
          hess(i,j) = 0.0d0
        enddo
      enddo
      do i=1,nbrat
        do k=1,3
          xxm0(k,i) = xxm(k,i)
        enddo
        symbolm0(i) = symbolm(i)
      enddo

! Semianalytical calculation of the projected Hessian matrix
! hh is the stepsize
      hh = 1.0d-5
      do i=1,3
      do j=1,nbrat
         jpos = 3*(j-1) + i
! Calculate gradients at xx + stepsize
        xxm(i,j) = xxm0(i,j) + hh
!xxx        call getpem(xxm,indmm,symbolm,cell,nbrat,pema,pemd,gpema, &
!xxx             gpemd,dvec,mm,1,nsurf)
        call getpem(xxm,indmm,symbolm,cell,nbrat,pema,pemd,gpema,  &
             gpemd,dvec,mm,1,nsurf, idum1, idum2)

        if (repflag.eq.0) then
! Adiabatic representation
           do k=1,3
             do l=1,nbrat
               gv1(k,l) = gpema(k,l,nsurf)
             enddo
           enddo
         elseif (repflag.eq.1) then
! Diabatic representation
           do k=1,3
             do l=1,nbrat
               gv1(k,l) = gpemd(k,l,nsurf,nsurf)
             enddo
           enddo
         else
           write(6,*) 'ERROR: REPFLAG = ',repflag,' in NORMOD2'
           stop
        endif
! Calculate gradients at xx - stepsize
        xxm(i,j) = xxm0(i,j) - hh
!xxx        call getpem(xxm,indmm,symbolm,cell,nbrat,pema,pemd,gpema,  &
!xxx              gpemd,dvec,mm,1,nsurf)
        call getpem(xxm,indmm,symbolm,cell,nbrat,pema,pemd,gpema,  &
              gpemd,dvec,mm,1,nsurf, idum1, idum2)

        if (repflag.eq.0) then
! Adiabatic representation
           do k=1,3
             do l=1,nbrat
               gv2(k,l) = gpema(k,l,nsurf)
             enddo
           enddo
         elseif (repflag.eq.1) then
! Diabatic representation
           do k=1,3
             do l=1,nbrat
               gv2(k,l) = gpemd(k,l,nsurf,nsurf)
             enddo
           enddo
         else
           write(6,*) 'ERROR: REPFLAG = ',repflag,' in NORMOD2'
           stop
        endif
        do k=1,3
          do l=1,nbrat
             lpos = 3*(l-1) + k
! Hessian matrix, 3NBRAT X 3NBRAT matrix
! Data ordered (x1,y1,z1,x2,y2,z2,...xN,yN,zN)
             hess(jpos,lpos) = (gv1(k,l) - gv2(k,l))/(2.0d0*hh)
! Mass-scaling
             hess(jpos,lpos) = hess(jpos,lpos)*mu/sqrt(mmm(j)*mmm(l))
          enddo
        enddo
! Change the coordinate back to xxm(i,j)
        xxm(i,j) = xxm0(i,j)
      enddo
      enddo

! Calculation of the Projected matrix P
! We use mass-scaled coordinates for the projection matrix

      do i=1,3
         do j=1,nbrat
            xproj(i,j)= dsqrt(mmm(j)/mu)*xxm(i,j)
         enddo
      enddo

      if (ilinear.eq.2) then 
         call projct(xproj,mmm,nbrat,proj)
      elseif (ilinear.eq.1) then
! nhangle and nhbond are not useful here and are not added in the call of 
! linearbond
         linmol = .true. 
!xxx         call linearbond(linmol,nbrat,xxm,dirmol)
         call linearbond(linmol,nbrat,xxm,dirmol, nhbond, nhangle)
         lprint = .false.
         call projctlin(lprint,xproj,mmm,nbrat,dirmol,proj)
      endif       

! Calculation of the projected Hessian
! F^p = t^(1-P)F(1-P) = (1-P)F(1-P)

      do i=1,ndim
         do j=1,ndim
            proj(i,j) = -proj(i,j)
            if (i.eq.j) proj(i,j) = 1.0d0 + proj(i,j)
         enddo 
      enddo
      
      do i=1,ndim
         do j=1,ndim
            right(i,j) = 0.0d0
            do k=1,ndim
               right(i,j) = right(i,j) + hess(i,k)*proj(k,j)
            enddo
         enddo 
      enddo
      do i=1,ndim
         do j=1,ndim
            projhess(i,j) = 0.0d0
            do k=1,ndim
               projhess(i,j) = projhess(i,j) + proj(i,k)*right(k,j)
            enddo
         enddo 
      enddo

! Diagonalization of the projected Hessian.
! Frequencies out of f02abf are always ordered from smallest to the largest.
! F02ABF
!      ifail = 0
!      call f02abf(projhess,3*mnat,3*mnat,freqm,nmvectmp,3*mnat,work,ifail)
!      if (ifail.ne.0) print *,'Warning, ifail = ',ifail
!c DSYEV
!      call dsyev('v','u',ndim,projhess,nmax,freqm,work,lwork,info)
! DSYEVD
      call dsyevd('v','u',ndim,projhess,nmax,freqm,work,lwork,iwork, &
                  liwork,info)

! If the molecule is linear k=1,ndim-5
! If the molecule is nonlinear k=1,ndim-6
! Print out the smallest 5 or 6 frequencies
! nvibmods2 enables to determine the number of positive modes during the 
! dynamics by assuming that the configuration is never linear : certainly 
! true for ammonia.

      nvibmods2 = 0
      icptb = 0
      icptub = 0
      do i=1,ndim
! We keep bound frequencies
         freqtmp(i) = dsqrt(dabs(freqm(i))/mu)
         if (freqtmp(i)*autocmi.gt.freqlim.and.freqm(i).gt.0.0d0) then
            nvibmods2 = nvibmods2 + 1
            do j=1,ndim
               nmvec2(j,ndim-i+1) = projhess(j,i)
            enddo
            freq2(ndim-i+1) = sign( freqtmp(i),freqm(i) )
! icptb locally counts the number of bound frequencies
            icptb = icptb + 1 
         endif 
      enddo
      do i=1,ndim
! We also keep unbound frequencies
         if (freqtmp(i)*autocmi.gt.freqlim.and.freqm(i).lt.0.0d0) then
            nvibmods2 = nvibmods2 + 1
! icptub locally counts the number of unbound frequencies
            icptub = icptub + 1
            do j=1,ndim
               nmvec2(j,icptb+icptub) = projhess(j,i)
            enddo
            freq2(icptb+icptub) = sign( freqtmp(i),freqm(i) )
         endif 
      enddo
      do i=1,ndim
! We remove the 6 zero frequencies (overall translation and rotation)
         if (freqtmp(i)*autocmi.lt.freqlim) then
            do j=1,ndim
               nmvec2(j,ndim+icptub-i+1) = projhess(j,i)
            enddo
            freq2(ndim+icptub-i+1) = sign( freqtmp(i),freqm(i) )
         endif 
      enddo

! If the number of bound + unbound modes is found to be greater than 3N-6 this
! may mean that some modes are very small but do not equal 0. 
! We do not apply TRAPZ-like methods in this case.

      if (ilinear.eq.2.and.nvibmods2.gt.ndim-6) then

         ltrapz = .false.
         if(.not.minprinttrapz) then
           write(6,*)
           write(6,*) 'Nonlinear molecule with more than 3N-6 modes'
           write(6,*) 'Cartesian coordinates are not appropriate'
           write(6,*) 'TRAPZ-like methods are not used'
         endif
         return

      elseif(ilinear.eq.1.and.nvibmods2.gt.ndim-5) then

         ltrapz = .false.
         if(.not.minprinttrapz) then
           write(6,*)
           write(6,*) 'Linear molecule with more than 3N-5 modes'
           write(6,*) 'Cartesian coordinates are not appropriate'
           write(6,*) 'TRAPZ-like methods are not used'
         endif
         return

      endif 

! To be removed to limit the output file size
      if(.not.minprinttrapz) then
      write(6,*)
      write(6,111) 'Keeping ',nvibmods2,' modes with positive ', &
                   ' or imaginary frequencies.'
      write(6,333) (freq2(k)*autocmi,k=1,nvibmods2)
      write(6,111) 'Rejecting ',ndim-nvibmods2,' other ', &
                      'frequencies (overall translation and rotation).'
      write(6,333) (freq2(k)*autocmi,k=nvibmods2+1,ndim)
      write(6,*)
      endif

! Zero-point vibrational energy
      zpe2 = 0.0d0
      do i=1,nvibmods2
         if (freq2(i).gt.0.0d0) then  
            zpem2(i) = 0.5d0*freq2(i)
            zpe2 = zpe2 + 0.5d0*freq2(i)
         else
            zpem2(i) = 0.0d0
         endif
      enddo
      if(.not.minprinttrapz) then
      write(6,222) 'Zero-point vibrational energy for NH3 is: ',  &
                zpe2*autoev,' eV'
      write(6,*)
      endif

! Formats go here.
 111  format(1x,a,i5,2a)
 222  format(1x,a,f12.5)
 333  format(1x,10d16.8)

      end subroutine normod2

!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! DESCRIPTION OF THE SUBROUTINE
! This subroutine is equivalent to the subroutine valvag from Polyrate
! (in polysz.f). To terms to calculate P = P_trans + P_rot.
!
! STRUCTURE OF THE SUBROUTINE (input/output)
! xcoord(i,j): mass-scaled coordinates (input).
! mass(i): mass of atom i (input).
! nbrat : number of atoms (input).
! proj(i,j): projection matrix (output).
!
! Last modification: 09/18/2007.

      subroutine projct(xcoord,mass,nbrat,proj)
!      subroutine projct(xcoord,mass,nbrat,proj,eigt,eigrot)

      use param, only: mnat

      implicit none
      integer :: i,j,k,nbrat,ix,jx,ipos,jpos,k1,k2,k3,k4
      double precision :: det,detmtbt,totm,invi(3,3)
      double precision :: epsilon(3,3,3),inert(3,3),xcoord(3,mnat)
      double precision :: mass(mnat),proj(3*mnat,3*mnat),tmp(3,3)

!c Determination of translation and rotation eigenvectors (optional) 
!      integer :: lwork,info,jend
!      double precision :: prodscal
!      parameter (lwork=9*mnat-1)
!      real*8 :: invisq(3,3),right(3,3),valp(3),eigt(3*mnat,3)
!      double precision :: eigrot(3*mnat,3),work(lwork),unity(3,3)

! Determination of the completely antisymmetric unit pseudotensor of rank 3, 
! epsilon(3,3,3)

      do i=1,3
         do j=1,3
            do k=1,3
               epsilon(i,j,k) = 0.0d0 
            enddo 
         enddo
      enddo  
      epsilon(1,2,3) = 1.0d0
      epsilon(3,1,2) = 1.0d0
      epsilon(2,3,1) = 1.0d0 
      epsilon(1,3,2) = -1.0d0
      epsilon(2,1,3) = -1.0d0
      epsilon(3,2,1) = -1.0d0

! Determination of the Inertia matrix, inert(3,3), and its inverse invi(3,3)
! I^-1 = 1/det*t^Com(I)
! The transposition is not (a priori) needed since the inertia matrix is 
! symmetric and consequently its inverse is symmetric too.
 
      call mominert(xcoord,nbrat,inert)

      det = 1.0d0/detmtbt(inert)

      tmp(1,1) = ( inert(2,2)*inert(3,3) - inert(3,2)*inert(2,3) )*det
      tmp(1,2) = -( inert(2,1)*inert(3,3) - inert(3,1)*inert(2,3) )*det
      tmp(1,3) = ( inert(2,1)*inert(3,2) - inert(3,1)*inert(2,2) )*det
      tmp(2,1) = -( inert(1,2)*inert(3,3) - inert(3,2)*inert(1,3) )*det
      tmp(2,2) = ( inert(1,1)*inert(3,3) - inert(3,1)*inert(1,3) )*det
      tmp(2,3) = -(inert(1,1)*inert(3,2) - inert(3,1)*inert(1,2) )*det
      tmp(3,1) = ( inert(1,2)*inert(2,3) - inert(2,2)*inert(1,3) )*det
      tmp(3,2) = -( inert(1,1)*inert(2,3) - inert(2,1)*inert(1,3) )*det
      tmp(3,3) = ( inert(1,1)*inert(2,2) - inert(2,1)*inert(1,2) )*det

      do i=1,3
         do j=1,3
            invi(i,j) = tmp(j,i) 
         enddo
      enddo

! Final estimation of P_rot and P = P_trans + P_rot

! Total mass
      totm = 0.0d0
      do i=1,nbrat
         totm = totm + mass(i)
      enddo

      do i=1,nbrat
         do ix=1,3
            ipos = 3*(i-1) + ix 
            do j=1,nbrat
               do jx=1,3 
                  jpos = 3*(j-1) + jx

! P_rot                  
                  proj(ipos,jpos) = 0.0d0
                  do k1 = 1,3
                     do k2 = 1,3
                        do k3 = 1,3
                           do k4 = 1,3
                              proj(ipos,jpos) = proj(ipos,jpos) +  &
                              epsilon(k1,k2,ix)*epsilon(k3,k4,jx)* &
                              xcoord(k2,i)*xcoord(k4,j)*invi(k1,k3)  
                           enddo
                        enddo
                     enddo
                  enddo
! P_trans
                  if (ix.eq.jx) proj(ipos,jpos) = proj(ipos,jpos) +  &
                                dsqrt(mass(i)*mass(j))/totm 

               enddo
            enddo
         enddo
      enddo 

!c Determination of eigenvectors associated with the infinitesimal 
!c translation  and rotation (not necessary, left for tests)
!
!c We need to calculate I^(-1/2) = P D^(1/2) P^(-1) where D is the matrix 
!c obtained by diagonalizing I^(-1) and P is the matrix composed of 
!c eigenvectors that allow to diagonalize I^(-1). NB: P^(-1) = P^T
! I^(-1) = P D P^(-1) = P D^(1/2) D^(1/2) P^(-1)
!        = P D^(1/2) P^(-1) P D^(1/2) P^(-1)
!        = I^(-1/2) I^(-1/2) with I^(-1/2) = P D^(1/2) P^(-1)
!      call dsyev('v','u',3,invi,3,valp,work,lwork,info)
!                        
!      do i=1,3
!         do j=1,3
!            right(i,j) = dsqrt(valp(i))*invi(j,i)
!         enddo
!      enddo  
!      do i=1,3
!         do j=1,3
!            invisq(i,j) = 0.0d0
!            do k=1,3                 
!               invisq(i,j) = invisq(i,j) + invi(i,k)*right(k,j)
!            enddo
!         enddo
!      enddo 
!
!      do i=1,3*nbrat 
!         do j=1,3
!            eigt(i,j) = 0.0d0
!            eigrot(i,j) = 0.0d0
!         enddo
!      enddo 
!
!      do i=1,nbrat
!         do ix = 1,3
!            ipos = 3*(i-1) + ix
!c eigt for the translation
!            eigt(ipos,ix) = dsqrt(mass(i)/totm) 
!
!c eigrot for the rotation
!            do k1=1,3
!               do k2=1,3
!                  do k3=1,3 
!                     eigrot(ipos,k1) = eigrot(ipos,k1) + invisq(k1,k2)  &
!                     *epsilon(k2,k3,ix)*xcoord(k3,i)
!                  enddo
!               enddo
!            enddo 
!
!         enddo
!      enddo

      end subroutine projct 

!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! DESCRIPTION OF THE SUBROUTINE
! This subroutine is equivalent to momigen (in momigen.f) but inputs must be 
! mass-scaled.
!
! STRUCTURE OF THE SUBROUTINE
! xcoord(i,j): mass-scaled coordinates (input). 
! nbrat: number of atoms (input).
! inert(i,j): Inertia matrix (output).
!
! Last modification: 08/31/2007.

      subroutine mominert(xcoord,nbrat,inert)

      use param, only: mnat

      implicit none
      integer :: i,j,nbrat
      real*8  :: xcoord(3,mnat),inert(3,3),x,y,z

      do i=1,3
        inert(i,1) = 0.0d0
        inert(i,2) = 0.0d0
        inert(i,3) = 0.0d0
      enddo

      do i=1,nbrat
         x = xcoord(1,i)
         y = xcoord(2,i)
         z = xcoord(3,i)
         inert(1,1) = inert(1,1)+ y*y + z*z
         inert(2,2) = inert(2,2)+ x*x + z*z
         inert(3,3) = inert(3,3)+ x*x + y*y
         inert(1,2) = inert(1,2)- x*y
         inert(1,3) = inert(1,3)- x*z
         inert(2,3) = inert(2,3)- y*z
      enddo
      inert(2,1) = inert(1,2)
      inert(3,1) = inert(1,3)
      inert(3,2) = inert(2,3)

      end subroutine mominert

!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! DESCRIPTION OF THE SUBROUTINE
! This subroutine is the linear counterpart of projct for linear molecules (see above)
!
! STRUCTURE OF THE SUBROUTINE (input/output)
! lprint: logical to print out some information about the axis (input)
!          lprint = .false. -> no information printed out
!          lprint = .true. -> some information is printed out
!          lprint is hard coded to .true. in NORMOD and .false. in NORMOD2
! xcoord(i,j): mass-scaled coordinates (input).
! mass(i): mass of atom i (input).
! nbrat: number of atoms (input).
! dirmol(3): direction of the linear molecule (input)
! proj(i,j): projection matrix (output).
!
! Last modification: 11/16/2007.

      subroutine projctlin(lprint,xcoord,mass,nbrat,dirmol,proj)

      use param, only: mnat

      implicit none
      integer :: i,j,nbrat,ix,jx,ipos,jpos,iaxis,ialigned
      double precision :: xyzlim,totm,inert,dirmol(3)
      double precision :: xcoord(3,mnat),mass(mnat),proj(3*mnat,3*mnat)
      character :: charaxis(3)
      logical :: lprint

      parameter (xyzlim = 1.0d-6) 

! Determination of the axis direction
! We first test if the direction of x, y or z (often the case when used in normod.f)       
       charaxis(1) = 'x'
       charaxis(2) = 'y'
       charaxis(3) = 'z'

! iaxis save the index of the molecule axis if the axis is onr of the three Cartesian axis
       iaxis = 0
       ialigned = 0
       do i=1,3
           if ( dabs(dabs(dirmol(i))-1.0d0).le.xyzlim) then
             if (lprint) then 
                write(6,*) 'The molecule is aligned along ',charaxis(i) 
                write(6,*)
             endif
             iaxis = i
             ialigned = ialigned + 1
           endif  
       enddo
       if (iaxis.eq.0) then
          if (lprint) then 
             write(6,*) 'PROJCTLIN is not still working for any axis'      
             write(6,*)
          endif 
          stop
       elseif (ialigned.ne.1) then
           write(6,*) 'xyzlim must be increased in PROJCTLIN'      
           write(6,*)       
           stop 
       endif
 
! Final estimation of P_rot and P = P_trans + P_rot (if iaxis is not 0)

! Total mass
      totm = 0.0d0
      do i=1,nbrat
         totm = totm + mass(i)
      enddo

      if (iaxis.ne.0) then

! Calculation of the momentum of inertia
      inert = 0.0d0
      do i=1,nbrat
          inert = inert + xcoord(iaxis,i)**2
      enddo
   
      do i=1,nbrat
         do ix=1,3
            ipos = 3*(i-1) + ix 
            do j=1,nbrat
               do jx=1,3 
                  jpos = 3*(j-1) + jx
                  proj(ipos,jpos) = 0.0d0

! P_rot  and P_trans                
                  if (ix.eq.jx) proj(ipos,jpos) = proj(ipos,jpos) +  &
                                xcoord(iaxis,i)*xcoord(iaxis,j)/inert + &
                                dsqrt(mass(i)*mass(j))/totm

                  if ( (ix.eq.iaxis).and.(jx.eq.iaxis) ) proj(ipos,jpos)   &
               = proj(ipos,jpos) - xcoord(iaxis,i)*xcoord(iaxis,j)/inert

               enddo
            enddo
         enddo
      enddo 

      endif

      end subroutine projctlin
