      subroutine header

      write(6,100) 
      write(6,110)
      write(6,120)
      write(6,130)
      write(6,140)
      write(6,150)
      write(6,130)
      write(6,160)
      write(6,170)
      write(6,180)
      write(6,100) 

 100  format(1X,70(1H*))
 110  format(1X,2(1H*),31X,'ANT',32X,2(1H*))
 120  format(1X,2(1H*),14X,'Adiabatic and Nonadiabatic Trajectories', &
             13X,2(1H*) )
 130  format(1X,2(1H*),66X,2(1H*) )
 140  format(1X,2(1H*),26X,'Version 2016',28X,2(1H*) )
 150  format(1X,2(1H*),25X,'March 9, 2016',28X,2(1H*) )
 160  format(1X,2(1H*),8X,'J. Zheng, Z. H. Li, A. W. Jasper,', &
                       ' D. A. Bonhommeau,',7X,2(1H*))
 170  format(1X,2(1H*),3X,'R. Valero, R. Meana-Paneda, S. L. Mielke, and D. G.', &
                      ' Truhlar',4X,2(1H*))
 180  format(1X,2(1H*),22X,'University of Minnesota',21X,2(1H*))

      end subroutine header
