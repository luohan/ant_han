      subroutine initelec

! PREPARE INITIAL ELECTRONIC VARIABLES
! Real and imaginary parts all set to 0 except for real part
! of the initially occupied state

      use c_sys, only: nsurft
      use c_initial, only: nsurf0
      use c_traj, only: cre, cim, crei, phase, cimi

      implicit none
      integer i

      crei(:) = 0.d0
      cimi(:) = 0.d0
      phase(:) = 0.d0

      crei(nsurf0) = 1.d0

      cre(:) = 0.d0
      cim(:) = 0.d0
      cre(nsurf0) = 1.d0
!     used for CSDM
      cre(nsurf0+nsurft) = 1.d0
       
      end subroutine initelec
