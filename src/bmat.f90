!***********************************************************************
!     BMAT (From POLYRATE)
!     modified by Jingjing Zheng Sep. 21 2010
!***********************************************************************
!
!SUBROUTINE bmat(NF,NBL,NBA,NTO,NLBE,IBL,IBA,ITO,ILBE,X,Y,Z,NATOM,NINTC,BM,T)
SUBROUTINE bmat(NF,NBL,NBA,NTO,NIMP,NLBE,IBL,IBA,ITO,IMP,ILBE,X,Y,Z,NATOM,NINTC,BM,T)
!
!     CALCULATES THE MATRIX DR/DX (B matrix)
!
implicit none
integer :: irow, ij, nf, nbl, nba, nto,nimp, nlbe, natom, nintc
integer :: i, j, ijk, im, k, l, ijkl
integer :: IBL(2,NF),IBA(3,NF),ITO(4,NF),IMP(4,NF),ILBE(3,NF)
double precision :: dist, angl, cose, sine
double precision :: BM(NINTC,3*NATOM), X(NATOM),Y(NATOM),Z(NATOM)
double precision :: PX(3),PY(3),PZ(3),V(3),T(3,3),BMTE(3)
double precision :: rij, rij1, rij2, rij3, rjk1, rjk2, rjk3, rjk, pijk
double precision :: cijk, sijk, pxmg, pymg, pzmg, sjkl, sijkl
double precision :: rkl, rkl1, rkl2, rkl3, cjkl, cijkl, pjkl
double precision :: rjl1,rjl2,rjl3,pkjl,pijl,theta,ctheta,ttheta,EJK(3),EJL(3),EJI(3)
double precision :: rjl,rji,ckjl,skjl,cijl
!
BM(:,:) = 0D0
IROW=0
DO IJ=1,NBL
  IROW=IROW+1
  I=IBL(1,IJ)
  J=IBL(2,IJ)
  RIJ1=X(J)-X(I)
  RIJ2=Y(J)-Y(I)
  RIJ3=Z(J)-Z(I)
  RIJ=DIST(I,J,X,Y,Z,NATOM)
  BM(IROW,3*I-2)=-RIJ1/RIJ
  BM(IROW,3*I-1)=-RIJ2/RIJ
  BM(IROW,3*I)=-RIJ3/RIJ
  BM(IROW,3*J-2)=RIJ1/RIJ
  BM(IROW,3*J-1)=RIJ2/RIJ
  BM(IROW,3*J)=RIJ3/RIJ
enddo    
!
DO IJK=1,NBA
  IROW=IROW+1
  I=IBA(1,IJK)
  J=IBA(2,IJK)
  K=IBA(3,IJK)
  RIJ1=X(J)-X(I)
  RIJ2=Y(J)-Y(I)
  RIJ3=Z(J)-Z(I)
  RJK1=X(K)-X(J)
  RJK2=Y(K)-Y(J)
  RJK3=Z(K)-Z(J)
  RIJ=DIST(I,J,X,Y,Z,NATOM)
  RJK=DIST(J,K,X,Y,Z,NATOM)
  PIJK=ANGL(I,J,K,X,Y,Z,NATOM)
  CIJK=COSE(PIJK)
  SIJK=SINE(PIJK)
  BM(IROW,3*I-2)=(-CIJK*RIJ1*RJK-RIJ*RJK1)/(SIJK*RIJ**2*RJK)
  BM(IROW,3*I-1)=(-CIJK*RIJ2*RJK-RIJ*RJK2)/(SIJK*RIJ**2*RJK)
  BM(IROW,3*I)=(-CIJK*RIJ3*RJK-RIJ*RJK3)/(SIJK*RIJ**2*RJK)
  BM(IROW,3*J-2)= (-RIJ*RIJ1*RJK+CIJK*RIJ1*RJK**2-CIJK*RIJ**2*RJK1+RIJ*RJK*RJK1) &
       /(SIJK*RIJ**2*RJK**2)
  BM(IROW,3*J-1)= (-RIJ*RIJ2*RJK+CIJK*RIJ2*RJK**2-CIJK*RIJ**2*RJK2+RIJ*RJK*RJK2) &
       /(SIJK*RIJ**2*RJK**2)
  BM(IROW,3*J)= (-RIJ*RIJ3*RJK+CIJK*RIJ3*RJK**2-CIJK*RIJ**2*RJK3+RIJ*RJK*RJK3)  &
       /(SIJK*RIJ**2*RJK**2)
  BM(IROW,3*K-2)=(RIJ1*RJK+CIJK*RIJ*RJK1)/(SIJK*RIJ*RJK**2)
  BM(IROW,3*K-1)=(RIJ2*RJK+CIJK*RIJ*RJK2)/(SIJK*RIJ*RJK**2)
  BM(IROW,3*K)=(RIJ3*RJK+CIJK*RIJ*RJK3)/(SIJK*RIJ*RJK**2)
enddo    
!
!
!     B-matrix for linear bend  - added 09xxYC96
!
DO IJK=1,NLBE
  IROW=IROW+1
  I=ILBE(1,IJK)
  J=ILBE(2,IJK)
  K=ILBE(3,IJK) 
  RIJ=DIST(I,J,X,Y,Z,NATOM)
  RJK=DIST(J,K,X,Y,Z,NATOM)
!
!  remember C-A-B ==> K-J-I : cases of z-axis
!
!  choice #1 : Z axis is assigned as rjk
!
!      PZ(1)=X(K)-X(J)
!      PZ(2)=Y(K)-Y(J)
!      PZ(3)=Z(K)-Z(J)
!
!  choice #2 : Z axis is assigned as rij
!
!      PZ(1)=X(J)-X(I)
!      PZ(2)=Y(J)-Y(I)
!      PZ(3)=Z(J)-Z(I)
!
!  choice #3 : Z axis is assigned as rik
!
  PZ(1)=X(K)-X(I)
  PZ(2)=Y(K)-Y(I)
  PZ(3)=Z(K)-Z(I)
!
  IF (ABS(PZ(3)/RJK).NE.1) THEN
!
!     set up local coordinates
!
    V(1)=1.0d0
    V(2)=1.0d0
    V(3)=1.0d0
!
!     find PY perpendicular to PZ and V
!
    CALL XPROD(V,PZ,PY)
!
!     find PX perpendicular to PY and PZ
!
    CALL XPROD(PY,PZ,PX)
!
!     normalize
!
    PXMG=0.0d0
    PYMG=0.0d0
    DO IM=1,3
      PXMG=PXMG+PX(IM)*PX(IM)
      PYMG=PYMG+PY(IM)*PY(IM)
    ENDDO
    PXMG=SQRT(PXMG)
    PYMG=SQRT(PYMG)
    PZMG=RJK
!
!   set up transpose of transformation matrix
!
    DO IM=1,3
      T(1,IM)=PX(IM)/PXMG
      T(2,IM)=PY(IM)/PYMG
      T(3,IM)=PZ(IM)/PZMG
    ENDDO
  ELSE
    DO IM=1,3
      IF(PZ(3)/PZMG.LT.0) THEN
         T(IM,IM)=-1.0d0
      ELSE
         T(IM,IM)=1.0d0
      ENDIF
    ENDDO
  ENDIF
!
!   the Ry mode ; Califano + Mathematica
!
!   in Califano the molecule in C-A-B
!   here is                     K-J-I
!
  BMTE(1)= 1.0d0/RIJ 
  BMTE(2)= -(RIJ+RJK)/(RIJ*RJK)
  BMTE(3)= 1.0d0/RJK
!
!   transform back to the original coordinates
!     B = B'T(transpose)
!
  BM(IROW,3*I-2)=BMTE(1)*T(1,1)
  BM(IROW,3*I-1)=BMTE(1)*T(1,2)
  BM(IROW,3*I)  =BMTE(1)*T(1,3)
  BM(IROW,3*J-2)=BMTE(2)*T(1,1)
  BM(IROW,3*J-1)=BMTE(2)*T(1,2)
  BM(IROW,3*J)  =BMTE(2)*T(1,3)
  BM(IROW,3*K-2)=BMTE(3)*T(1,1)
  BM(IROW,3*K-1)=BMTE(3)*T(1,2)
  BM(IROW,3*K)  =BMTE(3)*T(1,3)
!
!   the Rx mode 
!
  IROW=IROW+1
  BM(IROW,3*I-2)=BMTE(1)*T(2,1)
  BM(IROW,3*I-1)=BMTE(1)*T(2,2)
  BM(IROW,3*I)  =BMTE(1)*T(2,3)
  BM(IROW,3*J-2)=BMTE(2)*T(2,1)
  BM(IROW,3*J-1)=BMTE(2)*T(2,2)
  BM(IROW,3*J)  =BMTE(2)*T(2,3)
  BM(IROW,3*K-2)=BMTE(3)*T(2,1)
  BM(IROW,3*K-1)=BMTE(3)*T(2,2)
  BM(IROW,3*K)  =BMTE(3)*T(2,3)
enddo    
! 
! Improper torsion / out of plane motion
DO IJKL =1, NIMP
  IROW = IROW+1
  I=IMP(1,IJKL)
  J=IMP(2,IJKL)
  K=IMP(3,IJKL)
  L=IMP(4,IJKL)
  RJK1=X(K)-X(J)
  RJK2=Y(K)-Y(J)
  RJK3=Z(K)-Z(J)
  RJL1=X(L)-X(J)
  RJL2=Y(L)-Y(J)
  RJL3=Z(L)-Z(J)
  RJK=DIST(J,K,X,Y,Z,NATOM)
  RJL=DIST(J,L,X,Y,Z,NATOM)
  PKJL=ANGL(K,J,L,X,Y,Z,NATOM)
  PIJL=ANGL(I,J,L,X,Y,Z,NATOM)
  PIJK=ANGL(I,J,K,X,Y,Z,NATOM)
  CKJL=COSE(PKJL)
  SKJL=SINE(PKJL)
  CIJL=COSE(PIJL)
  CIJK=COSE(PIJK)
! calculate theta
  EJK(1)=RJK1/RJK
  EJK(2)=RJK2/RJK
  EJK(3)=RJK3/RJK
  EJL(1)=RJL1/RJL
  EJL(2)=RJL2/RJL
  EJL(3)=RJL3/RJL
  RJI = DIST(J,I,X,Y,Z,NATOM)
  EJI(1)=(X(I)-X(J))/RJI
  EJI(2)=(Y(I)-Y(J))/RJI
  EJI(3)=(Z(I)-Z(J))/RJI
  CALL XPROD(EJK,EJL,V) 
  THETA = DACOS(V(1)*EJI(1)+V(2)*EJI(2)+V(3)*EJI(3))
  CTHETA = COSE(THETA)
  TTHETA = SINE(THETA)/COSE(THETA) 
  BM(IROW,3*I-2) = (V(1)/(CTHETA*SKJL)-TTHETA*RJL1)/RJI
  BM(IROW,3*I-1) = (V(2)/(CTHETA*SKJL)-TTHETA*RJL2)/RJI
  BM(IROW,3*I)   = (V(3)/(CTHETA*SKJL)-TTHETA*RJL3)/RJI
  BM(IROW,3*K-2) = (V(1)*(CKJL*CIJL-CKJL)/(SKJL**3*CTHETA))/RJK 
  BM(IROW,3*K-1) = (V(2)*(CKJL*CIJL-CKJL)/(SKJL**3*CTHETA))/RJK 
  BM(IROW,3*K)   = (V(3)*(CKJL*CIJL-CKJL)/(SKJL**3*CTHETA))/RJK 
  BM(IROW,3*L-2) = (V(1)*(CKJL*CIJL-CIJL)/(SKJL**3*CTHETA))/RJK 
  BM(IROW,3*L-1) = (V(2)*(CKJL*CIJL-CIJL)/(SKJL**3*CTHETA))/RJK 
  BM(IROW,3*L)   = (V(3)*(CKJL*CIJL-CIJL)/(SKJL**3*CTHETA))/RJK 
  BM(IROW,3*J-2) = -BM(IROW,3*I-2)-BM(IROW,3*K-2)-BM(IROW,3*L-2)
  BM(IROW,3*J-1) = -BM(IROW,3*I-1)-BM(IROW,3*K-1)-BM(IROW,3*L-1)
  BM(IROW,3*J)   = -BM(IROW,3*I)-BM(IROW,3*K)-BM(IROW,3*L)
ENDDO
!
!
!  Torsion
!
DO IJKL=1,NTO
  IROW=IROW+1
  I=ITO(1,IJKL)
  J=ITO(2,IJKL)
  K=ITO(3,IJKL)
  L=ITO(4,IJKL)
  RIJ1=X(J)-X(I)
  RIJ2=Y(J)-Y(I)
  RIJ3=Z(J)-Z(I)
  RJK1=X(K)-X(J)
  RJK2=Y(K)-Y(J)
  RJK3=Z(K)-Z(J)
  RKL1=X(L)-X(K)
  RKL2=Y(L)-Y(K)
  RKL3=Z(L)-Z(K)
  RIJ=DIST(I,J,X,Y,Z,NATOM)
  RJK=DIST(J,K,X,Y,Z,NATOM)
  RKL=DIST(K,L,X,Y,Z,NATOM)
  PIJK=ANGL(I,J,K,X,Y,Z,NATOM)
  CIJK=COSE(PIJK)
  SIJK=SINE(PIJK)
  PJKL=ANGL(J,K,L,X,Y,Z,NATOM)
  CJKL=COSE(PJKL)
  SJKL=SINE(PJKL)
  CIJKL=((-RIJ2*RJK1+RIJ1*RJK2)*(-RJK2*RKL1+RJK1*RKL2)+ &
       (RIJ3*RJK1-RIJ1*RJK3)*(RJK3*RKL1-RJK1*RKL3)+     &
       (-RIJ3*RJK2+RIJ2*RJK3)*(-RJK3*RKL2+RJK2*RKL3))/  &
       (SIJK*SJKL*RIJ*RJK*RJK*RKL)
  SIJKL=((-RIJ3*RJK2+RIJ2*RJK3)*RKL1+(RIJ3*RJK1-RIJ1*RJK3)*RKL2+  &
       (-(RIJ2*RJK1)+RIJ1*RJK2)*RKL3)/(RIJ*RJK*RKL*SIJK*SJKL)
  BM(IROW,3*I-2)=SIJKL*RIJ1/(CIJKL*SIJK**2*RIJ**2)+ &
       CIJK*SIJKL*RJK1/(CIJKL*SIJK**2*RIJ*RJK)+     &
       (RJK3*RKL2-RJK2*RKL3)/(CIJKL*SIJK*SJKL*RIJ*RJK*RKL)
  BM(IROW,3*I-1)=SIJKL*RIJ2/(CIJKL*SIJK**2*RIJ**2)+ &
       CIJK*SIJKL*RJK2/(CIJKL*SIJK**2*RIJ*RJK)+     &
       (-RJK3*RKL1+RJK1*RKL3)/(CIJKL*SIJK*SJKL*RIJ*RJK*RKL)
  BM(IROW,3*I)=SIJKL*RIJ3/(CIJKL*SIJK**2*RIJ**2)+ &
       CIJK*SIJKL*RJK3/(CIJKL*SIJK**2*RIJ*RJK)+   &
       (RJK2*RKL1-RJK1*RKL2)/(CIJKL*SIJK*SJKL*RIJ*RJK*RKL)
  BM(IROW,3*J-2)=-(SIJKL*RIJ1/(CIJKL*SIJK**2*RIJ**2))+  &
       CIJK*SIJKL*(RIJ1-RJK1)/(CIJKL*SIJK**2*RIJ*RJK)-  &
       SIJKL*RJK1/(CIJKL*RJK**2)+SIJKL*RJK1/(CIJKL*SIJK**2*RJK**2)+  &
       SIJKL*RJK1/(CIJKL*SJKL**2*RJK**2)+  &
       CJKL*SIJKL*RKL1/(CIJKL*SJKL**2*RJK*RKL)+  &
       (-RIJ3*RKL2-RJK3*RKL2+RIJ2*RKL3+RJK2*RKL3)/  &
       (CIJKL*SIJK*SJKL*RIJ*RJK*RKL)
  BM(IROW,3*J-1)=-SIJKL*RIJ2/(CIJKL*SIJK**2*RIJ**2)+  &
       CIJK*SIJKL*(RIJ2-RJK2)/(CIJKL*SIJK**2*RIJ*RJK)- & 
       SIJKL*RJK2/(CIJKL*RJK**2)+SIJKL*RJK2/(CIJKL*SIJK**2*RJK**2)+  &
       SIJKL*RJK2/(CIJKL*SJKL**2*RJK**2)+ &
       CJKL*SIJKL*RKL2/(CIJKL*SJKL**2*RJK*RKL)+  &
       (RIJ3*RKL1+RJK3*RKL1-RIJ1*RKL3-RJK1*RKL3)/  & 
       (CIJKL*SIJK*SJKL*RIJ*RJK*RKL) 
  BM(IROW,3*J)=-SIJKL*RIJ3/(CIJKL*SIJK**2*RIJ**2)+   &
       CIJK*SIJKL*(RIJ3-RJK3)/(CIJKL*SIJK**2*RIJ*RJK)-  &
       SIJKL*RJK3/(CIJKL*RJK**2)+SIJKL*RJK3/(CIJKL*SIJK**2*RJK**2)+  &
       SIJKL*RJK3/(CIJKL*SJKL**2*RJK**2)+   &
       (-RIJ2*RKL1-RJK2*RKL1+RIJ1*RKL2+RJK1*RKL2)/  &
       (CIJKL*SIJK*SJKL*RIJ*RJK*RKL)+   &
       CJKL*SIJKL*RKL3/(CIJKL*SJKL**2*RJK*RKL)   
  BM(IROW,3*K-2)=-CIJK*SIJKL*RIJ1/(CIJKL*SIJK**2*RIJ*RJK)+  &
       SIJKL*RJK1/(CIJKL*RJK**2)-SIJKL*RJK1/(CIJKL*SIJK**2*RJK**2)-  &
       SIJKL*RJK1/(CIJKL*SJKL**2*RJK**2)+  &
       CJKL*SIJKL*(RJK1-RKL1)/(CIJKL*SJKL**2*RJK*RKL)+  &
       SIJKL*RKL1/(CIJKL*SJKL**2*RKL**2)+  &
       (RIJ3*RJK2-RIJ2*RJK3+RIJ3*RKL2-RIJ2*RKL3)/  &
       (CIJKL*SIJK*SJKL*RIJ*RJK*RKL)
  BM(IROW,3*K-1)=-CIJK*SIJKL*RIJ2/(CIJKL*SIJK**2*RIJ*RJK)+  &
       SIJKL*RJK2/(CIJKL*RJK**2)-SIJKL*RJK2/(CIJKL*SIJK**2*RJK**2)-  &
       SIJKL*RJK2/(CIJKL*SJKL**2*RJK**2)+  &
       CJKL*SIJKL*(RJK2-RKL2)/(CIJKL*SJKL**2*RJK*RKL)+  &
       SIJKL*RKL2/(CIJKL*SJKL**2*RKL**2)+  &
       (-RIJ3*RJK1+RIJ1*RJK3-RIJ3*RKL1+RIJ1*RKL3)/  &
       (CIJKL*SIJK*SJKL*RIJ*RJK*RKL)
  BM(IROW,3*K)=-CIJK*SIJKL*RIJ3/(CIJKL*SIJK**2*RIJ*RJK)+  &
       SIJKL*RJK3/(CIJKL*RJK**2)-SIJKL*RJK3/(CIJKL*SIJK**2*RJK**2)-  &
       SIJKL*RJK3/(CIJKL*SJKL**2*RJK**2)+  &
       (RIJ2*RJK1-RIJ1*RJK2+RIJ2*RKL1-RIJ1*RKL2)/  &
       (CIJKL*SIJK*SJKL*RIJ*RJK*RKL)+  &
       CJKL*SIJKL*(RJK3-RKL3)/(CIJKL*SJKL**2*RJK*RKL)+  &
       SIJKL*RKL3/(CIJKL*SJKL**2*RKL**2)
  BM(IROW,3*L-2)=-CJKL*SIJKL*RJK1/(CIJKL*SJKL**2*RJK*RKL)+ &
       (-RIJ3*RJK2+RIJ2*RJK3)/(CIJKL*SIJK*SJKL*RIJ*RJK*RKL)- &
       SIJKL*RKL1/(CIJKL*SJKL**2*RKL**2) 
  BM(IROW,3*L-1)=-CJKL*SIJKL*RJK2/(CIJKL*SJKL**2*RJK*RKL)+  &
       (RIJ3*RJK1-RIJ1*RJK3)/(CIJKL*SIJK*SJKL*RIJ*RJK*RKL)- &
       SIJKL*RKL2/(CIJKL*SJKL**2*RKL**2)
  BM(IROW,3*L)=(-RIJ2*RJK1+RIJ1*RJK2)/  &
       (CIJKL*SIJK*SJKL*RIJ*RJK*RKL)-   &
       CJKL*SIJKL*RJK3/(CIJKL*SJKL**2*RJK*RKL)-  &
       SIJKL*RKL3/(CIJKL*SJKL**2*RKL**2)
enddo    

END

!***********************************************************************
!     DIST
!***********************************************************************
!
DOUBLE PRECISION FUNCTION DIST(I,J,X,Y,Z,NATOM)
!
!     CALCULATES THE DISTANCE I-J
!     CALLED BY: BMAT
implicit none
integer :: i, j, natom
double precision :: X(NATOM),Y(NATOM),Z(NATOM)
DIST=DSQRT((X(I)-X(J))**2+(Y(I)-Y(J))**2+(Z(I)-Z(J))**2)
END
!
!***********************************************************************
!     ANGL
!***********************************************************************
!
DOUBLE PRECISION FUNCTION ANGL(I,J,K,X,Y,Z,NATOM)
!
!     CALCULATES THE ANGLE I-J-K
!     CALLED BY:
!              BANGLE1,BMAT,BTENS,BTORSN,CONOUT,READINT,PTORS
!
implicit none
integer :: i, j, k, natom
double precision :: X(NATOM),Y(NATOM),Z(NATOM),dist
!
ANGL=((X(J)-X(I))*(X(J)-X(K))+(Y(J)-Y(I))*(Y(J)-Y(K))+  &
       (Z(J)-Z(I))*(Z(J)-Z(K)))/(DIST(I,J,X,Y,Z,NATOM)*DIST(J,K,X,Y,Z,NATOM))
IF (DABS(ANGL).GT.1d0) ANGL = 1.0d0
ANGL=DACOS(ANGL)
END
!
!***********************************************************************
!     SINE
!***********************************************************************
!
DOUBLE PRECISION FUNCTION sine(PHI)
!
!     CALLED BY: BMAT
!
implicit none
double precision, PARAMETER :: TINY=1.D-20 
double precision :: phi
SINE=DSIN(PHI)
IF(ABS(SINE).LT.TINY) SINE=DSIGN(TINY,SINE)
END
!
!***********************************************************************
!     COSE
!***********************************************************************
!
DOUBLE PRECISION FUNCTION cose(PHI)
!
!     CALLED BY: BMAT
!
implicit none
double precision, PARAMETER ::  TINY=1.D-20
double precision :: phi
COSE=DCOS(PHI)
IF(ABS(COSE).LT.TINY) COSE=SIGN(TINY,COSE)
END

!
!***********************************************************************
!     XPROD
!***********************************************************************
!
SUBROUTINE xprod(X,Y,Z)
!
!  FORMS THE CROSS PRODUCT OF VECTORS X AND Y, PLACING THE RESULT
!  IN VECTOR Z.  ALL VECTORS ARE THREE DIMENSIONAL.
!     CALLED BY: BMAT
!
implicit none
double precision :: X(3),Y(3),Z(3)
Z(1) = X(2)*Y(3) - X(3)*Y(2)
Z(2) = X(3)*Y(1) - X(1)*Y(3)
Z(3) = X(1)*Y(2) - X(2)*Y(1)
END
