      subroutine rotprin(xxm,mmm,natoms,momiprin,rotaxis,iflin)
!  this subroutine calculates the principal rotational axis
!  of an AG (atom group) and judges if they are linear
!    Input:
! xxm,mmm,natoms
!    Output:
! rotaxis(3,3): principal rotational axes of the AG with x with
!               smallest I, followed by y and then z
! momiprin(3):   principal rotational moment of inertia of AG in an
!               acsending order
! iflin:     to judge if a molecule is an atom (0), linear (1) or
!               nonolinear (2)
! 
      use param, only: mnat
      use c_output, only: idebug
      implicit none
!
      integer natoms
      integer iflin
      double precision xxm(3,mnat),mmm(mnat),det
      double precision momiprin(3),rotaxis(3,3)
! local
      integer i,j,k,lwork,info,ndim,ndimmax
      parameter (lwork=3*3-1)
      double precision work(lwork)
!     double precision, allocatable :: work(:)
      double precision momi(3,3)
!
! Assuming the origin has been placed at the center of mass of the molecule
! Calculate the moment of inertia tensor matrix
      call momigen(xxm,mmm,natoms,momi)
!
! diagonalize the I matrix by DSYEV
! See lapack manual for the usage of dsyev
      ndim = 3
      ndimmax = 3

      work = 0d0
      momiprin = 0d0
      call dsyev('V','L',3,momi,3,momiprin,work,lwork,info)

! Print out principal moment of inertia and its vector
      if (idebug) then
        write(6,*) 'Principal moment of inertia and its vector:'
        do i=1,3
          write(6,107) momiprin(i), (momi(j,i),j=1,3)
        enddo
        write(6,*) 
      endif
! To judge if a molecule is linear or nonlinear by judging principal
! moment of inertia. Remember that they are stored in momiprin in an
! ascending order. The threshold is 1.0d-5 
      if (momiprin(1).le.1.0d-5) then
         if (momiprin(2).le.1.0d-5) then
                 iflin = 0
!           write(6,*) 'This AG is just an atom'
         else
                 iflin = 1
!           write(6,*) 'This AG is linear'
         endif
       else
               iflin = 2
!           write(6,*) 'This AG is non-linear polyatom group'
      endif
! calculate determinant of momi
!     det = momi(1,1)*momi(2,2)*momi(3,3)+momi(1,2)*momi(2,3)*momi(1,3)
!    &    + momi(1,3)*momi(2,1)*momi(3,2)-momi(1,3)*momi(2,2)*momi(3,1) 
!    &    - momi(1,2)*momi(2,1)*momi(3,3)-momi(1,1)*momi(2,3)*momi(3,2)
!     write(6,*) 'Det of Momi', det
!
! Get the rotational axes, output momi of DSYEV
! Transform to the usual form of transformation matrix (The storage in
! momi is vector like)

! make sure the diagonal elements of momi are positive so that we prevent 
! the reflection of the geometry and rotaxis is a pure rotation matrix
      do i = 1, 3
        if(momi(i,i) .lt. 0d0) momi(:,i) = -momi(:,i)
      enddo
      do i=1,3
         rotaxis(i,1) = momi(1,i)
         rotaxis(i,2) = momi(2,i)
         rotaxis(i,3) = momi(3,i)
      enddo
 107  format(1x,f20.4,3f12.5)
!
      end subroutine rotprin
