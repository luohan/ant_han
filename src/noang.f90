      subroutine noang(iflinear,bigj,pp,xx,mm,natoms)
!
! Setting initial rotation for an AG
!  Input:
!  xx,pp,mm,natoms,iflinear
! This subroutine was inspired by a subroutine from GenDyne.
! bigj:          angular momentum
! iflinear:      0: an atom; 1: Linear; 2; non-linear
! momi(3,3):     Moment of Inertia matrix
! ivmomi(3,3):   Invert of momi
! ido:           0: remove angmom; 1: add angmom
!  Output:
!  pp
      use param, only: mnat, amutoau
      use c_output, only: idebug

      implicit none
      integer :: natoms,iflinear
      double precision :: pp(3,mnat),bigj(3),xx(3,mnat),mm(mnat)
! local
      integer, parameter :: lwork=5
      integer :: i,j,IPIV(3),infor
      double precision :: momi(3,3),d1,d2,d3,d4,d5,d6,det,omeg(3), &
                          c11,c12,c13,c21,c22,c23,c31,c32,c33,&
                          ivmomi(3,3),detmtbt
      double precision :: r,vect(3),pptm,dx,dy,dz
      double precision :: work(lwork)
!
      if (idebug) then
        write(6,*) 'Momenta before remove'
        do i=1,natoms
          write(6,107) i,mm(i)/amutoau,(pp(j,i),j=1,3)
        enddo
        write(6,*)
      endif
      do i=1,3
        do j=1,3
          momi(i,j) = 0.d0
        enddo
      enddo
!
!     Nothing to do for just an atom
      if (natoms.ne.1) then
!   Cannot calculate invert of the I matrix for linear molecules
        if (iflinear.eq.2) then
!         compute moment of intertia matrix momi
          call momigen(xx,mm,natoms,momi)
          do i=1,3
            omeg(i)=bigj(i)
          enddo
! omega = I-1 . J, therefore, omeg is the linear solution to I.omega = J
          call dgesv(3,1,momi,3,ipiv,omeg,3,infor)
!          call invmtbt(momi,ivmomi)
!          do i=1,3
!            omeg(i) = ivmomi(i,1)*bigj(1) + ivmomi(i,2)*bigj(2) +   &
!                      ivmomi(i,3)*bigj(3)
!          enddo
!
!C correction = r x omega
          do i=1,natoms
            pp(1,i)=pp(1,i)+mm(i)*(xx(2,i)*omeg(3)-xx(3,i)*omeg(2))
            pp(2,i)=pp(2,i)-mm(i)*(xx(1,i)*omeg(3)-xx(3,i)*omeg(1))
            pp(3,i)=pp(3,i)+mm(i)*(xx(1,i)*omeg(2)-xx(2,i)*omeg(1))
          enddo
        else
!        For linear AG, since it has been placed into a coord. sys. in
!        which all the atoms are aligned on x axis, it only needs to
!        keep only x part of the pp
!        c.g. subroutines rottran and rotprin
!          do i=1,natoms
!            pp(2,i) = 0.0d0
!            pp(3,i) = 0.0d0
!          enddo
          dx=xx(1,2)-xx(1,1)
          dy=xx(2,2)-xx(2,1)
          dz=xx(3,2)-xx(3,1)
          r=dx*dx+dy*dy+dz*dz
          r=dsqrt(r)
!         Linear molecular direction vector (any two atoms)
          vect(1)=dx/r
          vect(2)=dy/r
          vect(3)=dz/r
!         Projection of ppm on the molecular direction
          do i=1,natoms
            pptm=pp(1,i)*vect(1)+pp(2,i)*vect(2)+pp(3,i)*vect(3)
!         Final ppm
            pp(1,i)=pptm*vect(1)
            pp(2,i)=pptm*vect(2)
            pp(3,i)=pptm*vect(3)
          enddo
        endif
!     endif of non mono atoms
      endif
      if (idebug) then
        write(6,*) 'Momenta after remove'
        do i=1,natoms
          write(6,107) i,mm(i)/amutoau,(pp(j,i),j=1,3)
        enddo
        write(6,*)
      endif
 
 107  format(1x,i5,f12.4,3f14.8)
 
      end subroutine noang
