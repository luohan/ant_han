      subroutine nocompp(pp,mm,natoms,compp,totcomp,iremcom)
! This subroutine calculates the CoM momentum and removes CoM momentum
! Input: pp, mm, natoms
!  Program gives:
!  pp:     new pp (iremcom!=0), compp,totcomp
      use param, only: mnat, autofs, autoang
      use c_output, only: idebug

      implicit none
      integer :: natoms
      integer :: iremcom
      double precision :: pp(3,mnat),mm(mnat)
!     Three vector of CoM motion
      double precision :: compp(3)
!     Magnitude of total CoM momentum
      double precision :: totcomp
!     0: do not remove CoM motion, otherwise remove it
!
      integer :: im,i,j
      double precision :: mtot,tmp
!
! Initialize 
      compp(1) = 0.0d0 
      compp(2) = 0.0d0 
      compp(3) = 0.0d0 
      mtot     = 0.0d0
      do i=1,natoms
        compp(1) = compp(1) + pp(1,i)
        compp(2) = compp(2) + pp(2,i)
        compp(3) = compp(3) + pp(3,i)
        mtot=mtot+mm(i)
      enddo
      if (idebug) then
         write(6,*) 'CoM Momentum'
         write(6,'(1x,3f20.8)') (compp(j),j=1,3)
! Print out comvv
        write(6,*) 'CoM velocity (A/fs)'
        write(6,'(1x,3f20.8)') (compp(j)*autoang/  &
                 (mtot*autofs),j=1,3)
        write(6,*)
      endif
! Remove CoM momentum
      if (iremcom.ne.0) then
        do i=1,natoms
          tmp=mm(i)/mtot
          pp(1,i) = pp(1,i) - tmp*compp(1)
          pp(2,i) = pp(2,i) - tmp*compp(2)
          pp(3,i) = pp(3,i) - tmp*compp(3)
        enddo
        totcomp  = 0.0d0
        compp(1) = 0.0d0 
        compp(2) = 0.0d0 
        compp(3) = 0.0d0 
      else
! total linear momentum
        totcomp = compp(1)**2 + compp(2)**2 + compp(3)**2
        totcomp = sqrt(totcomp)
      endif
!
      end subroutine nocompp
