      subroutine frag
! This subroutine divide the atoms into the fragments that they belong to
!    Input:
! nbond,bndind,natoms (in module c_struct)
! iat:        count on atoms
! ifrag:      Count on fragments
!    Output:
! fragind:      store the atom index in a fragment
! nfrag:        fragments found
! natinfrag:    Number of atoms of a fragment
      use param, only: mnat, mnfrag, mnmol, mnbond
      use c_output, only:
      use c_struct, only: natoms, natinfrag, fragind, natinfrag,  &
                          fragind, nbond, bndind, nfrag

      implicit none
      integer :: i,j,k,l,i1,i2,i3,itmp,iat,ifrag,  &
                 indrest(mnat),natrest,natfragm,ifind,nnew,iyes
!
! Method 2
! Initialize
      natrest=natoms
      do i=1,natoms
        indrest(i)=i
      enddo
      nnew=0
      ifrag=1
 5    if (natrest.le.0) goto 99
!       loop over atoms in AG im
!         find an unsigned atom in the rest of atoms
          do i=1,natoms
            if (indrest(i).ne.0) then
              natfragm  = 1
              natinfrag(ifrag) = 1
              fragind(1,ifrag)= indrest(i)
              natrest=natrest-1
              indrest(i)=0
              nnew=1
              ifind=0
              goto 10
            endif
          enddo
          if (nnew.ne.1) goto 99
  10      do k=natinfrag(ifrag)-nnew+1,natinfrag(ifrag)
           iat=fragind(k,ifrag)
           do j=1,nbond(iat)
             natinfrag(ifrag)=natfragm
             iyes=0
             do i=1,natinfrag(ifrag)
               if (fragind(i,ifrag).eq.bndind(j,iat)) iyes=1
             enddo
             if (iyes.eq.0) then 
                natfragm=natfragm+1
                fragind(natfragm,ifrag) = bndind(j,iat)
                natrest = natrest-1
                indrest( bndind(j,iat) )=0
                ifind = ifind+1
             endif
           enddo
          enddo
          nnew=ifind
          ifind=0
          if (nnew.gt.0) then
              natinfrag(ifrag)=natfragm
              goto 10
            else
              ifrag=ifrag+1
              goto 5
          endif
 99   continue
      nfrag=ifrag-1
!      write(6,*) "nfrag in frag",nfrag
 
      end subroutine frag
