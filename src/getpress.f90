      subroutine getpress(dimens,presst)
!
! Compute the pressure of the system
! press:     Pressure
! presst:    Pressure tensor matrix (3by3)
! dimens:    Dimension of the system
! Input:
! xxm,ppm,gv,mmm,natoms,vol,dimens
! Output:
! press,presst
      use param, only: autoatm
      use c_output, only: idebug
      use c_struct, only: mmoffr, nfrag, natoms, xxm, compfr
      use c_traj, only: vol, gv, press

      implicit none
      integer :: dimens
      double precision :: presst(3,3)
! local
      integer :: i,j,k
!
      press=0.d0
      do i=1,3
        presst(i,1)=0.d0
        presst(i,2)=0.d0
        presst(i,3)=0.d0
      enddo
      do j=1,3
        do k=1,3
          do i=1,natoms
!            presst(j,k) = presst(j,k) + ppm(j,i)*ppm(k,i)/mmm(i) -  &
!                          gv(j,i)*xxm(k,i)
            presst(j,k) = presst(j,k) - xxm(j,i)*gv(k,i)
          enddo
          do i=1,nfrag
            presst(j,k) = presst(j,k)+compfr(j,i)*compfr(k,i)/mmoffr(i)
          enddo
          presst(j,k) = presst(j,k)/vol
        enddo
      enddo
! p = trace(presst)/3
!      do i=1,natoms
!        do j=1,3
!          press=press+ppm(j,i)**2/mmm(i)-xxm(j,i)*gv(j,i)
!          press=press+xxm(j,i)*gv(j,i)
!        enddo
!      enddo
!      press=(dble(natoms)*kb*temp0+press)/vol
      press=presst(1,1)+presst(2,2)+presst(3,3)
      press=press/dble(dimens)
!      press=press/(vol*dble(dimens))
!      press=dble(natoms)*kb*temp0/vol
      if (idebug) then
        write(6,1) 'Calculate the pressure and pressure tensor matrix', &
                   ' of the system'
        write(6,*) 'The pressure of the system = ',press*autoatm,' atm'
        write(6,*) 'Pressure tensor matrix: '
        do i=1,3
          write(6,'(1x,3e20.8)') (presst(i,j),j=1,3)
        enddo
      endif
!
 1    format(1x,5a)
 
      end subroutine getpress
