! QTRFIT 
! Original from J. C. Corchada and J. Espinosa-Garcia
! The old Jocobi subroutine for Diagonalization is replaced by Lapack subroutine
! Modification made by J. Zheng
!
! Find the quaternion, q, [and left rotation matrix, u] that minimizes
!
!   |qTXq - Y| ^ 2    [|uX - Y| ^ 2]
!
! This is equivalent to maximizing Re (qTXTqY).
!
! This is equivalent to finding the largest eigenvalue and corresponding
! eigenvector of the matrix
!
!   [A2   AUx  AUy  AUz ]
!   [AUx  Ux2  UxUy UzUx]
!   [AUy  UxUy Uy2  UyUz]
!   [AUz  UzUx UyUz Uz2 ]
!
! where
!
!   A2   = Xx Yx + Xy Yy + Xz Yz
!   Ux2  = Xx Yx - Xy Yy - Xz Yz
!   Uy2  = Xy Yy - Xz Yz - Xx Yx
!   Uz2  = Xz Yz - Xx Yx - Xy Yy
!   AUx  = Xz Yy - Xy Yz
!   AUy  = Xx Yz - Xz Yx
!   AUz  = Xy Yx - Xx Yy
!   UxUy = Xx Yy + Xy Yx
!   UyUz = Xy Yz + Xz Yy
!   UzUx = Xz Yx + Xx Yz
!
! The left rotation matrix, u, is obtained from q by
!
!   u = qT1q
!
! INPUT
!   n      - number of points
!   x      - test vector
!   y      - reference vector
!   w      - weight vector
!
! OUTPUT
!   q      - the best-fit quaternion
!   u      - the best-fit left rotation matrix
!   nr     - number of jacobi sweeps required 
!
!     SUBROUTINE qtrfit (n, x, y, w, q, u, nr)
      SUBROUTINE qtrfit (n, x, y, w, q, u)
      IMPLICIT CHARACTER (A-Z)
!
      INTEGER n
      DOUBLE PRECISION x (3, n)
      DOUBLE PRECISION y (3, n)
      DOUBLE PRECISION w (n)
      DOUBLE PRECISION q (0 : 3)
      DOUBLE PRECISION u (3, 3)
      INTEGER nr
!
      DOUBLE PRECISION xxyx, xxyy, xxyz
      DOUBLE PRECISION xyyx, xyyy, xyyz
      DOUBLE PRECISION xzyx, xzyy, xzyz
      DOUBLE PRECISION c (0 : 3, 0 : 3), v (0 : 3, 0 : 3)
      DOUBLE PRECISION d (0 : 3)
      INTEGER i
!
      INTEGER lwork, info
      DOUBLE PRECISION ev(4)
      DOUBLE PRECISION, ALLOCATABLE :: work(:)
!
! generate the upper triangle of the quadratic form matrix
!
      xxyx = 0.0D0
      xxyy = 0.0D0
      xxyz = 0.0D0
      xyyx = 0.0D0
      xyyy = 0.0D0
      xyyz = 0.0D0
      xzyx = 0.0D0
      xzyy = 0.0D0
      xzyz = 0.0D0
      DO 11000 i = 1, n
        xxyx = xxyx + x (1, i) * y (1, i) * w (i)
        xxyy = xxyy + x (1, i) * y (2, i) * w (i)
        xxyz = xxyz + x (1, i) * y (3, i) * w (i)
        xyyx = xyyx + x (2, i) * y (1, i) * w (i)
        xyyy = xyyy + x (2, i) * y (2, i) * w (i)
        xyyz = xyyz + x (2, i) * y (3, i) * w (i)
        xzyx = xzyx + x (3, i) * y (1, i) * w (i)
        xzyy = xzyy + x (3, i) * y (2, i) * w (i)
        xzyz = xzyz + x (3, i) * y (3, i) * w (i)
11000   CONTINUE
!
      c (0, 0) = xxyx + xyyy + xzyz
!
      c (0, 1) = xzyy - xyyz
      c (1, 1) = xxyx - xyyy - xzyz
!
      c (0, 2) = xxyz - xzyx
      c (1, 2) = xxyy + xyyx
      c (2, 2) = xyyy - xzyz - xxyx
!
      c (0, 3) = xyyx - xxyy
      c (1, 3) = xzyx + xxyz
      c (2, 3) = xyyz + xzyy
      c (3, 3) = xzyz - xxyx - xyyy
!
! diagonalize c
!
!     nr = 16
!     CALL jacobi (c, 4, 4, d, v, nr)

      lwork = -1
      allocate(work(1))
      call dsyev('V','U',4,C,4,ev,work,lwork,info)
      lwork=int(work(1))
      deallocate (work)
      allocate( work(lwork) )
      call dsyev('V','U',4,C,4,ev,work,lwork,info)
      deallocate (work)
      if(info.ne.0) then
        write(6,*) 'Error in diagolization of quaterion matrix'
        stop
      endif

!
! extract the desired quaternion
!
      q (0) = C (0, 3)
      q (1) = C (1, 3)
      q (2) = C (2, 3)
      q (3) = C (3, 3)
!
! generate the rotation matrix
!
      CALL q2mat (q, u)
 
      RETURN
      END
