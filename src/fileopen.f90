      subroutine fileopen(iflag)
      use c_output, only: lwrite
      implicit none
      integer iflag
      if (iflag.eq.0) then
        if (lwrite(43)) open(43,file='rcomi.43')
        if (lwrite(44)) open(44,file='rcomi2.44')
        if (lwrite(45)) open(45,file='lindi.45')
        if (lwrite(46)) open(46,file='lindbina.46')
        if (lwrite(47)) open(47,file='lindireoder.47')
        if (lwrite(48)) open(48,file='lindireoderaverge.48')
        if (lwrite(49)) open(49,file='natominbin.49')
      else
        if (lwrite(43)) close(43)
        if (lwrite(44)) close(44)
        if (lwrite(45)) close(45)
        if (lwrite(46)) close(46)
        if (lwrite(47)) close(47)
        if (lwrite(48)) close(48)
        if (lwrite(49)) close(49)
      endif

      end subroutine fileopen
