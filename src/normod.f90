! DESCRIPTION OF THE SUBROUTINE
! Compute the normal modes using a semianalytical (~numerical) Hessian.
! The old version of the code assumed that the initial configuration XX0 was a minimum.
! It is now possible to use a Projected Hessian and consequently perform normal mode
! analysis everywhere provided the numerical Hessian is accurate enough (smooth derivatives)
! and that the proper option NTRAPZ has been selected in the ouput file.
!
! STRUCTURE OF THE SUBROUTINE
! FREQ = Frequency in 1/a.u.time for each mode (ouput)
! EWELL = Potential energy of the well (ouput)
! RTURN = HO approximation of the turning point distance for each mode (mass-scaled) (output).
! Local storage variables:
! hh: stepsize to calculate the Hessian numerically
! xxm: local storage of coordinates
! freqm: local storage of frequencies
! maxforce: Maximum force
! rmsforce: Root mean square force
!
      subroutine normod(im)

      use param, only: mnat, autoev, autoang, autocmi, autofs, mu
      use c_output, only: minprinttrapz
      use c_struct, only: xxm, symbolm, natom, imulti, icharge, mmm,  &
                          indmm, nvibmods, linearmol, cell, natoms
      use c_sys, only: repflag
      use c_initial, only: freq, nmvec, ivibtype, nsurf0, ewell, xnmref, &
                           repflgi, nsurfi
      use c_traj, only: zpe, ntrapz, dvec, gpemd, gpema, pema, pemd

      implicit none
! Argument
      integer :: im
! Local variables
      integer :: i,j,jpos,lpos,k,l,ndim,icount,iend,nlim
      integer :: jcharge,jmulti
      double precision,parameter ::  hh =1d-4  ! the stepsize in the finite difference calculation of the Hessian
      double precision :: nhbond(mnat-1), nhangle(mnat-1)  ! only needed to fill out argument list
      double precision :: gv1(3,mnat),gv2(3,mnat),hess(3*mnat,3*mnat)
      double precision :: freqm(3*mnat),gvsum,maxforce,rmsforce
! Temporary storage of repflag
      integer :: repf
! Diagonalization variables and parameters for dsyevd.f (more details in normod-trapz.f)
      integer :: info,lwork,nmax,liwork
      parameter (liwork = 3+15*mnat)
      parameter (lwork = 1+18*mnat+18*mnat**2)
      parameter (nmax=3*mnat)
      integer :: iwork(liwork)
      double precision :: work(lwork)
! Variables for the projected Hessian calculation
      double precision :: xproj(3,mnat),projhess(3*mnat,3*mnat)
      double precision :: right(3*mnat,3*mnat),proj(3*mnat,3*mnat)
      double precision :: dirmol(3),rdum1(3),rdum2(3,3)
      logical :: linmol,lprint
! JZ
!     double precision :: totmass
!     double precision, allocatable :: coord(:,:),prt(:,:)
!     natoms = natom(im)
!     allocate (coord(natoms,3),prt(3*natoms,3*natoms))

      if(im.gt.0) then
         jcharge=icharge(im)
         jmulti =imulti(im)
      else
         jcharge=999
         jmulti =999
      endif

      repf = repflgi
      natoms = natom(im)
      if (natoms.lt.2) then
         write(6,*) 'cannot do normal modes for ',natoms,' atoms'
         stop
      endif

! Initialize
      do i=1,3*natoms
         do j=1,3*natoms
            hess(i,j) = 0.d0
         enddo
      enddo
!  Transfer to local storage
!  This have been done in PREMOL
!      do i=1,natoms
!        ii=iatom(im)+i-1
!        do k=1,3
!          xxm(k,i) = xx0(k,ii)
!        enddo
!        symbolm(i)=symbol0(ii)
!      enddo
! print the exact geometry used in normal mode analysis so that
! we can use this as reference geometry to visulize normal mode
      write(6,*) ' Exact geometry used in the normal mode analysis '
      do i = 1, natoms
         write(6,'(a,3f15.6)') symbolm(i),xxm(:,i)
      enddo
      call getpem(xxm,indmm,symbolm,cell,natoms,pema,pemd,gpema, &
            gpemd,dvec,mmm,1,nsurf0,jcharge,jmulti)
      xnmref = xxm
      if (repflag.eq.0) then
!       adiabatic
         ewell(im) = pema(nsurf0)
      elseif (repflag.eq.1) then
!       diabatic
         ewell(im) = pemd(nsurf0,nsurf0)
      else
         write(6,*) 'REPFLAG = ',repflag,' in NORMOD'
         stop
      endif
! normal mode on surface nsurfi, with representation repflgi
      if (repf.eq.0) then
         do k=1,3
            do l=1,natoms
               gv1(k,l) = gpema(k,l,nsurfi)
            enddo
         enddo
      elseif (repf.eq.1) then
         do k=1,3
            do l=1,natoms
               gv1(k,l) = gpemd(k,l,nsurfi,nsurfi)
            enddo
         enddo
      else
         write(6,*) 'ERROR: REPFLGI = ',repf,' in NORMOD'
         stop
      endif
      write(6,*) 'Energy at this geometry = ',ewell(im)*autoev
      write(6,*)
      write(6,*) '      Atom      x,y,z      Gradient (eV/A)'
      gvsum = 0.d0
      maxforce=0.d0
      do l=1,natoms
         do k=1,3
            write(6,100) l,k,gv1(k,l)*autoev/autoang
            gvsum = gvsum + gv1(k,l)**2
            if (dabs(gv1(k,l)).gt.dabs(maxforce)) maxforce = gv1(k,l)
         enddo
      enddo
      gvsum = sqrt(gvsum)
      rmsforce = gvsum/sqrt(dble(3*natoms))
      write(6,*) 'Maximum gradient = ',maxforce*autoev/autoang,' eV/A'
      write(6,*) 'RMS of gradient  = ',rmsforce*autoev/autoang,' eV/A'
      if (maxforce.gt.1.0d-3 .or. rmsforce.gt.0.8d-3) then
         write(6,107) 'WARNING: Gradient too large,',  &
               ' the structure does not seem to be a minimum'
      endif
      write(6,'(/,a,/)') 'Computing and diagonalizing the Hessian...'
!
! Compute Hessian matrix numerically from gradients
!
      do i=1,3
         do j=1,natoms
            jpos = 3*(j-1) + i
! Calculate gradient at xx + stepsize
            xxm(i,j) = xxm(i,j) + hh
            call getpem(xxm,indmm,symbolm,cell,natoms,pema,pemd,gpema,  &
                  gpemd,dvec,mmm,1,nsurfi,jcharge,jmulti)
            if (repf.eq.0) then
!         adiabatic
               do k=1,3
                  do l=1,natoms
                     gv1(k,l) = gpema(k,l,nsurfi)
                  enddo
               enddo
            elseif (repf.eq.1) then
!         diabatic
               do k=1,3
                  do l=1,natoms
                     gv1(k,l) = gpemd(k,l,nsurfi,nsurfi)
                  enddo
               enddo
            else
               write(6,*) 'ERROR: REPFLGI = ',repf,' in NORMOD'
               stop
            endif
! Calculate gradient at xx - stepsize
            xxm(i,j) = xxm(i,j) - 2.0d0*hh
            call getpem(xxm,indmm,symbolm,cell,natoms,pema,pemd,gpema,  &
                  gpemd,dvec,mmm,1,nsurfi,jcharge,jmulti)
            if (repf.eq.0) then
!         adiabatic
               do k=1,3
                  do l=1,natoms
                     gv2(k,l) = gpema(k,l,nsurfi)
                  enddo
               enddo
            elseif (repf.eq.1) then
!         diabatic
               do k=1,3
                  do l=1,natoms
                     gv2(k,l) = gpemd(k,l,nsurfi,nsurfi)
                  enddo
               enddo
            else
               write(6,*) 'ERROR: REPFLGI = ',repf,' in NORMOD'
               stop
            endif

            do k=1,3
               do l=1,j  ! to make sure Hessian is symmetrical
!xxx         do l=1,natoms
                  lpos = 3*(l-1) + k
! Hessian matrix, 3NBRAT X 3NBRAT matrix
! Data ordered (x1,y1,z1,x2,y2,z2,...xN,yN,zN)
                  hess(jpos,lpos) = (gv1(k,l) - gv2(k,l))/(2.0d0*hh)
! Mass-scaling
                  hess(jpos,lpos) = hess(jpos,lpos)*mu/sqrt(mmm(j)*mmm(l))
                  hess(lpos,jpos) = hess(jpos,lpos) ! to make sure Hessian is symmetrical
               enddo
            enddo
!       Change the coordinate back to its initial value ie xx0(i,j)
            xxm(i,j) = xxm(i,j) + hh
         enddo
      enddo

!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! NEW (11/13/2007): projected Hessian included here to allow user to start a calculation far
!                             from the minimum
! Calculation of the Projected matrix P: we use mass-scaled coordinates for the projection matrix
! NTRAPZ = 0: use of the traditional Hessian for the initial normal mode analysis and
!                     TRAPZ is not applied during the dynamics
! NTRAPZ = 1: use of the projected Hessian for the initial normal mode analysis and
!                     TRAPZ is not applied during the dynamics
! NTRAPZ = 2: use of the traditional Hessian for the initial normal mode analysis and
!                     TRAPZ is applied during the dynamics
! NTRAPZ = 3: use of the projected Hessian for the initial normal mode analysis and
!                     TRAPZ is applied during the dynamics

      ndim = 3*natoms
 200  if (ntrapz.eq.1.or.ntrapz.eq.3) then

         do i=1,3
            do j=1,natoms
               xproj(i,j)= sqrt(mmm(j)/mu)*xxm(i,j)
!                coord(j,i) = xxm(i,j)
            enddo
         enddo
!        totmass = 0d0
!        do j = 1, natoms
!           totmass = totmass + mmm(j)
!        enddo

         if (linearmol(im).eq.2) then
!           call gen_prt(coord,mmm,totmass,natoms,ndim,prt)
!            do i = 1, ndim
!            do j = 1, ndim
!               proj(i,j) = prt(i,j)
!            enddo
!            enddo
            call projct(xproj,mmm,natoms,proj)
         elseif (linearmol(im).eq.1) then
! Call of the subroutine to determine bond direction of the given molecule
! linearmol(im) has already been determined in readin.F (see rotprin.f)
! nhangle and nhbond are not useful here and are not added in the call of
! linearbond
            linmol = .true.
!xxx            call linearbond(linmol,natoms,xxm,dirmol)  ! added nonoptional arguments (slm)
            call linearbond(linmol,natoms,xxm,dirmol, nhbond, nhangle)

            lprint = .true.
            call projctlin(lprint,xproj,mmm,natoms,dirmol,proj)
         endif

! Calculation of the projected Hessian
! F^p = t^(1-P)F(1-P) = (1-P)F(1-P)

         do i=1,ndim
            do j=1,ndim
               proj(i,j) = -proj(i,j)
               if (i.eq.j) proj(i,j) = 1.0d0 + proj(i,j)
            enddo
         enddo

         do i=1,ndim
            do j=1,ndim
               right(i,j) = 0.0d0
               do k=1,ndim
                  right(i,j) = right(i,j) + hess(i,k)*proj(k,j)
               enddo
            enddo
         enddo
         do i=1,ndim
            do j=1,ndim
               projhess(i,j) = 0.0d0
               do k=1,ndim
                  projhess(i,j) = projhess(i,j) + proj(i,k)*right(k,j)
               enddo
            enddo
         enddo

! Diagonalization of the projected Hessian matrix.
         call dsyevd('v','u',ndim,projhess,nmax,freqm,work,lwork,iwork, &
               liwork,info)

         write(6,*)'   Index   Force Const (mass-scaled Eh/a0^2)'
         do k=1,ndim
            write(6,101) k,freqm(k)
         enddo

! repackage (order from biggest to smallest)
! Sorting and calculating all the frequencies; negative frequencies are
! imaginary frequencies. Unit: 1/time
         do k=1,ndim
            freq(k,im) = sign( sqrt( abs( freqm(ndim-k+1) )/mu ), &
                  freqm(ndim-k+1) )
! Transfer the frequencies from local to their global storage
         enddo
         if(ivibtype(im).eq.1) then
            if(freq(ndim,im).lt.0.d0) then
               if(linearmol(im).eq.2) then
                  freq(ndim-6,im) = freq(ndim,im)
               elseif(linearmol(im).eq.1) then
                  freq(ndim-5,im) = freq(ndim,im)
               endif
            else
               write(6,*)"Positive frequency for mode ",k," !"
               write(6,*)"Cant do bound modes!"
               stop
            endif
         endif
         do i=1,3
            do j=1,natoms
               jpos = 3*(j-1)+i
               do k=1,ndim
                  nmvec(i,j,ndim-k+1,im) = projhess(jpos,k)
               enddo
            enddo
         enddo

         if(ivibtype(im).eq.1) then
            do i=1,3
               do j=1,natoms
                  jpos = 3*(j-1)+i
                  nmvec(i,j,nvibmods(im),im) = projhess(jpos,1)
               enddo
            enddo
         endif

! Determination of nvibmods(im)
! Here we only work with positive frequencies
         nvibmods(im) = 0
         do i=1,ndim
            if (freq(i,im)*autocmi.gt.1.0d0)   &
                  nvibmods(im) = nvibmods(im) + 1
         enddo

         if(.not.minprinttrapz) then
            write(6,*)
            write(6,108) 'Keeping ',nvibmods(im),' modes with positive ', &
                  ' or imaginary frequencies.'
            write(6,109) (freq(k,im)*autocmi,k=1,nvibmods(im))
            write(6,108) 'Rejecting ',ndim-nvibmods(im),' other ', &
                  'frequencies (overall translation and rotation).'
            write(6,109) (freq(k,im)*autocmi,k=nvibmods(im)+1,ndim)
            write(6,*)
         endif

      elseif (ntrapz.eq.0.or.ntrapz.eq.2) then

! Diagonalization of the Hessian matrix
! The Hessian may only be used at minima (not recommended if there is a
! difference with the projected Hessian)

         call dsyevd('v','u',ndim,hess,nmax,freqm,work,lwork,iwork, &
               liwork,info)

         if(info.ne.0)then
            write(6,*)' In normod.f after return from dsyevd info=',info
            stop
         endif

         write(6,*)'   Index   Force Const (mass-scaled Eh/a0^2)'
         do k=1,ndim
            write(6,101) k,freqm(k)
         enddo

! repackage (order from biggest to smallest)
!  Sorting and calculating all the frequencies, negative frequencies are
!  imaginary frequencies. Unit: 1/time
         do k=1,ndim
            freq(k,im) = sign( sqrt( abs( freqm(ndim-k+1) )/mu ), &
                  freqm(ndim-k+1) )
! Transfer the frequencies from local to their global storage
         enddo
         do i=1,3
            do j=1,natoms
               jpos = 3*(j-1) + i
               do k=1,ndim
                  nmvec(i,j,ndim-k+1,im) = hess(jpos,k)
               enddo
            enddo
         enddo

         if(ivibtype(im).eq.1) then
            do i=1,3
               do j=1,natoms
                  jpos = 3*(j-1)+i
                  nmvec(i,j,nvibmods(im),im) = hess(jpos,1)
               enddo
            enddo
         endif


! This part is useful if we have planned to do the calculation at a minimum with the traditional
! Hessian (NTRAPZ = 0 or 2)
! If the molecule is linear k=1,ndim-5
! If the molecule is nonlinear k=1,ndim-6
! Print out the least 5 or 6 frequencies

! Determination of nvibmods(im)
! Here we only work with positive frequencies
         nvibmods(im) = 0
         do i=1,ndim
            if (freq(i,im)*autocmi.gt.1.0d0) &
                  nvibmods(im) = nvibmods(im) + 1
         enddo

         if(ivibtype(im).eq.1) then
            if(freq(ndim,im).lt.0.d0) then
               if(linearmol(im).eq.2) then
                  freq(ndim-6,im) = freq(ndim,im)
               elseif(linearmol(im).eq.1) then
                  freq(ndim-5,im) = freq(ndim,im)
               endif
            else
               write(6,*)"Positive frequency for mode ",k," !"
               write(6,*)"Cant do bound modes!"
               stop
            endif
         endif

         write(6,*)
         write(6,107) 'The lowest 5 or 6 frequencies (cm-1) deemed as', &
               ' translation and rotation:'
! nvibmods(im) = ndim-5
         if(ivibtype(im).eq.0) then
            write(6,'(1x,5f12.5)') (freq(k,im)*autocmi,  &
                  k=nvibmods(im)+1,ndim)
         elseif(ivibtype(im).eq.1) then
            write(6,'(1x,5f12.5)') (freq(k,im)*autocmi,  &
                  k=nvibmods(im)+2,ndim)
         endif
         write(6,108) 'Keeping ',nvibmods(im),' modes with largest', &
               ' frequencies.'
         write(6,*)
! Judge if the structure given is a local minimum. Only need to
! judge on the smallest one. (Freq(k) stored in an order of biggest to
! smallest)
         if (freq(nvibmods(im),im).lt.0.d0) then
            write(6,*) 'WARNING!: Negative frequency for mode ',k,' !'
            write(6,*) 'Cant do unbound modes!'
            stop
         elseif (freq(ndim,im)*autocmi.lt.-1.5d2) then
            write(6,*)'WARNING: The structure seems not a local minimum!'
            write(6,107) 'A negative frequency lower than 150.0 cm-1 has', &
                  ' been found!'
!          stop
         elseif(ivibtype(im).eq.1) then
            if(freq(nvibmods(im),im).gt.0.d0) then
               write(6,*)"Positive frequency for last mode!"
               write(6,*)"This is not a saddle point!"
               stop
            endif
         else
            continue
         endif

      else

         write(6,*)'###################################################'
         write(6,*)'                W A R N I N G                      '
         write(6,*)'NTRAPZ can only equal 0, 1, 2 or 3 and not ',ntrapz
         write(6,*)'The default is used ie NTRAPZ = 0'
         write(6,*)'###################################################'
         write(6,*)
         ntrapz = 0
         go to 200

      endif

!c Test on initial normal modes
!      do k=1,ndim
!         write(6,*) 'Normal mode nb ',k
!         write(6,*) 'Frequency of this mode =',freq(k,im)*autocmi,'cm-1'
!         do j=1,natoms
!            print *,(nmvec(i,j,k,im),i=1,3)
!         enddo
!         write(6,*)
!      enddo

! Print out vibrational modes
! Gaussian type of output
! icount: count for the time of loops over k
! iend:   the end
      icount=0
      iend=0
      k=1
      if(ivibtype(im).eq.0) then
         nlim = nvibmods(im)
      elseif(ivibtype(im).eq.1) then
         nlim = nvibmods(im) + 1
      endif
      do while(k.le.nlim)
         icount=icount+1
         if (iend.lt.(nlim/3)*3 ) then
            iend=3*icount
         else
            iend=nvibmods(im)
         endif
         write(6,102) (i,i=k,iend)
         write(6,103) 'Freq (cm-1)',(freq(i,im)*autocmi,    i=k,iend)
         write(6,103) 'Freq (1/fs)',(freq(i,im)/autofs,     i=k,iend)
         write(6,103) 'Freq (ev)  ',(freq(i,im)*autoev,i=k,iend)
         write(6,104) 'Atom','Symb','x','y','z','x','y','z','x','y','z'
         do j=1,natoms
            write(6,105) j,symbolm(j), &
                  ((nmvec(l,j,i,im),l=1,3),i=k,iend)
         enddo
         k=iend+1
      enddo
      write(6,*)
! Zero-point vibrational energy
      zpe(im)=0.d0
      do i=1,nvibmods(im)
         zpe(im) = zpe(im)+0.5d0*freq(i,im)
      enddo
      write(6,106) 'Zero-point vibrational energy for AG ',im,' is: ', &
            zpe(im)*autoev
! All formats go here.
      100  format(1x,2i10,f20.10)
      101  format(1x,i10,e12.5)
      102  format(15x,3i21)
      103  format(1x,a14,3f21.4)
      104  format(1x,2a7,9a7)
      105  format(1x,i7,a7,9f7.2)
      106  format(1x,a,i10,a,f12.5)
      107  format(1x,2a)
      108  format(1x,1a,i5,2a)
      109  format(1x,10d16.8)

end subroutine normod

!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Description of the subroutine
! Test of the linearity of the molecule im
! 1. Calculating all vectors between atom 1 and other atoms
!    (the only useful step if the molecule is known linear)
! 2. Calculating bond lengths and angles
!
! STRUCTURE OF THE SUBROUTINE (input/output)
! linmol: .true. if we only want to know the direction of a linear molecule
!         .false. if we want to know all bond lengths and angles
! nbrat : number of atoms of molecule im (input)
! coord(3,mnat): Cartesian positions of atoms (input)
! dirmol(3): direction axis of the linear molecule (output)
!
! Last modification 03/10/2008
!
subroutine linearbond(linmol,nbrat,coord,dirmol,nhbond,nhangle)
!      subroutine linearbond(linmol,nbrat,coord,dirmol,nhbond,nhangle, &
!                                    nhangle2)

      use param, only: mnat, pi

      implicit none
      integer :: i,j,nbrat
      double precision :: coord(3,mnat),nhvec(3,mnat-1)
      double precision :: nhbond(mnat-1),nhangle(mnat-1),dirmol(3)
      logical :: linmol

      if (linmol) then

         do i=1,3
            nhvec(i,1) = coord(i,1) - coord(i,nbrat)
         enddo
         nhbond(1) = sqrt(nhvec(1,1)**2+nhvec(2,1)**2+nhvec(3,1)**2)
         do i=1,3
            dirmol(i) = nhvec(i,1)/nhbond(1)
         enddo
         write(6,111)' Direction of the linear molecule: ', &
               (dirmol(i),i=1,3)

      else

         do i=1,nbrat-1
            nhangle(i) = 0.0d0
!            nhangle2(i) = 0.0d0
            do j=1,3
               nhvec(j,i) = coord(j,i+1)-coord(j,1)
            enddo
         enddo
         do i=1,nbrat-1
            nhbond(i) = sqrt(nhvec(1,i)**2+nhvec(2,i)**2+nhvec(3,i)**2)
         enddo

!c For the calculation of H-N-H angles with Al-Kashi formulas
!         hhbond(1) = sqrt( (coord(1,3) - coord(1,4))**2  &
!                          + (coord(2,3) - coord(2,4))**2  &
!                          + (coord(3,3) - coord(3,4))**2 )
!         hhbond(2) = sqrt( (coord(1,2) - coord(1,4))**2  &
!                          + (coord(2,2) - coord(2,4))**2  &
!                          + (coord(3,2) - coord(3,4))**2 )
!         hhbond(3) = sqrt( (coord(1,2) - coord(1,3))**2  &
!                          + (coord(2,2) - coord(2,3))**2  &
!                          + (coord(3,2) - coord(3,3))**2 )

! Calculation of H-N-H angles with the scalar product definition
         do i=1,nbrat-1

            if (i.lt.nbrat-1) then
               nhangle(i) = dacos( ( nhvec(1,i)*nhvec(1,i+1) +  &
                     nhvec(2,i)*nhvec(2,i+1) +  &
                     nhvec(3,i)*nhvec(3,i+1) )  &
                     /nhbond(i)/nhbond(i+1) )*180.0d0/pi

            else
               nhangle(i) = dacos( ( nhvec(1,i)*nhvec(1,1) +  &
                     nhvec(2,i)*nhvec(2,1) +           &
                     nhvec(3,i)*nhvec(3,1) )           &
                     /nhbond(i)/nhbond(1) )*180.0d0/pi
            endif

         enddo

!c Calculation of H-N-H angles with Al-Kashi theorem
!         nhangle2(1) = dacos( (nhbond(1)**2+nhbond(2)**2-hhbond(3)**2)   &
!                             /(2.0d0*nhbond(1)*nhbond(2)) )*180.0d0/pi
!         nhangle2(2) = dacos( (nhbond(2)**2+nhbond(3)**2-hhbond(1)**2)   &
!                             /(2.0d0*nhbond(2)*nhbond(3)) )*180.0d0/pi
!         nhangle2(3) = dacos( (nhbond(3)**2+nhbond(1)**2-hhbond(2)**2)   &
!                             /(2.0d0*nhbond(3)*nhbond(1)) )*180.0d0/pi

      endif

! Formats go here
      111  format(a,3d12.4)

end subroutine linearbond
