      subroutine rantherm(temp0m,im)

! Generate momenta randomly according to a Maxwell distribution.

      use param, only: kb
      use c_struct, only: natoms, mmm
      use c_traj, only: ppm

      implicit none
      double precision temp0m
! local
      integer i,j,im
      double precision x1,x2,w,y1,y2,keim,signs,kbtm,tmp,r1,r2

! convert uniformly distributed numbers to a gaussian distributed set
! polar form of the Box-Muller transformation
! transformed random numbers are from a distribution that has zero mean
! and unit standard deviation
      kbtm=kb*temp0m
      do i=1,natoms
        tmp=dsqrt( kbtm * mmm(i) )
        do j=1,3
! 111      r1=1.0
!          do while(r1.eq.1.0)
!            r1 = sprng(rng_stream)
!          enddo
!          keim = -0.5d0*dlog(1.d0-r1 )*kbtm
!          if (itempzpe) keim = -dlog(1.d0-r1 )*kbtm
!     &           + zpe(im)/dble(3*natoms)
!          r1 = sprng(rng_stream)
!          if (r1.gt.dexp(-keim/kbtm) ) goto 111
!          r2 = sprng(rng_stream)
!          if (r2 .gt. 0.5) then
!                  signs=1.d0
!            else
!                  signs=-1.d0
!          endif
!          ppm(j,i)=signs*dsqrt( 2.d0 * mmm(i) * keim )
 10       continue
          call get_rng(r1)
          call get_rng(r2)
          x1=2.d0*r1-1.d0
          x2=2.d0*r2-1.d0
          w = x1*x1+x2*x2
          if (w.ge.1.d0.or.w.eq.0.d0) goto 10
          w = dsqrt(-2.d0*dlog(w)/w)
          y1 = x1*w
!          y2 = x2*w
! you automatically get two random numbers (y1 and y2)
! i'm just going to use one for now, inefficient but simpler
!        transform to velocity kb*tempt, mmm, and ppm are in a.u.
!          ppm(j,i)=y1*dsqrt( kbtm * mmm(i) )
          ppm(j,i)=y1*tmp
!        using some Metropolis method to control the momentum so
!        that it will not be too large. The correct metropolis method
!        should be to reject ppm if r1 > exp(-Ei/kT)
!          r1 = sprng(rng_stream)
!          if (r1.gt.dexp(-y1**2/2) ) goto 10
        enddo
      enddo
      do i=1,natoms
        write(6,'(1x,i5,4f12.4)') i,mmm(i),(ppm(j,i),j=1,3)
      enddo
 
      end subroutine rantherm
