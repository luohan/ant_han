      subroutine momigen(xxm,mmm,natoms,momi)
!
! Calculate moment of intertia matrix momi
! xxm, ppm, mmm: Local storage of xx, pp and mm
      use param, only: mnat
      use c_output, only: idebug

      implicit none
      integer :: natoms
      double precision :: xxm(3,mnat),mmm(mnat)
      double precision :: momi(3,3),x,y,z

! local
      integer :: i,j
! compute moment of inertia matrix momi
      do i=1,3
        momi(i,1) = 0.d0
        momi(i,2) = 0.d0
        momi(i,3) = 0.d0
      enddo
!
      do i=1,natoms
         x=xxm(1,i)
         y=xxm(2,i)
         z=xxm(3,i)
         momi(1,1)=momi(1,1)+mmm(i)*(y*y+z*z)
         momi(2,2)=momi(2,2)+mmm(i)*(x*x+z*z)
         momi(3,3)=momi(3,3)+mmm(i)*(x*x+y*y)
         momi(1,2)=momi(1,2)-mmm(i)*x*y
         momi(1,3)=momi(1,3)-mmm(i)*x*z
         momi(2,3)=momi(2,3)-mmm(i)*y*z
      enddo
      momi(2,1)=momi(1,2)
      momi(3,1)=momi(1,3)
      momi(3,2)=momi(2,3)
      if (idebug) then
         write(6,*) 'Moment of inertia matrix'
         do i=1,3
           write(6,'(1x,4f15.3)') (momi(i,j),j=1,3)
         enddo
       endif

      end subroutine momigen
