      subroutine getdvec2(nclu,u2b2,gu2b2,dvec2b2)

! Computes an effective nonadiabatic coupling vector c
! from a 2x2 diabatic matrix. (Diabatic representation)

      use param, only: mntraj, mnat
      use c_sys, only: nsurft

      implicit none
      integer :: i1,i2,k,l,nclu
      double precision :: u2b2(2,2),gu2b2(3,mnat,2,2),dvec2b2(3,mnat)
!RMP100813
      double precision :: v2b2(2),cc(2,2),v1,v2
!     compute adiabatic info
!     diagonalize the 2x2 and compute the eigenvectors
      call diag2(u2b2,v2b2,cc)
      v1=v2b2(1)
      v2=v2b2(2)
! compute d
      do i1 = 1,3
       do i2 = 1,nclu
        dvec2b2(i1,i2) = 0.d0
        do k = 1, 2
         do l = 1, 2
          dvec2b2(i1,i2) = dvec2b2(i1,i2)   &
                          +cc(k,2)*cc(l,1)*gu2b2(i1,i2,k,l)
          enddo
        enddo
        if ((v1-v2) .ne. 0.0d0) then
! Bug in the sign of the 2x2 reduced non-adiabatic couplings    RMP
!          dvec2b2(i1,i2) = dvec2b2(i1,i2)/(v2-v1)
          dvec2b2(i1,i2) = dvec2b2(i1,i2)/(v1-v2)
        else
          dvec2b2(i1,i2) = 0.0d0
        endif
       enddo
      enddo

      end subroutine getdvec2


! RMP100813
      subroutine diag2(u,v,cc)
! Compute the eigenvalues and the eigenvectors of a 2x2 diabatic energy matrix 
      implicit none   
      double precision u(2,2)
!     output 
      double precision v(2),cc(2,2)
! 
      double precision u11,u12,u21,u22,v1,v2,sn1,cs1,tmp
!     compute adiabatic info
!     diagonalize the 2x2
      u11 = u(1,1)
      u12 = u(1,2)
      u21 = u(2,1)
      u22 = u(2,2)
      call dlaev2(u11,u12,u22,v2,v1,cs1,sn1)
!     phase convention (this part depends on how DLAEV2 works)
      if (v2.ge.v1) then
          cc(1,1) = -sn1
          cc(2,1) = cs1
          cc(1,2) = cs1
          cc(2,2) = sn1
      else
          tmp = v1
          v1 = v2
          v2 = tmp
          cc(1,1) = cs1
          cc(2,1) = sn1
          cc(1,2) = -sn1
          cc(2,2) = cs1
      endif
      v(1) = v1
      v(2) = v2
      end subroutine diag2
!
! This subroutine computes the reduced nonadiabatic coupling vectors 
! from the adiabatic energies and the gradients of diabatic potential 
! matrix elements for a multisurface system. Equations (45) and (46) 
! taken from the book chapter "Non-Born-Oppenheimer Molecular Dynamics 
! for Conical Intersections, Avoided Crossings, and Weak Interactions," 
! A. W. Jasper and D. G. Truhlar, in Conical Intersections: Theory, 
! Computation, and Experiment, edited by W. Domcke, D. R. Yarkony, and 
! H. Köppel (World Scientific, Singapore, 2011), pp. 375-412. (chapter 10) 
! [Adv. Ser. Phys. Chem. 17, C375-412 (2011)].  C or from 
! ANT07 and is described in M.D. Hack et al., J. Phys. Chem. A 103, 6309 (1999)
! (see Appendix A).
!
!RMP100813
! For the diabatic representation
!
      subroutine getdvecd(nclu,nsurf,pemd,gpemd,dvec)
      use param, only: mntraj, mnsurf, mnat
      use c_sys, only: nsurft
      implicit none
! input
      integer :: nsurf,nclu
      double precision :: pemd(mnsurf,mnsurf),gpemd(3,mnat,mnsurf,mnsurf)
! output
      double precision :: dvec(3,mnat,mnsurf,mnsurf)
!
      integer  :: i,j,k 
      double precision :: u2b2(2,2),gu2b2(3,mnat,2,2),v2b2(2),cc(2,2)
      double precision :: dvec2b2(3,mnat)
! 
      do k=1,nsurft
        if(k.ne.nsurf) then
              u2b2(1,1) = pemd(k,k)
              u2b2(1,2) = pemd(k,nsurf)
              u2b2(2,1) = pemd(nsurf,k)
              u2b2(2,2) = pemd(nsurf,nsurf)
              do i=1,3
               do j=1,NCLU
                gu2b2(i,j,1,1) = gpemd(i,j,k,k)
                gu2b2(i,j,1,2) = gpemd(i,j,k,nsurf)
                gu2b2(i,j,2,1) = gpemd(i,j,nsurf,k)
                gu2b2(i,j,2,2) = gpemd(i,j,nsurf,nsurf)
               enddo
              enddo
              call getdvec2(NCLU,u2b2,gu2b2,dvec2b2)
              do i=1,3
               do j=1,NCLU
! Bug in the sign of the 2x2 reduced non-adiabatic couplings    RMP
                if(k.gt.nsurf) then
                 dvec(i,j,k,nsurf) = dvec2b2(i,j)
                else
                 dvec(i,j,k,nsurf) = -dvec2b2(i,j)
                endif
!                 dvec(i,j,nsurf,k) = -dvec2b2(i,j)
                 dvec(i,j,nsurf,k) = -dvec(i,j,k,nsurf) 
               enddo
              enddo
        endif
      enddo
      return
      end


! RMP100813
      subroutine getdvect(nclu,gpemd,pema,dvec,cc)
      use param, only: mnat, mnsurf, mntraj
      use c_sys, only: nsurft
      implicit none 
! input
      integer :: nclu
      double precision :: pema(mnsurf)
      double precision :: cc(mnsurf,mnsurf),gpemd(3,mnat,mnsurf,mnsurf)
! output
      double precision :: dvec(3,mnat,mnsurf,mnsurf)
!
      integer :: i1,i2,i,j,k,l
!
!     Program to compute nonadiabatic coupling vectors from the adiabatic energies and
!     the gradients of diabatic potential matrix elements for a multisurface system
!     The formula is taken from ANT07 and is described in M.D. Hack et al., J. Phys. Chem. A 103, 6309 (1999)
!
! compute d
      do i1 = 1,3
      do i2 = 1,nclu

        do i = 1,nsurft
          dvec(i1,i2,i,i) = 0.d0
        enddo

        do i = 2,nsurft
          do j = 1, i-1
            dvec(i1,i2,i,j) = 0.d0
            do k = 1, nsurft
             do l = 1, nsurft
              dvec(i1,i2,i,j) = dvec(i1,i2,i,j)   &
                              + cc(k,i)*cc(l,j)*gpemd(i1,i2,k,l)
             enddo
            enddo
            if ((pema(j) - pema(i)) .ne. 0.0d0) then
              dvec(i1,i2,i,j) = dvec(i1,i2,i,j)/(pema(j)-pema(i))
            else
              dvec(i1,i2,i,j) = 0.0d0
            endif
            dvec(i1,i2,j,i) = -dvec(i1,i2,i,j)
          enddo
        enddo
      enddo
      enddo
 
      end 
