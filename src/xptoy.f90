      subroutine xptoy(y,dydx,nv,ido)
!
! The integrators integrate a single array Y using its derivatives
! DYDX.  This subroutine transforms back and forth between the 
! Y array and the various quantities.
! The Y array contains Elements:
!   1 to NATOMS              -> X coordinates of all atoms XX(1,i)
!   NATOMS+1 to 2*NATOMS     -> Y coordinates of all atoms XX(2,i)
!   2*NATOMS+1 to 3*NATOMS   -> Z coordinates of all atoms XX(3,i)
!   3*NATOMS+1 to 4*NATOMS   -> X momenta of all atoms PP(1,i)
!   4*NATOMS+1 to 5*NATOMS   -> Y momenta of all atoms PP(2,i)
!   5*NATOMS+1 to 6*NATOMS   -> Z momenta of all atoms PP(3,i)
!   6*NATOMS+1 to 6*NATOMS+NSURFT   -> PHASE(k)
!   6*NATOMS+NSURFT+1 to 6*NATOMS+2*NSURFT-> Real part of the electronic
!                                          variables CRE(k)
!   6*NATOMS+2*NSURFT+1 to 6*NATOMS+3*NSURFT -> Imaginary part of the electronic
!                                          variables CRE(k)
!   6*NATOMS+3*NSURFT+1 to 6*NATOMS+4*NSURFT -> Real part of the electronic
!                                           variables (coherent part for CSDM)
!   6*NATOMS+4*NSURFT+1 to 6*NATOMS+5*NSURFT -> Imaginary part of the electronic
!                                          variables (coherent part for CSDM)
!  RVM
!   6*NATOMS+5*NSURFT+1 to 6*NATOMS+5*NSURFT+ 3*NSURFT(NSURFT-1)/2 -> bjk elements,
!   bjk+ elements, and bjk- elements
!  RVM
!
!   y(nv-1), y(nv):         Nose-Hoover variable
!   dydx(nv-1), dydx(nv):  time derivatives of Nose-Hoover variables
!
      use param, only: mnyarray
      use c_struct, only: infree, xxm, mmm, nh, natoms
      use c_sys, only: intflag, nsurft, methflag
      use c_traj, only: inh, bfunc, pem, kbtarg, gcim, gcre, cim, cre, &
                        phase, gv, ppm
                  
      implicit none
      integer :: nv
      integer :: i,j,ido,i2,i3,i4,nat3,nat6
      double precision :: y(mnyarray),dydx(mnyarray)
!  RVM
      integer :: k,km,kp,kels
      kels = nsurft*(nsurft-1)/2
!  RVM
!
      nat3=3*natoms
      nat6=6*natoms
! For Nose-Hoover: variable nh(2): nh(1): Q, nh(2): ps, nh(3) dnh(1)/dt
      if (ido.eq.1) then

! break y into components
! get info from y and dydx
        nh(3,1)=0.d0
        do i=1,natoms
          do j=1,3
            i3=(j-1)*natoms+i
            i4=i3+nat3
            xxm(j,i)=y(i3)
            ppm(j,i)=y(i4)
            gv(j,i)=-dydx(i4)
! update dnh(2,1)/dt i.e. nh(3,1)
! nh(3,1) here is actually the kinetic energy of the AG
            if (inh) nh(3,1) = nh(3,1) + y(i4)**2/mmm(i)
          enddo
        enddo
        do i=1,nsurft
          phase(i) = y(nat6+i)
        enddo
        do i=1,nsurft
          cre(i) = y(nat6+nsurft+i)
          cim(i) = y(nat6+2*nsurft+i)
          gcre(i) = dydx(nat6+nsurft+i)
          gcim(i) = dydx(nat6+2*nsurft+i)
        enddo

        if (methflag.eq.4) then
! CSDM (coherent terms)
          do i=1,nsurft
            i2 = i + nsurft
            cre(i2) = y(nat6+3*nsurft+i)
            cim(i2) = y(nat6+4*nsurft+i)
            gcre(i2) = dydx(nat6+3*nsurft+i)
            gcim(i2) = dydx(nat6+4*nsurft+i)
          enddo
        endif

! Variable for Nose-Hoover thermostat
        if (inh) then
          nh(2,1)=y(nv-1)
          nh(2,2)=y(nv)
          nh(3,1)=(nh(3,1)-dble(infree)*kbtarg)/nh(1,1)-nh(2,1)*nh(2,2)
          nh(3,2)=(nh(1,1)*nh(2,1)**2-kbtarg)/nh(1,2)
        endif

      else
! assemble components into a 1-D vector for integration: x1 x2
! x3...y1 ... y(natoms)z1...z(natoms)
        do i=1,natoms
          do j=1,3
            i3=(j-1)*natoms+i
            i4=i3+nat3
            y(i3)=xxm(j,i)
            y(i4)=ppm(j,i)
            dydx(i3)=ppm(j,i)/mmm(i)
            dydx(i4)=-gv(j,i)
!     update dnh(1)/dt i.e. nh(3) 
            if (inh) dydx(i4)=dydx(i4)-nh(2,1)*y(i4)
          enddo
        enddo
        do i=1,nsurft
          y(nat6+i) = phase(i)
          dydx(nat6+i) = pem(i)
        enddo
        do i=1,nsurft
          y(nat6+nsurft+i) = cre(i)
          y(nat6+2*nsurft+i) = cim(i)
          dydx(nat6+nsurft+i) = gcre(i)
          dydx(nat6+2*nsurft+i) = gcim(i)
        enddo

        if (methflag.eq.4) then
!       CSDM (coherent terms)
          do i=1,nsurft
            i2 = i + nsurft
            y(nat6+3*nsurft+i) = cre(i2)
            y(nat6+4*nsurft+i) = cim(i2)
            dydx(nat6+3*nsurft+i) = gcre(i2)
            dydx(nat6+4*nsurft+i) = gcim(i2)
          enddo
        endif
!  RVM 
        if(intflag.eq.6) then
!        Store bjk, bjk+ and bjk- (Mike Hack's method)           
           do i= 2, nsurft
             do j= 1, i-1
                k = (i-1)*(i-2)/2 + j + 6*natoms +5*nsurft
                kp = k + kels
                km = k + 2*kels
                dydx(k) = bfunc(j,i)
! end
                dydx(kp) = 0.d0
                dydx(km) = 0.d0
                
                if(dydx(k) .gt. 0.d0) then
                  dydx(kp) = dydx(k)
                elseif (dydx(k) .lt. 0.d0) then
                  dydx(km) = dydx(k)
                endif      
             enddo
           enddo
        endif
!  RVM

!       Variable for Nose-Hoover thermostat
        if (inh) then
          y(nv-1)    = nh(2,1)
          y(nv)      = nh(2,2)
          dydx(nv-1) = nh(3,1)
          dydx(nv)   = nh(3,2)
        endif

      endif
      end subroutine xptoy
