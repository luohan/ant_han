      subroutine premol(im)

! Precompute information for each atom group.
! xxm, ppm, mmm: temporary storage of xx, pp, mm
! comxxm:        temporary storage of CoM

        use c_struct, only: mmm, xxm, xx0, indatom, symbol0, mm0, natoms, &
          natom, symbolm, iatom, indmm
        use c_sys, only: nsurft,ntraj
        use c_initial, only: ewell, vibsel, initx, arrad, nsurf0, &
          bmaxad, bminad, easym, rasym, bparam, adbmode, ademod, &
          phmode,temp0im,nspin,nspinmode,rotelevel,nrotmax
        use c_traj, only: zpe, jjad, vvad, escatad, ecolad, einti,&
          & rinad,routad,tauad,pprelad,phasead,kbt0
        use param, only: autoang,autoev,kb
        use hdf5_output, only: lwrite_h5, hdf5o_append_attr

        implicit none
        integer im
        integer i,j,ii
        double precision mtotal,com(3),evibi

        kbt0=kb*temp0im(im)
        natoms=natom(im)
        write(6,*) 'Precomputing info for AG ',im
        write(6,*) '------------------------------'

! Initialize
        mtotal = 0d0
        do i=1,natoms
          ii=iatom(im)+i-1
          mmm(i)     = mm0(ii)
          mtotal = mtotal + mmm(i)
          symbolm(i) = symbol0(ii)
          indmm(i)   = indatom(ii)
          do j=1,3
            xxm(j,i)=xx0(j,ii)
          enddo
        enddo
!
        call comgen(xxm,mmm,natoms,com,mtotal)
!
! NORMAL MODE METHOD
        zpe(im)=0.d0
        ewell(im)=0.d0
!xxx     if (vibsel(im).ne.0.or.(vibsel(im).eq.0.and.ltunn)) then
        if (vibsel(im).ne.0) then
          write(6,*) 'Normal mode analysis'
          if (natoms.eq.1) then
            write(6,*) 'Nothing to do for just one atom'
          else
            call normod(im)
          endif
        elseif (initx(im).eq.4) then
! ATOM-DIATOM METHOD RMP14
!     Always: nmol=1, natoms=3, im=1
          write(6,*) 'INITx = 4:  Atom-diatom initial conditions'
          call preatomdiatom(escatad,ecolad,einti,temp0im(1),jjad,vvad,arrad, &
            mmm,rinad,routad,tauad,phasead,bmaxad,bminad, &
            pprelad,nsurf0,nsurft,easym,rasym, &
            bparam,adbmode,ademod,phmode,evibi)
          if (ademod .eq. 4) then
            call preadrot(vvad,arrad,nsurf0,mmm,ntraj, &
              nspin,nspinmode,phmode,kbt0)
          end if
          if (lwrite_h5(30)) then
            call hdf5o_append_attr(30,'Re',rasym(nsurf0,arrad)*autoang)
            call hdf5o_append_attr(30,'Eelec',easym(nsurf0,arrad)*autoev)
            call hdf5o_append_attr(30,'Eint',einti*autoev)
            call hdf5o_append_attr(30,'Evib',evibi*autoev)
            if (ademod .eq. 4) then
              call hdf5o_append_attr(30,'Erot',rotelevel(0:nrotmax)*autoev)
            end if
          end if
!        call preatomdiatom(escatad,jjad,vvad,arrad,
!     &    rrad0,mmm,rinad,routad,tauad,bparam,
!     &    ,pprelad,ecolad,nsurf0,nsurft,easym,rasym)
        else
          write(6,*) 'Nothing to do...'
          write(6,*)
        endif

      end subroutine premol
