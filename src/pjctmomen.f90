      subroutine pjctmoment(pdi,nat,mass,px,xx,tunnd,tuntype)
! This subroutine calculates p dot d_i where d_i is the tunneling 
! direction. If p dot d_i is 0 or changes sign, we have reached a turning point
! RMP
      use param, only: mnat
      use c_initial, only: itunn, drr

      implicit none
      integer :: i,j,k,l,it,nat,nintc
      integer :: iq,tuntype
      real*8  :: qq(3*mnat),xout(3,mnat)
      real*8  :: pdi(3*nat),mass(mnat),px(3,mnat),xx(3,mnat),vx
      real*8  :: tunnd(3,mnat,3*mnat-6),qtunn(3*mnat),amat(3*mnat,3*mnat)
      double precision :: tund(3,mnat)
      logical :: lterm

      nintc = 3*nat-6
      qq(:) = 0d0
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Case 1. tunneling coordinate is a single internal coordinate  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      if(tuntype.eq.1) then
! going through all tunneling coordinates
      do it = 1, nintc
!     write(6,*) 'drr(it) ', drr(it),it
!       if(itunn(it).eq.1) qq(it) = 5d-7*drr(it)
! calculate the corresponding Cartesian coordinates iteratively
!       call iteramat(xx,xout,qq,nat,mass,lterm)
!
! calculate tunneling direction in Cartesian Coordinates
!
        if(itunn(it).eq.1) then
          qq(it) = 5d-7*drr(it)
          call iteramat(xx,xout,qq,nat,mass,lterm,amat)
! turning direction
          do i = 1, nat
            do j = 1, 3
              tunnd(j,i,it) = xout(j,i) - xx(j,i)
            enddo
          enddo
! RMP 
! The tunneling direction vector is normalized, d_i is transformed in D_i
          call normd(tunnd(:,:,it),nat)
! RMP 

          pdi(it)=0d0
          do i =1, nat
            do j =1, 3
! Using the normalized P and D vectors to compute P.D dot product
              vx = px(j,i)/mass(i)
! using normal mode for tunneling direction
!xxx              pdi(k) = pdi(k) + dsqrt(mass(i)/mu)*vx*nmvec(j,i,k,imol)
! using internal coordinate for tunneling direction 
              pdi(it) = pdi(it)+vx*tunnd(j,i,it) 
            enddo
          enddo
        endif
      enddo  ! end it
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Case 2. tunneling coordinate is a combination of two internal !
!         coordinates for atom-transfer reaction                !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       elseif(tuntype.eq.2) then
         do it =1, nintc
            qq(it) = 5d-7*drr(it)
         enddo
 
         call iteramat(xx,xout,qq,nat,mass,lterm,amat)
!
! always use the tunnd(:,:,1) for the tunneling direction of combined internal coordinates case
!
         it = 1
         do i = 1, nat
           do j = 1, 3
             tunnd(j,i,it) = xout(j,i) - xx(j,i)
           enddo 
         enddo
         call normd(tunnd(:,:,1),nat)
         pdi(it)=0d0
         do i =1, nat
           do j =1, 3
! Using the normalized P and D vectors to compute P.D dot product
             vx = px(j,i)/mass(i)
! using internal coordinate for tunneling direction 
             pdi(it) = pdi(it)+vx*tunnd(j,i,it)
           enddo
         enddo
       endif ! end tuntype

       end subroutine pjctmoment
!--------------------------------------------------------------

      subroutine iteramat(xx,xout,qq,nat,mass,lterm,amatout)
!
! Given a set of initial Cartesian coordinates, xx, and a set of
! internal coordinate displacements, qq, from these coordinates, this
! routine iteratively calculates the Cartesian coordinates, xout,
! corresponding to the displaced coordinates and also returns the Wilson A matrix
! for the final geometry
!
! xx: the initial Cartesian coordinates (input)
! xout: the final Cartesian coordinates corresponding the the displaced initial ones (output)
! qq: the displacement in the internal coorinates from the initial geometry (input)
! nat: the number of atoms (input)
! mass: the atomic masses (input)
! lterm: a logical variable indicating failure has occurred (output)
! amatout: the Wilson A matrix at the final configuration (output)
!
      use param, only: mnat, autoang, pi
      use c_initial, only: ntor, itor, nimptor, imptor, nlinben,  &
                           ilinben,  nstr, istr, nbend, ibend 

      implicit none
      integer, parameter :: maxiter=50 ! the maximum number of iterations before we abandon hope of convergence
      integer :: i,j,k,l,it,nat,nintc,info,lwork
      integer :: iq,i1,i2,i3,i4,iter
      real*8, parameter :: diff_limit=1d-7  ! the largest acceptable internal coordinate difference
      real*8  :: bond,angle,dihedral,oopba, delq(3*mnat)
      real*8  :: qq0(3*mnat),qq(3*mnat),dqq(3*mnat)
      real*8  :: dxx(3*mnat),xout(3,mnat),maxdiff,maxdiff1
      real*8  :: pdi(3*nat),mass(mnat),xx(3,mnat)
      real*8  :: tunnd(3,mnat,3*mnat-6),qtunn(3*mnat)
      real*8  :: t(3,3), amatout(3*mnat,3*mnat)
      real*8, allocatable :: bm(:,:),amat(:,:),xt(:),yt(:),zt(:)
      real*8, allocatable :: bubt(:,:),u(:,:),work(:),bt(:,:)
      integer, allocatable :: indx(:)
      logical :: lterm

! N improper torsions are not used !RMP

      lterm =.false.
      nintc = 3*nat-6 
      if(.not.allocated(bm)) then 
        allocate(bm(nintc,3*nat),amat(3*nat,nintc),bt(3*nat,nintc))
        allocate(xt(nat),yt(nat),zt(nat),u(3*nat,3*nat))
        allocate(bubt(nintc,nintc),indx(nintc))
      endif
      u(:,:) = 0d0
      do i = 1, nat
         xt(i) = xx(1,i)
         yt(i) = xx(2,i)
         zt(i) = xx(3,i)
         u(3*i-2,3*i-2) = 1.0d0/mass(i)
         u(3*i-1,3*i-1) = u(3*i-2,3*i-2)
         u(3*i  ,3*i  ) = u(3*i-2,3*i-2)
      enddo
! evaluate internal coordinates of input geometry
! bond lengths
      iq = 0
      do i = 1, nstr
        iq = iq + 1
        i1 = istr(1,i)
        i2 = istr(2,i)
        qq0(iq)= bond(xx(:,i1),xx(:,i2)) 
!       write(6,*) 'bond length', qq0(iq)*autoang
      enddo
! bond angles
      do i = 1, nbend
         iq = iq + 1
         i1 = ibend(1,i)
         i2 = ibend(2,i)
         i3 = ibend(3,i)
         qq0(iq) = angle(xx(:,i1),xx(:,i2),xx(:,i3))
!        write(6,*) 'bond angle', qq0(iq)*180d0/pi
      enddo

!RMP
! linear bond angles
      do i = 1,nlinben
         iq = iq + 1
         i1 = ilinben(1,i)
         i2 = ilinben(2,i)
         i3 = ilinben(3,i)
         qq0(iq) = angle(xx(:,i1),xx(:,i2),xx(:,i3))
!        write(6,*) 'bond angle', qq0(iq)*180d0/pi
      enddo

!RMP
! improper torsional angles: out-of-plane bending angle
      do i = 1, nimptor 
         iq = iq + 1
         i1 = imptor(1,i)
         i2 = imptor(2,i)
         i3 = imptor(3,i)
         i4 = imptor(4,i)
         qq0(iq) = oopba(xx(:,i1),xx(:,i2),xx(:,i3),xx(:,i4))
!        write(6,*) 'improper torsional angle', qq(iq)*57.2958d0
      enddo

! torsion angles
      do i = 1, ntor
         iq = iq + 1
         i1 = itor(1,i)
         i2 = itor(2,i)
         i3 = itor(3,i)
         i4 = itor(4,i)
         qq0(iq) = dihedral(xx(:,i1),xx(:,i2),xx(:,i3),xx(:,i4))
!        write(6,*) 'dihedral angle', qq(iq)*57.2958d0
      enddo

      xout(:,:) = xx(:,:)
      qq (:) = qq(:) + qq0(:)

! Put angles within allowed bounds
      iq=nstr
      do i=1, nbend+nlinben
         iq=iq+1
         if(qq(iq).gt.pi)then
           qq(iq)=2*pi-qq(iq)
         elseif(qq(iq).lt.0d0)then
           qq(iq)=abs(qq(iq))
         endif
      enddo
      do i = 1, nimptor+ntor
         iq = iq + 1
         if(qq(iq).gt.pi)then
           qq(iq)=qq(iq)-2*pi
         elseif(qq(iq).lt.-pi)then
           qq(iq)=qq(iq)+2*pi
         endif
      enddo

! start iteration
!xxx      do iter = 1, 100
      do iter = 1, maxiter
        dxx(:) = 0d0
        do i = 1, nat
         xt(i) = xout(1,i)
         yt(i) = xout(2,i)
         zt(i) = xout(3,i)
        enddo
! calculate Wilson B Matrix 
! RMP it includes linear bends
      call bmat(3*mnat,nstr,nbend,ntor,nimptor,nlinben,istr,ibend,itor, &
                imptor,ilinben,xt,yt,zt,nat,nintc,bm,t)
! calculate Wilson A matrix
      bubt(:,:)=matmul(bm(:,:),matmul(u(:,:),transpose(bm(:,:))))
!   CONSTRUCT (BM*U*BMT)^-1  AND A matrix
       call dgetrf(nintc,nintc,bubt,nintc,indx,info)
       if (info.ne.0 ) then
          write(6,*) "dgetrf exit abnormally with info =", info
!         write(6,*) "Current geometry"
!         do i = 1,nat
!           write(6,*) xout(:,i)*autoang
!         enddo 
!         write(6,*) "Input geometry"
!         do i = 1,nat
!           write(6,*) xx(:,i)*autoang
!         enddo
          lterm = .true.
          return
!         stop
       endif
       lwork = -1
       allocate(work(1))
       call dgetri(nintc,bubt,nintc,indx,work,lwork,info)
       lwork=int(work(1))
       deallocate (work)
       allocate( work(lwork) )
       call dgetri(nintc,bubt,nintc,indx,work,lwork,info)
       deallocate(work)
       if (info.ne.0 ) then
         write(6,*) "dgetri exited abnormally in iteramat"
         lterm = .true.
         return
!        stop
       endif
       amat(:,:)=matmul( (matmul(u(:,:),transpose(bm(:,:)))),bubt(:,:))

       delq(:)=qq(:)-qq0(:)
! Put angles within allowed bounds
      iq=nstr
      do i=1, nbend+nlinben
         iq=iq+1
         if(delq(iq).gt.pi)then
           delq(iq)=2*pi-delq(iq)
         elseif(delq(iq).lt.0d0)then
           delq(iq)=abs(delq(iq))
         endif
      enddo
      do i = 1, nimptor+ntor
         iq = iq + 1
         if(delq(iq).gt.pi)then
           delq(iq)=delq(iq)-2*pi
         elseif(delq(iq).lt.-pi)then
           delq(iq)=delq(iq)+2*pi
         endif
      enddo

       do i = 1, 3*nat
         do j = 1, nintc
!xxx          dxx(i) = dxx(i) + amat(i,j)*(qq(j)-qq0(j))
          dxx(i) = dxx(i) + amat(i,j)*delq(j)
         enddo
       enddo
       do i = 1, nat
         do j = 1, 3
           k=(i-1)*3+j
           xout(j,i) = xout(j,i) + dxx(k)
         enddo
       enddo
! calculate internal coordinate at this geometry
! bond lengths
      iq = 0
      do i = 1, nstr
        iq = iq + 1
        i1 = istr(1,i)
        i2 = istr(2,i)
        qq0(iq)= bond(xout(:,i1),xout(:,i2)) 
        if(qq0(iq)*autoang.lt.0.5d0) then
           lterm = .true.
           return
        endif
!       write(6,*) 'bond length', qq0(iq)*autoang
      enddo
    
! bond angles
      do i = 1, nbend
         iq = iq + 1
         i1 = ibend(1,i)
         i2 = ibend(2,i)
         i3 = ibend(3,i)
         qq0(iq) = angle(xout(:,i1),xout(:,i2),xout(:,i3))
!        write(6,*) 'bond angle', qq0(iq)*180d0/pi
      enddo

!RMP
! linear bond angles
      do i = 1,nlinben
         iq = iq + 1
         i1 = ilinben(1,i)
         i2 = ilinben(2,i)
         i3 = ilinben(3,i)
         qq0(iq) = angle(xx(:,i1),xx(:,i2),xx(:,i3))
!        write(6,*) 'bond angle', qq0(iq)*180d0/pi
      enddo
!RMP
! improper torsional angles: out-of-bending angle
      do i = 1, nimptor 
         iq = iq + 1
         i1 = imptor(1,i)
         i2 = imptor(2,i)
         i3 = imptor(3,i)
         i4 = imptor(4,i)
         qq0(iq) = oopba(xx(:,i1),xx(:,i2),xx(:,i3),xx(:,i4))
!        write(6,*) 'improper torsional angle', qq(iq)*57.2958d0
      enddo
!
! torsion angles
      do i = 1,ntor
        iq = iq + 1
        i1 = itor(1,i)
        i2 = itor(2,i)
        i3 = itor(3,i)
        i4 = itor(4,i)
        qq0(iq) = dihedral(xout(:,i1),xout(:,i2),xout(:,i3),xout(:,i4))
!       write(6,*) 'dihedral angle', qq(iq)*180d0/pi
      enddo

      dqq(:) = qq(:) - qq0(:)
! Put angles within allowed bounds
      iq=nstr
      do i=1, nbend+nlinben
         iq=iq+1
         if(dqq(iq).gt.pi)then
           dqq(iq)=2*pi-dqq(iq)
         elseif(dqq(iq).lt.0d0)then
           dqq(iq)=abs(dqq(iq))
         endif
      enddo
      do i = 1, nimptor+ntor
         iq = iq + 1
         if(dqq(iq).gt.pi)then
           dqq(iq)=dqq(iq)-2*pi
         elseif(dqq(iq).lt.-pi)then
           dqq(iq)=dqq(iq)+2*pi
         endif
      enddo

      maxdiff = 0d0
      do i = 1, 3*nat-6
!xxx        if(maxdiff.lt.dqq(i)) maxdiff = dqq(i)
        if(maxdiff.lt.abs(dqq(i))) maxdiff = abs(dqq(i))
      enddo

!xxx      if(maxdiff .lt. 1d-7 ) exit
      if(maxdiff .lt. diff_limit ) then
        lterm=.false.
        exit
      endif

       maxdiff1 = maxdiff

        if(iter.eq.maxiter)then  ! added to prevent termination with unconverged results
!xxx          write(6,*)'failure to converge in iteramat' 
!xxx          write(6,*)'maxdiff=',maxdiff
!xxx          qq(1)=sqrt(-1d0)  ! trying to cause a traceback  ! debugging 
!xxx          stop
          lterm=.true.  ! convergence has failed, the calling routine will deal with it
        endif

      enddo ! end iter

      do i = 1, nintc
        do j = 1, 3*nat
          amatout(j,i) = amat(j,i)
        enddo
      enddo

      end subroutine iteramat
