      subroutine getpem(xx,indmm,symb,cell,nclu,pema,pemd,gpema, &
          gpemd,dvec,mm,icall,nsurf,charge,multi)
!xxx     subroutine getpem(xx,indmm,symb,cell,nclu,pema,pemd,gpema, &
!xxx                       gpemd,dvec,mm,icall,nsurf)

!     This subroutine handles all potential calls.
!     The adiabatic matrix PEMA, its gradients GPEMA,
!     the diabatic matrix PEMD, its gradients GPEMD,
!     and the nonadiabatic coupling vector DVEC are returned
!     PEMA is a diagonal matrix, so it's a 1-D vector, with the
!     index running from 1-NSURFT.
!     PEMD is 2-D, with indices running from 1-NSURFT.
!     For GPEMA and GPEMD, the first index represents x,y,z, the
!     second represents the atom index, the third (and fourth for
!     PEMD) label the electronic states and run from 1-NSURFT.
!     NCLU is the number of atoms in this call, this number may not be
!     the same as NAT, but must be less than NAT

        use param, only: mnat, mnsurf, mntraj
        use c_sys, only: periodic, qcpack, repflag, methflag, nsurft, potflag
! HOCO variable
        use c_common, only: numstep
        implicit none

        integer :: i,j,k,l,i1,i2,j1,j2,icall
        integer :: charge,multi
        integer, save :: ifirst=0  ! On the first call sometimes special things are done
!     input
        integer :: nclu
        double precision :: xx(3,mnat),mm(mnat),cell(6)
        character*2 :: symb(mnat)
        integer :: indmm(mnat),nsurf
        double precision :: u2b2(2,2),gu2b2(3,mnat,2,2),dvec2b2(3,mnat) 
        double precision :: v2b2(2)

!     output
        double precision :: pemd(mnsurf,mnsurf),gpemd(3,mnat,mnsurf,mnsurf), &
          pema(mnsurf),gpema(3,mnat,mnsurf),dvec(3,mnat,mnsurf,mnsurf)

! local (for 3V-2 interface)
! RMP100813 
        double precision :: r(3),du11(3),du12(3),du22(3),cc(2,2),u11,u12,u22
!-----------Cleaning the code---------------------------------------------
!      double precision r(3),du11(3),du12(3),du22(3),v1,v2,u11,u12,u22,   &
!        cs1,sn1,cc(2,2),tmp
!-----------Cleaning the code---------------------------------------------

! local (for HO-MM-1 and HE-MM-1 interfaces)
        double precision :: dx(nclu),dy(nclu),dz(nclu),x(nclu),y(nclu),z(nclu),v
! local (for OH3 potential) Interface: 4-XS
        double precision :: dxo(4),dyo(4),dzo(4),xo(4),yo(4),zo(4)
        integer :: indatom(nclu)
        character*2 :: symbm(nclu)
        double precision :: com(3),mtot

! Local NH3 variables
        double precision :: v1,v2                                           ! RMP100813 
        double precision :: Xcart(12)
        double precision :: gcartU11(12), gcartU22(12), gcartU12(12), &
          gcartV1(12), gcartV2(12), dvecnh3(12), gcartV12(12)

! Local HBr variables

        integer :: nstepflag
        double precision :: UI(12),VI(12),gUI(3,2,12),gVI(3,2,12)
        double precision :: UIJ(12,12),gUIJ(3,2,12,12)
        double precision :: dvechbr(3,2,12,12)
        double precision :: VIdum(12),cchbr(12,12) 
        double precision :: trippione,trippizero,singpione,tripsigmaone

! Local CH2BrCl variables

        double precision :: UI2(24),VI2(24),gUI2(3,5,24),gVI2(3,5,24)
        double precision :: UIJ2(24,24),gUIJ2(3,5,24,24)
        double precision :: dvecch2brcl(3,5,24,24)
        double precision :: xcart7(15)

! Local HN2 variable 
        double precision :: grad(9),xcart8(9)
! Local variables for the HX2 (to study the nonadiabatic tunneling)
        double precision :: xnat(9)
        double precision :: uijnat(2,2),vinat(2)
        double precision :: guijnat(3,3,2,2),gvinat(3,3,2)
        double precision :: dvecnat(3,3,2,2),cchx2(2,2)
! Local MOPAC variable 
        double precision :: dxyz(nclu*3)
! local Phenol variable
        double precision :: uu(3,3),vv(3),guu(3,13,3,3),ccph(3,3)
        double precision :: gvv(3,13,3),dvec10(3,13,3,3),xph(3,13)

! RMP100813 
! This array solves the problem of the different dimensinality of DVEC.
        double precision :: ccc(mnsurf,mnsurf)

        do i=1,mnsurf
          do j=1,mnsurf
            ccc(i,j)=0.d0
          enddo
        enddo
! RMP100813 

! For the simulation in a box, if the atoms move out of the box, move
! them back
        if (.not.periodic) call hardwall(xx)


! ######################################################################
!     POTLIB HO-MM-1 interface
        if (potflag.eq.0) then
! ######################################################################
!     Single surface

! Transfer to local x,y,z
          do i=1,nclu
            x(i) = xx(1,i)
            y(i) = xx(2,i)
            z(i) = xx(3,i)
!      write(6,*) x(i),y(i),z(i)
          enddo

          if (periodic) then
            call pot(x,y,z,v,dx,dy,dz,cell,nclu,nclu)
          else
            call pot(x,y,z,v,dx,dy,dz,nclu,nclu)
          endif
          pema(1) = v
          pemd(1,1) = v
          do i=1,nclu
            gpema(1,i,1)=dx(i)
            gpema(2,i,1)=dy(i)
            gpema(3,i,1)=dz(i)
            gpemd(1,i,1,1)=dx(i)
            gpemd(2,i,1,1)=dy(i)
            gpemd(3,i,1,1)=dz(i)
            dvec(1,i,1,1)=0.d0
            dvec(2,i,1,1)=0.d0
            dvec(3,i,1,1)=0.d0
          enddo

! ######################################################################
!     POTLIB HE-MM-1 interface
        elseif (potflag.eq.2) then
! ######################################################################
!     Single surface

! Transfer to local x,y,z
          do i=1,nclu
            x(i) = xx(1,i)
            y(i) = xx(2,i)
            z(i) = xx(3,i)
            symbm(i) = symb(i)
          enddo

          if (periodic) then
            call pot(symbm,x,y,z,v,dx,dy,dz,cell,nclu,nclu)
          else
            call pot(symbm,x,y,z,v,dx,dy,dz,nclu,nclu)
          endif
          pema(1) = v
          pemd(1,1) = v
          do i=1,nclu
            gpema(1,i,1)=dx(i)
            gpema(2,i,1)=dy(i)
            gpema(3,i,1)=dz(i)
            gpemd(1,i,1,1)=dx(i)
            gpemd(2,i,1,1)=dy(i)
            gpemd(3,i,1,1)=dz(i)
            dvec(1,i,1,1)=0.d0
            dvec(2,i,1,1)=0.d0
            dvec(3,i,1,1)=0.d0
          enddo

! ######################################################################
!     POTLIB 3-2V interface
        elseif (potflag.eq.1) then
! ######################################################################
!     Triatomic, two-surface interface

!     r(1) = A-B
!     r(2) = B-C
!     r(3) = A-C
!     im = 1 = AB+C
!     im = 2 = BC+A
!     im = 3 = CA+B
!     transform to internals
          r(1)=0.d0
          r(2)=0.d0
          r(3)=0.d0
          do i=1,3
            r(1) = r(1) + (xx(i,1)-xx(i,2))**2
            r(2) = r(2) + (xx(i,2)-xx(i,3))**2
            r(3) = r(3) + (xx(i,1)-xx(i,3))**2
          enddo
          do i=1,3
            r(i)=dsqrt(r(i))
          enddo
          call pot(r,u11,du11,1,1)
          call pot(r,u12,du12,1,2)
          call pot(r,u22,du22,1,3)
!      write(6,*) u11,u12,u22,(r(i)*autoang,i=1,3)

!     save diabatic info
          pemd(1,1) = u11
          pemd(1,2) = u12
          pemd(2,1) = u12
          pemd(2,2) = u22
          do i=1,3
!       convert from internals to cartesian
            gpemd(i,1,1,1) = du11(1)/r(1)*(xx(i,1)-xx(i,2)) &
              +du11(3)/r(3)*(xx(i,1)-xx(i,3))
            gpemd(i,2,1,1) = du11(1)/r(1)*(xx(i,2)-xx(i,1)) &
              +du11(2)/r(2)*(xx(i,2)-xx(i,3))
            gpemd(i,3,1,1) = du11(2)/r(2)*(xx(i,3)-xx(i,2)) &
              +du11(3)/r(3)*(xx(i,3)-xx(i,1))
            gpemd(i,1,1,2) = du12(1)/r(1)*(xx(i,1)-xx(i,2)) &
              +du12(3)/r(3)*(xx(i,1)-xx(i,3))
            gpemd(i,2,1,2) = du12(1)/r(1)*(xx(i,2)-xx(i,1)) &
              +du12(2)/r(2)*(xx(i,2)-xx(i,3))
            gpemd(i,3,1,2) = du12(2)/r(2)*(xx(i,3)-xx(i,2)) &
              +du12(3)/r(3)*(xx(i,3)-xx(i,1))
            gpemd(i,1,2,1) = gpemd(i,1,1,2)
            gpemd(i,2,2,1) = gpemd(i,2,1,2)
            gpemd(i,3,2,1) = gpemd(i,3,1,2)
            gpemd(i,1,2,2) = du22(1)/r(1)*(xx(i,1)-xx(i,2)) &
              +du22(3)/r(3)*(xx(i,1)-xx(i,3))
            gpemd(i,2,2,2) = du22(1)/r(1)*(xx(i,2)-xx(i,1)) &
              +du22(2)/r(2)*(xx(i,2)-xx(i,3))
            gpemd(i,3,2,2) = du22(2)/r(2)*(xx(i,3)-xx(i,2)) &
              +du22(3)/r(3)*(xx(i,3)-xx(i,1))
          enddo
!RMP100813
!-----------Cleaning the code---------------------------------------------
!     compute adiabatic info
!     diagonalize the 2x2
!      call dlaev2(u11,u12,u22,v2,v1,cs1,sn1)
!
!     phase convention (this part depends on how DLAEV2 works)
!      if (v2.ge.v1) then
!        cc(1,1) = -sn1
!        cc(2,1) = cs1
!        cc(1,2) = cs1
!        cc(2,2) = sn1
!      else
!        tmp = v1
!        v1 = v2
!        v2 = tmp
!        cc(1,1) = cs1
!        cc(2,1) = sn1
!        cc(1,2) = -sn1
!        cc(2,2) = cs1
!      endif
!
!      pema(1) = v1
!      pema(2) = v2
!
!-----------Cleaning the code---------------------------------------------
!     compute adiabatic info
!     diagonalize the 2x2 and compute the eigenvectors
          u2b2(1,1) = pemd(1,1)
          u2b2(1,2) = pemd(1,2)
          u2b2(2,1) = pemd(2,1)
          u2b2(2,2) = pemd(2,2)
          call diag2(u2b2,v2b2,cc)
          pema(1) = v2b2(1)
          pema(2) = v2b2(2)
!RMP100813

!     gradients of adiabatic surfaces v1 and v2
!     this bit of code can handle an arbitrary number of surfaces
!     if cc is defined properly
          do j1 = 1, 3
            do j2 = 1, nclu
              do i = 1, nsurft
                gpema(j1,j2,i)=(cc(1,i)**2)*gpemd(j1,j2,1,1)
                do k = 2, nsurft
                  gpema(j1,j2,i)=gpema(j1,j2,i)+(cc(k,i)**2)*gpemd(j1,j2,k,k)
                  do l = 1, k-1
                    gpema(j1,j2,i)=gpema(j1,j2,i) &
                      +2.d0*cc(k,i)*cc(l,i)*gpemd(j1,j2,k,l)
                  enddo
                enddo
              enddo
            enddo
          enddo

          IF(METHFLAG.EQ.1.OR.METHFLAG.EQ.5) THEN
            if(repflag.eq.1) then
!           diabatic
!RMP100813
              call getdvecd(nclu,nsurf,pemd,gpemd,dvec)
!-----------Cleaning the code---------------------------------------------
!            do k=1,nsurft
!            if(k.ne.nsurf) then
!            u2b2(1,1) = pemd(k,k)
!            u2b2(1,2) = pemd(k,nsurf)
!            u2b2(2,1) = pemd(nsurf,k)
!            u2b2(2,2) = pemd(nsurf,nsurf)
!            do i=1,3
!            do j=1,NCLU
!              gu2b2(i,j,1,1) = gpemd(i,j,k,k)
!              gu2b2(i,j,1,2) = gpemd(i,j,k,nsurf)
!              gu2b2(i,j,2,1) = gpemd(i,j,nsurf,k)
!              gu2b2(i,j,2,2) = gpemd(i,j,nsurf,nsurf)
!            enddo
!            enddo
!            call getdvec2(NCLU,u2b2,gu2b2,dvec2b2)
!            do i=1,3
!             do j=1,NCLU
!              dvec(i,j,k,nsurf) = dvec2b2(i,j)
!              dvec(i,j,nsurf,k) = -dvec2b2(i,j)
!             enddo
!            enddo
!            endif
!            enddo
!-----------Cleaning the code---------------------------------------------
            elseif(repflag.eq.0) then
! compute d                                                      RMP100813
! transfer the cc matrix to ccc (the array in getdvect must have the same
! size) 
              call getdvec(nclu,pemd,gpemd,dvec)
! another possibility
!       do i=1,2
!        do j=1,2
!         ccc(i,j)=cc(i,j)
!        enddo
!       enddo
!       call getdvect(nclu,gpemd,pema,dvec,ccc)
!-----------Cleaning the code---------------------------------------------
!      do i1 = 1,3
!      do i2 = 1,nclu
!        do i = 2,nsurft
!          do j = 1, i-1
!            dvec(i1,i2,i,j) = 0.d0
!            do k = 1, nsurft
!            do l = 1, nsurft
!              dvec(i1,i2,i,j) = dvec(i1,i2,i,j)  &
!                              + cc(k,i)*cc(l,j)*gpemd(i1,i2,k,l)
!            enddo
!            enddo
!            if ((pema(j) - pema(i)) .ne. 0.0d0) then
!              dvec(i1,i2,i,j) = dvec(i1,i2,i,j)/(pema(j)-pema(i))
!            else
!              dvec(i1,i2,i,j) = 0.0d0
!            endif
!            dvec(i1,i2,j,i) = -dvec(i1,i2,i,j)
!          enddo
!        enddo
!      enddo
!      enddo
!-----------Cleaning the code---------------------------------------------
            endif
          ENDIF

! ######################################################################
!     POTLIB 4-XS interface
        elseif (potflag.eq.4) then
! ######################################################################
!     Four atoms, one-surface interface
!   Common name:
!   Number of derivatives: 1
!   Number of bodies: 4
!   Number of electronic surfaces: 1
!   Interface: 4-XS
!   Reference: G. C. Schatz and H. Elgersma
!              Chem. Phys. Lett. 73, 21 (1980).
!     Single surface
!
!     In the calculation of potential, atom arrangement is O H H H
          do i=1,nclu
            xo(i) = xx(1,i)
            yo(i) = xx(2,i)
            zo(i) = xx(3,i)
          enddo
!     OH2 (H3+O is unlikely a kind of reactant arrangment)
          if (nclu.eq.3) then
            xo(4) = 500.0d0
            yo(4) = 500.0d0
            zo(4) = 500.0d0
          elseif (nclu.eq.2) then
!        OH
!xxx         if (symb(1).eq.'O ') then
            if ((symb(1).eq.'O ').or.(symb(1).eq.'o ')) then
              xo(3) = 500.0d0
              yo(3) = 500.0d0
              zo(3) = 500.0d0
              xo(4) = 600.0d0
              yo(4) = 600.0d0
              zo(4) = 600.0d0
!          H2
            else
              xo(1) = 500.0d0
              yo(1) = 500.0d0
              zo(1) = 500.0d0
              xo(2) = 600.0d0
              yo(2) = 600.0d0
              zo(2) = 600.0d0
              xo(3) = xx(1,1)
              yo(3) = xx(2,1)
              zo(3) = xx(3,1)
              xo(4) = xx(1,2)
              yo(4) = xx(2,2)
              zo(4) = xx(3,2)
            endif
          else
            continue
          endif
          call pot(xo,yo,zo,v,dxo,dyo,dzo)
          pema(1) = v
          pemd(1,1) = v
          do i=1,nclu
            dvec(1,i,1,1)=0.d0
            dvec(2,i,1,1)=0.d0
            dvec(3,i,1,1)=0.d0
          enddo
!xxx      if (nclu.eq.2 .and. symb(1).eq.'H ') then
          if ((nclu.eq.2 .and. symb(1).eq.'H ').or. &
            (nclu.eq.2 .and. symb(1).eq.'h '))   then
            gpema(1,1,1)=dxo(3)
            gpema(2,1,1)=dyo(3)
            gpema(3,1,1)=dzo(3)
            gpemd(1,1,1,1)=dxo(3)
            gpemd(2,1,1,1)=dyo(3)
            gpemd(3,1,1,1)=dzo(3)
            gpema(1,2,1)=dxo(4)
            gpema(2,2,1)=dyo(4)
            gpema(3,2,1)=dzo(4)
            gpemd(1,2,1,1)=dxo(4)
            gpemd(2,2,1,1)=dyo(4)
            gpemd(3,2,1,1)=dzo(4)
          else
            do i=1,nclu
              gpema(1,i,1)=dxo(i)
              gpema(2,i,1)=dyo(i)
              gpema(3,i,1)=dzo(i)
              gpemd(1,i,1,1)=dxo(i)
              gpemd(2,i,1,1)=dyo(i)
              gpemd(3,i,1,1)=dzo(i)
            enddo
          endif

! ######################################################################
!     POTLIB NH3 Potential interface
        elseif (potflag.eq.3) then
! ######################################################################

! GET NH3 potential
          k=0
          do i=1,NCLU
            do j=1,3
              k=k+1
              Xcart(k)=xx(j,i)
            end do
          end do
!       call pot(Xcart,U11,U22,U12,V1,V2,
!    1           gcartU11,gcartU22,gcartU12,
!    2           gcartV1,gcartV2)
! use surfgen lib
          call pot(Xcart,U11,U22,U12,V1,V2,gcartU11,gcartU22,gcartU12, &
            gcartV1,gcartV2,gcartV12)

          pema(1)=V1
          pema(2)=V2
          pemd(1,1)=U11
          pemd(2,2)=U22
          pemd(1,2)=U12
          pemd(2,1)=U12
          k=0
          dvec = 0d0
          do i=1,NCLU
            do j=1,3
              k=k+1
              gpemd(j,i,1,1)=gcartU11(k)
              gpemd(j,i,2,2)=gcartU22(k)
              gpemd(j,i,1,2)=gcartU12(k)
              gpemd(j,i,2,1)=gcartU12(k)
              gpema(j,i,1)=gcartV1(k)
              gpema(j,i,2)=gcartV2(k)
! use surfgen
              dvec(j,i,1,2) = gcartV12(k)
              dvec(j,i,2,1) = -gcartV12(k)
            end do
          end do
!     IF(METHFLAG.EQ.1.OR.METHFLAG.EQ.5) THEN
          if(repflag.eq.1) then
!           diabatic
!RMP100813
            call getdvecd(nclu,nsurf,pemd,gpemd,dvec)
!-----------Cleaning the code---------------------------------------------
!            do k=1,nsurft
!            if(k.ne.nsurf) then
!            u2b2(1,1) = pemd(k,k)
!            u2b2(1,2) = pemd(k,nsurf)
!            u2b2(2,1) = pemd(nsurf,k)
!            u2b2(2,2) = pemd(nsurf,nsurf)
!            do i=1,3
!            do j=1,NCLU
!              gu2b2(i,j,1,1) = gpemd(i,j,k,k)
!              gu2b2(i,j,1,2) = gpemd(i,j,k,nsurf)
!              gu2b2(i,j,2,1) = gpemd(i,j,nsurf,k)
!              gu2b2(i,j,2,2) = gpemd(i,j,nsurf,nsurf)
!            enddo
!            enddo
!            call getdvec2(NCLU,u2b2,gu2b2,dvec2b2)
!            do i=1,3
!             do j=1,NCLU
!              dvec(i,j,k,nsurf) = dvec2b2(i,j)
!              dvec(i,j,nsurf,k) = -dvec2b2(i,j)
!             enddo
!            enddo
!            endif
!            enddo
!-----------Cleaning the code---------------------------------------------
          elseif(repflag.eq.0) then
!RMP100813
! The getdvec subroutine was updated 
            call getdvec(nclu,pemd,gpemd,dvec)
          endif
!     ENDIF
! ######################################################################
!     POTLIB HE-MM-1 interface
        elseif (potflag.eq.5) then
! ######################################################################
!     Single surface

! Transfer to local x,y,z
          do i=1,nclu
            x(i) = xx(1,i)
            y(i) = xx(2,i)
            z(i) = xx(3,i)
            indatom(i) = indmm(i)
          enddo

          if (periodic) then
            call pot(indatom,x,y,z,v,dx,dy,dz,cell,nclu,nclu)
          else
            call pot(indatom,x,y,z,v,dx,dy,dz,nclu,nclu)
          endif
          pema(1) = v
          pemd(1,1) = v
          do i=1,nclu
            gpema(1,i,1)=dx(i)
            gpema(2,i,1)=dy(i)
            gpema(3,i,1)=dz(i)
            gpemd(1,i,1,1)=dx(i)
            gpemd(2,i,1,1)=dy(i)
            gpemd(3,i,1,1)=dz(i)
            dvec(1,i,1,1)=0.d0
            dvec(2,i,1,1)=0.d0
            dvec(3,i,1,1)=0.d0
          enddo

! ######################################################################
!      POTLIB HBr interface 
        elseif (potflag.eq.6) then
          if(ifirst.eq.0)then
            ifirst=1
            write(6,*)'HBr potential reference:'
            write(6,'(a)')'R. Valero, D.G. Truhlar, and A.W. Jasper, J. Phys. Chem. A, 112, 5756 (2008)'
            if(repflag.eq.1) then
              stop 'HBr potential is not debugged for use with diabatic representation'
            endif
          endif

          k=0
          do i=1,NCLU
            do j=1,3
              k=k+1
              Xcart(k)=xx(j,i)
            end do
          end do
          call pot(Xcart,UI,UIJ,VI,gUI,gUIJ,gVI,dvechbr)
          do i=1,12  
            pema(i) = VI(i)
          enddo
          do i=1,12
            do j=1,12 
              pemd(i,j)=UIJ(i,j)
            enddo
          enddo
          do i=1,12
            pemd(i,i)=UI(i)
          enddo
          do i=1,NCLU
            do j=1,3
              do k=1,12
                do l=1,12
                  dvec(j,i,k,l) = dvechbr(j,i,k,l)
                  gpemd(j,i,k,l)=gUIJ(j,i,k,l)
                enddo
              enddo
              do k=1,12
                gpemd(j,i,k,k)=gUI(j,i,k)
              enddo
              do k=1,12
                gpema(j,i,k)=gVI(j,i,k)
              enddo
            end do
          end do
          IF(METHFLAG.EQ.1.OR.METHFLAG.EQ.5) THEN
            if(repflag.eq.1) then
!           diabatic
!RMP100813
              call getdvecd(nclu,nsurf,pemd,gpemd,dvec)
!-----------Cleaning the code---------------------------------------------
!            do k=1,nsurft
!            if(k.ne.nsurf) then
!            u2b2(1,1) = pemd(k,k)
!            u2b2(1,2) = pemd(k,nsurf)
!            u2b2(2,1) = pemd(nsurf,k)
!            u2b2(2,2) = pemd(nsurf,nsurf)
!            do i=1,3
!            do j=1,NCLU
!              gu2b2(i,j,1,1) = gpemd(i,j,k,k)
!              gu2b2(i,j,1,2) = gpemd(i,j,k,nsurf)
!              gu2b2(i,j,2,1) = gpemd(i,j,nsurf,k)
!              gu2b2(i,j,2,2) = gpemd(i,j,nsurf,nsurf)
!            enddo
!            enddo
!            call getdvec2(NCLU,u2b2,gu2b2,dvec2b2)
!            do i=1,3
!             do j=1,NCLU
!              dvec(i,j,k,nsurf) = dvec2b2(i,j)
!              dvec(i,j,nsurf,k) = -dvec2b2(i,j)
!             enddo
!            enddo
!            endif
!            enddo
!-----------Cleaning the code---------------------------------------------
            endif
          ENDIF

! ######################################################################
!      POTLIB BrCH2Cl interface 
        elseif (potflag.eq.7) then
! ######################################################################

          k=0
          do i=1,NCLU
            do j=1,3
              k=k+1
              xcart7(k)=xx(j,i)
            end do
          end do

          call pot(Xcart7,UI2,UIJ2,VI2,gUI2,gUIJ2,gVI2,dvecch2brcl,icall)

          do i=1,nsurft
            pema(i) = VI2(i)
          enddo
          do i=1,nsurft
            do j=1,nsurft
              pemd(i,j)=UIJ2(i,j)
            enddo
          enddo
          do i=1,nsurft
            pemd(i,i)=UI2(i)
          enddo
          do i=1,NCLU
            do j=1,3
              do k=1,nsurft
                do l=1,nsurft
                  dvec(j,i,k,l) = dvecch2brcl(j,i,k,l)
                  gpemd(j,i,k,l)=gUIJ2(j,i,k,l)
                enddo
              enddo
              do k=1,nsurft
                gpemd(j,i,k,k)=gUI2(j,i,k)
              enddo
              do k=1,nsurft
                gpema(j,i,k)=gVI2(j,i,k)
              enddo
            end do
          end do
! RVM  calculate "nonadiabatic couplings" in diabatic rep and pass on to dvec
          IF(METHFLAG.EQ.1.OR.METHFLAG.EQ.5) THEN
            if(repflag.eq.1) then
!           diabatic
!RMP100813
              call getdvecd(nclu,nsurf,pemd,gpemd,dvec)
!-----------Cleaning the code---------------------------------------------
!            do k=1,nsurft
!            if(k.ne.nsurf) then
!            u2b2(1,1) = pemd(k,k)
!            u2b2(1,2) = pemd(k,nsurf)
!            u2b2(2,1) = pemd(nsurf,k)
!            u2b2(2,2) = pemd(nsurf,nsurf)
!            do i=1,3
!            do j=1,NCLU
!              gu2b2(i,j,1,1) = gpemd(i,j,k,k)
!              gu2b2(i,j,1,2) = gpemd(i,j,k,nsurf)
!              gu2b2(i,j,2,1) = gpemd(i,j,nsurf,k)
!              gu2b2(i,j,2,2) = gpemd(i,j,nsurf,nsurf)
!            enddo
!            enddo
!            call getdvec2(NCLU,u2b2,gu2b2,dvec2b2)
!            do i=1,3
!             do j=1,NCLU
!              dvec(i,j,k,nsurf) = dvec2b2(i,j)
!              dvec(i,j,nsurf,k) = -dvec2b2(i,j)
!             enddo
!            enddo
!            endif
!            enddo
!-----------Cleaning the code---------------------------------------------
            endif
          ENDIF
! ###################################################################RMP
!     HN2 interface 
!     Reference: H. Koizumi, G. C. Schatz and S. P. Walch
!            J. Chem. Phys. 95 4130 (1991)
        elseif (potflag.eq.8) then
! ######################################################################
          k=0
          do i=1,nclu
            do j=1,3
              k=k+1
              xcart8(k)=xx(j,i)
            end do
          end do
          call pot(v,xcart8,grad)
          pema(1) = v
          pemd(1,1) = v
          do i=1,nclu
            gpema(1,i,1)=grad(3*i-2)
            gpema(2,i,1)=grad(3*i-1)
            gpema(3,i,1)=grad(3*i)
            gpemd(1,i,1,1)=grad(3*i-2)
            gpemd(2,i,1,1)=grad(3*i-1)
            gpemd(3,i,1,1)=grad(3*i)
            dvec(1,i,1,1)=0.d0
            dvec(2,i,1,1)=0.d0
            dvec(3,i,1,1)=0.d0
          enddo
! ###################################################################RMP
!     HX2 interface 
!     Reference: 
!     PES for study the nonadiabatic tunneling
!     Based on the H-N dissociation energy profile
! ######################################################################
        elseif (potflag.eq.9) then
          k=0
          do i=1,nclu
            do j=1,3
              k=k+1
              xnat(k)=xx(j,i)
            enddo
          enddo
          call pot(Xnat,UIJnat,VInat,gUIJnat,gVInat,dvecnat,cchx2)
!
! Energies
          do i=1,nsurft
            pema(i)=vinat(i)
            do j=1,nsurft
              pemd(i,j)=uijnat(i,j)
            enddo
          enddo
!c Gradients and NACT vectors
          do i=1,3
            do j=1,nclu
              do k=1,nsurft
                gpema(i,j,k)=gvinat(i,j,k)
!           write(6,*) 'grad adiab',gpema(i,j,1)
                do l=1,nsurft
                  gpemd(i,j,k,l)=guijnat(i,j,k,l)
!           write(6,*) 'grad ddiab',gpemd(i,j,1,1)
                  dvec(i,j,k,l)=dvecnat(i,j,k,l)
                enddo
              enddo
            enddo
          enddo
!##################################################################
!     Phenol potential 
! #################################################################
        elseif(potflag .eq. 10) then
          do i = 1, nclu
            do j = 1, 3
              xph(j,i) = xx(j,i)
            enddo
          enddo
          call  pot(1,xph,uu,guu,vv,gvv,dvec10,ccph,repflag)
! Energies
          do i=1,nsurft
            pema(i)=vv(i)
            do j=1,nsurft
              pemd(i,j)=uu(i,j)
            enddo
          enddo
! Gradients and nonadiabatic coupling vectors
          do i=1,3
            do j=1,nclu
              do k=1,nsurft
                gpema(i,j,k)=gvv(i,j,k)
                do l=1,nsurft
                  gpemd(i,j,k,l) = guu(i,j,k,l)
                  dvec(i,j,k,l)  = dvec10(i,j,k,l)
                enddo
              enddo
            enddo
          enddo
! #####################################################################
! N4 potential
! ######################################################################
!     Tetra-atomic, single-surface interface, designed for diatom-
!     diatom collisions, (such as N2+N2, N2+O2, and O2+O2).
!     Jason D. Bender 2014/02
        elseif (potflag.eq.11) then
! ######################################################################
          if (nclu .ne. 4) then
            write(6,*) 'ERROR: With potflag==11, getpem must be called '//  &
              'with nclu==4 (i.e., for 4 atoms).'
            stop
          endif
          do i=1,nclu
            x(i)= xx(1,i)
            y(i)= xx(2,i)
            z(i)= xx(3,i)
          enddo
          call pot(x,y,z,v,dx,dy,dz)
          pema(1) = v
          pemd(1,1) = v
          do i=1,nclu
            gpema(1,i,nsurf)= dx(i)
            gpema(2,i,nsurf)= dy(i)
            gpema(3,i,nsurf)= dz(i)
            gpemd(1,i,nsurf,nsurf)= dx(i)
            gpemd(2,i,nsurf,nsurf)= dy(i)
            gpemd(3,i,nsurf,nsurf)= dz(i)
            dvec( 1,i,nsurf,nsurf)= 0d0
            dvec( 2,i,nsurf,nsurf)= 0d0
            dvec( 3,i,nsurf,nsurf)= 0d0
          enddo
!
!#######################################################################
! HOCO potential
!#######################################################################
        elseif( potflag.eq.12) then
! GET HOCO potential
!     OH2 (H3+O is unlikely a kind of reactant arrangment)
          if (nclu.eq.3) then
            Xcart(1) = 0.0d0
            Xcart(2) = 0.0d0
            Xcart(3) = 10.0d0
            Xcart(4) = xx(1,1)
            Xcart(5) = xx(2,1)
            Xcart(6) = xx(3,1)
            Xcart(7) = xx(1,2)
            Xcart(8) = xx(2,2)
            Xcart(9) = xx(3,2) 
            Xcart(10) = xx(1,3)
            Xcart(11) = xx(2,3)
            Xcart(12) = xx(3,3)
          elseif (nclu.eq.2) then
!        HO
            if (symb(1).eq.'h ') then
              Xcart(1) = xx(1,1)
              Xcart(2) = xx(2,1)
              Xcart(3) = xx(3,1)
              Xcart(4) = xx(1,2)
              Xcart(5) = xx(2,2)
              Xcart(6) = xx(3,2)
              Xcart(7) = 0.d0
              Xcart(8) = 0.d0
              Xcart(9) = 10.d0
              Xcart(10) = 0.d0
              Xcart(11) = 0.d0
              Xcart(12) = 12.1190d0
!        CO
            else
              Xcart(1) = 0.d0
              Xcart(2) = 0.d0
              Xcart(3) = 10.d0
              Xcart(4) = 0.d0
              Xcart(5) = 0.d0
              Xcart(6) = 11.8365d0
              Xcart(7) = xx(1,1)
              Xcart(8) = xx(2,1)
              Xcart(9) = xx(3,1) 
              Xcart(10) = xx(1,2)
              Xcart(11) = xx(2,2)
              Xcart(12) = xx(3,2)
            endif
          else
            k=0
            do i=1,nclu
              do j=1,3
                k=k+1
                Xcart(k)=xx(j,i)
              end do
            end do
          endif

!       if(numstep.eq.0.or.numstep.eq.1) call initializecollins
          call pot(Xcart,V1,gcartV1)

          pema(1)=V1
          pemd(1,1)=V1
          do i=1,nclu
            dvec(1,i,1,1)=0.d0
            dvec(2,i,1,1)=0.d0
            dvec(3,i,1,1)=0.d0
          enddo
!
          if (nclu.eq.2 .and. symb(1).eq.'h ') then
            gpema(1,1,1)=gcartV1(1)
            gpema(2,1,1)=gcartV1(2)
            gpema(3,1,1)=gcartV1(3)
            gpemd(1,1,1,1)=gcartV1(1)
            gpemd(2,1,1,1)=gcartV1(2)
            gpemd(3,1,1,1)=gcartV1(3)
            gpema(1,2,1)=gcartV1(4)
            gpema(2,2,1)=gcartV1(5)
            gpema(3,2,1)=gcartV1(6)
            gpemd(1,2,1,1)=gcartV1(4)
            gpemd(2,2,1,1)=gcartV1(5)
            gpemd(3,2,1,1)=gcartV1(6)
          elseif (nclu.eq.2 .and. symb(1).eq.'c ') then
            gpema(1,1,1)=gcartV1(7)
            gpema(2,1,1)=gcartV1(8)
            gpema(3,1,1)=gcartV1(9)
            gpemd(1,1,1,1)=gcartV1(7)
            gpemd(2,1,1,1)=gcartV1(8)
            gpemd(3,1,1,1)=gcartV1(9)
            gpema(1,2,1)=gcartV1(10)
            gpema(2,2,1)=gcartV1(11)
            gpema(3,2,1)=gcartV1(12)
            gpemd(1,2,1,1)=gcartV1(10)
            gpemd(2,2,1,1)=gcartV1(11)
            gpemd(3,2,1,1)=gcartV1(12)
          elseif(nclu.eq.3) then
            gpema(1,1,1)=gcartV1(4)
            gpema(2,1,1)=gcartV1(5)
            gpema(3,1,1)=gcartV1(6)
            gpemd(1,1,1,1)=gcartV1(4)
            gpemd(2,1,1,1)=gcartV1(5)
            gpemd(3,1,1,1)=gcartV1(6)
            gpema(1,2,1)=gcartV1(7)
            gpema(2,2,1)=gcartV1(8)
            gpema(3,2,1)=gcartV1(9)
            gpemd(1,2,1,1)=gcartV1(7)
            gpemd(2,2,1,1)=gcartV1(8)
            gpemd(3,2,1,1)=gcartV1(9)
            gpema(1,3,1)=gcartV1(10)
            gpema(2,3,1)=gcartV1(11)
            gpema(3,3,1)=gcartV1(12)
            gpemd(1,3,1,1)=gcartV1(10)
            gpemd(2,3,1,1)=gcartV1(11)
            gpemd(3,3,1,1)=gcartV1(12)
          else
            k=0
            do i=1,NCLU
              do j=1,3
                k=k+1
                gpemd(j,i,1,1)=gcartV1(k)
                gpema(j,i,1)=gcartV1(k)
              end do
            end do
          endif
!
! ######################################################################
!     direct dynamics interface for G09 and Molpro
        elseif (potflag.eq.-1) then
! ######################################################################
          do i=1,nclu
            x(i) = xx(1,i)
            y(i) = xx(2,i)
            z(i) = xx(3,i)
            symbm(i) = symb(i)
          enddo
          call pot(symbm,x,y,z,pema,gpema,dvec,nclu,mnat,nsurft,mnsurf, &
            1,qcpack,charge,multi)

!     set diabatic energies to zero. these shouldn't be used
          do i=1,nsurft
            do j=i,nsurft
              pemd(i,j)=0.d0
              do j1 = 1, 3
                do j2 = 1, nclu
                  gpemd(j1,j2,i,j)=0.d0
                enddo
              enddo
            enddo
          enddo
!###########################################################
!    direct dynamics interface for MOPAC 
!    single surface trajectory
        elseif (potflag.eq.-2) then

          call pot(indmm,xx,v,dxyz,.true.,nclu,nclu*3)

          pema(1) = v
          pemd(1,1) = v
          do i=1,nclu
            gpema(1,i,1)=dxyz(3*(i-1)+1)
            gpema(2,i,1)=dxyz(3*(i-1)+2)
            gpema(3,i,1)=dxyz(3*(i-1)+3)
            gpemd(1,i,1,1)=dxyz(3*(i-1)+1)
            gpemd(2,i,1,1)=dxyz(3*(i-1)+2)
            gpemd(3,i,1,1)=dxyz(3*(i-1)+3)
            dvec(1,i,1,1)=0.d0
            dvec(2,i,1,1)=0.d0
            dvec(3,i,1,1)=0.d0
          enddo

        endif

      end subroutine getpem
