      subroutine ranmolcub(a,b,c)
!
! Generate nunit random molecule within a cubic of rran(1)*rran(2)*rran(3)
!
! xxm:    coordinates for the molecule (AG)
! rijco:  the smallest distance between two atoms allowed 0.8*R0covalent
! ran:    random number
! iclu:   The times tried to generated a whole set of coordinates   
! ikp:  couting the atoms kept 
! irej:   0: keep, 1: reject
! rijm:      distance between two atoms
      use param, only: mnat, mnfrag, mnmol, mnbond
      use c_struct, only: xxm, rijco, nunit, iflinear, rotaxis, &
                    momiprin, natoms, xunit, mm0, natunit, indmm
      implicit none
      integer iclu,ikp,itry,i,j,k,ii,itmp
      double precision rijm,xxn(3),rsmall,comx(3),mtot,xt(3,50),ran(3)
      double precision a,b,c
 
!     CoM coordinate of xunit
        comx(1) = 0.0d0 
        comx(2) = 0.0d0 
        comx(3) = 0.0d0 
      mtot=0.0d0
      do i=1,natunit
        comx(1) = comx(1) + mm0(i)*xunit(1,i)
        comx(2) = comx(2) + mm0(i)*xunit(2,i)
        comx(3) = comx(3) + mm0(i)*xunit(3,i)
        mtot = mtot + mm0(i)
      enddo
      comx(1) = comx(1)/mtot
      comx(2) = comx(2)/mtot
      comx(3) = comx(3)/mtot
      do i=1,natunit
          xunit(1,i) = xunit(1,i) - comx(1)
          xunit(2,i) = xunit(2,i) - comx(2)
          xunit(3,i) = xunit(3,i) - comx(3)
          xxm(1,i)=xunit(1,i)
          xxm(2,i)=xunit(2,i)
          xxm(3,i)=xunit(3,i)
      enddo
!  transfer the building block mol to its own principle axis of rotation
!  rotprin uses xxm, natoms, mmm to transfer data. before calling this
!  subroutine, xxm is not used, the first j mmm is the same as the
!  building unit. but natoms should be changed back
      itmp=natoms
      natoms=natunit
      call rotprin(xxm,mm0,natoms,momiprin,rotaxis,iflinear)
      call rottran(rotaxis,xunit,natunit)
      natoms=itmp

!
      iclu   = 0
 10   continue
!
!
      iclu = iclu + 1
      if (iclu.gt.20) then
        write(6,111) 'Tried too many times to generate a random sphere', &
                     ' structure ... quitting'
        stop
      endif
! generate nunit points within the cubic
      itry = 0
      ikp = 0
      do while (ikp .lt. nunit)
!
        itry = 0
 20     itry = itry + 1
        if (itry.eq.100000) then
          write(6,*) 'Bad packing.  Start again.'
          goto 10
        endif
 30     do i=1,3
          call get_rng(ran(i)) 
        enddo
!       a new generated CoM of building mol.
        xxn(1) = a* ran(1)
        xxn(2) = b* ran(2)
        xxn(3) = c* ran(3)
!       get the coord. of building mol. from the storage.
        do i=1,natunit
          xt(1,i)=xunit(1,i)
          xt(2,i)=xunit(2,i)
          xt(3,i)=xunit(3,i)
        enddo
!       generate a rotation for the building mol.
        call ranrot(rotaxis,.false.)
        call rottran(rotaxis,xt,natunit)
!       coord. of the new building mol.
        do i=1,natunit
          xt(1,i)=xt(1,i)+xxn(1)
          xt(2,i)=xt(2,i)+xxn(2)
          xt(3,i)=xt(3,i)+xxn(3)
        enddo
!       if the new mol. is out of the cell, regenerate
        do i=1,natunit
          if (xt(1,i).gt.a .or. xt(1,i).lt.0.d0) goto 30
          if (xt(2,i).gt.b .or. xt(2,i).lt.0.d0) goto 30
          if (xt(3,i).gt.c .or. xt(3,i).lt.0.d0) goto 30
        enddo
!       rijm: distance between all other atoms and the new mol.
        do i=1,ikp*natunit
          do j=1,natunit
!         since all the unit are the same, the indmm of the first j are
!         the same as those of the unit
            rsmall = 0.8d0*rijco( indmm(i),indmm(j) )
            rijm = 0.0d0
            do k=1,3
              rijm = rijm + ( xxm(k,i) - xt(k,j) )**2
            enddo
            rijm = dsqrt(rijm)
            if (rijm.lt. 0.8d0*rsmall) goto 20
          enddo
        enddo
!       save geometry 
        do j=1,natunit
          ii=ikp*natunit+j
          do k=1,3
            xxm(k,ii) = xt(k,j)
          enddo
        enddo
        ikp = ikp + 1
      enddo

 111  format(1x,2a)

      end subroutine ranmolcub
