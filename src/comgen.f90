      subroutine comgen(xx,mm,natoms,com,mtotal)
! This subroutine calculate the center of mass for a molecule
! and always place the origin at the CoM
! Input: xx,mm,natoms
!  Program give:
!  com:  CoM of the AG
!  xx:     new coord. moved to CoM
!  mtotal:  Total weight of the AG
      use param, only: mnat

      implicit none
      integer :: natoms
      integer :: i,j
      double precision :: xx(3,mnat),mm(mnat)
      double precision :: com(3),mtotal
!
! Calculate the center of mass of AG im
! Initialize center of mass values to be zero and calculate the total
! mass of AG im
      com(1) = 0.0d0 
      com(2) = 0.0d0 
      com(3) = 0.0d0 
!
      mtotal=0.0d0
      do i=1,natoms
        com(1) = com(1) + mm(i)*xx(1,i)
        com(2) = com(2) + mm(i)*xx(2,i)
        com(3) = com(3) + mm(i)*xx(3,i)
        mtotal = mtotal + mm(i)
      enddo
! The real center of mass for AG im
      com(1) = com(1)/mtotal
      com(2) = com(2)/mtotal
      com(3) = com(3)/mtotal
! Place the origin at the CoM
      do i=1,natoms
          xx(1,i) = xx(1,i) - com(1)
          xx(2,i) = xx(2,i) - com(2)
          xx(3,i) = xx(3,i) - com(3)
      enddo
!
      end subroutine comgen
