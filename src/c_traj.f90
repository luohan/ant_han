module c_traj

use param, only: mnat, mngauss, mnsurf, mntmod, mntor, mnmol, mnphase
!
! COMMON BLOCK WITH VARIBLES THAT EVOLVE ALONG EACH SINGLE TRAJECTORY
!
! Note: When "j" is an index, the array runs over atoms,
!   and when "k" is an index, the array runs over surfaces,
!   and when "a" is an index, the array runs over the x,y,z Cartesian
!   components.

! **********************************************************************
! INITIAL VALUES
! **********************************************************************
! PPI(a,j) : Initial values of PP for a trajectory
! CREI(k)  : Initial value of the real part of the electronic coefficients
! CIMI(k)  : Initial value of the imaginary part of the electronic coefficients
! BIGJI(3,mnmol+1) : Initial values of components of total angular momentum,
!            that for all the AGs is stored in BIGJI(3,mnmol+1)
! BIGJTOTI(mnmol+1) : Initial values of total angular momentum, the last
!      stores the value for the Whole system
! PEI      : Initial values of potential energy
! KEI      : Initial values of kinetic energy
! TEI      : Initial values of total energy
! TEMPI    : Initial temperature
! **********************************************************************


! **********************************************************************
! NUCLEAR AND ELECTRONIC VARIABLES
! **********************************************************************
! PPM(a,j)  : Working storage of PP
! PPF(a,j)  : Final PP
! NSURF     : Current surface
! PEM(k)    : Potential energies for whichever representation we are using.
! erot(mnmol)    : Rotational energy of an AG
! evib(mnmol)    : Vibrational energy of an AG (relative ewell)
! zpe(mnmol)     : Zero-Point Vibrational Energy
! **********************************************************************


! **********************************************************************
! OTHER VARIABLES
! **********************************************************************
! BIGJ(3)   : Components of total angular momentum
! BIGJTOT   : Total angular momentum
! PE        : Current potential energy
! KE        : Current kinetic energy
! TE        : Current total energy
! TEMP      : Current temperature
! press     : Current pressure
! vol       : Current volumne
! TIME      : Current time
! ISTEP     : Number of integrator steps
! STEP      : actual time step
! ITRAJ     : Trajectory number
! kbt0      :     kb*temp0
! kbtarg    :     kb*temptarg
! inh       : if using Nose-Hoover or not
! isave     : count number of points saved during an equilibrium run
! nreinit   :    After a long run, the rotational momentum of the AG may
!                vary a lot. Remove it every nreinit steps. Default 500
! lzerocom  : Move the CoM back to origin
! lzerotrans: Remove the CoM motion
!
! nhop(i)   : number of hops (only used in TFS, FSTU, and FSTU/SD)
!             i=1 -> Total number of hops
!             i=2 -> Number of energetically allowed hops
!             i=3 -> Number of frustrated hops which are reflected
!             i=4 -> Number of frustrated hops which are ignored
!
! frusmeth: integer to choose the method to treat frustrated hops
! frusmeth = 0: all fustrated hops are ignored.
! frusmeth = 1: all frustrated hops are reflected.
! frusmeth = 2: gradV is applied for frustrated hops.
! tinyrho : a parameter used to avoid dividing by zero (or by a number that is very
!           close to zero) in non-Born-Oppenheimer decay-of-mixing trajectories when
!           using eq. (69) of NBO book chapter (A. Jasper and D. G. Truhlar)
!
! **************************************************************************
!           TRAPZ-like methods
! **************************************************************************
!
! ntrapz: integer to choose if a projected hessian and/or a TRAPZ-like method
!         has to be used.
! ntrapz = 0: use of the traditional Hessian for the initial normal mode
!             analysis and TRAPZ-like methods are not applied during the
!             dynamics.
! ntrapz = 1: use of the projected Hessian for the initial normal mode analysis
!             and TRAPZ-like methods are not applied during the dynamics.
! ntrapz = 2: use of the traditional Hessian for the initial normal mode
!             analysis and TRAPZ-like methods are applied during the dynamics.
! ntrapz = 3: use of the projected Hessian for the initial normal mode analysis
!             and TRAPZ-like methods are applied during the dynamics.
!
! nvers: integer to select the ZPE maintenance method
!         (only used for ntrapz = 2 or 3)
! nvers = 0: the general TRAPZ method.
! nvers = 1: the mTRAPZ method.
! nvers = 2: the mTRAPZ* method.
!
! freqlim: frequency limit to avoid entering TRAPZ-like methods (cm-1)
!          (The default is 1cm-1)
!
! itrapz: a counter that determines the number of times TRAPZ-like methods are
!         used throughout each trajectory.
!
! prodzpe: product ZPE (only used with mTRAPZ*, ie nvers = 2)
!
! **************************************************************************
!           Mode populations
! **************************************************************************
!
! lmodpop = .true.: we want to study mode populations.
! lmodpop = .false.: we do not want to study mode populations.
!
! ralpha: coefficient that is used to divide the initial vibrational energy
!         when calculating mode populations (the default is 1.0d0)
!
!  RVM Use of stochastic decoherence
!  stodecoflag = 0  Do not use it
!  stodecoflag = 1  Use it in either TFS or FSTU (recommended)
! **********************************************************************
!  Analysis control information
! **********************************************************************
! nstat:         integer, Start to average properties after nstat steps
! cohesive:      Average on cohesive energy
! kinetic:       Average on kinetic energy
! potential:     Average on potential energy
! totenergy:     Average on total energy
! heatcap:       Calculate heat capacity
! icfrag:        Average on the concentration of each fragment
! rbin:          Bin distance (width of the bins)
! **********************************************************************
!
      double precision :: ppi(3,mnat),ppm(3,mnat),ppf(3,mnat),  &
                          bigji(3,mnmol+1),bigjtoti(mnmol+1),   &
                          tei,pe,ke,te,kbt0,kbtarg,prodzpe,     &
                          bigj(3),bigjtot,temp,press,vol,step,  &
                          time,evib(mnmol+1),zpe(mnmol+1),erot(mnmol+1),&
                          rbin,tinyrho,freqlim,ralpha
      integer :: nsurf,istep,itraj,nstat,nreinit,isave,nhop(4),ntrapz, &
                 nvers,stodecoflag,frusmeth,itrapz
      logical :: cohesive,kinetic,potential,totenergy,heatcap,inh,icfrag, &
                 lzerocom,lzerotrans,lmodpop
! **********************************************************************
! Surface variables
! pema(mnsurf)         :  PE of adiabatic surface (single surface)
! pemd(mnsurf,mnsurf)  :  PE of non-adiabatic surface (multiple surface)
! gpema(3,mnat,mnsurf)        :  Gradient of adiabatic surface
! gpemd(3,mnat,mnsurf,mnsurf) :  Gradient of non-adiabatic surface
! dvec(3,mnat,mnsurf,mnsurf)  : Components of the nonadiabatic coupling
!  vector for every set of potential energy surfaces.
! gvec(3,mnat,mnsurf,mnsurf)  :  Gradient vector of non-adiabatic surface
! phop(mnsurf)                :  Hopping probability (for surface
!                                hoppping) or switching probability (for
!                                SCDM and CSDM) divided by the stepsize.
!   RVM   bjk is used to determine phop using Mike Hack's method
! bfunc(mnsurf,mnsurf)
!   RVM
! GV(a,j)   : Nuclear gradient.
! CRE(2*k)  : Real part of the electronic coefficients
! CIM(2*k)  : Imaginary part of the electronic coefficients
! GCRE(2*k) : Time derivative of real part of electronic coefficients
! GCIM(2*k) : Time derivative of imaginary part of electronic coefficients
! Note      : For the CSDM method, CRE, CIM, GCRE, and GCIM also contain
!             the coherent parts of these quantities stored as
!             elements NSURFT+1 to 2*NSURFT.
! DVEC(a,j,k,k) : Components of the nonadiabatic coupling vector for
!                 every set of potential energy surfaces.
! PHASE(k)  : Integration of the potnential energies.
! **********************************************************************
      double precision :: pema(mnsurf),pemd(mnsurf,mnsurf),pem(mnsurf),  &
                          gpema(3,mnat,mnsurf),gpemd(3,mnat,mnsurf,mnsurf),  &
                          gpem(3,mnat,mnsurf),gv(3,mnat),   &
                          gvec(3,mnat,mnsurf,mnsurf),dvec(3,mnat,mnsurf,mnsurf), &
                          gcre(2*mnsurf),gcim(2*mnsurf),crei(2*mnsurf),phase(mnsurf),  &
                          cimi(2*mnsurf),cre(2*mnsurf),re(2*mnsurf),cim(2*mnsurf), &
                          phop(mnsurf)
!  RVM  bjk to be used in Mike Hack's definition of hopping and switching probs
!    y and yo are used by the integrators
      double precision :: bfunc(mnsurf,mnsurf)

!  RVM
!
! **********************************************************************
! For reactive collision
! ireact     = if a reaction occurs, ireact=1, else ireact=0
! icollide   = count on collision trajectory
! rxrate        Reaction rate constant
! rxprob        Reaction propability
! cross         reactive cross-section
! t0react       The time when a reaction occurs
! **********************************************************************
      integer :: ireact,icollide
      double precision :: rxrate,cross,rxprob,t0react,trxover
!
!  JZ tunneling
!
! ***********************************************************************
! For tunneling
! ltunn : .true. for tunneling .false. for no tunneling. default is .false.
! eta   : a parameter between 0 and 1 for army ants algorithm
!
!
      logical :: ltunn
      double precision :: eta,actint_thr,weight,sumw,collw,node(mngauss), gw(mngauss)
!xxx      double precision :: tunnd(3,mnat)
      double precision :: tunnd(3,mnat,3*mnat-6)
      integer :: ngauss,ntunn_mode,tuntype,itmod(mntmod)

!  JZ torsion
      integer ::  indtor(mntor),nflip

! *********************************************************************
! Atom-diatom related
! *********************************************************************
      integer :: vvad,jjad
      double precision :: escatad,ecolad,einti,rinad,routad,tauad,pprelad
      double precision :: phasead(mnphase,2)
! The following are used for atom-diatom scattering conditions (INITx = 4)
! In ANT16, they are part of c_inital.f90, Han moved it here to make them
! different for different trajectory
! vvad       Initial vibrational quantum number of the diatom
! jjad       Initial rotational quantum number of the diatom
! escatad    Fixed total energy (input in eV, converted immediately to au)
! ecolad     Initial atom-diatom collisional energy (input in eV, converted to au)
! einti       Rovibrational energy diapot(vvad,jjad), calculated in preatomdiatom
! rinad      Inner turning point of initial diatom
! routad     Outer turning point of initial diatom
! tauad      Period in au of initial diatom
! pprelad    Initial relative momenta in au
! phasead    Molecular distance for a period of vibration
!            molecular ditance v.s. phase (1:mnphase)
      double precision :: thetaad,phiad,etaad,psiad,chiad,b0ad
        !check Ab initio state-specific N2+ O dissociation and exchange modeling for molecular simulations to learn
! *********************************************************************
end module c_traj
