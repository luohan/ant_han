      subroutine ewkb(ji,vi,arri,mm,erotvib,rin,rout,nsurf,ierror)

! This subroutine is specialized for atom-diatom initial conditions.
! It computes the WKB energy for the rovibrational state (vi,ji).
! INPUT
! ji = initial rot state of diatom
! vi = initial vib state of the diatom
! arri = initial arrangement
! mm = masses of the atoms
! OUTPUT
! erotvib = rovibrational energy for (vi,ji) state

      use param, only: mnat, autoev, autoang
      use c_output,only: maxprint

      implicit none
      logical :: ierror

      integer :: ji,vi,arri,nsurf
      double precision :: mm(mnat),erotvib,xj

!     local
      integer :: i,j,JMAX
      double precision :: rmin,emin,emax,xv,f,fmid,dx,rtbis_vib,etry,rin,rout

      double precision :: rmax,emax2,tinyv
      parameter(tinyv=1.0d-12)

      ierror = .false.
      xj = dble(ji)

! Need to bracket energy for (vi,ji) state
!     find minimum of diatom (no rotation)
      call diamin(rmin,emin,xj,arri,mm,nsurf,ierror)
      if (ierror)  return
!      write(6,*)'Minimum energy for diatom in arr ',arri,  &
!        ' is ',emin*autoev,' eV at ',rmin*autoang,' A'
!     get asymptotic energy
      call diapot(50.d0,arri,emax2,0.d0,mm,nsurf)
!      write(6,*)'Asym energy is ',emax*autoev,' eV'

!     find maximum energy for possible quasi-bond state
      call diamax(rmax,emax,xj,arri,mm,nsurf,rmin,30.0d0,ierror)
      if  (ierror) return
!      write(6,*)'Maximum energy for diatom in arr ',arri,  &
!        ' is ',emax2*autoev,' eV at ',rmax*autoang,' A'
      if (dabs(emax2-emax) > tinyv) then
        if (maxprint) then
         write(6,"('J = ',I4,' have quasi-bond')") ji
        end if
      else
         rmax = 50.d0
         emax = emax2   !right bond for turn
      endif


! Now determine energy of (vi,ji) state
!     compute vibrational action at E = emin
      call vwkb(arri,mm,emin,xj,xv,rin,rout,nsurf,rmin,rmax,emin,emax,ierror)
      if (ierror) return
      f = xv - dble(vi)
!      write(6,*)'vibrational action at emin = ',xv
!     compute vibrational action at E = emin
      call vwkb(arri,mm,emax,xj,xv,rin,rout,nsurf,rmin,rmax,emin,emax,ierror)
      if (ierror) return
      fmid = xv - dble(vi)
!      write(6,*)'vibrational action at emax = ',xv

      if (f*fmid.ge.0.d0) then
          write(6,1000)
          ierror = .true.
          return
          ! stop
      end if
      if (f.lt.0.d0) then
          rtbis_vib=emin
          dx=emax-emin
      else
          rtbis_vib=emax
          dx=emin-emax
      end if

      JMAX=100
      do j = 1, JMAX
          dx=dx*0.5d0
          etry=rtbis_vib+dx
          call vwkb(arri,mm,etry,xj,xv,rin,rout,nsurf,rmin,rmax,emin,emax,ierror)
          if (ierror) return
!          write(6,*)'trying...',etry*27.211d0
!          write(6,*)'vibrational action at etry = ',xv
          fmid = xv - dble(vi)
          if (fmid.le.0.d0) then
            rtbis_vib=etry
          elseif (abs(dx).lt.1.d-13.or.fmid.eq.0.d0) then
            exit
          end if
      end do
 1000 format(2x,'root must be bracketed in rtbis_vib, check ewkb.f90')
 1010 format(2x,'Too many bisections in rtbis_vib, check ewkb.f90')
      if (j == JMAX) then
        write(6,1010)
        ierror = .true.
        return
      end if
      erotvib = etry

      end subroutine ewkb
