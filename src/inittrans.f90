      subroutine inittrans(temptarg,im)
!
! Setting initial CoM translation motion for an AG
!   Input:
!  temptarg
!    Output:
!  new compp
      use param, only: kb
      use c_struct, only: mmofag
      use c_initial, only: compp

      implicit none
      integer :: im
      double precision :: temptarg
! local
      integer :: i
      double precision :: tmp,tmp1,signs,keim,r1,r2
!
      tmp1=mmofag(im)*kb*temptarg
      do i=1,3
!       etrans generated: 0.5*kB*T
 3333   call  get_rng(r1)
        tmp = 1.d0-r1
        if (tmp.eq.1.d0) goto 3333
!      keim=-0.5kTln(1-r1)
        call get_rng(r2) 
        if (r2 .gt. 0.5) then
                signs=1.d0
        else
                signs=-1.d0
        endif
        compp(i,im)=-signs*dsqrt(-tmp1*dlog(tmp))
      enddo
 
      end subroutine inittrans
