      subroutine getgrad(dmag)

c Calls GETPEM once, and then computes several important variables:
C iflinear:      0: an atom; 1: Linear; 2; non-linear
C  INPUT:
C xxm,ppm,mmm,symbolm,nsurf,nsurft,iflinear,bigj
C  OUTPUT:
C pe,pem,pema,pemd,gpema,gpemd,gpem,gv,cre,cim,gcre,gcim,phop,phase
c gv(3,mnat)     : Nuclear gradient.
c cre(2*mnsurf)  : Real part of the electronic coefficients
c cim(2*mnsurf)  : Imaginary part of the electronic coefficients
c gcre(2*mnsurf) : Time derivative of real part of electronic coefficients
c gcim(2*mnsurf) : Time derivative of imaginary part of electronic coefficients
c PHOP = Hopping probability (for surface hoppping) or switching
c   probability (for SCDM and CSDM) divided by the stepsize.

c  RVM
C For Mike Hack's method of integration we define the bjk here
C bjk = -2*Re(ajk* R.djk) (adiabatic)
C bjk =  2 * hbar-1 *Im(ajk* pemd(j,k)) (diabatic)
C  RVM
c pe             : Potential energy
c dmag = Magnitude of the nonadiabatic coupling vector (for CSDM)
c GCRE and GCIM = Time derivatives of the real and imaginary parts
c   of the electronic variables, including the DM terms for the
c   SCDM and CSDM methods.
c pem            : Array of adiabatic or diabatic potential energies.
c gpem = Gradients of the adiabatic or diabatic potential energies.

      use param, only: mnmol, mnat, mnsurf, mngauss, mntmod, mntor,
     &                 mnfrag, mnmol, mntraj, mnbond
      use c_struct
      use c_sys, only: nsurft, methflag, e0param, cparam, intflag,
     &                 repflag, nsurft
      implicit none
      include 'c_traj.f'
C
      integer i,j,k,l,m,ii,i2,j2
      integer hh,hi
      double precision 
     & rhor(mnsurf,mnsurf),rhoi(mnsurf,mnsurf),vdotd(mnsurf,mnsurf)

      double precision ppvib(3,mnat),svec(3,mnat,mnsurf),
     & es,taui(mnsurf),ps(mnsurf),tmp,dum,dvecmag,pdotd,
     & u2b2(2,2),gu2b2(3,mnat,2,2),dvec2b2(3,mnat),gcred(mnsurf),
     & gcimd(mnsurf),vd(mnsurf),pdeco(3,mnat),smagms,
     & rhorc(mnsurf,mnsurf),rhoic(mnsurf,mnsurf),tmpr,
     & tmpi,dsum2,dmag,sntmp,cstmp,rhotmp,
     & rhorc2(mnsurf,mnsurf),rhoic2(mnsurf,mnsurf)

      double precision comxxm(3),comppm(3),mtot,totcomp
      double precision bigjm(3),bigjtm
      common /c_taui/ taui

c GET ENERGIES,pema,pemd,gpema,gpemd,dvec
      call getpem(xxm,indmm,symbolm,cell,natoms,pema,pemd,gpema,
     &            gpemd,dvec,mmm)

C for multiple surface trajectories (nsurft > 1)
      if (nsurft.gt.1) then

c ZERO
      do i=1,nsurft
        phop(i) = 0.d0
      enddo
c POTENTIAL ENERGIES AND GRADIENTS
      IF (METHFLAG.EQ.0.OR.METHFLAG.EQ.1.OR.METHFLAG.EQ.5) THEN
c     single surface propagation or surface hopping calculation

      if (repflag.eq.0) then
c       adiabatic
        pe = pema(nsurf)
        do i=1,3
        do j=1,natoms
          gv(i,j)=gpema(i,j,nsurf)
        enddo
        enddo
      else if (repflag.eq.1) then
c       diabatic
        pe = pemd(nsurf,nsurf)
        do i=1,3
        do j=1,natoms
          gv(i,j)=gpemd(i,j,nsurf,nsurf)
        enddo
        enddo
      else
        write(6,*)'REPFLAG = ',repflag,' in GETGRAD'
        stop
      endif

      ELSE IF (METHFLAG.EQ.2.OR.METHFLAG.EQ.3.OR.METHFLAG.EQ.4) THEN
c     Semiclassical Ehrenfest and DM methods

c     convert electronic variables to density matrix
      call getrho(cre,cim,rhor,rhoi,nsurft)

      if (repflag.eq.1) then

c       diabatic representation
        pe = 0.d0
        do k=1,nsurft
        do l=1,nsurft
c integrate phase angle separately
        tmp = phase(l)-phase(k)
        sntmp = dsin(tmp)
        cstmp = dcos(tmp)
        rhotmp = (rhor(k,l)*cstmp+rhoi(k,l)*sntmp)
c       imaginary terms cancel exactly
        pe=pe+rhotmp*pemd(k,l)
c end
c integrate whole coefficient
c        pe = pe + rhor(k,l)*pemd(k,l)
c end
        enddo
        enddo
        do 15 i=1,3
        do 15 j=1,natoms
        gv(i,j) = 0.d0
        do 15 k=1,nsurft
        do 15 l=1,nsurft
c integrate phase angle separately
        tmp = phase(l)-phase(k)
        sntmp = dsin(tmp)
        cstmp = dcos(tmp)
        rhotmp = (rhor(k,l)*cstmp+rhoi(k,l)*sntmp)
        gv(i,j)=gv(i,j)+rhotmp*gpemd(i,j,k,l)
c end
c integrate whole coefficient
c        gv(i,j) = gv(i,j) + rhor(k,l)*gpemd(i,j,k,l)
c end
  15    continue

      else if (repflag.eq.0) then

c       adiabatic representation
        pe = 0.d0
        do k=1,nsurft
        pe = pe + rhor(k,k)*pema(k)
        enddo
        do 20 i=1,3
        do 20 j=1,natoms
        gv(i,j) = 0.d0
        do 20 k=1,nsurft
        gv(i,j) = gv(i,j) + rhor(k,k)*gpema(i,j,k)
        do 20 l=1,nsurft
c integrate whole coefficient
c        gv(i,j) = gv(i,j) - 2.d0*rhor(k,l)*dvec(i,j,k,l)*pema(k)
c end
c integrate phase angle separately
        tmp = phase(k)-phase(l)
        sntmp = dsin(tmp)
        cstmp = dcos(tmp)
        rhotmp = (rhor(k,l)*cstmp-rhoi(k,l)*sntmp)
        gv(i,j) = gv(i,j) - 2.d0*rhotmp*dvec(i,j,k,l)*pema(k)
c end
  20    continue

      else
        write(6,*)'REPFLAG = ',repflag,' in GETGRAD'
        stop
      endif

      ELSE

      write(6,*)'METHFLAG = ',methflag,' is not allowed in GETGRAD'
      stop

      ENDIF


c FOR USE BY CSDM, MAGNITUDE OF D
      if (methflag.eq.4) then
c       compute sum of magnitude of coupled DVECs
        dmag = 0.d0
        do k=1,nsurft
          if (k.ne.nsurf) then
            dsum2 = 0.d0
            do i=1,3
            do j=1,natoms
              dsum2 = dsum2 + dvec(i,j,k,nsurf)**2
            enddo
            enddo
          endif
          dsum2 = max(0.d0,dsum2)
          dsum2 = dsqrt(dsum2)
          dmag = dmag + dsum2
        enddo
      endif

c TIME-DERIVATIVES OF THE ELECTRONIC COORDINATES
c     All methods, DM methods add terms later
        if (repflag.eq.1) then
C RVM If we're not using Mike Hack's method define phop as before
         if(intflag.ne.6) then
c         diabatic rep
          do i=1,nsurft
            gcim(i) = 0.d0
            gcre(i) = 0.d0
            do j=1,nsurft
c integrate whole coefficient
c              gcre(i) = gcre(i) - cim(j)*pemd(i,j)
c              gcim(i) = gcim(i) + cre(j)*pemd(i,j)
c end
c integrate phase angle separately
              if (i.ne.j) then
                tmp = phase(j)-phase(i)
                sntmp = dsin(tmp)
                cstmp = dcos(tmp)
                tmpr = -(sntmp*cre(j)-cstmp*cim(j))*pemd(i,j)
                tmpi = -(sntmp*cim(j)+cstmp*cre(j))*pemd(i,j)
                gcre(i) = gcre(i) + tmpr
                gcim(i) = gcim(i) + tmpi
                if (i.eq.nsurf) then
                  phop(j) = -2.d0*(tmpr*cre(i)+tmpi*cim(i))
     &               /(cre(i)**2+cim(i)**2)
                endif 
              endif 
c end
            enddo
          enddo
C  RVM Mike Hack's method in diabatic rep
C  Define bjk for hopping probabilities: gkj = (integral(bjk+) + integral(bjk-))/akk
         elseif(intflag.eq.6) then
          do i=1,nsurft
            gcim(i) = 0.d0
            gcre(i) = 0.d0
            do j=1,nsurft
c integrate whole coefficient
c              gcre(i) = gcre(i) - cim(j)*pemd(i,j)
c              gcim(i) = gcim(i) + cre(j)*pemd(i,j)
c end
c integrate phase angle separately
              if (i.ne.j) then
                tmp = phase(j)-phase(i)
                sntmp = dsin(tmp)
                cstmp = dcos(tmp)
                tmpr = -(sntmp*cre(j)-cstmp*cim(j))*pemd(i,j)
                tmpi = -(sntmp*cim(j)+cstmp*cre(j))*pemd(i,j)
                gcre(i) = gcre(i) + tmpr
                gcim(i) = gcim(i) + tmpi
              endif
             enddo
            enddo
            do i = 2, nsurft
             do j = 1, i-1
                tmp = phase(j)-phase(i)
                sntmp = dsin(tmp)
                cstmp = dcos(tmp)
                tmpr = -(sntmp*cre(j)-cstmp*cim(j))*pemd(i,j)
                tmpi = -(sntmp*cim(j)+cstmp*cre(j))*pemd(i,j)
                  bfunc(j,i) = -2.d0*(tmpr*cre(i)+tmpi*cim(i))
c end
             enddo
            enddo
        endif 
        elseif (repflag.eq.0) then
c         adiabatic rep
c         compute velocity-dot-dvec
          do 10 k=1,nsurft
          do 10 l=1,nsurft
          vdotd(k,l) = 0.d0
          do 10 i=1,3
          do 10 j=1,natoms
            vdotd(k,l) = vdotd(k,l) + dvec(i,j,k,l)*ppm(i,j)/mmm(j)
  10      continue
C  
C RVM If we're not using Mike Hack's method define phop as before
         if(intflag.ne.6) then
          do i=1,nsurft
c integrate whole coefficient
c            gcre(i) =  cim(i)*pema(i)
c            gcim(i) = -cre(i)*pema(i)
c end
c integrate phase angle separately
            gcre(i) = 0.d0
            gcim(i) = 0.d0
            do j=1,nsurft
c end
c integrate whole coefficient
c              gcre(i) =  gcre(i) - cre(j)*vdotd(i,j)
c              gcim(i) =  gcim(i) - cim(j)*vdotd(i,j)
c end
c integrate phase angle separately
              if (i.ne.j) then
                tmp = phase(j)-phase(i)
                sntmp = dsin(tmp)
                cstmp = dcos(tmp)
                tmpr = -(cstmp*cre(j)+sntmp*cim(j))*vdotd(i,j)
                tmpi = -(cstmp*cim(j)-sntmp*cre(j))*vdotd(i,j)
                gcre(i) = gcre(i) + tmpr
                gcim(i) = gcim(i) + tmpi
                if (i.eq.nsurf) then
                  phop(j) = -2.d0*(tmpr*cre(i)+tmpi*cim(i))
     &               /(cre(i)**2+cim(i)**2)
                endif
               endif
c end
            enddo
          enddo
C  RVM Mike Hack's method in adiabatic rep
C  Define bjk for hopping probabilities: gkj = (integral(bjk+) + integral(bjk-))/akk
        elseif(intflag.eq.6) then
          do i=1,nsurft
c integrate whole coefficient
c            gcre(i) =  cim(i)*pema(i)
c            gcim(i) = -cre(i)*pema(i)
c end
c integrate phase angle separately
            gcre(i) = 0.d0
            gcim(i) = 0.d0
            do j=1,nsurft
c end
c integrate whole coefficient
c              gcre(i) =  gcre(i) - cre(j)*vdotd(i,j)
c              gcim(i) =  gcim(i) - cim(j)*vdotd(i,j)
c end
c integrate phase angle separately
              if (i.ne.j) then
                tmp = phase(j)-phase(i)
                sntmp = dsin(tmp)
                cstmp = dcos(tmp)
                tmpr = -(cstmp*cre(j)+sntmp*cim(j))*vdotd(i,j)
                tmpi = -(cstmp*cim(j)-sntmp*cre(j))*vdotd(i,j)
                gcre(i) = gcre(i) + tmpr
                gcim(i) = gcim(i) + tmpi
               endif 
             enddo
            enddo
            do i = 2, nsurft
             do j= 1, i-1 
                tmp = phase(j)-phase(i)
                sntmp = dsin(tmp)
                cstmp = dcos(tmp)
                tmpr = -(cstmp*cre(j)+sntmp*cim(j))*vdotd(i,j)
                tmpi = -(cstmp*cim(j)-sntmp*cre(j))*vdotd(i,j)
                bfunc(j,i) = -2.d0*(tmpr*cre(i)+tmpi*cim(i))
              enddo
             enddo
         endif
c end
        else
          write(6,*)'REPFLAG = ',repflag,' in GETGRAD'
          stop
        endif

      if (methflag.eq.4) then
c       CSDM
c       propagate coherent part of the electronic coordinates
c       put these quantities in CRE and CIM after real coefficients
c       the integrator will integrate the whole thing
c       Also compute phop using the coherent variables.  Phop was computed
c       above using the decoherent variables, here we overwrite it for CSDM.
        if (repflag.eq.1) then
C RVM If we're not using Mike Hack's method define phop as before
         if(intflag.ne.6) then
c         diabatic rep
          do i=1,nsurft
            i2 = i + nsurft
            gcim(i2) = 0.d0
            gcre(i2) = 0.d0
            do j=1,nsurft
              j2 = j + nsurft
c integrate whole coefficient
c              gcre(i2) = gcre(i2) - cim(j2)*pemd(i,j)
c              gcim(i2) = gcim(i2) + cre(j2)*pemd(i,j)
c end
c integrate phase angle separately
              if (i.ne.j) then
                tmp = phase(j)-phase(i)
                sntmp = dsin(tmp)
                cstmp = dcos(tmp)
                tmpr=-(sntmp*cre(j2)-cstmp*cim(j2))*pemd(i,j)
                tmpi=-(sntmp*cim(j2)+cstmp*cre(j2))*pemd(i,j)
                gcre(i2)=gcre(i2)+tmpr
                gcim(i2)=gcim(i2)+tmpi
                if (i.eq.nsurf) then
                  phop(j) = -2.d0*(tmpr*cre(i2)+tmpi*cim(i2))
     &               /(cre(i2)**2+cim(i2)**2)
                endif
              endif
c end
            enddo
          enddo
C  RVM Mike Hack's method in diabatic rep
C  Define bjk for hopping probabilities: gkj = (integral(bjk+) + integral(bjk-))/akk
         elseif(intflag.eq.6) then
          do i=1,nsurft
            i2 = i + nsurft
            gcim(i2) = 0.d0
            gcre(i2) = 0.d0
            do j=1,nsurft
              j2 = j + nsurft
c integrate whole coefficient
c              gcre(i2) = gcre(i2) - cim(j2)*pemd(i,j)
c              gcim(i2) = gcim(i2) + cre(j2)*pemd(i,j)
c end
c integrate phase angle separately
              if (i.ne.j) then
                tmp = phase(j)-phase(i)
                sntmp = dsin(tmp)
                cstmp = dcos(tmp)
                tmpr=-(sntmp*cre(j2)-cstmp*cim(j2))*pemd(i,j)
                tmpi=-(sntmp*cim(j2)+cstmp*cre(j2))*pemd(i,j)
                gcre(i2)=gcre(i2)+tmpr
                gcim(i2)=gcim(i2)+tmpi
              endif
             enddo
            enddo
            do i = 2, nsurft
              i2 = i + nsurft
             do j = 1, i-1
               j2 = j + nsurft
                tmp = phase(j)-phase(i)
                sntmp = dsin(tmp)
                cstmp = dcos(tmp)
                tmpr = -(sntmp*cre(j2)-cstmp*cim(j2))*pemd(i,j)
                tmpi = -(sntmp*cim(j2)+cstmp*cre(j2))*pemd(i,j)
                  bfunc(j,i) = -2.d0*(tmpr*cre(i2)+tmpi*cim(i2))
c end
             enddo
            enddo
           endif
        elseif (repflag.eq.0) then
C RVM If we're not using Mike Hack's method define phop as before
         if(intflag.ne.6) then
          do i=1,nsurft
            i2 = i + nsurft
c integrate whole coefficient
c            gcre(i2) =  cim(i2)*pema(i)
c            gcim(i2) = -cre(i2)*pema(i)
c end
c integrate phase angle separately
            gcim(i2) = 0.d0
            gcre(i2) = 0.d0
c end
            do j=1,nsurft
              j2 = j + nsurft
c integrate whole coefficient
c              gcre(i2) =  gcre(i2) - cre(j2)*vdotd(i,j)
c              gcim(i2) =  gcim(i2) - cim(j2)*vdotd(i,j)
c end
c integrate phase angle separately
              if (i.ne.j) then
                tmp = phase(j)-phase(i)
                sntmp = dsin(tmp)
                cstmp = dcos(tmp)
                tmpr=-(cstmp*cre(j2)+sntmp*cim(j2))*vdotd(i,j)
                tmpi=-(cstmp*cim(j2)-sntmp*cre(j2))*vdotd(i,j)
                gcre(i2)=gcre(i2)+tmpr
                gcim(i2)=gcim(i2)+tmpi
                if (i.eq.nsurf) then
                  phop(j) = -2.d0*(tmpr*cre(i2)+tmpi*cim(i2))
     &               /(cre(i2)**2+cim(i2)**2)
                endif 
               endif
             enddo
            enddo 
C  RVM Mike Hack's method in adiabatic rep
C  Define bjk for hopping probabilities: gkj = (integral(bjk+) + integral(bjk-))/akk
        elseif(intflag.eq.6) then
          do i=1,nsurft
            i2 = i + nsurft
c integrate whole coefficient
c            gcre(i2) =  cim(i2)*pema(i)
c            gcim(i2) = -cre(i2)*pema(i)
c end
c integrate phase angle separately
            gcim(i2) = 0.d0
            gcre(i2) = 0.d0
c end
            do j=1,nsurft
              j2 = j + nsurft
c integrate whole coefficient
c              gcre(i2) =  gcre(i2) - cre(j2)*vdotd(i,j)
c              gcim(i2) =  gcim(i2) - cim(j2)*vdotd(i,j)
c end
c integrate phase angle separately
              if (i.ne.j) then
                tmp = phase(j)-phase(i)
                sntmp = dsin(tmp)
                cstmp = dcos(tmp)
                tmpr=-(cstmp*cre(j2)+sntmp*cim(j2))*vdotd(i,j)
                tmpi=-(cstmp*cim(j2)-sntmp*cre(j2))*vdotd(i,j)
                gcre(i2)=gcre(i2)+tmpr
                gcim(i2)=gcim(i2)+tmpi
              endif
             enddo
            enddo  
            do i = 2, nsurft
             i2 = i + nsurft
             do j= 1, i-1   
               j2 = j + nsurft 
                tmp = phase(j)-phase(i)
                sntmp = dsin(tmp)
                cstmp = dcos(tmp)
                tmpr=-(cstmp*cre(j2)+sntmp*cim(j2))*vdotd(i,j)
                tmpi=-(cstmp*cim(j2)-sntmp*cre(j2))*vdotd(i,j)
                bfunc(j,i) = -2.d0*(tmpr*cre(i2)+tmpi*cim(i2))
              enddo
             enddo
          endif
c end
        else
          write(6,*)'REPFLAG = ',repflag,' in GETGRAD'
          stop
        endif
      endif

c SPECIAL DM TERMS
      if (methflag.eq.3.or.methflag.eq.4) then
c       DM methods
c       compute pvib
        if (natoms.eq.3) then
          call pjsplit(xxm,ppm,mmm,ppvib)
         else
c         THIS PART (FOR MORE THAN 3 ATOMS) HASN'T BEEN TESTED
         do i=1,3
          do j=1,natoms
            ppvib(i,j) = ppm(i,j)
          enddo
         enddo
         call angmom(iflinear,bigjm,ppvib,xxm,natoms,bigjtm)
         call noang(iflinear,bigjm,ppvib,xxm,mmm,natoms)
        endif

        do k=1,nsurft
        if (k.eq.nsurf) then
c       skip
        else
c         compute 'reduced' nonadiabatic coupling for states k and l
          if (repflag.eq.1) then
c           diabatic
            u2b2(1,1) = pemd(k,k)
            u2b2(1,2) = pemd(k,nsurf)
            u2b2(2,1) = pemd(nsurf,k)
            u2b2(2,2) = pemd(nsurf,nsurf)
            do i=1,3
            do j=1,natoms
              gu2b2(i,j,1,1) = gpemd(i,j,k,k)
              gu2b2(i,j,1,2) = gpemd(i,j,k,nsurf)
              gu2b2(i,j,2,1) = gpemd(i,j,nsurf,k)
              gu2b2(i,j,2,2) = gpemd(i,j,nsurf,nsurf)
            enddo
            enddo
            call getdvec2(natoms,u2b2,gu2b2,dvec2b2)
          else
c           adiabatic rep
c           just use real DVEC
            do i=1,3
            do j=1,natoms
              dvec2b2(i,j) = dvec(i,j,k,nsurf)
            enddo
            enddo
          endif

c         compute PP dot DVEC2b2 / |DVEC2b2|
          pdotd = 0.d0
          dvecmag = 0.d0
          do i=1,3
          do j=1,natoms
C            pdotd = pdotd + dvec2b2(i,j)*ppm(i,j)/mmm(j)
            pdotd = pdotd + dvec2b2(i,j)*ppvib(i,j)/mmm(j)
            dvecmag = dvecmag + (dvec2b2(i,j)**2)/mmm(j)
          enddo
          enddo
          if (dvecmag.lt.1.d-40) then
          pdotd = 0.d0
          else
          dvecmag = dsqrt(dvecmag)
          pdotd = pdotd/dvecmag
          endif

c         compute SVEC(i,j,k) = DVEC2b2*PP_DVEC+PPVIB
          smagms = 0.d0
          do i=1,3
          do j=1,natoms
            svec(i,j,k)=dvec2b2(i,j)*pdotd+ppvib(i,j)
            smagms = smagms + (svec(i,j,k)**2)/mmm(j)
          enddo
          enddo
          if (smagms.le.0.d0) then
c         problem
          goto 99
          endif
          smagms = dsqrt(smagms)
c         Note:  SVEC is not mass-scaled
c                SMAGMS is the magnitude of the mass-scaled SVEC

c         compute energy along s vector (ES)
          ps(k) = 0.d0
          do i=1,3
          do j=1,natoms
c           use mass-scaling, SMAGMS normalizes the mass-scaled SVEC
c           when computing ES
            ps(k) = ps(k) + svec(i,j,k)*ppm(i,j)/mmm(j)
          enddo
          enddo
          Es = (ps(k)**2)/(2.d0*smagms**2)
   
c         compute tau matrix
C          Cparam = 1.d0
C  RVM E0 is now an input parameter
c          E0param = 0.1d0
          if (repflag.eq.0) tmp = dabs(pema(k)-pema(nsurf))
          if (repflag.eq.1) tmp = dabs(pemd(k,k)-pemd(nsurf,nsurf))
c         inverse of tau
          taui(k) = tmp/(Cparam+E0param/Es)
        endif
        taui(nsurf) = 0.d0
        enddo

c       compute decoherent time-derivs of electronic coords
        gcred(nsurf) = 0.d0
        gcimd(nsurf) = 0.d0
        do k=1,nsurft
          if (k.ne.nsurf) then
          gcred(nsurf) = gcred(nsurf) + rhor(k,k)*taui(k)
          gcimd(nsurf) = gcimd(nsurf) + rhor(k,k)*taui(k)
          gcred(k) = -0.5d0*taui(k)*cre(k)
          gcimd(k) = -0.5d0*taui(k)*cim(k)
          endif
        enddo
        if (rhor(nsurf,nsurf).ne.0.d0) then
        gcred(nsurf) = gcred(nsurf)*0.5d0*cre(nsurf)/rhor(nsurf,nsurf)
        gcimd(nsurf) = gcimd(nsurf)*0.5d0*cim(nsurf)/rhor(nsurf,nsurf)
        else
        goto 99
        endif

c         adiabatic
c       compute time-deriv of decoherent potential
        do k=1,nsurft
        vd(k) = 0.d0
        enddo
        if (repflag.eq.1) then
c         diabatic
          do k=1,nsurft
c           diagonal terms
            if (k.ne.nsurf) then
              vd(k) = vd(k) - rhor(k,k)*taui(k)*pemd(k,k)
            elseif (k.eq.nsurf) then
              do l=1,nsurft
                if (l.ne.k) then
                  vd(l) = vd(l) + rhor(l,l)*taui(l)*pemd(k,k)
                endif
              enddo
            else
              write(6,*)'Strange things in GETGRAD (1)...'
              stop
            endif
            do l=1,nsurft
c             off-diagonal terms
              tmp = phase(l)-phase(k)
              sntmp = dsin(tmp)
              cstmp = dcos(tmp)
              tmpr=rhor(k,l)*cstmp+rhoi(k,l)*sntmp
              if (k.ne.nsurf.and.l.ne.nsurf.and.k.ne.l) then
                vd(k)=vd(k)-0.5d0*(taui(k)+taui(l))*tmpr*pemd(k,l)
              elseif (k.eq.nsurf.and.l.ne.nsurf) then
                vd(l) = vd(l)-0.5d0*taui(l)*tmpr*pemd(k,l)
                do m=1,nsurft
                  if (m.ne.nsurf) then
                    vd(m) = vd(m) + 0.5d0*rhor(m,m)*taui(m)/
     &                      rhor(nsurf,nsurf)*tmpr*pemd(k,l)
                  endif
                enddo
              elseif (l.eq.nsurf.and.k.ne.nsurf) then
                vd(k) = vd(k)-0.5d0*taui(k)*tmpr*pemd(k,l)
                do m=1,nsurft
                  if (m.ne.nsurf) then
                    vd(m) = vd(m) + 0.5d0*rhor(m,m)*taui(m)/
     &                      rhor(nsurf,nsurf)*tmpr*pemd(k,l)
                  endif
                enddo
              endif
            enddo
          enddo
        else 
          do k=1,nsurft
c           diagonal terms
            if (k.ne.nsurf) then
              vd(k) = vd(k) - rhor(k,k)*taui(k)*pema(k)
            elseif (k.eq.nsurf) then
              do l=1,nsurft
                if (l.ne.k) then
                  vd(l) = vd(l) + rhor(l,l)*taui(l)*pema(k)
                endif
              enddo
            else
              write(6,*)'Strange things in GETGRAD (1)...'
              stop
            endif
          enddo
        endif

c       compute decoherent force
        do i=1,3
          do j=1,natoms
              pdeco(i,j) = 0.d0
            do k=1,nsurft
              if (k.ne.nsurf) then
c       magnitude of svec cancels magnitude of svec in ps(k)
                pdeco(i,j)=pdeco(i,j)-vd(k)*svec(i,j,k)/ps(k)
              endif
            enddo
          enddo
        enddo

c       add decoherent terms to electronic probs
        do k=1,nsurft
          gcre(k) = gcre(k) + gcred(k)
          gcim(k) = gcim(k) + gcimd(k)
        enddo

c       add force to grad pe
c       add the negative so it is like a gradient, it's negative will be taken
c       in XPTOY

        do i=1,3
          do j=1,natoms
            gv(i,j) = gv(i,j) - pdeco(i,j)
          enddo
        enddo
      endif

 99   continue
C
C save PEM as the diagonal elements of whichever represenation we are using
      do i=1,nsurft
        if (repflag.eq.0) then
        pem(i) = pema(i)
        do j=1,3
        do k=1,natoms
          gpem(j,k,i) = gpema(j,k,i)
        enddo
        enddo
        else
        pem(i) = pemd(i,i)
        do j=1,3
        do k=1,natoms
          gpem(j,k,i) = gpemd(j,k,i,i)
        enddo
        enddo
        endif
      enddo

C for single surface trajectories
      else
        pe = pema(nsurf)
        pem(1) = pema(1)
        do i=1,3
        do j=1,natoms
          gv(i,j)=gpema(i,j,nsurf)
        enddo
        enddo
        gcre(1) = 0.d0
        gcim(1) = 0.d0
      endif
C
 111  format(1x,100g10.3)
      return
      end
