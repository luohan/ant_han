      subroutine halperin(xx,rij,rijmax,nat,mnat,phi6,phi6a)
! Calculate bond-orientational parameter
! Ref. Nelson, D. R.; Halperin, B. I., Physical Review B 1979, 
! 19, 2457-2484.
      implicit none
      integer :: nat,mnat
      double precision :: xx(3,mnat), rij(mnat,mnat)
! r <rijmax will be deemed as neighbors
      double precision :: rijmax, phi6, phi6a(mnat)

      integer :: i,j,k
      integer :: ine
      double precision :: dx,dy,dz,r,rxy
! Real part and imaginary part
      double precision,parameter :: pi=3.1415926535d0
      double precision :: phi6i,phi6r
      double precision :: phi6ai,phi6ar
      double precision :: cost(nat,nat),sint(nat,nat),theta(nat,nat),tmp
      double precision :: cosp(nat,nat),sinp(nat,nat),phi(nat,nat)
      double precision :: legendre
! phi6a=sum(exp(6i theta(i,j))/Ni
! Theta(i,j) is the angle of rij between an axis (x here)

      phi6 = 0.d0
      do i=1,nat
        phi6a = 0.d0
      enddo
      phi6ai=0.d0
      phi6ar=0.d0

      do i=1,nat
      do j=i+1,nat
        if (rij(i,j).le.rijmax) then
! Mid point of a bond between i and j 
          dx = 0.5d0*(xx(1,i)+xx(1,j))
          dy = 0.5d0*(xx(2,i)+xx(2,j))
          dz = 0.5d0*(xx(3,i)+xx(3,j))
          rxy = dx*dx + dy*dy
          r  = dsqrt(rxy + dz*dz)
          rxy = dsqrt(rxy)
! phi (angle between rij and z: 0-PI)
          if (r.ne.0.d0) then
          tmp     =  dz/r
          if (tmp.gt.1.d0) tmp=1.d0
          if (tmp.lt.-1.d0) tmp=-1.d0
          phi(i,j)  = dacos(tmp)
! Theta (angle between r and x: 0-2PI)
          if (rxy.ne.0.d0) then
            tmp     =  dx/rxy
            if (tmp.gt.1.d0) tmp=1.d0
            if (tmp.lt.-1.d0) tmp=-1.d0
! dacos: 0-PI
            theta(i,j) = dacos(tmp)
            if (dy.lt.0.d0) then
              theta(i,j) = 2.d0*pi-theta(i,j)
            endif
          else
              theta(i,j) = 0.d0
          endif
          cost(i,j) = dcos(theta(i,j))
          sint(i,j) = dsin(theta(i,j))
          cost(j,i) = cost(i,j)
          sint(j,i) = sint(i,j)
          endif
        endif
      enddo
      enddo

      do i=1,nat
      phi6r = 0.d0
      phi6i = 0.d0
      ine = 0
      do j=1,nat
        if (i.eq.j) cycle
        if (rij(i,j).le.rijmax) then
          ine   = ine + 1
          phi6r = phi6r + cost(i,j)
          phi6i = phi6i + sint(i,j)
        endif
      enddo
      phi6r = phi6r/dble(ine)
      phi6i = phi6i/dble(ine)
      phi6a(i) = phi6r**2+phi6i**2
      phi6ar = phi6ar + phi6r
      phi6ai = phi6ai + phi6i
      enddo

      phi6ar = phi6ar/dble(nat)
      phi6ai = phi6ai/dble(nat)
      phi6   = phi6ar**2 + phi6ai**2

      return
      END

      double precision function nlegendre(x,l)
! Normalized Legendre polynomial
      implicit none
      integer l
      double precision legendre,x
      integer i,n

        nlegendre = dsqrt(0.5d0*dble(2*l+1))*legendre(x,l)

      end function nlegendre

      double precision function legendre(x,l)
! Legendre polynomial
      implicit none
      integer l
      double precision x,p0,p1,p2
      integer i,n

      p0 = 1.d0
      p1 = x
      if (l.eq.0) then
        legendre = 1.d0
      elseif (l.eq.1) then
        legendre = x
      else
        do i=2,l
          n = i-1
!         recurrence relation
          p2 = dble(2*n+1)*x*p1 - dble(n)*p0
          p2 = p2/dble(i)
          p0 = p1
          p1 = p2
        enddo
        legendre = p2
      endif

      end function legendre

      FUNCTION PLM(LIN,MINN,COSTH)
!
!     COMPUTES NORMALIZED ASSOC. LEGENDRE POLYNOMIALS BY RECURSION.
!     THE VALUES RETURNED ARE NORMALIZED FOR INTEGRATION OVER X
!     (I.E. INTEGRATION OVER COS THETA BUT NOT PHI).
!     NOTE THAT THE NORMALIZATION GIVES
!           PLM(L,0,1)=SQRT(L+0.5)
!           PLM(L,0,X)=SQRT(L+0.5) P(L,X)
!     FOR M.NE.0, THE VALUE RETURNED DIFFERS FROM THE USUAL
!           DEFINITION OF THE ASSOCIATED LEGENDRE POLYNOMIAL
!           (E.G. EDMONDS PAGES 23-24)
!           BY A FACTOR OF (-1)**M*SQRT(L+0.5)*SQRT((L-M)!/(L+M)!)
!     THUS THE SPHERICAL HARMONICS ARE
!          CLM = PLM * EXP(I*M*PHI) / SQRT(L+0.5)
!          YLM = PLM * EXP(I*M*PHI) / SQRT(2*PI)
!     THIS ROUTINE ALWAYS RETURNS THE VALUE FOR ABS(MINN); NOTE THAT
!          FOR MINN.LT.0 THIS VALUE SHOULD BE MULTIPLIED BY PARITY(MINN)
!
!     FUNCTION PM1(LIN,MINN,COSTH)
!     This routine appears to be much more stable for large l, m than
!       the routine from Nerf/ modified according to R.T Pack
!     It was obtained:
!     From: Marie-Lise Dubernet <mld@ipp-garching.mpg.de>
!     Date: Mon, 19 Jun 1995 12:48:11 +0200 (MET DST)
!     Some mods 27-28 June 95 by SG for speed and to accord w/ MOLSCAT 
!     Bugs fixed 21 Sept 95 (SG)
!
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
!
!     CHECK FOR ABS(COSTH).LE.1.D0 ... 
      IF (ABS(COSTH).GT.1.D0) THEN
        WRITE(6,*) ' *** ILLEGAL ARGUMENT TO PLM. X =',COSTH
        STOP
      ENDIF
!     SAVE ARGUMENTS IN LOCAL VARIABLES
      L=LIN
      M=ABS(MINN)
      X=COSTH
!
!  IF M>L PLM=0 !
      IF(M.GT.L) THEN
        PLM=0.D0
        RETURN
      ENDIF
      LMAX=L
!
      IF (M.GT.0) GO TO 5
!  HERE FOR REGULAR LEGENDRE POLYNOMIALS
      PLM=1.D0
      PM2=0.D0
      XL=0.D0
      DO 2 L=1,LMAX
      XL=XL+1.D0
      PP=((2.D0*XL-1.D0)*X*PLM-(XL-1.D0)*PM2)/XL
      PM2=PLM
    2 PLM=PP
      GO TO 9000
!
!  HERE FOR ALEXANDER-LEGENDRE POLYNOMIALS
!
    5 IMAX=2*M
      RAT=1.D0
      AI=0.D0
      DO 6 I=2,IMAX,2
      AI=AI+2.D0
    6 RAT=RAT*((AI-1.D0)/AI)
!     Y=SIN(THETA)
      Y=SQRT(1.D0-X*X)
      PLM=SQRT(RAT)*(Y**M)
      PM2=0.D0
      LOW=M+1
      XL=LOW-1
      DO 10 L=LOW,LMAX
      XL=XL+1.D0
      AL=DBLE((L+M)*(L-M))
      AL=1.D0/AL
      AL2=(DBLE((L+M-1)*(L-M-1)))*AL
      AL=SQRT(AL)
      AL2=SQRT(AL2)
      PP=(2.D0*XL-1.D0)*X*PLM*AL-PM2*AL2
      PM2=PLM
   10 PLM=PP
      PLM=PLM*xPARITY(MINN)
!
!     CONVERT TO MOLSCAT'S IDIOSYNCRATIC NORMALIZATION
9000  PLM=PLM*SQRT(XL+0.5D0)

      RETURN
      END

      FUNCTION xPARITY(I)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      xPARITY=1.D0
       IF((I/2)*2-I.NE.0) xPARITY=-1.D0
      RETURN
      END
