
!C*********************************************************************!
      subroutine toBowman(x,bow)
!C*********************************************************************!
!     Transfer cartesian coordinates into bowman coordinates for 4 atom
!     system
      implicit none
!     X in one array with x1,y1,z1,...x4,y4,z4
      double precision  x(12)
      double precision  bow(7) 
      double precision  PI
      parameter ( PI = 3.1415926535897930d0 )
!
! This function returns the Bowman  
! coordinates from cartesian coordinates
! All angles are in DEGREES
!
! Output:
! bow(1) : r1
! bow(2) : r2
! bow(3) : r3
! bow(4) : theta1
! bow(5) : theta2
! bow(6) : theta3
! bow(7) : beta
!
! Input: 
! coord(1,:) : N
! coord(2,:) : H1
! coord(3,:) : H2
! coord(4,:) : H3
!
      double precision  r(3)
      double precision  beta(3)  !angles in degrees
      double precision  theta(3) !angles in degrees

!------------------------C
! Parameter list
!------------------------C
      integer IOUT1,INP1
      double precision TOL
      parameter ( IOUT1 = 0)
      parameter ( INP1 = 5 )
      parameter ( TOL = 1.0d-4 )

!------------------------C
! Geometry
!------------------------C
      double precision  cartN(4,3)
      double precision  cartP(4,3)

!------------------------C
! Unit Vectors
!------------------------C
      double precision  uA(3),uB(3),uC(3)
      double precision  uT(3)
      double precision  uBA(3),uCA(3),uCB(3)

!------------------------C
! Eq. of Plane
!------------------------C
      double precision  a1
      double precision  a2
      double precision  a3
      double precision  d
      double precision  dist

!------------------------C
! Misc
!------------------------C
      integer i,j,k,n,m
      double precision  xt,yt,zt,w
      double precision  b(3,3)
      double precision  a(3,3)

!=====================================================================
! Get data from user

!---------------------------------------------------------------------
! Now we will evaluate the direction of the trisector vector 
! the unit vectors along Ha, Hb and Hc bonds are labelled as 
! uA, uB, and uC. The trisector direction is refered as uT 

! cartN has the origin on the N atom
      do i=1,4
        cartN(i,1) = x(3*i-2) - x(1)
        cartN(i,2) = x(3*i-1) - x(2)
        cartN(i,3) = x(3*i)   - x(3)
      end do

! Get the bond distances
      do i=2,4
       r(i-1) = dsqrt( cartN(i,1)**2 + cartN(i,2)**2 + cartN(i,3)**2 )
      enddo

! get the units vectors along Ha, Hb, and Hc
      do i=1,3
        uA(i) = cartN(2,i)/r(1)
        uB(i) = cartN(3,i)/r(2)
        uC(i) = cartN(4,i)/r(3)
      enddo
!
! The trisector direction satisfies the following eq 
! (by definition of it being trisector)
! uT . uA = cos(beta)
! uT . uB = cos(beta)
! uT . uC = cos(beta)
!
! These three equation can be modified to:
! uT . (uB-uA) = 0
! uT . (uC-uA) = 0
! uT . (uC-uB) = 0
!
      do i=1,3
        uBA(i) = uB(i)-uA(i)
        uCA(i) = uC(i)-uA(i)
        uCB(i) = uC(i)-uB(i)
      enddo
!
! Find the eq of the plane defined by 
! points uBA, uCA, and uCB.
! The normal to that plane is the trisector
! directions 
!
! The eq of a plane 
!		Ax + By + Cz + D = 0
! defined by points:
! (x1,y1,z1) (x2,y2,z2) and (x3,y3,z3) is 
!
!     | 1 y1 z1 |      | x1 1 z1 |      | x1 y1 1 |	  | x1 y1 z1 |
! A = | 1 y2 z2 |  B = | x2 1 z2 |  C = | x2 y2 1 |  D = -| x2 y2 z2 |
!     | 1 y3 z3 |      | x3 1 z3 |      | x3 y3 1 |       | x3 y3 z3 |
!
!
! The eq of the normal to the plane is given by the vector {A,B,C}
!
      do i=1,3
        a(1,i) = uBA(i)
        a(2,i) = uCA(i)
        a(3,i) = uCB(i)
      enddo
      do i=1,3
        do j=1,3
        do k=1,3
          b(j,k) = a(j,k)
        enddo
        enddo
        do j=1,3
          b(j,i) = 1.0d0
        end do
        call det3(b,uT(i))
      end do

      w = dsqrt( uT(1)*uT(1) + uT(2)*uT(2) + uT(3)*uT(3) )
      do i=1,3
        uT(i) = uT(i) / w
      enddo

! Get the value of  beta (angle between any H-N and {A,B,C}) 
      w =  uT(1)*uA(1) + uT(2)*uA(2) + uT(3)*uA(3)
      beta(1) = dacos( w ) * 180.0d0/PI
!	beta(2) = dacos( dot_product(uT,uB) ) * 180.0d0/PI
!	beta(3) = dacos( dot_product(uT,uC) ) * 180.0d0/PI

!---------------------------------------------------------------------C
! Now we define the eq. of the plane
!	a1x + a2y + a3z + d = 0
! such that the normal to the plane is uT and 
! the Ha hydrogen lies on that plane
!
! The values of {a1,a2,a3} are the same as
! the values of uT(1),uT(2),uT(3). We will use
! the coordinates of Ha to find the value of d
!
      a1 = uT(1)
      a2 = uT(2)
      a3 = uT(3)
      w =  uT(1)*cartN(2,1) + uT(2)*cartN(2,2) + uT(3)*cartN(2,3)
      d = -w

!---------------------------------------------------------------------C
! Project all the atoms on the plane
! The projected geometry is known as cartP
! 
! The project of a point A on the plane P is denoted by point B
! The relation between rA and rB is given as :
! rB = rA - (dist*uT)
! where dist is the distance of point A from the plane.
!
      do i=1,4
        xt = cartN(i,1)
        yt = cartN(i,2)
        zt = cartN(i,3)
        call distPlane(a1,a2,a3,d,xt,yt,zt,dist)
        do j=1,3
          cartP(i,j) =  cartN(i,j) - dist*uT(j)
        enddo
      end do
      
      do i=2,4
        do j=1,3
          cartP(i,j) = cartP(i,j) - cartP(1,j)
        enddo
      end do

      do i=1,3
      cartP(1,i) = 0.0d0
      enddo
!---------------------------------------------------------
! The theta are labelled as:
! H1-N-H2 : theta(1)
! H1-N-H3 : theta(2)
! H2-N-H3 : theta(3)
!
! Note that theta <= 180 because we are using acos
! and acos gives angle in the range [-PI,PI]
! Also all the angles are +ve
!	

      k = 0
      do j=2,4
      do i=j+1,4
        k = k + 1
        xt = dsqrt( cartP(i,1)**2+cartP(i,2)**2+cartP(i,3)**2 )
        yt = dsqrt( cartP(j,1)**2+cartP(j,2)**2+cartP(j,3)**2 )
        zt = cartP(i,1)*cartP(j,1) + cartP(i,2)*cartP(j,2) +   &
            cartP(i,3)*cartP(j,3)
        w = zt/(xt*yt)
        if( w .gt.  1.0d0) w =  1.0d0
        if( w .lt. -1.0d0) w = -1.0d0
        theta(k) =  dabs(dacos(w)) * 180.0d0/PI
      end do
      end do
!---------------------------------------------------------
! Now we we will relabel theta such that
! H1-N-H2 : theta(3)
! H1-N-H3 : theta(2)
! H2-N-H3 : theta(1)
      xt = theta(1)
      theta(1) = theta(3) 
      theta(3) = xt
!---------------------------------------------------------
! all theta should be +ve and <= 180
!    do k=1,3
!        if(theta(k) < 0.0d0) theta(k) = -theta(k)
!        Cif(theta(k) > 180.0d0) theta(k) = theta(k) - 180.0d0
!    end do
    
      bow(1) = r(1)
      bow(2) = r(2)
      bow(3) = r(3)
      bow(4) = theta(1)
      bow(5) = theta(2)
      bow(6) = theta(3)
      bow(7) = beta(1)

      END subroutine toBowman

!C********************************************************!!
      SUBROUTINE distPlane(a1,a2,a3,d,x,y,z,dist)
!C********************************************************!!
      implicit none
      
      double precision a1
      double precision a2
      double precision a3
      double precision d
      double precision x
      double precision y
      double precision z
      double precision dist
!
! This subroutine calculates the dist between the 
! point (x,y,z) and the plane (a1x+a2y+a3z+d=0)
!
! dist = a1x + a2y + a3z + d
!		----------------------
!		sqrt(a**2+b**2+c**2)

      dist = ( (a1*x)+(a2*y)+(a3*z)+d ) /    &
         dsqrt( (a1**2) + (a2**2) + (a3**2) )

      END SUBROUTINE distPlane

!C********************************************************!!
       SUBROUTINE det3(a,ans)
!C********************************************************!!
      implicit none
      double precision a(3,3)
      double precision ans
!
! Compute determinant of a 3x3 matrix
!
      double precision x,y,z

      x = a(1,1)*( (a(2,2)*a(3,3))-(a(2,3)*a(3,2)) )
      y = a(1,2)*( (a(2,1)*a(3,3))-(a(2,3)*a(3,1)) )
      z = a(1,3)*( (a(2,1)*a(3,2))-(a(2,2)*a(3,1)) )

      ans = x - y + z

      END SUBROUTINE det3
!C******************************************!!
      subroutine carttoang(cart,ans)
!C******************************************!!
!
! This function converts cartesian coordinates
! to internal coordinates (dist and bond angles) for 4 atom system ONLY
!
! Cartesian coordinate format: x1,y1,z1,x2,y2,z2....xn,yn,zn
! N  : cart(1-3)
! H1 : cart(4-6)
! H2 : cart(7-9)
! H3 : cart(10-12)
!
! Dist in Ang. Angles in degrees
! Internal coordinate format:
! H1-N-H2 : theta3
! H1-N-H3 : theta2
! H2-N-H3 : theta1
!
![r1,r2,r3,theta1,theta2,theta3]
!-----------------------------------------------------
      implicit none

! I/O variables
      double precision PI
      parameter ( PI = 3.1415926535897930d0 )
      double precision cart(12)

! Local variables
      integer i,j,k
      double precision r(3),t(3)
      double precision theta(3)
      double precision cartH(3,3)
      double precision ans(7)

! Put N at the origin and 
! and get N-H bond vectors
      k=0
      do i=2,4
        k=k+1
        cartH(k,1)=cart(3*i-2)-cart(1) 
        cartH(k,2)=cart(3*i-1)-cart(2) 
        cartH(k,3)=cart(3*i  )-cart(3) 
      end do
 
! calculate bond distances
      do i=1,3
        r(i)=dsqrt(cartH(i,1)*cartH(i,1)+cartH(i,2)*   &
        cartH(i,2)+cartH(i,3)*cartH(i,3) )
      end do
!------------------------------------------------------
! calculate bond angles
!
      t(1) = (cartH(2,1)*cartH(3,1)+cartH(2,2)*cartH(3,2)+  &
              cartH(2,3)*cartH(3,3))/(r(2)*r(3))
      t(2) = (cartH(1,1)*cartH(3,1)+cartH(1,2)*cartH(3,2)+  &
              cartH(1,3)*cartH(3,3))/(r(1)*r(3))
      t(3) = (cartH(1,1)*cartH(2,1)+cartH(1,2)*cartH(2,2)+  &
              cartH(1,3)*cartH(2,3))/(r(1)*r(2))
      do i=1,3
!        by definition 0 <= dacos(t) <= PI
         theta(i) =  dacos( t(i) )
!        convert to degrees
         theta(i) = theta(i)*180.0d0/PI
      end do

! return answer
      do i=1,3
        ans(i)   = r(i)
        ans(3+i) = theta(i)
      enddo

      END subroutine carttoang

!*******************************************!
      subroutine cart_to_polar(x,ans)
!*******************************************!
      implicit none
      double precision x(3)
      double precision ans(3)
      double precision PI
      parameter ( PI = 3.1415926535897930d0 )
!
! ans(1) : r
! ans(2) : phi (0-2PI)  !in degrees
! ans(3) : theta (0-PI) !in degrees
!
      double precision  t

      ans(1) = dsqrt( x(1)*x(1) + x(2)*x(2) + x(3)*x(3) )
      if (ans(1).ne.0.d0) then
        t      = dacos(x(3)/ans(1))      !angle is between [-PI,PI]
        ans(3) = dasin(dsin(t))          !angle is between [0,2PI]

        if( x(1) .ne. 0.0d0 ) then
          ans(2) = datan(x(2)/x(1))
        else 
          ans(2) = 0.50d0 * PI
          if (x(2) .eq. 0.0d0) ans(2) = 0.0d0
        end if

! Convert to degrees
        ans(2) = ans(2) * 180.0d0/PI
        ans(3) = ans(3) * 180.0d0/PI
      else
        ans(2) = 0.d0
        ans(3) = 0.d0
      endif

      END subroutine cart_to_polar

!*******************************************!
      subroutine polar_to_cart(p,ans)
!*******************************************!
      implicit none
      double precision p(3)
      double precision ans(3)
      double precision PI
      parameter ( PI = 3.1415926535897930d0 )
!
! p(1) : r
! p(2) : phi (0-2PI)  !in degrees
! p(3) : theta (0-PI) !in degrees
!
      double precision  phi,theta
      double precision  sinth

      phi = p(2) * PI/180.0d0
      theta = p(3) * PI/180.0d0

      sinth = dsin(theta)

      ans(1) = p(1) * dcos(phi) * sinth
      ans(2) = p(1) * dsin(phi) * sinth
      ans(3) = p(1) * dcos(theta)

      END subroutine polar_to_cart

!*********************************************!
      subroutine fromBowman(bow,coord)
!*********************************************!
      implicit none
      double precision  bow(7)
      double precision  coord(12)
      double precision PI
      parameter ( PI = 3.1415926535897930d0 )
!
! This function returns the cartesian 
! coordinates from bowman coordinates for 4 atom systems ONLY
! All angles are in DEGREES
!
! Input:
! bow(1) : r1
! bow(2) : r2
! bow(3) : r3
! bow(4) : theta1
! bow(5) : theta2
! bow(5) : theta3
! bow(7) : beta
!
! Output: 
! coord(1:3)   : N   x,y,z
! coord(4:6)   : H1  x,y,z
! coord(7:9)   : H2  x,y,z
! coord(10:12) : H3  x,y,z
!
      double precision r1,r2,r3
      double precision theta1,theta2,theta3
      double precision phi1,phi2,phi3
      double precision beta
      double precision ph1(3)
      double precision ph2(3)
      double precision ph3(3)
      double precision  ans(3)
      integer i,j
      double precision  small
      small = 1.d-3

! define stuff
      r1 = bow(1)
      r2 = bow(2)
      r3 = bow(3)
      theta1 = bow(4)
      theta2 = bow(5)
      theta3 = bow(6)
      beta = bow(7)
!
! z-axis is the trisector vector
! G1, G2, G3 are the projection of H1, H2 and H3
! in the xy plane
! 
!  G2
!   \
!    N--G1-------> x-axis
!   /
!  G3
!
! Angle definitions:
! G1-N-G2 : theta3
! G1-N-G3 : theta2
! G2-N-G3 : theta1
!

! define phi 
      phi1 = 0.0d0
      phi2 = theta3
! the following line (Shikha) is correct only when the sum of
! the three theta is 360.0
!      phi3 = theta1+theta3
      if (dabs(360.d0-(theta1+theta2+theta3)).lt.small) then
        phi3 = theta1+theta3
      elseif (dabs(theta1-theta2-theta3).lt.small) then
        phi3 = 360.d0-theta2
      elseif (dabs(theta2-theta1-theta3).lt.small) then
        phi3 =  theta2
      elseif (dabs(theta3-theta1-theta2).lt.small) then
        phi3 =  theta2
      else
        write(6,*) 'Is this geometry possible?'
        write(6,*) bow
        stop
      endif

! vector for H1 in polar
      ph1(1) = r1       
      ph1(2) = phi1     !in degrees
      ph1(3) = beta     !in degrees

! vector for H2 in polar
      ph2(1) = r2
      ph2(2) = phi2   !G2 makes theta2 with x-axis
      ph2(3) = beta

! vector for H3 in polar
      ph3(1) = r3
      ph3(2) = phi3   !G3 makes theta2+theta3 with x-axis
      ph3(3) = beta

! convert to cartesian vectors
! N is at the origin
      coord(1) = 0.0d0
      coord(2) = 0.0d0
      coord(3) = 0.0d0
! H1
      call polar_to_cart(ph1,ans)
      do i=1,3
        coord(3+i) = ans(i)
      enddo
! H2
      call polar_to_cart(ph2,ans)
      do i=1,3
        coord(6+i) = ans(i)
      enddo
! H3
      call polar_to_cart(ph3,ans)
      do i=1,3
        coord(9+i) = ans(i)
      enddo

      END subroutine fromBowman

!!************************************************************!!
      subroutine angle_to_cart(angint,coord)
!!************************************************************!!
!
! Convert Valence Internal Coordinates (VIC)
! to cartesian coordinates. for 4 atom systems only
!
!
! Cartesian coordinate format:
! N  : cart(1:3)
! H1 : cart(4:6)
! H2 : cart(7:9)
! H3 : cart(10:12)
!
! Dist in Ang. Angles in degrees
! Internal coordinate format:
! H1-N-H2 : theta3
! H1-N-H3 : theta2
! H2-N-H3 : theta1
!
![r1,r2,r3,theta1,theta2,theta3]
!
      implicit none
!--------------------
! I/O variables
!--------------------
      double precision  angint(7)
      double precision  coord(12)
!--------------------
! Local variables
!--------------------
      double precision  r1
      double precision  r2
      double precision  r3
      double precision  theta1
      double precision  theta2
      double precision  theta3
      double precision  uH3(3)
!--------------------
! Misc
!--------------------
      double precision  degtopi
      parameter (  degtopi = 3.1415926535897930d0/180.d0 )
      double precision  x
      double precision  costh1,costh2,costh3,sinth3
      double precision  small
      parameter (small = 1.d-10)
      integer i

! initialize
        r1 = angint(1)
        r2 = angint(2)
        r3 = angint(3)
        theta1 = angint(4)*degtopi
        theta2 = angint(5)*degtopi
        theta3 = angint(6)*degtopi
        do i = 1,12
          coord(i) = 0.0d0
        enddo
        costh2 = dcos(theta2)
        costh3 = dcos(theta3)
        sinth3 = dsin(theta3)

! N is at origin

! H1 is along x
        coord(4) = r1
        
! H2 is in the xy plane
        coord(7) = r2*costh3
        coord(8) = r2*sinth3

! H3 is in the xyz p
! If H2 along x: sin(theta3) = 0), put H3 on xz plane
! theta3 = 0   theta1=theta2
! theta3 = PI  theta1=PI-theta2
! unit vector along the N-H3 bond (uH3)
      uH3(1) = costh2
      if ( dabs(sinth3).lt.small ) then
        uH3(2) = 0.d0
        uH3(3) = dsin(theta2)
      else
        costh1 = dcos(theta1)
        uH3(2) = ( costh1 - costh2*costh3 )/sinth3
        x = uH3(1)**2 + uH3(2)**2
        if( x .le. 1.d0) then
                uH3(3) = dsqrt(1.0d0-x)
!  Due to numerical errors, x may be just marginally larger than 1.d0
!  (planar structure)
        elseif( (x-1.d0) .le. small) then
              uH3(3) = 0.d0
        else
           write(6,*)'ERROR: Set of three bond angles is unphysical'
           write(6,*) r1,r2,r3,theta1/degtopi,               &
            theta2/degtopi,theta3/degtopi
           stop
        endif
      endif
      coord(10) = r3 * uH3(1)
      coord(11) = r3 * uH3(2)
      coord(12) = r3 * uH3(3)

      END subroutine angle_to_cart

!--------------------------------------------
      subroutine rarray(natoms,x,mx,r,mr)
C calculate the rij array N(N-1)/2 elements
! x:  x1,y1,z1,...,xn,yn,zn
      implicit none

      integer natoms
! dimension of x and r, must be larger than 3*natoms and
! natoms*(natoms-1)/2 respectively
      integer mx,mr
      double precision x(mx),r(mr)

      integer i,j,k,ii,ij
      double precision dx,dy,dz

      k = 0
      do i = 1,natoms
        ii = 3*(i-1)
      do j = i+1,natoms
        k  = k+1
        ij = 3*(j-1)
        dx = x(ij+1) - x(ii+1)
        dy = x(ij+2) - x(ii+2)
        dz = x(ij+3) - x(ii+3)
        r(k) = dsqrt( dx*dx + dy*dy + dz*dz )
      enddo
      enddo

      end subroutine rarray

!--------------------------------------------
      subroutine rmtr0(x,r,natoms,maxatom)
! calculate the distance matrix N(N-1)/2 elements
! x(3,maxatom)  
      implicit none

      integer natoms
! dimension of x and r, must be larger than 3*natoms and
! natoms*(natoms-1)/2 respectively
      integer natoms,maxatom
      double precision x(3,maxatom),r(maxatom,maxatom)

      integer i,j
      double precision dx,dy,dz

      do i = 1,natoms
      r(i,i) = 0.d0
      do j = i+1,natoms
        dx = x(1,i) - x(1,j)
        dy = x(2,i) - x(2,j)
        dz = x(3,i) - x(3,j)
        r(i,j) = dsqrt( dx*dx + dy*dy + dz*dz )
        r(j,i) = r(i,j)
      enddo
      enddo

      end subroutine rmtr0

      subroutine rcompare(r1,r2,ndim,mxdim,dr,lsame)
! Compare two rij arrays
      implicit none
      integer ndim,mxdim
      double precision r1(mxdim),r2(mxdim),dr
! dr :     if less than this  the two distances are the same
! r12:     typical bond distance of two atoms in the molecule in A
      double precision r12,rmin
      parameter (r12 = 2.70d0)
      parameter (rmin= 2.d0*r12)
      logical lsame
      integer i,j,k
      lsame = .true.

      do i = 1,ndim
! Do not compare long rij 
        if ( r1(i).le.rmin .and. r2(i) .le. rmin) then
          if (dabs(r1(i)-r2(i)).gt.dr) then
             lsame = .false.
             exit
          endif
        endif
      enddo

      end subroutine rcompare

      subroutine three2one1(xx,p,natoms,maxatom,ido)
! Convert xx(3,i) to p (x1,y1,z1,...,xNatoms,yNatoms,zNatoms)
! ido:   1: from xx to p; else: from p to xx
      implicit none
      integer natoms,maxatom,ido
      double precision xx(3,maxatom),p(3*maxatom)
      integer i,j,ii

      if (ido.eq.1) then
        do i = 1,natoms
          ii = 3*i
          p(ii - 2) = xx(1,i)
          p(ii - 1) = xx(2,i)
          p(ii )    = xx(3,i)
        enddo
      else
        do i = 1,natoms
          ii = 3*i
          xx(1,i)   = p(ii - 2)
          xx(2,i)   = p(ii - 1)
          xx(3,i)   = p(ii )   
        enddo
      endif

      end subroutine three2one1

      subroutine three2one2(xx,p,natoms,maxatom,ido)
! Convert xx(3,i) to p (x1,...,xNatoms,...,z1,...,zNatoms)
! ido:   1: from xx to p; else: from p to xx
      implicit none
      integer natoms,maxatom,ido
      double precision xx(3,maxatom),p(3*maxatom)
      integer i,j,ii

      if (ido.eq.1) then
        do j = 1,3
        ii = natoms*(j-1)
        do i = 1,natoms
          p(ii + i) = xx(j,i)
        enddo
        enddo
      else
        do j = 1,3
        ii = natoms*(j-1)
        do i = 1,natoms
          xx(j,i)   = p(ii + i)
        enddo
        enddo
      endif

      end subroutine three2one2
