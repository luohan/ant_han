      subroutine diapot(x,im,v,xj,mm,nsurf)

! Compute the diatomic energy for molecular arrangement
! number IM. Specialized for atom-diatom initial conditions.

      use param, only: mnat, mnsurf
      use c_struct, only: symbol0, cell, indmm, ldiat
      use c_sys, only: repflag 

      implicit none
      integer :: im,j,nsurf,i,icall,ii
      double precision :: v,xx(3,mnat),r(3),xj,erot,rmass,x,mm(mnat)
      character*2 :: symb(mnat)

      double precision :: pemd(mnsurf,mnsurf),gpemd(3,mnat,mnsurf,mnsurf),  &
                          pema(mnsurf),gpema(3,mnat,mnsurf), &
                          dvec(3,mnat,mnsurf,mnsurf)

! RMP14 
! Array  (local variable: im):
! 1  AB
! 2  BC
! 3  AC
! 4  AD
! 5  BD
! 6  CD

!
! changes made to run direct dynamics
! Jingjing Zheng Nov. 2, 2012
!     ii = 0
!     do i=1,natoms
!       ii = iatom(im)+i-1
!       symb(i) = symbol0(ii)
!     enddo
! end of direct dynamics changes 
!

      r(1) = 100.0d0
      r(2) = 100.0d0
      r(3) = 100.0d0
      r(im) = x
      call rtox(xx,r,0)
! RMP14
! Calls specific diatomic subroutine
      if(ldiat) then
        call diapot_int(r,im,nsurf,v)
      else
        call getpem(xx,indmm,symbol0,cell,3,pema,pemd,gpema,gpemd,dvec,mm,icall,nsurf)
        if (repflag.eq.1) then
          v = pemd(nsurf,nsurf)
        else
          v = pema(nsurf)
        endif
      endif


! add rotation if j > 0
      if (xj.ge.0) then
      if (im.eq.1) rmass = mm(1)*mm(2)/(mm(1)+mm(2))
      if (im.eq.2) rmass = mm(2)*mm(3)/(mm(2)+mm(3))
      if (im.eq.3) rmass = mm(3)*mm(1)/(mm(3)+mm(1))
      erot = 0.5d0*xj*(xj+1.0d0)/(rmass*x**2)
      v = v + erot
      endif

      end subroutine diapot

! This subroutine should be replace the the corresponding subroutine
! to compute the diatomic potential. It would be more convenient comment
! out this lines and use the subroutine included the the PES.
!      subroutine diapot_int(r,im,nsurf,v)
!      implicit none
!xxx      double precision r,v
!      double precision :: r(3),v
!      integer :: im,nsurf
! r: interatomic distance in a.u.
! v: potential in hartree
! im:
! Array  (local variable: im):
! 1  AB | A+BC | AB+CD
! 2  BC |      |
! 3  AC |      |
! 4  AD        |
! 5  BD        |
! 6  CD        |
!      end subroutine diapot_int
