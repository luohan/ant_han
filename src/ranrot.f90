      subroutine ranrot(rotaxism,iangle)
! This subroutine generates a random rotational transform matrix. 
! Ref. A.R. Leach: Molecular modelling - Principles and applications,
!      2nd Ed. Page 420-422. Chapter 8.5 Monte Carlo Simulation of Molecules
!     Vesely F.J. Angular Monte Carlo Integration Using Quanternion
!     Parameters: A Sphereical References Potential for CCl4. J. Comp.
!     Phys. 1982, 47, 291-296
! Generate four random numbers with their sum equals 1. Build a matrix
! using these four random numbers
! rotaxism(3,3):  Rotational transformation matrix
! q3:     The four random numbers generated (will be used in the
!                 later integration of reaction cross section.
! ranm(4):        Local storage of four random numbers
! iangle:         true: directly change on angles, except on cos(theta)
!                 0: quaterion
! cs1             cos(s1)
! ss1             sin(s1)
      use param, only: pi
      implicit none
      logical iangle
      double precision rotaxism(3,3),q0,q1,q2,q3
!
      integer i,j
      double precision s1,s2,s3,cs1,ss1,cs2,ss2,cs3,ss3,ranm(4)
!
      if (.not.iangle) then
        s1=2.0d0
        s2=2.0d0
!     generate two random numbers(-1,1) with sum of square < 1
        do while(s1.ge.1.0d0)
          call get_rng(ranm(1))
          call get_rng(ranm(2))
          ranm(1) = -1.0+2.0*ranm(1)
          ranm(2) = -1.0+2.0*ranm(2)
          s1 = ranm(1)**2 + ranm(2)**2
        enddo
!     generate another two random numbers with sum of square < 1
        do while(s2.ge.1.0d0 .or. s2.eq.0.d0)
          call get_rng(ranm(3))
          call get_rng(ranm(4))
          ranm(3) = -1.0+2.0*ranm(3)
          ranm(4) = -1.0+2.0*ranm(4)
          s2 = ranm(3)**2 + ranm(4)**2
        enddo
        q0 = ranm(1)
        q1 = ranm(2)
        q2 = ranm(3)*dsqrt( (1-s1)/s2 )
        q3 = ranm(4)*dsqrt( (1-s1)/s2 )
!     Construction the transformation matrix
        rotaxism(1,1)=q0**2 + q1**2 - q2**2 - q3**2
        rotaxism(1,2)=2.d0*(q1*q2+q0*q3)
        rotaxism(1,3)=2.d0*(q1*q3-q0*q2)
!
        rotaxism(2,1)=2.d0*(q1*q2-q0*q3)
        rotaxism(2,2)=q0**2 - q1**2 + q2**2 - q3**2
        rotaxism(2,3)=2.d0*(q2*q3+q0*q1)
!
        rotaxism(3,1)=2.d0*(q1*q3+q0*q2)
        rotaxism(3,2)=2.d0*(q2*q3-q0*q1)
        rotaxism(3,3)=q0**2 - q1**2 - q2**2 + q3**2
!     Change on angles s1(theta): 0-pi (angle to +z), s2(phi),
!     s3(posi): 0-2pi
      else
        do i=1,3
          call get_rng(ranm(i)) 
        enddo
!     Three angles: uniform on cos(theta)
        cs1 = 1.d0-2.d0*ranm(1)
        s1 = dacos(cs1)
        s2 = 2.d0*pi*ranm(2)
        s3 = 2.d0*pi*ranm(3)
!     cos and sin of angles s1,s2,s3
        ss1 = dsin(s1)
        cs2 = dcos(s2)
        ss2 = dsin(s2)
        cs3 = dcos(s3)
        ss3 = dsin(s3)
!
        rotaxism(1,1)= cs2*cs3 - ss2*cs1*ss3
        rotaxism(1,2)= ss2*cs3 + cs2*cs1*ss3
        rotaxism(1,3)= ss1*ss3
!      
        rotaxism(2,1)=-cs2*ss3 - ss2*cs1*cs3
        rotaxism(2,2)=-ss2*ss3 + cs2*cs1*cs3
        rotaxism(2,3)= ss1*cs3
!      
        rotaxism(3,1)= ss2*ss1
        rotaxism(3,2)=-cs2*ss1
        rotaxism(3,3)= cs1
!      
      endif
!  Debug information print
!      do j=1,3
!        write(6,*) (rotaxism(j,i),i=1,3)
!      enddo
 
      end subroutine ranrot
