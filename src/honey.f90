      subroutine honey(itmp,time)
! see Honeycut & Anderson J. Phys. Chem. 91, 4950 (1987).

      use param, only: mnat, autofs, autoang
      use c_output, only: lwrite
      use c_struct, only: rij, natoms

      implicit none
      integer, parameter :: nmax=10
      integer :: i,j,k,ii,ind1,ind2,ind3,isav(nmax), &
                 isav2(mnat),k1,k2,nhit,itmp
      double precision :: rcut,time, &
                          haparam(0:nmax,0:nmax,0:nmax),tmph(10)

! cutoff for first shell
      rcut = 3.5d0/autoang

      do i=0,nmax
      do j=0,nmax
      do k=0,nmax
        haparam(i,j,k) = 0.d0
      enddo
      enddo
      enddo
      nhit = 0
      do i=1,natoms
        do j=i+1,natoms
          ind1 = 0
          ind2 = 0
          ind3 = 0
          do ii=1,nmax
            isav(ii)=0
          enddo
          do ii=1,natoms
            isav2(ii)=0
          enddo
! Find out at most nmax atoms fallen within rcut reach of both i and j,
! counted by ind1, save the index into isav
          if (rij(i,j).le.rcut) then
            do 10 k=1,natoms
              if (k.eq.i.or.k.eq.j) go to 10
! Sth must be wrong here
!              do ii=1,3
!                rik = rik + (xxm(ii,i)-xxm(ii,k))**2
!                rjk = rjk + (xxm(ii,j)-xxm(ii,k))**2
!              enddo
              if (rij(j,k).le.rcut.and.rij(i,k).le.rcut  &
                  .and.ind1.le.nmax) then
                ind1 = ind1 + 1
                isav(ind1)=k
              endif
  10        continue
!  In these ind1 atoms, find out pair distances less than rcut, count
!  them by ind2, count how many nearest atoms around each atom. Save the
!  nearest neighborhood numbers into isav2.
            do k1=1,ind1
            do k2=k1+1,ind1
!              rkk = 0.d0
!              do ii=1,3
!                rkk = rkk + (xxm(ii,isav(k1))-xxm(ii,isav(k2)))**2
!              enddo
              if (rij(isav(k1),isav(k2)).le.rcut) then
                ind2 = ind2 + 1
                isav2(k1) = isav2(k1) + 1
                isav2(k2) = isav2(k2) + 1
              endif
            enddo
            enddo
!  Find out an atom with nearest neighborhood number >=2
            ind3 = 1
            do k1=1,ind1
              if (isav2(k1).ge.2) ind3 = 2
            enddo
            nhit = nhit + 1
! lizh: strange. ind1.ge.0 and ind2.ge.0.and.ind3.ge.0 always true.
! lizh: ind1 .le. nmax always true. 
! lizh: ind2 .le. ind1*(ind1-1)/2, thus, .le. nmax*(nmax-1)/2
! lizh: ind3 .eq. 1 or 2, thus ind3.le.nmax always true.
!            if (ind1.ge.0.and.ind2.ge.0.and.ind3.ge.0) then
!            if (ind1.le.nmax.and.ind2.le.nmax.and.ind3.le.nmax) then
!            haparam(ind1,ind2,ind3) = haparam(ind1,ind2,ind3) + 1.d0
!            endif
!            endif
            if (ind2.le.nmax) then
              haparam(ind1,ind2,ind3) = haparam(ind1,ind2,ind3) + 1.d0
            endif
          endif
        enddo
      enddo

!      do i=0,nmax
!      do j=0,nmax
!      do k=0,nmax
!        if (haparam(i,j,k).gt.0.d0) write(45,100)i,j,k,  &
!       haparam(i,j,k)/dble(nhit)
!      enddo
!      enddo
!      enddo
!      write(45,*)
! 100  format(3i5,f15.5)

       tmph(1) = 0.d0
       tmph(2) = 0.d0
       tmph(3) = 0.d0
       tmph(4) = 0.d0
       tmph(5) = 0.d0
       do i=0,nmax
         tmph(1) = tmph(1) + haparam(2,i,1) + haparam(2,i,2)
         tmph(2) = tmph(2) + haparam(3,i,1) + haparam(3,i,2)
         tmph(3) = tmph(3) + haparam(4,i,1) + haparam(4,i,2)
         tmph(4) = tmph(4) + haparam(5,i,1) + haparam(5,i,2)
         tmph(5) = tmph(5) + haparam(6,i,1) + haparam(6,i,2)
       enddo
       tmph(6) = haparam(4,2,1)
       tmph(7) = haparam(4,2,2)

       if (lwrite(42)) write(42,101)itmp,time*autofs,  &
       (tmph(k)/dble(nhit),k=1,7)
 101   format(i7,100f10.3)

      end subroutine honey
