      module tpotmod
! this module stores parameters needed to obtain function results for the Brent
! zero finder used to obtain the final turning point position 
      use param, only: mnat
      integer, save :: if1, itsearch(2)
      double precision, save :: vturn, xlast(3,mnat), xnow(3,mnat), drrpass(3*mnat), &
                          din(2), scale
      end module tpotmod
!---------------------------------

      subroutine actionint(ithistraj,xturn,pturn,itint,drr,nstr,nbend, &
                           turnd,lbranch,im,wght,lchgw,xnew,pnew)
!
! This subroutine calculates the imaginary action integral for tunneling
!      
      use param, only: mnat, mnsurf, autoang, mu, pi
      use c_struct, only: icharge, imulti, nat, mmm, symbol0
      use c_traj, only: eta, gw, node, ngauss, ngauss, pe
      use c_output, only: time_brent, time_pgen
      use tpotmod

      implicit none
      integer :: nstep,im,nchunk,ncstep,ntotc,ithistraj, iflag, n_waypts
      integer :: i,j,k,l,n,npoint,nstr,nbend,nd, iii, npointx
      integer :: itint(2)
      double precision :: ke_old, ke_new !debugging variables
      double precision :: xturn(3,mnat),pturn(3,mnat),turnd(3,mnat), time1, time2
      double precision :: xx(3,mnat),xnew(3,mnat),pnew(3,mnat),xmid(3,mnat)
      double precision :: xprev(3,mnat), xstash(3,mnat)
      double precision :: xtemp(3,mnat), ptemp(3,mnat)
      double precision :: xtemp2(3,mnat), ptemp2(3,mnat)
      double precision :: lambda,v0,dxi,theta,dv, dv_old
      double precision :: probt,wght,gamma,direction,lambda2,ram,pam,dist
      double precision :: bigjm(3),com(3),compp(3),mtotal,totcomp,dmag
      double precision :: din1(2),dinprev(2),drr(3*mnat), turn2
      double precision, external :: brent, tpot
      logical :: lbranch,lchgw,lterm
! local storage for PES
!     double precision :: pema(mnsurf),pemd(mnsurf,mnsurf), &
!                         gpema(3,mnat,mnsurf),gpemd(3,mnat,mnsurf,mnsurf), &
!                         dvec(3,mnat,mnsurf,mnsurf)
      integer :: jcharge,jmulti
      logical :: langle 
! profile array for isoinertial coordinates
      double precision, allocatable :: len_iso(:),xs_iso(:,:,:), waypt(:,:,:)
      double precision :: leng,len_max,len_node,liso1,liso2,lint1,lint2

! Gauss-legendre quadrature
!     double precision  :: node(ngauss),gw(ngauss)
      
! save the potential of the turning point to v0

      v0 = pe
      lbranch = .false.
      lchgw   = .false.
      din(:)    = 0d0
      din1(:)   = 0d0
      dinprev(:) = 0d0
      if(itint(2).ne.0) then
        call comparint(itint,xturn,lterm)
        if(.not.lterm) return
      endif
!
! Search for the end point of the tunneling path along an internal coordinate
!     if(itint.le.nstr) then
      if((itint(1).le.nstr.and.itint(2).eq.0).or.itint(2).ne.0) then
        dxi    =  2d-3 ! in the unit of a.u. for length
        langle = .false.
        ntotc  =  38
        ncstep =  100 
      else
!xxx        dxi    = 0.1d0*PI/180d0 ! in the unit radian for angles  
!xxx        dxi    = 0.07d0*PI/180d0 ! in the unit radian for angles  
        dxi    = 0.1d0*PI/180d0 ! in the unit radian for angles  
        langle = .true.
        ntotc  =  18
        ncstep = 100
      endif

      if(.not.allocated(len_iso)) then
        allocate(len_iso(0:ntotc*ncstep),xs_iso(3,mnat,ncstep), waypt(3,mnat,400) )
      endif
!
! Now the predefined tunneling path length is ntotc*ncstep*dxi 
!
      nstep = 0    ! number of steps used for the path search
      theta = 0d0  ! action integral value
      if(im.gt.0) then
        jcharge=icharge(im)
        jmulti =imulti(im)
      else
        jcharge=999
        jmulti =999
      endif
! print tunneling direction
!     write(6,*) ' tunneling direction'
!     do i = 1, nat
!       write(6,'(1x,3f7.2)') (turnd(j,i),j= 1,3)  
!     enddo
!     stop
! determining the direction of tunneling path
      direction = 1d0
      din(1) = dxi*direction*drr(itint(1))
      if(itint(2).ne.0) din(2) = dxi*direction*drr(itint(2))
      call intcart(xturn,xx,nat,mmm,din,itint,1,lterm)
      if(lterm) stop 'failure in intcart'
      call getgrad2(xx,dmag)
      dv = pe -v0
      if(dv.lt.0d0.and.itint(1).gt.nstr+nbend.and.itint(2).eq.0) then 
         direction = -1d0
      elseif(dv.lt.0d0.and.itint(2).ne.0) then
         return
      elseif(dv.lt.0d0.and.itint(1).le.nstr+nbend.and.itint(2).eq.0)then
         return
      endif
!
! divide whole path into NTOTC pieces, each piece has ncstep steps
!
      nchunk = 0
      n_waypts=1
      waypt(:,:,1)=xturn(:,:)
      xprev(:,:) = xturn(:,:)
      ptemp(:,:)=pturn(:,:)
      xtemp(:,:)=xturn(:,:)
      npoint = 0
      len_iso(:) = 0d0
      din(1) = dxi*direction*drr(itint(1))
      if(itint(2).ne.0) din(2) = dxi*direction*drr(itint(2))
      do k = 1, ntotc
        n = 0
        xstash(:,:)=xprev(:,:)
        do j = 1, ncstep
          n  = n + 1
          npoint = npoint + 1
          call intcart(xprev,xx,nat,mmm,din,itint,1,lterm)
          if(lterm) stop 'failure in intcart'
          call cartoiso(xx,nat,mmm,1) ! change xx to isoinertial coordinate
          xs_iso(:,:,n) = xx(:,:)
          call cartoiso(xprev,nat,mmm,1) ! change previous point to isoinertial 
          call length(nat,xx,xprev,len_iso(npoint))
          len_iso(npoint) = len_iso(npoint) + len_iso(npoint-1)
          call cartoiso(xx,nat,mmm,2) ! change back to Cartesian 
          if(mod(j,10).eq.0)then  
            xtemp2(:,:)=xx(:,:)
            n_waypts=n_waypts+1
            if(n_waypts.gt.400)then
              stop 'too many waypts'
            endif
            waypt(:,:,n_waypts)=xx(:,:)
          endif
          xprev(:,:) = xx(:,:)
          if(j.eq.ncstep) then
            call getgrad2(xx,dmag)
            dv_old = dv
            dv = pe -v0
          endif
        enddo
        nchunk = nchunk + 1
        if(dv .lt. 0d0 ) exit
      enddo
      if (nchunk .eq. ntotc) return  ! the action integral is too large for tunneling, so return
!
!  Now determine the outer turning point
! 
      xlast(:,:)=xstash(:,:)
      if1=0
      drrpass(:)=drr(:)
      itsearch(:)=itint(:)
      scale=dxi*direction*ncstep
      vturn=v0
      call cpu_time(time1)
      if(npoint-ncstep.eq.0)then ! xlast is the inner turning point, so nudge forward
        turn2=brent(1d-10,1d0,tpot,1d-10,0d0,dv) 
      else
        turn2=brent(0d0,1d0,tpot,1d-10,dv_old,dv) 
      endif
      call cpu_time(time2)
      time_brent=time_brent+time2-time1
      pe=tpot(turn2)+vturn ! gets final set of coordinates and energy 
      iii=int(turn2*ncstep)
      npointx=npoint-ncstep+iii
      xnew(:,:)=xnow(:,:) ! store the coordinates of the outer turning point
      xtemp2(:,:)=xnow(:,:)
      call cartoiso(xtemp2,nat,mmm,1) ! get coordinates of outer turning point in isoinertial coordinates
      call length(nat,xtemp2,xs_iso(:,:,iii),len_max) ! calculate dist. between turning pt. and last point
      len_max=len_max+len_iso(npointx)

      npoint = npoint - ncstep
      npoint=npoint+iii

!xxx      do k = 1, ncstep
!xxx         npoint = npoint + 1
!xxx         xx(:,:) = xs_iso(:,:,k)
!xxx         call cartoiso(xx,nat,mmm,2) 
!xxx         call getgrad2(xx,dmag)
!xxx         dv = pe -v0
!xxx         if (dv.lt.0d0) then
!xxx           write(6,*)'step after outer turning point=',k
!xxx            exit
!xxx         endif
!xxx      enddo  
!xxx      len_max = len_iso(npoint)
!xxx      xnew(:,:) = xx(:,:)  ! ending point geometry 
!
! Gauss-Legendre quadrature for the integral in isoinertial coordinates
!
      j = 1
      xprev(:,:) = xturn(:,:)
      dinprev = 0d0
      nd = 10
      if(langle) nd = 50
      do k = 1, ngauss
         len_node = 0.5d0*(node(k)+1d0)*len_max
         do i = j, npoint
           if(len_node.le.len_iso(i)) exit
         enddo
         j = i - 1 
         liso2 = len_iso(i) 
         liso1 = len_iso(i-1)
! internal coordinate 1
         lint1 = dxi*dble(i-1)*direction*drr(itint(1))
         lint2 = dxi*dble(i)*direction*drr(itint(1))
         din(1) =((len_node-liso1)*lint2+(liso2-len_node)*lint1)/ &
              (liso2-liso1)
! internal coordinate 2
         if(itint(2).ne.0) then
           lint1 = dxi*dble(i-1)*direction*drr(itint(2))
           lint2 = dxi*dble(i)*direction*drr(itint(2))
           din(2) =((len_node-liso1)*lint2+(liso2-len_node)*lint1)/ &
                  (liso2-liso1)
         endif
         din1(:) = din(:) - dinprev(:)
         dinprev(:) = din(:)
         call intcart(xprev,xx,nat,mmm,din1,itint,nd,lterm)
         if(lterm) stop 'failure in intcart'
         xprev(:,:) = xx(:,:)
         call getgrad2(xx,dmag)
         dv = pe -v0
         if(k.eq. ngauss.and.dv.lt.0d0) dv = 0d0
         if(k.ne. ngauss.and.dv.lt.0d0) then
            write(6,*) 'n-th Gauss node = ', k
            write(6,*) 'negative Delta_V ', dv
            return
         endif
         if(dv .gt. 1d5)  then
!xxx           write(6,*) 'din =', din
           write(6,*) 'SCF failed in MOPAC (in Gauss-Legendre integral)'
           do i = 1, nat
             write(6,'(2X,a,3f12.6)')symbol0(i),(xprev(j,i)*autoang,j=1,3)
           enddo
           do i = 1, nat
             write(6,'(2X,a,3f12.6)') symbol0(i),(xx(j,i)*autoang,j =1, 3)
           enddo
           write(6,*) 'K =', k
           write(6,*) 'node(k) ', node(k)
           write(6,*) 'node(k-1) ', node(k-1)
!xxx          stop
           return  ! For SCF failure case in MOPAC
         endif
         theta = theta + sqrt(dv)*gw(k)
      enddo
      theta = theta*0.5d0*len_max*sqrt(2d0*mu)
!
! Army ant algorithm for branching
!
! generate a random number for accepting the tunneling probability
! and initialize variables
      call get_rng(lambda)
      probt = exp(-2d0*theta)
!xxx      if (probt.gt.0.95d0) return
      if(probt.gt.0.95d0)then
        write(6,*)'Surprisingly large tunneling prob.=',probt
      endif
      gamma = max(eta,probt)
        if(gamma .gt. lambda) then 
!xxx          call get_rng(lambda2)
          lchgw = .true.
!xxx          if(lambda2 .gt. 0.5)  then  ! no need to waste a second random number here
          if((1d0-gamma*0.5d0).ge.lambda)  then  ! tunneling event (occurs with prob. gamma/2)
             lbranch = .true.
!xxx             wght = probt/gamma
             wght = 2d0*probt/gamma
!            call comgen(xnew,mmm,nat,com,mtotal)
!            do i = 1, nat
! amplitude of p and r vectors 
!               pam =sqrt(pturn(1,i)**2+pturn(2,i)**2+pturn(3,i)**2)
!               ram =sqrt(xnew(1,i)**2+xnew(2,i)**2+xnew(3,i)**2)
!            do j = 1, 3 
!               pnew(j,i) = pam*xx(j,i)/ram
!            enddo 
!            enddo
!xxx             call cnsvangmom(xturn,pturn,xnew,pnew,nat)
! attempt to slow walk the iterative determination of pnew to convergence

             call cpu_time(time1)
             do i=2,n_waypts
               call cnsvangmom(waypt(:,:,i-1),ptemp,waypt(:,:,i),ptemp2,nat,iflag)
               if(iflag.eq.0)then
                 ptemp(:,:)=ptemp2(:,:)
                 xtemp(:,:)=waypt(:,:,i)
               else
                 write(6,*)'nonfatal failure in cnsvangmom, i=', i
               endif
             enddo

             call cnsvangmom(xtemp,ptemp,xnew,pnew,nat,iflag)
             call cpu_time(time2)
             time_pgen=time_pgen+time2-time1
             if(iflag.eq.1)then
               stop 'fatal convergence failure in cnsvangmom'
             endif

!start debugging
!     ke_old=0.d0
!     ke_new=0d0
!     do i=1,nat
!       ke_old = ke_old+( pturn(1,i)*pturn(1,i) + pturn(2,i)*pturn(2,i) + &
!                 pturn(3,i)*pturn(3,i) ) /mmm(i)
!       ke_new = ke_new+( pnew(1,i)*pnew(1,i) + pnew(2,i)*pnew(2,i) + &
!                 pnew(3,i)*pnew(3,i) ) /mmm(i)
!     enddo
!     ke_old = 0.5d0*ke_old
!     ke_new = 0.5d0*ke_new
!     write(6,*)'old and new ke=',ke_old*27.2113961d0,ke_new*27.2113961d0
!end debugging

! print the beginning, middle, and end of tunneling path for visualization purposes
! RMP
! print the beginning, and end of tunneling path for visualization purposes
           write(34,'(1X,i5)') nat
           write(34,'(2(2X,a,es14.5e3),2X,a,i7)')'Prob.',probt,'Weight', &
                                              wght,'TRAJ', ithistraj
            do k =1,nat
            write(34,'(1X,a,3f12.6)') symbol0(k), &
                                     (xturn(j,k)*autoang,j=1,3)
            enddo
            write(34,'(1X,i5)') nat
            write(34,*)
            do k =1,nat
              write(34,'(1X,a,3f12.6)') symbol0(k), &
                                     (xnew(j,k)*autoang,j=1,3)
            enddo
!
! Angular momentum -- debug purposes
!     do i = 1, 3
!        bigjm(i) = 0d0
!     enddo
!     do i=1,nat
!      bigjm(1)=bigjm(1)+(xturn(2,i)*pturn(3,i)-xturn(3,i)*pturn(2,i))
!      bigjm(2)=bigjm(2)+(xturn(1,i)*pturn(3,i)-xturn(3,i)*pturn(1,i))
!      bigjm(3)=bigjm(3)+(xturn(1,i)*pturn(2,i)-xturn(2,i)*pturn(1,i))
!     enddo
!     write(6,'(a,3f8.3)') 'Angular momentum before tunneling', &
!                          (bigjm(j),j=1,3)

!     do i = 1, 3
!        bigjm(i) = 0d0
!     enddo
!     do i=1,nat
!      bigjm(1)=bigjm(1)+(xnew(2,i)*pnew(3,i)-xnew(3,i)*pnew(2,i))
!      bigjm(2)=bigjm(2)+(xnew(1,i)*pnew(3,i)-xnew(3,i)*pnew(1,i))
!      bigjm(3)=bigjm(3)+(xnew(1,i)*pnew(2,i)-xnew(2,i)*pnew(1,i))
!     enddo
!     write(6,'(a,3f8.3)') 'Angular momentum after tunneling', &
!                          (bigjm(j),j=1,3)
!
!
           else
             lbranch = .false.
!xxx             wght = 1d0 - probt/gamma
             wght = 2d0*(1d0 - probt/gamma)
           endif
        endif
      
      end subroutine actionint
!-----------------------------

      subroutine comparint(itint,xx,lok)
!
! compare two bond lengths that are associated with transferred atom
! For example: A-H ...... B : H is transferred from A to B
! r1 = r_AH; r2 = r_BH
! In the case r_BH < r_AH, the action integral will stop and this tunneling 
! path will be discarded.
!
      use param, only: mnat
      use c_initial, only: istr, drr

      implicit none
      integer :: i,j,k,i1,i2,itint(2)
      real*8 ::  xx(3,mnat), r1, r2
      double precision, external :: bond 
      logical lok
      lok = .false.
      i1 = istr(1,itint(1))
      i2 = istr(2,itint(1))
      r1 = bond(xx(:,i1),xx(:,i2)) 
      i1 = istr(1,itint(2))
      i2 = istr(2,itint(2))
      r2 = bond(xx(:,i1),xx(:,i2)) 
      if( r1 .gt. r2 .and. drr(itint(1)).lt.0d0) lok =.true.
      if( r1 .lt. r2 .and. drr(itint(1)).gt.0d0) lok =.true.
      if( r1 .gt. r2 .and. drr(itint(1)).gt.0d0) lok =.false.
      if( r1 .lt. r2 .and. drr(itint(1)).lt.0d0) lok =.false.

      end subroutine comparint
!-----------------------------

      double precision function tpot(zed)
      use tpotmod
      use c_struct, only: icharge, imulti, nat, mmm, symbol0
      use c_traj, only: pe
      implicit none
      double precision :: zed, zold, dmag
      logical :: lterm
      save zold
      
      if(if1.ne.0)then !this is not the first potential call for this zero search
        xlast(:,:)=xnow(:,:)
      else  ! We need to treat the first call special
        if1=1
        zold=0d0
      endif

      din(1)=(zed-zold)*scale*drrpass(itsearch(1))
      if(itsearch(2).ne.0)then
        din(2)=(zed-zold)*scale*drrpass(itsearch(2))
      endif

      call intcart(xlast, xnow, nat, mmm, din, itsearch, 1, lterm)
! Using getgrad2 calls for now because we have no procedures in place to get
! the potential without also getting the gradient; this should be changed....
      call getgrad2(xnow, dmag)  
      zold=zed
      tpot=pe-vturn
      end
