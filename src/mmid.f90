      SUBROUTINE mmid(y,dydx,nv,time,htot,nstep,yout)
! Modified midpoint method from Numerical Recipes
! Used by the Bulirsch-Stoer integrator.
      use param, only: mnyarray
      implicit none
      INTEGER nstep,nv
      INTEGER i,n, ii
      DOUBLE PRECISION htot,time,dydx(mnyarray),y(mnyarray),yout(mnyarray)
      DOUBLE PRECISION h,h2,swap,x,ym(mnyarray),yn(mnyarray)
      h=htot/dble(nstep)
      do 11 i=1,nv
        ym(i)=y(i)
        yn(i)=y(i)+h*dydx(i)
11    continue
      x=time+h
      call derivs(x,yn,yout,nv,0)
!     write(6,'(a,3f12.4)') 'gcre - derivs ',gcre(1),gcre(2),gcre(3)
!     write(6,'(a,3f12.4)') 'gcim - derivs ',gcim(1),gcim(2),gcim(3)
      h2=2.d0*h
      do 13 n=2,nstep
        do 12 i=1,nv
          swap=ym(i)+h2*yout(i)
          ym(i)=yn(i)
          yn(i)=swap
12      continue
        x=x+h
        call derivs(x,yn,yout,nv,0)
13    continue
      do 14 i=1,nv
        yout(i)=0.5d0*(ym(i)+yn(i)+h*yout(i))
14    continue
      END SUBROUTINE mmid
