! Default values
! Default values for the controlling parameters are all set here
! Control
      intflag=1
      atomdiatom=.false.
      reactcol=.false.
      hstep0=2.0d0/autofs
      temp0 = 298.15d0
      nramp=1000000000
      periodic = .false.
      dimen = 3
      p0 = 1.d0/autoatm
      equilibrium=.false.
      itherm=0
      itempzpe=.false.
      qcpack = 2 ! (Molpro)
! Reactive collision:
      stateselect=.false.
      transelect=.false.
      b_fix=.false.
      itosave=5000
      nsave  =10
      ifr0eff=.false.
! atomdiatom
      vvad = 0
      jjad = 0
! Surface
      repflag=0
      methflag=0
      frusmeth = 0
      nsurf0 = 1
      nsurfi = 1
!     repflgi= -1
      repflgi=repflag
      nsurft = 1
      stodecoflag = 0
      cparam = 1.d0
      e0param = 0.1d0
! Study of mode populations
      lmodpop = .false.
      ralpha = 1.0d0
! TRAPZ-like methods
      ntrapz = 0
      nvers = 1
      prodzpe = 1.0d6
      freqlim = 1.0d0
! Trajectories
      pickthr=0.1
      ipicktrj=0
      demin=-1.d50
      demax=1.d50
      ifnoang=.true.
      ifeqwithrot=.false.
      ifreinitrot=.true.
      iscaleenergy=.false.
      irottemp = .false.
      irotfreeze = .false.
      lzerocom=.true.
      lzerotrans=.true.
      tinyrho = 1d-4
! Temperature control
      nrampstep = 20000
      n_ramp   = 20000
      nequil   = 0
      rampfact = 1.0d0
      nramp    = 1
      iadjtemp = 0
      nreinit = 10000
      sysfreq = 1.d0
!      nh(1,1)=50.d0
!      nh(1,2)=50.d0
      taut=0.d0
      taup=0.d0
      vfreq=0.d0
!     ntraj = 1000
      tflag(1) = 0
      tflag(2) = 0
      ieqtherm=0
! Initial condition preparation for AGs initx(im)=0, initp(im)=0
      lfixte=.false.
      deltate=0.d0
      do i=1,mnmol
        initx(i)  = 0
        initp(i)  = 1
        vibsel(i) = 0
        ivibdist(i) = 0
        ivibtype(i) = 0
        rotsel(i) = .false.
        rotst(i)  = .false.
        roteng(i) = .false.
        ewell(i)  = 0.d0
        temp0im(i)= 298.15d0
        nvibmods(i) = 0
        vibene(:,i) = 0.02
        vibdistn(:,i) = -1
        verte(i) = 0d0
        bandwd(i) = 0d0
      enddo
      icompp=.false.
      icomxx=.false.
! Initial rotational quantum number qnmrot0(3,im)
! Initial vibrational quantum number qnmvib0(3*mnat,im)
      do i=1,mnmol
        do j=1,3
          qnmrot0(j,i) = 0
          comxx(j,i) = 0.d0
          compp(j,i) = 0.d0
        enddo
        do j=1,3*mnat
          qnmvib0(j,i) = 0
        enddo
        etrans(i)=0.d0
      enddo
! termination condition
      nbondform  = 0
      nbondbreak = 0
      nbondtype = 0
      agmerge   = .true.
      agdefrag  = .true.
      attrans   = .true.
      determ    = 1.d50
      n_frags = 2
      n_turn  = 3
      n_atoffr = 0
      termsteps = 50
      bdfmthr = 1.5d0
      bdbrthr = 2.5d0
! Default distance of different type of bonds
      do i=1,111
        do j=1,111
          rijco(i,j) = ( atmradco(i)+atmradco(j) )/autoang
        enddo
      enddo
! default values for t_nstep is based on: if each step is 2 fs, and the
! running time is 10000 fs, then t_nstep=5000
      termflag = 0
      t_nstep = 5000
      t_stime = 5000.0d0/autofs
      t_gradmag = 1.d-4*autoang/autoev
      t_gradmax = 5.d-4*autoang/autoev
! Output
      outflag=0
      nprint=1
      minprinticon=.false.
      minprinttrapz=.false.
      maxprint=.false.
      idebug=.false.
      lwrite=.true.
! hdf5 output
      outflag_h5 = 0
      lwrite_h5=.true.
! Analysis control
      nstat = 1
      cohesive  = .false.
      kinetic   = .false.
      potential = .false.
      totenergy = .true.
      heatcap = .false.
      icfrag = .false.
      rbin   = 1.0d0/autoang
! Always assuming the whole AG is non linear
      linearmol(mnmol+1) = 2
! cell (isolated cluster)
      icell = 0
!
      ireadchg = .false.
! direct dynamics
      lreadin = .false.
! tunneling
      ltunn = .false.
      eta = 0.95
      ngauss = 6
      itunn(:) = 0
      drr(:) = 0d0
! internal coordinates
      nstr = 0
      nbend = 0
      ntor = 0
      nimptor = 0
      nlinben = 0
! RMP14
! Random number seed
      ranseed=0
! RMP14
! Uses the diapot_int subroutine specific for diatoms
      ldiat=.false.
! default settings of impact parameter
      bminad = -1.0d0
      bmaxad = 6.5d0
      bparam = -1.0d0
! Atom-diatom mode
      adbmode=1
      ecolad = 0.0d0
      escatad = 0.0d0
! Atom-diatom energy mode by input
      ademod=1
! Atom-diatom phase mode by input
      phmode=1
      nspinmode=0
      nspin=-1.0d0
