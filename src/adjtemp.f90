      subroutine adjtemp(tempnow,temptarg,im)
! Simplest adjusting procedure: problem: hot solvent, cold solute
!   Input:
! ppm(3,mnat)      : Momenta
! mmm(mnat)        : Atom weight
! iadjtemp         : Method choice for thermostat
! natoms           : Number of atoms
! taut             : Coupling parameter for Berendsen thermostat
! vfreq            : Collision frequency for Andersen thermostat
! tempnow:  Current temperature
! temptarg: Target temperature
!   Output:
! ppm(3,mnat)      : Momenta
      use c_output, only: idebug
      use c_struct, only: natoms, mmm
      use c_sys, only: vfreq, taut, iadjtemp
      use c_traj, only: kbtarg, ppm, step 

      implicit none
      integer :: im
      double precision :: tempnow,temptarg
!     Local
      integer :: i,j
      double precision :: r1,r2,r3
      double precision :: signs,adjfact,mmofagm,tmp,adjprob
      double precision :: x1,x2,w,y1
      double precision :: comxxm(3)
!
      adjprob=vfreq*step
      if (iadjtemp.eq.0) then
        tmp=1.d0+(step/taut)*(temptarg/tempnow-1.d0)
        if (tmp.le.0.d0) then
          write(6,*) 'ERROR: in Berendsen thermostat' 
          write(6,*) 'TAUT is too small'
          stop
        endif
        adjfact=dsqrt(tmp)
        if (idebug) write(6,*) 'Temp. Adj. fact',adjfact
        do i=1,natoms
          ppm(1,i)=adjfact*ppm(1,i)
          ppm(2,i)=adjfact*ppm(2,i)
          ppm(3,i)=adjfact*ppm(3,i)
        enddo
       elseif (iadjtemp.eq.1) then
        adjfact=dsqrt(temptarg/tempnow)
        do i=1,natoms
          ppm(1,i)=adjfact*ppm(1,i)
          ppm(2,i)=adjfact*ppm(2,i)
          ppm(3,i)=adjfact*ppm(3,i)
        enddo
!       Andersen thermostat
       elseif (iadjtemp.eq.2) then
        do i=1,natoms
          call get_rng(r3)
          if (r3 .lt. adjprob) then
            do j=1,3
             w=2.d0
             do while( w.ge.1.d0.or.w.eq.0.d0)
                call get_rng(r1)
                call get_rng(r2)
                x1=2.d0*r1-1.d0
                x2=2.d0*r2-1.d0
                w = x1*x1+x2*x2
             enddo
             w  = dsqrt(-2.d0*dlog(w)/w)
             y1 = x1*w
             ppm(j,i) = y1*dsqrt(mmm(i)*kbtarg)
            enddo
          endif
        enddo
       else
         continue
      endif
 
      end subroutine adjtemp
