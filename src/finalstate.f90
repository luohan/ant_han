      subroutine finalstate

! Computes some quantities after each trajectory finishes.
! Writes some output files.
! comxxfr:   CoM coord. of a fragment
! comvvfr:   CoM velocity of a fragment
! compfr:   CoM velocity of a fragment
! kefrag:    Kinetic energy of a fragment
! relmu:     Reduced mass of two fragments
! relmuag:     Reduced mass of two AGs in a reaction collision run
! kerefr:    Relative kinetic energy between two fragments
! iflin:     0: an atom; 1: linear; 2: non-linear
! bigjtfr(mnfrag):    Overall angular momentum
! bigjfr(3,mnfrag):   Angular momentum vector
! bigj(3):            Temp. Storage of angular momentum
! evibm:              Temp. Storage of vib energy
! evibrot:            Temp. Storage of vib energy
!
      use param, only: mnsurf, mnfrag, mnmol, autoev, autofs, autoang, amutoau
      use c_output, only: lwrite, maxprint
      use c_struct, only: xxf, mm0, mmm, xxm, iflinear, rijco, symbol0, &
                          mmoffr, momiprin, rotaxis, natoms, fragind,&
                          compfr, nfrag, compfr, natinfrag
      use c_sys, only: methflag, nsurft, trajlist, tflag
      use c_initial, only: rasym, easym, arrad, initx, nsurf0
      use c_traj, only: t0react, trxover, ireact, bigj, ppm, weight,  &
                        indtor, nflip, istep, time, ppf, pe, tei, te,  &
                        itrapz, itraj, nsurf, bigjtoti, bigjtot, crei, &
                        cimi, cim, cre, nhop, thetaad, phiad, etaad, &
                        psiad, b0ad, chiad, ecolad,vvad,jjad
      use c_term, only: trmax, dihfin, dihd, indfm, outform, indbr, &
                        outbreak, termflag, nbondbreak, tibreak
      use hdf5_output, only : lwrite_h5, hdf5o_write_array

      implicit none
      integer :: i,j,k,arr,index1,festate
      double precision :: rhor(mnsurf,mnsurf),rhoi(mnsurf,mnsurf),tmp,tmpi, &
                          rr(3),jp(9),jq(9),jm(9),xv,xj,rin,rout,&
                          erotm,evibm,evibrot,erottot,ediatot,kerel,kedia, &
                          xjx,xjy,xjz,eint

!     for unit 31
      integer :: ifrag,iflin(mnfrag)
      double precision :: rr1,rr2,comxxfr(3,mnfrag),comvvfr(3,mnfrag),&
                          relmu(mnfrag,mnfrag),kefrag(mnfrag),&
                          kerefr(mnfrag,mnfrag),comxxm(3), &
                          bigjfr(3,mnfrag),bigjtfr(mnfrag)

!     Han added for modified vwkb
      double precision :: rmin,rmax,emin,emax,emax2,tinyv
      parameter(tinyv=1.0d-12)
!     Han added for dissociated reaction
      integer :: idiss, ii, ij, irel
      double precision :: p_dia
      logical :: iquasi,ierror
      ierror = .false.

! Print of itrapz, the number of times TRAPZ-like methods were used
      write(6,*) 'Number of entry in TRAPZ-like methods = ',itrapz
      write(6,*)

!     get trajectory index1
      if (tflag(2).eq.1) then
        index1 = trajlist(itraj)
      else
        index1 = itraj
      endif

!     check integration
      write(6,*)'Energy conservation'
      write(6,100)'Initial total energy = ',tei*autoev,' eV'
      write(6,100)'Final total energy   = ',te*autoev,' eV'
      write(6,'(1x,a40,es18.8,a)')'Converged to       = ',(te-tei)*autoev,' eV'
      write(6,*)

      write(6,*)   'Angular momentum conservation'
      write(6,100) 'Initial angular mom. = ',bigjtoti(mnmol+1),' au'
      write(6,100) 'Final angular mom.   = ',bigjtot,' au'
      write(6,'(1x,a40,es18.8,a)') 'Converged to      = ',(bigjtot-bigjtoti(mnmol+1)),' au'
      write(6,*)

      write(6,*)'Electronic state population conservation'
      call getrho(crei,cimi,rhor,rhoi,nsurft)
      tmpi = 0.d0
      do i=1,nsurft
       tmpi = tmpi + rhor(i,i)
      enddo
      write(6,102)'                       ','total',(i,i=1,nsurft)
      write(6,103)'Initial populations  = ',tmpi,(rhor(i,i),i=1,nsurft)
      call getrho(cre,cim,rhor,rhoi,nsurft)
      tmp = 0.d0
      do i=1,nsurft
        tmp = tmp + rhor(i,i)
      enddo
      write(6,103)'Final populations    = ',tmp,(rhor(i,i),i=1,nsurft)
      write(6,104)'Converged to         = ',(tmp-tmpi)
      write(6,*)

!     dump final state info
!  correct the final electronic state for mean-field method
!  Note that nsurf is not always the current state for mean-field method
      festate = 1
      if(methflag.eq.2.or.methflag.eq.3.or.methflag.eq.4) then
        do i = 2, nsurft
          if(rhor(i,i).gt.rhor(festate,festate)) festate = i
        enddo
      else
        festate = nsurf
      endif
      write(6,*)'Initial electronic state  =',nsurf0
!     write(6,*)'Final electronic state    =',nsurf
      write(6,*)'Final electronic state    =',festate
      if (methflag.eq.1.or.methflag.eq.5) then
      write(6,*)
      write(6,111)'Final stats on hopping events: Total number of hops', &
                ', number of Energetically Allowed hops', &
                ', number of Frustrated and Reflected hops',   &
                ', and number of Frustrated and Ignored hops',  &
                (nhop(i),i=1,4)
      endif
      write(6,*)

      if (initx(1).eq.4) then
!     for atom-diatom initial conditions, perform the following analyses,
!     which follow NAT
         call rtox(xxf,rr,1)
         ! find shotest interatomic distance
         if (rr(1).lt.rr(2).and.rr(1).lt.rr(3)) arr=1
         if (rr(2).lt.rr(1).and.rr(2).lt.rr(3)) arr=2
         if (rr(3).lt.rr(1).and.rr(3).lt.rr(2)) arr=3
         write(6,*)'Initial arrangement       =',arrad
         call carttojac(xxf,ppf,mm0,jq,jp,jm,0,arr)

!     add by Han Luo to roughly check dissociation
         ii = arr         ! get atom id
         ij = arr+1
         if (ij > 3) ij = 1
         irel = ij+1
         if (irel >3) irel = 1

         !component of momentum on iteratomic direction
         p_dia = jp(1)*jq(1)+jp(2)*jq(2)+jp(3)*jq(3)
         p_dia = p_dia /dsqrt(jq(1)*jq(1)+jq(2)*jq(2)+jq(3)*jq(3))

         k = 0
         idiss = 0
         ! find the bond in tibreak croresspond to arr
         do i = 1,nbondbreak
            if ((tibreak(i,1) .eq. ii ) .and. (tibreak(i,2) .eq. ij))then
               k = i
               exit
            else if ((tibreak(i,1) .eq. ij ) .and. &
                        (tibreak(i,2) .eq. ii))then
               k = i
               exit
            endif
         enddo
         ! 1) distance check
         if (k .ne. 0)then
           if (rr(arr) .lt. trmax(k)) then
             idiss = 0
           else
             if (p_dia .gt. 0.0d0) then
               idiss = 1   ! we're pretty sure this is a dissociation
             else
               idiss = 2   ! not sure and we need to further detect
             endif
           endif
         else
           write(6,*) "Fail: bond information not provided for arrad=",arr
         end if

         kerel = 0.d0
         kedia = 0.d0
         do i=1,3
            kedia = kedia + jp(i)**2/(2.d0*jm(i)) ! this quantity oscillates as the diatom vibrates and rotates
            kerel = kerel + jp(i+3)**2/(2.d0*jm(i+3)) ! this quantity converges as the fragments separate
         enddo
         erottot = te - kedia - kerel - pe ! should be near zero for total angular momentum zero
         ediatot = pe + kedia ! total diatomic energy
         eint = ediatot - easym(nsurf,arr) ! set zero of energy at the classical equilibrium
         xjx = jq(2)*jp(3)-jq(3)*jp(2)
         xjy = jq(3)*jp(1)-jq(1)*jp(3)
         xjz = jq(1)*jp(2)-jq(2)*jp(1)
         ! xj = dsqrt(xjx*xjx+xjy*xjy+xjz*xjz) ! rotational quantum number is magnitude of RxP
         ! xj = xj-0.5d0 ! Bohr-Sommerfeld conditions
         xj = dsqrt(xjx*xjx+xjy*xjy+xjz*xjz+0.25)
         xj = xj -0.5d0
         xj = max(0.d0,xj)
         ! erotm = (xj+0.5d0)**2/(2.d0*rasym(nsurf,arr)**2*jm(1)) ! assume separability
         erotm = xj*(xj+1.0d0)/(2.d0*rasym(nsurf,arr)**2*jm(1)) ! assume separability
         evibm = eint - erotm
!      compute vibrational action using WKB
         if (idiss .eq. 0) then
           iquasi = .false.
           call diamin(rmin,emin,xj,arr,mmm,nsurf,ierror)
           if (ierror) then
             ! this is very likely a dissociation trajectory because no minimum point
             ! is found on the potential curve
             rmin = -1.d0
             call diapot(50.d0,arr,emax,0.d0,mmm,nsurf)  !50 bohr is hardcoded
             rmax = 50.0d0
             continue
           else
             call diapot(50.d0,arr,emax2,0.d0,mmm,nsurf)
             call diamax(rmax,emax,xj,arr,mmm,nsurf,rmin,30.0d0,ierror)
             if (ierror) then
               rmax = 50.0d0
               emax = emax2
             else
               if (dabs(emax2-emax) > tinyv) then
                 iquasi = .true.
              ! final state could be a quasi-bond state or dissociation
               else
                 rmax = 50.0d0   !right bondary for turn, hard coded
                 emax = emax2
                 iquasi = .false.
               endif
             end if
           end if

            ! The following conditions must be satisfied to make the
            ! molecule dissociate:
            ! - The total energy is larger than asymptotic energy or quasi bond
            ! - The momentum of vibration must be outward
           if ((ediatot - emax) .ge. tinyv) then
             idiss = 2
             if (p_dia .gt. 0) idiss = 1  ! pretty sure
           elseif (rmin .lt. 0.0d0) then
             ! edia < asymptotic value but no minimum point is found on pes
             idiss = 2
           else
             call vwkb(arr,mmm,ediatot,xj,xv,rin,rout,nsurf,rmin,rmax,emin,emax,ierror)
             if (ierror) then
               write(*,*) "vwkb in finalstate fail"
               idiss = 2
             else
               if (iquasi) then
                 if (rr(arr) .gt. rmax) then
                   idiss = 2
                 endif
               else
                 if (rr(arr) .gt. rmax) then
                   idiss = 3
                 !this condition shouldn't happen, set in case vwkb doesn't solve
                 !equation successfully
                 endif
               endif
               if (xv .lt. -0.5d0 ) then
                 idiss = 2 ! we didn't find turning points
               endif
             end if

             if ( (idiss .ne. 3) .and. (idiss .ne. 0))then
               if (p_dia .gt. 0) then
                 idiss = 1
               endif
             endif
           endif
         endif

         if (idiss .ne. 0) then
           xj = -1.0d0
           xv = -1.0d0
           kerel = kedia+kerel
           write(6,*)'Dissociation flag         =',idiss
           write(6,*)
! idiss = 1: confirmed dissociation
! idiss = 3: not sure
! idiss = 2: not sure
           if (idiss == 1) then
             arr = 4
           else
             arr = -idiss
           end if
         endif
         write(6,*)'Final arrangement         =',arr
! calculate scattering angle
!   Scattering angle is defined as the angle between velocity of the single atom
!   and +z axis. For nonreacting collision, this is the correct one
!   However, for exchange reaction, you may consider use pi-chiad
         if (idiss == 0) then
           chiad = jp(6)/dsqrt(jp(4)*jp(4)+jp(5)*jp(5)+jp(6)*jp(6))
           chiad = dacos(chiad)
           if (jp(5) .lt. 0)  chiad = -chiad
         else
           chiad = 0.0d0
         end if
         write(6,*)'Scattering Angle          =',chiad
         write(6,*)

         eint = eint*autoev
         kerel = kerel*autoev
         erotm = erotm*autoev
         evibm = evibm*autoev
         if (lwrite(30)) write(30,115)index1,nsurf,arr,idiss,time*autofs,istep, &
            rhor(1,1),rhor(2,2),te*autoev,easym(nsurf,arr)*autoev, &
            kerel,eint,evibm,erotm,xv,xj,chiad
         if (lwrite_h5(30)) then
           if (nsurft .eq. 1) then
             call hdf5o_write_array(30,index1,(/b0ad*autoang,ecolad*autoev, &
               dble(vvad)+dble(jjad)/1000,thetaad,phiad,etaad,psiad/), &
               (/dble(arr),kerel,xv,xj,chiad,time*autofs,(te-tei)*autoev/))
           else
             call hdf5o_write_array(30,index1,(/b0ad*autoang,ecolad*autoev, &
               dble(vvad)+dble(jjad)/1000,thetaad,phiad,etaad,psiad/), &
               (/dble(arr),kerel,xv,xj,chiad,time*autofs,(te-tei)*autoev,dble(festate)/))
           end if
         end if
! NOTE:  XV is not accurate for SE method, because a pure state is used
!        instead of the mixed state in VWKB!
      endif
!
!     for dissociation termination conditions, compute some quantities
!   Termflag = 3 and termflag = 5
      if (termflag.eq.3 .or. termflag.eq.5) then
       write(6,*) 'Final state analysis for TERMFLAG = ',termflag
       if (outbreak.ne.0) then
         write(6,*) 'Broke a ',symbol0(indbr(outbreak,1)),'-',  &
          symbol0(indbr(outbreak,2)),  &
          ' bond with atoms indices',indbr(outbreak,1), &
          ' and ',indbr(outbreak,2)
!   RVM & DAB
       else
         write(6,*) 'No dissociation for this trajectory'
       endif
       if (outform.ne.0) then
         write(6,*) 'Formed a ',symbol0(indfm(outform,1)),'-',  &
          symbol0(indfm(outform,2)),  &
          ' bond with atoms indices',indfm(outform,1),  &
          ' and ',indfm(outform,2)
!   RVM & DAB
       else
         write(6,*) 'No bond formation for this trajectory'
       endif
       write(6,*) 'Assigning fragment # based on bonding formation'
      endif
!   Termflag = 8
      if(termflag.eq.8) then
         write(6,*) 'Final state analysis for TERMFLAG = ',termflag
         if(nflip.gt.0) then
            do i = 1, nflip
              write(6,'(2X,a,4(i2,a),f8.2,a,es12.6)') 'Torsion angle ', &
              dihd(1,indtor(i)),'-',dihd(2,indtor(i)),'-',      &
              dihd(3,indtor(i)),'-',dihd(4,indtor(i)),' rotate to ',  &
              dihfin(i), '  with weight = ', weight
            enddo
         else
            write(6,*) 'No internal rotation occurs '
         endif

      endif
!
!  Print out fragment information for termflag = 3,4,5
      if (termflag.ge.3 .and. termflag.le.5) then
!      if (maxprint) then
       call rijmatr
       call frag
       write(6,*) 'Fragment analysis information'
       write(6,*) 'Number of fragments found: ',nfrag
       do ifrag=1,nfrag
!        Initialize
         do j=1,3
           compfr(j,ifrag) = 0.d0
         enddo
         write(6,*) 'Analyzing fragment ',ifrag
         write(6,*) 'Coordinates: '
         write(6,'(1x,74a1)') ('-',i=1,74)
         write(6,2000) '#','Index','Symbol','x','y','z'
         write(6,'(1x,74a1)') ('-',i=1,74)
         do k=1,natinfrag(ifrag)
!          put this frag's info into a single array
           mmm(k) = mm0(fragind(k,ifrag))
           do j=1,3
             xxm(j,k)  = xxf(j,fragind(k,ifrag))
             ppm(j,k)  = ppf(j,fragind(k,ifrag))
             compfr(j,ifrag) = compfr(j,ifrag)+ppm(j,k)
           enddo
           write(6,2050) k,fragind(k,ifrag),symbol0(fragind(k,ifrag)), &
              (xxm(j,k)*autoang,j=1,3)
         enddo
         write(6,'(1x,74a1)') ('-',i=1,74)
!        Move this fragment to its own CoM
         natoms=natinfrag(ifrag)
         call comgen(xxm,mmm,natoms,comxxm,mmoffr(ifrag))
!        Get comxx of this fragment from comxxm
         do j=1,3
           comxxfr(j,ifrag) = comxxm(j)
         enddo
!        Call rotprin to see if this frag is an atom, linear or
!        non-linear poly-atom
         call rotprin(xxm,mmm,natoms,momiprin,rotaxis,iflin(ifrag))
!        Angular momentum of this fragment
         iflinear=iflin(ifrag)
         call angmom(iflinear,bigj,ppm,xxm,natoms,bigjtfr(ifrag))
!        Get angular moment of this fragment from bigjm
         do j=1,3
           bigjfr(j,ifrag) = bigj(j)
         enddo
!        Translation energy of this fragment
         kefrag(ifrag) = 0.d0
         do j=1,3
           comvvfr(j,ifrag)=compfr(j,ifrag)/mmoffr(ifrag)
           kefrag(ifrag) = kefrag(ifrag) + compfr(j,ifrag)**2
         enddo
         kefrag(ifrag) = kefrag(ifrag)/(2.d0*mmoffr(ifrag))
       enddo
!      Relative translation energy
       do i=1,nfrag
         do j=i+1,nfrag
           kerefr(i,j) = 0.d0
           relmu(i,j) = mmoffr(i)*mmoffr(j)/(mmoffr(i)+mmoffr(j))
           do k=1,3
             kerefr(i,j) = kerefr(i,j) + ( compfr(k,j) - compfr(k,i) )**2
           enddo
           kerefr(i,j) = kerefr(i,j)/(2.d0*relmu(i,j))
         enddo
       enddo
!      CoM
       write(6,*) 'Center of mass coordinate (A)'
       write(6,'(1x,74a1)') ('-',i=1,74)
       write(6,2060) 'Fragment','Mass','x','y','z'
       write(6,'(1x,74a1)') ('-',i=1,74)
       do ifrag=1,nfrag
         write(6,2070) ifrag,mmoffr(ifrag)/amutoau,  &
                  (comxxfr(j,ifrag)*autoang,j=1,3)
       enddo
       write(6,'(1x,74a1)') ('-',i=1,74)
!      CoM Velocity
       write(6,*) 'Center of mass velocity (au)'
       write(6,'(1x,74a1)') ('-',i=1,74)
       write(6,2060) 'Fragment','KE(eV)','x','y','z'
       write(6,'(1x,74a1)') ('-',i=1,74)
       do ifrag=1,nfrag
         write(6,2070) ifrag,kefrag(ifrag)*autoev,  &
                       (comvvfr(j,ifrag),j=1,3)
       enddo
       write(6,'(1x,74a1)') ('-',i=1,74)
!      Angular momentum around its CoM
       write(6,*) 'Angular momentum (au) around the CoM of a fragment'
       write(6,*) 'Single atoms are not shown'
       write(6,'(1x,74a1)') ('-',i=1,74)
       write(6,2060) 'Frag','Tot(au)','x','y','z'
       write(6,'(1x,74a1)') ('-',i=1,74)
       do ifrag=1,nfrag
         if (natinfrag(ifrag).ge.2) write(6,2070) ifrag,bigjtfr(ifrag), &
                       (bigjfr(j,ifrag),j=1,3)
       enddo
       write(6,'(1x,74a1)') ('-',i=1,74)
! Relative translation speed, its vector, distance between CoM
       if (nfrag.gt.1) then
         write(6,*) 'Relative translation speed and its vector'
         write(6,'(1x,80a1)') ('-',i=1,80)
         write(6,2080) 'Frag','Frag','R (A)','Vrel (A/fs)','x','y','z'
         write(6,'(1x,80a1)') ('-',i=1,80)
         do i=1,nfrag
           do j=i+1,nfrag
             tmp=0.d0
             tmpi=0.d0
             do k=1,3
               tmp=tmp+(comvvfr(k,j)-comvvfr(k,i))**2
               tmpi=tmpi+(comxxfr(k,j)-comxxfr(k,i))**2
             enddo
             tmp=dsqrt(tmp)
             tmpi=dsqrt(tmpi)
             write(6,2090) i,j,tmpi*autoang,tmp*autoang/autofs,  &
               ( ((comvvfr(k,j)-comvvfr(k,i))*autoang/autofs ), k=1,3 )
           enddo
         enddo
         write(6,'(1x,80a1)') ('-',i=1,80)
       endif
      endif
      if (termflag.eq.6 .and. ireact.eq.1) then
        write(6,2100) 'Sticking time for trajectory ',itraj,' is ',  &
           (trxover-t0react)*autofs,' fs'
        write(6,*) 'trxover,t0react,bndbr,ind1,ind2',trxover*autofs, &
          t0react*autofs,trmax(1)*autoang,indbr(1,1),indbr(1,2),     &
          rijco(indbr(1,1),indbr(1,2))*autoang
      endif
!
!  Weight of each trajectory
!
      write(6,'(1x,a,es14.6e3)') 'Weight of this trajectory is ',weight
!
! All formats
 100  format(1x,a40,f18.8,a)
 102  format(1x,a,10x,a,100i15)
 103  format(1x,a,100f15.5)
 104  format(1x,a,e18.8)
 115  format(1x,4i5,f15.5,i10,100f10.5)
 1031 format(1x,3i5,100f15.5)
 2000 format(1x,3a10,3a14)
 2050 format(1x,2i10,a10,3f14.8)
 2060 format(1x,a8,4a14)
 2070 format(1x,i8,f14.6,3f14.8)
 2080 format(1x,2a5,5a14)
 2090 format(1x,2i5,5f14.8)
 2100 format(1x,a,i5,a,f14.2,a)
 111  format(1x,4a,4i7)

      end subroutine finalstate
