      subroutine radialdist(itmp,step,time,nbinrad,rmax,rmin,rbin,  &
                            raddist,iprint)
!
! really the pair correlation function
!      http://www.physics.emory.edu/~weeks/idl/gofr2.html
!
      use param, only: pi, autofs
      use c_struct, only: natoms, rij 

      implicit none
      integer :: i,j,k,ibin,nbinrad,iprint,itmp
      double precision :: raddist(0:nbinrad+1),tmp,step,rbin,&
                          time,xnorm,rmax,rmin,rrmax
!
!    
! find out the largest rij distance in the AG
      rrmax=-1.d0
      do i=1,natoms
      do j=i+1,natoms
        rrmax=max(rrmax,rij(i,j))
      enddo
      enddo
      if (rrmax.le.rmax) then
        nbinrad = int((rrmax-rmin)/rbin)
      endif
      do i=1,natoms
      do j=i+1,natoms
        if (rij(i,j).lt.rmin) then
          ibin = 0
        elseif (rij(i,j).gt.rmax) then
          ibin = nbinrad + 1
        else
!         determine bin number
          ibin = int( (rij(i,j) - rmin) /rbin ) + 1
        endif
        raddist(ibin) = raddist(ibin) + step
      enddo
      enddo
!
      if (iprint.eq.1) then
      tmp = time*dble(natoms)
      tmp = tmp*rbin*4.d0*pi
      write(41,10) itmp,time*autofs,raddist(0)/time,  &
        (raddist(k)/(tmp*((dble(k)-0.5d0)*rbin+rmin)**2),k=1,nbinrad), &
         raddist(nbinrad+1)/time
      endif
 10   format(1x,i7,400f12.4)
!
      end subroutine radialdist
