      subroutine hop(newsurf)

! Perform a successful or a frustrated hop from NSURF to NEWSURF.
! For a successful hop, NSURF is set to NEWSURF and PP is adjusted
!   to conserve energy.
! For a frustrated hop, NSURF is unchanged, and PP is adjusted for
!   a reflected-and-hop-back.  PP is unchanged for an ingored
!   frustrated hop.
! Input  ppm
! Output ppm

      use param, only: autoev, autofs
      use c_struct, only: mmm, natoms
      use c_traj, only: nhop, ppm, gpem, frusmeth, itraj, time, pem, dvec, nsurf

      implicit none
! input
      integer :: newsurf

! local
      integer :: i,j,k,l
      double precision :: dxi,sectrm,a,b,a2,ab,ra,radi,scr,&
                          tmp,deltae,dot1,dot2

! check for forbidden hops
        deltae = pem(newsurf) - pem(nsurf)

        a=0.0d0
        b=0.0d0
        do i=1,3
        do j=1,natoms
          scr=dvec(i,j,nsurf,newsurf)/mmm(j)
  
!         a = momentum.dot.dvec/mass
!         b = dvec.dot.dvec/mass
          a=a+scr*ppm(i,j)
          b=b+scr*dvec(i,j,nsurf,newsurf)
        enddo
        enddo
        a2=a*a
        ab=a/b
!       sectrm = deltae/kinetic energy along d
     
        sectrm = 2.0d0*deltae*b/a2
        radi = 1.0d0-sectrm

!       if radi > 0 then delta > kinetic energy along d, so we can hop
!       note that for a hop down, sectrm is negative, so we can always hop
        nhop(1) = nhop(1) + 1 
        if(radi.ge.0.0d0) then
!         successful hop
          write(6,*)'HOP!',nsurf,' ---> ',newsurf,  &
         '  ',time*autofs,'  ',itraj,'  ',autoev*(pem(2)-pem(1)),autoev*pem(1),autoev*pem(2)
          nhop(2) = nhop(2) + 1
          radi=sqrt(radi)
          ra=ab*(1.0d0-radi)
          do i=1,3
          do j=1,natoms
           ppm(i,j) = ppm(i,j) - dvec(i,j,nsurf,newsurf)*ra
          enddo
          enddo
          nsurf = newsurf
        else
          if (frusmeth.eq.0) then
!           do nothing
            write(6,*)'FRUST! IGNORE'
            nhop(4) = nhop(4) + 1
          elseif (frusmeth.eq.1) then
!           frustrated hop, reflect
            write(6,*)'FRUST! REFLECT'
            nhop(3) = nhop(3) + 1
            ra=2.0d0*ab
            do i=1,3
            do j=1,natoms
              ppm(i,j) = ppm(i,j) - dvec(i,j,nsurf,newsurf)*ra
            enddo
            enddo
          elseif (frusmeth.eq.2) then
!           gradV method
!           dot1 is the gradient of the NEWSURF in the direction of dvec
            dot1 = 0.d0
            do i=1,3
            do j=1,natoms
              dot1 = dot1 + gpem(i,j,newsurf)*dvec(i,j,nsurf,newsurf)
!              if (repflag.eq.0) then
!             adiabatic
!              dot1 = dot1 + gpema(i,j,newsurf)*dvec(i,j,nsurf,newsurf)
!              elseif (repflag.eq.1) then
!             diabatic
!              dot1 = dot1 + gpemd(i,j,newsurf,newsurf)  &
!                                              *dvec(i,j,nsurf,newsurf)
!              else
!              write(6,*)'REPFLAG = ',repflag,' in HOPCHECK'
!              stop
!              endif
            enddo
            enddo
!           dot2 is the momentum in the direction of dvec
            dot2 = 0.d0
            do i=1,3
            do j=1,natoms
              dot2 = dot2 + ppm(i,j)*dvec(i,j,nsurf,newsurf)
            enddo
            enddo
            if (dot1*dot2.gt.0.d0) then
!           reflect
            write(6,*)'FRUST! REFLECT'
            ra=2.0d0*ab
            do i=1,3
            do j=1,natoms
              ppm(i,j) = ppm(i,j) - dvec(i,j,nsurf,newsurf)*ra
            enddo
            enddo
            nhop(3) = nhop(3) + 1
            else
            write(6,*)'FRUST! IGNORE'
!           ignore
            nhop(4) = nhop(4) + 1
            endif
          else
            write(6,*)'FRUSMETH = ',frusmeth,' in HOPCHECK'
            stop
          endif
        endif

      end subroutine hop
