      subroutine rk4(y,dydx,nv,im)
!     Fourth order Runge-Kutta integrator. From numerical recipes.

      use param, only: mnyarray
      use c_sys, only: hstep

      implicit none
      integer :: nv,im
      double precision :: y(mnyarray),dydx(mnyarray)

      integer :: i
      double precision :: hh,h6,dum
      double precision :: yt(mnyarray),dym(mnyarray),dyt(mnyarray)

      hh=hstep/2.d0
      h6=hstep/6.d0
      dum=0.d0

!     first step
      do 11 i=1,nv
      yt(i)=y(i)+hh*dydx(i)
 11   continue

!     second step
      call derivs(dum,yt,dyt,nv,im)
      do 12 i=1,nv
      yt(i)=y(i)+hh*dyt(i)
 12   continue

!     third step
      call derivs(dum,yt,dym,nv,im)
      do 13 i=1,nv
      yt(i)=y(i)+hstep*dym(i)
      dym(i)=dyt(i)+dym(i)
 13   continue

!     fourth step
      call derivs(dum,yt,dyt,nv,im)
!     accumulate increments with proper weights
      do 14 i=1,nv
      y(i)=y(i)+h6*(dydx(i)+dyt(i)+2.d0*dym(i))
 14   continue

      end subroutine rk4
