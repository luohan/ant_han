      subroutine initrot(im)
!
! Setting initial rotation for an AG
!   Input:
!  xxm,ppm,mmm,natoms
! qnmrot0       : Rotation state for linear AG
! erot(im)      : Total rotation energy of an AG
! eroti(3,im)   : Three components of rotation energy of an AG
! momiprin(3)   : Three principal moment of inertia (in c_struct, cal by
!                 calling rotprin
! rotsel(im)    : 1: User provide; 0: Randomly generate (in c_sys.f)
! iflinear:     : 0: an atom; 1: Linear; 2; non-linear
!    Output:
!  new erot, eroti (if not rot selelect )
!  new ppm
!    Other variables
! vthet1:       angular rotation speed around x axis
! vthet2:       angular rotation speed around y axis
! vthet3:       angular rotation speed around z axis
! bigj0(3):     Angular momentum to be set
! bigjt:        Total angular momentum
      use param, only: amutoau, autoev
      use c_output, only: idebug, minprinticon
      use c_struct, only: xxm, mmm, natoms, momiprin, iflinear
      use c_initial, only: eroti, qnmrot0, rotsel, irottemp
      use c_traj, only: erot, ppm, kbt0, bigj

      implicit none
      integer :: im
! local
      integer :: i,j
      double precision :: tmp,tmp1,signs(3),bigjm(3),bigj0(3), &
                          bigjtm,bigjti,vthet1,vthet2,vthet3,ranm(4)
!
      if (iflinear.eq.1) then
!        User provide rotational state
         write(6,*) "Stateselect of rotation only for linear AG"
!        No rotational state provided, randomly generate one
         if (.not.rotsel(im)) then
            write(6,*) "Rotational state randomly decided"
!        erot generated: erot*kB*T, uniformly distributed on exp(-erot)
 3333       call get_rng(ranm(1))
            tmp1 = 1.d0-ranm(1)
            if (tmp1.eq.1.d0) goto 3333
!          tmp always < 0
            tmp = 2.0d0*momiprin(3)*kbt0*dlog(tmp1)
            qnmrot0(1,im) = int( (sqrt(1.d0-4.d0*tmp)-1.d0)/2.0d0 )
            write(6,*) "Rot. state generated: ",qnmrot0(1,im)
            write(6,*) "momiprin(3) ",momiprin(3)
            erot(im) = dble( qnmrot0(1,im)*(qnmrot0(1,im)+1) )/   &
                  ( 2.d0*momiprin(3) )
            write(6,106) "Rot. energy randomly generated: ", &
                  erot(im)*autoev,"eV"
         else
            write(6,*) "Rot. state provided by user: ",qnmrot0(1,im)
            write(6,106) "Rot. energy provided by user:   ", &
                  erot(im)*autoev,"eV"
         endif
!      Direction of the rotation
!        ranm(2) = sprng(rng_stream) RMP14
         call get_rng(ranm(2))
         if (ranm(2) .gt. 0.5) then
            signs(1)=1.d0
         else
            signs(1)=-1.d0
         endif
!      angular rotation speed theta
         vthet3 = signs(1)*sqrt( dble( qnmrot0(1,im)*   &
               (qnmrot0(1,im)+1) ) )/momiprin(3)
!        Since a linear molecule has been placed on x axis, atoms in
!        this AG will only have p vector perpendicular to x axis:
!        arbitrarily set it along y axis
         do i=1,natoms
            ppm(2,i) = ppm(2,i) + mmm(i)*xxm(1,i)*vthet3
         enddo
      elseif (iflinear.eq.0) then
         write(6,*) "Nothing to do for just one atom"
!     Nonlinear polyatom AG
      else
         if (.not.rotsel(im) .and. irottemp ) then
!        Rotational state determined by randmomly selected
!        Gaussian distribution around 0.5*kB*T
            write(6,*) "Thermal distribution of rotational momentum",  &
                  " for nonlinear polyatom AG"
            erot(im) = 0.d0
            do i=1,3
               ranm(i)=1.0
               do while(ranm(i).eq.1.0)
                  call get_rng(ranm(i))
               enddo
               eroti(i,im) = -0.5d0*dlog(1.d0-ranm(i) )*kbt0
               erot(im) = erot(im) + eroti(i,im)
            enddo
            write(6,100) "Rot. energy randomly generated: ", &
                  (eroti(j,im)*autoev,j=1,3),"eV"
         else
            write(6,100) "Rot. energy provided by user:   ", &
                  (eroti(j,im)*autoev,j=1,3)," eV"
         endif
         do i=1,3
            call get_rng(ranm(4))
            if (ranm(4) .gt. 0.5) then
               signs(i)=1.d0
            else
               signs(i)=-1.d0
            endif
!         Get the three angular momentum
            bigj0(i) = signs(i)*sqrt( 2.d0*eroti(i,im) * momiprin(i) )
            bigjm(i) = bigj0(i)+bigj(i)  !remove spurious angular momentum
         enddo
!  Call noang to add angular momentum
         write(6,100) "Angular mom set to",(-bigj0(i),i=1,3)
!        bigjti=sqrt(bigj0(1)**2+bigj0(3)**2+bigj0(3)**2)
         call noang(iflinear,bigjm,ppm,xxm,mmm,natoms)
!        vthet1=signs(1)*sqrt( 2.d0*eroti(1,im) / momiprin(1) )
!        vthet2=signs(2)*sqrt( 2.d0*eroti(2,im) / momiprin(2) )
!        vthet3=signs(3)*sqrt( 2.d0*eroti(3,im) / momiprin(3) )
!        do i=1,natoms
!           ppm(1,i)=ppm(1,i)+mmm(i)*(dabs(xxm(2,i))*vthet3+ &
!                     dabs(xxm(3,i))*vthet2)
!           ppm(2,i)=ppm(2,i)+mmm(i)*(dabs(xxm(3,i))*vthet1+ &
!                     dabs(xxm(1,i))*vthet3)
!           ppm(3,i)=ppm(3,i)+mmm(i)*(dabs(xxm(1,i))*vthet2+ &
!                     dabs(xxm(2,i))*vthet1)
!        enddo
      endif
!
      if (.not.minprinticon) then
!      if (idebug) then
         write(6,*) "Momenta after setting initial rotation"
         do i=1,natoms
            write(6,107) i,mmm(i)/amutoau,(ppm(j,i),j=1,3)
         enddo
         write(6,*)
      endif
!
 100  format(1x,a,3f12.5,1x,a)
 106  format(1x,a,f12.5,1x,a)
 107  format(1x,i5,f12.4,3f14.8)
!
      end subroutine initrot
