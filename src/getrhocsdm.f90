      subroutine getrhocsdm(cre,cim,rhor,rhoi,nsurft)

! Compute the density matrix from the electronic coefficients.
! Note:  Phases are handled separately, and RHOR and RHOI do
! not include phases.  These may need to be added to RHOR and RHOI,
! depending on how these quantities are used.

      use param, only: mnsurf
      implicit none

      integer :: nsurft,i,j,i2,j2
      double precision :: cim(2*mnsurf),cre(2*mnsurf), &
                          rhor(mnsurf,mnsurf),rhoi(mnsurf,mnsurf)

      do i=1,nsurft
       i2 = i + nsurft
        do j=1,nsurft
          j2 = j + nsurft
          rhor(i,j) = cre(i2)*cre(j2)+cim(i2)*cim(j2)
          rhoi(i,j) = cre(i2)*cim(j2)-cre(j2)*cim(i2)
        enddo
      enddo

      end subroutine getrhocsdm
