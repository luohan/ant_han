
!      subroutine volume(x,y,z,vol,rho,natom,maxatom)
      subroutine volume(vol,rho,maxatom)

!  vol = the volume in cubic Angstroms
!  rho = the density in g/mL
!  x,y,z = one dimensional arrays of the x,y,z coordinates
!  xxm(3,mnat) = three dimensional arrays of the x,y,z coordinates
!  coord = (3,natom) array of x,y,z coordinates
!  rad = 1d array with the van der Waals radius of Al

      use param, only: autoang
      use c_struct, only: xxm, natoms

      implicit none
      double precision vol,coord,rad,rho,volum
      integer maxatom,i,j,k 
      logical lsetup
      save lsetup
      dimension rad(maxatom)
      dimension coord(3,maxatom)

      if(lsetup .EQV. .false. ) then
         call setupvolume
         lsetup = .true.
         write(6,*) 'in setup'
      endif
 
      do i=1,natoms
        coord(1,i) = xxm(1,i)*autoang
        coord(2,i) = xxm(2,i)*autoang
        coord(3,i) = xxm(3,i)*autoang
        rad(i) = 2.35d0
      enddo

!      vol = volum(natom,rad,coord)
!      rho = dble(natom)*27.d0/vol
      vol = volum(natoms,rad,coord)
      rho = dble(natoms)*27.d0/vol
      rho = rho*1.66054d0

      end subroutine volume
