      subroutine adtod(im,job,denr,deni)

! Convert inital population on adiabatic state to diabatic state
! if diabatic representation is used
! it is written for HX2 case temporarily

      use param, only: mnsurf
      use c_struct, only: xxm, natom, nat
      use c_sys, only: nsurft, methflag, potflag, repflag
      use c_traj, only: cre, cim

      implicit none
      double precision :: FAC= 0.529177208D0   ! bohr to angstrom
      integer :: im,i,j,job
      double precision :: X(3*nat)
! arrays containing potential surfaces and Cartesian derivatives
      double precision :: UIJ(nsurft,nsurft),VI(nsurft)
      double precision :: guu(3,natom(im),nsurft,nsurft)
      double precision :: gvv(3,natom(im),nsurft)
      double precision :: dvectmp(3,natom(im),nsurft,nsurft)
      double precision :: cc(nsurft,nsurft),cinv(nsurft,nsurft)
      double precision :: cret(2*mnsurf),cimt(2*mnsurf)
! density matrix for diabatic representation
      double precision :: denr(mnsurf,mnsurf),deni(mnsurf,mnsurf)

! job = 0 : for initial condition 
! job = 1 : convert adiabatic density to diabatic density during trajectory

! debuging
      double precision :: rhor(mnsurf,mnsurf),rhoi(mnsurf,mnsurf)
      
!     repflag = 0
      if(potflag.eq.9) then
! Convert from bohr to angstrom
        do i=1,natom(im)
          do j = 1, 3
!          X(3*i-3+j)=xxm(j,i)*FAC
           X(3*i-3+j)=xxm(j,i)
          enddo
        enddo
        call pot(X,UIJ,VI,guu,gvv,dvectmp,cc)
      elseif(potflag.eq.10) then
        call pot(0,xxm,uij,guu,vi,gvv,dvectmp,cc,repflag)
      else
        write(6,*)'This potential is not added in adtod subroutine',&
          ' so the diabatic representation is still used for ',&
          'initial condition'
        return
      endif
!
! inverse of an orthogonal matrix is its transpose
      cinv(:,:) = transpose(cc(:,:))
! convert adiabatic population to diabatic population
      cret(:) = 0d0
      cimt(:) = 0d0
      do i = 1, nsurft
      do j = 1, nsurft
!       cret(i) = cret(i) + cre(j)*cinv(i,j)
        cret(i) = cret(i) + cre(j)*cinv(j,i)
        cimt(i) = cimt(i) + cim(j)*cinv(j,i)
      enddo
      enddo

      if(job.eq.0) then
        do i = 1, nsurft
         cre(i) = cret(i)
         if(methflag.eq.4) cre(i+nsurft) = cret(i)
        enddo
        write(6,'(a,a)') 'Initial electronic coefficients for ' &
           ,'diabatic states'
        do i = 1, nsurft
          write(6,'(i4,2es14.6,a)') i, cre(i),cim(i),'i'
        enddo

      elseif(job.eq.1) then
        call getrho(cret,cimt,denr,deni,nsurft)

      endif

      end subroutine adtod
