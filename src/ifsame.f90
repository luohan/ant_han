      subroutine ifsame(ifrag,im,issame)
! This subroutine judges if fragment ifrag is the same as the initial
! reactant im
!    Input:
! indatom,nattype (in common block c_struct.f)
! nattype(im,111)  Number of the atoms which are the same type     
! im:         count on reactants
! ifrag:      Count on fragments
!    Output:
! issame:     if the fragment is the same as AG im
      use c_struct, only: nattype, natinfrag, indatom, fragind
      implicit none
 
      integer ifrag,im
      logical issame
      integer i,ii,j,nattypem(111)
!
! Initialize
      issame=.true.
      do j=1,111
        nattypem(j)=0
      enddo
! Sum those same atoms
      do i=1,natinfrag(ifrag)
        ii=indatom(fragind(i,ifrag))
        nattypem(ii)= nattypem(ii)+1
      enddo
      do j=1,111
        if (nattype(im,j).ne.nattypem(j)) then
          issame=.false.
          goto 999
        endif
      enddo
! debug information
!      if (idebug) then
!         write(6,*) "Period ind,AG",im,"frag",ifrag
!         do j=1,111
!           write(6,*) j,nattype(im,j),nattypem(j)
!         enddo
!      endif
!
 999  continue
      end subroutine ifsame
