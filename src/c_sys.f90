module c_sys
! **********************************************************************
! SYSTEM
! COMMON BLOCKS FOR FREQUENTLY USED VARIABLES THAT ARE THE SAME FOR ALL
! TRAJECTORIES
! **********************************************************************
! Note: When "i" is an index, the array runs over atom groups,
!   and when "j" is an index, the array runs over atoms,
!   and when "k" is an index, the array runs over surfaces.
! **********************************************************************
! CONTROL VARIABLES
! **********************************************************************
! potflag    Flag that controls which potential interface to use
! ntraj      Number of trajectories
! methflag   Flag for which single surface or non-BO method is used
! tflag      Special trajectory options
! repflag    Representation flag, =0 for adiabatic, =1 for diabatic
! cparam     The constant C used for the CSDM or SCDM method
! e0param    The constant E0 used for the CSDM and SCDM method
! ieqtherm   Ensemble used for equilibrium run: 0: nve (default) 1: nvt;
!            2: npt
! itherm:    0: NVE; 1: NVT; 2: NPT
! temp0      Target temperature for the simulation
! dimen      Dimension of the system. Default: 3
! p0         Target pressure for the simulation
! vol0       Initial volumn of the system
! periodic   Periodic run
! itempzpe   if assign zero-point vibration at 0 K
! reactcol   Controls run reactive collision (1) or not (0, default)
! qcpack     QC package used for direct dynamics (1: Gaussian; 2:
!            Molpro)
!
      use param, only: mntraj
      integer :: potflag,ntraj,methflag,tflag(2),repflag,itherm,ieqtherm, &
                 nsurft,dimen,maxtraj,trajlist(mntraj),qcpack
      logical :: periodic,reactcol,itempzpe
      double precision :: temp0,p0,vol0,cparam,e0param
!
! **********************************************************************
! INTEGRATOR VARIABLES
! **********************************************************************
! hstep  = stepsize (in au)
! hstep0 = initial stepsize (input in fs, converted immediately to au)
! eps = converge energy for each step to this tolerance when using
!       the BS integrator (au)
! intflag = flag that controls which integrator to use

      double precision ::  hstep,hstep0,eps
      integer :: intflag
!
! **********************************************************************
! Thermal stat conditions
! **********************************************************************
! iadjtemp:   Temperature adjusting scheme
! iadjpress:  Pressure adjusting scheme
! taut:       Coupling parameter of Berendsen themostat (iadjtemp=0)
! taup:       Coupling parameter of Berendsen barostat (iadjpress=0)
! vfreq:      Collision frequency between bath atom and atom of the
!             system
! sysfreq:    Charactoristic vibration period of the system studied
!             (fs)

      integer :: iadjtemp,iadjpress
      double precision :: taut,taup,vfreq,sysfreq
!
! **********************************************************************
! SPECIAL TRAJECTORY OPTIONS
! **********************************************************************
! ramptime = ramp trajectory momentum every RAMPTIME time units 
!            (for TFLAG(1) = 2).  Read in fs, converted to au immediately.
! rampfact = ramp trajectory momentum by RAMPFACT (for TFLAG(1) = 2).
! nramp  = ramp trajectory momentum NRAMP times (for TFLAG(1) = 2).
! t_ramp = ramp trajectory momentum t_ramp fs (for TFLAG(1) = 2).
! tequil = equilibrate for tequil fs after ramp (for TFLAG(1) = 2).
! iadjtemp:  Method to adjust temperature
! taut:      Coupling parameter whose magnitude determines how tightly
!            the bath and the system are coupled together

      double precision :: rampfact
      integer :: nramp,nrampstep,n_ramp,nequil
end module c_sys
