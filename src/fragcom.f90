      subroutine fragcom(nfrdep,rcommin)
! Judge if two fragmentation occurs by calculating the radial CoM
! motion of the fragment. If the projection of CoM motion onto the CoM
! coordinate of the fragment is greater than zero, then it is leaving,
! else, it is not leaving, there should be no fragmentation
      use param, only: mnfrag
      use c_struct, only: nfrag, indfrdep, mmoffr, mmm, xxm, compfr, comp, &
                          natinfrag, fragind
      use c_sys, only: reactcol
      use c_traj, only: ppm
      use c_term, only: termflag

      implicit none
      integer :: i1,k,l, nfrdep
      double precision :: comx(3,mnfrag),compt(3),rcom,comv(3),tmp, &
                          comt,rcommin(mnfrag),rcomij(mnfrag,mnfrag)
 
      do nfrdep=1,nfrag
         indfrdep(nfrdep)=0
         rcommin(nfrdep)=1.d50
         rcomij(nfrdep,nfrdep)=0.d0
      enddo
      nfrdep=0
      do k=1,nfrag
        comx(1,k)=0.d0
        comx(2,k)=0.d0
        comx(3,k)=0.d0
        compt(1)=0.d0
        compt(2)=0.d0
        compt(3)=0.d0
        mmoffr(k)=0.d0
        do l=1,natinfrag(k)
         i1=fragind(l,k)
         mmoffr(k)=mmoffr(k)+mmm(i1)
         comx(1,k)=comx(1,k)+mmm(i1)*xxm(1,i1)
         comx(2,k)=comx(2,k)+mmm(i1)*xxm(2,i1)
         comx(3,k)=comx(3,k)+mmm(i1)*xxm(3,i1)
         compt(1)=compt(1)+ppm(1,i1)
         compt(2)=compt(2)+ppm(2,i1)
         compt(3)=compt(3)+ppm(3,i1)
        enddo
        compfr(1,k)=compt(1)
        compfr(2,k)=compt(2)
        compfr(3,k)=compt(3)
        comx(1,k)=comx(1,k)/mmoffr(k)
        comx(2,k)=comx(2,k)/mmoffr(k)
        comx(3,k)=comx(3,k)/mmoffr(k)
        rcom=comx(1,k)**2+comx(2,k)**2+comx(3,k)**2
        rcom=dsqrt(rcom)
!          write(6,*) k,comx(1),comx(2),comx(3)
!          write(6,*) k,compt(1),compt(2),compt(3)
!      direction vector of comx
        comv(1)=comx(1,k)/rcom
        comv(2)=comx(2,k)/rcom
        comv(3)=comx(3,k)/rcom
!        Dot compt onto CoM direction vector
        comt=compt(1)*comv(1)+compt(2)*comv(2)+compt(3)*comv(3)
!       com*comv: commppfr along the direction of comx
        if (comt.gt.1.d-5) then
                nfrdep=nfrdep+1
                indfrdep(nfrdep)=k
                comp(1,nfrdep)=2.d0*comt*comv(1)
                comp(2,nfrdep)=2.d0*comt*comv(2)
                comp(3,nfrdep)=2.d0*comt*comv(3)
        endif
      enddo
      if (reactcol.or.termflag.eq.7.or.termflag.eq.4) then
        do k=1,nfrag
          do l=k+1,nfrag
            rcomij(k,l)=dsqrt( (comx(1,k)-comx(1,l))**2 +  &
                               (comx(2,k)-comx(2,l))**2 +  &
                               (comx(3,k)-comx(3,l))**2 )
          enddo
          rcomij(l,k)=rcomij(k,l)
        enddo
        do k=1,nfrag
          do l=1,nfrag
            if (k.ne.l) rcommin(k)=min( rcomij(k,l),rcommin(k) )
          enddo
        enddo
        if (nfrag.eq.1) rcommin(nfrag)=0.d0
      endif
 
      end subroutine fragcom
