C***********************************************************************
C                        Analytical PES for ABC system
C***********************************************************************
C  This program determines the energy in eV (Sub. ESM) of a triatomic 
C  ABC potential by means of an analytical monovaluated Sorbie-Murrell 
C  function for a given geometry (RAB=R1,RBC=R2,RAC=R3) in A. First 
C  derivatives (eV/A) respect these distances and respect two distances
C  (R1, R2) and the angle formed (alpha) are also calculated  
C  (sub. D1SM)
C
C  The zero of energy is at A + B + C
C
C  The parameters of the PES are read from the input file (by means of 
C  sub. SMDATA).
C***********************************************************************
C                   VERSION  /11-5-2006/
C                          Dr. Ramon Sayos
C                   Dept. Quimica Fisica, Univ. Barcelona
C
C                   (first version: 1996)
C***********************************************************************
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION R(3),X(3),DER(3),D1VR(3)
      DATA PI/3.141592653589793D0/
      OPEN(UNIT=6,FILE='results.out',STATUS='UNKNOWN',FORM='FORMATTED')
c
c     PES parameters
c
      CALL SMDATA
c
c Read distances AB, BC and AC
c     
      READ(5,*) R(1),R(2),R(3)
c      
      CO=(R(1)**2+R(2)**2-R(3)**2)/(2.0D0*R(1)*R(2))
      IF(CO.GT.1.0D0.AND.CO.LT.1.1D0) CO=1.0D0
      IF(CO.GT.-1.1D0.AND.CO.LT.-1.0D0) CO=-1.0D0
      ALPHA=DACOS(CO)
c      
      WRITE(6,*)
      WRITE(6,*) 'R1,R2,R3 (A), ALPHA (deg):'
      WRITE(6,1) (R(J),J=1,3),ALPHA*180.0/PI
c
c Calculation of V (eV) at R(1),R(2),R(3)
c
      CALL ESM(R,OBJF)
      WRITE(6,*) 'energy (eV):'
      WRITE(6,1) OBJF 
C
c First derivatives of V respect R(1),R(2),R(3) (eV/A)
c
      CALL D1SM(R,D1VR)
c   
      WRITE(6,*)
      WRITE(6,*) 'derivatives respect to R1,R2,R3 (eV/A):'
      WRITE(6,1) D1VR(1),D1VR(2),D1VR(3)
c    
c First derivatives of R(3) respect a R(1),R(2),ALPHA
c
c
      R31=(R(1)-R(2)*DCOS(ALPHA))/R(3)
      R32=(R(2)-R(1)*DCOS(ALPHA))/R(3)
      R3A=R(1)*R(2)*DSIN(ALPHA)/R(3)
c
c First derivatives of V respect to R(1),R(2),ALPHA (eV/A,eV/rad)
c
      DER(1)=D1VR(1)+D1VR(3)*R31
      DER(2)=D1VR(2)+D1VR(3)*R32
      DER(3)=D1VR(3)*R3A
c
      WRITE(6,*)
      WRITE(6,*) 'derivatives respect to R1,R2,ALPHA (eV/A,eV/rad):'
      WRITE(6,1) DER(1),DER(2),DER(3)
c
      CLOSE(6)
      STOP
c
c Formats
c
    1 FORMAT(/,4(G12.6,1X),/)
      END
C
C***********************************************************************
C
C     This subroutine read the PES paramenters
C
C***********************************************************************
C
      SUBROUTINE SMDATA
      IMPLICIT REAL*8 (A-H,O-Z)
      CHARACTER*72 TITLE
      COMMON/PSORMU/VMO(3),AL(3),ROTAT1(3,3),DE(3),RE(3),CC(3,6),GA(3),
     &TH(0:11,0:11,0:11),ROTAT(3,3),RABC(3),V0,ISIM,NGRAD(3),NC
C
C Lectura de los parametros de los terminos monoatomicos
C
      READ(5,44) TITLE
      WRITE(6,45) TITLE
c      
      READ(5,*) (VMO(I),I=1,3)
      READ(5,*) (AL(I),I=1,3)
      WRITE(6,*)
      WRITE(6,34) 'VA,VB,VC (eV) =',(VMO(I),I=1,3)
      WRITE(6,35) 'AL(1),AL(2),AL(3) (A-1) =',(AL(I),I=1,3)
      WRITE(6,39)
      DO  I=1,3
       READ(5,*) (ROTAT1(I,J),J=1,3)
       WRITE(6,32) (ROTAT1(I,J),J=1,3)
      ENDDO
      WRITE(6,*)
C
C  Lectura de los parametros de los 3 terminos diatomicos (de R1 a R3)
C
      DO I=1,3
       READ(5,*) DE(I),RE(I),NGRAD(I)
       READ(5,*) (CC(I,J),J=1,NGRAD(I))
       WRITE(6,*)
       WRITE(6,43) I
       WRITE(6,*)
       WRITE(6,30) 'DE =',DE(I),'(eV)'
       WRITE(6,30) 'RE =',RE(I),' (A)'
       WRITE(6,*) '          GRADO DEL POLINOMIO =',NGRAD(I)
        DO  J=1,NGRAD(I)
         WRITE(6,36) I,J,CC(I,J),J
        ENDDO
      ENDDO
C
C  Lectura de los parametros del termino triatomico
C
      READ(5,*)  (RABC(I),I=1,3)
      WRITE(6,40)
      WRITE(6,37) (I,RABC(I),I=1,3)
c
      WRITE(6,38)
      DO I=1,3
        READ(5,*) (ROTAT(I,J),J=1,3)
        WRITE(6,32) (ROTAT(I,J),J=1,3)
      ENDDO
c
      READ(5,*) (GA(I),I=1,3)
      WRITE(6,*)
      WRITE(6,35) 'GA(1),GA(2),GA(3) (A-1) =',(GA(I),I=1,3)
      WRITE(6,*)
c
      READ(5,*) NC,ISIM
      WRITE(6,41) ISIM
      WRITE(6,42) NC
c
c   Lectura y escritura de los coeficientes del polinomio SM 
c   termino triatomico)
c
      CALL HERBST(2)
c
c  Formatos
c
   30 FORMAT(11X,A4,G12.6,A9)
   31 FORMAT(A72)
   32 FORMAT(11X,6(G12.6))
   34 FORMAT(11X,A15,3(1X,G12.6),/)
   35 FORMAT(11X,A25,3(1X,G12.6))
   36 FORMAT(11X,'CC(',I1,I2,')=', G12.6,'(A-',I1,')')
   37 FORMAT(11X,3('RABC(',I1,') =',G12.6),/)
   38 FORMAT(11X,'Matriz de las coordenadas de simetria del termino tria
     &tomico:',/)
   39 FORMAT(/,11X,'Matriz de las coordenadas de los terminos monoatomic
     &os:',/)
   40 FORMAT(/,11X,'Estructura de referencia (A) :',/)
   41 FORMAT(/,11X,'Opcion de coordenadas de simetria:',I2,/)
   42 FORMAT(/,11X,'Grado del polinomio:',I2)
   43 FORMAT(/,11X,'Parametros de la molecula diatomica No.',I2)
   44 FORMAT(A72)
   45 FORMAT(/,11X,A72) 
      RETURN
      END
C
C***********************************************************************
C
C SUBRUTINA QUE ESCRIBE O LEE LOS PARAMETROS EN NOTACION HERBST
C
C        NF= 0 ESCRIBE POR LA UNIDAD 6 LOS COEF. INDICANDO LA NOTACION
C              HERBST Y SM
C
C          = 1 ESCRIBE POR LA UNIDAD 7 LOS COEFICIENTES
C
C          = 2 LEE POR LA UNIDAD 5 LOS COEF.  (NOTACION  HERBST)
C              Y LOS ESCRIBE POR LA UNIDAD 6 INDICANDO AMBAS NOTACIONES
C              (despues de leerlos los multiplica por V0)
c
C***********************************************************************
C
      SUBROUTINE HERBST(NF)
      IMPLICIT REAL*8 (A-H,O-Z)
      CHARACTER*72 TITLE
      COMMON/PSORMU/VMO(3),AL(3),ROTAT1(3,3),DE(3),RE(3),CC(3,6),GA(3),
     &TH(0:11,0:11,0:11),ROTAT(3,3),RABC(3),V0,ISIM,NGRAD(3),NC
      DIMENSION IAC(3)
c
      IF(NF.EQ.0) THEN
      WRITE(6,*)
      WRITE(6,*) 'PARAMETROS OPTIMOS DEL POLINOMIO :'
      WRITE(6,*)
      WRITE(6,*) 'GAMMAS OPTIMOS:'
      WRITE(6,80) (GA(I),I=1,3)
      WRITE(6,*)
      WRITE(6,82) V0
      WRITE(6,*)
      ELSE
      IF(NF.EQ.1) THEN
      WRITE(7,80) (VMO(I),I=1,3)
      WRITE(7,80) (AL(I),I=1,3)
      DO I=1,3
         WRITE(7,80) (ROTAT1(I,J),J=1,3)
      ENDDO
      DO 601 I=1,3
      WRITE(7,81) DE(I),RE(I),NGRAD(I)
      WRITE(7,80) (CC(I,J),J=1,NGRAD(I))
  601 CONTINUE
      WRITE(7,80) V0,VA,VB,VC
      WRITE(7,80) (RABC(I),I=1,3)
      DO 611 I=1,3
      WRITE(7,80) (ROTAT(I,J),J=1,3)
  611 CONTINUE
      WRITE(7,80) (GA(I),I=1,3)
      WRITE(7,*) NC, ISIM
      WRITE(7,80) V0
      ELSE
      READ(5,*)  V0
      WRITE(6,82)  V0
      ENDIF
      ENDIF
      TH(0,0,0)=V0
      DO 10 I=1,3
         DO 101 IP=1,3
            IAC(IP)=0
101      CONTINUE
         DO 102 IP=1,3
            IF(I.EQ.IP) IAC(IP)=IAC(IP)+1
102      CONTINUE
         IF(NF.EQ.0) WRITE(6,731) I,IAC(1),IAC(2),IAC(3),
     *   TH(IAC(1),IAC(2),IAC(3))
         IF(NF.EQ.1) WRITE(7,80) TH(IAC(1),IAC(2),IAC(3))
         IF(NF.EQ.2) THEN
         READ(5,*)   TH(IAC(1),IAC(2),IAC(3))
         WRITE(6,731) I,IAC(1),IAC(2),IAC(3),
     *   TH(IAC(1),IAC(2),IAC(3))
         TH(IAC(1),IAC(2),IAC(3))=TH(IAC(1),IAC(2),IAC(3))*V0
         ENDIF
   10 CONTINUE
C
      IF(NC.EQ.1) GO TO 49
      DO 11 I=1,3
      DO 12 J=I,3
         DO 111 IP=1,3
            IAC(IP)=0
111      CONTINUE
         DO 112 IP=1,3
            IF(I.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(J.EQ.IP) IAC(IP)=IAC(IP)+1
112      CONTINUE
         IF(NF.EQ.0) WRITE(6,732) I,J,IAC(1),IAC(2),IAC(3),
     *   TH(IAC(1),IAC(2),IAC(3))
         IF(NF.EQ.1) WRITE(7,80) TH(IAC(1),IAC(2),IAC(3))
         IF(NF.EQ.2) THEN
         READ(5,*)   TH(IAC(1),IAC(2),IAC(3))
         WRITE(6,732) I,J,IAC(1),IAC(2),IAC(3),
     *   TH(IAC(1),IAC(2),IAC(3))
         TH(IAC(1),IAC(2),IAC(3))=TH(IAC(1),IAC(2),IAC(3))*V0
         ENDIF
   12 CONTINUE
   11 CONTINUE
C
      IF(NC.EQ.2) GO TO 49
      DO 13 I=1,3
      DO 14 J=I,3
      DO 15 K=J,3
         DO 131 IP=1,3
            IAC(IP)=0
131      CONTINUE
         DO 132 IP=1,3
            IF(I.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(J.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(K.EQ.IP) IAC(IP)=IAC(IP)+1
132      CONTINUE
         IF(NF.EQ.0) WRITE(6,733) I,J,K,IAC(1),IAC(2),IAC(3),
     *   TH(IAC(1),IAC(2),IAC(3))
         IF(NF.EQ.1) WRITE(7,80) TH(IAC(1),IAC(2),IAC(3))
         IF(NF.EQ.2) THEN
         READ(5,*)   TH(IAC(1),IAC(2),IAC(3))
         WRITE(6,733) I,J,K,IAC(1),IAC(2),IAC(3),
     *   TH(IAC(1),IAC(2),IAC(3))
         TH(IAC(1),IAC(2),IAC(3))=TH(IAC(1),IAC(2),IAC(3))*V0
         ENDIF
   15 CONTINUE
   14 CONTINUE
   13 CONTINUE
C
      IF(NC.EQ.3) GO TO 49
      DO 16 I=1,3
      DO 17 J=I,3
      DO 18 K=J,3
      DO 19 L=K,3
         DO 161 IP=1,3
            IAC(IP)=0
161      CONTINUE
         DO 162 IP=1,3
            IF(I.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(J.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(K.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(L.EQ.IP) IAC(IP)=IAC(IP)+1
162      CONTINUE
         IF(NF.EQ.0) WRITE(6,734) I,J,K,L,IAC(1),IAC(2),IAC(3),
     *   TH(IAC(1),IAC(2),IAC(3))
         IF(NF.EQ.1) WRITE(7,80) TH(IAC(1),IAC(2),IAC(3))
         IF(NF.EQ.2) THEN
         READ(5,*)   TH(IAC(1),IAC(2),IAC(3))
         WRITE(6,734) I,J,K,L,IAC(1),IAC(2),IAC(3),
     *   TH(IAC(1),IAC(2),IAC(3))
         TH(IAC(1),IAC(2),IAC(3))=TH(IAC(1),IAC(2),IAC(3))*V0
         ENDIF
   19 CONTINUE
   18 CONTINUE
   17 CONTINUE
   16 CONTINUE
C
      IF(NC.LT.5) GO TO 49
      DO 20 I=1,3
      DO 21 J=I,3
      DO 22 K=J,3
      DO 23 L=K,3
      DO 24 M=L,3
         DO 201 IP=1,3
            IAC(IP)=0
201      CONTINUE
         DO 202 IP=1,3
            IF(I.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(J.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(K.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(L.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(M.EQ.IP) IAC(IP)=IAC(IP)+1
202      CONTINUE
         IF(NF.EQ.0) WRITE(6,735) I,J,K,L,M,IAC(1),IAC(2),IAC(3),
     *   TH(IAC(1),IAC(2),IAC(3))
         IF(NF.EQ.1) WRITE(7,80) TH(IAC(1),IAC(2),IAC(3))
         IF(NF.EQ.2) THEN
         READ(5,*)   TH(IAC(1),IAC(2),IAC(3))
         WRITE(6,735) I,J,K,L,M,IAC(1),IAC(2),IAC(3),
     *   TH(IAC(1),IAC(2),IAC(3))
         TH(IAC(1),IAC(2),IAC(3))=TH(IAC(1),IAC(2),IAC(3))*V0
         ENDIF
   24 CONTINUE
   23 CONTINUE
   22 CONTINUE
   21 CONTINUE
   20 CONTINUE
C
      IF(NC.LT.6) GO TO 49
      DO 25 I=1,3
      DO 26 J=I,3
      DO 27 K=J,3
      DO 28 L=K,3
      DO 29 M=L,3
      DO 30 N1=M,3
         DO 251 IP=1,3
            IAC(IP)=0
251      CONTINUE
         DO 252 IP=1,3
            IF(I.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(J.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(K.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(L.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(M.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(N1.EQ.IP) IAC(IP)=IAC(IP)+1
252      CONTINUE
         IF(NF.EQ.0) WRITE(6,736) I,J,K,L,M,N1,IAC(1),IAC(2),IAC(3),
     *   TH(IAC(1),IAC(2),IAC(3))
         IF(NF.EQ.1) WRITE(7,80) TH(IAC(1),IAC(2),IAC(3))
         IF(NF.EQ.2) THEN
         READ(5,*)   TH(IAC(1),IAC(2),IAC(3))
         WRITE(6,736) I,J,K,L,M,N1,IAC(1),IAC(2),IAC(3),
     *   TH(IAC(1),IAC(2),IAC(3))
         TH(IAC(1),IAC(2),IAC(3))=TH(IAC(1),IAC(2),IAC(3))*V0
         ENDIF
 30   CONTINUE
 29   CONTINUE
 28   CONTINUE
 27   CONTINUE
 26   CONTINUE
 25   CONTINUE
C
      IF(NC.LT.7) GO TO 49
      DO  31 I=1,3
      DO  32 J=I,3
      DO  33 K=J,3
      DO  34 L=K,3
      DO  35 M=L,3
      DO  36 N1=M,3
      DO  37 N2=N1,3
         DO 311 IP=1,3
            IAC(IP)=0
311      CONTINUE
         DO 312 IP=1,3
            IF(I.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(J.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(K.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(L.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(M.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(N1.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(N2.EQ.IP) IAC(IP)=IAC(IP)+1
312      CONTINUE
         IF(NF.EQ.0) WRITE(6,737) I,J,K,L,M,N1,N2,IAC(1),IAC(2),IAC(3),
     *   TH(IAC(1),IAC(2),IAC(3))
         IF(NF.EQ.1) WRITE(7,80) TH(IAC(1),IAC(2),IAC(3))
         IF(NF.EQ.2) THEN
         READ(5,*)   TH(IAC(1),IAC(2),IAC(3))
         WRITE(6,737) I,J,K,L,M,N1,N2,IAC(1),IAC(2),IAC(3),
     *   TH(IAC(1),IAC(2),IAC(3))
         TH(IAC(1),IAC(2),IAC(3))=TH(IAC(1),IAC(2),IAC(3))*V0
         ENDIF
 37   CONTINUE
 36   CONTINUE
 35   CONTINUE
 34   CONTINUE
 33   CONTINUE
 32   CONTINUE
 31   CONTINUE
C
      IF(NC.LT.8) GO TO 49
      DO  38 I=1,3
      DO  39 J=I,3
      DO  40 K=J,3
      DO  41 L=K,3
      DO  42 M=L,3
      DO  43 N1=M,3
      DO  44 N2=N1,3
      DO  45 N3=N2,3
         DO 381 IP=1,3
            IAC(IP)=0
381      CONTINUE
         DO 382 IP=1,3
            IF(I.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(J.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(K.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(L.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(M.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(N1.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(N2.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(N3.EQ.IP) IAC(IP)=IAC(IP)+1
382      CONTINUE
         IF(NF.EQ.0) WRITE(6,738) I,J,K,L,M,N1,N2,N3,IAC(1),IAC(2),
     *   IAC(3),TH(IAC(1),IAC(2),IAC(3))
         IF(NF.EQ.1) WRITE(7,80) TH(IAC(1),IAC(2),IAC(3))
         IF(NF.EQ.2) THEN
         READ(5,*)   TH(IAC(1),IAC(2),IAC(3))
         WRITE(6,738) I,J,K,L,M,N1,N2,N3,IAC(1),IAC(2),
     *   IAC(3),TH(IAC(1),IAC(2),IAC(3))
         TH(IAC(1),IAC(2),IAC(3))=TH(IAC(1),IAC(2),IAC(3))*V0
         ENDIF
 45   CONTINUE
 44   CONTINUE
 43   CONTINUE
 42   CONTINUE
 41   CONTINUE
 40   CONTINUE
 39   CONTINUE
 38   CONTINUE
C
      IF(NC.LT.9) GO TO 49
      DO  46 I=1,3
      DO  47 J=I,3
      DO  48 K=J,3
      DO  491 L=K,3
      DO  50 M=L,3
      DO  51 N1=M,3
      DO  52 N2=N1,3
      DO  53 N3=N2,3
      DO  54 N4=N3,3
         DO 461 IP=1,3
            IAC(IP)=0
461      CONTINUE
         DO 462 IP=1,3
            IF(I.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(J.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(K.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(L.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(M.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(N1.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(N2.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(N3.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(N4.EQ.IP) IAC(IP)=IAC(IP)+1
462      CONTINUE
         IF(NF.EQ.0) WRITE(6,739) I,J,K,L,M,N1,N2,N3,N4,IAC(1),IAC(2),
     *   IAC(3),TH(IAC(1),IAC(2),IAC(3))
         IF(NF.EQ.1) WRITE(7,80) TH(IAC(1),IAC(2),IAC(3))
         IF(NF.EQ.2) THEN
         READ(5,*)   TH(IAC(1),IAC(2),IAC(3))
         WRITE(6,739) I,J,K,L,M,N1,N2,N3,N4,IAC(1),IAC(2),
     *   IAC(3),TH(IAC(1),IAC(2),IAC(3))
         TH(IAC(1),IAC(2),IAC(3))=TH(IAC(1),IAC(2),IAC(3))*V0
         ENDIF
 54   CONTINUE
 53   CONTINUE
 52   CONTINUE
 51   CONTINUE
 50   CONTINUE
 491  CONTINUE
 48   CONTINUE
 47   CONTINUE
 46   CONTINUE
C
      IF(NC.LT.10) GO TO 49
      DO  55 I=1,3
      DO  56 J=I,3
      DO  57 K=J,3
      DO  58 L=K,3
      DO  59 M=L,3
      DO  60 N1=M,3
      DO  61 N2=N1,3
      DO  62 N3=N2,3
      DO  63 N4=N3,3
      DO  64 N5=N3,3
         DO 551 IP=1,3
            IAC(IP)=0
551      CONTINUE
         DO 552 IP=1,3
            IF(I.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(J.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(K.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(L.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(M.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(N1.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(N2.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(N3.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(N4.EQ.IP) IAC(IP)=IAC(IP)+1
            IF(N5.EQ.IP) IAC(IP)=IAC(IP)+1
552      CONTINUE
         IF(NF.EQ.0) WRITE(6,740) I,J,K,L,M,N1,N2,N3,N4,N5,IAC(1),IAC(2)
     *   ,IAC(3),TH(IAC(1),IAC(2),IAC(3))
         IF(NF.EQ.1) WRITE(7,80) TH(IAC(1),IAC(2),IAC(3))
         IF(NF.EQ.2) THEN
         READ(5,*)   TH(IAC(1),IAC(2),IAC(3))
         WRITE(6,740) I,J,K,L,M,N1,N2,N3,N4,N5,IAC(1),IAC(2)
     *   ,IAC(3),TH(IAC(1),IAC(2),IAC(3))
         TH(IAC(1),IAC(2),IAC(3))=TH(IAC(1),IAC(2),IAC(3))*V0
         ENDIF
 64   CONTINUE
 63   CONTINUE
 62   CONTINUE
 61   CONTINUE
 60   CONTINUE
 59   CONTINUE
 58   CONTINUE
 57   CONTINUE
 56   CONTINUE
 55   CONTINUE
   49 CONTINUE
   80 FORMAT(11X,6(G12.6,1X))
   82 FORMAT(11X,'V0 =',G12.6)
   81 FORMAT(1X,2(G12.6,1X),I3)
  331 FORMAT(A72)
 731  FORMAT(11X,'CC1(',I2,')',
     *' OR ','C(',I2,',',I2,',',I2,')=',G15.8)
 732  FORMAT(11X,'CC2(',I2,',',I2,')',
     *' OR ','C(',I2,',',I2,',',I2,')=',G15.8)
 733  FORMAT(11X,'CC3(',2(I2,','),I2,')',
     *' OR ','C(',I2,',',I2,',',I2,')=',G15.8)
 734  FORMAT(11X,'CC4(',3(I2,','),I2,')',
     *' OR ','C(',I2,',',I2,',',I2,')=',G15.8)
 735  FORMAT(11X,'CC5(',4(I2,','),I2,')',
     *' OR ','C(',I2,',',I2,',',I2,')=',G15.8)
 736  FORMAT(11X,'CC6(',5(I2,','),I2,')',
     *' OR ','C(',I2,',',I2,',',I2,')=',G15.8)
 737  FORMAT(11X,'CC7(',6(I2,','),I2,')',
     *' OR ','C(',I2,',',I2,',',I2,')=',G15.8)
 738  FORMAT(11X,'CC8(',7(I2,','),I2,')',
     *' OR ','C(',I2,',',I2,',',I2,')=',G15.8)
 739  FORMAT(11X,'CC9(',8(I2,','),I2,')',
     *' OR ','C(',I2,',',I2,',',I2,')=',G15.8)
 740  FORMAT(11X,'CC10(',9(I2,','),I2,')',
     *' OR ','C(',I2,',',I2,',',I2,')=',G15.8)
      RETURN
      END
C
C***********************************************************************
C
C  This subroutine determines the energy in eV of a triatomic ABC 
C  potential by means of an analytical monovaluated Sorbie-Murrell 
C  function at a given geometry (RAB,RBC,RAC)
C                 (units: eV y A)
C
C***********************************************************************
C
      SUBROUTINE ESM(R,OBJF)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/PSORMU/VMO(3),AL(3),ROTAT1(3,3),DE(3),RE(3),CC(3,6),GA(3),
     *TH(0:11,0:11,0:11),ROTAT(3,3),RABC(3),V0,ISIM,NGRAD(3),NC
      COMMON/MAGNIS1/RD(3),S(3),SS(3),V1,V2,V12,V(3),V3,PO,T(3)
     *,PS(-2:10,3),H1(3),S1(3),VM(3),ENER
      DIMENSION R(3)
C
C Calculo de los desplazamientos internucleares respecto a las diatomicas
C y a la estructura de referencia
C
      DO I=1,3
       RD(I)=R(I)-RE(I)
       S(I)=R(I)-RABC(I)
      ENDDO
C
C  Calculo de los potenciales monoatomicos:
C
      DO I=1,3
         S1(I)=0.0D0
          DO J=1,3
             S1(I)=S1(I)+S(J)*ROTAT1(I,J)
          ENDDO
      ENDDO
c
      DO I=1,3
        B1=AL(I)*S1(I)/2.0D0
        B2=DEXP(-B1)
        B3=DEXP(B1)+DEXP(-B1)
        H1(I)=B2/B3
        VM(I)=VMO(I)*H1(I)
      ENDDO
C
C  Calculo de los potenciales diatomicos:
C
      DO I=1,3
       AUX=DE(I)*DEXP(-CC(I,1)*RD(I))
       AUX2=1.0D0
        DO J=1,NGRAD(I)
         AUX2=AUX2+CC(I,J)*RD(I)**J
        ENDDO
       V(I)=-AUX*AUX2
      ENDDO
C
C  Calculo del potencial triatomico:
C
C  Coordenadas de simetria
C
      DO I=1,3
         SS(I)=0.0D0
          DO  J=1,3
               SS(I)=SS(I)+S(J)*ROTAT(I,J)
          ENDDO
      ENDDO
      IF(ISIM.EQ.1) THEN
        DO I=1,3
           S(I)=SS(I)
        ENDDO
      ENDIF
C
C  Contribucion de los terminos mono- y diatomicos al potencial
C
      V1=VM(1)+VM(2)+VM(3)
      V2=V(1)+V(2)+V(3)
      V12=V1+V2
c
c   Calculo de la funcion de alcance T
c   T(I)=(1.0D0-DTANH(GA(I)*SS(I)/2.0D0))
c
      DO I=1,3
        A1=GA(I)*SS(I)/2.0D0
        A2=DEXP(-A1)
        A3=DEXP(A1)+DEXP(-A1)
        T(I)=((2.0D0*A2)/A3)
      ENDDO
c
c   Calculo de las potencias de S(I)**J
c
      PS(-2,1)=0.0D0
      PS(-2,2)=0.0D0
      PS(-2,3)=0.0D0
      PS(-1,1)=0.0D0
      PS(-1,2)=0.0D0
      PS(-1,3)=0.0D0
c
      DO I=0,NC
         DO J=1,3
            PS(I,J)=S(J)**I
         ENDDO
      ENDDO
c
c   Calculo del termino triatomico (algoritmo optimizado por R.Sayos)
c
         PO=0.0D0
         DO 10 K=0,NC
           DO 20 L=0,NC-K
             DO 30 M=0,NC-K-L
                       PO=PO+TH(K,L,M)*PS(K,1)*PS(L,2)*PS(M,3)
 30          CONTINUE
 20        CONTINUE
 10     CONTINUE
c
         PO=PO/TH(0,0,0)
         V3=TH(0,0,0)*PO*T(1)*T(2)*T(3)
c
      ENER=V1+V2+V3
      OBJF=ENER
      RETURN
      END
C
C***********************************************************************
C
C  This subroutine computes the gradient (D1VR(I), I=1,3) respect
C  R(1),R(2),R(3)  (eV/A)
C
C
C***********************************************************************
C
      SUBROUTINE D1SM(R,D1VR)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/PSORMU/VMO(3),AL(3),ROTAT1(3,3),DE(3),RE(3),CC(3,6),GA(3),
     *TH(0:11,0:11,0:11),ROTAT(3,3),RABC(3),V0,ISIM,NGRAD(3),NC
      COMMON/MAGNIS1/RD(3),S(3),SS(3),V1,V2,V12,V(3),V3,PO,T(3)
     *,PS(-2:10,3),H1(3),S1(3),VM(3),ENER
      COMMON/MAGNIS2/DH1(3,3),DVM(3,3),DV(3,3),DT(3,3),DPO(3),DS(3,3),
     *DV3(3),DDH1(3,3,3),DDVM(3,3,3),DDV(3,3),DDT(3,3,3),DDPO(3,3),
     *DDV3(3,3)
      DIMENSION R(3),D1VR(3)
C
C  Some parameters of common MAGNIS1 are calculated
C
      CALL ESM(R,OBJF)
C
C
C Inicializacion de algunas variables
C
      DO I=1,3
        DO J=1,3
         DVM(I,J)=0.0D0
         DV(I,J)=0.0D0
         DS(I,J)=0.0D0
        ENDDO
        DS(I,I)=1.0D0
      ENDDO
      IF(ISIM.EQ.1) THEN
       DO I=1,3
        DO J=1,3
         DS(I,J)=ROTAT(I,J)
        ENDDO
       ENDDO
      ELSE
       CONTINUE
      ENDIF
c
c Derivadas primeras de los terminos monoatomicos:
c
      DO I=1,3
       DO J=1,3
        DH1(I,J)=(-AL(I)*ROTAT1(I,J)/4.0D0)/((DCOSH(AL(I)*S1(I)/2.0D0
     &           ))**2)
        DVM(I,J)=VMO(I)*DH1(I,J)
       ENDDO
      ENDDO
c
c Derivadas primeras de los terminos diatomicos:
c
      DO I=1,3
       AUX=0.0D0
         DO K=1,NGRAD(I)
          AUX=AUX+DBLE(K)*CC(I,K)*RD(I)**(K-1)
         ENDDO
       DV(I,I)=-DE(I)*AUX*DEXP(-CC(I,1)*RD(I))-CC(I,1)*V(I)
      ENDDO
c
c Derivadas primeras del termino triatomico:
c
c
c Derivadas primeras de la funcion de alcance
c
      DO I=1,3
        DO J=1,3
       DT(I,J)=(-GA(I)*ROTAT(I,J)/2.0D0)/((DCOSH(GA(I)*SS(I)/2.0D0))**2)
        ENDDO
      ENDDO
c
c Derivadas primeras del polinomio (algoritmo optimizado por R.Sayos)
c
      IF(NC.LT.1) GO TO 55
c
      DO 10 I=1,3
        DPO(I)=0.0D0
C
        DO 11 K=0,NC
         DO 12 L=0,NC-K
            DO 13 M=0,NC-K-L
             DPO(I)=DPO(I)+TH(K,L,M)*(
     &              DBLE(K)*PS(K-1,1)*DS(1,I)*PS(L,2)*PS(M,3)
     &              +PS(K,1)*DBLE(L)*PS(L-1,2)*DS(2,I)*PS(M,3)
     &              +PS(K,1)*PS(L,2)*DBLE(M)*PS(M-1,3)*DS(3,I))
13           CONTINUE
12         CONTINUE
11        CONTINUE
c
c   Se divide por TH(0,0,0) ya que los coeficientes estaban multiplicados por
c   este parametro
c
          DPO(I)=DPO(I)/TH(0,0,0)
c
   10 CONTINUE
   55 CONTINUE
c
c Derivadas primeras del termino triatomico
c
      DO 14 I=1,3
      DV3(I)=TH(0,0,0)*(DPO(I)*T(1)*T(2)*T(3)+PO*DT(1,I)*T(2)*T(3)+
     *PO*T(1)*DT(2,I)*T(3)+PO*T(1)*T(2)*DT(3,I))
C
C Derivadas primeras de V respecto a R(1),R(2),R(3) (eV/A)
C
      D1VR(I)=DVM(1,I)+DVM(2,I)+DVM(3,I)+DV(I,I)+DV3(I)
   14 CONTINUE
      RETURN
      END