
# coding: utf-8

#!/usr/bin/python
import os,re,copy
import numpy as np
from scipy.io import savemat,loadmat
import matplotlib.pyplot as plt
import pandas as pd
#%% constants
numre = re.compile(r'-?\d+(?:\.\d+)?(?:E-?\d+)?')
#fort221type = {'name': ['TrjID','Time','AveP','P1','P2'], \
#       'usecols':(0,1,3,4,5),'dtype':[np.int,]+[np.float,]*4}

#%% function to calculate rovibrational energy
def CalculateVJ(xx,pp,mm,ii,ij,irel,tot,pe,elec):
    ev2hart=0.0367502
    kcal2ev=0.0433634
    bohr2a=0.52917721067
    xx = xx/bohr2a
    if elec == 2:
        if irel == 0:
            easym = -228.41*kcal2ev
            rasym = 1.1034
        else:
            easym = -152.53*kcal2ev
            rasym = 1.1598
    else:
        if irel == 0:
            easym = -7.1528
            rasym = 1.0977
        else:
            easym = -3.4671
            rasym = 1.1508
        easym = easym- 2.1133 + 45.37*kcal2ev

    easym = easym*ev2hart
    rasym = rasym/bohr2a
    autoev = 27.21138602
    autoamu= 5.4857990907E-4
    amutoau = 1.0/autoamu
    mm = [i*amutoau for i in mm]
    tot = tot/autoev
    pe = pe/autoev
    jq = [0.0,]*9
    jp = jq[:]
    jm = jp[:]
    m2 = mm[ii]+mm[ij]
    m1 = sum(mm)
    for i in range(3):
        jq[i] = xx[ij,i]-xx[ii,i]
        jq[i+3] = xx[irel,i] - (mm[ii]*xx[ii,i]+mm[ij]*xx[ij,i])/m2
        jq[i+6] = (mm[irel]*xx[irel,i]+mm[ij]*xx[ij,i]+mm[ii]*xx[ii,i])/m1

        jp[i] = -mm[ij]/m2*pp[ii,i]+mm[ii]/m2*pp[ij,i]
        jp[i+3] = m2/m1*pp[irel,i]-mm[irel]*(pp[ii,i]+pp[ij,i])/m1
        jp[i+6] = pp[irel,i]+pp[ii,i]+pp[ij,i]
        jm[i] = mm[ii]*mm[ij]/m2
        jm[i+3] = mm[irel]*m2/m1
        jm[i+6] = m1
    kerel = 0.
    kedia = 0.
    for i in range(3):
        kedia = kedia + jp[i]**2/(2.*jm[i]) # this quantity oscillates as the diatom vibrates and rotates
        kerel = kerel + jp[i+3]**2/(2.*jm[i+3]) #this quantity converges as the fragments separate
    erottot = tot - kedia - kerel - pe # should be near zero for total angular momentum zero
    ediatot = pe + kedia #total diatomic energy
    eint = ediatot - easym
    xjx = jq[1]*jp[2]-jq[2]*jp[1]
    xjy = jq[2]*jp[0]-jq[0]*jp[2]
    xjz = jq[0]*jp[1]-jq[1]*jp[0]
    xj = np.sqrt(xjx*xjx+xjy*xjy+xjz*xjz) # rotational quantum number is magnitude of RxP
    xj = xj-0.5 # Bohr-Sommerfeld conditions
    xj = max(0.0,xj)
    erotm = (xj+0.5)**2/(2.0*rasym**2*jm[0]) # assume separability
    evibm = eint - erotm
    return (xj,erotm/ev2hart,evibm/ev2hart,[xjx,xjy,xjz])


#%% defination of single trjectory
class SingleTrj(object):
    def __init__(self,ID):
        self.ID = ID
    def initmol(self,qmode,ke0,pe0,ang0):
        self.qmode = qmode
        self.ke0=ke0
        self.pe0=pe0
        self.tote0=ke0+pe0
        self.ang0=ang0
        self.ee = None
    def assign_hop(self,HOP,HOPC):
        self.HOP=HOP
        self.HOPC=HOPC
    def finalstate(self,bond,elec,tf):
        self.bond = bond
        self.elec = elec
        self.tf = tf #reaction time
    def assign_221(self,energy):  #221 file
        self.ee = energy
    def assign_2021(self,xf,pef,pf,kef):
        self.xf = xf        # in angstrom
        self.pef = pef      # in eV
        self.pf = pf        # in a.u.
        self.kef = kef      # in eV
    def assign_VJ(self,ireac,erot=0.0,evib=0.0,J=[0.0]*3):
        self.erot = erot
        self.evib = evib
        self.ireac = ireac
        self.J = J

#%% defination of multitrj
class MultiTrj(object):
    def __init__(self,nmol,mass,ntrj):
        self.nmol = nmol
        self.mass = mass
        self.ntrj = ntrj
        self.DAT = [SingleTrj(i+1) for i in range(ntrj)]

    def __len__(self):
        return self.ntrj

    def __getitem__(self,n):
        return self.DAT[n]

    def __setitem__(self,n,value):
        if len(value) != 4:
            raise ValueError('Incorrect number of input for set SingleTrj')
        self.DAT[n].initmol(value[0],value[1],value[2],value[3])

    def __iter__(self):
        return iter(self.DAT)

    def append(self,MTrj):
        self.DAT += MTrj.DAT
        for i in range(len(MTrj)):
            self.DAT[i+self.ntrj].ID = self.ntrj+i+1
        self.ntrj += MTrj.ntrj

    # only call this function before concate trajectories
    def read_221(self,folder):
        fname = os.path.join(folder,'fort.221')
     #  data = np.genfromtxt(fname,usecols=(0,1,3,4,5))  #two slow, not acceptable!
        data = pd.read_csv(fname,usecols=[0,1,3,4,5],sep='\s+').values

        localntrj = int(data[-1][0])
        if localntrj != self.ntrj:
            raise ValueError('fort221 doesn''t have enough trjs %d/%d'%(localntrj,self.ntrj))
        head = 0
        PTrjID = 1.0
        for i,ID in enumerate(data[1:,0]):
            if ID != PTrjID:
                self.DAT[int(PTrjID-1)].assign_221(data[head:i,1:])
                head = i
                PTrjID = ID
        self.DAT[int(PTrjID-1)].assign_221(data[head:,1:])

    def readoutput(self,fname,NVIB):
        with open(fname,'r') as f:
            line = f.readline()
            while line:
                line = f.readline()
                if '***TRAJECTORY' in line:
                    TrjID=int(re.findall(r'\d+',line)[0])
                    TrjIndex = TrjID-1
                 #   print('Reading output %d/%d trj'%(TrjID,self.ntrj))
                    while True:
                        line = f.readline()
                        if ('Mode' in line) and ('Q. Number' in line) :
                            qmode = [np.nan]*NVIB
                            line = f.readline()
                            while 'HO approximation' not in line:
                                line = line.split()
                                qmode[int(line[0])-1] = float(line[1])
                                line = f.readline()
                        elif ' Remove overall angular momentum' in line:
                            ang0 = float(numre.findall(f.readline())[0])
                            ke0 = float(numre.findall(f.readline())[0])
                            pe0 = float(numre.findall(f.readline())[0])
                        elif 'time(fs)' in line:
                            HOP = []
                     #       HOP_energy=[]
                            while ('Reaction occurs' not in line) and ('Final information' not in line):
                                line = f.readline()
                                if 'HOP!' in line:
                                    hop_info = list(map(float,numre.findall(line)[0:-1]))  # p -> q time
                       #             hop_info2 = list(map(float,numre.findall(f.readline())[1:]))
                                    hop_info2 = list(map(float,numre.findall(f.readline())))  # somthing
                                 #   hop_info=hop_info+hop_info2
                                    HOP.append(hop_info)
                         #           HOP_energy.append(np.array([hop_info[2],\
                         #                                       hop_info[int(hop_info[0])+2],\
                         #                                       hop_info[3],\
                         #                                       hop_info[4]]))
                         #   HOP_energy=np.asarray(HOP_energy)
                         #     temp=np.vstack((self.DAT[TrjIndex].ee,HOP_energy))
                         #     self.DAT[TrjIndex].ee = temp[temp[:,0].argsort()]
                            HOP = np.asarray(HOP)
                            if 'Reaction occurs' in line:
                                reaction_time = float(numre.findall(line)[0])
                            else:  #time is after final information for trajectory
                                reaction_time = float(f.readline().split()[0])
                        elif  '...entering FINALSTATE...' in line:
                            while 'exiting FINALSTATE' not in line:
                                line = f.readline()
                                if 'Initial electronic' in line:
                                    ## get final electronic state
                                    e0 = int(line.split('=')[1])
                                    line = f.readline()
                                    ef = int(line.split('=')[1])
                                    elec = [e0,ef]
                                elif 'Final stats on hopping events'  in line:
                                    HOPcount = list(map(int,numre.findall(line)))
                                elif 'Fragment analysis information' in line:
                                    num_frag = int(f.readline().split(':')[1])
                                    bond = []
                                    for i in range(num_frag):
                                        frag_bond = []
                                        for j in range(5):   # header of fragment analyzing
                                            f.readline()
                                        line = f.readline()
                                        while '----' not in line:
                                            frag_bond.append(int(line.split()[1]))
                                            line = f.readline()
                                        bond.append(frag_bond)
                                    if num_frag != len(bond):
                                        raise ValueError('Something wrong with fragment analysis')
                            break #break the infinite loop when we meet exiting finalstate
                    self.DAT[TrjIndex].initmol(qmode=qmode,ke0=ke0,pe0=pe0,ang0=ang0)
                    self.DAT[TrjIndex].finalstate(bond=bond,elec=elec,tf=reaction_time)
                    self.DAT[TrjIndex].assign_hop(HOP=HOP,HOPC=HOPcount)
    def readfort2021(self,folder):
        xfile = os.path.join(folder,'fort.20')
        pfile = os.path.join(folder,'fort.21')
        f_x = open(xfile,'r')
        f_p = open(pfile,'r')

        for i in range(self.ntrj):
            xline = f_x.readline().split()
            pline = f_p.readline().split()
            TrjID = int(xline[0])
            if (i != TrjID-1) or (TrjID != int(pline[0])):
                raise ValueError('fort20 index wrong %d'%(TrjID,))
            pe = float(xline[1])
            ke = float(pline[1])
            coord = []
            mom = []
            for j in range(self.nmol):
                coord.append( list(map(float,f_x.readline().split()[2:])))
                mom.append(list(map(float,f_p.readline().split()[2:])))
            coord = np.asarray(coord)
            mom = np.asarray(mom)
            self.DAT[i].assign_2021(coord,pe,mom,ke)
        f_x.close()
        f_p.close()

    def VJ(self):
        for i,Trj in enumerate(self.DAT):
            if len(Trj.bond) == 1:
                ireac = 0
                Trj.assign_VJ(ireac)
            elif len(Trj.bond) == 3:
                ireac = 4
                Trj.assign_VJ(ireac)
            else:
                temp = [len(i) for i in Trj.bond]
                single_bond = temp.index(min(temp))
                multi_bond = 1-single_bond
                irel = Trj.bond[single_bond][0]-1
                ii = Trj.bond[multi_bond][0]-1
                ij = Trj.bond[multi_bond][1]-1

                ireac = irel +1 #O-NN', N-ON', N'-ON

                tot = Trj.kef+Trj.pef
                (postJ,erot,evib,J)=CalculateVJ(Trj.xf,Trj.pf,\
                            self.mass,ii,ij,irel,tot,Trj.pef,Trj.elec[1])
                Trj.assign_VJ(ireac=ireac,erot=erot,evib=evib,J=J)
    def savemat(self,fname,npack,compact=True,additiondic=None):
        ## compact form won't save ee
        temp = self[0].__dict__
        if compact is True:
            ff = [i for i in sorted(list(temp.keys())) if (i!= 'ID') and (i!='bond') and (i != 'ee')]
        else:
            ff = [i for i in sorted(list(temp.keys())) if (i!= 'ID') and (i!='bond')]
        dtype = [(i,float) for i in ff]
    # 'HOP',  'HOPC',  'ID',  'J',  'ang0',  'bond',  'ee',  'elec',
    # 'erot',  'evib',  'ireac',  'ke0',  'kef',  'pe0',  'pef', 'pf',  'qmode',  'tf',  'tote0',  'xf'
        dtype[ff.index('HOPC')] = ('HOPC',int,(4,))
        dtype[ff.index('qmode')] = ('qmode',float,(4,))
        dtype[ff.index('elec')] = ('elec',int)
        dtype[ff.index('xf')] = ('xf',float,(3,3))
        dtype[ff.index('pf')] = ('pf',float,(3,3))
        dtype[ff.index('J')] = ('J',float,(3,))
        dtype[ff.index('ireac')] = ('ireac',int)
        if compact is not True:
            dtype[ff.index('ee')] = ('ee','O')
        dtype[ff.index('HOP')] = ('HOP','O')
        ielec = ff.index('elec')

        if compact:
            order = [value for (i,value) in sorted(list(temp.items())) if (i!= 'ID') and (i!='bond') and (i != 'ee')]
        else:
            order = [value for (i,value) in sorted(list(temp.items())) if (i!= 'ID') and (i!='bond') ]
        order[ielec] = order[ielec][1]
        order = tuple(order)

        dat = np.array([order]*self.ntrj,dtype=dtype)

        if compact:
            for i,STrj in enumerate(self[1:]):
                order = [value for (key,value) in sorted(list(STrj.__dict__.items())) if (key!= 'ID') and (key!='bond') and (key != 'ee')]
                order[ielec] = order[ielec][1]
                dat[i+1]=tuple(order)
        else:
            for i,STrj in enumerate(self[1:]):
                order = [value for (key,value) in sorted(list(STrj.__dict__.items())) if (key!= 'ID') and (key!='bond')]
                order[ielec] = order[ielec][1]

                dat[i+1]=tuple(order)

        mdict = {'MultiTrj':dat,'mass':self.mass,'nmol':self.nmol,'ntrj':self.ntrj,'npack':npack,'compact':compact}

        if additiondic:
          #  mdict = {**mdict, **additiondic} #only valid for python > 3.5
            mdict.update(additiondic.copy())
        savemat(fname,mdict=mdict,\
                    do_compression=True)
        return dat

def loadMultiTrjMat(fname):
    dat = loadmat(fname,squeeze_me=True)
    dattrj = dat['MultiTrj']
    ntrj = dat['ntrj']
    nmol = dat['nmol']
    mass = list(dat['mass'])
    npack = dat['npack']
    compact = dat['compact']
    Trj = MultiTrj(nmol,mass,ntrj)
    for index,i in enumerate(dattrj):
        for jname,jvalue in zip(i.dtype.names,i):
            setattr(Trj[index],jname,jvalue)

    for j,i in enumerate(Trj):
        i.elec = [1,i.elec]
        k = abs(i.ireac)
        if k == 0:
            i.bond = [[1,2,3]]
        elif k == 4:
            i.bond =[[1],[2],[3]]
        elif k == 1:
            i.bond = [[1],[2,3]]
        elif k == 2:
            i.bond = [[2],[1,3]]
        elif k == 3:
            i.bond = [[3],[1,2]]
        i.HOPC = list(i.HOPC)
        i.J = list(i.J)
    return (Trj,ntrj,npack,nmol,mass)



def loadMultiTrj(folder,ntrj,fhead='N2O_',compact=True,npack=10,mass=[16.0,14.0,14.0],nmol=3):
    print('Read folder: %s'%folder)
    print('=============================')
    Trj = None
    for i in range(1,npack+1,1):
        local_folder = os.path.join(folder,'pack_'+str(i))
        a=MultiTrj(nmol,mass,int(ntrj/npack))
        if compact is False:
            print('Read fort221 %d'%(i,))
            a.read_221(local_folder)
        print('Read output %d'%(i,))
        a.readoutput(os.path.join(local_folder,fhead+str(i)+'.out'),4)
        print('Read fort2021 %d'%(i,))
        a.readfort2021(local_folder)
        print('Calculate VJ %d'%(i,))
        a.VJ()
        if i == 1:
            Trj = a
        else:
            Trj.append(copy.deepcopy(a))
  #  with open(os.path.join(folder,'data.db'), "wb") as myFile:
   #     pickle.dump(Trj, myFile)
   # print('Save  matfile for folder = %s'%(folder,))
   # Trj.savemat(os.path.join(folder,'data.mat'),npack)
    return Trj

def plot_hop(self,i):
    line1,=plt.plot(self.DAT[i-1].ee[:,0],self.DAT[i-1].ee[:,2],'b',linewidth=2.0,label=r'$^1A''$')
    line2,=plt.plot(self.DAT[i-1].ee[:,0],self.DAT[i-1].ee[:,3],'r',linewidth=2.0,label=r'$^3A"$')
    plt.plot(self.DAT[i-1].ee[:,0],self.DAT[i-1].ee[:,1],'k^')
    plt.xlabel('Time [fs]', fontsize=16)
    plt.ylabel(r'$V_{pot}$ [eV]', fontsize=16)
    axes = plt.gca()
    for tick in axes.xaxis.get_major_ticks():
        tick.label.set_fontsize(14)
    for tick in axes.yaxis.get_major_ticks():
        tick.label.set_fontsize(14)
    axes.set_ylim([-11,0])
    plt.legend(loc='upper right', fontsize=16)
    plt.show()

