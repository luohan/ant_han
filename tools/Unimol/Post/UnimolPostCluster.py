
# coding: utf-8

#!/usr/bin/python
import os#,re#, glob,shlex,time
#import numpy as np
#import scipy.constants as constants
#from subprocess import PIPE,check_output,Popen
#from scipy.io import savemat,loadmat

from ANTUnimol import MultiTrj,loadMultiTrj
#import matplotlib.pyplot as plt

# In[7]:

V1=[10,11,12,13,14,15,16,18,20]
allfolder = ['V1='+str(i) for i in V1]
mass = [16.0,14.0,14.0]
ntrj=100000
nmol=3
npack=10
for folder in allfolder:
    Trj=loadMultiTrj(folder=folder,ntrj=ntrj,fhead='N2O_',compact=True,npack=npack,mass=mass,nmol=nmol)
  #  with open(os.path.join(folder,'data.db'), "wb") as myFile:
   #     pickle.dump(Trj, myFile)
    print('Save  matfile for folder = %s'%(folder,))
    Trj.savemat(fname = os.path.join(folder,'data.mat'),npack = npack)
