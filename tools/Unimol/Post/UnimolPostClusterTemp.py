
# coding: utf-8

#!/usr/bin/python
import os#,re#, glob,shlex,time
#import numpy as np
#import scipy.constants as constants
#from subprocess import PIPE,check_output,Popen
#from scipy.io import savemat,loadmat

from ANTUnimolTemp import MultiTrj,loadMultiTrj
#import matplotlib.pyplot as plt

# In[7]:

Temp = [1000., 2000., 3000., 4000., 5000., 6000., 7000., 8000.]
Temp = [ 6000., 7000., 8000.]

allfolder = ['Temp='+'%.1f'%(i,) for i in Temp]
mass = [16.0,14.0,14.0]
ntrj=100000
nmol=3
npack=10
for folder in allfolder:
    (Trj,npack_correct)=loadMultiTrj(folder=folder,ntrj=ntrj,fhead='N2O_',compact=True,npack=npack,mass=mass,nmol=nmol)
  #  with open(os.path.join(folder,'data.db'), "wb") as myFile:
   #     pickle.dump(Trj, myFile)
    print('Save  matfile for folder = %s'%(folder,))
    Trj.savemat(fname = os.path.join(folder,'data.mat'),npack = npack_correct)
