      program collinear
        use param, only:autoev,autoang,mnat
        implicit none
        real(8) :: rmin, rmax, req, rdiff, X(3),Y(3),Z(3)
        real(8) :: dEdX(3),dEdY(3),dEdZ(3), vasym,vasym2
        real(8),allocatable :: r(:), V(:)
        character(2) :: symb(mnat)
        character(30) :: FNAME
        integer :: i,j,k,n,nat=3
        real(8),parameter :: ev2hart=0.03674932248D0
        real(8),parameter :: kcal2hart=1.593601438d-3

        rdiff = 0.01d0
        rmin = 1.0d0
        rmax = 5.0d0
        n = nint((rmax-rmin)/rdiff+1.0d0)
        symb(1) = 'o'
        symb(2) = 'n'
        symb(3) = 'n'
        allocate(r(n),V(n))
        r(1)=rmin
        do i = 2,n
          r(i)=r(i-1)+rdiff
        end do

        k = 1
        select case(k)
        case(1)
        ! 3app
          req = 2.0d0
          FNAME = '3app'
          vasym =  -228.41d0*kcal2hart/ev2hart
        case(2)
        ! 3ap
          req = 2.0d0
          vasym =  -228.41d0*kcal2hart/ev2hart
          FNAME = '3ap'
        case(3)
          req = 2.0d0
          vasym = -9.26610
          FNAME = '1ap'
        end select



        ! 3ap

        Y = 0
        Z = 0
        do i = 1,n
          X(2) = 0.0d0
          X(3) = req
          X(1) = req*0.5d0+r(i)
          X = X/autoang
          call pot(symb, X, Y, Z, V(i), dEdX, dEdY, dEdZ, nat, mnat)
          V(i) = V(i)*autoev
        end do
        ! X(1)=100
!
        ! call pot(symb,X,Y,Z,vasym2,dEdX,dEdY,dEdZ,nat,mnat)
        ! write(*,*) vasym2*autoev
        V = V-vasym

        OPEN(10,FILE=trim(FNAME)//'.dat')
        WRITE(10,*) "VARIABLES = ",'"r (A)",','"V (eV)"'
        WRITE(10,'("ZONE I=",I3," T=""",A,"""")') n,trim(FNAME)
        do i = 1,n
          write(10,*) r(i), V(i)
        end do
        close(10)
        deallocate(V,r)
        end program



