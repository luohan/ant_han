#!/bin/bash -e
#------------------------
os=rhel6
app=hdf5
shortver=1.8.16
version=1.8.16_intel-16.0.3
comp_version=intel/16.0.3
srcdir=$HOME/Soft/HDF5/src
prefix=$HOME/Soft/HDF5/$version
#------------------------
module purge
module load intel
module list

mkdir -p $srcdir
cd $srcdir
wget -N https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.8/hdf5-1.8.16/src/hdf5-1.8.16.tar.gz
echo "Extracting..."
#tar xvf $app-$shortver.tar.gz
cd $app-$shortver
mkdir -p $prefix
echo "./configure..."
./configure --prefix=$prefix --enable-fortran --enable-fortran2003 --enable-cxx --with-pic |& tee config.lst
echo "make..."
make 2>&1 |tee make.out
echo "make check..."
make check 2>&1 | tee  make_check.out 
echo "make install..."
make install > make_install.out 2>&1
cp config.log *.out $srcdir/build.sh $prefix
