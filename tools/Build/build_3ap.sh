#!/bin/bash -e
install_mod () {
  if [[ "$(hostname)" == *"ecn"* ]]; then
     modfile=$HOME/.local/modulefiles/ant/$1.lua
  else
     modfile=$HOME/privatemodules/ant/$1.lua
   fi 
   echo $modfile
  mkdir -p $(dirname $modfile)
cat << EOF > $modfile
whatis("Description: ANT for PES ${POT}")
load("intel")
load("myhdf5")
prepend_path ('PATH', "${CWD}/exe")
-- execute{cmd="ln -sf ${CWD}/exe/$2 ${CWD}/exe/ant", modeA={"load"}}
-- execute{cmd="rm -f  ${CWD}/exe/ant", modeA={"unload"}}
set_alias("ant","${CWD}/exe/$2")
EOF
}

CWD=/scratch/rice/l/luo160/ANT/ant_han
POT=n2o_3ap_Gamallo
DIAPOT=diapot2
NAME=n2o_3ap
ant_exe=ANT-${POT}.x.opt

cd $CWD/src
if [[ $1 == *'py'* ]]; then
  source deactivate
  module purge
fi
module load use.own
module load intel
module load myhdf5
make all POT=${POT} DIAPOT=$DIAPOT  -j 8
install_mod $NAME $ant_exe

  
