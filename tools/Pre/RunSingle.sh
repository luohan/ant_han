#!/bin/sh
## current directory
## run a single input
ANT_DIR=/scratch/rice/l/luo160/ANT/ant_han
WORK_DIR=$(pwd)
INPUT=$1
RUN_TIME=10
POT=N2O_GM

# compile ANT
cd ${ANT_DIR}/src
echo "Clean up previous compilation"
#gmake realclean
echo "Compile "${POT}" potential"
gmake POT=${POT} DIAPOT=diapot2
ant_exe=ANT-${POT}.x.opt
cp ${ANT_DIR}/exe/${ant_exe} ${WORK_DIR}/

# run the code
cd ${WORK_DIR}
chmod +x ${ant_exe}
ulimit -s unlimited
#rm -rf pack*
for (( i=1;i<=$RUN_TIME;i++))
do
  module load intel
  folder="${WORK_DIR}/pack_$i"
  input_file="${INPUT%.in}_$i.in"
  output_file="${INPUT%.in}_$i.out"
  error_file=${INPUT%.in}_$i.err
  mkdir -p ${folder}
  cp ${INPUT} ${folder}/${input_file}
  cp ${ant_exe} ${folder}/
  sed -i 's/ranseed=[0-9]\{1,\}/ranseed\='$((999999999-$i))'/g' ${folder}/${input_file}

  cd ${folder}
  echo $(pwd)
  nohup ./${ant_exe} < ${input_file} > ${output_file} 2> ${error_file} &
  echo $i $! $((999999999-$i)) >> ../log.txt
  echo $i' starts running'
  cd ${WORK_DIR}
done

