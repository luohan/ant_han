program main
  implicit none
  integer,parameter :: dp=8
  real(dp),parameter :: kcal2ev=0.0433634D0,ev2hart=0.0367502D0
  real(dp),parameter :: bohr2a=0.52917721067D0
  integer :: i=1,j=1,k=1
  real(dp),allocatable :: x(:,:),y(:,:),E(:,:),dEdN(:,:,:),fun(:,:)
  real(dp) :: dEdX(3),dEdY(3),dEdZ(3),temp
  real(dp) :: xmin = 0.01D00,xmax =8.0D0
  real(dp) :: xin(3),yin(3)=0.0D00,zin(3)=0.0D0
  character(2) :: symb(3) = (/'o','n','n'/)
  character(10) :: filename
  integer :: m
  m = FLOOR((xmax - xmin)/0.01D00)+1
  ALLOCATE(x(m,m),y(m,m),e(m,m),dEdN(m,m,3),fun(m,6))

  j = 1
  xin(3) = 1.1326d0/bohr2a
  xin(2) = 0.0D00
  xin(1) = -1.1999d0/bohr2a
  call pot(symb,xin,yin,zin,e(i,j),dEdX,dEdY,dEdZ,3,3)
  !-----------------colinear configureation-----------------------
  symb(1) = 'o'
  symb(2) = 'n'
  do i =1,m
    do j = 1,m
      x(i,j) = xmin + 0.01D00*DBLE(i-1)
      y(i,j) = xmin + 0.01D00*DBLE(j-1)
      xin(1) = -y(i,j)/bohr2a
      xin(2) = 0.0D00
      xin(3) = x(i,j)/bohr2a
      call pot(symb,xin,yin,zin,e(i,j),dEdX,dEdY,dEdZ,3,3)


      e(i,j) =(e(i,j)/ev2hart-(-3.468d0))/kcal2ev
      dEdN(i,j,:) = dEdX/ev2hart/kcal2ev/bohr2a
    enddo
  enddo

  OPEN(UNIT=k,FILE="pes.xyz",FORM="UNFORMATTED")
  WRITE(k) m,m
  WRITE(k) ((x(i,j),i=1,m),j=1,m), &
    ((y(i,j),i=1,m),j=1,m)
  CLOSE(k)

  OPEN(UNIT=k,FILE="pes.fun",FORM="UNFORMATTED")
  WRITE(k) m,m,4
  WRITE(k) ((e(i,j),i=1,m),j=1,m), (((dEdN(i,j,k),i=1,m),j=1,m),k=1,3)
  CLOSE(k)

  k = 10
  j = 10
  OPEN(UNIT=k,FILE="pes.dat")
  DO i=1,m
     write(k,*) x(i,j),e(i,j),dEdN(i,j,3)
  enddo
  close(k)
  !-----------------c2v configureation-----------------------
  symb(1) = 'n'
  symb(2) = 'o'
  do i =1,m
    do j = 1,m
      x(i,j) = xmin + 0.01D00*DBLE(i-1)
      y(i,j) = xmin + 0.01D00*DBLE(j-1)
      xin(1) = -y(i,j)/bohr2a
      xin(2) = 0.0D00
      xin(3) = x(i,j)/bohr2a
      call pot(symb,xin,yin,zin,e(i,j),dEdX,dEdY,dEdZ,3,3)


      e(i,j) =(e(i,j)/ev2hart-(-3.468d0))/kcal2ev
      dEdN(i,j,:) = dEdX/ev2hart/kcal2ev/bohr2a
    enddo
  enddo

  OPEN(UNIT=k,FILE="pesc2v.xyz",FORM="UNFORMATTED")
  WRITE(k) m,m
  WRITE(k) ((x(i,j),i=1,m),j=1,m), &
    ((y(i,j),i=1,m),j=1,m)
  CLOSE(k)

  OPEN(UNIT=k,FILE="pesc2v.fun",FORM="UNFORMATTED")
  WRITE(k) m,m,4
  WRITE(k) ((e(i,j),i=1,m),j=1,m), (((dEdN(i,j,k),i=1,m),j=1,m),k=1,3)
  CLOSE(k)

  k = 10
  j = 100
  OPEN(UNIT=k,FILE="pesc2v.dat")
  DO i=1,m
     write(k,*) x(i,j),e(i,j),dEdN(i,j,3)
  enddo
  close(k)
  
  DEALLOCATE(x,y,E,dEdN)
  end program



