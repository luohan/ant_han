#!/bin/sh
## run a batch of Et
## current directory
ANT_DIR=/scratch/rice/l/luo160/ANT/ant_han
ROOT_DIR=$(pwd)
INPUT=$1
RUN_TIME=10
POT=N2O_GM

# compile ANT
cd ${ANT_DIR}/src
echo "Clean up previous compilation"
#gmake realclean
echo "Compile "${POT}" potential"
gmake POT=${POT} DIAPOT=diapot2
ant_exe=ANT-${POT}.x.opt
cp ${ANT_DIR}/exe/${ant_exe} ${ROOT_DIR}/

for Et in {1,2,3,4,5,6,8,10}
do
  WORK_DIR=${ROOT_DIR}"/Et="${Et}
  mkdir -p ${WORK_DIR}
  cd ${WORK_DIR}
  # run the code
  cp ${ROOT_DIR}/${ant_exe} ${WORK_DIR}
  cp ${ROOT_DIR}/${INPUT} ${WORK_DIR}
  sed -i 's/ecoli=[0-9]*\.[0-9]*/ecoli='${Et}'\.0/g' ${INPUT}

  echo ${Et}" is created"
  chmod +x ${ant_exe}
  ulimit -s unlimited
  #rm -rf pack*
  for (( i=1;i<=$RUN_TIME;i++))
  do
    module load intel
    folder=${WORK_DIR}/pack_$i
    input_file=${INPUT%.in}_$i.in
    output_file=${INPUT%.in}_$i.out
    error_file=${INPUT%.in}_$i.err
    mkdir -p ${folder}
    cp ${INPUT}  ${folder}/${input_file}
    cp ${ant_exe} ${folder}/
    sed -i 's/ranseed=[0-9]\{1,\}/ranseed\='$((999999999-$i))'/g' ${folder}/${input_file}

    cd ${folder}
    echo $(pwd)
    nohup ./${ant_exe} < ${input_file} > ${output_file} 2> ${error_file} &
    echo $i $! $((999999999-$i)) >> ../log.txt
    echo $i' starts running'
    cd ${WORK_DIR}
  done
  cd ${ROOT_DIR}
done
