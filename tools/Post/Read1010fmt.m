function [Dhead,Dat] = Read1010fmt(filename,ntrj,natom )
Dhead = zeros(ntrj,1);
Dat = zeros(natom,3,ntrj);
fid = fopen(filename,'r');
for i = 1:ntrj
    temp = fgetl(fid);
    Dhead(i) = sscanf(temp,'%*d %f\n',1);
    for j=1:natom
        temp = fgetl(fid);
        temp = sscanf(temp(6:end),'%f');
        Dat(j,:,i) = temp(2:end)';
    end
end
fclose(fid);
Dhead = num2cell(Dhead);
Dat = mat2cell(Dat,3,3,ones(1,ntrj));
end

