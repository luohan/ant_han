#/bin/bash -le
WKDIR=$RCAC_SCRATCH'/ANT/N2O_vis'
cd $WKDIR
T=(3000 4000 $(seq 5000 2500 20000))
V=($(seq 0 1 60))
printf "   ,"  > runtable.csv
for i in ${T[@]}; do
   printf "T=%5i," $i >> runtable.csv
done
printf "\n" >> runtable.csv

for v in ${V[@]}; do
    echo $v
    printf "%3i," $v >> runtable.csv
    for i in ${T[@]}; do
       folder="T="$i"/V="$v
       if [[ -d $folder ]]; then
          fsize=$(du -sh $folder|awk '{print $1}')
       else
          fsize="      #"
       fi
       printf "%7s," $fsize  >> runtable.csv
    done
    printf "\n" >> runtable.csv
done
if [[ $1 == "1" ]]; then
  qstat -u luo160 | mailx -s "test" -a ./runtable.csv luo160@purdue.edu
else
  less ./runtable.csv
fi
