#!/bin/sh
## current directory
ANT_DIR=/scratch/rice/l/luo160/ANT/ant_han
ROOT_DIR=$(pwd)
INPUT=$1
RUN_TIME=10
POT=N2O_GM
module load intel
module load matlab
for DIR in ${ROOT_DIR}/*/
do
  WORK_DIR=${DIR%*/}
  echo ${WORK_DIR}
  for (( i=1;i<=$RUN_TIME;i++))
  do
    folder="${WORK_DIR}/pack_$i"
    input_file="${INPUT%.in}_$i.in"
    output_file="${INPUT%.in}_$i.out"
    error_file=${INPUT%.in}_$i.err
    # add impact parameter
    #awk -F ":" '/Impact parameter/ {printf "%10.5f\n", $2}' ${folder}/${output_file} > ${folder}/b.out
    #awk -F ":" '/Theta/ {printf "%10.5f\n", $2}'            ${folder}/${output_file} > ${folder}/Theta.out
    #awk -F ":" '/Phi/ {printf "%10.5f\n", $2}'              ${folder}/${output_file} > ${folder}/Phi.out
    #awk -F ":" '/Eta/ {printf "%10.5f\n", $2}'              ${folder}/${output_file} > ${folder}/Eta.out
    #awk -F ":" '/Phase/ {printf "%10.5f\n", $2}'            ${folder}/${output_file} > ${folder}/Phase.out
    #cp ${folder}/fort.30 ${folder}/fort.out
    #pr -mts' ' ${folder}/fort.out ${folder}/b.out      > ${folder}/fort.out.tmp
    #mv ${folder}/fort.out.tmp ${folder}/fort.out
    #pr -mts' ' ${folder}/fort.out ${folder}/Theta.out  > ${folder}/fort.out.tmp
    #mv ${folder}/fort.out.tmp ${folder}/fort.out
    #pr -mts' ' ${folder}/fort.out ${folder}/Phi.out    > ${folder}/fort.out.tmp
    #mv ${folder}/fort.out.tmp ${folder}/fort.out
    #pr -mts' ' ${folder}/fort.out ${folder}/Eta.out    > ${folder}/fort.out.tmp
    #mv ${folder}/fort.out.tmp ${folder}/fort.out
    #pr -mts' ' ${folder}/fort.out ${folder}/Phase.out  > ${folder}/fort.out.tmp
    #mv ${folder}/fort.out.tmp ${folder}/fort.out
    cat ${folder}/fort.30 >> ${WORK_DIR}/fort.30.tmp
  done
  echo "fort30 file is generated"
  awk '{
  printf "%10s ",NR
  for (i = 2; i <=NF; ++i) {
    if (i < 5)
      printf "%5s ",$i
    else
      printf "%10s ",$i
    } 
    printf "\n"
  }' ${WORK_DIR}/fort.30.tmp > ${WORK_DIR}/fort.30
  rm ${WORK_DIR}/fort.30.tmp
  mat_file=$(basename "${WORK_DIR}")
  cp fort30post.m Parameter.mat ${WORK_DIR}/
  cd ${WORK_DIR}
  # use matlab postprocess
  matlab -nodisplay -nodesktop -nosplash -r "fort30post('${mat_file}');quit"
  echo ${mat_file} " is created"
done

