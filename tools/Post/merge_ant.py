# -*- coding: utf-8 -*-
"""
Created on Fri May  4 23:37:18 2018

automatically combined all hdf5 files and generate a csv file showing finished run

@author: Han
"""

from ant_post import Merge_Unit_Parallel, Merge_Unit
from subprocess import check_output
import sys
import glob
import os

if len(sys.argv) < 2:
    cwd = '/scratch/rice/l/luo160/ANT/N2O_vis'
else:
    cwd = sys.argv[1]
    
os.chdir(cwd)
maxV = 60
maxV=60
# get all T folder
T = [3, 4, 5, 7.5, 10, 12.5, 15 ,17.5, 20]
#T = [5,7.5,10,12.5,15,17.5]
T = [int(i*1000) for i in T]
#Tfolder = glob.glob('T=*/')
#Tfolder = [i.replace('/','') for i in Tfolder if i != 'T=300/']
Tfolders = [os.path.join(cwd,'T=%d'%(i,)) for i in T]
table = [['#'  for i in range(maxV+1)] for j in range(len(Tfolders))]

fcheck = open(os.path.join(cwd,'check.csv'),'w')
ftable = open(os.path.join(cwd,'runtable2.csv'),'w')

# header
ftable.write('   ,')
for iT in T:
    ftable.write('T=%5d,'%(iT,))
ftable.write('\n')
    
    
for i, Tdir in enumerate(Tfolders):
    ToMerge = []
    for V in range(maxV+1):
        folder = os.path.join(cwd,Tdir,'V=%d'%(V,))
        size = '#'
        if os.path.isdir(folder):
            size = check_output('du -sh %s'%(folder,),shell=True).decode()
            size = size.split()[0]
            if os.path.isfile(os.path.join(folder,'30.h5')):
                fcheck.write('MergeExists, %s\n'%(folder,))
            else:
                pack_folders = glob.glob(os.path.join(folder,'pack_*'))
                if len(pack_folders) != 10:
                    fcheck.write('NoEnoughPack, %s\n'%(folder,))
                
                iadd = True
                for ipack, pack_dir in enumerate(pack_folders):
                    Logfile = os.path.join(pack_dir,'Log.txt')
                    if os.path.isfile(Logfile):
                        with open(Logfile,'r') as f_log:
                            lines = f_log.readlines()
                        if '100000/' not in lines[-1]:
                            iadd = False
                            fcheck.write('NotFinish, %s\n'%(pack_dir,))
                            break
                    else:
                        fcheck.write('NotFoundLog %s\n'%(pack_dir,))
                    
                    errfile = glob.glob(os.path.join(pack_dir,'*.err'))[0]
                    if os.path.isfile(errfile):
                        with open(errfile,'r') as f_log:
                            lines = f_log.readlines()
                        if len(lines) != 0:
                            fcheck.write('%s\n'%(lines[0],))
                            iadd = False
                            fcheck.write('ExistsError, %s\n'%(pack_dir,))
                            break
                    else:
                        fcheck.write('NotFoundError %s\n'%(pack_dir,))

                if iadd:
                    ToMerge.append(folder)
        else:
            fcheck.write('FolderNotFound, %s\n'%(folder,))
        table[i][V] = size
    print("T = %f"%(T[i],))
    for i in ToMerge:
        print("    "+i)
    Merge_Unit_Parallel(ToMerge, process_units=[20, 21, 30])
                
for V in range(maxV+1):
    ftable.write('%3d,'%(V,))
    for i in range(len(Tfolders)):
        ftable.write('%7s,'%(table[i][V],))
    ftable.write('\n')

ftable.close()
fcheck.close()               
