grep -r --include='*.in' "temp0=" | grep -v "temp0=17500"
echo "========================"
w=($(find . -name 'Log.txt' ))
for i in ${w[@]}; do  z=$(tail -n 1 $i);  if [[ $z != *"100000/"* ]]; then  echo $i $z;  fi;  done

T=($(seq 5000 2500 20000))
for i in ${T[@]}; do
   cd T\=$i
   grep -r --include='*.in' "temp0=" | grep -v "temp0=$i"
   cd ..
done


T=($(seq 5000 2500 20000))
V=($(seq 0 1 60))
printf " ,"  > runtable.csv
for i in ${T[@]}; do
   printf "T=%i," $i >> runtable.csv
done
printf "\n" >> runtable.csv

for v in ${V[@]}; do
    echo $v
    printf "%i," $v >> runtable.csv
    for i in ${T[@]}; do
       folder="T="$i"/V="$v
       if [[ -d $folder ]]; then
          fsize=$(du -sh $folder|awk '{print $1}')
       else
          fsize="#"
       fi
       printf "%s," $fsize  >> runtable.csv
    done
    printf "\n" >> runtable.csv
done



w=($(find . -name '*.err' ))
for i in ${w[@]}; do 
if [[ -s $i ]]; then
   echo $i
fi
done

pids=""
for ((i=1;i<=30;i++)); do
nohup sleep $((30-$i)) > /dev/null 2>&1 &
pids=$pids" "$!
done
ps huH p $pids|wc -l


if [ -f "$WORK_DIR/Log.txt" ]; then
   z=$(tail -n 1 $WORK_DIR/Log.txt)
   if [[ $z == *"100000/"* ]]; then
      echo "a"
   fi
fi