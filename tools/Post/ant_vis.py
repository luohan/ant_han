# -*- coding: utf-8 -*-
"""
Created on Sat May  5 15:04:34 2018

@author: Han
"""

from ant_post import Merge_Unit,Merge_Unit_Parallel,QCTXsec,QCTArray
import os
import numpy as np
import matplotlib.pyplot as plt
import glob
import re
import scipy.constants
from py2tecplot import py2dat
from scipy.io import savemat
import threading

def OutputVisXsec (Tfolder, Vmax, T):
    Omega11 = np.ones((Vmax+1))*-1
    Omega11var = Omega11.copy()
    Omega22 = Omega11.copy()
    Omega22var = Omega11.copy()
    
    Vdirs = glob.glob(os.path.join(Tfolder,'V=*'))
    Vdirs.sort(key=lambda x:int(os.path.split(x)[1].replace('V=','')))
    Vlevels = [int(os.path.split(x)[1].replace('V=','')) for x in Vdirs]

    for iV in range(len(Vdirs)):
        Vdir = Vdirs[iV]
        V = Vlevels[iV]
        print('T=%d, V=%d'%(T,V))
        fort30 = os.path.join(Vdir,'30.h5')
        Xsec = QCTXsec(file=fort30)
        
        ReXsecFile = os.path.join(Vdir, 'ReXsec')
        Xsec.Calculate_Reac(output_fname=ReXsecFile,output_size = [69, 278], assign=True)
        
        Xsec.Calculate_Vis(mode=2)

        Omega11[V] = Xsec.Omega11.val
        Omega11var[V] = Xsec.Omega11.sigma
        
        Omega22[V] = Xsec.Omega22.val
        Omega22var[V] = Xsec.Omega22.sigma        
    
    VisFile = os.path.join('CI', 'T=%d_CI'%(T,))
    savemat(VisFile,
        {'Omega11': Omega11,
         'Omega11var': Omega11var,
         'Omega22': Omega22,
         'Omega22var': Omega22var,
         'T': T, 'Vmax':Vmax,
         'Vlevels': np.asarray(Vlevels,np.int),
         'unit': Xsec.Omega11.unit}, do_compression=True)

class VisThread(threading.Thread):
    def __init__(self, Tfolder, Vmax, T):
        threading.Thread.__init__(self)
        self.Tfolder = Tfolder
        self.Vmax = Vmax
        self.T = T
    def run(self):
        OutputVisXsec(self.Tfolder, self.Vmax, self.T)


if ( __name__ == '__main__' ):
    Vmax = 60
    cwd = '/scratch/rice/l/luo160/ANT/N2O_vis'
    Tlist = [20, 17.5, 15, 12.5, 10, 7.5, 5, 4, 3]
    Tlist = [int(i*1000) for i in Tlist]
    
    # for T in Tlist:
        # Tfolder = os.path.join(cwd,'T=%d'%(T,))
        # OutputVisXsec (Tfolder, Vmax, T)
    mythreads = []
    for T in Tlist:
        Tfolder = os.path.join(cwd,'T=%d'%(T,))
        mythreads.append(VisThread(Tfolder, Vmax, T))
    for i in mythreads:
        i.start()
    for i in mythreads:
        i.join()
