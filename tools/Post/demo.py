# -*- coding: utf-8 -*-

from ant_post import Merge_Unit, Merge_Unit_Parallel, QCTXsec, QCTArray
import os
import glob
import re


def OutputReacXsec(folders):
    for i in folders:
        fort30 = os.path.join(i, '30.h5')
        xsecFile = os.path.join(i, 'ReXsec')

        Xsec = QCTXsec(fort30)
        Xsec.Calculate_Reac(output_fname=xsecFile)


if (__name__ == '__main__'):
    cwd = '/scratch/rice/l/luo160/ANT/N2O_collinear/Vr=7'
    os.chdir(cwd)
    folders = glob.glob('V=*')
    Vlist = [int(re.findall(r'\d+', i)[0]) for i in folders]
    ToMerge = [os.path.join(cwd, i) for i in folders]
    Merge_Unit_Parallel(ToMerge, [30], 1)
    OutputReacXsec(folders)
