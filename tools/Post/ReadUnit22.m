function [Out] = ReadUnit22(filename,MaxTrj,MaxTimeStep )
Out = repmat(struct('Out',zeros(11,MaxTimeStep)),[MaxTrj,1]);
fid = fopen(filename,'r');

ntrj0 = 0;
ntstep = 0;
while ~feof(fid)
    tline = fgetl(fid);
    tline = fgetl(fid);
    temp = sscanf(tline, '%d %d %f %f\n');
    
    ntrj = temp(1);
    if ntrj ~= ntrj0
        if ntrj0 ~= 0
            Out(ntrj0).Out = Out(ntrj0).Out(:,1:ntstep);
        end
        ntrj0 = ntrj;
        ntstep = 1;
    else
        ntstep = ntstep+1;
    end
    
    Out(ntrj).Out(11,ntstep) = temp(3);
    Out(ntrj).Out(10,ntstep) = temp(4);
    
    
    for j=1:3
        temp = fgetl(fid);
        temp = sscanf(temp(6:end),'%f');
        Out(ntrj).Out(j,ntstep) = temp(1);
        Out(ntrj).Out(j+3,ntstep) = temp(2);
        Out(ntrj).Out(j+6,ntstep) = temp(3);
    end
end
Out(ntrj0).Out = Out(ntrj0).Out(:,1:ntstep);
fclose(fid);

end

