#!/usr/bin/python
import os,re, glob,shlex,time
import numpy as np
import scipy.constants as constants
from subprocess import PIPE,check_output,Popen
from scipy.io import savemat,loadmat
import threading

# define some global data
au2kg = constants.physical_constants['atomic mass constant'][0] # in kg
eV2J  = constants.eV
m = np.array([16.0,14.0,14.0])
mu_a_bc = m[0]*(m[1]+m[2])/(np.sum(m))  # in a.u.
mu_a_bc = mu_a_bc*au2kg
Vmax = 59
Jmax = 269

class EtThread (threading.Thread):
    def __init__(self,ID,IC,IC_DIR,fort30type,outtype,inputname,checkfile):
        global mu_a_bc,eV2J
        threading.Thread.__init__(self)
        self.ID = ID
        self.IC = IC   # name of folder Et=* or Vr=*
        self.IC_DIR = IC_DIR
        self.checkfile = checkfile
        if 'Et' in IC:
            self.Et = float(re.findall(r'Et=(\d+\.?\d*)',IC)[0])
            self.Vr = np.sqrt(2.0*self.Et*eV2J/mu_a_bc)/1e3
        elif 'Vr' in IIC:
            self.Vr = float(re.findall(r'Vr=(\d+\.?\d*)',IC)[0])
            self.Et = 0.5*mu_a_bc*self.Vr*self.Vr*1e6/eV2J
        # information to extract
        self.fort30type = fort30type
        self.outtype = outtype
        self.inputname = inputname
    def run(self):
        print("Start analysis :",self.IC,"Location:",self.IC_DIR, "Thread:",self.ID)
        pack_info = Unpack(self.IC,self.IC_DIR,self.fort30type,\
                           self.outtype,self.inputname,\
                           self.checkfile)
        print("IC:",self.IC,"npack:",pack_info[0],\
                "trj:",pack_info[2],"/",pack_info[1]," are brocken")


def Unpack (IC,sub,fort30type,outtype,inputname,checkfile):
    global Vmax,Jmax
    pi = constants.pi
    # find bmax
    inputfile = os.path.join(sub,inputname+'.in')
    bmaxad = None
    bminad = 0.
    with open(inputfile,'r') as f:
        for line in f:
            if 'bmaxad' in line:
                bmax = re.findall('bmaxad\s*=\s*(\d+(?:\.\d*)?)',line)
                if bmax:
                    bmax = float(bmax[0])
                else:
                    raise Exception('"bmaxad=" is found in %s but no value is provided'%(inputfile,))
            if 'bminad' in line:
                bmin = re.findall('bminad\s*=\s*(\d+(?:\.\d*)?)',line)
                if bmin:
                    bmin = float(bmin[0])
                else:
                    raise Exception('"bminad=" is found in %s but no value is provided'%(inputfile,))
    if bmaxad:
        raise Exception('"bmaxad" is not found in %s'%(inputfile,))

    cmd_out = ['grep']
    for i in outtype['name']:
        cmd_out = cmd_out + ['-e']+[i]
    npack = len(glob.glob(sub+'/pack_*'))
    pack_info = np.zeros((2,npack),dtype='int')

    if not checkfile:
        if not os.path.isfile(os.path.join(sub,'data.mat')):
            checkfile = True

    # fort30 = b''  #String method
    if checkfile:
        for ipack in range(1,npack+1):
            pack_info[0][ipack-1] = ipack
            pack_dir = os.path.join(sub,'pack_'+str(ipack))
            outputfile = glob.glob(pack_dir+'/'+inputname+'*.out')[0]
            #fort30 = fort30+subprocess.check_output(['cat',pack_dir+'/fort.30'])  #String method

            # test if this is a good run
            with open(outputfile,'rb') as f:
                firstline = f.readline()
                f.seek(-2,2)
                while f.read(1) != b"\n":
                    f.seek(-2,1)
                lastline = f.readline()
                if b'Well done' not in lastline:
                    pack_info[1][ipack-1] = 0
                    print('Wrong pack for:'+IC,'pack:',ipack,"Info:",lastline.decode())
                    continue

            p1 = Popen(cmd_out+[outputfile],stdout=PIPE)
            p2 = Popen(['cut','-d:','-f2'],stdin=p1.stdout,stdout=PIPE)
            p1.stdout.close()
            data_add = p2.communicate()[0]
            p1.wait()
            data_add = np.fromstring(data_add,sep='\n')
            ntrj = int(int(data_add.size)/len(outtype['name']))
            pack_info[1][ipack-1] = ntrj
            data_add = data_add.reshape((ntrj,len(outtype['name'])))

            data0 = np.genfromtxt(pack_dir+'/fort.30',\
                    usecols=fort30type['usecols'])

            data0 = np.c_[data0,data_add]

            if ipack == 1:
                data = data0
            else:
                data = np.append(data,data0,axis = 0)

        savemat(sub+'/data.mat',{'data':data,\
                'varnames':fort30type['name']+outtype['name'],\
                'pack_info':pack_info,'bmax':bmax,'bmin':bmin},do_compression=True)
    else:
        print("Load data file %s"%(sub+'/data.mat',))
        temp = loadmat(sub+'/data.mat')
        pack_info=temp['pack_info']
        data = temp['data']

    # following is used to replace fort30post.m
    ntrj0 = np.sum(pack_info[1,:])
    Vindx = fort30type['name'].index('V')
    Jindx = fort30type['name'].index('J')
    Dissindx = fort30type['name'].index('DissFlag')
    Arrange = fort30type['name'].index('Arrange')
    CorrectIndex = np.where( (data[:,Dissindx] == 0 ) | (data[:,Dissindx] == 1))[0]

    ntrj = CorrectIndex.size

    cdata = data[CorrectIndex,:]

    CurrentError = np.array([(ntrj0 - ntrj,)],dtype= [('Count','int')])
    CurrentOut = np.zeros([4,Vmax+11,Jmax+11],dtype='float')
    for i in range(ntrj):
        V = int(round(cdata[i,Vindx]))
        J = int(round(cdata[i,Jindx]))

        if V < 0:
            V = 0
        if J < 0:
            J = 0
        Diss = int(cdata[i,Dissindx])
        Ar = int(cdata[i,Arrange])

        if Diss != 0:
            CurrentOut[3,0,0] += 1
        elif Ar == 2:
            CurrentOut[0,V,J] += 1
        elif Ar == 3:
            CurrentOut[1,V,J] += 1
        else:
            CurrentOut[2,V,J] += 1

    CurrentOut = CurrentOut / ntrj * (bmax * bmax - bmin*bmin) * pi
    savemat(os.path.join(sub,IC+'.mat'),\
            {'CurrentOut':CurrentOut,'CurrentError':CurrentError,\
            'ntrj':ntrj,'ntrj0':ntrj0,'pack_info':pack_info,'bmax':bmax,'bmin':bmin},\
            do_compression=True)

    return [npack,ntrj0,ntrj0-ntrj0]




if __name__ == '__main__':
    # set working directory, should be V=*J=*
    ROOT_DIR=os.getcwd()
    print('Root directory:')
    print(ROOT_DIR)
    IC_DIR = glob.glob(ROOT_DIR+'/Et=*')
    IC_DIR = glob.glob(ROOT_DIR+'/Vr=*')+IC_DIR
    # remove unwanted direcotories
    IC_DIR = [i for i in IC_DIR if 'double' not in i]
    IC_DIR = [i for i in IC_DIR if 'Vr=11' in i or 'Vr=13' in i]

    IC = [os.path.basename(i) for i in IC_DIR]
    ninitial = len(IC)
    print('\nInitial condition:')
    print(IC)

    INPUT = glob.glob(ROOT_DIR+'/*.in')[0]
    cmd = 'grep "vvad" '+os.path.basename(INPUT)

    inputname =os.path.basename(INPUT)[0:-3]
    output=check_output(shlex.split(cmd),universal_newlines=True)

    V_level = re.search(r'vvad=(\d+)',output).group(1)
    J_level = re.search(r'jjad=(\d+)',output).group(1)
    print('V =',V_level,', J =',J_level)



    fort30type = {'name': ['Surface','Arrange','DissFlag','Time','Steps',\
        'DensE1','DensE2','Etot','Epmin','Et',\
        'Erv','Ev','Er','V','J','Xscatter'],\
        'usecols':tuple(range(1,17))}
    outtype = {'name':['Impact parameter','Theta','Phi','Eta','Phase']}

    mythread = [0]*len(IC)
    for i,IIC in enumerate(IC):
        mythread[i] = EtThread(i,IIC,IC_DIR[i],fort30type,outtype,inputname,False)

    start_time = time.time()

    for i in mythread:
        i.start()

    for i in mythread:
        i.join()


    print("Finsh, runs for",time.time()-start_time,'seconds')


