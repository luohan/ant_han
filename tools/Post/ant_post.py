# -*- coding: utf-8 -*-
"""
Created on Tue Apr 24 11:39:22 2018

@author: Han
"""

import os
import h5py
import threading
import glob
import numpy as np
from scipy.io import savemat
import scipy.constants
import scipy.stats


class Merge_Unit_Thread(threading.Thread):
    def __init__(self, unit, foldername, ndatpack):
        threading.Thread.__init__(self)
        self.unit = unit
        self.foldername = foldername
        self.ndatpack = ndatpack

    def run(self):
        Merge_Unit(self.unit, self.foldername, self.ndatpack)


def Bin_b(data, b, nbins):
    """Bin data by impact parameter to get appropriate opacity function

    Args:
        data: data to bin
        b:    impact parameter for these data
        nbins: number of bins
    Return:
        bcen: center of bins
        bedge: edge of bins
        y: the summation of value of data in each bin
    """
    hists, bedge = np.histogram(b, bins=nbins, weights=data)
    bcen = 0.5 * (bedge[0:-1] + bedge[1:])
    return (bcen, bedge, hists)


def Merge_Unit_Parallel(foldernames, process_units=[10, 11, 20, 21, 30], ndatpack0=-1, serial=False):
    """Merge calculation results in Parallel

    Args:
        foldernames: (list) a list of foldername containing the "pack_XXX" files
                    (str)  foldername
        process_units: (list) a list of units to process (default: [10,11,20,21,30])
        ndatpack0: number of datapack to process (default: program find by itself)

    """
    mythreads = []
    if isinstance(foldernames, str):
        foldernames = [foldernames]
    for foldername in foldernames:
        if ndatpack0 == -1:
            ndatpack = len(glob.glob(os.path.join(foldername, 'pack_*')))
        else:
            ndatpack = ndatpack0
        print('Folder: %s Datapack: %d' % (foldername, ndatpack))
        mythreads += [Merge_Unit_Thread(i, foldername, ndatpack)
                      for i in process_units]

    if not serial:
        for i in mythreads:
            i.start()
        for i in mythreads:
            i.join()
    else:
        for foldername in foldernames:
            if ndatpack0 == -1:
                ndatpack = len(glob.glob(os.path.join(foldername, 'pack_*')))
            else:
                ndatpack = ndatpack0
            print('Folder: %s Datapack: %d' % (foldername, ndatpack))
            for i in process_units:
                Merge_Unit(i, foldername, ndatpack)


def Merge_Unit(process_unit, foldername, ndatpack=-1):
    """Merge calculation results of a bunch of hdf5 files

    Args:
        process_unit: unit to process
        foldername: folder containing the "pack_XX" files
        ndatpack: number of datapack to process (default: program find by itself)
    """

    fname = os.path.join(foldername, '%d.h5' % (process_unit,))
    if ndatpack == -1:
        ndatpack = len(glob.glob(os.path.join(foldername, 'pack_*')))

    with h5py.File(fname, mode='w') as h5fid:
        h5attrs = h5fid.attrs
        ntrj = 0
        for ipack in range(ndatpack):
            ifname = os.path.join(foldername, 'pack_%d' %
                                  (ipack + 1,), '%d.h5' % (process_unit,))
            with h5py.File(ifname, mode='r') as ih5fid:
                ih5attrs = ih5fid.attrs
                ntrj += ih5attrs['Number of trajectory']
                if ipack == 0:
                    # copy attributes
                    for i, iattr in enumerate(ih5attrs.items()):
                        attr = iattr[0]
                        value = iattr[1]
                        if attr in ['Randseed', 'Start Time', 'Number of trajectory', 'End Time']:
                            attr = attr + '_%d' % (ipack + 1,)
                        h5attrs.create(attr, value)
                    # copy dataset
                    dset_name = [i for i in ih5fid.keys()]
                    if ih5fid.get(dset_name[0]).dtype == object:
                        raise Exception('It couldn''t merge 22 and 23 file')
                    for idset_name in dset_name:
                        idset = ih5fid.get(idset_name)
                        dset = h5fid.create_dataset(idset_name, data=idset.value,
                                                    chunks=idset.chunks,
                                                    shape=idset.shape,
                                                    dtype=idset.dtype,
                                                    compression=idset.compression,
                                                    compression_opts=idset.compression_opts,
                                                    maxshape=idset.maxshape)
                        for iattr in idset.attrs.items():
                            dset.attrs.create(iattr[0], iattr[1])
                else:
                    # copy attributes
                    for i, iattr in enumerate(ih5attrs.items()):
                        attr = iattr[0]
                        value = iattr[1]
                        if attr in ['Randseed', 'Start Time', 'Number of trajectory', 'End Time']:
                            attr = attr + '_%d' % (ipack + 1,)
                            h5attrs.create(attr, value)
                    # copy dataset
                    dset_name = [i for i in ih5fid.keys()]
                    for idset_name in dset_name:
                        idset = ih5fid.get(idset_name)
                        dset = h5fid.get(idset_name)
                        dset_shape0 = list(dset.shape)
                        dset_shape = dset_shape0[:]
                        dset_shape[0] = dset.shape[0] + idset.shape[0]
                        dset_shape = tuple(dset_shape)
                        dset.resize(dset_shape)
                        dset[dset_shape0[0]:, :] = idset.value
            h5fid.flush()
            print('Folder: %s Unit: %d %3d/%3d\n' %
                  (foldername, process_unit, ipack + 1, ndatpack))

        h5attrs.create('Ntrj', ntrj)
        print('Folder: %s Unit: %d  finish\n' % (foldername, process_unit))


class QCTArray():
    """Class for QCT related array

    """

    def __init__(self, val, sigma=None, unit=None):
        self.val = np.asarray(val, np.float64)
        if sigma is None:
            self.sigma = np.zeros(val.shape)
        else:
            self.sigma = np.asarray(sigma, np.float64)
        self.unit = unit
        self.shape = val.shape
        self.flags = val.flags
        self.strides = val.strides
        self.ndim = val.ndim
        self.dtype = val.dtype

    def __copy__(self):
        return self.copy()

    def copy(self):
        """Return a copy of the class
        """
        return QCTArray(self.val.copy(), self.sigma.copy(), self.unit)

    def __getitem__(self, index):
        return self.val[index]

    def __setitem__(self, index, key):
        self.val.__setitem__(index, key)

    def setsigma(self, index, key):
        """Set variance of the data
        """
        self.sigma.__setitem__(index, key)

    def __repr__(self):
        return self.val.__repr__()

    def __mul__(self, y):
        if not isinstance(y, (float, int, np.ndarray)):
            raise TypeError
        if isinstance(y, np.ndarray):
            if y.size > 1:
                raise TypeError
        return QCTArray(self.val.copy() * y, self.sigma.copy() * y, self.unit)

    def __truediv__(self, y):
        if not isinstance(y, (float, int, np.ndarray)):
            raise TypeError
        if isinstance(y, np.ndarray):
            if y.size > 1:
                raise TypeError
        return QCTArray(self.val.copy() / y, self.sigma.copy() / y, self.unit)


class QCTXsec():
    def __init__(self, file=None, temp0=None):
        self.file = file
        self.Xsec = []
        if file:
            self.folder = os.path.dirname(file)
            with h5py.File(file, mode='r') as h5fid:
                self.bmax = h5fid.attrs['Bmax'][0]
                self.bmin = h5fid.attrs['Bmin'][0]
                if 'Ntrj' in list(h5fid.attrs.keys()):
                    self.ntrj0 = h5fid.attrs['Ntrj'][0]
                else:
                    self.ntrj0 = h5fid.attrs['Number of trajectory'][0]

                self.Symbol = h5fid.attrs['Symbol']
                self.mass = h5fid.attrs['Mass']
                self.Symbol = [i.decode("utf-8").upper() for i in self.Symbol]
                self.arr0 = int(h5fid.attrs['Arr0'])
                # old version don't have temp
                if temp0:
                    self.temp0 = temp0
                elif 'Temp0' in list(h5fid.attrs.keys()):
                    self.temp0 = h5fid.attrs['Temp0'][0]
                else:
                    self.temp0 = 300.0
                # some attributes old version of ant doesn't have
                default_attrs = [['B_mode', 'bmode', 3], ['E_mode', 'emode', 2],
                                 ['Et0', 'et0', 0.0], ['Eint', 'eint', 0.0],
                                 ['Evib', 'evib', 0.0], ['Eelec', 'elec', 0.0]]
                for i in default_attrs:
                    if i[0] in list(h5fid.attrs.keys()):
                        setattr(self, i[1], float(h5fid.attrs[i[0]]))
                    else:
                        setattr(self, i[1], i[2])
                if self.arr0 == 1:
                    # print('Collision: {2:s} - {0:s}{1:s}'.format(*self.Symbol))
                    self.mu_a_bc = (self.mass[0] + self.mass[1]) * \
                        self.mass[2] / np.sum(self.mass)
                elif self.arr0 == 2:
                    # print('Collision: {0:s} - {1:s}{2:s}'.format(*self.Symbol))
                    self.mu_a_bc = (self.mass[1] + self.mass[2]) * \
                        self.mass[0] / np.sum(self.mass)
                elif self.arr0 == 3:
                    # print('Collision: {1:s} - {0:s}{2:s}'.format(*self.Symbol))
                    self.mu_a_bc = (self.mass[2] + self.mass[0]) * \
                        self.mass[1] / np.sum(self.mass)

                self.Final = h5fid.get('final').value
                self.FinalVar = [i.decode('utf-8')
                                 for i in h5fid.get('final').attrs['Varname']]

                self.Initial = h5fid.get('initial').value
                self.InitialVar = [i.decode('utf-8')
                                   for i in h5fid.get('initial').attrs['Varname']]
                # self.invh is the inverse of opacity function
                # self.invh = 1/h(x)
                if self.bmode == 3 or self.bmode == 5:
                    self.invh = np.ones(self.ntrj0)
                elif self.bmode == 4:
                    Bindx = self.InitialVar.index('b')
                    self.invh = 1.0 / \
                        (3.0 *
                         (1.0 - self.Initial[:, Bindx] / float(self.bmax)))

    def copy(self):
        new = QCTXsec(temp0=self.temp0)
        new.__dict__ = self.__dict__.copy()
        return new

    def Calculate_Reac(self, output_fname=None, ntrj_use=None, assign=False, temp0=None,
                       output_size=[69, 278]):
        """Calculate reaction cross sections and state to state cross sections

        Args:
            file(str): 30.h5 file
            output_fname(str): name of file to write, stored in same directory
                               as file
            ntrj_use(int,optional): number of trajectories to use to calculate
            assign(logical,optional): True to assign the result to self.ReXsec
            temp0 (float,optional): temperature to calculate rate
            output_size : size of output array
        """
        fdata = self.Final.copy().transpose()
        Vindx = self.FinalVar.index('V')
        Jindx = self.FinalVar.index('J')
        Arrindx = self.FinalVar.index('arr')

        # calculate state-to-state cross section
        invh = self.invh.copy()

        fdata = fdata[(Vindx, Jindx, Arrindx), :]
        fdata = np.rint(fdata).astype(int)   # round data
        ntrj0 = fdata.shape[1]

        # select out trajectories used to calculate reaction cross sections
        if ntrj_use is not None:
            ntrj_use = np.asarray(ntrj_use, dtype=np.int)
            ntrj_use = ntrj_use.flatten()
            if ntrj_use.size == 1:
                if ntrj_use > ntrj0:
                    raise Exception(
                        'ntrj_use=%d is larger than available trjs=%d' % (ntrj_use, ntrj0))
                else:
                    ntrj0 = ntrj_use
                    fdata = fdata[:, 0:ntrj0]
                    invh = invh[0:ntrj0]
            else:
                ntrj0 = ntrj_use.size
                fdata = fdata[:, ntrj_use]
                invh = invh[ntrj_use]

        # filter out wrong trajectories
        index = fdata[2] > 0
        CurrentError = np.where(fdata[2] < 0)[0]
        if CurrentError.size == 0:
            CurrentError = np.array([-1])
        fdata = fdata[:, index]                   # select arr > 0
        invh = invh[index]

        # number of effective trjs, big N
        ntrj = fdata.shape[1]
        fdata[fdata < 0] = 0                      # round V,J <0 to 0

        sindex = fdata[2].argsort()
        fdata = fdata[:, sindex]  # sort by arr
        invh = invh[sindex]

        isplit = np.searchsorted(fdata[2], [2, 3, 4])

        fdata_split = np.hsplit(fdata, isplit)    # split data by arr

        invh_split = np.hsplit(invh, isplit)

        Vmax = output_size[0]
        Jmax = output_size[1]

        Jbin = np.arange(-0.5, Jmax + 0.5, 1)
        Vbin = np.arange(-0.5, Vmax + 0.5, 1)
        Part = []

        # count number of different results
        for i, j in zip(fdata_split[0:3], invh_split[0:3]):
            # function g
            if i.size > 0:
                g = np.ones(i[0].shape)
                gdh = g * j
                H, _, _, _ = scipy.stats.binned_statistic_2d(
                    i[0], i[1], (gdh, gdh**2), statistic='sum', bins=[Vbin, Jbin])
            else:
                H = np.zeros([Vbin.size-1, Jbin.size-1])
            Part.append(H)

        ReXsec = np.zeros([4, Vmax, Jmax],
                          dtype=float)   # V go from 0 to Vmax+10
        ReXsecVar = np.zeros([4, Vmax, Jmax],
                             dtype=float)   # V go from 0 to Vmax+10

        # non reaction trajectories
        ReXsec[0] = Part[self.arr0 - 1][0] / ntrj    # I
        ReXsecVar[0] = np.sqrt(
            (Part[self.arr0 - 1][1] / ntrj - ReXsec[0]**2) / (ntrj - 1))  # sigma = sqrt(VAR)
        # AB+C, exchange
        ReXsec[1] = Part[0][0] / ntrj
        ReXsecVar[1] = np.sqrt((Part[0][1] / ntrj - ReXsec[1]**2) / (ntrj - 1))
        # AC+B, exchange
        ReXsec[2] = Part[2][0] / ntrj
        ReXsecVar[2] = np.sqrt((Part[2][1] / ntrj - ReXsec[2]**2) / (ntrj - 1))
        # dissociation
        g = np.ones(invh_split[3].shape)
        g2h2 = np.sum(invh_split[3]**2)
        ReXsec[3, 0, 0] = np.sum(g * invh_split[3]) / ntrj  # dissociation
        ReXsecVar[3, 0, 0] = np.sqrt(
            (g2h2 / ntrj - ReXsec[3, 0, 0]**2) / (ntrj - 1))

        # summation of exchange
        ExXsec = np.sum(ReXsec[1:3])
        ExXsecVar = np.sqrt((np.sum((Part[0][1] + Part[2][1])) / ntrj -
                             (np.sum(Part[0][0] + Part[2][0]) / ntrj)**2) / (ntrj - 1))

        # summation of dissociation
        DisXsec = ReXsec[3, 0, 0]
        DisXsecVar = ReXsecVar[3, 0, 0]

        V = np.pi * (self.bmax**2 - self.bmin**2)
        ReXsec *= V
        ReXsecVar *= V
        ExXsec *= V
        ExXsecVar *= V
        DisXsec *= V
        DisXsecVar *= V
        ReXsec = QCTArray(ReXsec, ReXsecVar, unit='A^2')
        ExXsec = QCTArray(ExXsec, ExXsecVar, unit='A^2')
        DisXsec = QCTArray(DisXsec, DisXsecVar, unit='A^2')
        # cross sections in A^2
        if not temp0:
            temp0 = self.temp0

        c_xsec2rate = np.sqrt(8.0 * temp0 / np.pi * scipy.constants.k / self.mu_a_bc /
                              scipy.constants.value('atomic mass constant')) * scipy.constants.N_A * 1.0e-14
        # convert Xsec -> rate
        #  A^2  -> cm^3/mol/s

        if output_fname:
            savemat(os.path.join(os.path.dirname(self.file), output_fname),
                    {'ReXsec': ReXsec.val,
                     'CurrentError': CurrentError,
                     'ntrj': ntrj, 'ntrj0': ntrj0,
                     'bmax': self.bmax, 'bmin': self.bmin,
                     'ReXsecU': ReXsec.sigma,
                     'T': temp0},
                    do_compression=True)

        Rentrj = ntrj

        if assign:
            self.ReXsec = ReXsec
            self.ReNtrj = Rentrj
            self.ExXsec = ExXsec
            self.DisXsec = DisXsec
            self.Xsec2Rate = c_xsec2rate

        return (ReXsec, ExXsec, DisXsec, c_xsec2rate)

    def Calculate_Vis(self, temp0=None, mode=1):
        """Calculate collision integral based on Wang-Change and Ulenbeck''s theory

        Args:
            mode = 1: ntrj = number of nonreactive trajectories
                 = 2: ntrj = number of all trajectories
        """
        # autoev = 27.21138602

        if not temp0:
            kbt0 = self.temp0 * \
                scipy.constants.value('kelvin-electron volt relationship')
        else:
            kbt0 = temp0 * \
                scipy.constants.value('kelvin-electron volt relationship')

        Arrindx = self.Final[:, self.FinalVar.index('arr')]
        inreac, = np.nonzero(Arrindx == self.arr0)  # get non reaction data
        nnreac = inreac.size
        if mode == 2:
            w, = np.nonzero(Arrindx > 0)
            nnreac = w.size

        # extract variables to use
        Et0 = self.Initial[inreac, self.InitialVar.index('Et0')].copy()
        # b = self.Initial[inreac, self.InitialVar.index('b')].copy()
        Etf = self.Final[inreac, self.FinalVar.index('Etf')].copy()
        Chi = self.Final[inreac, self.FinalVar.index('Chi')].copy()
        invh = self.invh[inreac].copy()

        # calculate some values
        gam2 = Et0 / kbt0
        gamp2 = Etf / kbt0
        gam = np.sqrt(gam2)
        gamp = np.sqrt(gamp2)
        sc = np.sin(Chi)
        cc = np.cos(Chi)

        g = gam2 - gam * gamp * cc
        g2 = gam2 * gam2 * sc * sc + \
            np.power((gam2 - gamp2), 2) * (1 / 3 - 0.5 * sc * sc)

        Omega11 = np.sum(g * invh) / nnreac
        Omega11var = np.sqrt(
            (np.sum(np.power((g * invh), 2)) / nnreac - Omega11**2) / (nnreac - 1.0))
        Omega22 = np.sum(g2 * invh) / nnreac
        Omega22var = np.sqrt(
            (np.sum(np.power((g2 * invh), 2)) / nnreac - Omega22**2) / (nnreac - 1.0))

        # c1 = np.sqrt(scipy.constants.k * 1e7 * self.temp0 / 2/np.pi/
        #             (self.mu_a_bc * scipy.constants.value('atomic mass constant') * 1000.))
        # c1 should have unit of cm/s

        # what we out put here is sigma*Omega11* and sigma*Omega22*
        Omega11 = QCTArray(Omega11, sigma=Omega11var, unit='A^2')
        Omega11 = Omega11 * (self.bmax**2 / 2)

        Omega22 = QCTArray(Omega22, Omega22var, unit='A^2')
        Omega22 = Omega22 * (self.bmax**2 / 4)

        self.Omega22 = Omega22
        self.Omega11 = Omega11

        return (Omega11, Omega22)
