%% ANT post-processing
clear
clc
ntrj = 200;
natom = 3;
name_atom = {'o','n','n'};
DAT = repmat(struct('Et0',0,'Ei0',0,'b',0,'x0',zeros(natom,3),'p0',zeros(natom,3),...
    'Etf',0,'Eif',0,'xf',zeros(natom,3),'pf',zeros(natom,3),...
    'Trj',[]),[ntrj 1]);
%% load initial initial position
[Dhead,Dat] = Read1010fmt('fort.10',ntrj,natom );
[DAT.Ei0] = Dhead{:};
[DAT.x0]  = Dat{:};
%% load initial initial momentum
[Dhead,Dat] = Read1010fmt('fort.11',ntrj,natom );
[DAT.Et0] = Dhead{:};
[DAT.p0]  = Dat{:};
%% load initial impact parameter
[status,cmdout] = system('awk -F ":" ''/Impact parameter/ {print $2}'' N2O.out');
b = sscanf(cmdout,' %f');
for i = 1:ntrj
    DAT(i).b = b(i);
end
    
%% load final position
[Dhead,Dat] = Read1010fmt('fort.20',ntrj,natom );
[DAT.Eif] = Dhead{:};
[DAT.xf]  = Dat{:};
%% load initial initial position
[Dhead,Dat] = Read1010fmt('fort.21',ntrj,natom );
[DAT.Etf] = Dhead{:};
[DAT.pf]  = Dat{:};
%% load intermidiate trj
[Out] = ReadUnit22('fort.22',ntrj,10000 );
for i = 1:ntrj
    DAT(i).Trj = Out(i).Out;
end
%% load fort 30 file
fid = fopen('fort.30','r');
DAT = fscanf(fid,'%f');
DAT = reshape(DAT,17,[]);
fclose(fid);
DAT = DAT(1:end,:);
% remove wrong trj
DAT(end-2,:) = round(DAT(end-2,:));
DAT(end-1,:) = round(DAT(end-1,:));
DAT = DAT(:,DAT(end-2,:) >= 0);
DAT = DAT(:,DAT(end-1,:) >= 0);
ntrj = length(DAT);

reac_trj = find(DAT(3,:) == 1 | DAT(3,:) ==3);
ex_xsec = pi*5*5*length(reac_trj)/ntrj;




for i=1:ntrj
    plot3(DAT(i).Trj(1:3,:)',DAT(i).Trj(4:6,:)',DAT(i).Trj(7:9,:)')
axis equal
pause
end
















trj = 1;
dis = zeros(1,3);
for i = 1:3
    j = i+1;
    if j>3
        j = 1;
    end
    dis(i) = sqrt(sum((DAT(trj).xf(i,:)-DAT(trj).xf(j,:)).^2));
end

x0 = DAT(trj).x0(:,1);
y0 = DAT(trj).x0(:,2);
z0 = DAT(trj).x0(:,3);
plot3(x0(1),y0(1),z0(1),'bo')
hold on
plot3(x0(2),y0(2),z0(2),'ro')
plot3(x0(3),y0(3),z0(3),'ro')
axis equal


x0 = DAT(trj).xf(:,1);
y0 = DAT(trj).xf(:,2);
z0 = DAT(trj).xf(:,3);
plot3(x0(1),y0(1),z0(1),'bx')
hold on
plot3(x0(2),y0(2),z0(2),'rx')
plot3(x0(3),y0(3),z0(3),'rx')
axis equal