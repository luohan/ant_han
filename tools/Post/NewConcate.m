clear
clc
State = [5 0;...
         5 20;...
         20 0;...
         20 20];
Ert = [1.5568;1.6547;5.1748 ; 5.2564];
Ediss = 9.90487;
Bmax = 5;
[m,n]=size(State);
load ../QCT/Mol_Input/N2+O/double/Parameter.mat par prop

EtXsec(m,1) = struct('Et',[],'ExXsec',[],'V',[],'J',[]);

for i =1:m
    CurrentV = State(i,1);
    CurrentJ = State(i,2);
    cdir = ['V=',num2str(CurrentV),'J=',num2str(CurrentJ)];
    subdir = dir(cdir);
    isub = [subdir(:).isdir]; %# returns logical vector
    nameFolds = {subdir(isub).name}';
    nameFolds(ismember(nameFolds,{'.','..','Vr=5_double'})) = [];    
    
    Et = zeros(1,length(nameFolds));
    Xsec = Et;
    for j = 1:length(Et)
        if ~isempty(strfind(nameFolds{j},'Et'))
            Et(j) = sscanf(nameFolds{j},'Et=%f');
        else
            Et(j) = sscanf(nameFolds{j},'Vr=%f');
            Et(j) = 0.5*prop.mu_a_bc*(Et(j)/100)^2*par.conv3;
        end
        temp=load([cdir,'/',nameFolds{j},'/',nameFolds{j},'.mat']);
        Xsec(j) = sum(sum(sum(temp.CurrentOut(2:3,:,:))));
    end
    [Et, index ] = sort(Et);
    Xsec = Xsec(index);
    EtXsec(i).Et = Et;
    EtXsec(i).ExXsec = Xsec;
    EtXsec(i).V = CurrentV;
    EtXsec(i).J = CurrentJ;
end

tdata = [];
tdata.varnames = {'E<sub>tra</sub> (eV)', 'Cross-Sections (A<sup>2</sup>)'};
for i = 1:m
    zonename = ['ANT V=',num2str(State(i,1)),' J=',num2str(State(i,2))];
    tdata.lines(i) = struct('x',EtXsec(i).Et,'y',EtXsec(i).ExXsec,'zonename',zonename);
end
mat2tecplot(tdata,'ANTEtXsec.plt');
