# -*- coding: utf-8 -*-
"""
Created on Sat May  5 15:56:56 2018

@author: Han
"""
import numpy as np
def py2dat(tdata,fname):
    if not isinstance(tdata,dict):
        raise TypeError
    else:
        with open(fname,'w') as f:
            # variables
            f.write('VARIABLES = ')
            for var in tdata['varnames'][:-1]:
                f.write('"%s", '%(var,))
            f.write('"%s"\n'%(tdata['varnames'][-1],))
            
            for iline,line in enumerate(tdata['lines']):
                x = np.asarray(line['x'])
                lenx = x.size
                data = x.reshape(1,lenx)
                
                y = np.asarray(line['y']).reshape(1,lenx)
                data = np.vstack((data,y))
                
                if 'z' in line.keys():
                    if line['z']:
                        z = np.asarray(line['z']).reshape(1,lenx)
                        data = np.vstack((data,z))
                if 'v' in line.keys():
                    if line['v']:
                        v = np.asarray(line['v'])
                        if v.ndim == 1:
                            v = v.reshape(1,lenx)
                        data = np.vstack((data,v))
                
                if 'zonename' in line.keys():
                    if len(line['zonename']) == 0:
                        zonename = 'ZONE %d'%(iline,)
                    else:
                        zonename = line['zonename']
                else:
                    zonename = 'ZONE %d'%(iline,)
                    
                ivarloc = 0        
                f.write('ZONE I = %d T="%s" DATAPACKING=POINT\n'%(lenx,zonename))
                f.write(' '+np.array2string(data.T).replace('[','').replace(']','') )
                f.write('\n\n')
            
                
                            
                