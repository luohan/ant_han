%% ANT post-processing
function fort30post(name)
load Parameter.mat
Vindx = 15;
Jindx = 16;
Typeindx = 3;
Dissindx = 4;
Bmax = 5;
% load data
fid = fopen('fort.30','r');
DAT = fscanf(fid,'%f');
DAT = reshape(DAT,17,[]);
fclose(fid);

ntrj0 = length(DAT);

wrong = find(DAT(Vindx,:) == -1 & DAT(Jindx,:) == -1);
correct = find(DAT(Vindx,:) ~= -1 & DAT(Jindx,:) ~= -1);

DAT = DAT(:,correct);
DAT(end-2,:) = round(DAT(end-2,:));  %V level
DAT(end-1,:) = round(DAT(end-1,:));

ntrj = length(correct);

DAT(end-2, DAT(end-2,:)<0) = 0;
DAT(end-1, DAT(end-1,:)<0) = 0;


% DAT = DAT(:,DAT(Vindx,:) >= 0);
% DAT = DAT(:,DAT(Jindx,:) >= 0);

% ntrj = length(DAT);

CurrentError = struct('Count',ntrj0-ntrj);
CurrentOut = zeros(4,prop.Vmax(1)+10,prop.Jmax(1,1)+10);

for i = 1:ntrj
    V = DAT(Vindx,i)+1;
    J = DAT(Jindx,i)+1;
    
    if DAT(Dissindx,i) ~= 0
        CurrentOut(4,1,1) =  CurrentOut(4,1,1) +1;
    elseif DAT(Typeindx,i) == 2
        CurrentOut(1,V,J) =  CurrentOut(1,V,J) +1;
    elseif DAT(Typeindx,i) == 3
        CurrentOut(2,V,J) =  CurrentOut(2,V,J) +1;
    else
        CurrentOut(3,V,J) =  CurrentOut(3,V,J) +1;
    end
end
CurrentOut = CurrentOut/ntrj*(pi*Bmax*Bmax);
save([name,'.mat'],'CurrentOut','CurrentError','ntrj','ntrj0')
