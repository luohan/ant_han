# -*- coding: utf-8 -*-
"""
Created on Tue Apr 24 11:39:22 2018

@author: Han
"""

import os
import h5py
import numpy as np
import threading
from scipy.io import savemat,loadmat


def Merge_Unit(process_unit,foldername,ndatpack):
    fname = os.path.join(foldername,'%d.h5'%(process_unit,))
    with h5py.File(fname,mode='w') as h5fid:
        h5attrs = h5fid.attrs
        ntrj = 0
        for ipack in range(ndatpack):
            ifname = os.path.join(cwd,foldername,'pack_%d'%(ipack+1,),'%d.h5'%(process_unit,))
            with h5py.File(ifname,mode='r') as ih5fid:
                ih5attrs = ih5fid.attrs
                ntrj += ih5attrs['Number of trajectory']
                if ipack == 0:
                    # copy attributes
                    for i,iattr in enumerate(ih5attrs.items()):
                        attr = iattr[0]
                        value = iattr[1]
                        if attr not in ['Datname','Natom','Mass','Initial PES','Total PES','Symbol','Arr0',
                                        'Bmax','Bmin','Nprint']:
                            attr = attr+'_%d'%(ipack+1,)
                        h5attrs.create(attr,value)
                    # copy dataset
                    dset_name = [i for i in ih5fid.keys()]
                    if ih5fid.get(dset_name[0]).dtype == object:
                        raise Exception('It couldn''t merge 22 and 23 file')    
                    for idset_name in dset_name:
                        idset = ih5fid.get(idset_name)
                        dset = h5fid.create_dataset(idset_name, data=idset.value,
                                chunks=idset.chunks,shape=idset.shape,dtype=idset.dtype,
                                compression=idset.compression,compression_opts=idset.compression_opts,
                                maxshape=idset.maxshape)
                        for iattr in idset.attrs.items():
                            dset.attrs.create(iattr[0],iattr[1])
                else:
                    # copy attributes
                    for i,iattr in enumerate(ih5attrs.items()):
                        attr = iattr[0]
                        value = iattr[1]
                        if attr not in ['Datname','Natom','Mass','Initial PES','Total PES','Symbol','Arr0',
                                        'Bmax','Bmin','Nprint']:
                            attr = attr+'_%d'%(ipack+1,)
                            h5attrs.create(attr,value)
                    # copy dataset
                    dset_name = [i for i in ih5fid.keys()]
                    for idset_name in dset_name:
                        idset = ih5fid.get(idset_name)
                        dset = h5fid.get(idset_name)
                        dset_shape0 = list(dset.shape)
                        dset_shape = dset_shape0[:]
                        dset_shape[0] = dset.shape[0]+idset.shape[0]
                        dset_shape = tuple(dset_shape)
                        dset.resize(dset_shape)
                        dset[dset_shape0[0]:,:] = idset.value 
            h5fid.flush()
            print('Unit: %d  %3d/%3d\n'%(process_unit,ipack+1,ndatpack))
            
        h5attrs.create('Ntrj',ntrj)     
        print('Unit: %d  finish\n'%(process_unit,))
    
class MyThread(threading.Thread):
    def __init__(self,unit,foldername,ndatpack):
        threading.Thread.__init__(self)
        self.unit = unit
        self.foldername = foldername
        self.ndatpack = ndatpack

    def run(self):
        Merge_Unit(self.unit,self.foldername,self.ndatpack)

def Calculate_Unitform_Xsec(file,Vmax,Jmax,IC):
    h5fid = h5py.File(file,mode='r')
    bmax = h5fid.attrs['Bmax']
    bmin = h5fid.attrs['Bmin']
    ntrj0 = h5fid.attrs['Ntrj']
    PostDset = h5fid.get('final')
    Symbol = h5fid.attrs['Symbol']
    Symbol = [i.decode("utf-8").upper() for i in Symbol]
    Arr0 = int(h5fid.attrs['Arr0'])
    if Arr0 == 1:
        print('Collision: {2:s} - {0:s}{1:s}'.format(*Symbol))
    elif Arr0 == 2:
        print('Collision: {0:s} - {1:s}{2:s}'.format(*Symbol))
    elif Arr0 == 3:
        print('Collision: {1:s} - {0:s}{2:s}'.format(*Symbol))    
        
    Varnames = list(PostDset.attrs['Varname'])
    Vindx = Varnames.index(b'V')
    Jindx = Varnames.index(b'J')
    Arrindx = Varnames.index(b'arr')
    
    cdata = PostDset.value.copy().transpose()
    cdata = cdata[(Vindx,Jindx,Arrindx),:]
    cdata = np.rint(cdata).astype(int)   # roun data
    cdata = cdata[:,cdata[2,:]>0]        # select arr > 0
    ntrj = cdata.shape[1]                # effective trjs
    cdata[cdata<0] = 0                   # round V,J <0 to 0
    cdata = cdata[:,cdata[2,:].argsort()]  # sort by arr

    
    CurrentError = np.array([(ntrj0 - ntrj,)],dtype= [('Count','int')])
    
    isplit = np.searchsorted(cdata[2],[2,3,4])
    cdata_split = np.hsplit(cdata, isplit)    # split data by arr
    
    Jbin = np.arange(-0.5,Jmax+10+1.5,1)
    Vbin = np.arange(-0.5,Vmax+10+1.5,1)
    
    Count = []
    for i in cdata_split[0:3]:
        H,_,_ =  np.histogram2d(i[0],i[1], bins=(Vbin, Jbin))
        Count.append(H)
        
    CurrentOut = np.zeros([4,Vmax+10+1,Jmax+10+1],dtype=float)   # V go from 0 to Vmax+10
    CurrentOut[0,:,:]=Count[Arr0-1]  #non reaction
    CurrentOut[1,:,:]=Count[0]       #AB+C
    CurrentOut[2,:,:]=Count[2]       #AB+C
    CurrentOut[3,0,0]=cdata_split[3].shape[1]
    CurrentOut = CurrentOut / ntrj * (bmax * bmax - bmin*bmin) * np.pi
    
    savemat(os.path.join(os.path.dirname(file),IC+'.mat'),\
            {'CurrentOut':CurrentOut,'CurrentError':CurrentError,\
            'ntrj':ntrj,'ntrj0':ntrj0,'bmax':bmax,'bmin':bmin},\
            do_compression=True)        
   


if ( __name__ == '__main__' ):
    cwd = 'E:\\Research\\QCT_TSH\\HDF5Implement'
    try:
        os.chdir(cwd)
    except: 
        cwd = os.getcwd()
    UNIT_HDF5 = [10,11,20,21,30]

    Vr = 11
    ndatpack = 10
    foldername = os.path.join(cwd,'Vr=%d'%(Vr,))
    
    # combine packs
    mythread = [MyThread(i,foldername,ndatpack) for i in UNIT_HDF5]
    for i in range(len(mythread)):
        mythread[i].start()
    for i in range(len(mythread)):
        mythread[i].join()
    print('HDF5 files combined')
    
    # calculate cross sections
    Vmax = 59
    Jmax = 269
    fort30 = os.path.join(foldername,'30.h5')
    IC = 'V=20J=0'
    Calculate_Unitform_Xsec(fort30,Vmax,Jmax,IC)