#!/usr/bin/python -e
# Copyright (C) Han Luo
# Licensed under the GNU General Public License version 3 or (at your
# option) any later version. See the file COPYING for details.
#
# Derive dependency for codes
import re, sys, os,glob

class FortranFile:
    def __init__(self,fname):
        self.fname = fname
        self.mod = []
        self.use = []
        self.inc = []
        self.findmod()

    def findmod(self):
        remod = re.compile('^\s*module\s+(.*)')
        remoduse = re.compile('^\s*use\s+([\w]*)(?:\,\s*only\s*:)?')
        reinc = re.compile('^\s*include\s+[\'"]([\w\.]+)[\'"]')
        mymod = []
        moduse = []
        myinc = []
        with open(self.fname,'r') as f:
            for line in f:
                mymod += remod.findall(line.lower())
                moduse += remoduse.findall(line.lower())
                myinc += reinc.findall(line.lower())
        moduse = list(set(moduse))
        myinc = list(set(myinc))
        for i in mymod:
            if i in moduse:
                moduse.remove(i)
        self.mod = sorted(mymod)
        self.use = sorted(moduse)
        self.inc = sorted(myinc)

    def __str__(self):
        line = "Module have:\n["
        for j,i in enumerate(self.mod):
            if j < len(self.mod)-1:
                line += i+', '
            else:
                line += i
        line += ']\nModule use:\n['
        for j,i in enumerate(self.use):
            if j < len(self.use)-1:
                line += i+', '
            else:
                line += i
        line +=']'
        return line

class FortranDepend():
    def __init__(self,Ffiles):
        self.nfiles = len(Ffiles)
        index = sorted(range(self.nfiles), key=lambda k:os.path.basename(Ffiles[k].fname))
        self.Ffiles = [Ffiles[i] for i in index]
        self.fname = [os.path.splitext(i.fname)[0]+'.o' for i in self.Ffiles]
        self.depend = [self.derive_depend(i) for i in range(self.nfiles)]

    def derive_depend(self,i):
        depend = []
        if self.Ffiles[i].use:
            for moduse in self.Ffiles[i].use:
                for iFile in self.Ffiles:
                    if moduse in iFile.mod:
                        depend.append(os.path.splitext(iFile.fname)[0]+'.o')
                        break
        if self.Ffiles[i].inc:
            depend = depend+self.Ffiles[i].inc
        if len(depend) > 0:
            if 'param.o' in depend:
                depend.remove('param.o')
                depend = ['param.o']+depend
        return depend

    def write_depend(self,fname):
        maxlen = max(len(i) for i in self.fname)
        maxlen = maxlen+1
        maxlen = 17
        with open(fname,'w') as f:
            for obj,dep in zip(self.fname,self.depend):
                if 'rng_int' in obj:
                    continue
                l = len(dep)
                if dep:
                    k = max(len(i) for i in dep)
                    #strformat = '%-'+'%d'%(maxlen,)+'s: '+('%%%d'%(k)+'s,')*l+'\n'
                    strformat = '%-'+'%d'%(maxlen,)+'s'+('%s ')*(l-1)+'%s'+'\n'
                    strwrite = (obj+':',)+tuple(dep)
                else:
                    strformat = '%-'+'%d'%(maxlen,)+'s'+'\n'
                    strwrite = (obj+':',)
                line = strformat%strwrite
                line = line.rstrip()
                f.write(line+'\n')
            f.write("$(POT).o:        param.o c_dd.o $(patsubst %,dummy,$(POT))\n")
            f.write("$(DIAPOT).o:     param.o c_sys.o")


if __name__ == "__main__":
    src_dir = '.'
    deps_file = 'deps.txt'

    os.chdir(src_dir)
    f90file = glob.glob('*.f90')+glob.glob('*.F90')
    f77file = glob.glob('*.f')+glob.glob('*.for')+glob.glob('*.F')+glob.glob('*.FOR')
    srcfiles = f90file+f77file

    srcfiles = ["atomdata.f90", "param.f90", "c_traj.f90", "c_struct.f90", "c_term.f90", "c_initial.f90", "c_sys.f90", "c_output.f90", "c_dd.f90", "angmom.f90", "ant.f90", "actionint.f90", "actionint_nt.f90", "adjtemp.f90", "adjpress.f90", "adtod.f90", "arcom.f90", "atomdiatom.f90", "bmat.f90", "brent.f90", "bsstep.f90", "carttojac.f90", "checkfrus.f90", "checkhop.f90", "cnsvangmom.f90", "comgen.f90", "decocheck.f90", "derivs.f90", "diamin.f90", "diamax.f90", "driver.f90", "driverim.f90", "detmtbt.f90", "elecdeco.f90", "erotc.f90", "ewkb.f90", "fileopen.f90", "finalstate.f90", "frag.f90", "fragcom.f90", "func.f90", "gaussq.f", "gen_prt.f90", "geom_opt.f90", "getdvec2.f90", "getdvec.f90", "getgrad.f90", "getgrad2.f90", "getpem.f90", "getrho.f90", "getrhocsdm.f90", "gettemp.f90", "getpress.f90", "gepol_mod.f", "gepol_unmod.f90", "setupvolume.f90", "volume_interface.f90", "hardwall.f90", "header.f90", "hop.f90", "honey.f90", "ifsame.f90", "ifsame2.f90", "initelec.f90", "initmol.f90", "initrot.f90", "inittrans.f90", "intcart.f90", "intcoord.f90", "integhop.f90", "invmtbt.f90", "lindemann.f90", "liouville.f90", "matprint.f90", "mmid.f90", "momigen.f90", "noang.f90", "nocompp.f90", "normod.f90", "normod-trapz.f90", "period.f90", "periodimage.f90", "pjctmomen.f90", "pjsplit.f90", "popmod.f90", "popnorm.f90", "preatomdiatom.f90", "premol.f90", "pzextr.f90", "radialdist.f90", "ranclucub.f90", "ranclusp.f90", "rancluns.f90", "ranmolcub.f90", "ranrot.f90", "rantherm.f90", "readin.f90", "relenerg.f90", "rijmatr.f90", "rminfrag.f90", "rk4.f90", "rng_int.F90", "rotprin.f90", "rottran.f90", "rtox.f90", "scale.f90", "stodeco.f90", "takestep.f90", "takestep2.f90", "trapz.f90", "turn.f90", "turnpt.f90", "utility.f", "verlet.f90", "vwkb.f90", "xptoy.f90"]
    Ffiles = [None]*len(srcfiles)
    for i,fname in enumerate(srcfiles):
        Ffiles[i] = FortranFile(fname)

    Fdep = FortranDepend(Ffiles)
    Fdep.write_depend(deps_file)

