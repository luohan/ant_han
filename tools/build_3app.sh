#!/bin/bash -e
install_mod () {
  if [[ "$(hostname)" == *"ecn"* ]]; then
     modfile=$HOME/.local/modulefiles/ant/$1.lua
  else
    modfile=$HOME/privatemodules/ant/$1.lua
   fi 
   echo $modfile
  mkdir -p $(dirname $modfile)
cat << EOF > $modfile
whatis("Description: ANT for PES ${POT}")
load("intel")
prepend_path ('PATH', "${CWD}/exe")
prepend_path ('PYTHONPATH', "${CWD}/tools/Post/")
execute{cmd="ln -sf ${CWD}/exe/$2 ${CWD}/exe/ant", modeA={"load"}}
execute{cmd="rm -f  ${CWD}/exe/ant", modeA={"unload"}}
EOF
}

CWD=${HOME}/Soft/QCT/ant_han
POT=n2o_3app_Gamallo
DIAPOT=diapot2
NAME=n2o_3app
ant_exe=ANT-${POT}.x.opt

cd $CWD/src
module load intel
make all POT=${POT} DIAPOT=$DIAPOT -j 4
install_mod $NAME $ant_exe

  
