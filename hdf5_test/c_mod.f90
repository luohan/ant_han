
      module c_struct
        use param, only : mnat
        implicit none
        integer :: nat
        character*2 :: symbol0(mnat)
        double precision :: mm0(mnat),xxm(3,mnat)
      end module

      module c_sys
        implicit none
        integer :: nsurft,ntraj
        double precision :: hstep0
      end module

      module c_initial
        implicit none
        integer :: nsurf0
        real(8) :: bminad,bmaxad
      end module

      module c_traj
        use param, only: mnat, mngauss, mnsurf, mntmod, mntor, mnmol
        implicit none
        double precision :: pe,ke,te,ppm(3,mnat)
      end module

			module c_output
			  implicit none
			  integer :: nprint
			end module

