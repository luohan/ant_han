      program main2
        use hdf5_high
        implicit none
        character(len=30) :: fname='test2.h5',datname='DS1'
        integer :: ndat = 1,ih5file=1
        integer :: dims(2)=(/1,1/),edim(2)=(/3,2/)
        integer,allocatable :: dat(:,:)
        integer :: i,j,k

        call hdf5h_allocate(1)
        call hdf5h_file_open(fname,ndat,ih5file)
        call hdf5h_create_data(datname, ih5file, dims,dtype_opt=-104, edim_opt=edim)

        do i = 1,10
          allocate(dat(3,2*i))
          do j = 1,3
            do k=1,2*i
              dat(j,k) = j+3*(k-1)+i
            end do
          end do

          write(*,"('dat ',I2,':')") i
          do j = 1,3
            write(*,*) (dat(j,k),k=1,2*i)
          end do

          call hdf5h_write_vdata(dat,ih5file,idat_opt=1,offset_opt=i-1)
          deallocate(dat)
        end do
        call hdf5h_file_close(ih5file)
        call hdf5h_deallocate()
      end program

