! program to test hdf5_output and hdf5_high
      program test
        use c_struct,  only: nat,symbol0,mm0,xxm
        use c_sys,     only: nsurft,ntraj,hstep0
        use c_initial, only: nsurf0,bmaxad,bminad
        use c_traj,    only: ppm,pe,ke,te
				use c_output,  only: nprint
        use param, only : amutoau,autofs,autoev,autoang
        use hdf5_output
        implicit none
        integer :: i,randseed=15,ithistraj=1,arr,k,istep,fstep
        real(8) :: b,Et0,VJ,theta,phi,eta,psi,Etf,V,J,chi,time,dEtot

        bminad = 0.0d0
        bmaxad = 5.0d0
        nat = 3
				nprint = 2
        symbol0(1) = 'o'
        symbol0(2) = 'n'
        symbol0(3) = 'n'
        mm0(1) = 16.d0*amutoau
        mm0(2) = 14.d0*amutoau
        mm0(3) = 14.d0*amutoau
        nsurft = 1
        nsurf0 = 1
        ntraj = 1000
        hstep0 = 0.1/autofs
        do i = 1,7
          lwrite_h5(UNIT_HDF5(i)) = .TRUE.
        end do
        call hdf5o_init(randseed)


        do ithistraj=1,2
          fstep = 10*ithistraj
          call hdf5o_trj_init()
          time = 0.0d0
          do istep = 1,fstep
            do i=1,3
              do k = 1,3
                xxm(k,i) = dble(k+10*(i-1))+dble(ithistraj)+dble(istep-1)+0.1d0
                ppm(k,i) = dble(k+20*(i-1))+dble(ithistraj)+dble(istep-1)+0.1d0
              end do
            end do
            pe = 1.2345+dble(ithistraj)+dble(istep-1)
            ke = 5.4321+dble(ithistraj)+dble(istep-1)
            time = time + istep*hstep0

            if (istep == 1)then
              write(*,*) "xxm:"
              do i=1,3
                write(*,*) (xxm(k,i), k=1,3)
              end do
              write(*,*) "pe"
              write(*,*) pe
              write(*,*) "ppmm:"
              do i=1,3
                write(*,*) (ppm(k,i), k=1,3)
              end do
              write(*,*) "ke"
              write(*,*) ke
              call hdf5o_write_peratom(10,ithistraj,pe,xxm(:,1:nat))
              call hdf5o_write_peratom(11,ithistraj,ke,ppm(:,1:nat))
            end if

            if (mod(istep,nprint) == 0) then
              call hdf5o_trj_insert_22(nat,time*autofs,pe*autoev,xxm*autoang)
              call hdf5o_trj_insert_23(nat,time*autofs,ke*autoev,ppm)
              call hdf5o_trj_move()
            end if


            if (istep == fstep) then
              b=1.0d0+dble(ithistraj)
              Et0=2.0d0+dble(ithistraj)
              VJ=10.01+dble(ithistraj)
              theta=3.0d0+dble(ithistraj)
              phi=4.0d0+dble(ithistraj)
              eta = 5.0d0+dble(ithistraj)
              psi = 6.0d0+dble(ithistraj)
              arr=2
              Etf = 2.00001d0+dble(ithistraj)
              V=9.8d0+dble(ithistraj)
              J =  200.0d0+dble(ithistraj)
              chi = 1.11d0+dble(ithistraj)
              dEtot = Et0-Etf

              call hdf5o_write_array(30,ithistraj,(/b,Et0,VJ,theta,phi,eta,psi/),&
                (/dble(arr),Etf,V,J,chi,time,dEtot/))
              call hdf5o_trj_end(nat,ithistraj)
            end if
          end do
        end do
        call hdf5o_end()

      end program
