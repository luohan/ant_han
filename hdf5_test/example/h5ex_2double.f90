!************************************************************
!
!  This example show how to save a variables size array
!
!  This file is intended for use with HDF5 Library version 1.8
!  with --enable-fortran2003
!
!************************************************************

PROGRAM main

  USE HDF5
  USE ISO_C_BINDING

  IMPLICIT NONE

  CHARACTER(LEN=18), PARAMETER :: filename  = "h5ex_double.h5"
  CHARACTER(LEN=3) , PARAMETER :: dataset   = "DS1"
  INTEGER, PARAMETER :: LEN0 = 3,LEN1=2,LEN2=3

  INTEGER(HID_T)  :: file, filetype, memtype, space, dset,atype ! Handles
  INTEGER :: hdferr
  INTEGER(HSIZE_T), DIMENSION(1:2)   :: maxdims
  INTEGER(HSIZE_T) :: i, j

  ! vl data
  TYPE vl
     real(8), DIMENSION(:,:), POINTER :: data
  END TYPE vl
  TYPE(vl), DIMENSION(:), ALLOCATABLE :: ptr

  TYPE(hvl_t), DIMENSION(1:2), TARGET :: wdata ! Array of vlen structures
  TYPE(hvl_t), DIMENSION(1:2), TARGET :: rdata ! Pointer to vlen structures

  INTEGER(hsize_t), DIMENSION(1:1) :: dims = (/2/)
  integer(hsize_t) :: adim(1)
  INTEGER, DIMENSION(:), POINTER :: ptr_r
  TYPE(C_PTR) :: f_ptr

  !
  ! Initialize FORTRAN interface.
  !
  CALL h5open_f(hdferr)
  !
  ! Initialize variable-length data.  wdata(1) is a countdown of
  ! length LEN0, wdata(2) is a Fibonacci sequence of length LEN1.
  !

  ALLOCATE( ptr(1:2) )
  ALLOCATE( ptr(1)%data(LEN0,LEN1) )
  ALLOCATE( ptr(2)%data(LEN0,LEN2) )


  wdata(1)%len = LEN1

  adim = (/LEN0/)
  do i=1,LEN1
    do j = 1,LEN0
      ptr(1)%data(j,i) = dble(j+LEN0*(i-1))+0.2d0
    end do
  end do
  write(*,*) "data1"
  do i=1,LEN0
    write(*,*) (ptr(1)%data(i,j),j=1,LEN1)
  end do
  wdata(1)%p = C_LOC(ptr(1)%data(1,1))


  wdata(2)%len = LEN2
  do i=1,LEN2
    do j = 1,LEN0
      ptr(2)%data(j,i) = dble(j+LEN0*(i-1))+0.1d0
    end do
  end do
  write(*,*) "data2"
  do i=1,LEN0
    write(*,*) (ptr(2)%data(i,j),j=1,LEN2)
  end do
  wdata(2)%p = C_LOC(ptr(2)%data(1,1))

  !
  ! Create a new file using the default properties.
  !
  CALL h5fcreate_f(filename, H5F_ACC_TRUNC_F, file, hdferr)
  !
  ! Create variable-length datatype for file and memory.
  !
  call h5tarray_create_f(H5T_NATIVE_DOUBLE, 1, adim, atype, hdferr)

  !CALL h5tvlen_create_f(H5T_NATIVE_DOUBLE, filetype, hdferr)
  CALL h5tvlen_create_f(atype, filetype, hdferr)
  CALL h5tvlen_create_f(atype, memtype, hdferr)
  !
  ! Create dataspace.
  !
  CALL h5screate_simple_f(1, dims, space, hdferr)
  !
  ! Create the dataset and write the variable-length data to it.
  !
  CALL h5dcreate_f(file, dataset, filetype, space, dset, hdferr)

  f_ptr = C_LOC(wdata(1))
  CALL h5dwrite_f(dset, memtype, f_ptr, hdferr)

  CALL h5dclose_f(dset , hdferr)
  CALL h5sclose_f(space, hdferr)
  CALL h5tclose_f(filetype, hdferr)
  CALL h5tclose_f(memtype, hdferr)
  CALL h5fclose_f(file , hdferr)
  DEALLOCATE(ptr)

  ! !
  ! ! Now we begin the read section of this example.
!
  ! !
  ! ! Open file and dataset.
  ! !
  ! CALL h5fopen_f(filename, H5F_ACC_RDONLY_F, file, hdferr)
  ! CALL h5dopen_f(file, dataset, dset, hdferr)
!
  ! !
  ! ! Get dataspace and allocate memory for array of vlen structures.
  ! ! This does not actually allocate memory for the vlen data, that
  ! ! will be done by the library.
  ! !
  ! CALL h5dget_space_f(dset, space, hdferr)
  ! CALL h5sget_simple_extent_dims_f(space, dims, maxdims, hdferr)
!
  ! !
  ! ! Create the memory datatype.
  ! !
  ! CALL h5tvlen_create_f(H5T_NATIVE_INTEGER, memtype, hdferr)
!
  ! !
  ! ! Read the data.
  ! !
  ! f_ptr = C_LOC(rdata(1))
  ! CALL h5dread_f(dset, memtype, f_ptr, hdferr)
  ! !
  ! ! Output the variable-length data to the screen.
  ! !
  ! DO i = 1, dims(1)
     ! WRITE(*,'(A,"(",I0,"):",/,"{")', ADVANCE="no") dataset,i
     ! CALL c_f_pointer(rdata(i)%p, ptr_r, [rdata(i)%len] )
     ! DO j = 1, rdata(i)%len
        ! WRITE(*,'(1X,I0)', ADVANCE='no') ptr_r(j)
        ! IF ( j .LT. rdata(i)%len) WRITE(*,'(",")', ADVANCE='no')
     ! ENDDO
     ! WRITE(*,'( " }")')
  ! ENDDO
!
  ! !
  ! ! Close and release resources.  Note the use of H5Dvlen_reclaim
  ! ! removes the need to manually deallocate the previously allocated
  ! ! data.
  ! !
  ! CALL h5dvlen_reclaim_f(memtype, space, H5P_DEFAULT_F, f_ptr, hdferr)
  ! CALL h5dclose_f(dset , hdferr)
  ! CALL h5sclose_f(space, hdferr)
  ! CALL h5tclose_f(memtype, hdferr)
  ! CALL h5fclose_f(file , hdferr)
!
END PROGRAM main
